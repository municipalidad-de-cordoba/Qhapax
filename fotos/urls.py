from django.conf.urls import url
from . import views


urlpatterns = [url(r'^$',
                   views.index,
                   name='index'),
               url(r'^foto/(?P<foto_id>[0-9]+)$',
                   views.foto,
                   name='fotos.foto'),
               url(r'^foto/(?P<process>[\w-]+)/(?P<foto_id>[0-9]+)$',
                   views.foto,
                   name='fotos.foto.process'),
               ]
