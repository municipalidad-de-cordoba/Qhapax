from django.db import models
from core.models import Persona
from funcionarios.models import Cargo
from versatileimagefield.fields import VersatileImageField, PPOIField


class TagPrensa(models.Model):
    '''
    Tag para buscar la foto despues
    '''
    nombre = models.CharField(max_length=90)

    def __str__(self):
        return self.nombre


class AlbumPrensa(models.Model):
    '''
    Fotos de uso interno de la gestion
    Las oficinas de medios en general necesitan un repositorio de imágenes
    #TODO extraer metadatos de exif y usar cuando sean interesantes
    '''
    titulo = models.CharField(max_length=90, null=True, blank=True)
    descripcion = models.CharField(max_length=200, null=True, blank=True)

    tags = models.ManyToManyField(TagPrensa, blank=True)
    # personas que aparecen en la foto
    personas = models.ManyToManyField(Persona, blank=True)
    # oficinas de gobierno vinculadas
    oficinas = models.ManyToManyField(Cargo, blank=True)

    # metadata
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.titulo


class ImagenPrensa(models.Model):
    imagen = VersatileImageField(
        upload_to='prensa',
        height_field='alto',
        width_field='ancho',
        ppoi_field='ppoi')

    titulo = models.CharField(max_length=90, null=True, blank=True)
    descripcion = models.CharField(max_length=200, null=True, blank=True)

    publica = models.BooleanField(default=False)
    tags = models.ManyToManyField(TagPrensa, blank=True)
    # personas que aparecen en la foto
    personas = models.ManyToManyField(Persona, blank=True)
    # oficinas de gobierno vinculadas
    oficinas = models.ManyToManyField(Cargo, blank=True)

    # Metadata
    album = models.ForeignKey(AlbumPrensa)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)

    ancho = models.PositiveIntegerField(
        null=True, blank=True, default=0)  # se carga automaticamente
    alto = models.PositiveIntegerField(
        null=True, blank=True, default=0)  # se carga automaticamente
    ppoi = PPOIField('Image PPOI')

    def __str__(self):
        return '{} ({})'.format(self.titulo, self.album.titulo)
