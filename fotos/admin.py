from django.contrib import admin
from .models import AlbumPrensa, ImagenPrensa, TagPrensa


class AlbumPrensaAdmin(admin.ModelAdmin):
    exclude = ('ancho', 'alto')
    list_display = ('id', 'titulo')
    filter_horizontal = ('tags', 'personas', 'oficinas')
    list_filter = ['tags', 'personas', 'oficinas']


class ImagenPrensaAdmin(admin.ModelAdmin):
    exclude = ('ancho', 'alto')
    list_display = ('id', 'titulo')
    filter_horizontal = ('tags', 'personas', 'oficinas')
    list_filter = ['tags', 'personas', 'oficinas']


admin.site.register(AlbumPrensa, AlbumPrensaAdmin)
admin.site.register(ImagenPrensa, ImagenPrensaAdmin)
admin.site.register(TagPrensa)
