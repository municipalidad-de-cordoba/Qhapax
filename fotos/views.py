from django.http import HttpResponse
from .models import ImagenPrensa
from .lib.myimagekit import Thumb


def index(request):
    return HttpResponse("Aplicación para contenido de prensa")


def foto(request, foto_id, process=None):
    '''
    procesar on the fly una imagen con resizes, filtros o cortes predefinidos
    el argumento process son indicadores separados por guiones - con diferentes
      datos a cerca de la imagen que se espera generar <on the fly>
    '''
    foto = ImagenPrensa.objects.get(pk=foto_id)
    if not process:
        return HttpResponse(foto.imagen.read(), content_type="image/png")

    args = process.split('-')
    params = {}
    for arg in args:
        if arg.startswith('format'):  # ejemplos -formatJPEG -formatPNG
            params['fformat'] = arg[6:]
        elif arg == 'bw':  # blanco y negro
            params['bland_and_white'] = True
        elif arg.startswith('ql'):
            # ejemplos -ql40 dara una imagen comprimida al 40%
            params['quality'] = int(arg[2:])
        elif arg.startswith('w'):  # ejemplos -w251
            params['width'] = int(arg[1:])
        elif arg.startswith('h'):  # ejemplos -h183
            params['height'] = int(arg[1:])

    ret = Thumb(source=foto.imagen).on_the_fly(**params)

    return HttpResponse(ret.read(), content_type="image/png")
