'''
Librería de procesamiento de imagenes
http://django-imagekit.readthedocs.io/en/latest/
'''
from imagekit import ImageSpec
from imagekit.processors import ResizeToFill, ResizeToFit, SmartResize, Adjust


class Thumb(ImageSpec):
    processors = []
    format = 'JPEG'
    options = {'quality': 90}

    def on_the_fly(self, width=None, height=None, fformat='JPEG', quality=80,
            bland_and_white=False):
        wh = {}
        if width:
            wh['width'] = width
        if height:
            wh['height'] = height
        procs = []
        if width or height:
            procs = [ResizeToFit(**wh)]

        if bland_and_white:
            procs.append(Adjust(color=0))
        self.processors = procs
        self.format = fformat
        self.options = {'quality': quality}
        return self.generate()
