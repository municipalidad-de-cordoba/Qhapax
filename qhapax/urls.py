"""
qhapax URL Configuration
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import logout
from rest_framework_cache.registry import cache_registry
from django.views.generic.base import RedirectView
from website.views_search import QhapaxSearchView
from login_usuarios.views import TwitterLogin, TwitterConnect
from rest_auth.registration.views import (
    SocialAccountListView, SocialAccountDisconnectView
)
from allauth.account.views import confirm_email as allauthemailconfirmation
from rest_framework_jwt.views import verify_jwt_token

cache_registry.autodiscover()

urlpatterns = [
    url(r'^', include('website.urls')),
    url(r'^select2/', include('django_select2.urls')),
    # formularios FOBI|
    url(r'^formularios/ver-formularios/', include('fobi.urls.view')),
    url(r'^formularios/editar-formularios/', include('fobi.urls.edit')),
    url(r'^formularios/form-handlers/db-store/', include('fobi.contrib.plugins.form_handlers.db_store.urls')),

    url(r'^funcionarios/', include('funcionarios.urls')),
    url(r'^entes-privados/', include('entesprivados.urls')),
    url(r'^transporte-publico/', include('transportepublico.urls')),
    url(r'^boletines-municipales/', include('boletinoficial.urls')),
    url(r'^fotos/', include('fotos.urls')),
    url(r'^datos/', include('portaldedatos.urls')),
    url(r'accounts/logout/$', logout, {'next_page': '/'}, name="logout"),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^api/', include('api.urls')),
    url(r'^registro-civil/', include('registro_civil.urls')),
    url(r'^api/docs/', include('rest_framework_docs.urls')),
    url(r'^administrador/', include('backend.urls')),
    url(r'^alumbrado/', include('alumbrado.urls')),
    url(r'^acceso-informacion-publica/', include('accesoinfopublica.urls')),
    url(r'^acceso-audiencia/', include('audienciafuncionarios.urls')),
    url(r'^propuesta-ciudadana/', include('propuestaciudadana.urls')),
    url(r'^software-publico/', include('softwaremunicipal.urls')),
    url(r'^obras-publicas/', include('obraspublicas.urls')),
    url(r'^obras-de-terceros/', include('obrasdeterceros.urls')),
    url(r'^eventos-publicos/', include('eventospublicos.urls')),
    url(r'^plan-de-metas/', include('plandemetas.urls')),
    url(r'^guias-turisticos/', include('guiasturisticos.urls')),
    url(r'^credenciales-prensa/', include('credencialesprensa.urls')),
    url(r'^credenciales/', include('credenciales.urls')),
    url(r'^convenios/', include('convenios.urls')),
    url(r'^cooperativas-estacionamiento/', include('cooperativas_estacionamiento.urls')),
    url(r'^credenciales-habilitadores-de-negocios/', include('credencialeshabilitaciondenegocios.urls')),
    url(r'^organizaciones/', include('organizaciones.urls')),
    url(r'^centros-vecinales/', include('centrosvecinales.urls')),
    url(r'^grappelli/', include('grappelli.urls')),  # pa'l admin
    url(r'^admin/', admin.site.urls),  # temporarlmente mientras el otro se construye
    url(r'^search/', QhapaxSearchView.as_view(), name='qhapax.search_view'),
    # url(r'^search/', include('haystack.urls')),
    url(r'^geo/', include('geo.urls')),
    url(r'^pagos/', include('pagos.urls')),
    url(r'^visualizaciones/', include('visualizaciones.urls')),
    url(r'^tarifaria/', include('ordenanzatributaria.urls')),
    url(r'^blockchain/', include('blockchain.urls')),
    url(r'^externals/', include('externals.urls')),
    url(r'^quinteros/', include('quinteros.urls')),
    url(r'^tableros-internos/', include('tableros_internos.urls')),
    url(r'^ordenes-de-publicidad/', include('ordenesdepublicidad.urls')),
    url(r'^ventanilla-unica/', include('ventanilla_unica.urls')),
    # ---------------------Logins----------------------------
    url(r'^login/', include('login_usuarios.urls')),
    url(r'^cuentas/', include('allauth.urls')),
    url(r'^social-auth/', include('social_django.urls', namespace='social')),
    url(r'^api-token-verify/', verify_jwt_token),
    url(r'^rest-auth/registration/account-confirm-email/(?P<key>[\s\d\w().+-_\',:&]+)/$',
        allauthemailconfirmation, name="account_confirm_email"),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/twitter/$', TwitterLogin.as_view(), name='twitter_login'),
    url(r'^rest-auth/twitter/connect/$', TwitterConnect.as_view(), name='twitter_connect'),
    # url(r'^rest-auth/github/$', GithubLogin.as_view(), name='github_login'),
    # url(r'^rest-auth/github/connect/$', GithubConnect.as_view(), name='github_connect'),
    # url(r'^rest-auth/google/$', GoogleLogin.as_view(), name='google_login'),
    # url(r'^rest-auth/google/connect/$', GoogleConnect.as_view(), name='google_connect'),
    url(r'^socialaccounts/$', SocialAccountListView.as_view(), name='social_account_list'),
    url(r'^socialaccounts/(?P<pk>\d+)/disconnect/$', SocialAccountDisconnectView.as_view(), name='social_account_disconnect'),

    url(r'^arbolado/', include('arbolado.urls')),
    url(r'^ambiente/', include('ambiente.urls')),
    # ------------ redirecciones simples --------------------------------
    url(r'^apps$',
            RedirectView.as_view(url='software-publico/aplicaciones-moviles/'),
            name='seccion-apps'),
    url(r'^portal-de-mapas$',
            RedirectView.as_view(url='https://modernizacionmunicba.github.io/portal-de-mapas/www/#mapa-6'),
            name='portal-de-mapas'),
    url(r'^app-cultura$',
            RedirectView.as_view(url='https://modernizacionmunicba.github.io/agenda-cultural-de-cordoba'),
            name='apps-cultura'),
    url(r'^app-turismo$',
            RedirectView.as_view(url='https://modernizacionmunicba.github.io/eventos-turismo'),
            name='apps-turismo'),
    url(r'^cuantotengo$',
            RedirectView.as_view(url='software-publico/aplicaciones-moviles/'),
            name='apps-cuanto-tengo'),
    url(r'^presupuesto-2017$',
            RedirectView.as_view(url='https://modernizacionmunicba.github.io/presupuesto-cordoba-2017'),
            name='presupuesto.2017'),
    url(r'^presupuesto-abierto-ciudadano$',
            RedirectView.as_view(url='https://modernizacionmunicba.github.io/presupuesto-cordoba-2017'),
            name='presupuesto-abierto-ciudadano.2017'),



    # ------------ redirecciones QR --------------------------------
    # redicciones QR. Usado para emitir códigos QR y poder
    # definir su funcionalidad luego o cambiar con el tiempo
    # esto debería ser un plugin o no estar aquí
    url(r'^qr/feria-del-libro$',
            RedirectView.as_view(url='http://cultura.cordoba.gov.ar/'),
            name='qr-feria-del-libro'),

    url(r'^consultas-guia-estadistica$',
            RedirectView.as_view(url='https://docs.google.com/a/data99.com.ar/forms/d/e/1FAIpQLSe14TWXxyW-vX4lFA0lzBW2TZOnOGja57fFu8hg-WgBvzj8ww/viewform'),
            name='consultas-guia-estadistica'),

    url(r'^transporte-de-emergencia$',
            RedirectView.as_view(url='https://goo.gl/3kJ9Pw'),
            name='transporte-de-emergencia'),


] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
