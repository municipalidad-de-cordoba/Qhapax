import os
from kombu import Exchange, Queue
from celery.schedules import crontab


# Añadimos la siguiente opción url del broker al que se conectará celery.
CELERY_BROKER_URL='redis://redis:6379'
CELERY_RESULT_BACKEND = 'redis://redis:6379'

BROKER_URL = ''

# We don't want to have dead connections stored on rabbitmq, so we have to negotiate using heartbeats
BROKER_HEARTBEAT = '?heartbeat=30'
if not BROKER_URL.endswith(BROKER_HEARTBEAT):
    BROKER_URL += BROKER_HEARTBEAT

BROKER_POOL_LIMIT = 1
BROKER_CONNECTION_TIMEOUT = 10

# Celery configuration

# configure queues, currently we have only one
CELERY_DEFAULT_QUEUE = 'default'
CELERY_QUEUES = (
    Queue('default', Exchange('default'), routing_key='default'),
)

# los mjs de tareas son reconocidos después de que se haya ejecutado la tarea
CELERY_ACKS_LATE = True

# si se almacena o no los valores de retorno de la tarea
CELERY_IGNORE_RESULT = True

CELERY_REDIS_MAX_CONNECTIONS = 1

# Don't use pickle as serializer, json is much safer
CELERY_TASK_SERIALIZER = "json"
CELERY_ACCEPT_CONTENT = ['application/json']

# Se puede personalizar los propios manejadores de registro(logging)
CELERYD_HIJACK_ROOT_LOGGER = False

# http://celery.readthedocs.io/en/latest/userguide/configuration.html#std:setting-worker_prefetch_multiplier
CELERYD_PREFETCH_MULTIPLIER = 1

# maximo numero de tareas por proceso
CELERYD_MAX_TASKS_PER_CHILD = 1000

# configurar las tareas periodicas acá(para celery beat)
CELERY_BEAT_SCHEDULE = {
    # ejecuta la tarea todos los días a las 6 a.m
    'importar_nacimientos_diario': {
        'task': 'cordobaplantavida.tasks.importar_nacimientos',
        'schedule': crontab(hour='6', minute=0),
    },
    # ejecuta la tarea todos los domingos a las 11 a.m.
    'importar_nacimientos_semanalmente': {
        'task': 'cordobaplantavida.tasks.importar_nacimientos',
        'schedule': crontab(hour=11, minute=0, day_of_week='sunday')
    },
    # ejecuta la tarea el segundo dia de cada mes
    'importar_nacimientos_mensualmente': {
        'task': 'cordobaplantavida.tasks.importar_nacimientos',
        'schedule': crontab(hour=5, minute=0, day_of_month='2')
    },
    # Guardamos la cantidad de usuario por dia de GO
    'guardar_usuarios_go': {
        'task': 'softwaremunicipal.tasks.grabarusuariospordias',
        'schedule': crontab(hour=0, minute=1)
    },
}

CELERY_TIMEZONE = 'America/Argentina/Cordoba'

try:
    from qhapax.local_settings_celery import *
except ImportError:
    pass