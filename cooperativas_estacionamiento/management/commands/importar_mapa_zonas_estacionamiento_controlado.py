#!/usr/bin/python
"""
De cada obra hay trazadas (información GEO) aquí se importa esta parte
"""
from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from django.contrib.gis.geos import LineString
from fastkml import kml
from cooperativas_estacionamiento.models import (CooperativaDeEstacionamiento,
                                                 ZonasDeEstacionamiento)
import re

TAG_RE = re.compile(r'<[^>]+>')


class Command(BaseCommand):
    help = """Comando para Importar mapa kml de zonas de estacionamiento controlado """

    def add_arguments(self, parser):
        parser.add_argument(
            '--path',
            type=str,
            help='Path del archivo KML local')

    @transaction.atomic
    def handle(self, *args, **options):
        cooperativas_id = {
            "Coop Parque Ltda": 40333,
            "Coop LA UNION LTDA": 30867,
            "Coop APARCAR LTDA": 27736,
            "Coop CVA LTDA": 28982,
            "Coop PROGRESO LTDA": 39979}

        self.stdout.write(self.style.SUCCESS(
            'Iniciando importación de zonas de estacionamiento'))
        url = options['path']

        try:
            with open(url, 'rt', encoding="utf-8") as myfile:
                doc = myfile.read().encode('utf-8')
            self.stdout.write(
                self.style.SUCCESS(
                    'Buscando datos en {}'.format(url)))
        except Exception as e:
            self.stdout.write(
                self.style.ERROR(
                    'Error al leer de {}'.format(url)))
            sys.exit(1)

        # Create the KML object to store the parsed result
        k = kml.KML()

        # Read in the KML string
        k.from_string(doc)
        features = list(k.features())

        for feature in features:
            self.stdout.write("=============================")
            self.stdout.write("Doc: {}".format(feature.name))
            self.stdout.write("=============================")

            folders = list(feature.features())
            for folder in folders:
                # se omite la zona de estacionamiento sin control de
                # cooperativas
                if folder.name == 'AREA SEMM':
                    pass
                else:
                    self.stdout.write("=============================")
                    self.stdout.write("Folder: {}".format(folder.name))
                    self.stdout.write("=============================")

                    places = list(folder.features())
                    for place in places:
                        if place.name in cooperativas_id:
                            coop, created = CooperativaDeEstacionamiento.objects.get_or_create(
                                matricula=cooperativas_id[place.name])
                            # se crea una nueva zona por cada iteración
                            zona = ZonasDeEstacionamiento.objects.create(
                                cooperativa=coop)
                            # Algunas zonas no tienen descripcion
                            if place.description:
                                # elimino tags html
                                zona.descripcion = TAG_RE.sub(
                                    '', place.description)
                            else:
                                zona.descripcion = None
                            # LineString es 2-dim en este caso, sino da error!
                            geometry = LineString(
                                [xy[0:2] for xy in place.geometry.coords])
                            zona.zona = geometry
                            try:
                                zona.save()
                            except Exception as e:
                                self.stdout.write(
                                    self.style.ERROR(
                                        'Error {}. Al grabar zona de: {}'.format(
                                            e, place.name)))
                                sys.exit(1)
                        else:
                            self.stdout.write(
                                self.style.ERROR(
                                    'Error de formato {}'.format(
                                        place.name)))
                            sys.exit(1)
        self.stdout.write("TODO OK")
        self.stdout.write("FIN")
