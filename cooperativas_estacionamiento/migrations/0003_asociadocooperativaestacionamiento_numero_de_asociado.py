# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-02 13:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cooperativas_estacionamiento', '0002_auto_20171130_1247'),
    ]

    operations = [
        migrations.AddField(
            model_name='asociadocooperativaestacionamiento',
            name='numero_de_asociado',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
    ]
