# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-30 15:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cooperativas_estacionamiento', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comunucacioncooperativaestacionamiento',
            name='objeto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contacto', to='cooperativas_estacionamiento.CooperativaDeEstacionamiento'),
        ),
    ]
