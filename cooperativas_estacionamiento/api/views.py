from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import (AsociadoCooperativaEstacionamientoSerializer,
                          CooperativaDeEstacionamientoSerializer,
                          CargoCooperativaEstacionamientoSerializer)
from api.pagination import DefaultPagination
from cooperativas_estacionamiento.models import (CooperativaDeEstacionamiento,
                                                 AsociadoCooperativaEstacionamiento,
                                                 CargoCooperativaEstacionamiento)
from django.db.models import Q


class CooperativaDeEstacionamientoViewSet(viewsets.ModelViewSet):
    """
    Lista de cooperativas de estacionamiento y sus asociados
    Se puede buscar por nombre con el param "q"
    """
    serializer_class = CooperativaDeEstacionamientoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CooperativaDeEstacionamiento.objects.all().order_by('nombre')

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(nombre__icontains=q)
            )
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class AsociadoCooperativaEstacionamientoViewSet(viewsets.ModelViewSet):
    """
    Lista de asociados de cooperativas de estacionamiento.
    Se puede buscar por nombre, apellido con el parametro "?q"
    Por ejemplo "?q=perez"
    Se puede buscar por numero de asociado o dni con el parametro "?n".
    Por ejemplo "?n=30123654"
    Se puede buscar por cargo id con el parametro "?cargo_id"
    Por ejemplo "?cargo_id=1"
    """
    serializer_class = AsociadoCooperativaEstacionamientoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = AsociadoCooperativaEstacionamiento.objects.filter(
            activo=True)

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(nombre__icontains=q) |
                Q(apellido__icontains=q)
            )

        n = self.request.query_params.get('n', None)
        if n is not None:
            queryset = queryset.filter(
                Q(numero_de_asociado__icontains=n) |
                Q(unique_id__icontains=n)
            )

        cargo_id = self.request.query_params.get('cargo_id', None)
        if cargo_id is not None:
            queryset = queryset.filter(
                Q(cargo__id=cargo_id)
            )

        queryset.order_by('apellido')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CargoCooperativaEstacionamientoViewSet(viewsets.ModelViewSet):
    """
    Lista de cargos segun id
    """

    serializer_class = CargoCooperativaEstacionamientoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CargoCooperativaEstacionamiento.objects.all()
        queryset.order_by('id')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
