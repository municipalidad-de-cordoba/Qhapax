from django.conf.urls import url, include
from rest_framework import routers
from .views import (AsociadoCooperativaEstacionamientoViewSet,
                    CargoCooperativaEstacionamientoViewSet,
                    CooperativaDeEstacionamientoViewSet)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register('cooperativas-de-estacionamiento',
                CooperativaDeEstacionamientoViewSet,
                base_name='cooperativa-de-estacionamiento.api.lista')
router.register(
    'asociados-cooperativas-de-estacionamiento',
    AsociadoCooperativaEstacionamientoViewSet,
    base_name='asociados-cooperativas-de-estacionamiento.api.lista')
router.register('cargos-cooperativas-de-estacionamiento',
                CargoCooperativaEstacionamientoViewSet,
                base_name='cargo-cooperativas-de-estacionamiento.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
