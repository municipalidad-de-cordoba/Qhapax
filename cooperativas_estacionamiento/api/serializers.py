from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from versatileimagefield.serializers import VersatileImageFieldSerializer
from cooperativas_estacionamiento.models import (CooperativaDeEstacionamiento,
                                                 AsociadoCooperativaEstacionamiento,
                                                 CargoCooperativaEstacionamiento)


class CooperativaDeEstacionamientoSerializer(CachedSerializerMixin):
    contacto = serializers.StringRelatedField(many=True)
    asociados = serializers.StringRelatedField(many=True)

    class Meta:
        model = CooperativaDeEstacionamiento
        fields = ('nombre', 'matricula', 'domicilio_sede_social', 'contacto',
                  'asociados')


class CargoCooperativaEstacionamientoSerializer(CachedSerializerMixin):
    class Meta:
        model = CargoCooperativaEstacionamiento
        fields = '__all__'


class AsociadoCooperativaEstacionamientoSerializer(CachedSerializerMixin):
    cooperativa = serializers.CharField(source='cooperativa.nombre')
    dni = serializers.CharField(source='unique_id')
    cargo = serializers.CharField(source='cargo.nombre')
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail", 'thumbnail__125x125')])

    class Meta:
        model = AsociadoCooperativaEstacionamiento
        fields = (
            'apellido',
            'nombre',
            'numero_de_asociado',
            'dni',
            'cargo',
            'cooperativa',
            'foto')


cache_registry.register(CooperativaDeEstacionamientoSerializer)
cache_registry.register(AsociadoCooperativaEstacionamientoSerializer)
cache_registry.register(CargoCooperativaEstacionamientoSerializer)
