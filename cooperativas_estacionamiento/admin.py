from django.contrib import admin
from .models import (AsociadoCooperativaEstacionamiento,
                     CooperativaDeEstacionamiento,
                     CargoCooperativaEstacionamiento,
                     ComunucacionCooperativaEstacionamiento,
                     ZonasDeEstacionamiento)
from core.models import ComunicacionPersona
from core.admin import QhapaxOSMGeoAdmin


class ComunicacionAsociadoInline(admin.StackedInline):
    model = ComunicacionPersona
    extra = 1


class AsociadoCooperativaEstacionamientoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'apellido', 'activo', 'cargo', 'cooperativa']
    list_filter = ['cargo', 'cooperativa']
    search_fields = [
        'nombre',
        'apellido',
        'unique_id',
        'cargo__nombre',
        'cooperativa__nombre']
    inlines = [ComunicacionAsociadoInline]


class ComunicacionCooperativaInline(admin.StackedInline):
    model = ComunucacionCooperativaEstacionamiento
    extra = 1


class CooperativaDeEstacionamientoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'matricula', 'domicilio_sede_social']
    search_fields = ['nombre', 'matricula', 'domicilio_sede_social']
    inlines = [ComunicacionCooperativaInline]


class ZonasDeEstacionamientoAdmin(QhapaxOSMGeoAdmin):
    list_display = ['cooperativa', 'descripcion']
    search_fields = ['cooperativa']


admin.site.register(
    AsociadoCooperativaEstacionamiento,
    AsociadoCooperativaEstacionamientoAdmin)
admin.site.register(
    CooperativaDeEstacionamiento,
    CooperativaDeEstacionamientoAdmin)
admin.site.register(CargoCooperativaEstacionamiento)
admin.site.register(ZonasDeEstacionamiento, ZonasDeEstacionamientoAdmin)
