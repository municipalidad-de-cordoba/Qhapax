from django.conf.urls import url
from . import views


urlpatterns = [url(r'^lista-de-cooperativas.(?P<filetype>csv|xls)$',
                   views.planilla_cooperativas,
                   name='cooperativas_estacionamiento.lista'),
               url(r'^lista-de-asociados.(?P<filetype>csv|xls)$',
                   views.planilla_asociados,
                   name='cooperativas_estacionamiento.asociados.lista'),
               url(r'^asociado-cooperativa-(?P<pk>[0-9]+).html$',
                   views.ficha_asociado_cooperativa_estacionamiento,
                   name='cooperativas_estacionamiento.asociado.ficha'),
               ]
