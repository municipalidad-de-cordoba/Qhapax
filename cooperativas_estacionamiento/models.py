from core.models import Persona, Comunicacion
from django.contrib.gis.db import models
from django.urls import reverse


class CooperativaDeEstacionamiento(models.Model):
    nombre = models.CharField(max_length=150, null=True, blank=True)
    matricula = models.PositiveIntegerField(default=0)
    domicilio_sede_social = models.CharField(max_length=150)

    def __str__(self):
        return '{} {}'.format(self.nombre, self.matricula)


class ComunucacionCooperativaEstacionamiento(Comunicacion):
    ''' vías de comunicación multiple de las cooperativas (tel, email, web, etc) '''
    objeto = models.ForeignKey(
        CooperativaDeEstacionamiento,
        on_delete=models.CASCADE,
        related_name='contacto')

    class Meta:
        unique_together = (("tipo", "valor", "objeto"),)


class CargoCooperativaEstacionamiento(models.Model):
    # asociado comun, presidente, vice
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre


class AsociadoCooperativaEstacionamiento(Persona):
    ''' cada uno de los asociados, el presidente, el vide, etc '''
    cargo = models.ForeignKey(CargoCooperativaEstacionamiento)
    cooperativa = models.ForeignKey(
        CooperativaDeEstacionamiento,
        related_name='asociados')
    numero_de_asociado = models.CharField(max_length=30, null=True, blank=True)
    activo = models.BooleanField(
        default=True,
        help_text='Desmarcar cuando la cooperativa o la persona indique que ya no esta asociado')

    def get_absolute_url(self):
        return reverse(
            'cooperativas_estacionamiento.asociado.ficha',
            kwargs={
                'pk': self.id})

    def __str__(self):
        nombre = '' if self.nombre is None else self.nombre
        apellido = '' if self.apellido is None else self.apellido
        return '{}: {} {}'.format(self.cargo.nombre, nombre, apellido)


class ZonasDeEstacionamiento(models.Model):
    """
    Zonas de estacionamiento controlado. Basado en el kml publicado
    """
    cooperativa = models.ForeignKey(
        CooperativaDeEstacionamiento,
        related_name='zonas')
    descripcion = models.TextField(null=True, blank=True)
    zona = models.LineStringField(null=True, blank=True)

    def __str__(self):
        return 'Descripción: {}'.format(self.descripcion)
