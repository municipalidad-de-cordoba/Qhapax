from django.apps import AppConfig


class CooperativasEstacionamientoConfig(AppConfig):
    name = 'cooperativas_estacionamiento'
