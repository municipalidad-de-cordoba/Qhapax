from django.shortcuts import render
from cooperativas_estacionamiento.models import (CooperativaDeEstacionamiento,
                                                 AsociadoCooperativaEstacionamiento)
import django_excel as excel
from django.views.decorators.cache import cache_page
from django.shortcuts import get_object_or_404


@cache_page(60 * 60 * 12)  # 12 h
def planilla_cooperativas(request, filetype):
    '''
    Planilla de cooperativas y sus asociados
    http://localhost:8000/naranjitas/lista-de-cooperativas-y-asociados.xls
    '''

    cooperativas = CooperativaDeEstacionamiento.objects.all().order_by('nombre')
    csv_list = []
    csv_list.append(['cooperativa', 'matricula', 'domicilio_sede_social'])

    for cooperativa in cooperativas:
        # presidente = '{}, {}'.format(
        #     cooperativa.presidente.apellido,
        #     cooperativa.presidente.nombre)
        # dni = '{} {}'.format(
        #     cooperativa.presidente.tipo_id,
        #     cooperativa.presidente.unique_id)

        csv_list.append([
            cooperativa.nombre,
            cooperativa.matricula,
            cooperativa.domicilio_sede_social,
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 h
def planilla_asociados(request, filetype):
    '''
    Planilla de cooperativos y sus asociados
    '''

    # FIXME, falla: .order_by(['cooperativa__nombre', 'cargo__nombre',
    # 'apellido'])
    asociados = AsociadoCooperativaEstacionamiento.objects.filter(activo=True)
    csv_list = []

    csv_list.append(['Apellido',
                     'Nombre',
                     'Código asociado',
                     'DNI',
                     'Cooperativa',
                     'Cargo',
                     'URL Foto',
                     'URL Ficha web'])
    base_url = request.website

    for asociado in asociados:
        url = '{}{}'.format(base_url, asociado.get_absolute_url())
        # qr_download = asociado.get_download_link_qr(url=url)
        if asociado.foto.name:
            foto_thumb = "{}{}".format(
                base_url, asociado.foto.thumbnail['600x600'].url)
        else:
            foto_thumb = ""

        numero_de_asociado = 0 if asociado.numero_de_asociado is None else asociado.numero_de_asociado
        codigo = '{}-{}'.format(asociado.id, numero_de_asociado)

        csv_list.append([
            asociado.apellido,
            asociado.nombre,
            codigo,
            asociado.unique_id,
            asociado.cooperativa.nombre,
            asociado.cargo.nombre,
            foto_thumb,
            url
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 h
def ficha_asociado_cooperativa_estacionamiento(request, pk):
    '''
    Ficha del asociados de la cooperativa
    '''
    asociado = get_object_or_404(
        AsociadoCooperativaEstacionamiento,
        pk=pk,
        activo=True)
    base_url = request.website
    url = '{}{}'.format(base_url, asociado.get_absolute_url())
    qr_image = asociado.get_image_qr(url=url)
    qr_download = asociado.get_download_link_qr(url=url)
    context = {
        'asociado': asociado,
        'qr_image': qr_image,
        'qr_download': qr_download}
    url = 'cooperativas_estacionamiento/ficha.html'
    return render(request, url, context)
