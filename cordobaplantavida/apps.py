from django.apps import AppConfig


class CordobaplantavidaConfig(AppConfig):
    name = 'cordobaplantavida'
