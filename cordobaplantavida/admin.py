from django.contrib import admin
from .models import ArbolCordobaPlantaVida, Nacido, Progenitor
from .filters import CodigoDePadresListFilter, PadresSinDNIListFilter


''' #FIXME, no se como hacer inlines desde dos objetos de la misma clase
class Progenitor1InLine(admin.StackedInline):
    model = Progenitor
    fk_name = "progenitor_1"
    extra = 0

class Progenitor2InLine(admin.StackedInline):
    model = Progenitor
    fk_name = "progenitor_2"
    extra = 0
'''


class NacidoAdmin(admin.ModelAdmin):
    search_fields = ['dni', 'nombres', 'apellidos']

    def prog1(self, obj):
        return '--' if obj.progenitor1 is None else '[{}] {} {}'.format(
            obj.progenitor1.sexo, obj.progenitor1.nombres, obj.progenitor1.apellidos)

    def prog2(self, obj):
        return '--' if obj.progenitor2 is None else '[{}] {} {}'.format(
            obj.progenitor2.sexo, obj.progenitor2.nombres, obj.progenitor2.apellidos)

    def codigo_conformacion_de_progenitores(self, obj):
        return obj.codigo_conformacion_de_progenitores()

    def usa_primero_apellido(self, obj):
        return obj.usa_primero_apellido()

    list_display = (
        'dni',
        'fecha_nacimiento',
        'oficina_registrante',
        'fecha_registro',
        'nombres',
        'apellidos',
        'usa_primero_apellido',
        'codigo_registro_civil',
        'codigo_conformacion_de_progenitores',
        'prog1',
        'prog2')

    # OJO, va a ser persadísimo si no metemos un select2 o algo así. Será un
    # listado con miles de elementos
    exclude = ['progenitor1', 'progenitor2']
    list_filter = (
        CodigoDePadresListFilter,
        'oficina_registrante',
        PadresSinDNIListFilter)
    # inlines = [Progenitor1InLine, Progenitor2InLine]


class ProgenitorAdmin(admin.ModelAdmin):
    search_fields = ['dni', 'nombres', 'apellidos']
    list_display = ('dni', 'nombres', 'apellidos', 'barrio', 'sexo', 'email',
                    'telefono_fijo', 'telefono_movil')


class ArbolCordobaPlantaVidaAdmin(admin.ModelAdmin):
    def nacido(self, obj):
        return '{} {} DNI:{}'.format(
            obj.nacido.nombres,
            obj.nacido.apellidos,
            obj.nacido.dni)

    def barrios(self, obj):
        barrios = []
        if obj.nacido.progenitor1 is not None and obj.nacido.progenitor1.barrio:
            barrios.append(obj.nacido.progenitor1.barrio)
        if obj.nacido.progenitor2 is not None and obj.nacido.progenitor2.barrio:
            barrios.append(obj.nacido.progenitor2.barrio)
        return ' ó '.join(barrios)

    def estadod(self, obj):
        return obj.get_estado_display()

    def progenitor1(self, obj):
        p = ''
        if obj.nacido.progenitor1 is not None:
            p = '{} {} DNI:{}'.format(obj.nacido.progenitor1.nombres,
                                      obj.nacido.progenitor1.apellidos,
                                      obj.nacido.progenitor1.dni)
            if obj.nacido.progenitor1.telefono_fijo or obj.nacido.progenitor1.telefono_movil:
                p += ' Tels:{} {}'.format(
                    obj.nacido.progenitor1.telefono_fijo,
                    obj.nacido.progenitor1.telefono_movil)
            if obj.nacido.progenitor1.email:
                p += ' Mail:{}'.format(obj.nacido.progenitor1.email)
        return p

    def progenitor2(self, obj):
        p = ''
        if obj.nacido.progenitor2 is not None:
            p = '{} {} DNI:{}'.format(obj.nacido.progenitor2.nombres,
                                      obj.nacido.progenitor2.apellidos,
                                      obj.nacido.progenitor2.dni)
            if obj.nacido.progenitor2.telefono_fijo or obj.nacido.progenitor2.telefono_movil:
                p += ' Tels:{} {}'.format(
                    obj.nacido.progenitor2.telefono_fijo,
                    obj.nacido.progenitor2.telefono_movil)
            if obj.nacido.progenitor2.email:
                p += ' Mail:{}'.format(obj.nacido.progenitor2.email)
        return p

    def fecha_registro_civil(self, obj):
        return obj.nacido.fecha_registro

    list_display = (
        'nacido',
        'estadod',
        'barrios',
        'progenitor1',
        'progenitor2',
        'fecha_registro_civil',
        'fecha_notificacion',
        'fecha_semilla_entregada',
        'fecha_plantado_por_municipio',
        'especie_entregada')
    search_fields = [
        'nacido__dni',
        'nacido__nombres',
        'nacido__apellidos',
        'nacido__progenitor1__barrio',
        'nacido__progenitor2__barrio']
    # van a ser miles de nacidos pronto
    exclude = ['nacido', 'plan_forestal', 'fecha_notificacion',
               'fecha_semilla_entregada', 'fecha_plantado_por_municipio']
    list_filter = ['estado']
    ordering = ['-nacido__fecha_registro']


admin.site.register(Nacido, NacidoAdmin)
admin.site.register(Progenitor, ProgenitorAdmin)
admin.site.register(ArbolCordobaPlantaVida, ArbolCordobaPlantaVidaAdmin)
