# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-09-17 17:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cordobaplantavida', '0006_auto_20180913_1922'),
    ]

    operations = [
        migrations.AddField(
            model_name='nacido',
            name='ultimos_datos_crudos_importados',
            field=models.TextField(blank=True, null=True),
        ),
    ]
