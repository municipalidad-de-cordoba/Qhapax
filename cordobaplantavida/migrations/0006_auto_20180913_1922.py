# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-09-13 22:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cordobaplantavida', '0005_auto_20170630_1005'),
    ]

    operations = [
        migrations.CreateModel(
            name='OficinaRegistranteNacidos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=90)),
            ],
        ),
        migrations.AddField(
            model_name='nacido',
            name='oficina_registrante',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cordobaplantavida.OficinaRegistranteNacidos'),
        ),
    ]
