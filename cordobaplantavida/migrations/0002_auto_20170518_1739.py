# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-05-18 20:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cordobaplantavida', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='progenitor',
            name='barrio',
            field=models.CharField(blank=True, max_length=90, null=True),
        ),
        migrations.AlterField(
            model_name='progenitor',
            name='telefono_fijo',
            field=models.CharField(blank=True, max_length=90, null=True),
        ),
        migrations.AlterField(
            model_name='progenitor',
            name='telefono_movil',
            field=models.CharField(blank=True, max_length=90, null=True),
        ),
    ]
