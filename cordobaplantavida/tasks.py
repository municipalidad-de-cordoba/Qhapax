from __future__ import absolute_import, unicode_literals
from qhapax.celeryconf import app
import datetime
import requests
import json
from cordobaplantavida.models import Nacido, Progenitor
from cordobaplantavida import settings as settings_rc


@app.task
def importar_nacimientos(desde=None, hasta=None):
    """
    Importa datos de nacimiento desde el API del registro civil.
    Similar al comando importar_nacimientos_registro_civil pero esta es una
    función común que retorna un mensaje de éxito o una lista de mensajes de
    errores ocurridos, pensada para que se ejecute automaticamente con celery.
    """
    if desde is not None:
        try:
            fecha_desde = datetime.datetime.strptime(options['fechaDesde'], "%Y-%m-%d")
        except:
            r = 'La fecha DESDE {} no es válida. Use Y-m-d'.format(desde)
            return r
    else:
        fecha_desde = datetime.date.today() - datetime.timedelta(days=7)

    if hasta is not None:
        try:
            fecha_hasta = datetime.datetime.strptime(options['fechaHasta'], "%Y-%m-%d")
        except:
            r = 'La fecha HASTA {} NO es válida. Use Y-m-d'.format(hasta)
            return r
    else:
        fecha_hasta = datetime.date.today()

    desde = fecha_desde.strftime("%d/%m/%Y")
    hasta = fecha_hasta.strftime("%d/%m/%Y")

    # si se piden muchos días debe dividirse en varios pedidos de no mas de x díás
    delta = datetime.timedelta(days=1)
    while fecha_desde <= fecha_hasta:
        url_base = settings_rc.URL_BASE_NACIMIENTOS
        user = settings_rc.USER_WS_REGISTRO_CIVIL
        passw = settings_rc.PASS_WS_REGISTRO_CIVIL
        url = '{}?user={}&password={}&fechaDesde={}&fechaHasta={}&fechaNacimiento=&codTramite='.format(url_base, user, passw, desde, desde)

        fecha_desde += delta
        desde = fecha_desde.strftime("%d/%m/%Y")

        """ EJEMPLO DE RESULTADO
        {
        "TableResult": [{
            "Codigo": 6512313, "OficinaIns": "Cba. Cap./San Vicente",
            "FechaIns_Nacido": "30/05/2017", "FechaNac_Nacido": "01/05/2017",
            "DNI_Nacido": "056064863", "Nombres_Nacido": "mia agostina", "Apellidos_Nacido": "guerrero cortez",
            "DNI_Progenitor1": "040901967", "Nombres_Progenitor1": "MARIA BELEN", "Apellidos_Progenitor1": "CORTEZ",
            "CodSexo_Progenitor1": "F", "Email_Progenitor1": "", "TelefonoFijo_Progenitor1": "", "TelefonoMovil_Progenitor1": "3516000000",
            "Barrio_Progenitor1": "Renacimiento", "DNI_Progenitor2": "040963646", "Nombres_Progenitor2": "MARIO GASTON",
            "Apellidos_Progenitor2": "GUERRERO", "CodSexo_Progenitor2": "M"
        }, {
            "Codigo": 6513968, "OficinaIns": "Cba. Cap./CPC Colon",
            "FechaIns_Nacido": "30/05/2017", "FechaNac_Nacido": "21/04/2017",
            "DNI_Nacido": "056065887", "Nombres_Nacido": "EMILIA", "Apellidos_Nacido": "NIFFELER",
            "DNI_Progenitor1": "038412612", "Nombres_Progenitor1": "DANIELA MACARENA", "Apellidos_Progenitor1": "PALACIO",
            "CodSexo_Progenitor1": "F", "Email_Progenitor1": "", "TelefonoFijo_Progenitor1": "", "TelefonoMovil_Progenitor1": "3516470778",
            "Barrio_Progenitor1": "ALTO ALBERDI", "DNI_Progenitor2": "035267932", "Nombres_Progenitor2": "FERNANDO LUIS",
            "Apellidos_Progenitor2": "NIFFELER",
            "CodSexo_Progenitor2": "M"
            }]
        }   """

        basedata = requests.get(url).content # es tipo byte 
        basedata = basedata.decode("utf-8") 

        try:
            jsondata = json.loads(basedata)
        except Exception as e:
            r = 'JSON Error: {}'.format(e)

        nacidos = len(jsondata['TableResult'])

        # chequear los datos
        errores = []
        nuevos = 0
        repetidos = 0

        for dato in jsondata['TableResult']:
            codigo_registro_civil = dato['Codigo']

            existe = len(Nacido.objects.filter(codigo_registro_civil=codigo_registro_civil)) > 0

            if not existe: 
                dni = dato['DNI_Nacido']
                nombres = dato['Nombres_Nacido']
                apellidos = dato['Apellidos_Nacido']

                nacido = Nacido(codigo_registro_civil=codigo_registro_civil, dni=dni,
                                    nombres=nombres, apellidos=apellidos)

                fecha_nacimiento = dato['FechaNac_Nacido']
                fecha_nacimiento = datetime.datetime.strptime(fecha_nacimiento, "%d/%m/%Y").date()

                fecha_registro = dato['FechaIns_Nacido']
                fecha_registro = datetime.datetime.strptime(fecha_registro, "%d/%m/%Y").date()

                nacido.fecha_registro = fecha_registro
                nacido.fecha_nacimiento = fecha_nacimiento
                nacido.save()
                nuevos += 1
            else:
                repetidos += 1
                continue

            dni = dato['DNI_Progenitor1']
            if dni != '':
                progenitor, created = Progenitor.objects.get_or_create(dni=dni)

                progenitor.nombres = dato['Nombres_Progenitor1']
                progenitor.apellidos = dato['Apellidos_Progenitor1']
                progenitor.sexo = dato['CodSexo_Progenitor1']

                progenitor.email = '' if 'Email_Progenitor1' not in dato.keys() else dato['Email_Progenitor1']
                progenitor.telefono_fijo = '' if 'TelefonoFijo_Progenitor1' not in dato.keys() else dato['TelefonoFijo_Progenitor1']
                progenitor.telefono_movil = '' if 'TelefonoMovil_Progenitor1' not in dato.keys() else dato['TelefonoMovil_Progenitor1']
                progenitor.barrio = '' if 'Barrio_Progenitor1' not in dato.keys() else dato['Barrio_Progenitor1']
                progenitor.save()

                nacido.progenitor1 = progenitor
                nacido.save()

            dni = dato['DNI_Progenitor2']
            if dni != '':
                progenitor2, created = Progenitor.objects.get_or_create(dni=dni)
                progenitor2.nombres = dato['Nombres_Progenitor2']
                progenitor2.apellidos = dato['Apellidos_Progenitor2']
                progenitor2.sexo = dato['CodSexo_Progenitor2']

                progenitor2.email = '' if 'Email_Progenitor2' not in dato.keys() else dato['Email_Progenitor2']
                progenitor2.telefono_fijo = '' if 'TelefonoFijo_Progenitor2' not in dato.keys() else dato['TelefonoFijo_Progenitor2']
                progenitor2.telefono_movil = '' if 'TelefonoMovil_Progenitor2' not in dato.keys() else dato['TelefonoMovil_Progenitor2']
                progenitor2.barrio = '' if 'Barrio_Progenitor2' not in dato.keys() else dato['Barrio_Progenitor2']
                progenitor2.save()

                nacido.progenitor2 = progenitor2
                nacido.save()

        if len(errores) > 0:
            r = errores
        else:
            r = "Ok. Fin de la importación"
        return r
