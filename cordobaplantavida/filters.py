from django.contrib import admin
from django.db.models import Q


class PadresSinDNIListFilter(admin.SimpleListFilter):
    title = 'Algún Padre sin DNI'

    parameter_name = 'padres_sin_dni'

    def lookups(self, request, model_admin):
        return ((1, 'Alguno de los padres sin DNI'),
                (2, 'Todos'),
                )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """

        if self.value() == 1:
            q = queryset.filter(
                Q(progenitor1__dni='') | Q(progenitor2__dni='')
            )
        else:
            q = queryset
        return q


class CodigoDePadresListFilter(admin.SimpleListFilter):
    title = 'Codigo de padres'

    parameter_name = 'codigo_de_padres'

    def lookups(self, request, model_admin):
        return (('FF', 'Dos progenitores Femeninas'),
                ('MM', 'Dos progenitores Masculinos'),
                ('M', 'Un solo progenitor Masculino'),
                ('F', 'Una sola progenitora Femenina'),
                ('FM', 'Dos progenitores Masculino y Femenino'),
                ('Sin-datos', 'No hay datos de progenitores')
                )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """

        if self.value() == 'FF':
            q = queryset.filter(progenitor1__sexo='F', progenitor2__sexo='F')
            # ids = [obj.id for obj in queryset if obj.codigo_conformacion_de_progenitores() == 'FF']
            # q = queryset.filter(id__in=ids)
        elif self.value() == 'FM':
            q = queryset.filter(
                (Q(progenitor1__sexo='F') & Q(progenitor2__sexo='M')) |
                (Q(progenitor1__sexo='M') & Q(progenitor2__sexo='F'))
            )
            # ids = [obj.id for obj in queryset if obj.codigo_conformacion_de_progenitores() == 'FM']
            # q = queryset.filter(id__in=ids)
        elif self.value() == 'MM':
            q = queryset.filter(progenitor1__sexo='M', progenitor2__sexo='M')
            # ids = [obj.id for obj in queryset if obj.codigo_conformacion_de_progenitores() == 'MM']
            # q = queryset.filter(id__in=ids)
        elif self.value() == 'F':
            q = queryset.filter(
                (Q(progenitor1__sexo='F') & Q(progenitor2__isnull=True)) |
                (Q(progenitor2__sexo='F') & Q(progenitor1__isnull=True))
            )
            # ids = [obj.id for obj in queryset if obj.codigo_conformacion_de_progenitores() == 'F']
            # q = queryset.filter(id__in=ids)
        elif self.value() == 'M':
            q = queryset.filter(
                (Q(progenitor1__sexo='M') & Q(progenitor2__isnull=True)) |
                (Q(progenitor2__sexo='M') & Q(progenitor1__isnull=True))
            )
            # ids = [obj.id for obj in queryset if obj.codigo_conformacion_de_progenitores() == 'M']
            # q = queryset.filter(id__in=ids)
        elif self.value() == 'Sin-datos':
            q = queryset.filter(
                progenitor1__isnull=True,
                progenitor2__isnull=True)
        else:
            q = queryset
        return q
