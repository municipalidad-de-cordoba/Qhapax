#!/usr/bin/python
"""
Importar nacimientos del web service del registro civil
"""
from django.core.management.base import BaseCommand
from cordobaplantavida.models import (Nacido, Progenitor,
                                      OficinaRegistranteNacidos)
from cordobaplantavida import settings as settings_rc
# from django.db import transaction
import sys
import datetime
import requests
import json
import uuid


class Command(BaseCommand):
    help = """Comando para Importar nacimientos del web service del registro civil """

    def add_arguments(self, parser):
        parser.add_argument(
            '--fechaDesde',
            nargs='?',
            type=str,
            help='Fecha de registro en el civil (Y-m-d) para traer los nacimientos Desde')
        parser.add_argument(
            '--fechaHasta',
            nargs='?',
            type=str,
            help='Fecha de registro en el civil (Y-m-d) para traer los nacimientos Hasta')
        parser.add_argument(
            '--debug',
            nargs='?',
            type=bool,
            default=False,
            help='Mostrar datos recibidos de servidor')
        parser.add_argument(
            '--force_update',
            nargs='?',
            type=bool,
            default=False,
            help='Mostrar datos recibidos de servidor')
        parser.add_argument(
            '--dias_juntos',
            nargs='?',
            type=int,
            default=1,
            help='Mostrar datos recibidos de servidor')

    # @transaction.atomic
    def handle(self, *args, **options):

        if options['fechaDesde']:
            try:
                fecha_desde = datetime.datetime.strptime(
                    options['fechaDesde'], "%Y-%m-%d")
            except BaseException:
                self.stdout.write(self.style.ERROR(
                    'La fecha DESDE {} NO es válida. Use Y-m-d'.format(options['fechaDesde'])))
                sys.exit(1)
        else:
            fecha_desde = datetime.date.today() - datetime.timedelta(days=7)

        if options['fechaHasta']:
            try:
                fecha_hasta = datetime.datetime.strptime(
                    options['fechaHasta'], "%Y-%m-%d")
            except BaseException:
                self.stdout.write(self.style.ERROR(
                    'La fecha HASTA {} NO es válida. Use Y-m-d'.format(options['fechaHasta'])))
                sys.exit(1)
        else:
            fecha_hasta = datetime.date.today()

        self.stdout.write(self.style.SUCCESS(
            'Buscando datos desde {} - hasta {}'.format(fecha_desde, fecha_hasta)))

        desde = fecha_desde.strftime("%d/%m/%Y")
        hasta = fecha_hasta.strftime("%d/%m/%Y")

        # si se piden muchos días debe dividirse en varios pedidos de no mas de
        # x díás
        debug = options['debug']
        force_update = options['force_update']

        dias_juntos = options['dias_juntos']
        delta = datetime.timedelta(days=dias_juntos)

        while fecha_desde <= fecha_hasta:
            url_base = settings_rc.URL_BASE_NACIMIENTOS
            user = settings_rc.USER_WS_REGISTRO_CIVIL
            passw = settings_rc.PASS_WS_REGISTRO_CIVIL
            este_hasta = fecha_desde + delta - datetime.timedelta(days=1)
            if este_hasta > fecha_hasta:
                este_hasta = fecha_hasta
            hasta = este_hasta.strftime("%d/%m/%Y")
            url = '{}?user={}&password={}&fechaDesde={}&fechaHasta={}&fechaNacimiento=&codTramite='.format(
                url_base, user, passw, desde, hasta)

            fecha_desde += delta
            desde = fecha_desde.strftime("%d/%m/%Y")

            """ EJEMPLO DE RESULTADO
            {
            "TableResult": [{
                "Codigo": 6512313, "OficinaIns": "Cba. Cap./San Vicente",
                NUEVO sept 2018: "OficinaIns":"Cba. Cap./CPC Mercado Norte"
                "FechaIns_Nacido": "30/05/2017",
                "FechaNac_Nacido": "01/05/2017",
                "DNI_Nacido": "056064863", "Nombres_Nacido": "mia agostina",
                "Apellidos_Nacido": "guerrero cortez",
                "DNI_Progenitor1": "040901967",
                "Nombres_Progenitor1": "MARIA BELEN",
                "Apellidos_Progenitor1": "CORTEZ",
                "CodSexo_Progenitor1": "F", "Email_Progenitor1": "",
                "TelefonoFijo_Progenitor1": "",
                "TelefonoMovil_Progenitor1": "3516000000",
                "Barrio_Progenitor1": "Renacimiento",
                "DNI_Progenitor2": "040963646",
                "Nombres_Progenitor2": "MARIO GASTON",
                "Apellidos_Progenitor2": "GUERRERO", "CodSexo_Progenitor2": "M"
            }, {
                "Codigo": 6513968, "OficinaIns": "Cba. Cap./CPC Colon",
                "FechaIns_Nacido": "30/05/2017", "FechaNac_Nacido": "21/04/2017",
                "DNI_Nacido": "056065887", "Nombres_Nacido": "EMILIA",
                "Apellidos_Nacido": "NIFFELER", "DNI_Progenitor1": "038412612",
                "Nombres_Progenitor1": "DANIELA MACARENA",
                "Apellidos_Progenitor1": "PALACIO", "CodSexo_Progenitor1": "F",
                "Email_Progenitor1": "", "TelefonoFijo_Progenitor1": "",
                "TelefonoMovil_Progenitor1": "3516470778",
                "Barrio_Progenitor1": "ALTO ALBERDI",
                "DNI_Progenitor2": "035267932",
                "Nombres_Progenitor2": "FERNANDO LUIS",
                "Apellidos_Progenitor2": "NIFFELER",
                "CodSexo_Progenitor2": "M"
                }]
            }   """

            self.stdout.write(
                self.style.SUCCESS(
                    'Buscando datos en {}'.format(url)))

            basedata = requests.get(url).content  # es tipo byte
            basedata = basedata.decode("utf-8")

            if debug:
                self.stdout.write(
                    self.style.ERROR(
                        'DEBUG: {}'.format(basedata)))
            try:
                jsondata = json.loads(basedata)
            except Exception as e:
                self.stdout.write(
                    self.style.ERROR(
                        'JSON Error: {}'.format(basedata)))
                sys.exit(1)

            nacidos = len(jsondata['TableResult'])
            self.stdout.write(
                self.style.SUCCESS(
                    'Se obtuvieron {} datos'.format(nacidos)))

            # chequear los datos
            errores = []
            nuevos = 0
            repetidos = 0
            repetidos_actualizados = 0
            repetidos_ignorados = 0

            for dato in jsondata['TableResult']:
                codigo_registro_civil = dato['Codigo']

                nacidos = Nacido.objects.filter(
                    codigo_registro_civil=codigo_registro_civil)
                existe = len(nacidos) > 0

                dni = dato['DNI_Nacido']
                nombres = dato['Nombres_Nacido']
                apellidos = dato['Apellidos_Nacido']

                if not existe:

                    nacido = Nacido(
                        codigo_registro_civil=codigo_registro_civil,
                        dni=dni,
                        nombres=nombres,
                        apellidos=apellidos)
                    nuevos += 1
                else:
                    repetidos += 1
                    if not force_update:
                        repetidos_ignorados += 1
                        continue
                    else:
                        nacido = nacidos[0]
                        repetidos_actualizados += 1

                fecha_nacimiento = dato['FechaNac_Nacido']
                fecha_nacimiento = datetime.datetime.strptime(
                    fecha_nacimiento, "%d/%m/%Y").date()

                fecha_registro = dato['FechaIns_Nacido']
                fecha_registro = datetime.datetime.strptime(
                    fecha_registro, "%d/%m/%Y").date()

                nacido.fecha_registro = fecha_registro
                nacido.fecha_nacimiento = fecha_nacimiento

                oficina_str = dato.get('OficinaIns', '').strip().upper()
                if oficina_str != '':
                    oficina, created = OficinaRegistranteNacidos.objects.get_or_create(
                        nombre=oficina_str)
                    nacido.oficina_registrante = oficina

                dct = json.dumps(dato, sort_keys=True, indent=4)
                if debug:
                    self.stdout.write(
                        self.style.SUCCESS(
                            'DEBUG DICT DATO: {}'.format(dct)))
                nacido.ultimos_datos_crudos_importados = dct

                nacido.save()

                dni1 = dato['DNI_Progenitor1']
                if dni1 == '':
                    dni1 = 'SIN DNI {}'.format(uuid.uuid4())

                if dato['Nombres_Progenitor1'] != '':
                    progenitor, created = Progenitor.objects.get_or_create(
                        dni=dni1)

                    progenitor.nombres = dato['Nombres_Progenitor1']
                    progenitor.apellidos = dato['Apellidos_Progenitor1']
                    progenitor.sexo = dato['CodSexo_Progenitor1'].upper(
                    ).strip()

                    progenitor.email = '' if 'Email_Progenitor1' not in dato.keys(
                    ) else dato['Email_Progenitor1']
                    progenitor.telefono_fijo = '' if 'TelefonoFijo_Progenitor1' not in dato.keys(
                    ) else dato['TelefonoFijo_Progenitor1']
                    progenitor.telefono_movil = '' if 'TelefonoMovil_Progenitor1' not in dato.keys(
                    ) else dato['TelefonoMovil_Progenitor1']
                    progenitor.barrio = '' if 'Barrio_Progenitor1' not in dato.keys(
                    ) else dato['Barrio_Progenitor1']
                    progenitor.save()

                    nacido.progenitor1 = progenitor
                else:
                    nacido.progenitor1 = None

                nacido.save()

                # no tengo forma clara de saber cuando falta un progenitor

                # dejo de venir en algunos casos!
                dni2 = dato.get('DNI_Progenitor2', '')
                nombre2 = dato.get('Nombres_Progenitor2', '')
                hay_progenitor_2 = False

                if nombre2 != '':
                    hay_progenitor_2 = True  # casos raros de gente sin DNI.
                    #FIXME documentar
                if dni1 == dni2:
                    hay_progenitor_2 = False

                if not hay_progenitor_2:
                    nacido.progenitor2 = None
                else:
                    if dni2 == '':
                        dni2 = 'SIN DNI {}'.format(uuid.uuid4())

                    progenitor2, created = Progenitor.objects.get_or_create(
                        dni=dni2)
                    progenitor2.nombres = dato['Nombres_Progenitor2']
                    progenitor2.apellidos = dato['Apellidos_Progenitor2']
                    progenitor2.sexo = dato['CodSexo_Progenitor2'].upper(
                    ).strip()

                    progenitor2.email = '' if 'Email_Progenitor2' not in dato.keys(
                    ) else dato['Email_Progenitor2']
                    progenitor2.telefono_fijo = '' if 'TelefonoFijo_Progenitor2' not in dato.keys(
                    ) else dato['TelefonoFijo_Progenitor2']
                    progenitor2.telefono_movil = '' if 'TelefonoMovil_Progenitor2' not in dato.keys(
                    ) else dato['TelefonoMovil_Progenitor2']
                    progenitor2.barrio = '' if 'Barrio_Progenitor2' not in dato.keys(
                    ) else dato['Barrio_Progenitor2']
                    progenitor2.save()

                    nacido.progenitor2 = progenitor2
                nacido.save()

            if len(errores) > 0:
                self.stdout.write(
                    self.style.ERROR(
                        'ERRORES AL PROCESAR LA LINEA {}'.format(count)))
                for e in errores:
                    self.stdout.write(self.style.ERROR(e))
                sys.exit(1)

            self.stdout.write(
                self.style.SUCCESS(
                    'Archivo cargado con éxito, se cargaron {} registros nuevos ({} repetidos: {} actualizados, {} ignorados)'.format(
                        nuevos,
                        repetidos,
                        repetidos_actualizados,
                        repetidos_ignorados)))
