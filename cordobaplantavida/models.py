from django.db import models
import uuid
from cordobaplantavida import settings as settings_cpv
from plandeforestacion.models import PlanForestal, EspecieArbol
from django.utils import timezone

# -----------------------------------------------------------
# ------------ REGISTRO CIVIL -------------------------------
# -----------------------------------------------------------


class OficinaRegistranteNacidos(models.Model):
    ''' oficinas del registro civil '''
    nombre = models.CharField(max_length=90)

    def __str__(self):
        return self.nombre


class Progenitor(models.Model):
    '''
    Cada uno de los padres que anotan a sus hijos
    (a veces es solo uno por nacimiento)
    '''
    dni = models.CharField(max_length=60)
    nombres = models.CharField(max_length=90)
    apellidos = models.CharField(max_length=90)
    # CodSexo_Progenitor1 parece que es F o M nada mas, no lo se
    sexo = models.CharField(max_length=5)
    email = models.CharField(max_length=90)
    telefono_fijo = models.CharField(max_length=90, null=True, blank=True)
    telefono_movil = models.CharField(max_length=90, null=True, blank=True)
    barrio = models.CharField(max_length=90, null=True, blank=True)
    uuid = models.UUIDField(
        default=uuid.uuid4,
        editable=False)  # codigo para anonimizar

    def __str__(self):
        return '{} {} DNI:{}'.format(self.nombres, self.apellidos, self.dni)


class Nacido(models.Model):
    '''Cada uno de los niños nacidos y registrados por nuestro registro civil'''
    codigo_registro_civil = models.CharField(
        max_length=20, unique=True)  # id en el sistema del registre civil
    fecha_nacimiento = models.DateField(
        null=True, blank=True)  # fecha en que realmente nació
    # fecha en la que lo anoto en el registro civil
    fecha_registro = models.DateField(null=True, blank=True)
    dni = models.CharField(max_length=20)
    nombres = models.CharField(max_length=90)
    apellidos = models.CharField(max_length=90)
    progenitor1 = models.ForeignKey(
        Progenitor,
        null=True,
        on_delete=models.SET_NULL,
        related_name='progenitor_1')
    progenitor2 = models.ForeignKey(
        Progenitor,
        null=True,
        on_delete=models.SET_NULL,
        related_name='progenitor_2')
    uuid = models.UUIDField(
        default=uuid.uuid4,
        editable=False)  # codigo para anonimizar
    oficina_registrante = models.ForeignKey(
        OficinaRegistranteNacidos, null=True, blank=True)

    ultimos_datos_crudos_importados = models.TextField(null=True, blank=True)

    def __str__(self):
        return '{} {} DNI:{}'.format(self.nombres, self.apellidos, self.dni)

    def papa_mama(self):
        if self.codigo_conformacion_de_progenitores() != 'FM':
            return None, None

        if self.progenitor1.sexo == 'M':
            papa = self.progenitor1
            mama = self.progenitor2
        else:
            papa = self.progenitor2
            mama = self.progenitor1

        return papa, mama

    def usa_primero_apellido(self):
        ''' ver que apellidos usa primero '''
        # solo los casos tradicionales de papa + mama
        if self.codigo_conformacion_de_progenitores() != 'FM':
            return 'No aplica'

        apellidos_hije = self.apellidos.upper()

        papa, mama = self.papa_mama()

        apellidos_papa = papa.apellidos.upper().split()
        apellidos_mama = mama.apellidos.upper().split()

        papa_antes = 99
        for a in apellidos_papa:
            fnd = apellidos_hije.find(a)
            if fnd > -1:
                if fnd < papa_antes:
                    papa_antes = fnd  # mejor posicion del apellido del papa

        mama_antes = 99
        for a in apellidos_mama:
            fnd = apellidos_hije.find(a)
            if fnd > -1:
                if fnd < mama_antes:
                    mama_antes = fnd  # mejor posicion del apellido del papa

        # pueden tener el mismo apellido los dos!
        # pueden tener apellidos que empiecen con las mismas letras #FIXME
        # (podrían dar igual)
        if papa_antes < mama_antes:
            return 'PAPA'

        if mama_antes < papa_antes:
            return 'MAMA'

        return 'No identificado'  # {} {}'.format(papa_antes, mama_antes)

    def codigo_conformacion_de_progenitores(self):
        '''
        codificación rapida de la conformacion del o los padres que hacen
        la inscripcion
        '''
        padres = []
        p1 = self.progenitor1
        p2 = self.progenitor2
        if p1 is not None:
            padres.append(p1.sexo.upper().strip())
        if p2 is not None:
            padres.append(p2.sexo.upper().strip())

        # ordeno la lista final para que no existan grupos distintos MF y FM
        # (masculino y femenino)
        cod_padres = ''.join(sorted(padres))
        if cod_padres == '':
            cod_padres = 'Sin-datos'

        return cod_padres

    def save(self, *args, **kwargs):
        # si es un nuevo objeto DEBO mandarle un email de notificación (DESPUES
        # de grabarlo)
        nuevo = self.pk is None

        super(Nacido, self).save(*args, **kwargs)

        if nuevo:
            # crear el registro en Córdoba planta vida
            plan_forestal = PlanForestal.objects.get(
                pk=settings_cpv.ID_PLAN_FORESTAL_ACTIVO)
            cpv = ArbolCordobaPlantaVida(
                nacido=self, plan_forestal=plan_forestal)
            # desde el 4/7 se entregan notificaciones a los padres en el
            # registro civil
            cpv.estado = ArbolCordobaPlantaVida.NOTIFICACION_RECIBIDA
            cpv.save()
            # Mandar el email si hay y marcar como notificado


# -----------------------------------------------------------
# ------------ FIN REGISTRO CIVIL ---------------------------
# -----------------------------------------------------------


class ArbolCordobaPlantaVida(models.Model):
    '''Córdoba planta vida es un arbol por cada nacido
    Seguimos el camino de la entrega de cada árbol y registramos los cambios de estado
    Periodicamente se debe revisar los plazos y cambiar el estado desde el sistema
    '''
    _orig_estado = None

    def __init__(self, *args, **kwargs):
        super(ArbolCordobaPlantaVida, self).__init__(*args, **kwargs)
        ''' anotar el estado para cuando lo grabe registrar el cambio '''
        self._orig_estado = self.estado

    nacido = models.ForeignKey(Nacido, on_delete=models.CASCADE)
    # ver que plan forestal esta activo
    plan_forestal = models.ForeignKey(
        PlanForestal,
        on_delete=models.CASCADE,
        default=settings_cpv.ID_PLAN_FORESTAL_ACTIVO)

    PENDIENTE = 10  # el niño nació y tenemos que plantar el árbol
    NOTIFICACION_ENVIADA = 20  # mandamos un email notificando
    # lo llamamos o verificamos la recepción del correo de notificación
    NOTIFICACION_RECIBIDA = 30
    # los padres vinieron a retirar las plantas al vivero o se las llevamos
    SEMILLAS_ENTREGADAS = 40
    # pasaron los 60 días desde que se registró el nacimiento y no vinieron a
    # buscarlo
    VENCIDO_PLAZO_ENTREGA = 50
    ARBOL_PLANTADO_POR_EL_MUNICIPIO = 60  # el municipio plantó el arbol
    CADUCADO = 70 # despues de otros 60 días extras lo caduco para que no moleste en los registros

    estados = (
        (PENDIENTE,
         'Pendiente'),
        (NOTIFICACION_ENVIADA,
         'Notificación Enviada'),
        (NOTIFICACION_RECIBIDA,
         'Notificación recibida por los padres'),
        (SEMILLAS_ENTREGADAS,
         'Arbol entregado a los padres'),
        (VENCIDO_PLAZO_ENTREGA,
         'Vencido el plazo inicial de entrega (AUTOMATICO)'),
        (ARBOL_PLANTADO_POR_EL_MUNICIPIO,
         'El municipio planto el arbol luego de vencido el plazo de entrega'),
        (CADUCADO,
         'Caduco el plazo para que el municipio plante el arbol (AUTOMATICO)'))

    estado = models.PositiveIntegerField(choices=estados, default=PENDIENTE)
    observaciones_internas = models.TextField(null=True, blank=True)

    # ver que vereda y barrio tiene para que quede definida su especie
    # el operador lo va a ver en la web y entregar en consecuencia
    especie_entregada = models.ForeignKey(
        EspecieArbol,
        on_delete=models.SET_NULL,
        null=True,
        blank=True)

    fecha_notificacion = models.DateTimeField(null=True, blank=True)
    fecha_semilla_entregada = models.DateTimeField(null=True, blank=True)
    fecha_plantado_por_municipio = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '{} {}: {}'.format(
            self.nacido.nombres,
            self.nacido.apellidos,
            self.get_estado_display())

    def save(self, *args, **kwargs):
        now = timezone.now()
        if self._orig_estado != self.estado:
            # cambio!
            if self.estado == self.NOTIFICACION_ENVIADA or (
                    self._orig_estado == self.PENDIENTE and self.estado == self.NOTIFICACION_RECIBIDA):
                # si pasa a enviada o si viniendo de nada pasa a recibida
                # directamente (no incluye que pase de enviada a confirmación
                # de recibida)
                self.fecha_notificacion = now
            elif self.estado == self.SEMILLAS_ENTREGADAS:
                self.fecha_semilla_entregada = now
            elif self.estado == self.ARBOL_PLANTADO_POR_EL_MUNICIPIO:
                self.fecha_plantado_por_municipio = now

        super(ArbolCordobaPlantaVida, self).save(*args, **kwargs)
