import datetime
from haystack import indexes
from .models import PlanDeMetas, Lineamiento, Componente, Objetivo, Meta, IndicadorMeta


class PlanDeMetasIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    titulo = indexes.CharField(model_attr='titulo')
    descripcion = indexes.CharField(model_attr='descripcion', null=True)
    #html = indexes.CharField(model_attr='html')
    imagen = indexes.CharField(model_attr='imagen')

    def get_model(self):
        return PlanDeMetas

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(publicado=True)


class LineamientoIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    titulo = indexes.CharField(model_attr='titulo')
    descripcion = indexes.CharField(model_attr='descripcion', null=True)
    #html = indexes.CharField(model_attr='html')
    imagen = indexes.CharField(model_attr='imagen')

    def get_model(self):
        return Lineamiento

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(publicado=True)


class ComponenteIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    titulo = indexes.CharField(model_attr='titulo')
    descripcion = indexes.CharField(model_attr='descripcion', null=True)
    #html = indexes.CharField(model_attr='html')

    def get_model(self):
        return Componente

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(publicado=True)


class ObjetivoIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    titulo = indexes.CharField(model_attr='titulo')
    #html = indexes.CharField(model_attr='html')

    def get_model(self):
        return Objetivo

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(publicado=True)


class MetaIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    titulo = indexes.CharField(model_attr='titulo')
    #html = indexes.CharField(model_attr='html')
    linea_base = indexes.CharField(model_attr='linea_base')
    estado_de_avance = indexes.CharField(model_attr='estado_de_avance')

    def get_model(self):
        return Meta

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(publicado=True)
