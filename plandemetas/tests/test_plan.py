import pytest
from .factories import PlanDeMetasFactory

@pytest.mark.django_db
def test_redirect(client):
    """se prueba que urls viejas siguen siendo válidas aun cuando cambia el slug"""
    plan = PlanDeMetasFactory()
    original_slug = plan.slug
    original_url = plan.get_absolute_url()

    # edito titulo
    plan.titulo = 'Nuevo {}'.format(plan.titulo)
    plan.save()

    assert plan.slug != original_slug
    assert plan.get_absolute_url() != original_url

    # acceder a la url original redirige a la nueva
    response = client.get(original_url)
    assert response.status_code == 301
    assert response['location'] == plan.get_absolute_url()
