from django.utils.text import slugify
import factory
from factory.django import DjangoModelFactory


class PlanDeMetasFactory(DjangoModelFactory):
    class Meta:
        model = 'plandemetas.PlanDeMetas'

    titulo = factory.Sequence(lambda n: 'Plan de metas {}'.format(n))
    orden_prioridad = factory.Sequence(lambda n: n + 1)