from django import forms
from .models import Meta, IndicadorMeta
from ckeditor.widgets import CKEditorWidget


class MetaAnualUpdateForm(forms.ModelForm):
    '''
    Actualización, solo algunos campos
    '''

    def __init__(self, *args, **kwargs):
        super(MetaAnualUpdateForm, self).__init__(*args, **kwargs)

        self.fields['estado_de_avance'].required = False
        # self.fields['oficinas_relacionadas'].required = False
        self.fields['observaciones_generales'].disabled = True
        self.fields['linea_base'].disabled = True

    class Meta:
        model = Meta
        exclude = ['publicado', 'oficinas_relacionadas', 'orden_prioridad', 'new_titulo', 'new_linea_base']

        widgets = {
            'objetivo': forms.Textarea(attrs={'readonly': True, 'style': 'background-color: gray', 'rows': '4', 'cols': '120'}),
            'titulo': forms.Textarea(attrs={'readonly': True, 'style': 'background-color: gray', 'rows': '4', 'cols': '120'}),
            'oficina_responsable': forms.Select(attrs={'disabled': 'disabled', 'style': 'background-color: gray'}),
            # 'linea_base': CKEditorWidget(attrs={'readonly': True, 'rows': 3, 'style': 'background-color: green'}),
            # 'observaciones_generales': CKEditorWidget(attrs={'readonly': True, 'style': 'background-color: green'}),
            'estado_de_avance': forms.Select(attrs={'disabled': 'disabled', 'style': 'background-color: gray'}),
            # 'orden_prioridad': forms.TextInput(attrs={'readonly': True, 'style': 'background-color: gray'}),
            # 'publicado': forms.TextInput(attrs={'readonly': True, 'style': 'background-color: gray'}),
            'oficinas_relacionadas': forms.Select(attrs={'disabled': 'disabled', 'style': 'background-color: gray'}),
        }


class IndicadorMetaAnualUpdateForm(forms.ModelForm):
    '''
    Actualización, solo algunos campos
    '''

    class Meta:
        model = IndicadorMeta
        fields = ('indicador', 'valor_indicador', 'new_valor_indicador')

        widgets = {
            'indicador': forms.Textarea(attrs={'readonly': True, 'style': 'background-color: gray', 'rows': '4', 'cols': '120'}),
            'valor_indicador': forms.Textarea(attrs={'readonly': True, 'style': 'background-color: gray', 'rows': '4', 'cols': '120'}),
        }
