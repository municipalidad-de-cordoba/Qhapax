from django.db import models
from ckeditor.fields import RichTextField
from funcionarios.models import Cargo
from django.utils.text import slugify
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from datetime import date
from .managers import MetasManager



class OdsMetas(models.Model):
    '''
    Objetivos de desarrollo sostenible que se linkean a las metas
    link: https://www.un.org/sustainabledevelopment/es/objetivos-de-desarrollo-sostenible/
    '''

    nombre_ods = models.CharField(max_length=500)
    link_ods = models.CharField(max_length=500)
    imagen_ods = models.ImageField(
        upload_to='imagenes/plan-de-metas/ods', null=True, blank=True)

    def __str__(self):
        return '{}'.format(self.nombre_ods)


class PlanDeMetas(models.Model):
    '''
    datos generales del plan de metas
    '''
    titulo = models.CharField(max_length=90)
    # slug del titulo para la web
    slug = models.SlugField(max_length=90, null=True, blank=True)
    descripcion = models.CharField(max_length=200, null=True, blank=True)
    html = RichTextField(null=True, blank=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    publicado = models.BooleanField(default=False)
    orden_prioridad = models.PositiveIntegerField()
    # para la estética pública
    imagen = models.ImageField(
        upload_to='imagenes/plan-de-metas', null=True, blank=True)
    color_1 = models.CharField(max_length=200, null=True, blank=True)
    color_2 = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.titulo

    def get_absolute_url(self):
        return reverse('website.plandemetas', kwargs={'plan': self.slug})

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        super(PlanDeMetas, self).save(*args, **kwargs)

    @property
    def lineamientos(self):
        '''
        devolver solo los publicados
        '''
        return self.lineamiento_set.filter(publicado=True)

    class Meta:
        verbose_name_plural = "1. Planes de metas"
        ordering = ['orden_prioridad']


class Lineamiento(models.Model):
    '''
    Cada una de las lineas de accion del plan.
    '''
    plan_de_metas = models.ForeignKey(PlanDeMetas, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=90)
    # slug del titulo para la web
    slug = models.SlugField(max_length=90, null=True, blank=True)
    descripcion = models.CharField(max_length=200, null=True, blank=True)
    html = RichTextField(null=True, blank=True)
    publicado = models.BooleanField(default=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    orden_prioridad = models.PositiveIntegerField()

    # para la estética pública
    imagen = models.ImageField(
        upload_to='imagenes/lineamiento', null=True, blank=True)
    color_1 = models.CharField(max_length=200, null=True, blank=True)
    color_2 = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.titulo

    def get_absolute_url(self):
        return reverse('website.plandemetas.lineamiento', kwargs={'plan': self.plan_de_metas.slug, 'linea': self.slug})

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        super(Lineamiento, self).save(*args, **kwargs)

    @property
    def componentes(self):
        '''
        devolver solo los publicados
        '''
        return self.componente_set.filter(publicado=True)

    class Meta:
        unique_together = (("slug", "plan_de_metas"),)
        verbose_name_plural = "2. Lineamientos"
        ordering = ['orden_prioridad']


class Componente(models.Model):
    ''' Cada componente del lineamiento '''
    lineamiento = models.ForeignKey(Lineamiento, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=90)
    # slug del titulo para la web
    slug = models.SlugField(max_length=90, null=True, blank=True)
    descripcion = models.CharField(max_length=200, null=True, blank=True)
    html = RichTextField(null=True, blank=True)
    html_version_resumida_web = RichTextField(null=True, blank=True, help_text='versión resumida para la web')
    # versión especial para la web más compresible.
    html_corto_web = RichTextField(null=True, blank=True)
    publicado = models.BooleanField(default=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    orden_prioridad = models.PositiveIntegerField()

    def __str__(self):
        return '{}: {}'.format(self.titulo, self.lineamiento)

    def get_absolute_url(self):
        return reverse('website.plandemetas.componente', kwargs={'plan': self.lineamiento.plan_de_metas.slug, 'linea': self.lineamiento.slug, 'comp_id': self.id})

    @property
    def objetivos(self):
        '''
        devolver solo los publicados
        '''
        return self.objetivo_set.filter(publicado=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        super(Componente, self).save(*args, **kwargs)

    class Meta:
        unique_together = (("slug", "lineamiento"),)
        verbose_name_plural = "3. Componentes"
        ordering = ['orden_prioridad']


class Objetivo(models.Model):
    '''
    Cada objetivo del componente
    '''
    componente = models.ForeignKey(Componente, on_delete=models.CASCADE)
    titulo = models.TextField()
    html = RichTextField(null=True, blank=True)
    publicado = models.BooleanField(default=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    orden_prioridad = models.PositiveIntegerField()

    def __str__(self):
        return '{}: {}'.format(self.titulo, self.componente)

    def get_absolute_url(self):
        args = {'plan': self.componente.lineamiento.plan_de_metas.slug,
                'linea': self.componente.lineamiento.slug,
                'comp_id': self.componente.id,
                'comp_real_id': self.componente.id,
                'obj_id': self.id}
        url = reverse('website.plandemetas.objetivo', kwargs=args)
        url = url.replace('%23', '#')  #FIXME hay un "#" en la URL (deberiamos revisar eso) y la funcion reverse lo encodea
        return url

    @property
    def metas(self):
        '''
        devolver solo los publicados
        '''
        return self.meta_set.filter(publicado=True)

    class Meta:
        verbose_name_plural = "4. Objetivos"
        ordering = ['titulo']


class Meta(models.Model):
    '''
    Cada meta del objetivo
    '''
    _orig_actualizacion = None

    objetivo = models.ForeignKey(Objetivo, on_delete=models.CASCADE)
    titulo = models.TextField()
    html = RichTextField(null=True, blank=True)
    linea_base = RichTextField(null=True, blank=True)
    actualizacion_anual_aprobada = models.BooleanField(default=False)

    NO_INICIADA = 10
    EN_CURSO = 20
    ALCANZADA = 30
    SUPERADA = 40
    estados = ((NO_INICIADA, 'No iniciado'),
               (EN_CURSO, 'En curso'),
               (ALCANZADA, 'Meta alcanzada'),
               (SUPERADA, 'Meta superada'))
    estado_de_avance = models.PositiveIntegerField(
        choices=estados, default=NO_INICIADA)
    observaciones_generales = RichTextField(null=True, blank=True)
    publicado = models.BooleanField(default=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    oficina_responsable = models.ForeignKey(
        Cargo, null=True, blank=True, related_name='oficina_responsable')
    oficinas_relacionadas = models.ManyToManyField(
        Cargo, blank=True, related_name='oficinas_relacionadas')
    orden_prioridad = models.PositiveIntegerField()

    # datos temporales a la espera de publicación. Asi se permite decidir el momento de publicación
    # los equipos de las metas proponen y los responsables del plan de meta
    # evalúan
    new_linea_base = RichTextField(
        null=True, blank=True, verbose_name="Nueva linea de base propuesta")
    new_estado_de_avance = models.PositiveIntegerField(choices=estados, default=NO_INICIADA,
                                                       verbose_name="Nuevo estado de avance propuesto")
    new_titulo = models.TextField(null=True, blank=True,
                                  verbose_name="Nuevo titulo de la meta propuesto",
                                  help_text="No debería cambiarse salvo casos excepcionales")
    new_observaciones_generales = RichTextField(null=True, blank=True,
                                                verbose_name="Nuevas observaciones a agregar",
                                                help_text="Agregar solo las novedades de esta evaluación")

    fecha_actualizacion_oficial = models.DateField(default=date(
        2019, 1, 1), help_text='Fecha de actualizacion oficial informada')

    ods = models.ManyToManyField(OdsMetas, null=True, blank=True, help_text="Objetivo de desarrollo sostenible que corresponde a la meta")

    objects = MetasManager()

    def __init__(self, *args, **kwargs):
        super(Meta, self).__init__(*args, **kwargs)
        # Anoto estado original para despues poder ver si cambia
        self._orig_actualizacion = self.actualizacion_anual_aprobada

    def __str__(self):
        return '{}: {}'.format(self.titulo, self.objetivo)

    def estado_de_avance_estimado(self):
        ''' en la época de actualizaciones muchas veces no se carga el new_
            entonces necesitamos estimar cual sería el nuevo.
            Sólo cuando new_estado_de_avance > estado_de_avance lo considero el _nuevo_
            '''

        return max(self.new_estado_de_avance, self.estado_de_avance)

    def estado_de_avance_estimado_mydisplay(self):
        return dict(self.estados)[self.estado_de_avance_estimado()]

    def save(self, *args, **kwargs):
        """
        Se sobreescribe el save() para que cuando la meta este aprobada se
        actualice fecha_actualizacion_oficial
        """
        # Me fijo que las fechas sean distintas y ademas que
        # actualizacion_anual_aprobada este en True
        if (self._orig_actualizacion != self.actualizacion_anual_aprobada) and self.actualizacion_anual_aprobada:
            self.fecha_actualizacion_oficial = date.today()

        super(Meta, self).save(*args, **kwargs)

    def get_absolute_url(self):
        args = {'plan': self.objetivo.componente.lineamiento.plan_de_metas.slug,
                'linea': self.objetivo.componente.lineamiento.slug,
                'comp_id': self.objetivo.componente.id,
                'comp_real_id': self.objetivo.componente.id,
                'obj_id': self.objetivo.id,
                'meta_id': self.id}
        url = reverse('website.plandemetas.meta', kwargs=args)
        url = url.replace('%23', '#')  #FIXME hay un "#" en la URL (deberiamos revisar eso) y la funcion reverse lo encodea
        return url

    def cambios_pendientes(self):
        ''' indica si se han cargado propuestas de actualizacion en la meta o indicadores (los campos new_) '''
        ret = []
        if self.new_titulo:
            ret.append(
                {'field': 'new_titulo', 'new': self.new_titulo, 'old': self.titulo})
        if self.new_linea_base:
            ret.append({'field': 'new_linea_base',
                        'new': 'REVISAR', 'old': 'REVISAR'})
        if self.new_estado_de_avance and self.new_estado_de_avance != self.estado_de_avance and self.new_estado_de_avance != self.NO_INICIADA:
            ret.append({'field': 'new_estado_de_avance',
                        'new': self.new_estado_de_avance, 'old': self.estado_de_avance})
        if self.new_observaciones_generales:
            ret.append({'field': 'new_observaciones_generales',
                        'new': 'REVISAR', 'old': 'REVISAR'})

        for indicador in self.indicadores:
            if indicador.new_indicador:
                ret.append({'field': 'new_indicador',
                            'new': indicador.new_indicador, 'old': indicador.indicador})
            if indicador.new_valor_indicador:
                ret.append({'field': 'new_valor_indicador',
                            'new': indicador.new_valor_indicador, 'old': indicador.valor_indicador})

        return ret

    @property
    def tiene_cambios_pendientes(self):
        return len(self.cambios_pendientes()) > 0

    @property
    def indicadores(self):
        '''
        devolver solo los publicados
        '''
        return self.indicadormeta_set.filter(publicado=True)

    class Meta:
        verbose_name_plural = "5. Metas"
        ordering = ['orden_prioridad']
        # una vez al año (posiblemente se mejore) los encargados de las metas pueden entrar a actualizar las metas
        # solo deben poder cambiar detalles específicos
        permissions = (
            # una vez al año (posiblemente se mejore) los encargados de las metas pueden entrar a actualizar las metas
            # solo deben poder cambiar detalles específicos
            # Hace algunos cambios básicos permitidos para el
            # update anual
            ('metas_editor_anual',
             'Puede hacer actualización anual de metas'),
        )


class IndicadorMeta(models.Model):
    '''
    Cada uno de los indicadores del avance de la meta
    '''
    meta = models.ForeignKey(Meta, on_delete=models.CASCADE)
    indicador = models.TextField()
    # no todas las metas son cuantificables o se miden de la misma forma
    # se pondrá a mano
    valor_indicador = models.TextField(null=True, blank=True)
    orden_prioridad = models.PositiveIntegerField()
    publicado = models.BooleanField(default=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    # datos temporales a la espera de publicación. Asi se permite decidir el
    # momento de publicación
    new_indicador = models.TextField(null=True, blank=True,
                                     verbose_name="Nuevo texto del indicador propuesto",
                                     help_text="No debería cambiarse salvo casos excepcionales")
    new_valor_indicador = models.TextField(null=True, blank=True,
                                           verbose_name="Nuevo valor del indicador propuesto",
                                           help_text="Es necesario ser preciso en esta evaluación")

    def __str__(self):
        return '{}: {}'.format(self.indicador, self.meta)

    def avance_2018(self):  # avance hasta finales de 2018
        return None if len(self.avanceindicadormeta_set.filter(fecha='2018-12-31')) == 0 else self.avanceindicadormeta_set.filter(fecha='2018-12-31')[0]
    
    def avance_2017(self):  # avance hasta finales de 2017
        return None if len(self.avanceindicadormeta_set.filter(fecha='2017-12-31')) == 0 else self.avanceindicadormeta_set.filter(fecha='2017-12-31')[0]
    
    def avance_2016(self):  # avance hasta finales de 2016
        return None if len(self.avanceindicadormeta_set.filter(fecha='2016-12-31')) == 0 else self.avanceindicadormeta_set.filter(fecha='2016-12-31')[0]
        
    class Meta:
        verbose_name_plural = "Indicadores de metas"
        ordering = ['orden_prioridad']


class AvanceIndicadorMeta(models.Model):
    ''' histórico de cada uno de los avances periódicos de las metas '''
    indicador = models.ForeignKey(IndicadorMeta)
    fecha = models.DateField()
    valor_indicador = models.TextField(null=True, blank=True,
                                           verbose_name="Valor del indicador propuesto para la fecha indicada",
                                           help_text="Es necesario ser preciso en cada evaluación")

    def __str__(self):
        return '{} {}'.format(self.indicador.indicador, self.fecha)

class UserPlanDeMetas(models.Model):
    ''' usuario especial de carga del plan de metas. Sirve para definir a que áreas tiene permisos '''
    user = models.OneToOneField(User, on_delete=models.SET_NULL,
                                blank=True, null=True, verbose_name='Usuario de sistema')
    oficinas_autorizadas_metas = models.ManyToManyField(
        Cargo, blank=True, help_text='Oficinas de las que puede editar metas')

    def __str__(self):
        oficinas = ', '.join(
            [x.nombre for x in self.oficinas_autorizadas_metas.all()])
        return '{} {}'.format(self.user.username, oficinas)

    def metas_asignadas(self, plan):
        return Meta.objects.filter(oficina_responsable__in=self.oficinas_autorizadas_metas.all(),
                                        objetivo__componente__lineamiento__plan_de_metas=plan)

    class Meta:
        verbose_name_plural = "Usuarios del plan de metas"
