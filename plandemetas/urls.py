from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^plan-(?P<plan_de_metas_pk>[0-9]+)-metas.(?P<filetype>csv|xls)$', views.lista_metas, name='metas.lista'),
    url(r'^estado-de-metas-(?P<plan_de_metas_pk>[0-9]+).(?P<filetype>csv|xls)$', views.estado_general_metas, name='metas.estado.general'),
    url(r'^plan-(?P<plan_de_metas_pk>[0-9]+)-metas-priv.(?P<filetype>csv|xls)$', views.lista_metas_con_datos_privados, name='metas.lista'),
    url(r'^usuarios-plan-de-metas.(?P<filetype>csv|xls)$', views.usuarios_plan_de_meta_csv, name='plan.de.metas.usuarios'),
    ]
