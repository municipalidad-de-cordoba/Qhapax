#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from datetime import date
from plandemetas.models import Meta, PlanDeMetas, UserPlanDeMetas
import sys
from django.template import loader, Context, Template
from core.lib.comm import QhapaxMail


class Command(BaseCommand):
    help = """ Avisarle a todos los usuarios que el sistema está listo """

    def add_arguments(self, parser):
        parser.add_argument('--plan_de_metas_id', type=int, help='ID del Plan de Metas a actualizar')
        parser.add_argument('--send_real_mail', type=bool, default=False, help='Mandar email (aunque sea el texto')
        parser.add_argument('--debug', type=bool, default=False, help='Mostrar detalles internos')
        parser.add_argument('--test_to_email', type=str, default='', help='Prueba para mandar solo un email a esta casilla')
        parser.add_argument('--users_in', type=str, default='', help='Manda solo a los usuarios especificados en una lista de IDs separados por comas')


    def handle(self, *args, **options):
        
        email_responsable ='dirdesarrolloestrategico@gmail.com'
        send_real_mail = options['send_real_mail']
        debug = options['debug']
        test_to_email = options['test_to_email']
        plan_de_metas_id = options['plan_de_metas_id']
        plan_de_metas = PlanDeMetas.objects.get(pk=plan_de_metas_id)

        #iterar por los usuarios
        users_in = options['users_in']  # ID DEL USUARIO DEL PLAN, no de auth
        if users_in == '':
            usuarios = UserPlanDeMetas.objects.all()
        else:
            users_ids = users_in.split(',')
            usuarios = UserPlanDeMetas.objects.filter(id__in=users_ids)
            self.stdout.write(self.style.WARNING('Usuarios FILTRADOS: {} -> {}'.format(usuarios.count(), usuarios)))

        emails = 0
        for usuario in usuarios:
            email = usuario.user.email
            if email is not None and email != '':
                emails += 1
            else:
                self.stdout.write(self.style.WARNING('El usuario {} {} {} NO TIENE EMAIL'.format(usuario.user.username, usuario.user.first_name, usuario.user.last_name)))
            metas = usuario.metas_asignadas(plan=plan_de_metas)
        
            template = loader.get_template('plandemetas/email-anual-metas.html')
            context = {'usuario': usuario, 'metas': metas, 'email_responsable': email_responsable}
            ctx = Context(context)
            texto = template.render(context)

            
            if debug:
                self.stdout.write(self.style.WARNING('TEXTO {} \n {}'.format(email, texto)))

            if send_real_mail:
                qm = QhapaxMail()
                subject = "Sistema de actualización anual de metas para {}".format(usuario.user.first_name)
                if test_to_email != '':
                    email = test_to_email
                else:
                    # copiar al responsable
                    email = '{},{}'.format(email, email_responsable)
                qm.send_mail(subject, texto, to=email)
                if test_to_email != '':
                    self.stdout.write(self.style.WARNING('PRUEBA TERMINADA y enviada a {}'.format(email)))
                    sys.exit(1)
                else:
                    self.stdout.write(self.style.WARNING('Email enviado a {}'.format(email)))
        
        self.stdout.write(self.style.SUCCESS('Terminado. {} emails generados'.format(emails)))