#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from datetime import date
from plandemetas.models import Meta, PlanDeMetas, AvanceIndicadorMeta
from django.db import transaction
import sys


class Command(BaseCommand):
    help = """ Actualizar en la web y API el plan de metas que se modificaba en los campos
                temporales
                Para emular la grabación:
                python manage.py actualizar_plan_de_metas --plan_de_metas_id=1 --anio=2018
                Para grabar a la base de datos los cambios:
                python manage.py actualizar_plan_de_metas --plan_de_metas_id=1 --anio=2018 --grabar=True
                
                Aprobar todas las actualizaciones y darle si a todo
                python manage.py actualizar_plan_de_metas --plan_de_metas_id=1 --anio=2018 --grabar=True --incluir_despublicadas=True --aprobar_metas_anuales=True

                MANDAR EL EMAIL DE FILICITACIONES AL TERMINAR!
                python manage.py felicitar_usuarios_plandemetas
                 """

    def add_arguments(self, parser):
        parser.add_argument('--plan_de_metas_id', type=int, help='ID del Plan de Metas a actualizar')
        parser.add_argument('--anio', type=int, help='anio actual')
        parser.add_argument('--grabar', dest='grabar', default=False, help="Grabar y no emular la actualización")
        parser.add_argument('--incluir_despublicadas', default=False, help="Incluir las metas no publicadas")
        parser.add_argument('--aprobar_metas_anuales', default=False, help="Aprobar las metas (quitará acceso a los usuarios y pasara el control al administrador)")


    @transaction.atomic
    def handle(self, *args, **options):
        try:
            plan_de_metas_id = int(options['plan_de_metas_id'])
            grabar = options['grabar']
        except Exception as e:
            self.stdout.write(self.style.ERROR('Error en los parámetros: {}'.format(e)))
            sys.exit(1)

        if grabar:
            self.stdout.write(self.style.WARNING('--- Se grabará la actualización de los campos ---'))

        incluir_despublicadas = options['incluir_despublicadas']
        aprobar_metas_anuales = options['aprobar_metas_anuales']
        anio = options['anio']

        plan_de_metas = PlanDeMetas.objects.get(pk=plan_de_metas_id)
        metas = Meta.objects.filter(objetivo__componente__lineamiento__plan_de_metas=plan_de_metas)

        if incluir_despublicadas == False:
            self.stdout.write(self.style.WARNING('--- SE INCLUYEN DESPUBLICADAS ---'))
            metas.filter(publicado=True)
        
        if aprobar_metas_anuales:
            self.stdout.write(self.style.WARNING('--- SE APRUEBAN TODAS LAS METAS ---'))
            metas.update(actualizacion_anual_aprobada=True)

        cambios = []
        actualizaciones = 0
        self.stdout.write(self.style.WARNING('INICIANDO, se procesaran {} metas'.format(metas.count())))
        for meta in metas:
            # revisar de a uno los campos y si tienen novedades

            if not aprobar_metas_anuales and meta.actualizacion_anual_aprobada:
                self.stdout.write(self.style.ERROR('La meta {} no tiene la actualizacion aprobada. SE CANCELA TODO'.format(meta.titulo)))
                sys.exit(1)

            if meta.new_linea_base is not None and len(meta.new_linea_base) > 0:
                cambio = 'META[{}]: Cambia Línea Base: {} /// POR /// {}'.format(meta.id, meta.linea_base, meta.new_linea_base)
                cambios.append(cambio)
                
                self.stdout.write(self.style.ERROR('CAMBIA LINEA BASE NO PERMITIDA {}'.format(cambio)))
                
                """
                if grabar:
                    meta.linea_base = meta.new_linea_base
                    meta.new_linea_base = ""  # vaciar para que no entre de nuevo
                    meta.save()
                    actualizaciones += 1
                """

            if meta.new_estado_de_avance > meta.estado_de_avance:
                cambio = 'META[{}]: Cambia Estado de avance : {} /// POR /// {}'.format(meta.id, meta.estado_de_avance, meta.new_estado_de_avance)
                cambios.append(cambio)
                self.stdout.write(self.style.SUCCESS(cambio))
                if grabar:
                    meta.estado_de_avance = meta.new_estado_de_avance
                    meta.new_estado_de_avance = Meta.NO_INICIADA  # default para que no entre de nuevo
                    meta.save()
                    actualizaciones += 1
    
            if meta.new_titulo is not None and len(meta.new_titulo) > 0:
                cambio = 'META[{}]: Cambia Titulo: {} /// POR /// {}'.format(meta.id, meta.titulo, meta.new_titulo)
                cambios.append(cambio)
                
                self.stdout.write(self.style.ERROR('CAMBIA TITULO NO PERMITIDA {}'.format(cambio)))
                sys.exit(1)
                
                """
                if grabar:
                    meta.titulo = meta.new_titulo  # vaciar para que no entre de nuevo
                    meta.new_titulo = ""
                    meta.save()
                    actualizaciones += 1
                """

            if meta.new_observaciones_generales is not None and len(meta.new_observaciones_generales) > 0:
                new_obs = meta.new_observaciones_generales
                lin = "\n--------------------\n"
                cambio = '{2}META[{0}]: {2}Cambia Observaciones generales: {2} {1} {2}/// SE AGREGA ///{2} {3}'.format(meta.id, meta.observaciones_generales, lin, new_obs)
                cambios.append(cambio)
                self.stdout.write(self.style.SUCCESS(cambio))
                if grabar:
                    meta.observaciones_generales = "<p>{}</p>{}".format(anio, new_obs)
                    meta.new_observaciones_generales = ""  # vaciar para que no entre de nuevo
                    meta.save()
                    actualizaciones += 1

            for indicador in meta.indicadores:
            
                new_val = indicador.new_valor_indicador
                if new_val is not None and len(new_val) > 0:
                    lin = "\n--------------------\n"
                    cambio = '{2}INDICADOR[{0}]:{2}Cambia Valor Indicador: {1} ==> {3}'.format(indicador.id, indicador.valor_indicador, lin, new_val)
                    cambios.append(cambio)
                    self.stdout.write(self.style.SUCCESS(cambio))
                    if grabar:
                        
                        # Guardo en new_avance los campos new_*
                        new_avance, created = AvanceIndicadorMeta.objects.get_or_create(indicador=indicador,
                                                                            fecha=date.today())
                        new_avance.valor_indicador=new_val
                        new_avance.save()

                        indicador.valor_indicador = new_val
                        indicador.new_valor_indicador = ""  # vaciar para que no entre de nuevo
                        indicador.save()
                        actualizaciones += 1

        self.stdout.write(self.style.SUCCESS("{} CAMBIOS DETECTADOS".format(len(cambios))))
        for c in cambios:
            self.stdout.write(self.style.SUCCESS(c))

        self.stdout.write(self.style.SUCCESS('Archivo cargado con éxito, se actualizaron {} campos'.format(actualizaciones)))