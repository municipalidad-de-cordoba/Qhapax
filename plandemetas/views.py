from django.shortcuts import get_object_or_404
from django.views.decorators.cache import cache_page
from .models import *
from wkhtmltopdf.views import PDFTemplateResponse
import django_excel as excel
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
import django_excel as excel


@cache_page(60 * 60 * 12)  # 12 h
def plan_de_metas_completo_PDF(request, plan_de_metas_id):
    '''
    Construir PDF con el plan de metas completo versión pública
    '''
    template = 'plandemetas/plan-de-metas-completo.html'

    plan = get_object_or_404(PlanDeMetas, pk=plan_de_metas_id)
    context = {'plan': plan}
    response = PDFTemplateResponse(request=request,
                                   template=template,
                                   # fuerza descarga OK filename="plan-{}.pdf".format(plan_de_metas_id),
                                   context=context,
                                   show_content_in_browser=False,
                                   cmd_options={'page-size': 'A4',
                                                   'title': plan.titulo,
                                                   'margin-top': 25,
                                                   'margin-bottom': 15,
                                                   'margin-left': 15,
                                                   'margin-right': 15,
                                                   'footer-line': None,
                                                   'footer-center': '[page]',
                                                   'footer-left': plan.titulo,
                                                   'footer-right': 'Municipalidad de Córdoba'
                                                   })
    return response


@cache_page(60 * 60)  # 1 hora
def lista_metas(request, plan_de_metas_pk, filetype):
    '''
    lista de metas abiertas
    '''
    plan = get_object_or_404(PlanDeMetas, pk=plan_de_metas_pk, publicado=True)
    
    csv_list = []
    csv_list.append(['Indicador', 'estado', 'ID Meta', 'Meta', 'estado meta', 'ID Objetivo', 'Objetivo', 'ID Componente', 'Componente', 'ID Lineamiento', 'Lienamiento'])
    for lineamiento in plan.lineamientos:
        for componente in lineamiento.componentes:
            for objetivo in componente.objetivos:
                for meta in objetivo.metas:
                    for indicador in meta.indicadores:
                        csv_list.append([indicador.indicador, indicador.valor_indicador, meta.id, meta.titulo, meta.get_estado_de_avance_display(),
                                            objetivo.id, objetivo.titulo, componente.id, componente.titulo, lineamiento.id, lineamiento.titulo])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)

@login_required
@cache_page(60 * 60)  # 1 hora
def estado_general_metas(request, plan_de_metas_pk, filetype):
    '''
    lista de metas abiertas
    '''
    plan = get_object_or_404(PlanDeMetas, pk=plan_de_metas_pk, publicado=True)

    csv_list = []
    csv_list.append(['Lineamiento', 'Componente', 'Superadas',
                     'Porcentaje Superadas', 'Alcanzadas',
                     'Porcentaje Alcanzadas', 'En Curso',
                     'Porcentaje En Curso', 'No Iniciada',
                     'Porcentaje No Iniciada'])
    metas = Meta.objects.filter(objetivo__componente__lineamiento__plan_de_metas__id=1).select_related('estado_de_avance')
    superadas = metas.filter(estado_de_avance=Meta.SUPERADA).count()
    alcanzadas = metas.filter(estado_de_avance=Meta.ALCANZADA).count()
    en_curso = metas.filter(estado_de_avance=Meta.EN_CURSO).count()
    no_iniciadas = metas.filter(estado_de_avance=Meta.NO_INICIADA).count()
    csv_list.append(['TODOS',
                     "",
                     superadas,
                     round((superadas*100)/metas.count(), 2),
                     alcanzadas,
                     round((alcanzadas*100)/metas.count(), 2),
                     en_curso,
                     round((en_curso*100)/metas.count(), 2),
                     no_iniciadas,
                     round((no_iniciadas*100)/metas.count(), 2)
                  ])

    for lineamiento in plan.lineamientos:
        # sobrescribo los valores anteriores
        superadas = metas.filter(estado_de_avance=Meta.SUPERADA,
          objetivo__componente__lineamiento=lineamiento).count()
        alcanzadas = metas.filter(estado_de_avance=Meta.ALCANZADA,
          objetivo__componente__lineamiento=lineamiento).count()
        en_curso = metas.filter(estado_de_avance=Meta.EN_CURSO,
          objetivo__componente__lineamiento=lineamiento).count()
        no_iniciadas = metas.filter(estado_de_avance=Meta.NO_INICIADA,
          objetivo__componente__lineamiento=lineamiento).count()
        csv_list.append([lineamiento.titulo,
                         "",
                         superadas,
                         round((superadas*100)/metas.count(), 2),
                         alcanzadas,
                         round((alcanzadas*100)/metas.count(), 2),
                         en_curso,
                         round((en_curso*100)/metas.count(), 2),
                         no_iniciadas,
                         round((no_iniciadas*100)/metas.count(), 2)
                      ])
        for componente in lineamiento.componentes:
            # sobrescribo los valores anteriores
            superadas = metas.filter(estado_de_avance=Meta.SUPERADA,
              objetivo__componente=componente).count()
            alcanzadas = metas.filter(estado_de_avance=Meta.ALCANZADA,
              objetivo__componente=componente).count()
            en_curso = metas.filter(estado_de_avance=Meta.EN_CURSO,
              objetivo__componente=componente).count()
            no_iniciadas = metas.filter(estado_de_avance=Meta.NO_INICIADA,
              objetivo__componente=componente).count()
            csv_list.append(["",
                             componente.titulo,
                             superadas,
                             round((superadas*100)/metas.count(), 2),
                             alcanzadas,
                             round((alcanzadas*100)/metas.count(), 2),
                             en_curso,
                             round((en_curso*100)/metas.count(), 2),
                             no_iniciadas,
                             round((no_iniciadas*100)/metas.count(), 2)
                            ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@login_required  #FIXME hacer un permiso especial 
@cache_page(60 * 60)  # 1 hora
def lista_metas_con_datos_privados(request, plan_de_metas_pk, filetype):
    '''
    lista de metas incluyendo datos privados
    '''
    plan = get_object_or_404(PlanDeMetas, pk=plan_de_metas_pk, publicado=True)
    
    csv_list = []
    csv_list.append(['Indicador', 'estado', 'ID Meta', 'Meta', 'estado meta',
                        'Reparticion responsable meta',
                        'Secretaría', 'Funcionario actual',
                        'ID Objetivo', 'Objetivo', 'ID Componente', 'Componente',
                        'ID Lineamiento', 'Lienamiento'])
    for lineamiento in plan.lineamientos:
        for componente in lineamiento.componentes:
            for objetivo in componente.objetivos:
                for meta in objetivo.metas:
                    reparticion = meta.oficina_responsable
                    if reparticion is None:
                        reparticion_nombre = 'Sin reparticion designada'
                        secretaria_nombre = 'Sin reparticion designada'
                        funcionario_nombre = 'Sin reparticion designada'
                    else:
                        reparticion_nombre = reparticion.oficina
                        secretaria = reparticion.area_gobierno
                        secretaria_nombre = secretaria.oficina
                        if reparticion.en_funcion_activa is None:
                            funcionario_nombre = 'Sin funcionario designado'
                        else:
                            funcionario_nombre = reparticion.en_funcion_activa.funcionario.nombrepublico
                        
                    for indicador in meta.indicadores:
                        csv_list.append([indicador.indicador, indicador.valor_indicador, meta.id,
                                            meta.titulo, meta.get_estado_de_avance_display(),
                                            reparticion_nombre, secretaria_nombre, funcionario_nombre,
                                            objetivo.id, objetivo.titulo, componente.id,
                                            componente.titulo, lineamiento.id, lineamiento.titulo])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 h
@login_required
def test_plan_de_metas_PDF(request, plan_de_metas_id):
    '''
    Construir PDF temporal con el test plan de metas.
    '''
    template = 'plandemetas/test-plan-de-metas.html'

    plan = get_object_or_404(PlanDeMetas, pk=plan_de_metas_id)
    context = {'plan': plan}
    response = PDFTemplateResponse(request=request,
                                   template=template,
                                   # fuerza descarga OK filename="plan-{}.pdf".format(plan_de_metas_id),
                                   context=context,
                                   show_content_in_browser=False,
                                   cmd_options={'page-size': 'A4',
                                                   'title': plan.titulo,
                                                   'margin-top': 25,
                                                   'margin-bottom': 15,
                                                   'margin-left': 15,
                                                   'margin-right': 15,
                                                   'footer-line': None,
                                                   'footer-center': '[page]',
                                                   'footer-left': plan.titulo,
                                                   'footer-right': 'Municipalidad de Córdoba'
                                                   })
    return response

def plan_de_metas_temporal_PDF(request, plan_de_metas_id):
    '''
    Construir PDF temporal con los campos new en vez de los campos viejos.
    '''
    template = 'plandemetas/plan-de-metas-temporal.html'

    plan = get_object_or_404(PlanDeMetas, pk=plan_de_metas_id)
    context = {'plan': plan}

    tamanio_pagina = request.GET.get('hoja', 'A4')
    response = PDFTemplateResponse(request=request,
                                   template=template,
                                   # fuerza descarga OK filename="plan-{}.pdf".format(plan_de_metas_id),
                                   context=context,
                                   show_content_in_browser=False,
                                   cmd_options={'page-size': tamanio_pagina,
                                                   'title': plan.titulo,
                                                   'margin-top': 25,
                                                   'margin-bottom': 15,
                                                   'margin-left': 15,
                                                   'margin-right': 15,
                                                   'footer-line': None,
                                                   'footer-center': '[page]',
                                                   'footer-left': plan.titulo,
                                                   'footer-right': 'Municipalidad de Córdoba'
                                                   })
    return response

@cache_page(60 * 60 * 12)  # 12 h
@login_required
def usuarios_plan_de_meta_csv(request, filetype):
    ''' Lista de usuarios del plan de metas '''

    usuarios = UserPlanDeMetas.objects.all()
    csv_list = []
    csv_list.append(['Username', 'Nombre', 'Apellido', 'Oficina autorizada', 'email'])

    for usuario in usuarios:
        csv_list.append([
            usuario.user.username,
            usuario.user.first_name,
            usuario.user.last_name,
            usuario.oficinas_autorizadas_metas.all()[0],
            usuario.user.email])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
