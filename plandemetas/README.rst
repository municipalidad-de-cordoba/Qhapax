Plan de metas del gobierno
==========================

Las metas del gobierno estan estructuradas segun el siguiente nivel
jerárquico.

| *Lineamientos estratégicos*: Plan mas general de acciones de gobierno.
| Cada lineamiento tiene *Componentes*.
| Cada Componente tiene *Objetivos*.
| Cada objetivo tiene`
Una vez por año los `usuarios encargados <https://github.com/avdata99/Qhapax/blob/develop/plandemetas/models.py#L366>`_. 
de cada meta deben cargar la actualización de sus indicadores.
En la `lista de metas <https://gobiernoabierto.cordoba.gob.ar/admin/plandemetas/meta/>`_. se ve que metas tienen 
*cambios pendientes* de aplicarse (esto quiere decir que ya fue modificada 
Los cambios a realizar son:
 - Cambiar el estado de avanzace de la meta si corresponde en *Nuevo estado de avance propuesto*.
 - Agregar *Observaciones* del proceso anual. No duplicar lo de años anteriores, esto es un agregado solo de novedades de este años.
 - Dentro de cada un de los indicadores de la meta (si corresponde) cambiar el *Nuevo valor del indicador propuesto*.

Una vez que todos los cambios sean propuestos está lista la tarea de los usuarios encargados de las metas.
Luego de esto los administradores deben revisar todas las metas y aprobarlas.

Finalmente el equipo de sistemas debe correr el script *actualizar_plan_de_metas*.

Puede pasarse con el parámetro *--grabar False* primero para verificar.

Notificar a los usuarios. Prueba previa

```
# sin mandar emails
python manage.py notificar_usuarios_plandemetas --plan_de_metas_id 1 --send_real_mail False
# mandar un email
python manage.py notificar_usuarios_plandemetas --plan_de_metas_id 1 --test_to_email andres@data99.com.ar --send_real_mail True
```

Avisar a todos sin prueba
```
python manage.py notificar_usuarios_plandemetas --plan_de_metas_id 1 --send_real_mail True
```

Probar el plan de metas antes de publicacion anual
--------------------------------------------------

En este link podes se ven en rojo los cambios antes de que se apliquen
/test-plan-de-metas/PDF/plan-1.pdf

En este se ve como quedaría presentado sin los resaltados en rojo.​
/plan-de-metas-temporal/PDF/plan-1.pdf

Ambos links tienen cache porque la generación de PDF es costosa, asegurarse de actualizar en caso de querer reflejar cambios del sistema en el PDF.

Modelos
-------

.. automodule:: plandemetas.models
   :members:
