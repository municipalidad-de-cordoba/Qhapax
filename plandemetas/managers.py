from django.db import models


class MetasManager(models.Manager):

	def tienen_cambios_pendientes(self):
		''' metas que tienen cambios pendientes de validar '''
		pendientes_ids = [meta.id for meta in self.all() if meta.tiene_cambios_pendientes]
		return self.filter(id__in=pendientes_ids)
