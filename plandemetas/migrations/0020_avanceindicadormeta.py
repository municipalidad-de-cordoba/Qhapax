# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-02-21 19:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('plandemetas', '0019_auto_20180216_1203'),
    ]

    operations = [
        migrations.CreateModel(
            name='AvanceIndicadorMeta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('valor_indicador', models.TextField(blank=True, help_text='Es necesario ser preciso en cada evaluación', null=True, verbose_name='Valor del indicador propuesto para la fecha indicada')),
                ('indicador', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='plandemetas.IndicadorMeta')),
            ],
        ),
    ]
