# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2017-11-13 23:52
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plandemetas', '0013_auto_20171113_2048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='indicadormeta',
            name='new_indicador',
            field=models.TextField(blank=True, help_text='No debería cambiarse salvo casos excepcionales', null=True, verbose_name='Nuevo texto del indicador propuesto'),
        ),
        migrations.AlterField(
            model_name='indicadormeta',
            name='new_valor_indicador',
            field=models.TextField(blank=True, help_text='Es necesario ser preciso en esta evaluación', null=True, verbose_name='Nuevo valor del indicador propuesto'),
        ),
        migrations.AlterField(
            model_name='meta',
            name='new_observaciones_generales',
            field=ckeditor.fields.RichTextField(blank=True, help_text='Agregar solo las novedades de esta evaluación', null=True, verbose_name='Nuevas observaciones a agregar'),
        ),
        migrations.AlterField(
            model_name='meta',
            name='new_titulo',
            field=models.TextField(blank=True, help_text='No debería cambiarse salvo casos excepcionales', null=True, verbose_name='Nuevo titulo de la meta propuesto'),
        ),
    ]
