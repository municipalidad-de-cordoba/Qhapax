from django.contrib import admin
from .models import *
from .forms import *
from django.http import Http404


class LineamientoInline(admin.StackedInline):
    model = Lineamiento
    exclude = ('slug', )
    extra = 1


class PlanDeMetasAdmin(admin.ModelAdmin):
    # inlines = [LineamientoInline]
    exclude = ('slug', )

    def agregar_lineamiento(self, obj):
        url = '/admin/plandemetas/lineamiento/add/'
        cls = 'class="addlink"'
        trg = 'target="_blank"'
        txt = 'Agregar lineamiento'
        return '<a href="{}?plan_de_metas={}" {} {}>{}</a>'.format(url, obj.id, cls, trg, txt)

    agregar_lineamiento.allow_tags = True
    list_display = ('titulo', 'orden_prioridad', 'publicado', 'agregar_lineamiento')
    search_fields = ['titulo', 'descripcion']


class ComponenteInline(admin.StackedInline):
    model = Componente
    exclude = ('slug', )
    extra = 1


class LineamientoAdmin(admin.ModelAdmin):
    # inlines = [ComponenteInline]
    exclude = ('slug', )

    def agregar_componente(self, obj):
        url = '/admin/plandemetas/componente/add/'
        cls = 'class="addlink"'
        trg = 'target="_blank"'
        txt = 'Agregar componente'
        return '<a href="{}?lineamiento={}" {} {}>{}</a>'.format(url, obj.id, cls, trg, txt)

    agregar_componente.allow_tags = True
    list_display = ('titulo', 'orden_prioridad',
                    'plan_de_metas', 'agregar_componente')
    ordering = ['orden_prioridad']
    list_filter = ['plan_de_metas']
    search_fields = ['titulo', 'descripcion']
    fields = ('plan_de_metas', ('titulo', 'orden_prioridad'),
              'descripcion', 'html', 'imagen', ('color_1', 'color_2'))


class ObjetivoInline(admin.StackedInline):
    model = Objetivo
    extra = 1


class ComponenteAdmin(admin.ModelAdmin):
    # inlines = [ObjetivoInline]
    exclude = ('slug', )

    def agregar_objetivo(self, obj):
        url = '/admin/plandemetas/objetivo/add/'
        cls = 'class="addlink"'
        trg = 'target="_blank"'
        txt = 'Agregar objetivo'
        return '<a href="{}?componente={}" {} {}>{}</a>'.format(url, obj.id, cls, trg, txt)

    agregar_objetivo.allow_tags = True
    list_display = ('id', 'titulo', 'orden_prioridad', 'lineamiento', 'agregar_objetivo')
    list_filter = ['lineamiento', 'lineamiento__plan_de_metas']
    search_fields = ['titulo', 'descripcion']
    fields = (('lineamiento', 'orden_prioridad'), 'titulo',
              'descripcion', 'html', 'html_corto_web')


class MetaInline(admin.StackedInline):
    model = Meta
    exclude = ('estado_de_avance', 'iniciada', 'terminada', 'html')
    extra = 1


class ObjetivoAdmin(admin.ModelAdmin):
    # inlines = [MetaInline]
    exclude = ('html', )

    def agregar_meta(self, obj):
        url = '/admin/plandemetas/meta/add/'
        cls = 'class="addlink"'
        trg = 'target="_blank"'
        txt = 'Agregar meta'
        return '<a href="{}?objetivo={}" {} {}>{}</a>'.format(url, obj.id, cls, trg, txt)

    agregar_meta.allow_tags = True
    list_display = ('id', 'titulo', 'orden_prioridad', 'agregar_meta')
    list_filter = ['componente', 'componente__lineamiento',
                    'componente__lineamiento__plan_de_metas']
    search_fields = ['titulo']
    fields = (('componente', 'orden_prioridad'), 'titulo')
    ordering = ['orden_prioridad', 'titulo']


class IndicadorMetaInline(admin.StackedInline):
    model = IndicadorMeta
    extra = 1

    def get_formset(self, request, obj=None, **kwargs):
        ''' si es alguien que tiene un permiso limitado solo permitir esa carga '''
        if request.user.has_perm('plandemetas.metas_editor_anual') and not request.user.is_superuser:
            # permiso especial, solo puede tocar decreto y vinculados
            kwargs['form'] = IndicadorMetaAnualUpdateForm
        return super(IndicadorMetaInline, self).get_formset(request, obj, **kwargs)


class AvanceIndicadorMetaAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'valor_indicador', 'indicador')
    search_fields = ['indicador__indicador', 'indicador__meta__titulo']
    ordering = ['-fecha']


class UserPlanDeMetasAdmin(admin.ModelAdmin):

    def lista_oficinas_autorizadas_metas(self, obj):
        return ', '.join([x.nombre for x in obj.oficinas_autorizadas_metas.all()])
    
    def username(self, obj):
        return '' if obj.user is None else obj.user.username
    
    def first_name(self, obj):
        return '' if obj.user is None else obj.user.first_name
    
    def last_name(self, obj):
        return '' if obj.user is None else obj.user.last_name

    def email(self, obj):
        return '' if obj.user is None else obj.user.email

    list_display = ('username', 'first_name', 'last_name',
                    'lista_oficinas_autorizadas_metas', 'email')
    search_fields = ['user__username', 'user__first_name', 'user__last_name', 'user__email']
    ordering = ['user__username']
    list_select_related = ['user']


class PendientesDeCambiosListFilter(admin.SimpleListFilter):
    title = 'pendientes de cambio'

    parameter_name = 'pendientes_de_cambio'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (('NO', 'Sin cambios pendientes'), ('SI', 'Con cambios pendientes'))

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value() == 'SI':
            pendientes_ids = [
                meta.id for meta in queryset if meta.tiene_cambios_pendientes]
            q = queryset.filter(id__in=pendientes_ids)
        elif self.value() == 'NO':
            pendientes_ids = [
                meta.id for meta in queryset if not meta.tiene_cambios_pendientes]
            q = queryset.filter(id__in=pendientes_ids)
        else:
            q = queryset
        return q


class MetaAdmin(admin.ModelAdmin):
    inlines = [IndicadorMetaInline]
    list_display = ('id', 'titulo', 'tiene_cambios_pendientes',
                    'actualizacion_anual_aprobada', 'orden_prioridad', 'oficina_responsable')
    list_filter = [PendientesDeCambiosListFilter, 'publicado', 'objetivo', 'objetivo__componente',
                    'objetivo__componente__lineamiento', 'new_estado_de_avance', 'estado_de_avance',
                    'oficina_responsable', 'oficinas_relacionadas']
    search_fields = ['titulo', 'objetivo__titulo']

    def tiene_cambios_pendientes(self, obj):
        spa = 'SI' if obj.tiene_cambios_pendientes else 'NO'
        ret = '<span title="{}">{}</span>'.format(
            obj.cambios_pendientes(), spa)
        return ret

    tiene_cambios_pendientes.allow_tags = True

    def get_form(self, request, obj=None, **kwargs):
        ''' si es alguien que tiene un permiso limitado solo permitir esa carga '''
        exclude = ('fecha_actualizacion_oficial',)
        if request.user.has_perm('plandemetas.metas_editor_anual') and not request.user.is_superuser:
            # permiso especial, solo puede tocar decreto y vinculados
            kwargs['form'] = MetaAnualUpdateForm
            self.fields = ('titulo', 'estado_de_avance',
                               'observaciones_generales', 'linea_base',
                               'new_observaciones_generales',
                               'new_estado_de_avance')

        else:
            self.fields = (('objetivo', 'orden_prioridad'), 'titulo', 'actualizacion_anual_aprobada',
                           'ods', 'estado_de_avance', 'publicado',
                           'oficina_responsable', 'oficinas_relacionadas',
                           'linea_base', 'observaciones_generales',
                           'new_titulo', 'new_observaciones_generales',
                           'new_estado_de_avance', 'new_linea_base')

        return super(MetaAdmin, self).get_form(request, obj, **kwargs)

    def get_queryset(self, request):
        ''' permitir que cada usuario vea solo las metas de las áreas donde tiene permisos '''
        qs = super(MetaAdmin, self).get_queryset(request)
        if request.user.has_perm('plandemetas.metas_editor_anual') and not request.user.is_superuser:
            # ver que sea usuario del plan de metas
            try:  # el usuario puede no existir aquí
                user = UserPlanDeMetas.objects.get(user=request.user)
            except Exception as e:
                raise Http404(
                    "Este usaurio todavía no fue asignado a ningúa área específica, contacte al administrador del sistema")

            # cargar solo las metas que tengan como oficina responsable a
            # alguna de las que el tipo tiene autorizadas
            qs = qs.filter(actualizacion_anual_aprobada=True,
                            oficina_responsable__in=user.oficinas_autorizadas_metas.all())

        return qs

    '''
    def get_inline_instances(self, request, obj=None):
        if request.user.has_perm('plandemetas.metas_editor_anual') and not request.user.is_superuser:
            return []
        else:
            return [inline(self.model, self.admin_site) for inline in self.inlines]
    '''


class IndicadorMetaAdmin(admin.ModelAdmin):
    list_display = ('meta', 'publicado', 'indicador', 'valor_indicador',
                    'orden_prioridad', 'new_indicador', 'new_valor_indicador')
    list_filter = ['publicado', 'meta']
    search_fields = ['indicador', 'meta__titulo']

    def get_form(self, request, obj=None, **kwargs):
        ''' si es alguien que tiene un permiso limitado solo permitir esa carga '''
        if request.user.has_perm('plandemetas.metas_editor_anual') and not request.user.is_superuser:
            # permiso especial, solo puede tocar decreto y vinculados
            kwargs['form'] = IndicadorMetaAnualUpdateForm
            self.fields = ('indicador', 'valor_indicador',
                           'new_valor_indicador')
        else:
            self.fields = ('meta', 'publicado', 'indicador', 'valor_indicador',
                           'orden_prioridad', ('new_indicador', 'new_valor_indicador'))
        return super(IndicadorMetaAdmin, self).get_form(request, obj, **kwargs)

    def get_queryset(self, request):
        ''' permitir que cada usuario vea solo las metas de las áreas donde tiene permisos '''
        qs = super(IndicadorMetaAdmin, self).get_queryset(request)
        if request.user.has_perm('plandemetas.metas_editor_anual') and not request.user.is_superuser:
            # ver que sea usuario del plan de metas
            try:  # el usuario puede no existir aquí
                user = UserPlanDeMetas.objects.get(user=request.user)
            except Exception as e:
                raise Http404(
                    "Este usaurio todavía no fue asignado a ningúa área específica, contacte al administrador del sistema")
            # cargar solo las metas que tengan como oficina responsable a
            # alguna de las que el tipo tiene autorizadas
            qs = qs.filter(
                meta__oficina_responsable__in=user.oficinas_autorizadas_metas.all())
        return qs

    def save_model(self, request, obj, form, change):
        ''' interferir el SAVE para revisar los cambios '''
        # obj es el Indicador listo para grabarse
        super(IndicadorMetaAdmin, self).save_model(request, obj, form, change)


class OdsMetasAdmin(admin.ModelAdmin):
    list_display = ('nombre_ods', 'link_ods')


admin.site.register(PlanDeMetas, PlanDeMetasAdmin)
admin.site.register(Lineamiento, LineamientoAdmin)
admin.site.register(Componente, ComponenteAdmin)
admin.site.register(Objetivo, ObjetivoAdmin)
admin.site.register(IndicadorMeta, IndicadorMetaAdmin)
admin.site.register(Meta, MetaAdmin)
admin.site.register(UserPlanDeMetas, UserPlanDeMetasAdmin)
admin.site.register(AvanceIndicadorMeta, AvanceIndicadorMetaAdmin)
admin.site.register(OdsMetas, OdsMetasAdmin)
