from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from cortesdetransito.models import CorteDeTransito, AutoridadesPresentes, AgrupadorCorte, FotoCorteDeTransito
from versatileimagefield.serializers import VersatileImageFieldSerializer


class AutoridadesPresentesSerializer(CachedSerializerMixin):
    class Meta:
        model = AutoridadesPresentes
        fields = ['id', 'nombre']


class FotoCorteDeTransitoSerializer(CachedSerializerMixin):
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail", 'thumbnail__125x125')])

    class Meta:
        model = FotoCorteDeTransito
        fields = ['id', 'foto', 'descripcion']


class CorteDeTransitoSerializer(CachedSerializerMixin):
    motivo = serializers.CharField(source='motivo.nombre')
    responsable = serializers.CharField(source='responsable_principal.nombre')
    autoridades_presentes = AutoridadesPresentesSerializer(many=True)
    agrupador = serializers.SerializerMethodField()
    alcance = serializers.CharField(source='get_alcance_display')
    carril = serializers.CharField(source='get_carril_display')
    direccion_transito = serializers.CharField(
        source='get_direccion_transito_display')
    gravedad = serializers.CharField(source='get_gravedad_display')
    momento = serializers.CharField(source='get_momento_display')
    fotos = FotoCorteDeTransitoSerializer(many=True, read_only=True)

    def get_agrupador(self, obj):
        return '' if obj.agrupador is None else obj.agrupador.nombre

    class Meta:
        model = CorteDeTransito
        fields = [
            'id',
            'gravedad',
            'agrupador',
            'motivo',
            'responsable',
            'calle_principal',
            'esquina',
            'entre_calle_1',
            'entre_calle_2',
            'momento',
            'observaciones',
            'autoridades_presentes',
            'alcance',
            'carril',
            'direccion_transito',
            'fecha_inicio_estimada',
            'fecha_finalizacion_estimada',
            'fotos']


class CorteDeTransitoGeoSerializer(
        GeoFeatureModelSerializer,
        CachedSerializerMixin):
    ''' versión GeoJson de los cortes de tránsito '''
    motivo = serializers.CharField(source='motivo.nombre')
    responsable = serializers.CharField(source='responsable_principal.nombre')
    autoridades_presentes = AutoridadesPresentesSerializer(many=True)
    agrupador = serializers.SerializerMethodField()
    alcance = serializers.CharField(source='get_alcance_display')
    carril = serializers.CharField(source='get_carril_display')
    direccion_transito = serializers.CharField(
        source='get_direccion_transito_display')
    gravedad = serializers.CharField(source='get_gravedad_display')
    momento = serializers.CharField(source='get_momento_display')
    fotos = FotoCorteDeTransitoSerializer(many=True)

    def get_agrupador(self, obj):
        return obj.agrupador.nombre if obj.agrupador is not None else obj.agrupador

    class Meta:
        model = CorteDeTransito
        geo_field = "poligono"
        fields = [
            'id',
            'gravedad',
            'agrupador',
            'motivo',
            'responsable',
            'calle_principal',
            'esquina',
            'entre_calle_1',
            'entre_calle_2',
            'momento',
            'observaciones',
            'autoridades_presentes',
            'alcance',
            'carril',
            'direccion_transito',
            'fecha_inicio_estimada',
            'fecha_finalizacion_estimada',
            'fotos']


class CorteDeTransitoWazeSerializer(CachedSerializerMixin):
    """
    versión GeoJson de los cortes de tránsito adaptado para Waze
    """
    id = serializers.SerializerMethodField()
    description = serializers.CharField(source='motivo.nombre')
    reference = serializers.CharField(source='responsable_principal.nombre')
    direction = serializers.SerializerMethodField()
    street = serializers.CharField(source='calle_principal')
    starttime = serializers.CharField(source='fecha_inicio_estimada')
    endtime = serializers.CharField(source='fecha_finalizacion_estimada')
    paths = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()

    def get_id(self, obj):
        return 'Cba{}'.format(obj.id)

    def get_type(self, obj):
        return 'ROAD_CLOSED'

    def get_direction(self, obj):
        if obj.alcance != obj.ALC_MEDIA_CALZADA:
            direction = "BOTH_DIRECTIONS"
        else:
            direction = "ONE_DIRECTION"
        return direction

    def get_paths(self, obj):
        # elimino la última coordenada así deja de ser polígono
        coordenadas = tuple(set(obj.poligono.coords[0]))
        return (coordenadas,)

    class Meta:
        model = CorteDeTransito
        fields = ['id', 'type', 'paths', 'street', 'starttime', 'endtime',
                  'description', 'direction', 'reference']


# Registro los serializadores en la cache de DRF
cache_registry.register(AutoridadesPresentesSerializer)
cache_registry.register(CorteDeTransitoSerializer)
cache_registry.register(CorteDeTransitoWazeSerializer)
