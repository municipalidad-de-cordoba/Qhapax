from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from rest_framework.filters import OrderingFilter
from .serializers import (CorteDeTransitoSerializer,
                          CorteDeTransitoGeoSerializer,
                          CorteDeTransitoWazeSerializer)
from api.pagination import DefaultPagination
from django.db.models import Q
from datetime import datetime
from cortesdetransito.models import CorteDeTransito


class CorteDeTransitoViewSet(viewsets.ModelViewSet):
    """
    Filtros:
    q = texto a buscar entre los campos de ese tipo (calles, responsables, etc)
    momento = CorteDeTransito.MOM_CORTE_NO_PROGRAMADO (10) o CorteDeTransito.MOM_CORTE_PROGRAMADO (20)
    id_agrupador = id del agrupador. Pueden ser varios ids separados por comas.

    Puede ordenarse por:
        ID: &ordering=id o DESC &ordering=-id

    """
    serializer_class = CorteDeTransitoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    filter_backends = (OrderingFilter,)

    def get_queryset(self):

        queryset = CorteDeTransito.objects.filter(Q(publicado=True), Q(
            finalizado=False), Q(fecha_finalizacion_estimada__gte=datetime.now()))

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(motivo__nombre__icontains=q) |
                Q(responsable_principal__nombre__icontains=q) |
                Q(calle_principal__icontains=q) |
                Q(esquina__icontains=q) |
                Q(entre_calle_1__icontains=q) |
                Q(entre_calle_2__icontains=q) |
                Q(observaciones__icontains=q)
            )

        momento = self.request.query_params.get('momento', None)
        if momento is not None:
            queryset = queryset.filter(momento=int(momento))

        id_agrupador = self.request.query_params.get('id_agrupador', None)
        if id_agrupador is not None:
            id_agrupador = id_agrupador.split(',')
            queryset = queryset.filter(agrupador__in=id_agrupador)

        queryset = queryset.order_by('-momento', 'fecha_finalizacion_estimada')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CorteDeTransitoGeoViewSet(viewsets.ModelViewSet):
    """
    Filtros:
    q = texto a buscar entre los campos de ese tipo (calles, responsables, etc)
    momento = CorteDeTransito.MOM_CORTE_NO_PROGRAMADO (10) o CorteDeTransito.MOM_CORTE_PROGRAMADO (20)
    id_agrupador = id del agrupador. Pueden ser varios ids separados por comas.

    Puede ordenarse por:
        ID: &ordering=id o DESC &ordering=-id
    """
    serializer_class = CorteDeTransitoGeoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    filter_backends = (OrderingFilter,)

    def get_queryset(self):

        queryset = CorteDeTransito.objects.filter(Q(publicado=True), Q(
            finalizado=False), Q(fecha_finalizacion_estimada__gte=datetime.now()))

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(motivo__nombre__icontains=q) |
                Q(responsable_principal__nombre__icontains=q) |
                Q(calle_principal__icontains=q) |
                Q(esquina__icontains=q) |
                Q(entre_calle_1__icontains=q) |
                Q(entre_calle_2__icontains=q) |
                Q(observaciones__icontains=q)
            )

        momento = self.request.query_params.get('momento', None)
        if momento is not None:
            queryset = queryset.filter(momento=int(momento))

        id_agrupador = self.request.query_params.get('id_agrupador', None)
        if id_agrupador is not None:
            id_agrupador = id_agrupador.split(',')
            queryset = queryset.filter(agrupador__in=id_agrupador)

        queryset = queryset.order_by('-momento', 'fecha_finalizacion_estimada')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class HistoricoDeTransitoGeoViewSet(viewsets.ModelViewSet):
    """
    Filtros:
    q = texto a buscar entre los campos de ese tipo (calles, responsables, etc)
    momento = CorteDeTransito.MOM_CORTE_NO_PROGRAMADO (10) o CorteDeTransito.MOM_CORTE_PROGRAMADO (20)
    id_agrupador = id del agrupador. Pueden ser varios ids separados por comas.

    Puede ordenarse por:
        ID: &ordering=id o DESC &ordering=-id
    """
    serializer_class = CorteDeTransitoGeoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    filter_backends = (OrderingFilter,)

    def get_queryset(self):

        queryset = CorteDeTransito.objects.filter(publicado=True)

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(motivo__nombre__icontains=q) |
                Q(responsable_principal__nombre__icontains=q) |
                Q(calle_principal__icontains=q) |
                Q(esquina__icontains=q) |
                Q(entre_calle_1__icontains=q) |
                Q(entre_calle_2__icontains=q) |
                Q(observaciones__icontains=q)
            )

        momento = self.request.query_params.get('momento', None)
        if momento is not None:
            queryset = queryset.filter(momento=int(momento))

        id_agrupador = self.request.query_params.get('id_agrupador', None)
        if id_agrupador is not None:
            id_agrupador = id_agrupador.split(',')
            queryset = queryset.filter(agrupador__in=id_agrupador)

        queryset = queryset.order_by('-momento', 'fecha_finalizacion_estimada')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CorteDeTransitoWazeViewSet(viewsets.ModelViewSet):
    """
    Filtros:
    q = texto a buscar entre los campos de ese tipo (calles, responsables, etc)
    """
    serializer_class = CorteDeTransitoWazeSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):

        queryset = CorteDeTransito.objects.filter(Q(publicado=True), Q(
            finalizado=False), Q(fecha_finalizacion_estimada__gte=datetime.now()))

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(motivo__nombre__icontains=q) |
                Q(responsable_principal__nombre__icontains=q) |
                Q(calle_principal__icontains=q) |
                Q(esquina__icontains=q) |
                Q(entre_calle_1__icontains=q) |
                Q(entre_calle_2__icontains=q) |
                Q(observaciones__icontains=q)
            )

        queryset = queryset.order_by('-momento', 'fecha_finalizacion_estimada')

        # filtro aquellos cortes válidos para Waze
        queryset = queryset.exclude(Q(motivo__nombre=None) |
                                    Q(responsable_principal__nombre=None) |
                                    Q(poligono=None)
                                    )

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
