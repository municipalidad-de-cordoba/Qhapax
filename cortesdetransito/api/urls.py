from django.conf.urls import url, include
from rest_framework import routers
from .views import (CorteDeTransitoViewSet, CorteDeTransitoGeoViewSet,
                    HistoricoDeTransitoGeoViewSet, CorteDeTransitoWazeViewSet)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'cortes-activos',
    CorteDeTransitoViewSet,
    base_name='cortes-activos.api')
router.register(
    r'cortes-activos-geo',
    CorteDeTransitoGeoViewSet,
    base_name='cortes-activos-geo.api')
router.register(
    r'historico-de-cortes',
    HistoricoDeTransitoGeoViewSet,
    base_name='historico-de-cortes.api')
router.register(
    r'cortes-transito-waze',
    CorteDeTransitoWazeViewSet,
    base_name='cortes-transito-waze.api')

urlpatterns = [
    url(r'^', include(router.urls)),
]
