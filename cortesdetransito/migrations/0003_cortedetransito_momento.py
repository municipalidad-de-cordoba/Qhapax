# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-08-15 14:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cortesdetransito', '0002_auto_20170803_1521'),
    ]

    operations = [
        migrations.AddField(
            model_name='cortedetransito',
            name='momento',
            field=models.PositiveIntegerField(choices=[(10, 'Corte actual'), (20, 'Corte programado')], default=10),
        ),
    ]
