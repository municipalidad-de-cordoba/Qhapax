from django.contrib import admin
from .models import (AgrupadorCorte, AutoridadesPresentes, CorteDeTransito,
                     FotoCorteDeTransito, MotivoCorteDeTransito,
                     ResponsablesCorte)
from core.admin import QhapaxOSMGeoAdmin


class MotivoCorteDeTransitoAdmin(admin.ModelAdmin):
    list_display = ('nombre', )
    # list_filter = ['tags', 'personas', 'oficinas']
    search_fields = ['nombre']


class ResponsablesCorteAdmin(admin.ModelAdmin):
    list_display = ('nombre', )
    # list_filter = ['tags', 'personas', 'oficinas']
    search_fields = ['nombre']


class AutoridadesPresentesAdmin(admin.ModelAdmin):
    list_display = ('nombre', )
    # list_filter = ['tags', 'personas', 'oficinas']
    search_fields = ['nombre']


class AgrupadorCorteAdmin(admin.ModelAdmin):
    search_fields = ['id', 'nombre']
    list_display = ('id', 'nombre')
    search_fields = ['nombre']


class FotoCorteDeTransitoInline(admin.StackedInline):
    model = FotoCorteDeTransito
    extra = 1


class CorteDeTransitoAdmin(QhapaxOSMGeoAdmin):
    def alcance(self, obj):
        return obj.get_alcance_display()

    def carriles(self, obj):
        return obj.get_carril_display()

    def gravedad(self, obj):
        return obj.get_gravedad_display()

    def momento(self, obj):
        return obj.get_momento_display()

    list_display = (
        'id',
        'agrupador',
        'calle_principal',
        'esquina',
        'entre_calle_1',
        'entre_calle_2',
        'alcance',
        'gravedad',
        'motivo',
        'responsable_principal',
        'publicado',
        'finalizado',
        'fecha_inicio_estimada',
        'fecha_finalizacion_estimada',
        'momento')

    # list_filter = ['tags', 'personas', 'oficinas']
    exclude = ['id_waze', ]
    search_fields = [
        'id',
        'calle_principal',
        'esquina',
        'entre_calle_1',
        'entre_calle_2']
    list_filter = [
        'publicado',
        'finalizado',
        'responsable_principal',
        'gravedad',
        'motivo',
        'momento']
    inlines = [FotoCorteDeTransitoInline]


admin.site.register(MotivoCorteDeTransito, MotivoCorteDeTransitoAdmin)
admin.site.register(ResponsablesCorte, ResponsablesCorteAdmin)
admin.site.register(AutoridadesPresentes, AutoridadesPresentesAdmin)
admin.site.register(CorteDeTransito, CorteDeTransitoAdmin)
admin.site.register(AgrupadorCorte, AgrupadorCorteAdmin)
