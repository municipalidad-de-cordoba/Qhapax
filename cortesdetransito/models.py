from django.db import models
from django.utils import timezone
from django.contrib.gis.db import models as models_geo
from versatileimagefield.fields import VersatileImageField, PPOIField


class MotivoCorteDeTransito(models.Model):
    ''' motivos por los cuales puede estar cortado el tránsito
        ej: accidente, obra, semaforo apagado
    '''
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre


class ResponsablesCorte(models.Model):
    ''' empresa o grupo responsable del corte
        EJ: EPEC, Aguas Cordobesas, Marcha Partido Políticos, etc.'''

    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre


class AutoridadesPresentes(models.Model):
    ''' Autoridades que pueden actuar
        ej: Policia Municipal, provincial, 107, otros '''
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre


class AgrupadorCorte(models.Model):
    """
    Categoría o agrupación general
    """
    nombre = models.CharField(max_length=250)
    descripcion = models.TextField(
        verbose_name='Descripcion del agrupador',
        help_text="Detalles generales de los cortes agrupados",
        null=True,
        blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Agrupadores de cortes de tránsito"
        ordering = ['nombre']


class CorteDeTransito(models.Model):
    """
    cortes de tránsito en la vía pública
    """

    agrupador = models.ForeignKey(
        AgrupadorCorte,
        null=True,
        blank=True,
        on_delete=models.SET_NULL)
    motivo = models.ForeignKey(
        MotivoCorteDeTransito,
        null=True,
        on_delete=models.SET_NULL)
    responsable_principal = models.ForeignKey(
        ResponsablesCorte, null=True, on_delete=models.SET_NULL)

    calle_principal = models.CharField(
        max_length=90, help_text='Calle principalmente afectada')
    esquina = models.CharField(
        max_length=90,
        null=True,
        blank=True,
        help_text='esquina SOLO si corresponde')
    entre_calle_1 = models.CharField(
        max_length=90,
        null=True,
        blank=True,
        help_text='Solo si corresnpode, la PRIMERA callen entre las que esta el corte')
    entre_calle_2 = models.CharField(
        max_length=90,
        null=True,
        blank=True,
        help_text='Solo si corresnpode, la SEGUNDA callen entre las que esta el corte')

    publicado = models.BooleanField(
        default=True, help_text='Información que pueden ver el público')
    finalizado = models.BooleanField(
        default=False, help_text='Marca de que el corte ya finalizo')

    observaciones_internas = models.TextField(
        null=True, blank=True, help_text='Detalles internos (no públicos) del corte')
    observaciones = models.TextField(
        null=True,
        blank=True,
        help_text='Detalles públicos del corte')

    autoridades_presentes = models.ManyToManyField(
        AutoridadesPresentes, blank=True)

    MOM_CORTE_NO_PROGRAMADO = 10
    MOM_CORTE_PROGRAMADO = 20

    momentos = ((MOM_CORTE_NO_PROGRAMADO, 'Corte no programado'),
                (MOM_CORTE_PROGRAMADO, 'Corte programado')
                )

    momento = models.PositiveIntegerField(
        choices=momentos, default=MOM_CORTE_PROGRAMADO)
    poligono = models_geo.PolygonField(null=True, blank=True)

    # alcances del corte
    ALC_TOTAL = 10
    ALC_MEDIA_CALZADA = 20
    ALC_DESCONOCIDO = 30

    alcances = ((ALC_TOTAL, 'Corte total'),
                (ALC_MEDIA_CALZADA, 'Corte media calzada'),
                (ALC_DESCONOCIDO, 'Desconocido')
                )
    alcance = models.PositiveIntegerField(
        choices=alcances, default=ALC_DESCONOCIDO)

    # carriles afectados

    CARR_DERECHO = 10
    CARR_IZQUIERDO = 20
    CARR_CENTRAL = 30
    CARR_COMPLETO = 40
    CARR_DESCONOCIDO = 50

    carriles = ((CARR_DERECHO, 'Carril derecho'),
                (CARR_IZQUIERDO, 'Carril Izquierdo'),
                (CARR_CENTRAL, 'Carril Central'),
                (CARR_COMPLETO, 'Carril Completo'),
                (CARR_DESCONOCIDO, 'Desconocido')
                )
    carril = models.PositiveIntegerField(
        choices=carriles, default=CARR_DESCONOCIDO)

    # direcciones de circulación
    DIR_ESTE = 10
    DIR_OESTE = 20
    DIR_SUR = 30
    DIR_NORTE = 40
    DIR_DESCONOCIDA = 50

    direcciones = ((DIR_NORTE, 'Hacia el norte'),
                   (DIR_SUR, 'Hacia el sur'),
                   (DIR_ESTE, 'Hacia el este'),
                   (DIR_OESTE, 'Hacia el oeste'),
                   (DIR_DESCONOCIDA, 'Desconocida')
                   )
    direccion_transito = models.PositiveIntegerField(
        choices=direcciones, default=DIR_DESCONOCIDA)

    # gravedad del caso
    GRAVEDAD_BAJA = 10
    GRAVEDAD_MEDIA = 20
    GRAVEDAD_ALTA = 30
    GRAVEDAD_DESCONOCIDA = 40

    gravedades = ((GRAVEDAD_BAJA, 'Gravedad baja'),
                  (GRAVEDAD_MEDIA, 'Gravedad media'),
                  (GRAVEDAD_ALTA, 'Gravedad alta'),
                  (GRAVEDAD_DESCONOCIDA, 'Gravedad desconocida')
                  )

    gravedad = models.PositiveIntegerField(
        choices=gravedades, default=GRAVEDAD_DESCONOCIDA)

    def hace_15_minutos():
        return timezone.now() - timezone.timedelta(minutes=15)

    def en_una_hora():
        return timezone.now() + timezone.timedelta(hours=1)

    fecha_inicio_estimada = models.DateTimeField(default=hace_15_minutos)
    fecha_finalizacion_estimada = models.DateTimeField(default=en_una_hora)

    class Meta:
        verbose_name_plural = "Cortes de tránsito"


class FotoCorteDeTransito(models.Model):
    corte = models.ForeignKey(
        CorteDeTransito,
        on_delete=models.CASCADE,
        related_name='fotos')
    foto = ppoi = PPOIField('Punto de interes de la imágen')
    foto = VersatileImageField(
        upload_to='imagenes/cortes-de-transito',
        ppoi_field='ppoi')
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        descr = self.descripcion or ''
        return 'Foto de corte {} {}'.format(self.corte.id, descr)
