Cortes de tránsito
==================

En este módulo se registran los cortes de tránsito programados en la ciudad. Actualmente cuenta con las siguientes APIS públicas:

- cortes activos_: información sobre los cortes activos de la ciudad.

- cortes activos geográficos_: similar al anterior, pero con información georeferenciada.

- cortes activos para waze_: versión adaptada al estándar de waze, de modo que nuestra información está integrada a la plataforma de waze.

- cortes históricos_: historial de todos los cortes registrados.

.. _activos: https://gobiernoabierto.cordoba.gob.ar/api/v2/cortes-de-transito/cortes-activos/

.. _geográficos: https://gobiernoabierto.cordoba.gob.ar/api/v2/cortes-de-transito/cortes-activos-geo/

.. _waze: https://gobiernoabierto.cordoba.gob.ar/api/v2/cortes-de-transito/cortes-transito-waze/

.. _históricos: https://gobiernoabierto.cordoba.gob.ar/api/v2/cortes-de-transito/historico-de-cortes/

La información es cargada por personal de la oficina de tránsito, miestras mejoramos los procesos internos es posible que haya delay en la actualización y correción de datos.
