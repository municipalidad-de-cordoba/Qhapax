from django.db import models


class TipoIntervencionCalidadAlimentaria(models.model):
    '''
    Tipos de actuaciones de los ispectores de calidad alimentaria
    (infracción, clausura, etc)
    '''
    nombre = models.CharField(max_length=90)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class SubTipoIntervencionCalidadAlimentaria(models.Model):
    ''' cada uno de los tipos de intervencion específica '''
    tipo = models.ForeignKey(TipoIntervencionCalidadAlimentaria)
    nombre = models.CharField(max_length=90)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return '{} - {}'.format(self.tipo.nombre, self.nombre)


class RubroComercioCalidadAlimentaria(models.Model):
    ''' Rubros de comercios a inspeccionar '''
    nombre = models.CharField(max_length=90)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class MotivoVisitaCalidadAlimentaria(models.Model):
    ''' lista de los motivos por los que se visita un comercio '''
    nombre = models.CharField(max_length=90)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class MotivoIntervencionCalidadAlimentaria(models.Model):
    ''' lista de los motivos por los que se visita un comercio '''
    nombre = models.CharField(max_length=90)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre


'''
RUBROS
Estética
Odontología
Veterinaria
Laboratorio
Farmacia
Veterinaria
Centro médico
Kinesiología
Bar
Kiosco
Fabrica de pasta
Ṕanadería

MOTIVOS VISITA
Renovación C.A.

MOTIVO INTERVENCION
    Infraccion:
        ALIMENTOS SIN ROTULACION
        MERCADERIA NO APTA PARA CONSUMO HUMANO
        CARECE DE FRIO
    Clausura:
        INCUMPLIMIENTO A EMPLAZAMIENTO
        NO POSEE DE AGUA CALIENTE
        FALTA DE HIGIENE
        CARECE DE HABILITACION
        INSUFICIENTE SISTEMAS CONTRA INCENDIOS



INFRACCIONES
    1. INCUMPLIMIENTO A EMPLAZAMIENTO
    2. CERTIFICADO AMBIENTAL VENCIDO
    3. VIOLACION DE CLAUSURA
    4. VIOLAR NORMAS DE GESTION DE R. PATOGENOS
    5. CERTIFICADO DE DESINFECCION VENCIDO
    6. ENTORPECER TAREA DEL INSPECTOR
    7. NO RESPETAR INTERVENCION PREVENTIVA DE MERCADERIA
    8. MERCADERIA NO CERTIFICADA PARA CONSUMO HUMANO
CLAUSURAS
    9. INCUMPLIMIENTO A EMPLAZAMIENTO
    10. VIOLACION DE CLAUSURA
    11. CARECE DE HABILITACION
    12. FALTA DE HIGIENE
    13. INSUFICIENTE SISTEMAS CONTRA INCENDIOS
    14. NO POSEE DE AGUA CALIENTE
    15. FALTA DE SEGURIDAD
INTERVENCIONES
    16. CARECE DE FRIO
    17. MERCADERIA NO APTA PARA CONSUMO HUMANO
    18. CARECE DE CERTIFICADO HABILITANTE
    19. ALIMENTOS SIN ROTULACION
    20. TRANS. SIN HABILITACION
    21. FRAUDE BROMATOLOGICO
'''
