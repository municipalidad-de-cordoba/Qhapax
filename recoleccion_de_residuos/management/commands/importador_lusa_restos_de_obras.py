#!/usr/bin/python

from django.core.management.base import BaseCommand
from django.db import transaction
from recoleccion_de_residuos.models import EmpresaPrestatariaRecoleccion, TipoRecoleccion, TurnoRecoleccion, ZonaRecoleccion, BarrioRecoleccion, DiaRecoleccion
from core.utils import normalizar
import sys
import csv


class Command(BaseCommand):
    help = """Comando para cargar las zonas y horarios de basura"""

    def add_arguments(self, parser):
        parser.add_argument('--path', type=str, help='Path del archivo CSV local',
                            default='recoleccion_de_residuos/resources/lusa-restos-de-obras.csv')

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('--- Comenzando carga ---'))
        fieldnames = ['Barrio','Ruta_Número','Día', 'Turno']
        count = 0
        path = options['path']
        # Seteo de cosas que van a estar fijas en los diferentes scripts
        empresa = EmpresaPrestatariaRecoleccion.objects.get(nombre="LUSA")
        tipo_recoleccion = TipoRecoleccion.objects.get(nombre="Restos de obras")
        nombre_zona = "Zona 3"

        with open(path, encoding="UTF-8") as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=',', quotechar='"')

            header = reader.__next__()
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR('BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            for row in reader:
                count += 1
                print(row)

                barrio = normalizar(row['Barrio'])
                barrio_obj, created_b = BarrioRecoleccion.objects.get_or_create(nombre=barrio)
                ruta_numero = normalizar(row['Ruta_Número'])
                dia = normalizar(row['Día'])
                turno = normalizar(row['Turno'])
                tarde = normalizar("Tarde")
                maniana = normalizar("Mañana")

                if turno == tarde:
                    turno_obj = TurnoRecoleccion.objects.get(turno=TurnoRecoleccion.TURNO_TARDE,
                                                             desde="14:00",
                                                             hasta="22:00"
                                                            )
                elif turno == maniana:
                    turno_obj = TurnoRecoleccion.objects.get(turno=TurnoRecoleccion.TURNO_MAÑANA,
                                                             desde="06:00",
                                                             hasta="14:00"
                                                            )

                zona_recoleccion, created = ZonaRecoleccion.objects.get_or_create(
                                            numero_de_ruta=ruta_numero,
                                            empresa=empresa,
                                            tipos=tipo_recoleccion,
                                            nombre=nombre_zona
                                            )

                # Si ya estaba creado
                if not created:
                    zona_recoleccion.barrio.add(barrio_obj)
                    if zona_recoleccion.observaciones_internas:
                        tmp = str(zona_recoleccion.observaciones_internas)
                        observaciones_internas = "Barrio: {} - Turno: {} \n".format(barrio_obj.nombre, turno_obj)
                        zona_recoleccion.observaciones_internas = tmp + observaciones_internas
                    if zona_recoleccion.turno != turno_obj:
                        zona_recoleccion.turno.add(turno_obj)
                    zona_recoleccion.save()
                else:
                    zona_recoleccion.barrio.add(barrio_obj)
                    zona_recoleccion.observaciones_publicas = dia
                    zona_recoleccion.turno.add(turno_obj)
                    zona_recoleccion.save()

        self.stdout.write("Total de zonas grabadas: {}".format(count))
