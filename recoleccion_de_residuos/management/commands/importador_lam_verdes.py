#!/usr/bin/python

from django.core.management.base import BaseCommand
from django.db import transaction
from recoleccion_de_residuos.models import EmpresaPrestatariaRecoleccion, TipoRecoleccion, TurnoRecoleccion, ZonaRecoleccion, BarrioRecoleccion, DiaRecoleccion
from core.utils import normalizar
import sys
import csv


class Command(BaseCommand):
    help = """Comando para cargar las zonas y horarios de basura"""

    def add_arguments(self, parser):
        parser.add_argument('--path', type=str, help='Path del archivo CSV local',
                            default='recoleccion_de_residuos/resources/lam-verdes.csv')

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('--- Comenzando carga ---'))
        fieldnames = ['Barrio','Ruta_Número','Calles','Día', 'Turno']
        count = 0
        path = options['path']
        # Seteo de cosas que van a estar fijas en los diferentes scripts
        empresa = EmpresaPrestatariaRecoleccion.objects.get(nombre="LAM")
        tipo_recoleccion = TipoRecoleccion.objects.get(nombre="Verdes")
        nombre_zona = "Zona 2"

        with open(path, encoding="UTF-8") as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=',', quotechar='"')

            header = reader.__next__()
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR('BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            for row in reader:
                count += 1
                print(row)

                barrio = normalizar(row['Barrio'])
                barrio_obj, created_b = BarrioRecoleccion.objects.get_or_create(nombre=barrio)
                calles = normalizar(row['Calles'])
                ruta_numero = normalizar(row['Ruta_Número'])
                dia = normalizar(row['Día'])
                turno = normalizar(row['Turno'])
                tarde = normalizar("Tarde")

                if turno == tarde:
                    if dia == "lunes":
                        turno_obj = TurnoRecoleccion.objects.get(desde="14:00",
                                                                 hasta="22:00")
                        dia_obj = DiaRecoleccion.objects.get(dia="Lunes")
                    elif dia == "martes":
                        turno_obj = TurnoRecoleccion.objects.get(desde="14:00",
                                                                 hasta="22:00")
                        dia_obj = DiaRecoleccion.objects.get(dia="Martes")
                    elif dia == "miercoles":
                        turno_obj = TurnoRecoleccion.objects.get(desde="14:00",
                                                                 hasta="22:00")
                        dia_obj = DiaRecoleccion.objects.get(dia="Miercoles")
                    elif dia == "jueves":
                        turno_obj = TurnoRecoleccion.objects.get(desde="14:00",
                                                                 hasta="22:00")
                        dia_obj = DiaRecoleccion.objects.get(dia="Jueves")
                    elif dia == "viernes":
                        turno_obj = TurnoRecoleccion.objects.get(desde="14:00",
                                                                 hasta="22:00")
                        dia_obj = DiaRecoleccion.objects.get(dia="Viernes")
                    elif dia == "sabado":
                        turno_obj = TurnoRecoleccion.objects.get(desde="14:00",
                                                                 hasta="22:00")
                        dia_obj = DiaRecoleccion.objects.get(dia="Sabado")
                    else:
                        self.stdout.write(self.style.ERROR('Nuevo dia no detectado anteriormente: {}').format(dia))
                        sys.exit(1)

                zona_recoleccion, created = ZonaRecoleccion.objects.get_or_create(
                                            numero_de_ruta=ruta_numero,
                                            empresa=empresa,
                                            tipos=tipo_recoleccion,
                                            nombre=nombre_zona
                                            )

                # Si ya estaba creado
                if not created:
                    zona_recoleccion.barrio.add(barrio_obj)
                    if zona_recoleccion.observaciones_internas and calles:
                        tmp = str(zona_recoleccion.observaciones_internas)
                        observaciones_internas = "Barrio: {} - Calles: {} - Turno: {} \n".format(barrio_obj.nombre, calles, turno_obj)
                        zona_recoleccion.observaciones_internas = tmp + observaciones_internas
                    elif calles:
                        observaciones_internas = "Barrio: {} - Calles: {} - Turno: {} \n".format(barrio_obj.nombre, calles, turno_obj)
                        zona_recoleccion.observaciones_internas = observaciones_internas
                    if zona_recoleccion.turno != turno_obj:
                        zona_recoleccion.turno.add(turno_obj)
                    zona_recoleccion.save()
                else:
                    zona_recoleccion.barrio.add(barrio_obj)
                    zona_recoleccion.dia.add(dia_obj)
                    zona_recoleccion.turno.add(turno_obj)
                    zona_recoleccion.calles = calles
                    zona_recoleccion.save()

        self.stdout.write("Total de zonas grabadas: {}".format(count))
