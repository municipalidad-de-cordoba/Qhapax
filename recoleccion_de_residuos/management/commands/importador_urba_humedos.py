#!/usr/bin/python

from django.core.management.base import BaseCommand
from django.db import transaction
from recoleccion_de_residuos.models import EmpresaPrestatariaRecoleccion, TipoRecoleccion, TurnoRecoleccion, ZonaRecoleccion, BarrioRecoleccion, DiaRecoleccion
from core.utils import normalizar
import sys
import csv


class Command(BaseCommand):
    help = """Comando para cargar las zonas y horarios de basura"""

    def add_arguments(self, parser):
        parser.add_argument('--path', type=str, help='Path del archivo CSV local',
                            default='recoleccion_de_residuos/resources/urba-humedos.csv')

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('--- Comenzando carga ---'))
        fieldnames = ['Barrio','Calles','Ruta_Número','Día', 'Turno']
        count = 0
        path = options['path']
        # Seteo de cosas que van a estar fijas en los diferentes scripts
        empresa = EmpresaPrestatariaRecoleccion.objects.get(nombre="URBA")
        tipo_recoleccion = TipoRecoleccion.objects.get(nombre="Humedos")
        nombre_zona = "Zona 1"

        with open(path, encoding="UTF-8") as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=',', quotechar='"')

            header = reader.__next__()
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR('BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            for row in reader:
                count += 1
                print(row)

                barrio = normalizar(row['Barrio'])
                barrio_obj, created_b = BarrioRecoleccion.objects.get_or_create(nombre=barrio)
                calles = normalizar(row['Calles'])
                ruta_numero = normalizar(row['Ruta_Número'])
                dia = normalizar(row['Día'])
                turno = normalizar(row['Turno'])
                l_a_s = normalizar("Lunes a Sábados")
                d_a_v = normalizar("Domingo a Viernes")
                maniana = normalizar("06:00 a 14:00 hs")
                noche = normalizar("20:00 a 03:00 hs")

                if turno == maniana:
                    turno_obj = TurnoRecoleccion.objects.get(
                                    turno=TurnoRecoleccion.TURNO_MAÑANA,
                                    desde="06:00", hasta="14:00"
                                )
                elif turno == noche:
                    turno_obj = TurnoRecoleccion.objects.get(
                                    turno=TurnoRecoleccion.TURNO_NOCHE,
                                    desde="20:00", hasta="03:00"
                                )
                else:
                    self.stdout.write(self.style.ERROR('Nuevo turno no detectado antes: {} {}').format(dia, turno))
                    sys.exit(1)

                dia_list = []
                if dia == l_a_s:
                    dia_list.append(DiaRecoleccion.objects.get(dia="Lunes"))
                    dia_list.append(DiaRecoleccion.objects.get(dia="Martes"))
                    dia_list.append(DiaRecoleccion.objects.get(dia="Miercoles"))
                    dia_list.append(DiaRecoleccion.objects.get(dia="Jueves"))
                    dia_list.append(DiaRecoleccion.objects.get(dia="Viernes"))
                    dia_list.append(DiaRecoleccion.objects.get(dia="Sabado"))
                elif dia == d_a_v:
                    dia_list.append(DiaRecoleccion.objects.get(dia="Domingo"))
                    dia_list.append(DiaRecoleccion.objects.get(dia="Lunes"))
                    dia_list.append(DiaRecoleccion.objects.get(dia="Martes"))
                    dia_list.append(DiaRecoleccion.objects.get(dia="Miercoles"))
                    dia_list.append(DiaRecoleccion.objects.get(dia="Jueves"))
                    dia_list.append(DiaRecoleccion.objects.get(dia="Viernes"))


                zona_recoleccion, created = ZonaRecoleccion.objects.get_or_create(
                                                numero_de_ruta=ruta_numero,
                                                empresa=empresa,
                                                tipos=tipo_recoleccion,
                                                nombre=nombre_zona
                                            )

                # Si ya estaba creado
                if not created:
                    zona_recoleccion.barrio.add(barrio_obj)
                    if zona_recoleccion.observaciones_internas and calles:
                        tmp = str(zona_recoleccion.observaciones_internas)
                        observaciones_internas = "Barrio: {} - Calles: {} - Turno: {} \n".format(barrio_obj.nombre, calles, turno_obj)
                        zona_recoleccion.observaciones_internas = tmp + observaciones_internas
                    elif calles:
                        observaciones_internas = "Barrio: {} - Calles: {} - Turno: {} \n".format(barrio_obj.nombre, calles, turno_obj)
                        zona_recoleccion.observaciones_internas = observaciones_internas
                    if zona_recoleccion.turno != turno_obj:
                        zona_recoleccion.turno.add(turno_obj)
                    zona_recoleccion.save()
                else:
                    zona_recoleccion.dia = dia_list
                    zona_recoleccion.barrio.add(barrio_obj)
                    zona_recoleccion.turno.add(turno_obj)
                    zona_recoleccion.calles = calles
                    zona_recoleccion.save()

        self.stdout.write("Total de zonas grabadas: {}".format(count))
