from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from recoleccion_de_residuos.models import EmpresaPrestatariaRecoleccion, TipoRecoleccion, TurnoRecoleccion, BarrioRecoleccion, ZonaRecoleccion, DiaRecoleccion
from rest_framework import serializers
from html import unescape
import bleach


class EmpresaPrestatariaRecoleccionSerializer(CachedSerializerMixin):

    class Meta:
        model = EmpresaPrestatariaRecoleccion
        fields = ('id', 'nombre')


class TipoRecoleccionSerializer(CachedSerializerMixin):

    class Meta:
        model = TipoRecoleccion
        fields = ('id', 'nombre')


class TurnoRecoleccionSerializer(CachedSerializerMixin):

    class Meta:
        model = TurnoRecoleccion
        fields = ('id', 'desde', 'hasta')


class DiaRecoleccionSerializer(CachedSerializerMixin):

    class Meta:
        model = DiaRecoleccion
        fields = ('id', 'dia')


class BarrioRecoleccionSerializer(CachedSerializerMixin):

    class Meta:
        model = BarrioRecoleccion
        fields = ('id', 'nombre')


class ZonaRecoleccionSerializer(CachedSerializerMixin):
    empresa = EmpresaPrestatariaRecoleccionSerializer(read_only=True)
    tipos = TipoRecoleccionSerializer(read_only=True)
    dia = DiaRecoleccionSerializer(read_only=True, many=True)
    turno = TurnoRecoleccionSerializer(read_only=True, many=True)
    barrio = BarrioRecoleccionSerializer(read_only=True, many=True)

    # observaciones_internas = serializers.SerializerMethodField()

    # Por si hay que eliminar el \n
    # def get_observaciones_internas(self, obj):
    #     if obj.observaciones_internas:
    #         return obj.observaciones_internas.replace('\n', ' ')

    class Meta:
        model = ZonaRecoleccion
        fields = ('id', 'empresa', 'tipos', 'dia', 'quincena', 'turno', 'barrio',
                  'nombre', 'numero_de_ruta', 'calles', 'observaciones_publicas')


cache_registry.register(EmpresaPrestatariaRecoleccionSerializer)
cache_registry.register(TipoRecoleccionSerializer)
cache_registry.register(TurnoRecoleccionSerializer)
cache_registry.register(DiaRecoleccionSerializer)
cache_registry.register(BarrioRecoleccionSerializer)
cache_registry.register(ZonaRecoleccionSerializer)
