from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import EmpresaPrestatariaRecoleccionSerializer, TipoRecoleccionSerializer, TurnoRecoleccionSerializer, BarrioRecoleccionSerializer, ZonaRecoleccionSerializer, DiaRecoleccionSerializer
from api.pagination import DefaultPagination
from recoleccion_de_residuos.models import EmpresaPrestatariaRecoleccion, TipoRecoleccion, TurnoRecoleccion, BarrioRecoleccion, ZonaRecoleccion, DiaRecoleccion


class EmpresaPrestatariaRecoleccionViewSet(viewsets.ModelViewSet):
    """
    Empresas de recoleccion de residuos con sus correspondientes id's
    """
    serializer_class = EmpresaPrestatariaRecoleccionSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = EmpresaPrestatariaRecoleccion.objects.all()
        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class TipoRecoleccionViewSet(viewsets.ModelViewSet):
    """
    Todos los diferentes tipos de recolecciones con sus id's
    """
    serializer_class = TipoRecoleccionSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # Obtiene todos los tipos correspondientes a obras de terceros
        queryset = TipoRecoleccion.objects.all()

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class TurnoRecoleccionViewSet(viewsets.ModelViewSet):
    """
    Diferentes turnos con sus horarios
    """
    serializer_class = TurnoRecoleccionSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = TurnoRecoleccion.objects.all()

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class DiaRecoleccionViewSet(viewsets.ModelViewSet):
    """
    Dias con sus id's
    """
    serializer_class = DiaRecoleccionSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = DiaRecoleccion.objects.all()

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class BarrioRecoleccionViewSet(viewsets.ModelViewSet):
    """
    Barrios con sus id's
    Se puede filtrar por barrios separado por comas. API/?barrios=bajo general paz, san jose
    """
    serializer_class = BarrioRecoleccionSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = BarrioRecoleccion.objects.all()

        barrios = self.request.query_params.get('barrios', None)
        if barrios is not None:
            queryset = queryset.filter(nombre__icontains=barrios)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ZonaRecoleccionViewSet(viewsets.ModelViewSet):
    """
    Se puede filtrar por empresa con su id. Ej: API/?empresa_id=2, 1
    Se puede filtrar por tipo de recoleccion con su id. Ej: API/?tipo_recoleccion_id=3, 5
    Se puede filtrar por turno de recoleccion con su id. Ej: API/?turno_recoleccion_id=1, 2
    Se puede filtrar por barrio con su ids. Ej: API/?barrio_id=8, 9, 25
    Se puede filtrar por barrio por su nombre. Ej: API/?barrio=alem
    Se puede filtrar por numero de ruta. Ej: API/?numero_ruta=15
    Los filtros se pueden combinar. Ej: API/?empresa=1&barrio=pueyrredon
    """
    serializer_class = ZonaRecoleccionSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = ZonaRecoleccion.objects.all()

        empresa = self.request.query_params.get('empresa_id', None)
        if empresa is not None:
            empresa = empresa.split(',')
            queryset = queryset.filter(empresa__id__in=empresa)

        tipo_recoleccion = self.request.query_params.get('tipo_recoleccion_id', None)
        if tipo_recoleccion is not None:
            tipo_recoleccion = tipo_recoleccion.split(',')
            queryset = queryset.filter(tipos__id__in=tipo_recoleccion)

        turno_recoleccion = self.request.query_params.get('turno_recoleccion_id', None)
        if turno_recoleccion is not None:
            turno_recoleccion = turno_recoleccion.split(',')
            queryset = queryset.filter(turno__id__in=turno_recoleccion)

        barrio = self.request.query_params.get('barrio_id', None)
        if barrio is not None:
            barrio = barrio.split(',')
            queryset = queryset.filter(barrio__id__in=barrio)

        barrio_nombre = self.request.query_params.get('barrio', None)
        if barrio_nombre is not None:
            queryset = queryset.filter(barrio__nombre__icontains=barrio_nombre)

        numero_ruta = self.request.query_params.get('numero_ruta', None)
        if numero_ruta is not None:
            queryset = queryset.filter(numero_de_ruta__icontains=numero_ruta)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
