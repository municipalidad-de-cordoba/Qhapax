from django.conf.urls import url, include
from rest_framework import routers
from .views import *


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'empresa-prestataria', EmpresaPrestatariaRecoleccionViewSet, base_name='empresa-recoleccion-de-residuos.api')
router.register(r'tipo-recoleccion', TipoRecoleccionViewSet, base_name='tipo-recoleccion-de-residuos.api')
router.register(r'turno-recoleccion', TurnoRecoleccionViewSet, base_name='turno-recoleccion-de-residuos.api')
router.register(r'dia-recoleccion', DiaRecoleccionViewSet, base_name='dia-recoleccion-de-residuos.api')
router.register(r'barrio-recoleccion', BarrioRecoleccionViewSet, base_name='barrio-recoleccion-de-residuos.api')
router.register(r'zona-recoleccion', ZonaRecoleccionViewSet, base_name='zona-recoleccion-de-residuos.api')


urlpatterns = [
    url(r'^', include(router.urls)),
]
