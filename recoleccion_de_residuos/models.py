from django.contrib.gis.db import models
from django.contrib.auth.models import User


class EmpresaPrestatariaRecoleccion(models.Model):
    nombre = models.CharField(max_length=90)
    observaciones_publicas = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class UsuarioEmpresaRecoleccion(models.Model):
    # Modelo para manejar lo que cada usuario deberia ver.
    usuario = models.OneToOneField(User, on_delete=models.SET_NULL,
                                    blank=True, null=True,
                                    verbose_name='Usuario de sistema')
    empresa = models.ForeignKey(EmpresaPrestatariaRecoleccion, related_name='UsuariosEmpresas')

    def __str__(self):
        return '{} - {}'.format(self.usuario, self.empresa)


class TipoRecoleccion(models.Model):
    nombre = models.CharField(max_length=90, help_text='Tipo de residuos a recolectar')
    observaciones_publicas = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class TurnoRecoleccion(models.Model):

    TURNO_MAÑANA = 10
    TURNO_TARDE = 20
    TURNO_NOCHE = 30

    turnos = (
        (TURNO_MAÑANA, 'Mañana'),
        (TURNO_TARDE, 'Tarde'),
        (TURNO_NOCHE, 'Noche')
    )

    turno = models.PositiveIntegerField(choices=turnos, default=TURNO_MAÑANA)
    desde = models.TimeField()
    hasta = models.TimeField()

    observaciones_publicas = models.TextField(null=True, blank=True)

    def __str__(self):
        return '{}: {} - {}'.format(self.get_turno_display(), self.desde, self.hasta)


class DiaRecoleccion(models.Model):
    dia = models.CharField(max_length=90)

    def __str__(self):
        return self.dia


class BarrioRecoleccion(models.Model):
    nombre = models.CharField(max_length=90)
    observaciones_publicas = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class ZonaRecoleccion(models.Model):

    QUINCE_ALL = 0
    QUINCE_PRIMERA = 10
    QUINCE_SEGUNDA = 20

    quincenas = (
            (QUINCE_ALL, 'Ambas'),
            (QUINCE_PRIMERA, 'Primer quincena'),
            (QUINCE_SEGUNDA, 'Segunda quincena')
    )

    empresa = models.ForeignKey(EmpresaPrestatariaRecoleccion)
    tipos = models.ForeignKey(TipoRecoleccion, null=True, blank=True)
    dia = models.ManyToManyField(DiaRecoleccion)
    quincena = models.PositiveIntegerField(choices=quincenas, default=QUINCE_ALL, null=True, blank=True)
    turno = models.ManyToManyField(TurnoRecoleccion)
    barrio = models.ManyToManyField(BarrioRecoleccion)
    nombre = models.CharField(max_length=90, null=True, blank=True, help_text='Ej: Zona 1')
    numero_de_ruta = models.CharField(max_length=20, help_text='Código único de la ruta dentro de la empresa')
    calles = models.TextField(null=True, blank=True)
    poligono = models.PolygonField(null=True, blank=True)
    observaciones_publicas = models.TextField(null=True, blank=True)
    observaciones_internas = models.TextField(null=True, blank=True)

    def __str__(self):
        return 'Empresa: {} - Tipo residuos: {} - N° ruta: {}'.format(self.empresa.nombre, self.tipos.nombre , self.numero_de_ruta)

    class Meta:
        unique_together = (("empresa", "tipos", "numero_de_ruta"),)
