from django.contrib import admin
from .models import *
from core.admin import QhapaxOSMGeoAdmin
from django.contrib.gis.db import models
from django.forms import CheckboxSelectMultiple
from .forms import BarrioRecoleccionForm
from django.http import Http404



@admin.register(EmpresaPrestatariaRecoleccion)
class EmpresaPrestatariaRecoleccionAdmin(admin.ModelAdmin):
    search_fields = ['nombre', 'observaciones_publicas']
    list_display = ['nombre', 'observaciones_publicas']


@admin.register(UsuarioEmpresaRecoleccion)
class UsuarioEmpresaRecoleccionAdmin(admin.ModelAdmin):
    list_display = ['usuario', 'empresa']


@admin.register(TipoRecoleccion)
class TipoRecoleccionAdmin(admin.ModelAdmin):
    search_fields = ['nombre', 'observaciones_publicas']
    list_display = ['nombre', 'observaciones_publicas']


@admin.register(BarrioRecoleccion)
class BarrioRecoleccionAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    list_display = ['nombre']


@admin.register(TurnoRecoleccion)
class TurnoRecoleccionAdmin(admin.ModelAdmin):
    list_display = ['turno', 'desde', 'hasta', 'observaciones_publicas']
    fields = (('desde', 'hasta'), 'turno', 'observaciones_publicas')
    list_filter = ['turno', 'desde', 'hasta', 'observaciones_publicas']


@admin.register(DiaRecoleccion)
class DiaRecoleccionAdmin(admin.ModelAdmin):
    list_display = ['dia']


@admin.register(ZonaRecoleccion)
class ZonaRecoleccionAdmin(QhapaxOSMGeoAdmin):
    form = BarrioRecoleccionForm
    search_fields = ['nombre', 'numero_de_ruta', 'barrio__nombre', 'observaciones_publicas']
    list_display = ['empresa', 'tipos', 'numero_de_ruta', 'observaciones_internas' , 'observaciones_publicas']
    list_filter = ['empresa', 'turno', 'tipos']

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "empresa":
            if not request.user.is_superuser:
                try:
                    kwargs["queryset"] = EmpresaPrestatariaRecoleccion.objects.filter(nombre=request.user.usuarioempresarecoleccion.empresa)
                except Exception:
                    raise Http404("Usted no fue asignado a la empresa correspondiente")
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if not request.user.is_superuser:
            try:
                qs = qs.filter(empresa=request.user.usuarioempresarecoleccion.empresa)
            except Exception:
                raise Http404("Usted no fue asignado a la empresa correspondiente")
        return qs

    # para que ande el select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )
