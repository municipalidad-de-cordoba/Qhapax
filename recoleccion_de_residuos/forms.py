from django_select2.forms import Select2MultipleWidget
from .models import BarrioRecoleccion, ZonaRecoleccion
from django import forms


class BarrioRecoleccionWidget(Select2MultipleWidget):
    model = BarrioRecoleccion
    search_fields = ['nombre__icontains']
    max_results = 5

    # def label_from_instance(self, obj):
    #     return '{}'.format(obj.nombre)

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault('data-placeholder', 'Ingrese nombre del barrio')
        self.attrs.setdefault('data-minimum-input-length', 3)
        self.attrs.setdefault('data-width', '25em')

        return super(BarrioRecoleccionWidget, self).build_attrs(*args, **kwargs)


class BarrioRecoleccionForm(forms.ModelForm):

    class Meta:
        model = ZonaRecoleccion
        fields = '__all__'
        widgets = {
                    'barrio': BarrioRecoleccionWidget
                    }
