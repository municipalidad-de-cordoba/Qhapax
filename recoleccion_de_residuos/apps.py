from django.apps import AppConfig


class RecoleccionDeResiduosConfig(AppConfig):
    name = 'recoleccion_de_residuos'
