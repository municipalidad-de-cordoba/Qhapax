Instalación usando Docker
=========================

Si docker no está instalado/actualizado
---------------------------------------

Como ubuntu puede tener una version antigua se requiere instalar una más nueva
Extraido desde:
 - `Instalar docker <https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/>`_.  
 - `Post instalation <https://docs.docker.com/engine/installation/linux/linux-postinstall/#manage-docker-as-a-non-root-user>`_.  

.. code:: bash 

  sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo apt-key fingerprint 0EBFCD88
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  sudo apt-get update
  sudo apt-get install docker-ce
  sudo groupadd docker
  sudo usermod -aG docker $USER
  sudo -i
  curl -L https://github.com/docker/compose/releases/download/1.15.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
  sudo chown "$USER":"$USER" /home/"$USER"/.docker -R 
  sudo chmod +x /usr/local/bin/docker-compose

Una vez docker funcionando
--------------------------

.. code:: bash 

  git clone https://github.com/avdata99/Qhapax
  
  Dentro del directorio Qhapax(si el comando docker pide ser root, necesitará reiniciar su equipo o correr los siguientes comandos con sudo delante):
  
  docker-compose build
  docker-compose up
  docker-compose run web python3 manage.py migrate
  docker-compose run web python3 manage.py loaddata core/initial_data/*


Los últimos dos comandos deben correrse en otra terminal luego del docker-compose up o finalizando el mismo una vez terminado de correr.
