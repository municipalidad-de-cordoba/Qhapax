from django.db import models
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.utils.text import slugify


class GoogleUser(models.Model):
    ''' usuarios de google logueados '''
    google_id = models.CharField(max_length=60)
    nombre = models.CharField(max_length=160, null=True, blank=True)
    url_imagen = models.CharField(max_length=360, null=True, blank=True)
    email = models.EmailField()
    email_verified = models.NullBooleanField(
        default=None, null=True, blank=True)
    given_name = models.CharField(
        max_length=120,
        null=True,
        blank=True,
        help_text='Nombre de pila')
    family_name = models.CharField(
        max_length=120,
        null=True,
        blank=True,
        help_text='Apellido')
    locale = models.CharField(
        max_length=20,
        help_text='Idioma del usuario',
        null=True,
        blank=True)

    user = models.OneToOneField(User, null=True, blank=True)

    def get_unused_username(self):
        usuario = None
        sufijos = [''] + list(range(2, 400))
        for sufijo in sufijos:
            nombre = self.nombre
            if nombre is None and self.email is not None:
                nombre = self.email.split('@')[0]

            test = '{}{}'.format(nombre, sufijo)
            usuario = slugify(test).replace('-', '_')
            usuarios = User.objects.filter(username=usuario)
            if len(usuarios) == 0:
                return usuario
        raise ValueError('AL HORNO')

    def save(self, *args, **kwargs):
        if self.user is None:
            # ver si el email ya esta. Si es así (y esta verificado por google)
            # se "roba" el usuario que ya estaba
            email = self.email
            users = User.objects.filter(email=email)
            if users.count() > 0 and self.email_verified:
                self.user = users[0]
            else:
                usuario = self.get_unused_username()
                nombre = 'S/N' if self.given_name is None else self.given_name
                apellido = '' if self.family_name is None else self.family_name
                self.user = User.objects.create(is_staff=False,
                                                is_superuser=False,
                                                is_active=True,
                                                username=usuario,
                                                first_name=nombre,
                                                last_name=apellido,
                                                email=email)

            token = self.get_token()

        super().save(*args, **kwargs)

    def get_token(self):
        tokens = Token.objects.filter(user=self.user)
        if len(tokens) == 0:
            token = Token.objects.create(user=self.user)
        else:
            token = tokens[0]
        return token.key

    def __str__(self):
        return self.email
