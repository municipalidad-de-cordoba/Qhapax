from django.contrib import admin
from .models import GoogleUser


@admin.register(GoogleUser)
class GoogleUserAdmin(admin.ModelAdmin):

    def qhapax_username(self, obj):
        return obj.user.username

    list_display = [
        'google_id',
        'qhapax_username',
        'nombre',
        'email',
        'email_verified',
        'given_name',
        'family_name',
        'locale']
    search_fields = [
        'google_id',
        'nombre',
        'email',
        'given_name',
        'family_name',
        'locale']
    list_filter = ['locale', 'email_verified']
