from django.conf.urls import url
from django.contrib.auth.views import login
from django.contrib.auth.views import logout
from django.contrib.auth.views import logout_then_login
from django.contrib.auth.views import password_change
from django.contrib.auth.views import password_change_done
from django.contrib.auth.views import password_reset
from django.contrib.auth.views import password_reset_complete
from django.contrib.auth.views import password_reset_confirm
from django.contrib.auth.views import password_reset_done
from . import views
from website import views as qhapax_index

urlpatterns = [

    # Cuando se registran/loguean van al index del portal de Gobierno Abierto
    url(r'^$', qhapax_index.go_index, name='login.go_index'),
    url(r'^$', qhapax_index.index, name='login.index'),
    url(r'^register/$', views.register, name='login.register'),

    # login logout
    url(r'^login/$', login, name='login.login'),
    url(r'^logout/$', logout, name='login.logout'),
    url(r'^logout-then-login/$', logout_then_login,
        name='login.logout_then_login'),

    # change password
    url(r'^password-change/$', password_change, name='login.password_change'),
    url(r'^password-change/done/$', password_change_done,
        name='login.password_change_done'),

    # reset password
    # restore password urls
    url(r'^password-reset/$',
        password_reset,
        name='login.password_reset'),
    url(r'^password-reset/done/$',
        password_reset_done,
        name='login.password_reset_done'),
    url(r'^password-reset/confirm/(?P<uidb64>[-\w]+)/(?P<token>[-\w]+)/$',
        password_reset_confirm,
        name='login.password_reset_confirm'),
    url(r'^password-reset/complete/$',
        password_reset_complete,
        name='login.password_reset_complete'),
]
