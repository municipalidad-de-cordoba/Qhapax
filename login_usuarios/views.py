from django.shortcuts import render
from allauth.socialaccount.providers.twitter.views import TwitterOAuthAdapter
from rest_auth.registration.views import SocialLoginView, SocialConnectView
from rest_auth.social_serializers import (
    TwitterLoginSerializer, TwitterConnectSerializer,
    # GithubLoginSerializer, GithubConnectSerializer,
    # GoogleLoginSerializer, GoogleConnectSerializer
)

# from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter

from .forms import UserRegistrationForm


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            # Create a new user object but avoid saving it yet
            new_user = user_form.save(commit=False)
            # Set the chosen password
            new_user.set_password(
                user_form.cleaned_data['password'])
            # Save the User object
            new_user.save()
            return render(request,
                          'account/register_done.html',
                          {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request,
                  'account/register.html',
                  {'user_form': user_form})


class TwitterLogin(SocialLoginView):
    serializer_class = TwitterLoginSerializer
    adapter_class = TwitterOAuthAdapter


class TwitterConnect(SocialConnectView):
    serializer_class = TwitterConnectSerializer
    adapter_class = TwitterOAuthAdapter

# class GithubLogin(SocialLoginView):
#     serializer_class = GithubLoginSerializer
#     adapter_class = GithubOAuthAdapter

# class GithubConnect(SocialConnectView):
#     serializer_class = GithubConnectSerializer
#     adapter_class = GithubOAuthAdapter

# class GoogleLogin(SocialLoginView):
#     serializer_class = GoogleLoginSerializer
#     adapter_class = GoogleOAuthAdapter

# class GoogleConnect(SocialConnectView):
#     serializer_class = GoogleConnectSerializer
#     adapter_class = GoogleOAuthAdapter

# class FacebookConnect(SocialConnectView):
#     adapter_class = FacebookOAuth2Adapter
