from rest_framework.routers import DefaultRouter
from .views import GoogleUserViewSet

router = DefaultRouter()
router.register(r'google-users', GoogleUserViewSet, base_name='googleusers')

urlpatterns = router.urls
