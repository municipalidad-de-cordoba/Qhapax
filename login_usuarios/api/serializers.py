from rest_framework import serializers
from login_usuarios.models import GoogleUser


class GoogleUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = GoogleUser
        fields = ('id', 'google_id', 'nombre', 'url_imagen', 'email')
