from rest_framework import viewsets
from rest_framework.permissions import AllowAny, DjangoModelPermissions
from api.pagination import DefaultPagination
from .serializers import GoogleUserSerializer
from login_usuarios.models import GoogleUser
from rest_framework.response import Response
from google.oauth2 import id_token
from google.auth.transport import requests
from rest_framework.decorators import action
from rest_framework import status
from django.conf import settings
import logging
logger = logging.getLogger(__name__)


class GoogleUserViewSet(viewsets.ModelViewSet):
    serializer_class = GoogleUserSerializer
    permission_classes = [DjangoModelPermissions]
    pagination_class = DefaultPagination

    def list(self, request):
        pass

    def create(self, request):
        pass

    def retrieve(self, request, pk=None):
        pass

    @action(methods=['post'], detail=False, permission_classes=[AllowAny])
    def create_from_google_token(self, request):
        ''' metodo especial para crear un usuario que acaba de loguarse con Google '''
        data = request.data
        # preguntarle a google si el token esta OK
        # no se puede confiar en los otros campos, podrían ser fake
        token = data.get('id_token', None)
        if token is None:
            res = {
                'ok': False,
                'error': 'Falta el parámetro id_token en data: {}'.format(data)}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        origen = data.get('origen', 'android')  # android | web | ios

        if origen == 'android':
            google_login_client_id = settings.APP_GO_CLIENT_ID_ANDROID
        elif origen == 'ios':
            google_login_client_id = settings.APP_GO_CLIENT_ID_IOS
        elif origen == 'web':
            google_login_client_id = settings.APP_GO_CLIENT_ID_WEB
        else:
            res = {
                'ok': False,
                'error': 'Origen no válido (use ios | android | web). Params: {}'.format(data)}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        # sale de acá:
        # https://developers.google.com/identity/sign-in/web/backend-auth
        try:
            # Specify the CLIENT_ID of the app that accesses the backend:
            idinfo = id_token.verify_oauth2_token(
                token, requests.Request(), google_login_client_id)
            logger.info('google_user_valid: {}'.format(idinfo))

            # Or, if multiple clients access the backend server:
            # idinfo = id_token.verify_oauth2_token(token, requests.Request())
            # if idinfo['aud'] not in [CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]:
            #     raise ValueError('Could not verify audience.')

            if idinfo['iss'] not in [
                'accounts.google.com',
                    'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
                res = {'ok': False, 'error': 'Issuer Erroneo'}
                return Response(res, status=status.HTTP_400_BAD_REQUEST)

            # If auth request is from a G Suite domain:
            # if idinfo['hd'] != GSUITE_DOMAIN_NAME:
            #     raise ValueError('Wrong hosted domain.')

            # ID token is valid. Get the user's Google Account ID from the
            # decoded token.
            userid = idinfo['sub']

            """ resultado esperado en idnifo
            // These six fields are included in all Google ID Tokens.
            "iss": "https://accounts.google.com",
            "sub": "110169484474386276334", -> id único en google
            "azp": "1008719970978-hb24n2dstb40o45d4feuo2ukqmcc6381.apps.googleusercontent.com",
            "aud": "1008719970978-hb24n2dstb40o45d4feuo2ukqmcc6381.apps.googleusercontent.com",
            "iat": "1433978353",  -> supongo que es el tiempo de inicio
            "exp": "1433981953",  -> EXPIRATION TIME

            // These seven fields are only included when the user has granted the "profile" and
            // "email" OAuth scopes to the application.
            "email": "testuser@gmail.com",
            "email_verified": "true",
            "name" : "Test User",
            "picture": "https://lh4.googleusercontent.com/-kYgzyAWpZzJ/ABCDEFGHI/AAAJKLMNOP/tIXL9Ir44LE/s99-c/photo.jpg",
            "given_name": "Test",
            "family_name": "User",
            "locale": "en"
            """
        except ValueError as e:
            # Invalid token
            res = {
                'ok': False,
                'error': 'Token de Google inválido: {}'.format(e),
                'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        # google_user_valid: {'iss': 'accounts.google.com',
        #                       'azp': '613304297473-0e12is1kcsv1j9en3alavpp8ttjmc8p6.apps.googleusercontent.com',
        #                       'aud': '613304297473-0e12is1kcsv1j9en3alavpp8ttjmc8p6.apps.googleusercontent.com',
        #                       'sub': '100530708543483213226',
        #                       'hd': 'data99.com.ar',
        #                       'email': 'andres@data99.com.ar',
        #                       'email_verified': True,
        #                       'at_hash': 'aM-eTFJdHDAvAoSbHDW6GQ',
        #                       'name': 'Andres Vazquez',
        #                       'picture': 'https://lh4.googleusercontent.com/-aFeZcz2OtsA/AAAAAAAAAAI/AAAAAAAAdlY/Ki1fX3AodSA/s96-c/photo.jpg',
        #                       'given_name': 'Andres',
        #                       'family_name': 'Vazquez',
        #                       'locale': 'es-419',
        #                       'iat': 1539655742,
        #                       'exp': 1539659342,
        #                       'jti': 'fe54c9b37befa3b486525dda97d099fcfc4ed4dc'}

        google_user, google_user_created = GoogleUser.objects.get_or_create(
            google_id=idinfo['sub'])
        google_user.nombre = idinfo.get('name', None)
        google_user.url_imagen = idinfo.get('picture', None)
        google_user.email = idinfo.get('email', None)

        ev = idinfo.get('email_verified', None)
        if ev is not None and not isinstance(
                ev, bool):  # ver si lo entiende como booleano
            ev = True if ev.lower() == 'true' else False
        google_user.email_verified = ev

        google_user.given_name = idinfo.get('given_name', None)
        google_user.family_name = idinfo.get('family_name', None)
        google_user.locale = idinfo.get('locale', None)

        google_user.save()

        res = {'ok': True,
               'user_id': google_user.user.id,
               'username': google_user.user.username,
               'token': google_user.get_token(),
               }
        return Response(res, status=status.HTTP_201_CREATED)
