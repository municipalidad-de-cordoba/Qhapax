from django.apps import AppConfig


class TransportepublicoConfig(AppConfig):
    name = 'transportepublico'
