from django.contrib import admin
from django.core.urlresolvers import reverse
from .models import *
from core.admin import QhapaxOSMGeoAdmin
from .forms import VehiculoFormSelect2


class EmpresaTransportePublicoAdmin(admin.ModelAdmin):
    search_fields = ['id_externo', 'nombre_publico']
    list_display = ('id_externo', 'nombre_publico')
    exclude = ['organizacion']


class LineaTransportePublicoAdmin(admin.ModelAdmin):
    def empresap(self, obj):
        return obj.empresa.nombre_publico

    search_fields = ['nombre_publico', 'id_externo']
    list_display = ('id_externo', 'nombre_publico', 'empresap', 'troncal', 'fuente', 'publicado')
    list_filter = ['publicado', 'fuente', 'troncal']


class TroncalTransportePublicoAdmin(admin.ModelAdmin):
    search_fields = ['nombre_publico']
    list_display = ('id', 'nombre_publico')


class PosteParadaTransportePublicoAdmin(QhapaxOSMGeoAdmin):
    search_fields = ['id_externo', 'descripcion', 'calle_principal', 'calle_secundaria']
    list_display = ('id_externo', 'fuente', 'descripcion', 'sentido', 'calle_principal', 'calle_secundaria')
    list_filter = ['fuente']


class ParadaTransportePublicoAdmin(QhapaxOSMGeoAdmin):
    search_fields = ['poste__descripcion', 'linea__nombre_publico',
                        'recorrido__descripcion_corta',
                        'recorrido__descripcion_larga',
                        'recorrido__descripcion_interna']
    list_display = ('poste', 'linea', 'recorrido', 'fuente', 'distancia_a_recorrido', 'verificado')
    list_filter = ['linea', 'fuente', 'recorrido', 'verificado']


class RecorridoLineaTransportePublicoAdmin(QhapaxOSMGeoAdmin):

    def kilometros(self, obj):
        if obj.trazado is None:
            return 0.0
        linea = obj.trazado
        linea.transform(3857)
        return round(linea.length / 1000, 2)  # no estoy 100% seguro de esto

    search_fields = ['id', 'descripcion_corta', 'descripcion_larga', 'linea__nombre_publico', 'origen__nombre', 'destino__nombre']
    list_display = ('id', 'linea', 'kilometros', 'fuente', 'descripcion_corta', 'descripcion_larga', 'publicado', 'origen', 'destino', 'descripcion_interna')
    list_filter = ['fuente', 'linea', 'publicado', 'origen', 'destino']


class MarcaVehiculoAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    list_display = ('id', 'nombre')
    # list_filter = []


class ModeloVehiculoAdmin(admin.ModelAdmin):
    search_fields = ['marca__nombre', 'nombre']
    list_display = ('id', 'marca', 'nombre')
    list_filter = ['marca']


class TipoServicioTransporteAdmin(admin.ModelAdmin):
    search_fields = ['nombre', 'descripcion']
    list_display = ('id', 'nombre', 'descripcion')
    # list_filter = []


class VehiculoTransportePublicoMasivoAdmin(admin.ModelAdmin):
    list_display = ['interno', 'dominio', 'modelo_anio', 'activo', 'chasis_marca',
                    'chasis_numero', 'motor_numero', 'tanque_litros', 'asientos',
                    'hay_que_revisar', 'empresa', 'observaciones', 'fecha_alta', 'fecha_baja']
    list_filter = ['empresa', 'hay_que_revisar', 'activo']
    search_fields = ['interno', 'dominio', 'chasis_marca',
                    'chasis_numero', 'motor_numero']


class VehiculoTransportePublicoAdmin(admin.ModelAdmin):
    form = VehiculoFormSelect2
    search_fields = ['interno', 'patente', 'titular__nombre', 'titular__CUIT']
    list_display = ('id', 'interno', 'modelo', 'anio_fabricacion', 'publicado', 'tipo_permiso', 'patente', 'titular')
    list_filter = ['publicado', 'tipo_permiso', 'anio_fabricacion', 'modelo', 'modelo__marca', 'observado_o_a_revisar', 'condicion']

    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )
'''
class HabilitacionVehiculoTransporteAdmin(admin.ModelAdmin):
    search_fields = ['vehiculo__patente']
    list_display = ('vehiculo', 'fecha_inicio_habilitacion', 'fecha_vencimiento_habilitacion')
    list_filter = ['vehiculo']
'''

class FrecuenciaAdmin(admin.ModelAdmin):
    list_editable = ('frecuencia_en_minutos_esperada',)
    list_display = ['estacion', 'agrupador', 'linea', 'frecuencia_en_minutos_esperada',
        'unidades_activas_esperadas', 'descripcion']
    search_fields = ['descripcion']
    list_filter = ['estacion', 'agrupador', 'linea__nombre_publico']


class DiasGrupoFrecuenciasAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'lunes', 'martes', 'miercoles', 'jueves',
        'viernes', 'sabado', 'domingo', 'feriado']


class AgrupadorFranjaHorariaAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'dias']
    list_filter = ['nombre', 'dias']


class FranjaHorariaFrecuenciasAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'hora_desde', 'hora_hasta', 'agrupador']
    list_filter = ['nombre', 'agrupador']


admin.site.register(EmpresaTransportePublico, EmpresaTransportePublicoAdmin)
admin.site.register(LineaTransportePublico, LineaTransportePublicoAdmin)
admin.site.register(TroncalTransportePublico, TroncalTransportePublicoAdmin)
admin.site.register(PosteParadaTransportePublico, PosteParadaTransportePublicoAdmin)
admin.site.register(ParadaTransportePublico, ParadaTransportePublicoAdmin)
admin.site.register(RecorridoLineaTransportePublico, RecorridoLineaTransportePublicoAdmin)
admin.site.register(MarcaVehiculo, MarcaVehiculoAdmin)
admin.site.register(ModeloVehiculo, ModeloVehiculoAdmin)
admin.site.register(TipoServicioTransporte, TipoServicioTransporteAdmin)
admin.site.register(VehiculoTransportePublico, VehiculoTransportePublicoAdmin)
admin.site.register(TipoCombustible)
admin.site.register(TipoAdaptado)
# admin.site.register(HabilitacionVehiculoTransporte, HabilitacionVehiculoTransporteAdmin)
admin.site.register(VehiculoTransportePublicoMasivo, VehiculoTransportePublicoMasivoAdmin)
admin.site.register(Frecuencia, FrecuenciaAdmin)
admin.site.register(FranjaHorariaFrecuencias, FranjaHorariaFrecuenciasAdmin)
admin.site.register(DiasGrupoFrecuencias, DiasGrupoFrecuenciasAdmin)
admin.site.register(EstacionAnioFrecuencias)
admin.site.register(AgrupadorFranjaHoraria, AgrupadorFranjaHorariaAdmin)
