from django.shortcuts import render
from django.views.decorators.cache import cache_page
from .models import PosteParadaTransportePublico, ParadaTransportePublico, VehiculoTransportePublico, RecorridoLineaTransportePublico
import django_excel as excel
from transportepublico import settings as settings_tp
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required, login_required


@cache_page(60 * 60)  # 1 hora
def lista_paradas(request, filetype):
    '''
    lista de paradas de transporte urbano de pasajeros
    '''
    paradas = ParadaTransportePublico.objects.all()
    paradas.order_by('id')

    csv_list = []
    csv_list.append(['Empresa', 'Linea', 'Poste', 'descripcion',
                        'sentido', 'calle principal', 'calle secundaria',
                        'latitud', 'longitud'])
    for parada in paradas:
        poste = parada.poste
        linea = parada.linea
        csv_list.append([linea.empresa.nombre_publico,
                            linea.nombre_publico,
                            poste.id_externo,
                            poste.descripcion,
                            poste.sentido,
                            poste.calle_principal,
                            poste.calle_secundaria,
                            poste.ubicacion.coords[1],
                            poste.ubicacion.coords[0]])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60)  # 1 hora
def lista_recorridos_tup(request, filetype):
    '''
    lista de recorridos del transporte urbano de pasajeros
    '''
    recorridos = RecorridoLineaTransportePublico.objects.filter(publicado=True, linea__publicado=True).order_by('linea__empresa', 'linea')
    
    csv_list = []
    csv_list.append(['Empresa', 'Linea', 'ID Recorrido', 'descripcion corta', 'descripcion larga'])
    for recorrido in recorridos:
        csv_list.append([recorrido.linea.empresa.nombre_publico, recorrido.linea.nombre_publico,
                            recorrido.id_externo, recorrido.descripcion_corta, recorrido.descripcion_larga])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60)  # 1 hora
def lista_transportes_escolares(request, filetype):
    '''
    lista de paradas de transporte
    '''
    ids = [transporte_escolar.id for transporte_escolar in VehiculoTransportePublico.objects.filter(publicado=True) if transporte_escolar.habilitado()]
    transportes = VehiculoTransportePublico.objects.filter(id__in=ids)
    transportes = transportes.filter(tipo_permiso__id=settings_tp.TIPO_TRANSPORTE_ESCOLAR_ID)
    transportes.order_by('titular__nombre')

    csv_list = []
    csv_list.append(['Titular', 'CUIT', 'Patente', 'Marca', 'Modelo', 'Año'])
    for transporte in transportes:
        titular = None if transporte.titular is None else transporte.titular
        titular_nombre = 'DESCONOCIDO' if titular is None else titular.nombre
        titular_CUIT = 'DESCONOCIDO' if titular is None else titular.CUIT
        
        modelo = 'DESCONOCIDO' if transporte.modelo is None else transporte.modelo.nombre
        marca = 'DESCONOCIDO' if (transporte.modelo is None or transporte.modelo.marca is None) else transporte.modelo.marca.nombre
        
        csv_list.append([titular_nombre,
                            titular_CUIT,
                            transporte.patente,
                            marca,
                            modelo,
                            transporte.anio_fabricacion])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60)  # 1 hora
def lista_taxis(request, filetype):
    '''
    lista de taxis habilitados
    '''
    transportes = VehiculoTransportePublico.objects.filter(
                            publicado=True,
                            tipo_permiso__id=settings_tp.TIPO_TRANSPORTE_TAXI_ID,
                            condicion=VehiculoTransportePublico.COND_HABILITADO)

    transportes.order_by('titular__nombre')

    csv_list = []
    csv_list.append(['Titular', 'CUIT', 'Patente', 'Marca', 'Modelo', 'Año', 'estado'])
    for transporte in transportes:
        titular = None if transporte.titular is None else transporte.titular
        titular_nombre = 'DESCONOCIDO' if titular is None else titular.nombre
        titular_CUIT = 'DESCONOCIDO' if titular is None else titular.CUIT
        
        modelo = 'DESCONOCIDO' if transporte.modelo is None else transporte.modelo.nombre
        marca = 'DESCONOCIDO' if (transporte.modelo is None or transporte.modelo.marca is None) else transporte.modelo.marca.nombre
        
        csv_list.append([titular_nombre,
                            titular_CUIT,
                            transporte.patente,
                            marca,
                            modelo,
                            transporte.anio_fabricacion,
                            transporte.get_condicion_display()]
                            )

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60)  # 1 hora
def lista_remises(request, filetype):
    '''
    lista de remises habilitados
    '''
    transportes = VehiculoTransportePublico.objects.filter(
                            publicado=True,
                            tipo_permiso__id=settings_tp.TIPO_TRANSPORTE_REMIS_ID,
                            condicion=VehiculoTransportePublico.COND_HABILITADO)

    transportes.order_by('titular__nombre')

    csv_list = []
    csv_list.append(['Titular', 'CUIT', 'Patente', 'Marca', 'Modelo', 'Año', 'estado'])
    for transporte in transportes:
        titular = None if transporte.titular is None else transporte.titular
        titular_nombre = 'DESCONOCIDO' if titular is None else titular.nombre
        titular_CUIT = 'DESCONOCIDO' if titular is None else titular.CUIT
        
        modelo = 'DESCONOCIDO' if transporte.modelo is None else transporte.modelo.nombre
        marca = 'DESCONOCIDO' if (transporte.modelo is None or transporte.modelo.marca is None) else transporte.modelo.marca.nombre
        
        csv_list.append([titular_nombre,
                            titular_CUIT,
                            transporte.patente,
                            marca,
                            modelo,
                            transporte.anio_fabricacion,
                            transporte.get_condicion_display()]
                            )

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)



class MapParadasView(TemplateView):
    template_name = 'transportepublico/editar-bondis-en-mapa.html'

    @method_decorator(login_required)
    @method_decorator(permission_required('transportepublico.map_editor'))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['recorridos'] = RecorridoLineaTransportePublico.objects.all().order_by('linea__nombre_publico', 'descripcion_corta')
        return context