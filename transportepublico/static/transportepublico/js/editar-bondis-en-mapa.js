var url_portal = '';  // 'https://gobiernoabierto.cordoba.gob.ar';
var url_api_recorridos = '/api/v2/transporte-publico/recorridos-colectivos/';
var url_api_paradas = '/api/v2/transporte-publico/paradas-colectivos/';
var url_api_postes = '/api/v2/transporte-publico/postes-paradas-colectivos/';

var recorridos = [];
var paradas = [];
var recorrido = null;  // el recorrido actual elegido

'use strict';
$(document).ready(function() {
    // dibujar el recorrido y las paradas de el y de las otras líneas (de colores ditintos)

    let csrftoken = $("[name=csrfmiddlewaretoken]").val();  // se carga en el HTML con {% csrf_token %}

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);      
        }
        });
        
    load_recorridos();
    $('#select_filtro_recorridos').change(function(){
        let id_recorrido = $('#select_filtro_recorridos option:selected').val();
        clean_map();
        load_recorrido(id_recorrido);
        load_paradas(id_recorrido);
    });
});


function clean_map() {
    map.data.forEach(function (feature) {
        map.data.remove(feature);
    });
}

function load_recorridos(id_recorrido) {
    let url = url_portal + url_api_recorridos + '?page_size=600';
    let xhr = $.getJSON(url);
    xhr.done(function(){
        recorridos = xhr.responseJSON.results;

        for (i=0; i<recorridos.features.length; i++) {
            let este = recorridos.features[i];
            linea = este.properties.linea;
            recorrido = este.properties.descripcion_corta + ' (' + este.properties.descripcion_cuando_llega + ')';
        
            let op = '<option value="' + este.id + '">' + linea.nombre_publico + ' ' + recorrido + '</option>';
            $('#select_filtro_recorridos').append(op);
            
        }

    });

    

}

function load_recorrido(id_recorrido) {
    let url = url_portal + url_api_recorridos + id_recorrido;
    let xhr = $.getJSON(url);
    xhr.done(function(){
        recorrido = xhr.responseJSON;
        features = map.data.addGeoJson(recorrido);  // si fueran varios es .results

        for (i=0; i < features.length; i++) {
            features[i].setProperty('tipo', 'recorrido');
        }
        
    });

    

}

function load_paradas(id_recorrido) {
    let url = url_portal + url_api_paradas + '?recorrido_id=' + id_recorrido + '&page_size=1600';
    let xhr = $.getJSON(url);
    xhr.done(function(){
        paradas = xhr.responseJSON.results;
        map.data.addGeoJson(paradas);

        map.data.setStyle(function(feature) {
            if (feature.getProperty('tipo') == 'recorrido') {
                return { strokeColor: 'blue', strokeWeight: 4};
            } else {
                feature.setProperty('tipo', 'poste');
                /*
                "properties": {
                    "linea": { "id": 1, "id_externo": 1278, "nombre_publico": "10", "empresa": "", "id_empresa": "1" },
                    "poste": { "id": 1, "id_externo": "C0001", "descripcion": "C0001", "sentido": "NORTE", "calle_principal": "AVENIDA VUCETICH", "calle_secundaria": "SCHRODINGER"}
                */
                linea = feature.getProperty('linea');
                poste = feature.getProperty('poste');
                
                let id_propio = (poste == undefined) ? "SIN ID" : poste.id;
                let id_externo = (poste == undefined) ? "SIN ID" : poste.id_externo;
                let descripcion = (poste == undefined) ? "" : poste.descripcion;
                
                let sentido = (poste == undefined) ? "" : poste.sentido;
                let calle_principal = (poste == undefined) ? "" : poste.calle_principal;
                let calle_secundaria = (poste == undefined) ? "" : poste.calle_secundaria;
                
                title = 'Linea: ' + linea.nombre_publico + linea.empresa +
                        ' Poste: ' + id_externo + '(' + descripcion + ')' + ', sentido ' + sentido + 
                        ', calle principal: ' + calle_principal + ', calle secundaria: ' + calle_secundaria;
    
                chst='d_bubble_text_small';
                
                // ver si tienen asignadas líneas o recorridos
                // esperamos que tengan recorridos, a los que les falte, avisarlos
                let paradas = (poste == undefined) ? [] : poste.paradas;
                let faltantes = 0;  // cuantas paradas estan asignadas a recorridos
                let oks = 0;  // parsadas ya asignadas a recorrido
                for (i=0; i<paradas.length; i++) {
                    let parada = paradas[i];
                    if (parada.recorrido == null) {
                        faltantes += 1;
                    } else {
                        oks += 1;
                    }
                }
                let txt = id_propio + ' (' + oks + '/' + paradas.length + ')';
                if (faltantes < 10) {
                    color = '0' + faltantes + 'DDDD';
                } else if (faltantes > 9) {
                    color = faltantes + 'DDDD';
                } else {
                    color = '0066AA';
                }
                
                // https://developers.google.com/maps/documentation/javascript/datalayer
                icon='https://chart.googleapis.com/chart?chst=' + chst +'&chld=bb|' + txt + '|' + color + '|000000';
                visible = true;
    
                return { icon: icon, strokeWeight: 1, title: title, visible: visible, draggable: true};
                }
        });
    });

    

}