// manejador de paradas
var url_api_poste_con_recorridos = '/api/v2/transporte-publico/postes-con-paradas/';  // lleva el ID al final
var editar_paradas_url = static_folder + 'html/parada-editar.html';

'use strict';
$(document).ready(function() {

    clickPoste = function(poste) {
        /* POSTE ES 
        linea: empresa:"lisa", id:10, id_empresa:"1", id_externo:1287, nombre_publico:"19"
        poste: calle_principal:"AV VALPA", calle_secundaria:"Cs ECON", descripcion:"C4222", id:412, id_externo:"C4222", sentido:"SIN SENTIDO"
        tipo:"poste" */
        // traer datos del poste
        
        let url = url_api_poste_con_recorridos + poste.poste.id + '/';
        let xhr = $.getJSON(url);
        var poste_full = null;
        xhr.done(function(){
            poste_full = xhr.responseJSON;
            /* "id": 1926, "id_externo": "C6061", "descripcion": "C6061", "sentido": "SIN SENTIDO", "calle_principal": "MACKAY GORDON","calle_secundaria": "FRAY MIGUEL DE MOJICA",
               "paradas": [ {  "id": 3450,
                            "linea": { "id": 23, "id_externo": 1311, "nombre_publico": "32", "empresa": "bart", "id_empresa": "2"},
                            "recorrido": null,
                            "poste": 1926},
                            { "id": 3903,
                            "linea": { "id": 27, "id_externo": 1315, "nombre_publico": "36", "empresa": "bart", "id_empresa": "2"},
                            "recorrido": null,
                            "poste": 1926 } ] */
        
        $("#div_editar_parada").load(editar_paradas_url, function(response, status, xhr) {

            let url_editar = url_editar_poste.replace('/0', '/' + poste_full.id);
            let editar = '<a target="_blank" href="' + url_editar + '">[Editar]</a>';
            $('#idtitulo').html('POSTE ' + poste_full.id_externo + ' (' + poste_full.id + ') ' + editar);

            $("#div_editar_parada").modal();

            $lip = $('#lista_de_paradas');
            let paradas = poste_full.paradas;
            for (i=0; i<paradas.length; i++) {
                let parada = paradas[i];
                let recorrido = parada.recorrido;
                let descr_nueva = (recorrido == null) ? 'Sin descripcion nueva' : recorrido.descripcion_cuando_llega;
                let nombre = 'Recorrido ' + recorrido.descripcion_corta + ' (' + descr_nueva + ')';
                let delsp = '';  // '<span title="Desconectar RECORRIDO de este poste" class="delete_parada" data-tipo="linea" data-id="'+recorrido.id+'">[X]</span>';
                let url_editar = url_editar_parada.replace('/0', '/' + parada.id);
                let editar = '<a target="_blank" href="' + url_editar + '">Editar</a>';
                
                let url_editar_rec = url_editar_recorrido.replace('/0', '/' + recorrido.id);
                let editar_recorrido = '<a target="_blank" href="' + url_editar_rec + '">[Editar Recorrdo]</a>';
            
                let li = '<li style="color: green;">'+nombre+' (COD: '+parada.codigo+') '+ editar+' '+delsp+' ' + editar_recorrido + '</li>';
                $lip.append(li);
            }
        
          });

        });
        
    };

    // al presional eliminar que se elimine
    $('body').on('click', '.editar_parada', function () {
        let sid = $(this).data('id');
        
    });


});