from django.conf.urls import url, include
from rest_framework import routers
from .views import *


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'escolar', TransporteEscolarViewSet, base_name='transporte-escolar.api.lista')
router.register(r'taxis', TaxisViewSet, base_name='taxis.api.lista')
router.register(r'remis', RemisesViewSet, base_name='remis.api.lista')
router.register(r'paradas-colectivos', ParadaTransportePublicoViewSet, base_name='paradas-colectivos.api.lista')
router.register(r'postes-paradas-colectivos', PosteParadaTransportePublicoViewSet, base_name='poste-paradas-colectivos.api.lista')

router.register(r'recorridos-colectivos', RecorridoLineaTransportePublicoViewSet, base_name='recorridos-colectivos.api.lista')
router.register(r'recorridos-mejorados-colectivos', RecorridoLineaTrazadoMejoradoTransportePublicoViewSet, base_name='recorridos-mejorados-colectivos.api.lista')

router.register(r'lineas', LineaTransportePublicoViewSet, base_name='lineas.api.lista')
router.register(r'lineas-troncales', TroncalTransportePublicoViewSet, base_name='lineas-troncales.api.lista')
router.register(r'postes-con-paradas', PosteSimpleParadaTransportePublicoViewSet, base_name='postes-con-paradas.api.lista')

router.register(r'frecuencias', FrecuenciaViewSet, base_name='frecuencias.api')
router.register(r'estaciones', EstacionViewSet, base_name='estaciones.api')
router.register(r'dias-grupos', DiasGrupoFrecuenciasViewSet, base_name='diasgrupo.api')
router.register(r'franjas-horarias', FranjaHorariaViewSet, base_name='franjashorarias.api')
router.register(r'agrupadores', AgrupadorFranjaHorariaViewSet, base_name='agrupadores.api')

router.register(r'internos', VehiculoTransportePublicoMasivoViewSet, base_name='internos.api')

urlpatterns = [
    url(r'^', include(router.urls)),
]
