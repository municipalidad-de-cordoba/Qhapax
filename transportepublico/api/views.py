from rest_framework import viewsets
from rest_framework.permissions import (IsAuthenticatedOrReadOnly,
                                        IsAuthenticated,
                                        DjangoModelPermissions,
                                        DjangoModelPermissionsOrAnonReadOnly)
from rest_framework.filters import OrderingFilter
from .serializers import *
from api.pagination import DefaultPagination
from django.db.models import Q, F
from datetime import datetime, timedelta
from transportepublico import settings as settings_tp
from transportepublico.models import VehiculoTransportePublico, FranjaHorariaFrecuencias
from rest_framework_gis.filters import InBBoxFilter
from rest_framework.decorators import action
from datetime import timedelta, datetime, date

class TransporteEscolarViewSet(viewsets.ModelViewSet):
    """
    Lista de transportes escolares habilutados.
    Se puede buscar por patente, titular y CUIT con el param "q"
    Por ejemplo "q=perez"
    """
    serializer_class = TransporteEscolarSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    
    def get_queryset(self):
        # entregar los publicados y los habilitados
        ids = [transporte_escolar.id for transporte_escolar in VehiculoTransportePublico.objects.filter(publicado=True) if transporte_escolar.habilitado()]
        queryset = VehiculoTransportePublico.objects.filter(id__in=ids)
        queryset = queryset.filter(tipo_permiso__id=settings_tp.TIPO_TRANSPORTE_ESCOLAR_ID)

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(titular__nombre__icontains=q) |
                Q(titular__CUIT__icontains=q) |
                Q(patente__icontains=q) |
                Q(modelo__nombre__icontains=q) | 
                Q(modelo__marca__nombre__icontains=q)
                )
            
        queryset.order_by('titular__nombre')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class TaxisViewSet(viewsets.ModelViewSet):
    """
    Lista de transportes escolares habilutados.
    Se puede buscar por patente, titular y CUIT con el param "q"
    Por ejemplo "q=perez"
    """
    serializer_class = TaxisSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    
    def get_queryset(self):
        # entregar los publicados y los habilitados
        queryset = VehiculoTransportePublico.objects.filter(
                            publicado=True,
                            tipo_permiso__id=settings_tp.TIPO_TRANSPORTE_TAXI_ID,
                            condicion=VehiculoTransportePublico.COND_HABILITADO)
        
        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(titular__nombre__icontains=q) |
                Q(titular__CUIT__icontains=q) |
                Q(patente__icontains=q) |
                Q(modelo__nombre__icontains=q) | 
                Q(modelo__marca__nombre__icontains=q)
                )
            
        queryset.order_by('titular__nombre')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class RemisesViewSet(viewsets.ModelViewSet):
    """
    Lista de transportes escolares habilutados.
    Se puede buscar por patente, titular y CUIT con el param "q"
    Por ejemplo "q=perez"
    """
    serializer_class = RemisesSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    
    def get_queryset(self):
        # entregar los publicados y los habilitados
        queryset = VehiculoTransportePublico.objects.filter(
                            publicado=True,
                            tipo_permiso__id=settings_tp.TIPO_TRANSPORTE_REMIS_ID,
                            condicion=VehiculoTransportePublico.COND_HABILITADO)

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(titular__nombre__icontains=q) |
                Q(titular__CUIT__icontains=q) |
                Q(patente__icontains=q) |
                Q(modelo__nombre__icontains=q) | 
                Q(modelo__marca__nombre__icontains=q)
                )
            
        queryset.order_by('titular__nombre')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class RecorridoLineaTransportePublicoViewSet(viewsets.ModelViewSet):
    """
    Recorrido de todas las líneas de transporte de la ciudad.
    Se puede filtrar por id del recorrido con el parámetro 'id'. Por ejemplo:
    "id=1". Pueden ser varias ids separadas por comas.
    Se puede filtrar por línea con el parametro 'linea'. Por ejemplo:
    "linea=15". Pueden ser varias líneas, separadas por comas.
    Se puede filtrar por empresa con el parámetro 'e'. Por ejemplo:
    "e=CONIFERAL"
    Se puede filtrar por texto con el parámetro 'q'. Por ejemplo:
    "q=FERREYRA"
    """
    serializer_class = RecorridoLineaGeoTransportePublicoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # Filtramos por recorrido publicado
        queryset = RecorridoLineaTransportePublico.objects.filter(publicado=True)

        id_r = self.request.query_params.get('id', None)
        if id_r is not None:
            id_r = id_r.split(',')
            queryset = queryset.filter(id__in=id_r)

        line = self.request.query_params.get('linea', None)
        if line is not None:
            line = line.split(',')
            queryset = queryset.filter(linea__nombre_publico__in=line)

        e = self.request.query_params.get('e', None)
        if e is not None:
            queryset = queryset.filter(linea__empresa__nombre_publico__icontains=e)

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(descripcion_larga__icontains=q)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class RecorridoLineaTrazadoMejoradoTransportePublicoViewSet(viewsets.ModelViewSet):
    """
    Recorrido de todas las líneas de transporte de la ciudad.
    Se puede filtrar por id del recorrido con el parámetro 'id'. Por ejemplo:
    "id=1". Pueden ser varias ids separadas por comas.
    Se puede filtrar por línea con el parametro 'linea'. Por ejemplo:
    "linea=15". Pueden ser varias líneas, separadas por comas.
    Se puede filtrar por empresa con el parámetro 'e'. Por ejemplo:
    "e=CONIFERAL"
    Se puede filtrar por texto con el parámetro 'q'. Por ejemplo:
    "q=FERREYRA"
    """
    serializer_class = RecorridoLineaGeoTrazadoMejoradoTransportePublicoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # Filtramos por recorrido publicado
        queryset = RecorridoLineaTransportePublico.objects.filter(publicado=True)

        id_r = self.request.query_params.get('id', None)
        if id_r is not None:
            id_r = id_r.split(',')
            queryset = queryset.filter(id__in=id_r)

        line = self.request.query_params.get('linea', None)
        if line is not None:
            line = line.split(',')
            queryset = queryset.filter(linea__nombre_publico__in=line)

        e = self.request.query_params.get('e', None)
        if e is not None:
            queryset = queryset.filter(linea__empresa__nombre_publico__icontains=e)

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(descripcion_larga__icontains=q)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

class ParadaTransportePublicoViewSet(viewsets.ModelViewSet):
    """
    Paradas del transporte público de toda la ciudad georefenciadas.

    Se puede filtrar por linea con el parámetro 'linea'. Por ejemplo:
    "linea=41". Pueden ser varias lineas separadas por comas.
    Se puede filtrar por id de la línea con el parámetro 'id_linea'. Por ejemplo:
    "id_linea=1". Pueden ser varios ids separados por comas.
    Se puede filtrar por empresa con el parámetro 'e'. Por ejemplo:
    "e=CONIFERAL".
    Se puede filtrar por calle principal con el parámetro 'c'. Por ejemplo:
    "c=CASEROS".
    Se puede filtrar por calle secundaria(intersección) con el parámetro 'c_s'. Por ejemplo:
    "c_s=PARAGUAY"
    Se puede filtrar por id del poste con el parámetro 'poste_id'. Por ejemplo:
    "poste_id=1". Pueden ser varios ids separados por comas.
    Se puede filtrar por id del recorrido con el parámetro 'recorrido_id'. Por ejemplo:
    "recorrido_id=1". Pueden ser varios ids separados por comas.
    """
    serializer_class = ParadaTransportePublicoGeoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = ParadaTransportePublico.objects.filter(verificado=True)

        id_line = self.request.query_params.get('id_linea', None)
        if id_line is not None:
            id_line = id_line.split(',')
            queryset = queryset.filter(linea__id__in=id_line, linea__publicado=True)

        line = self.request.query_params.get('linea', None)
        if line is not None:
            line = line.split(',')
            queryset = queryset.filter(linea__nombre_publico__in=line, linea__publicado=True)

        e = self.request.query_params.get('e', None)
        if e is not None:
            queryset = queryset.filter(linea__empresa__nombre_publico__icontains=e)

        c = self.request.query_params.get('c', None)
        if c is not None:
            queryset = queryset.filter(poste__calle_principal__icontains=c)

        c_s = self.request.query_params.get('c_s', None)
        if c_s is not None:
            queryset = queryset.filter(poste__calle_secundaria__icontains=c_s)

        poste_id = self.request.query_params.get('poste_id', None)
        if poste_id is not None:
            poste_id = poste_id.split(',')
            queryset = queryset.filter(poste__id__in=poste_id)

        recorrido_id = self.request.query_params.get('recorrido_id', None)
        if recorrido_id:
            recorrido_id = recorrido_id.split(',')
            queryset = queryset.filter(recorrido__id__in=recorrido_id)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class PosteParadaTransportePublicoViewSet(viewsets.ModelViewSet):
    """
    Postes correspondientes a las paradas del transporte público de toda la ciudad.

    Se puede filtrar por linea con el parámetro 'linea'. Por ejemplo:
    "linea=41". Pueden ser varias lineas separadas por comas.
    Se puede filtrar por id de la línea con el parámetro 'id_linea'. Por ejemplo:
    "id_linea=1". Pueden ser varios ids separados por comas.
    Se puede filtrar por empresa con el parámetro 'e'. Por ejemplo:
    "e=CONIFERAL".
    Se puede filtrar por calle con el parámetro 'c'. Por ejemplo:
    "c=CASEROS".
    Se puede filtrar por id del poste con el parámetro 'poste_id'. Por ejemplo:
    "poste_id=1". Pueden ser varios ids separados por comas.
    Se puede filtrar por id del recorrido con el parámetro 'recorrido_id'. Por ejemplo:
    "recorrido_id=1". Pueden ser varios ids separados por comas.
    Se puede usar bounding box con el parámetro 'in_bbox'. Formato de
    parámetros (min Lon, min Lat, max Lon, max Lat). Ejemplo:
    "in_bbox"=-64.19215920250672,-31.413600429103756,-64.19254544060477,-31.41472664471516
    """
    serializer_class = PosteParadaTransportePublicoGeoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    bbox_filter_field = 'ubicacion'
    filter_backends = (InBBoxFilter, )

    def get_queryset(self):
        queryset = PosteParadaTransportePublico.objects.filter(paradas__verificado=True).distinct()

        id_line = self.request.query_params.get('id_linea', None)
        if id_line is not None:
            id_line = id_line.split(',')
            queryset = queryset.filter(paradas__linea__id__in=id_line, paradas__linea__publicado=True)

        line = self.request.query_params.get('linea', None)
        if line is not None:
            line = line.split(',')
            queryset = queryset.filter(paradas__linea__nombre_publico__in=line, paradas__linea__publicado=True)

        e = self.request.query_params.get('e', None)
        if e is not None:
            queryset = queryset.filter(paradas__linea__empresa__nombre_publico__icontains=e)

        c = self.request.query_params.get('c', None)
        if c is not None:
            queryset = queryset.filter(calle_principal__icontains=c)

        recorrido_id = self.request.query_params.get('recorrido_id', None)
        if recorrido_id:
            recorrido_id = recorrido_id.split(',')
            #FIXME: Django, por qué debo volver a agregar el filtro por verificadas para que funcione correctamente??
            queryset = queryset.filter(paradas__recorrido__id__in=recorrido_id, paradas__verificado=True)

        # esto no es necesario, con el poner /ID al final de la llamada alcanzaría.
        # lo dejo por compatibilidad con llamadas que existan previamente
        poste_id = self.request.query_params.get('poste_id', None)
        if poste_id is not None:
            poste_id = poste_id.split(',')
            queryset = queryset.filter(id__in=poste_id)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
    
    """ TODO permitir 
    @action(methods=['post'], detail=True, permission_classes=[DjangoModelPermissionsOrAnonReadOnly])
    def relocalizar(self, request, pk):
        ''' via API me pasa una nueva ubicación '''
        
        poste = PosteParadaTransportePublico.objects.get(pk=pk)
        data = request.data.copy()
        
        tsr.save()

        res = {'ok': True, 'info': 'Se marco como terminado el trabajo sin recorrido'}
        
        return Response(res, status=status.HTTP_200_OK)
    """

class PosteSimpleParadaTransportePublicoViewSet(viewsets.ModelViewSet):
    """
    Postes simples con los recorridos que para en el (sin geo)
    """
    serializer_class = PosteParadaTransportePublicoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    bbox_filter_field = 'ubicacion'
    filter_backends = (InBBoxFilter, )

    def get_queryset(self):
        queryset = PosteParadaTransportePublico.objects.filter(paradas__verificado=True).distinct()
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class TroncalTransportePublicoViewSet(viewsets.ModelViewSet):
    """
    Troncales correspondientes a las líneas del transporte público de toda la ciudad.
    """
    serializer_class = TroncalTransportePublicoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = TroncalTransportePublico.objects.all()

        return queryset.order_by('nombre_publico')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class LineaTransportePublicoViewSet(viewsets.ModelViewSet):
    """
    Lineas del transporte público de toda la ciudad de Córdoba.

    Se puede filtrar por troncal. Ej:
    id_troncal=1,2,3
    """
    serializer_class = LineaTransportePublicoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # Filtramos por lineas publicadas
        queryset = LineaTransportePublico.objects.filter(publicado=True)

        id_troncal = self.request.query_params.get('id_troncal', None)
        if id_troncal:
            ids = id_troncal.split(',')
            queryset = queryset.filter(troncal__in=ids)

        return queryset.order_by('nombre_publico')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class FranjaHorariaViewSet(viewsets.ModelViewSet):
    """
    Franjas horarias con sus id's y sus horarios desde-hasta
    Se puede filtrar por hora a buscar. Ej: API/?hora_a_buscar=15:35
    Se puede filtrar por id_dias a buscar. Ej: API/?id_dias=2
    Se pueden combinar los filtros. Ej: API/?hora_a_buscar=17:25&id_dias=1
    """
    serializer_class = FranjaHorariaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination


    def get_queryset(self):
        queryset = FranjaHorariaFrecuencias.objects.all()

        lista_de_horas = []
        hora_a_buscar = self.request.query_params.get('hora_a_buscar', None)
        if hora_a_buscar is not None:
            hora_a_buscar = datetime.strptime(hora_a_buscar, "%H:%M").time()
            for franja in queryset:
                if franja.hora_desde <= hora_a_buscar and hora_a_buscar <= franja.hora_hasta:
                    lista_de_horas.append(franja.id)
            queryset = queryset.filter(id__in=lista_de_horas)


        id_dias = self.request.query_params.get('id_dias', None)
        if id_dias is not None:
            id_dias = id_dias.split(',')
            queryset = queryset.filter(agrupador__dias__id__in=id_dias)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class DiasGrupoFrecuenciasViewSet(viewsets.ModelViewSet):
    """
    Grupo de dias con sus id's
    """
    serializer_class = DiasGrupoFrecuenciasSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = DiasGrupoFrecuencias.objects.all()
        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class EstacionViewSet(viewsets.ModelViewSet):
    """
    Estaciones con sus id's
    """
    serializer_class = EstacionSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = EstacionAnioFrecuencias.objects.all()
        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class AgrupadorFranjaHorariaViewSet(viewsets.ModelViewSet):
    """
    Agrupadores con sus id's, nombres y dias
    """
    serializer_class = AgrupadorFranjaHorariaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = AgrupadorFranjaHoraria.objects.all()
        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class FrecuenciaViewSet(viewsets.ModelViewSet):
    """
    API de frecuencia con sus respectivos filtros

    Se puede filtrar por id de estacion. Ej: API/?id_estacion=1, API/?id_estacion=2
    Se puede filtrar por anio. Ej: API/?anio=2018, API/?anio=2017
    Se puede filtrar por linea. Ej: API/?linea=15
    Se puede filtrar por id_agrupador. Ej: API/?id_agrupador=2
    Se puede filtrar por id_dias. Ej: API/?id_dias=3
    Se puede filtrar por frecuencia en minutos esperada. Ej: API/?frecuencia_esperada=11
    Se puede filtrar por unidades activas esperadas. Ej: API/?unidades_activas_esperadas=5

    NOTA: los filtros pueden ser varios separados por comas. Tambien es posible combinarlos.
    Ej: API/?frecuencia_esperada=11,5&id_estacion=2
    """
    serializer_class = FrecuenciaEsperadaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination


    def get_queryset(self):

        queryset = Frecuencia.objects.all()

        estacion = self.request.query_params.get('id_estacion', None)
        if estacion is not None:
            estacion = estacion.split(',')
            queryset = queryset.filter(estacion__id__in=estacion)


        anio = self.request.query_params.get('anio', None)
        if anio is not None:
            anio = anio.split(',')
            queryset = queryset.filter(estacion__anio__in=anio)


        linea = self.request.query_params.get('linea', None)
        if linea is not None:
            linea = linea.split(',')
            queryset = queryset.filter(linea__nombre_publico__in=linea)


        id_agrupador = self.request.query_params.get('id_agrupador', None)
        if id_agrupador is not None:
            id_agrupador = id_agrupador.split(',')
            queryset = queryset.filter(agrupador__id__in=id_agrupador)


        id_dias = self.request.query_params.get('id_dias', None)
        if id_dias is not None:
            id_dias = id_dias.split(',')
            queryset = queryset.filter(agrupador__dias__id__in=id_dias)


        frecuencia_esperada = self.request.query_params.get('frecuencia_esperada', None)
        if frecuencia_esperada is not None:
            frecuencia_esperada = frecuencia_esperada.split(',')
            queryset = queryset.filter(frecuencia_en_minutos_esperada__in=frecuencia_esperada)


        unidades_activas = self.request.query_params.get('unidades_activas_esperadas', None)
        if unidades_activas is not None:
            unidades_activas = unidades_activas.split(',')
            queryset = queryset.filter(unidades_activas_esperadas__in=unidades_activas)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class VehiculoTransportePublicoMasivoViewSet(viewsets.ModelViewSet):
    """
    Datos de las unidades de transporte de las diferentes empresas de transporte
    """
    serializer_class = VehiculoTransportePublicoMasivoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = VehiculoTransportePublicoMasivo.objects.all()
        return queryset.order_by('interno')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
