from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers

from transportepublico.models import *
from rest_framework_gis.serializers import GeoFeatureModelSerializer, GeometrySerializerMethodField


class TransporteEscolarSerializer(CachedSerializerMixin):
    titular = serializers.CharField(source='titular.nombre')
    CUIT = serializers.CharField(source='titular.CUIT')
    estado = serializers.CharField(source='estado_habilitacion')
    marca = serializers.CharField(source='modelo.marca.nombre')
    modelo_vehiculo = serializers.CharField(source='modelo.nombre')


    class Meta:
        model = VehiculoTransportePublico
        fields = ['id', 'titular', 'CUIT', 'patente', 'estado', 'anio_fabricacion',
                    'marca', 'modelo_vehiculo']


class TaxisSerializer(CachedSerializerMixin):
    titular = serializers.CharField(source='titular.nombre')
    CUIT = serializers.CharField(source='titular.CUIT')
    estado = serializers.CharField(source='get_condicion_display')
    marca = serializers.CharField(source='modelo.marca.nombre')
    modelo_vehiculo = serializers.CharField(source='modelo.nombre')


    class Meta:
        model = VehiculoTransportePublico
        fields = ['id', 'titular', 'CUIT', 'patente', 'estado', 'anio_fabricacion',
                    'marca', 'modelo_vehiculo']


class RemisesSerializer(CachedSerializerMixin):
    titular = serializers.CharField(source='titular.nombre')
    CUIT = serializers.CharField(source='titular.CUIT')
    estado = serializers.CharField(source='get_condicion_display')
    marca = serializers.CharField(source='modelo.marca.nombre')
    modelo_vehiculo = serializers.CharField(source='modelo.nombre')


    class Meta:
        model = VehiculoTransportePublico
        fields = ['id', 'titular', 'CUIT', 'patente', 'estado', 'anio_fabricacion',
                    'marca', 'modelo_vehiculo']


class LineaTransportePublicoSerializer(CachedSerializerMixin):

    empresa = serializers.StringRelatedField()
    id_empresa = serializers.CharField(source='empresa.id')

    class Meta:
        model = LineaTransportePublico
        fields = ['id', 'id_externo', 'nombre_publico', 'empresa', 'id_empresa']


class TroncalTransportePublicoSerializer(CachedSerializerMixin):

    lineas = LineaTransportePublicoSerializer(read_only=True, many=True)

    class Meta:
        model = TroncalTransportePublico
        fields = ['id', 'nombre_publico', 'lineas']


class PosteParadaTransportePublicoGeoSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):

    class Meta:
        model = PosteParadaTransportePublico
        geo_field = 'ubicacion'
        auto_bbox = True
        fields = ['id', 'id_externo', 'descripcion', 'sentido', 'calle_principal',
                    'calle_secundaria']


class RecorridoLineaTransportePublicoSerializer(CachedSerializerMixin):
    ''' recorrido sin geo '''
    linea = LineaTransportePublicoSerializer(read_only=True)
    descripcion_cuando_llega = serializers.CharField(source='descripcion_interna')

    class Meta:
        model = RecorridoLineaTransportePublico
        fields = ['id', 'linea', 'id_externo', 'descripcion_corta', 'descripcion_larga', 'descripcion_cuando_llega']


class ParadaSinPosteTransportePublicoSerializer(CachedSerializerMixin):
    ''' para sin el poste para ser el many de los postes '''
    linea = LineaTransportePublicoSerializer(read_only=True)
    recorrido = RecorridoLineaTransportePublicoSerializer(read_only=True)
    
    class Meta:
        model = ParadaTransportePublico    
        fields = ['id', 'linea', 'recorrido', 'poste', 'codigo']


class PosteParadaTransportePublicoSerializer(CachedSerializerMixin):
    paradas = ParadaSinPosteTransportePublicoSerializer(many=True)

    class Meta:
        model = PosteParadaTransportePublico
        fields = ['id', 'id_externo', 'descripcion', 'sentido', 'calle_principal',
                    'calle_secundaria', 'paradas']


class ParadaTransportePublicoSerializer(CachedSerializerMixin):
    poste = PosteParadaTransportePublicoSerializer(read_only=True)
    linea = LineaTransportePublicoSerializer(read_only=True)
    ubicacion = GeometrySerializerMethodField()

    class Meta:
        model = ParadaTransportePublico
        
        fields = ['id', 'linea', 'poste', 'codigo']


class ParadaTransportePublicoGeoSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):
    poste = PosteParadaTransportePublicoSerializer(read_only=True)
    linea = LineaTransportePublicoSerializer(read_only=True)
    ubicacion = GeometrySerializerMethodField()

    def get_ubicacion(self, obj):
        return obj.poste.ubicacion

    class Meta:
        model = ParadaTransportePublico
        geo_field = 'ubicacion'
        fields = ['id', 'linea', 'poste', 'codigo']


class RecorridoLineaGeoTransportePublicoSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):
    ''' recorrido con Geo '''
    linea = LineaTransportePublicoSerializer(read_only=True)
    descripcion_cuando_llega = serializers.CharField(source='descripcion_interna')

    class Meta:
        model = RecorridoLineaTransportePublico
        geo_field = 'trazado'
        fields = ['id', 'linea', 'id_externo', 'descripcion_corta', 'descripcion_larga', 'descripcion_cuando_llega']


class RecorridoLineaGeoTrazadoMejoradoTransportePublicoSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):
    ''' recorrido con Geo '''
    linea = LineaTransportePublicoSerializer(read_only=True)
    descripcion_cuando_llega = serializers.CharField(source='descripcion_interna')

    class Meta:
        model = RecorridoLineaTransportePublico
        geo_field = 'trazado_extendido'
        fields = ['id', 'linea', 'id_externo', 'descripcion_corta', 'descripcion_larga', 'descripcion_cuando_llega']


class FrecuenciaEsperadaSerializer(CachedSerializerMixin):
    ''' Serializer para API de frecuencia '''
    estacion = serializers.CharField(source='estacion.nombre')
    anio = serializers.IntegerField(source='estacion.anio')
    linea = serializers.CharField(source='linea.nombre_publico')
    agrupador = serializers.CharField(source='agrupador.nombre')
    dias = serializers.CharField(source='agrupador.dias.nombre')

    class Meta:
        model = Frecuencia
        fields = ['id', 'anio', 'estacion', 'dias' , 'agrupador', 'linea',
                'frecuencia_en_minutos_esperada', 'unidades_activas_esperadas']


class FranjaHorariaSerializer(CachedSerializerMixin):
    ''' Serializer para franja horaria de frecuencias '''
    agrupador = serializers.CharField(source='agrupador.nombre')
    dias = serializers.CharField(source='agrupador.dias.nombre')
    id_agrupador = serializers.IntegerField(source='agrupador.id')

    class Meta:
        model = FranjaHorariaFrecuencias
        fields = ['id', 'dias', 'nombre', 'agrupador', 'id_agrupador', 'hora_desde', 'hora_hasta']


class DiasGrupoFrecuenciasSerializer(CachedSerializerMixin):
    ''' Serializer para los dias de frecuencias '''

    class Meta:
        model = DiasGrupoFrecuencias
        fields = ['id', 'nombre']


class EstacionSerializer(CachedSerializerMixin):
    ''' Serializer para estaciones de frecuencias '''

    class Meta:
        model = EstacionAnioFrecuencias
        fields = ['id', 'nombre', 'anio']


class AgrupadorFranjaHorariaSerializer(CachedSerializerMixin):
    ''' Serializer para el agrupador de franjas horarias '''
    dias = serializers.CharField(source='dias.nombre')

    class Meta:
        model = AgrupadorFranjaHoraria
        fields = ['id', 'nombre', 'dias']


class VehiculoTransportePublicoMasivoSerializer(CachedSerializerMixin):
    """
    Datos de los colectivos
    """
    empresa = serializers.CharField(source='empresa.nombre_publico')

    class Meta:
        model = VehiculoTransportePublicoMasivo
        fields = ['id', 'interno', 'dominio', 'empresa']


# Registro los serializadores en la cache de DRF
cache_registry.register(TransporteEscolarSerializer)
cache_registry.register(LineaTransportePublicoSerializer)
cache_registry.register(PosteParadaTransportePublicoSerializer)
cache_registry.register(PosteParadaTransportePublicoGeoSerializer)
cache_registry.register(ParadaTransportePublicoSerializer)
cache_registry.register(ParadaTransportePublicoGeoSerializer)
cache_registry.register(ParadaSinPosteTransportePublicoSerializer)
cache_registry.register(RecorridoLineaTransportePublicoSerializer)
cache_registry.register(RecorridoLineaGeoTransportePublicoSerializer)
cache_registry.register(FrecuenciaEsperadaSerializer)
cache_registry.register(FranjaHorariaSerializer)
cache_registry.register(DiasGrupoFrecuenciasSerializer)
cache_registry.register(EstacionSerializer)
cache_registry.register(AgrupadorFranjaHorariaSerializer)
