ID_CIUDAD = 0  # ID de la ciudad para que EFISAT mande las líneas de transorte
URL_BASE_WS_PROVEEDOR = ''
URL_BASE_WS_PROVEEDOR_FAILBACK02 = ''  # url on failback
URL_BASE_WS_PROVEEDOR_FAILBACK03 = ''  # url on failback

USER_WS_PROVEEDOR = ''
PASS_WS_PROVEEDOR = ''

# TIPOLOGIAS DE TRANSPORTE PUBLICO GENERALES
TIPO_TRANSPORTE_ESCOLAR_ID = 0
TIPO_TRANSPORTE_TAXI_ID = 0
TIPO_TRANSPORTE_REMIS_ID = 0

# configuración oculta
try:
    from .local_settings import *
except ImportError:
    pass