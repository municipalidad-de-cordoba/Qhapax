from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^lista-paradas.(?P<filetype>csv|xls)$', views.lista_paradas, name='paradas.lista'),
    url(r'^lista-recorridos-trasnporte-urbano.(?P<filetype>csv|xls)$', views.lista_recorridos_tup, name='recorridos_tup.lista'),
    url(r'^lista-transportes-escolares.(?P<filetype>csv|xls)$', views.lista_transportes_escolares, name='transportes-escolares.lista'),
    url(r'^lista-taxis.(?P<filetype>csv|xls)$', views.lista_taxis, name='taxis.lista'),
    url(r'^lista-remises.(?P<filetype>csv|xls)$', views.lista_remises, name='remises.lista'),
    url(r'^map-editor$', views.MapParadasView.as_view(), name='transportepublico.map-editor'),
    
    ]
    