#!/usr/bin/python
"""
Importar  empresas y lineas de transporte desde webservice del proveedor EFISAT
"""
from django.core.management.base import BaseCommand
from transportepublico.models import EmpresaTransportePublico, LineaTransportePublico, FuenteDeDatos
from transportepublico import settings as settings_tp
from django.db import transaction
import sys
import datetime
import requests
import json
import xml.etree.ElementTree as etree


class Command(BaseCommand):
    help = """Comando para Importar empresas de transporte desde webservice del proveedor EFISAT """

    """
    def add_arguments(self, parser):
        parser.add_argument('--fechaDesde', nargs='?', type=str, help='Fecha de registro en el civil (Y-m-d) para traer los nacimientos Desde')
        parser.add_argument('--fechaHasta', nargs='?', type=str, help='Fecha de registro en el civil (Y-m-d) para traer los nacimientos Hasta')
    """

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando actualizacion de empresas de transporte de una ciudad'))
        fuente, created = FuenteDeDatos.objects.get_or_create(nombre='EFISAT')
        # requiere un POST con user y PASS

        # headers = {'Content-Type': 'application/soap+xml; charset=utf-8'}
        headers = {'Content-Type': 'application/soap+xml; charset=iso-8859-1'}
        # headers = {'Content-Type': 'text/xml; charset=utf-8'}

        body = """<?xml version="1.0" encoding="utf-8"?>
                    <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                      <soap12:Body>
                        <RecuperarLineaPorLocalidad xmlns="http://clsw.smartmovepro.net/">
                          <usuario>{user}</usuario>
                          <clave>{passw}</clave>
                          <localidad>{localidad}</localidad>
                          <provincia>{provincia}</provincia>
                          <pais>{pais}</pais>
                        </RecuperarLineaPorLocalidad>
                      </soap12:Body>
                    </soap12:Envelope>""".format(user=settings_tp.USER_WS_PROVEEDOR, 
                                                    passw=settings_tp.PASS_WS_PROVEEDOR,
                                                    localidad='CÓRDOBA',
                                                    provincia='CÓRDOBA',
                                                    pais='ARGENTINA'
                                                )

        url = settings_tp.URL_BASE_WS_PROVEEDOR
        self.stdout.write(self.style.SUCCESS('POST \n{} \n TO {}'.format(body, url)))

        try:
            response = requests.post(url, data=body, headers=headers, timeout=25.0)
        except Exception as e:
            self.stdout.write(self.style.ERROR('ERROR trayendo datos: {} \n BODY SENT: {}'.format(e, body)))
            sys.exit(1)
        # self.stdout.write(self.style.SUCCESS('RESPUESTA: {}'.format(response.text)))
        ''' MUESTRA 
            <?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
              <soap:Body>
                <RecuperarLineaPorLocalidadResponse xmlns="http://clsw.smartmovepro.net/">
                  <RecuperarLineaPorLocalidadResult>{"CodigoEstado":0,"MensajeEstado":"ok","lineas":[
                                                                    {"CodigoLineaParada":"1278","Descripcion":"10","CodigoEntidad":"117","CodigoEmpresa":175},
                                                                    {"CodigoLineaParada":"1280","Descripcion":"11","CodigoEntidad":"117","CodigoEmpresa":175},
                                                                                                    ]
                                                    .
                                                    .
                                                    .
                                                    }
                  </RecuperarLineaPorLocalidadResult>
                </RecuperarLineaPorLocalidadResponse>
              </soap:Body>
            </soap:Envelope>
        '''
        data = etree.fromstring(response.text)
        soap = data[0]  # soap:Envelope
        recupera = soap[0]  # RecuperarLineaPorCuandoLlegaResponse
        results = recupera[0]  # <RecuperarLineaPorCuandoLlegaResult>
        results_json = json.loads(results.text)
        """
        results_json = {"CodigoEstado: xxxx, MensajeEstado: xxxx, lineas:[{}]"}
        """

        empresa_nueva = 0
        empresa_repetida = 0
        linea_nueva = 0
        linea_repetida = 0

        if results_json['MensajeEstado'] == 'ok':
            for dato in results_json['lineas']:
                #dato
                # {"CodigoLineaParada":"1278",
                #  "Descripcion":"10",
                #  "CodigoEntidad":"117",
                #  "CodigoEmpresa":175
                #  }
                empresa, created = EmpresaTransportePublico.objects.get_or_create(id_externo=dato['CodigoEmpresa'])

                if not created:
                    empresa_repetida += 1
                else:
                    empresa_nueva += 1

                empresa_nombre = empresa.nombre_publico if not created else 'Empresa nueva'
                self.stdout.write(self.style.SUCCESS('Linea: {} - Empresa: {}'.format(dato['Descripcion'], empresa_nombre)))

                try:
                    linea = LineaTransportePublico.objects.get(id_externo=dato['CodigoLineaParada'], fuente=fuente)
                    if linea.nombre_publico != dato['Descripcion']:
                        linea_repetida += 1
                except Exception:
                    linea = LineaTransportePublico.objects.create(id_externo=dato['CodigoLineaParada'], nombre_publico=dato['Descripcion'], fuente=fuente, empresa=empresa)
                    linea.save()
                    linea_nueva += 1
        else:
            self.stdout.write(self.style.ERROR(' Error: {}'.format(results_json)))

        self.stdout.write(self.style.SUCCESS('FIN. {} empresas nuevas'.format(empresa_nueva)))
        self.stdout.write(self.style.SUCCESS('FIN. {} lineas nuevas. {} lineas actualizadas'.format(linea_nueva, linea_repetida)))
