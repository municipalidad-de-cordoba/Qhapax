from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from django.db import transaction
from barrios.models import Barrio
from transportepublico.models import RecorridoLineaTransportePublico
from django.contrib.gis.geos import Point
import sys


class Command(BaseCommand):
    help = """Comando para Importar origen y destino de recorridos de líneas del transporte urbano"""

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Iniciando importación...'))

        poligonos_barrios = Barrio.objects.values_list('poligono', flat=True)
        recorridos = RecorridoLineaTransportePublico.objects.all()

        for recorrido in recorridos:
            fallas = []
            este = '{} {}'.format(recorrido.descripcion_larga, recorrido.linea.nombre_publico)
            origen = Point(recorrido.trazado.coords[0])
            destino = Point(recorrido.trazado.coords[-1])
            #busca el barrio de origen
            for poligono in poligonos_barrios:
                if poligono is not None and poligono.contains(origen):
                    recorrido.origen = Barrio.objects.get(poligono=poligono)
                    break
            
            if recorrido.origen is None:
                fallas.append('origen')
                self.stdout.write(self.style.ERROR('Origen no encontrado. Recorrido {}'.format(este)))
            
            #busca el barrio de destino
            for poligono in poligonos_barrios:
                if poligono is not None and poligono.contains(destino):
                    recorrido.destino = Barrio.objects.get(poligono=poligono)
                    break
                                  
            if recorrido.destino is None:
                fallas.append('destino')
                self.stdout.write(self.style.ERROR('Destino no encontrado. Recorrido {}'.format(este)))
                                  
            #armo una descripcion que se verá en la app movil
            origen_str = 'Origen desconocido' 
            if recorrido.origen is None:
                if recorrido.destino is None:
                    descripcion_interna = recorrido.descripcion_larga
                else:
                    descripcion_interna = 'Hacia {} ({})'.format(recorrido.destino, recorrido.descripcion_larga)
            else:
                if recorrido.destino is None:
                    descripcion_interna = 'Desde {} ({})'.format(recorrido.origen, recorrido.descripcion_larga)
                else:
                    descripcion_interna = 'De {} a {}'.format(recorrido.origen, recorrido.destino)
                
            recorrido.descripcion_interna = descripcion_interna
            
            recorrido.save()

            if len(fallas) == 0:
                self.stdout.write(self.style.SUCCESS('Descripcion obtenida sin problemas {}'.format(este)))
            else:
                self.stdout.write(self.style.WARNING('Descripcion obtenida con fallas {}: {}'.format(este, fallas)))
            
            
        self.stdout.write(self.style.SUCCESS('Fin'))

