from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from django.db import transaction
from transportepublico.models import ParadaTransportePublico, LineaTransportePublico, RecorridoLineaTransportePublico, PosteParadaTransportePublico
# from django.contrib.gis.geos import Point
import sys
from django.conf import settings
from geopy import distance
from django.contrib.gis.measure import Distance
import django_excel as excel
import os


def get_close_coord(lista, coordenada):
    # Dado una 'lista' de tuplas retornamos la mas cercana a 'coordenada'
    return min(lista, key=lambda c: (c[0] - coordenada[0])**2 + (c[1] - coordenada[1])**2)


class Command(BaseCommand):
    help = """Comando para asociar paradas con recorrido del transporte urbano"""

    def add_arguments(self, parser):
        parser.add_argument('--distancia',
                            type=int,
                            default=50,
                            help='Distancia a partir de la cual queremos detectar postes alejados'
                            )

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Iniciando importación...'))

        lineas = LineaTransportePublico.objects.filter(publicado=True)
        recorridos = RecorridoLineaTransportePublico.objects.filter(linea__publicado=True, publicado=True)

        distancia_param = options['distancia']
        # lineas_sin_asociar = []
        # csv_list = []
        # csv_list.append(['id_parada', 'poste','linea'])
        resumen = []
        errores = []
        for recorrido in recorridos:

            paradas = ParadaTransportePublico.objects.filter(recorrido=recorrido, verificado=True)
            # postes = PosteParadaTransportePublico.objects.filter(paradas__linea=linea)

            if paradas.count() > 0:
                if recorrido.trazado_extendido is not None:
                    for parada in paradas:
                        #en este dic se guardaran las distancias
                        dict_min_rec = {}

                        coordenada_cercana = get_close_coord(recorrido.trazado_extendido.coords, parada.poste.ubicacion)

                        # calculo la distancia entre el poste y el punto mas cercano obtenido anteriormente
                        distancia = distance.distance(coordenada_cercana, parada.poste.ubicacion).m
                        dict_min_rec[recorrido.id] = distancia

                        # dict_min_rec guarda la distancia del poste y todos los respectivos recorridos
                        # la idea es que obtener la minima distancia(o las minimas en caso de mas de dos recorridos) y asociarlos
                        parada.distancia_a_recorrido = dict_min_rec[parada.recorrido.id]
                        self.stdout.write(self.style.SUCCESS('parada id {} asociada a linea {} con recorrido {}(a {} mts)'.format(parada.id, recorrido.linea.nombre_publico, recorrido.id, parada.distancia_a_recorrido)))
                        parada.save()

                        #guardamos paradas lejanas a su recorrido correspondiente
                        if parada.distancia_a_recorrido > distancia_param:
                            resumen_str = 'Parada id {} del recorrido id {} de la linea {} está ubicada a {}m.'.format(parada.id, recorrido.id, recorrido.linea.nombre_publico, parada.distancia_a_recorrido)
                            resumen.append(resumen_str)

                else:
                    error = 'Actualizar el recorrido id {} de la linea {} porque no tiene recorrido extendido'.format(recorrido.id, recorrido.linea.nombre_publico)
                    self.stdout.write(self.style.ERROR(error))
                    errores.append(error)

            else:
                error = 'El recorrido id {} de la linea {} no tiene paradas asociadas'.format(recorrido.id, recorrido.linea.nombre_publico)
                self.stdout.write(self.style.ERROR(error))
                errores.append(error)

        # dest = os.path.join(settings.MEDIA_ROOT, 'lista_paradas_sin_recorridos.csv')
        # excel.pe.save_as(array=csv_list, dest_file_name=dest)

        # self.stdout.write(self.style.SUCCESS('Archivo creado en {}'.format(dest)))

        if len(errores) > 0:
            self.stdout.write(self.style.ERROR('********************* ERRORES *********************'))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))

            self.stdout.write(self.style.ERROR('***************************************************'))

        if len(resumen) > 0:
            self.stdout.write(self.style.SUCCESS('********************* RESUMEN *********************'))
            for r in resumen:
                self.stdout.write(self.style.SUCCESS(r))

            self.stdout.write(self.style.SUCCESS('***************************************************'))

        self.stdout.write(self.style.SUCCESS('Fin.'))
