#!/usr/bin/python
from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from datetime import datetime
from django.contrib.gis.geos import LineString
from fastkml import kml
from transportepublico.models import RecorridoLineaTransportePublico


class Command(BaseCommand):
    help = """Comando para importar un recorrido desde un archivo .kml"""

    def add_arguments(self, parser):
        parser.add_argument('--path', type=str, help='Path del archivo KML local')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación del recorrido...'))
        url = options['path']

        try:
            with open(url, 'rt', encoding="utf-8") as myfile: doc=myfile.read().encode('utf-8')
            self.stdout.write(self.style.SUCCESS('Buscando datos en {}'.format(url)))
        except Exception as e:
            self.stdout.write(self.style.ERROR('Error al leer de {}'.format(url)))
            sys.exit(1)

        # Create the KML object to store the parsed result
        k = kml.KML()

        # Read in the KML string
        k.from_string(doc)
        features = list(k.features())

        for feature in features:
            self.stdout.write("=============================")
            self.stdout.write("Nombre recorrido: {}".format(feature.name))
            self.stdout.write("=============================")

            try:
                recorrido = RecorridoLineaTransportePublico.objects.get(descripcion_corta=feature.name)
            except Exception as e:
                self.stdout.write(self.style.ERROR('No se encontró el recorrido. Revisar script'))
            folders = list(feature.features())
            for folder in folders:
                self.stdout.write("=============================")
                self.stdout.write("Descripción: {}".format(folder.name))
                self.stdout.write("=============================")
                trazado = LineString([xy[0:2] for xy in folder.geometry.coords])
                recorrido.trazado = trazado
                recorrido.save()

                self.stdout.write(self.style.SUCCESS('Recorrido actualizado'))


        self.stdout.write("FIN")
