#!/usr/bin/python
from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from datetime import datetime
from django.conf import settings
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance
from fastkml import kml
from transportepublico.models import PosteParadaTransportePublico, ParadaTransportePublico
import django_excel as excel
import os


class Command(BaseCommand):
    help = """Comando para calcular paradas de transporte cercanas a los puntos de WiFi"""

    def add_arguments(self, parser):
        parser.add_argument('--path', type=str, help='Path del archivo KML local')
        parser.add_argument('--distancia', type=int, default=25, help='Distancia del radio a calcular en metros')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando...'))
        url = options['path']

        try:
            with open(url, 'rt', encoding="utf-8") as myfile: doc=myfile.read().encode('utf-8')
            self.stdout.write(self.style.SUCCESS('Buscando datos en {}'.format(url)))
        except Exception as e:
            self.stdout.write(self.style.ERROR('Error al leer de {}'.format(url)))
            sys.exit(1)

        # Create the KML object to store the parsed result
        k = kml.KML()

        # Read in the KML string
        k.from_string(doc)
        features = list(k.features())

        csv_list = []
        csv_list.append(['ubicacion WiFi', 'id_poste', 'latitud', 'longitud', 'ids_paradas', 'descripcion'])
        postes = PosteParadaTransportePublico.objects.all()
        paradas = ParadaTransportePublico.objects.all()
        for feature in features:
            self.stdout.write("=============================")
            self.stdout.write("Doc: {}".format(feature.name))
            self.stdout.write("=============================")

            folders = list(feature.features())
            total_paradas = 0
            for folder in folders:
                self.stdout.write("=============================")
                self.stdout.write("Ubicación WiFi: {}".format(folder.name))

                # creo el punto donde se ubica el WiFi
                punto_de_wifi = Point(folder.geometry.coords[0][0], folder.geometry.coords[0][1])
                radio = options['distancia']
                postes_asociados = postes.filter(ubicacion__distance_lte=(punto_de_wifi, Distance(m=radio))).distinct()
                paradas_asociadas = paradas.filter(poste__id__in=[poste.id for poste in postes_asociados]).distinct()

                for poste in postes_asociados:
                    csv_result = []
                    # ubicacion wifi
                    csv_result.append(folder.name)
                    # id_poste
                    csv_result.append(poste.id)
                    # latitud
                    csv_result.append(poste.ubicacion.coords[1])
                    # longitud
                    csv_result.append(poste.ubicacion.coords[0])

                    paradas_str = ''
                    desc = ''
                    for parada in paradas_asociadas:
                        paradas_str += '{} - '.format(parada.id)
                        desc += '({} - recorrido: {}) - '.format(parada.linea, parada.recorrido.descripcion_corta)
                    # id's_paradas
                    csv_result.append(paradas_str)

                    # descripcion
                    csv_result.append(desc)

                # if postes.count() > 0:
                    csv_list.append(csv_result)

                self.stdout.write("Postes asociados: {}".format(postes_asociados.count()))
                self.stdout.write("Paradas asociadas: {}".format(paradas_asociadas.count()))
                for parada in paradas_asociadas:
                    self.stdout.write("{} - recorrido: {}".format(parada.linea, parada.recorrido))
                total_paradas += paradas_asociadas.count()
                self.stdout.write("=============================")
            self.stdout.write('Cantidad de paradas con WiFi: {}'.format(total_paradas))

        # se guarda todo en un archivo
        dest = os.path.join(settings.MEDIA_ROOT, 'lista_paradas_con_wifi.csv')
        excel.pe.save_as(array=csv_list, dest_file_name=dest)

        self.stdout.write(self.style.SUCCESS('Archivo creado en {}'.format(dest)))
