#!/usr/bin/python
from django.core.management.base import BaseCommand
from random import randrange
from portaldedatos.models import ArchivoCSV
from organizaciones.models import Organizacion
from transportepublico.models import TipoServicioTransporte, VehiculoTransportePublico, MarcaVehiculo, ModeloVehiculo
from django.db import transaction
import csv
import sys
import datetime


class Command(BaseCommand):
    help = """Comando para importar lista de taxis.
                Requiere un CSV cargado en el sistema. Se marcará como _procesado_
                luego de ejecutarse para evitar duplicados
            """

    def add_arguments(self, parser):
        parser.add_argument('--csv_id', type=int, help='ID del CSV cargado al sistema')
        parser.add_argument('--clean_all', type=bool, default=False, help='Boorar antes todos los taxis del sistema')
        parser.add_argument('--force', action='store_true', dest='force',
                            default=False, help="forzar a procesar el archivo")

    @transaction.atomic
    def handle(self, *args, **options):
        force = options['force']
        
        if force:
            self.stdout.write(self.style.WARNING('--- Forzando importación de CSV ---'))

        try:
            if force:
                instanceCSV = ArchivoCSV.objects.get(pk=options['csv_id'])
            else:
                instanceCSV = ArchivoCSV.objects.get(pk=options['csv_id'], procesado=False)
        except ArchivoCSV.DoesNotExist:
            if force:
                self.stdout.write(self.style.ERROR('El CSV id: %s no existe' % options['csv_id']))
            else:
                self.stdout.write(self.style.ERROR('El CSV id: %s no existe o ya está procesado' % options['csv_id']))
            sys.exit(1)

        if instanceCSV is None:
            self.stdout.write(self.style.ERROR('No hay CSV'))
            sys.exit(1)

        self.stdout.write(self.style.SUCCESS('Importando csv (id: {})'.format(instanceCSV.id)))

        #TIENEN QUE TENER ENCABEZADO!
        if not instanceCSV.tiene_fila_encabezado:
            self.stdout.write(self.style.ERROR('El CSV indica que no tiene encabezado. Se procesará igualemente'))

        # asegurarse de que el archivo tenga la misma estructura (encabezados)
        
        fieldnames = ["Interno","Patente","Permisionario","Nº de Documento","Condición","Dto./Res.",
                        "fecha","Fecha Aprobado","Fecha Habilitación","Calle","Nº","Piso","Dpto.",
                        "Barrio","Marca","Modelo","Fecha de Actualización"]

        count = 0
        titulares_nuevos = 0
        titulares_repetidos = 0

        tipos_nuevos = 0
        tipos_repetidos = 0

        marcas_nuevas = 0
        marcas_repetidas = 0

        modelos_nuevos = 0
        modelos_repetidos = 0

        vehiculos_nuevos = 0
        vehiculos_repetidos = 0

        licencia, created = TipoServicioTransporte.objects.get_or_create(nombre="Taxi")
        licencia.save()

        clean_all = options['clean_all']
        if clean_all:
            taxis = VehiculoTransportePublico.objects.filter(tipo_permiso=licencia)
            self.stdout.write(self.style.ERROR('EMILIMINANDO {} TAXIS'.format(len(taxis))))
            taxis.delete()

        with open(instanceCSV.archivo_local.path) as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=instanceCSV.separado_por,
                                    quotechar=instanceCSV.contenedor_de_texto)

            header = reader.__next__()
            
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR('BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            for row in reader:
                count += 1
                observaciones_internas = []

                # chequear los datos
                errores = []
                # self.stdout.write(self.style.SUCCESS('Linea leida: {}'.format(row)))

                interno = row['Interno'].strip()
                if interno == '':  # si no tiene interno genero uno al azar y dejo marcado el error
                    interno = 'QQ{}'.format(randrange(90000) + 9999)
                    self.stdout.write(self.style.ERROR('No hay interno, se generó uno al azar {}, fila {}'.format(interno, count)))
                    observaciones_internas.append('Se genero un interno al azar porque estaba vacío: {}'.format(interno))
                
                titular = row['Permisionario'].strip()
                cuit = row['Nº de Documento'].strip()
                
                condicion_str = row['Condición'].strip()

                if condicion_str in ['HABILITADO']:
                    condicion = VehiculoTransportePublico.COND_HABILITADO
                elif condicion_str in ['CADUCIDAD']:
                    condicion = VehiculoTransportePublico.COND_CADUCIDAD
                elif condicion_str in ['TRANSF', 'TRANSFEREN', 'TRANF']:
                    condicion = VehiculoTransportePublico.COND_TRANSF
                elif condicion_str in ['VACANTE']:
                    condicion = VehiculoTransportePublico.COND_VACANTE
                elif condicion_str in ['ASPIRANTE']:
                    condicion = VehiculoTransportePublico.COND_ASPIRANTE
                elif condicion_str in ['C/AMPARO']:
                    condicion = VehiculoTransportePublico.COND_AMPARO
                elif condicion_str in ['TRANSF/FALL', 'TARNSF/FALL', 'TR.P/FALLE']:
                    observaciones_internas.append('La condicion original decía {} y se marcó como TRANSF'.format(condicion_str))
                    condicion = VehiculoTransportePublico.COND_TRANSF
                elif condicion_str in ['', '-']:
                    observaciones_internas.append('La condicion original estaba vacía y se marcó como DESCONOCIDA')
                    condicion = VehiculoTransportePublico.COND_DESCONOCIDO
                else:
                    observaciones_internas.append('La condicion original era {} y se marcó como DESCONOCIDA'.format(condicion_str))
                    self.stdout.write(self.style.ERROR('Fila {} CONDICION DESCONOCIDA {}'.format(count, condicion_str)))
                    condicion = VehiculoTransportePublico.COND_DESCONOCIDO

                decreto = row['Dto./Res.'].strip()
                
                decreto_fecha_str = row['fecha'].strip()
                

                if decreto_fecha_str == '':
                    decreto_fecha = None
                elif len(decreto_fecha_str) < 8:
                    observaciones_internas.append('La fecha del decreto no estaba completa, decía "{}"'.format(decreto_fecha_str))
                    self.stdout.write(self.style.ERROR("Fila {} Fecha decreto incompleta: {}".format(count, decreto_fecha_str)))
                    decreto_fecha = None
                else:
                    try:
                        decreto_fecha = datetime.datetime.strptime(decreto_fecha_str, "%d/%m/%Y").date()
                    except Exception as e:
                        error = 'Fila {} Fecha de decreto ({}) no es válida. ERROR: {}'.format(count, decreto_fecha_str, e)
                        decreto_fecha = None
                        self.stdout.write(self.style.ERROR(error))
                        observaciones_internas.append('Fecha de Decreto ({}) no es válida, queda vacía'.format(decreto_fecha_str))

                aprobado_fecha_str = row['Fecha Aprobado'].strip()
                if aprobado_fecha_str == '':
                    aprobado_fecha = None
                else:
                    try:
                        aprobado_fecha = datetime.datetime.strptime(aprobado_fecha_str, "%d/%m/%Y").date()
                    except Exception as e:
                        aprobado_fecha = None
                        error = 'Fila {} Fecha de aprobado ({}) no es válida. ERROR: {}'.format(count, aprobado_fecha_str, e)
                        observaciones_internas.append('Fecha de Aprobado ({}) no es válida, queda vacía'.format(aprobado_fecha_str))
                        self.stdout.write(self.style.ERROR(error))
                
                habilitacion_fecha_str = row['Fecha Habilitación'].strip()
                
                if habilitacion_fecha_str == '':
                    habilitacion_fecha = None
                else:
                    try:
                        habilitacion_fecha = datetime.datetime.strptime(habilitacion_fecha_str, "%d/%m/%Y").date()
                    except Exception as e:
                        habilitacion_fecha = None
                        error = 'Fila {}. Fecha de habilitacion ({}) no es válida. ERROR: {}'.format(count, habilitacion_fecha_str, e)
                        observaciones_internas.append('Fecha de habilitacion ({}) no es válida, queda vacía'.format(habilitacion_fecha_str))
                        self.stdout.write(self.style.ERROR(error))

                actualizacion_fecha_str = row['Fecha de Actualización'].strip()
                if actualizacion_fecha_str == '':
                    actualizacion_fecha = None
                else:
                    try:
                        actualizacion_fecha = datetime.datetime.strptime(actualizacion_fecha_str, "%d/%m/%Y").date()
                    except Exception as e:
                        actualizacion_fecha = None
                        error = 'Fila {} Fecha de actualizacion ({}) no es válida. ERROR: {}'.format(count, actualizacion_fecha_str, e)
                        self.stdout.write(self.style.ERROR(error))
                        observaciones_internas.append('Fecha de Actualizacion ({}) no es válida, queda vacía'.format(actualizacion_fecha_str))

                domicilio_calle = row['Calle'].strip()
                domicilio_nro = row['Nº'].strip()
                domicilio_piso = row['Piso'].strip()
                domicilio_dpto = row['Dpto.'].strip()
                domicilio_barrio = row['Barrio'].strip()

                dominio = row['Patente'].strip()

                # se requiere separar el modelo de vehículos en dos, uno de autos y otro de permisos
                # de modo que si no hjay patente entonces no se cargue directamente el auto ...
                if dominio == '':
                    dominio = 'SIN ASIGNAR'  # es el default de patente vacía
                
                marca_str = row['Marca'].strip()

                modelos = {
                    '': {'marca': 'Desconocido', 'modelo': '-'},
                    '0': {'marca': 'Desconocido', 'modelo': '-'},
                    'CEHVROLET CORSA': {'marca': 'Chevrolet', 'modelo': 'Corsa'}, 
                    'CHEV AVEO': {'marca': 'Chevrolet', 'modelo': 'Aveo'}, 
                    'CHEV CLASSIC': {'marca': 'Chevrolet', 'modelo': 'Classic'}, 
                    'CHEV CORSA': {'marca': 'Chevrolet', 'modelo': 'Corsa'}, 
                    'CHEV PRISMA': {'marca': 'Chevrolet', 'modelo': 'Prisma'}, 
                    'CHEVROLET AGILE': {'marca': 'Chevrolet', 'modelo': 'Agile'}, 
                    'CHEVROLET ASTRA': {'marca': 'Chevrolet', 'modelo': 'Astra'}, 
                    'CHEVROLET AVEO': {'marca': 'Chevrolet', 'modelo': 'Aveo'}, 
                    'CHEVROLET CLASSIC': {'marca': 'Chevrolet', 'modelo': 'Classic'}, 
                    'CHEVROLET COBALT': {'marca': 'Chevrolet', 'modelo': 'Cobalt'}, 
                    'CHEVROLET CORSA': {'marca': 'Chevrolet', 'modelo': 'Corsa'}, 
                    'CHEVROLET CORSA II': {'marca': 'Chevrolet', 'modelo': 'Corsa II'}, 
                    'CHEVROLET MERIVA': {'marca': 'Chevrolet', 'modelo': 'Meriva'}, 
                    'CHEVROLET PRISMA': {'marca': 'Chevrolet', 'modelo': 'Prisma'}, 
                    'CHEVROLET WAGON': {'marca': 'Chevrolet', 'modelo': 'Wagon'}, 
                    'CHEVRROLET CORSA': {'marca': 'Chevrolet', 'modelo': 'Corsa'}, 
                    'CHVROLET CLASSIC': {'marca': 'Chevrolet', 'modelo': 'Classic'}, 
                    'FIAT 125': {'marca': 'FIAT', 'modelo': '125'}, 
                    'FIAT DUNA': {'marca': 'FIAT', 'modelo': 'Duna'}, 
                    'FIAT PALIO': {'marca': 'FIAT', 'modelo': 'Palio'}, 
                    'FIAT PALIO W': {'marca': 'FIAT', 'modelo': 'Palio'}, 
                    'FIAT PUNTO': {'marca': 'FIAT', 'modelo': 'Punti'}, 
                    'FIAT REGATTA': {'marca': 'FIAT', 'modelo': 'Regatta'}, 
                    'FIAT SIENA': {'marca': 'FIAT', 'modelo': 'Siena'}, 
                    'FIAT TEMPRA': {'marca': 'FIAT', 'modelo': 'Tempra'}, 
                    'FIAT UNO': {'marca': 'FIAT', 'modelo': 'Uno'}, 
                    'FITA SIENA': {'marca': 'FIAT', 'modelo': 'Siena'}, 
                    'FORD ECCO SPORT': {'marca': 'Ford', 'modelo': 'EcoSport'}, 
                    'FORD ECOSPORT': {'marca': 'Ford', 'modelo': 'EcoSport'}, 
                    'FORD ESCORT': {'marca': 'Ford', 'modelo': 'Escort'}, 
                    'FORD FIESTA': {'marca': 'Ford', 'modelo': 'Fiesta'}, 
                    'FORD KA':  {'marca': 'Ford', 'modelo': 'Ka'}, 
                    'KIA CLARUS': {'marca': 'Kia', 'modelo': 'Clarus'}, 
                    'MERCEDES BENZ SPRINTER': {'marca': 'Mercedes Benz', 'modelo': 'Sprinter'}, 
                    'PEUGEOT 207': {'marca': 'Peugeot', 'modelo': '207'}, 
                    'PEUGEOT 306 D': {'marca': 'Peugeot', 'modelo': '306'}, 
                    'PEUGEOT 301': {'marca': 'Peugeot', 'modelo': '301'}, 
                    'PEUGEOT 307': {'marca': 'Peugeot', 'modelo': '307'}, 
                    'PEUGEOT 504': {'marca': 'Peugeot', 'modelo': '504'}, 
                    'PEUGEOT PARTNER': {'marca': 'Peugeot', 'modelo': 'Partner'}, 
                    'PEUGEOT PATNER': {'marca': 'Peugeot', 'modelo': 'Partner'}, 
                    'RENAULT 12': {'marca': 'Renault', 'modelo': '12'}, 
                    'RENAULT 19': {'marca': 'Renault', 'modelo': '19'}, 
                    'RENAULT CLIO': {'marca': 'Renault', 'modelo': 'Clio'}, 
                    'RENAULT LOGAN': {'marca': 'Renault', 'modelo': 'Logan'}, 
                    'RENAULT MEGANE': {'marca': 'Renault', 'modelo': 'Megane'}, 
                    'RENAULT SANDERO': {'marca': 'Renault', 'modelo': 'Sandero'}, 
                    'RENAULT SYMBOL': {'marca': 'Renault', 'modelo': 'Symbol'}, 
                    'TOYOTA COROLLA': {'marca': 'Toyota', 'modelo': 'Corolla'}, 
                    'TOYOTA ETIOS': {'marca': 'Toyota', 'modelo': 'Etios'}, 
                    'V W GOL COUNTRY': {'marca': 'Volkswagen', 'modelo': 'Gol Country'}, 
                    'V W VOYAGE': {'marca': 'Volkswagen', 'modelo': 'Voyage'}, 
                    'VOLKSWAGEN': {'marca': 'Volkswagen', 'modelo': '-'}, 
                    'VW BORA': {'marca': 'Volkswagen', 'modelo': 'Bora'}, 
                    'VW COUNTRY': {'marca': 'Volkswagen', 'modelo': 'Gol Country'}, 
                    'VW CROSS FOX': {'marca': 'Volkswagen', 'modelo': 'Cross Fox'}, 
                    'VW FOX': {'marca': 'Volkswagen', 'modelo': 'Cross Fox'}, 
                    'VW GOL': {'marca': 'Volkswagen', 'modelo': 'Gol'}, 
                    'VW GOL COUNTRY': {'marca': 'Volkswagen', 'modelo': 'Gol Country'}, 
                    'VW GOL TREND': {'marca': 'Volkswagen', 'modelo': 'Gol Trend'}, 
                    'VW POLO': {'marca': 'Volkswagen', 'modelo': 'Polo'}, 
                    'VW SENDA': {'marca': 'Volkswagen', 'modelo': 'Senda'}, 
                    'VW SURAN': {'marca': 'Volkswagen', 'modelo': 'Suran'}, 
                    'VW VOYAGE': {'marca': 'Volkswagen', 'modelo': 'Voyage'},
                    'VW CROSS UP': {'marca': 'Volkswagen', 'modelo': 'Cross Up'},
                    'FIAT CRONOS': {'marca': 'Fiar', 'modelo': 'Cronos'},
                    'CEVROLET PRISMA': {'marca': 'Chevrolet', 'modelo': 'Prisma'},
                    
                }

                este = modelos[marca_str]

                try:
                    anio = int(row['Modelo'].strip())
                except Exception as e:
                    observaciones_internas.append('El año del modelo es inválido ({}) queda vacío'.format(row['Modelo'].strip()))
                    anio = 0

                # en base al CUIT, en primer lugar, asegurarse que exista la Empresa (organizacion)
                orgs = Organizacion.objects.filter(nombre=titular, CUIT=cuit)
                if len(orgs) > 1:
                    organizacion = orgs[0]
                    created = False
                else:    
                    organizacion, created = Organizacion.objects.get_or_create(nombre=titular, CUIT=cuit)
                organizacion.domicilio_calle = domicilio_calle
                organizacion.domicilio_nro = domicilio_nro
                organizacion.domicilio_piso = domicilio_piso
                organizacion.domicilio_dpto = domicilio_dpto
                organizacion.domicilio_barrio = domicilio_barrio
                organizacion.save()

                if created:
                    titulares_nuevos += 1
                    # self.stdout.write(self.style.SUCCESS('Orga nueva: {}'.format(organizacion)))
                else:
                    titulares_repetidos += 1

                if created:
                    tipos_nuevos += 1
                else:
                    tipos_repetidos += 1

                marca, created = MarcaVehiculo.objects.get_or_create(nombre=este['marca'])
                marca.save()
                if created:
                    marcas_nuevas += 1
                else:
                    marcas_repetidas += 1

                modelo, created = ModeloVehiculo.objects.get_or_create(marca=marca, nombre=este['modelo'])
                modelo.save()
                if created:
                    modelos_nuevos += 1
                else:
                    modelos_repetidos += 1

                # ver si ya existe el interno
                taxis_con_este_interno = VehiculoTransportePublico.objects.filter(interno=interno)
                if len(taxis_con_este_interno) > 0:
                    self.stdout.write(self.style.ERROR('Ya hay un taxi con el interno {} (fila {})'.format(interno, count)))
                    sys.exit()
                    
                vehiculo, created = VehiculoTransportePublico.objects.get_or_create(interno=interno,
                                            titular=organizacion,
                                            patente=dominio,
                                            tipo_permiso=licencia,
                                            modelo=modelo,
                                            anio_fabricacion=anio)

                vehiculo.condicion = condicion
                vehiculo.decreto = decreto
                vehiculo.decreto_fecha = decreto_fecha
                vehiculo.fecha_aprobacion = aprobado_fecha
                vehiculo.fecha_habilitacion = habilitacion_fecha
                vehiculo.fecha_actualizacion = actualizacion_fecha
                
                obs_internas = ''
                if len(observaciones_internas) > 0:
                    vehiculo.observado_o_a_revisar = True

                for obs in observaciones_internas:

                    obs_internas = '{}/n{}'.format(obs_internas, obs)

                vehiculo.observaciones_internas = obs_internas
                vehiculo.save()

                if created:
                    vehiculos_nuevos += 1
                    # self.stdout.write(self.style.SUCCESS('FILA {} Vehiculo nuevo: {}'.format(count, vehiculo)))
                else:
                    vehiculos_repetidos += 1
                
                if len(errores) > 0:
                    self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR LA LINEA {}'.format(count)))
                    for e in errores:
                        self.stdout.write(self.style.ERROR(e))
                    sys.exit(1)

        instanceCSV.procesado = True
        instanceCSV.save()

        self.stdout.write(self.style.SUCCESS('Archivo cargado con éxito, se cargaron {} registros'.format(count)))
        self.stdout.write(self.style.SUCCESS('{} titulares de autos nuevos, {} repetidos'.format(titulares_nuevos, titulares_repetidos)))
        self.stdout.write(self.style.SUCCESS('{} tipos de licencia nuevas, {} repetidas'.format(tipos_nuevos, tipos_repetidos)))
        self.stdout.write(self.style.SUCCESS('{} vehiculos nuevos, {} repetidos'.format(vehiculos_nuevos, vehiculos_repetidos)))
        self.stdout.write(self.style.SUCCESS('{} marcas nuevas, {} repetidas'.format(marcas_nuevas, marcas_repetidas)))
        self.stdout.write(self.style.SUCCESS('{} modelos nuevos, {} repetidos'.format(modelos_nuevos, modelos_repetidos)))
