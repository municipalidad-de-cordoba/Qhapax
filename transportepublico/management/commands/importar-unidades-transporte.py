#!/usr/bin/python
from django.core.management.base import BaseCommand
from transportepublico.models import VehiculoTransportePublicoMasivo, EmpresaTransportePublico, ModeloVehiculo, MarcaVehiculo
from portaldedatos.models import ArchivoCSV
from django.db import transaction
from django.utils.text import slugify
import csv
import sys


class Command(BaseCommand):
    help = """Comando para importar lista de transportes privados.
                Requiere un CSV cargado en el sistema. Se marcará como _procesado_
                luego de ejecutarse para evitar duplicados
            """

    def add_arguments(self, parser):
        parser.add_argument('--csv_id', type=int, help='ID del CSV cargado al sistema')
        parser.add_argument('--force', action='store_true', dest='force',
                            default=False, help="forzar a procesar el archivo")

    @transaction.atomic
    def handle(self, *args, **options):
        force = options['force']
        if force:
            self.stdout.write(self.style.WARNING('--- Forzando importación de CSV ---'))

        csv_id = options['csv_id']
        instancesCSV = ArchivoCSV.objects.filter(pk=csv_id)
        if len(instancesCSV) == 0:
            self.stdout.write(self.style.ERROR('El CSV id {} no existe'.format(csv_id)))
            sys.exit(1)

        instanceCSV = instancesCSV[0]
        if instanceCSV.procesado and not force:
            self.stdout.write(self.style.ERROR('El CSV id {} ya fue  procesado, use --force'.format(csv_id)))
            sys.exit(1)

        self.stdout.write(self.style.SUCCESS('Importando csv (id: {})'.format(instanceCSV.id)))

        #TIENEN QUE TENER ENCABEZADO!
        if not instanceCSV.tiene_fila_encabezado:
            self.stdout.write(self.style.ERROR('El CSV indica que no tiene encabezado. Se procesará igualemente'))

        fieldnames = ['NUMERO', 'INTERNO', 'DOMINIO', 'AÑO', 'MARCA', 'MODELO', 'NUM_MOTOR', 'NUM_CHASIS', 'EMPRESA']

        count = 0
        unidades_nuevas = 0
        unidades_repetidas = 0

        motores_duplicados = 0
        chasis_duplicados = 0
        errores = []

        with open(instanceCSV.archivo_local.path, encoding=instanceCSV.encoding) as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=instanceCSV.separado_por,
                                    quotechar=instanceCSV.contenedor_de_texto)

            header = reader.__next__()
            print(header)
            print(fieldnames)
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR('BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            for row in reader:
                count += 1
                # chequear los datos
                self.stdout.write(self.style.SUCCESS('{} Linea leida: {}'.format(count, row)))

                interno = slugify(row['INTERNO'].replace('_', '-'))
                empresa_str = row['EMPRESA'].strip()
                dominio = slugify(row['DOMINIO'].replace('_', '-'))
                modelo_str = row['MODELO'].strip()
                motor = row['NUM_MOTOR'].strip().replace(' ', '').replace('-', '').upper()
                chasis = row['NUM_CHASIS'].strip().replace(' ', '').replace('-', '').upper()
                anio = row['AÑO'].strip() or None
                marca_str = row['MARCA'].strip()
                obs = ''

                try:
                    unidad, created = VehiculoTransportePublicoMasivo.objects.get_or_create(dominio=dominio,
                                            interno=interno)
                except Exception as e:
                    self.stdout.write(self.style.ERROR(e))
                    errores.append('Error {}. Dominio: {} Interno: {}'.format(e, dominio, interno))
                    # controlamos que error se da para anotarlo en observaciones
                    dominio_repetido = VehiculoTransportePublicoMasivo.objects.filter(dominio=dominio).count() > 0
                    interno_repetido = VehiculoTransportePublicoMasivo.objects.filter(interno=interno).count() > 0

                    # de todas formas se crea el interno para que lo corrija el encargado de estos datos
                    unidad = VehiculoTransportePublicoMasivo.objects.create(dominio=dominio+'REPETIDO',
                                    interno=interno+'REPETIDO')

                    obs = ''
                    if dominio_repetido:
                        obs += 'Dominio repetido.'

                    if interno_repetido:
                        obs += 'Interno repetido.'

                    unidad.observaciones = obs
                    unidad.hay_que_revisar = obs != ''
                    unidad.save()

                if created:
                    unidades_nuevas += 1
                else:
                    unidades_repetidas += 1

                if empresa_str in ['AUTOBUSES CORDOBA S.R.L.']:
                    empresa = EmpresaTransportePublico.objects.get(nombre_publico='Autobuses Córdoba')
                elif empresa_str in ['TAMSE']:
                    empresa = EmpresaTransportePublico.objects.get(nombre_publico='TAMSE')
                elif empresa_str in ['ERSA', 'ERSA URBANO S.A.']:
                    empresa = EmpresaTransportePublico.objects.get(nombre_publico='ERSA')
                elif empresa_str in ['CONIFERAL']:
                    empresa = EmpresaTransportePublico.objects.get(nombre_publico='Coniferal')
                else:
                    self.stdout.write(self.style.ERROR('Empresa no reconocida {}'.format(empresa_str)))
                    sys.exitr(1)

                # evitar duplicados (la base es muy mala)
                unidades_con_motor_igual = VehiculoTransportePublicoMasivo.objects.filter(motor_numero=motor).exclude(id=unidad.id)
                existe_motor = len(unidades_con_motor_igual) > 0

                if existe_motor:
                    u = unidades_con_motor_igual[0]
                    obs += 'Número de MOTOR duplicado con {} (int {})'.format(u.dominio, u.interno)
                    motores_duplicados += 1
                    motor = '{}-ERROR{}'.format(motor, motores_duplicados)

                unidades_con_chasis_igual = VehiculoTransportePublicoMasivo.objects.filter(chasis_numero=chasis).exclude(id=unidad.id)
                existe_chasis = len(unidades_con_chasis_igual) > 0

                if existe_chasis:
                    u = unidades_con_chasis_igual[0]
                    obs += 'Número de CHASIS duplicado con {} (int {})'.format(u.dominio, u.interno)
                    chasis_duplicados += 1
                    chasis = '{}-ERROR{}'.format(chasis, chasis_duplicados)

                hay_que_revisar = obs != ''
                unidad.observaciones = obs if hay_que_revisar is True else ''
                unidad.empresa = empresa
                unidad.chasis_numero = chasis
                unidad.motor_numero = motor
                unidad.modelo_anio = anio

                marca, marca_created = MarcaVehiculo.objects.get_or_create(nombre=marca_str)
                modelo, modelo_created = ModeloVehiculo.objects.get_or_create(marca=marca, nombre=modelo_str)

                unidad.modelo = modelo
                unidad.hay_que_revisar = hay_que_revisar
                unidad.save()
                self.stdout.write(self.style.SUCCESS('Unidad nueva DOM {} INT {}'.format(dominio, interno)))

        if len(errores) > 0:
            self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR INTERNOS:{}'.format(len(errores))))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))

        instanceCSV.procesado = True
        instanceCSV.save()

        self.stdout.write(self.style.SUCCESS('Archivo cargado con éxito, se cargaron {} registros'.format(count)))
        self.stdout.write(self.style.SUCCESS('{} vehiculos nuevos, {} repetidos'.format(unidades_nuevas, unidades_repetidas)))
        self.stdout.write(self.style.SUCCESS('{} motores repetidos, {} chasis repetidos'.format(motores_duplicados, chasis_duplicados)))
