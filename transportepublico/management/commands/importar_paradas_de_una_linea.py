#!/usr/bin/python
"""
Importar paradas de transporte desde webservice del proveedor EFISAT asociadas a la bandera
"""
from django.utils.text import slugify
from django.core.management.base import BaseCommand
from transportepublico.models import *
from transportepublico import settings as settings_tp
from django.db import transaction
import sys
import datetime
import requests
import json
import xml.etree.ElementTree as etree
from django.contrib.gis.geos import Point


class Command(BaseCommand):
    help = """Comando para Importar paradas de todas las lineas de transporte desde webservice del proveedor EFISAT"""

    def add_arguments(self, parser):
        parser.add_argument('--linea',
                            type=str,
                            help='Nombre público de la línea de transporte'
                            )

    # @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando actualizacion de paradas de transporte'))
        fuente, created = FuenteDeDatos.objects.get_or_create(nombre='EFISAT')
        # requiere un POST con user y PASS

        headers = {'Content-Type': 'application/soap+xml'}

        tot_nuevos_postes = 0
        tot_repetidos_postes = 0
        errores = []
        tot_nuevas_paradas = 0
        tot_repetidas_paradas = 0
        tot_actualizadas_paradas = 0

        linea_str = options['linea']
        linea_obj = LineaTransportePublico.objects.filter(nombre_publico=linea_str)

        if linea_obj.count() > 0:
            linea = linea_obj[0]
            self.stdout.write(self.style.SUCCESS(' ********* \n Buscando paradas línea {} {} {} \n ************ \n'.format(linea.nombre_publico, linea.empresa.nombre_publico, linea.id_externo)))

            body = """<?xml version="1.0" encoding="utf-8"?>
                      <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                        <soap12:Body>
                          <RecuperarParadasCompletoPorLinea xmlns="http://clsw.smartmovepro.net/">
                            <usuario>{user}</usuario>
                            <clave>{passw}</clave>
                            <codigoLineaParada>{linea_id_externo}</codigoLineaParada>
                          </RecuperarParadasCompletoPorLinea>
                        </soap12:Body>
                      </soap12:Envelope>""".format(user=settings_tp.USER_WS_PROVEEDOR,
                                                  passw=settings_tp.PASS_WS_PROVEEDOR,
                                                  linea_id_externo=linea.id_externo
                                                  )

            url = settings_tp.URL_BASE_WS_PROVEEDOR
            self.stdout.write(self.style.SUCCESS('POST TO {}'.format(url)))
            response = requests.post(url, data=body, headers=headers)

            # print(response.text)
            # sys.exit(1)
            """
            <?xml version="1.0" encoding="utf-8"?>
            <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
              <soap12:Body>
                <RecuperarParadasCompletoPorLineaResponse xmlns="http://clsw.smartmovepro.net/">
                  <RecuperarParadasCompletoPorLineaResult>string</RecuperarParadasCompletoPorLineaResult>
                </RecuperarParadasCompletoPorLineaResponse>
              </soap12:Body>
            </soap12:Envelope>
            """

            data = etree.fromstring(response.text)
            sopa = data[0]  # soap:Envelope
            recupera = sopa[0]  # RecuperarParadasCompletoPorLineaResponse
            results = recupera[0]  # <RecuperarParadasCompletoPorLineaResult>
            results_json = json.loads(results.text)
            # print(results_json)
            # print(results_json['paradas'])
            # print(type(results_json['paradas']))
            # sys.exit(1)
            """
            {'MensajeEstado': 'ok', 'CodigoEstado': 0, 'paradas':
                {'10 V': [{'Codigo': '31975', 'AbreviaturaBandera': '10 V', 'Descripcion': 'C1000', 'Identificador': 'C1000', 'AbreviaturaBanderaGIT': '10 V', 'LongitudParada': '-64,273549', 'AbreviaturaAmpliadaBandera': 'A ITUZAINGO', 'LatitudParada': '-31,344511'},
                          {'Codigo': '31976', 'AbreviaturaBandera': '10 V', 'Descripcion': 'C1002', 'Identificador': 'C1002', 'AbreviaturaBanderaGIT': '10 V', 'LongitudParada': '-64,273496', 'AbreviaturaAmpliadaBandera': 'A ITUZAINGO', 'LatitudParada': '-31,342087'}
                          .
                          .
                          .
                          ]
                }
            }
            """

            # self.stdout.write(self.style.SUCCESS('{} sopa'.format(sopa)))
            # self.stdout.write(self.style.SUCCESS('{} recupera'.format(recupera)))
            # self.stdout.write(self.style.SUCCESS('RES[0] = {}'.format(results_json)))

            # self.stdout.write(self.style.SUCCESS('{} RESULTADOS'.format(len(results_json))))

            # chequear los datos
            nuevos_postes = 0
            repetidos_postes = 0

            nuevas_paradas = 0
            repetidas_paradas = 0
            actualizadas_paradas = 0

            if results_json['MensajeEstado'] == 'ok':
                for k, v in results_json['paradas'].items():
                    # k es el recorrido de la/s linea/s sobre la que estoy iterando
                    # v es la lista de dict donde cada dict tiene la forma:
                    # {'Codigo': '31975',
                    #  'AbreviaturaBandera': '10 V',
                    #  'Descripcion': 'C1000',
                    #  'Identificador': 'C1000',
                    #  'AbreviaturaBanderaGIT': '10 V',
                    #  'LongitudParada': '-64,273549',
                    #  'AbreviaturaAmpliadaBandera': 'A ITUZAINGO',
                    #  'LatitudParada': '-31,344511'}

                    try:
                        # solo si existe el recorrido importamos las paradas asociadas
                        recorrido = RecorridoLineaTransportePublico.objects.get(descripcion_corta=slugify(k))
                        self.stdout.write(self.style.SUCCESS('Recorrido {}'.format(slugify(k))))

                    except Exception as e:
                        #excepcion asociada a la falta de un recorrido
                        self.stdout.write(self.style.ERROR(e))
                        error_str = 'El recorrido {} de la linea {} no está registrado \n'.format(slugify(k) ,linea)
                        self.stdout.write(self.style.ERROR(error_str))
                        errores.append(error_str)
                        recorrido = None

                    if recorrido is not None:
                        for parada in v:
                            self.stdout.write(self.style.SUCCESS('{} paradas'.format(len(v))))

                            poste, created = PosteParadaTransportePublico.objects.get_or_create(id_externo=parada['Codigo'], fuente=fuente)
                            poste.descripcion = parada['Descripcion']
                            latitud = float(parada['LatitudParada'].replace(',', '.'))
                            longitud = float(parada['LongitudParada'].replace(',', '.'))
                            poste.ubicacion = Point(longitud, latitud)
                            poste.save()

                            if not created:
                                self.stdout.write(self.style.SUCCESS('Poste existente {}'.format(poste.id_externo)))
                                repetidos_postes += 1
                            else:
                                self.stdout.write(self.style.SUCCESS('Nuevo poste {}'.format(poste.id_externo)))
                                nuevos_postes += 1

                            # Por defecto se crea como verificado = True
                            parada, created = ParadaTransportePublico.objects.get_or_create(poste=poste, codigo=parada['Identificador'], fuente=fuente, linea=linea, recorrido=recorrido)
                            if not created:
                                repetidas_paradas += 1
                                parada.verificado = True
                                parada.save()

                            else:
                                nuevas_paradas += 1
                                self.stdout.write(self.style.SUCCESS('Nueva parada {}'.format(parada.codigo)))

                        self.stdout.write(self.style.SUCCESS('********* OK ********* \n Postes: nuevos:{} - repetidos:{}  \n paradas nuevas:{} - repetidas:{}'.format(nuevos_postes,
                                                                                                                                                                    repetidos_postes,
                                                                                                                                                                    nuevas_paradas,
                                                                                                                                                                    repetidas_paradas)))
                        tot_nuevos_postes += nuevos_postes
                        tot_repetidos_postes += repetidos_postes
                        tot_nuevas_paradas += nuevas_paradas
                        tot_repetidas_paradas += repetidas_paradas
                        tot_actualizadas_paradas += actualizadas_paradas

            else:
                #Mensaje de estado != ok
                error_str = 'Mensaje de estado {} - Código {} - Linea {}'.format(results_json['MensajeEstado'], results_json['CodigoEstado'], linea)
                self.stdout.write(self.style.ERROR(' Error: {}'.format(error_str)))
                errores.append(error_str)

        if len(errores) > 0:
            self.stdout.write(self.style.ERROR('{} errores'.format(len(errores))))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))

        self.stdout.write(self.style.SUCCESS('********* FIN ********* \n Postes: nuevos:{} - repetidos:{}  \n paradas nuevas:{} - repetidas:{} - actualizadas:{}'.format(tot_nuevos_postes,
                                                                                                                                                        tot_repetidos_postes,
                                                                                                                                                        tot_nuevas_paradas,
                                                                                                                                                        tot_repetidas_paradas,
                                                                                                                                                        tot_actualizadas_paradas)))
