#!/usr/bin/python
"""
Setear a 0 todas las frecuencias
"""
from django.core.management.base import BaseCommand
from transportepublico.models import EstacionAnioFrecuencias, AgrupadorFranjaHoraria, Frecuencia, LineaTransportePublico
from django.db import transaction
import sys


class Command(BaseCommand):
    help = """Comando para setear en 0 las frecuencias de los colectivos """

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando seteo...'))

        registros = 0
        errores = []

        estaciones_anio = EstacionAnioFrecuencias.objects.all()
        agrupador_dias = AgrupadorFranjaHoraria.objects.all()
        lineas = LineaTransportePublico.objects.filter(publicado=True)

        for estacion in estaciones_anio:
            for agrupador in agrupador_dias:
                    for linea in lineas:
                        try:
                            frecuencia = Frecuencia.objects.create(
                                    estacion=estacion, agrupador=agrupador,
                                    linea=linea, frecuencia_en_minutos_esperada=0,
                                    unidades_activas_esperadas=0, descripcion=""
                                )
                            frecuencia.save()
                            registros += 1
                        except Exception as e:
                            self.stdout.write(self.style.ERROR(e))
                            errores.append('Error: '.format(e))


        self.stdout.write(self.style.SUCCESS('Cantidad de errores: {}'.format(len(errores))))
        self.stdout.write(self.style.SUCCESS('Cantidad de frecuencias seteadas: {}'.format(registros)))
