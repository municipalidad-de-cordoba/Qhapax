#!/usr/bin/python
from django.core.management.base import BaseCommand
from random import randrange
from portaldedatos.models import ArchivoCSV
from organizaciones.models import Organizacion
from transportepublico.models import TipoServicioTransporte, VehiculoTransportePublico, MarcaVehiculo, ModeloVehiculo
from django.db import transaction
import csv
import sys
import datetime


class Command(BaseCommand):
    help = """Comando para importar lista de taxis.
                Requiere un CSV cargado en el sistema. Se marcará como _procesado_
                luego de ejecutarse para evitar duplicados
            """

    def add_arguments(self, parser):
        parser.add_argument('--csv_id', type=int, help='ID del CSV cargado al sistema')
        parser.add_argument('--offset', type=int, help='Saltar filas', default=0)
        parser.add_argument('--clean_all', type=bool, default=False, help='Boorar antes todos los taxis del sistema')
        parser.add_argument('--force', action='store_true', dest='force',
                            default=False, help="forzar a procesar el archivo")

    @transaction.atomic
    def handle(self, *args, **options):
        force = options['force']
        if force:
            self.stdout.write(self.style.WARNING('--- Forzando importación de CSV ---'))

        try:
            if force:
                instanceCSV = ArchivoCSV.objects.get(pk=options['csv_id'])
            else:
                instanceCSV = ArchivoCSV.objects.get(pk=options['csv_id'], procesado=False)
        except ArchivoCSV.DoesNotExist:
            if force:
                self.stdout.write(self.style.ERROR('El CSV id: %s no existe' % options['csv_id']))
            else:
                self.stdout.write(self.style.ERROR('El CSV id: %s no existe o ya está procesado' % options['csv_id']))
            sys.exit(1)

        if instanceCSV is None:
            self.stdout.write(self.style.ERROR('No hay CSV'))
            sys.exit(1)

        self.stdout.write(self.style.SUCCESS('Importando csv (id: {})'.format(instanceCSV.id)))

        #TIENEN QUE TENER ENCABEZADO!
        if not instanceCSV.tiene_fila_encabezado:
            self.stdout.write(self.style.ERROR('El CSV indica que no tiene encabezado. Se procesará igualemente'))

        # asegurarse de que el archivo tenga la misma estructura (encabezados)
        fieldnames = ['Interno', 'Condición', 'Fecha Aprobado', 'Fecha Habilitación', 'Patente',
                        'Permisionario', 'Nº de Documento', 'Calle', 'Nº', 'Piso', 'Dpto.',
                        'Barrio', 'Marca', 'Modelo', 'Fecha de Actualización']

        count = 0
        titulares_nuevos = 0
        titulares_repetidos = 0

        tipos_nuevos = 0
        tipos_repetidos = 0

        marcas_nuevas = 0
        marcas_repetidas = 0

        modelos_nuevos = 0
        modelos_repetidos = 0

        vehiculos_nuevos = 0
        vehiculos_repetidos = 0

        licencia, created = TipoServicioTransporte.objects.get_or_create(nombre="Remis")
        licencia.save()

        clean_all = options['clean_all']
        if clean_all:
            remises = VehiculoTransportePublico.objects.filter(tipo_permiso=licencia)
            self.stdout.write(self.style.ERROR('EMILIMINANDO {} REMISES'.format(len(remises))))
            remises.delete()

        with open(instanceCSV.archivo_local.path) as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=instanceCSV.separado_por,
                                    quotechar=instanceCSV.contenedor_de_texto)

            header = reader.__next__()
            # hay campos vacios que se ven de formas raras. Limpiar:
            headers = [h for h in list(header.values()) if h != '' and type(h) != list]
            
            if sorted(fieldnames) != sorted(headers):
                self.stdout.write(self.style.ERROR('BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            offset = options['offset']

            for row in reader:
                count += 1
                if offset > count:
                    continue

                observaciones_internas = []

                # chequear los datos
                errores = []
                # self.stdout.write(self.style.SUCCESS('Linea leida: {}'.format(row)))

                interno = row['Interno'].strip()
                if interno == '' or len(interno) > 10:
                    uniq = "DESCONOC-{}".format(count)
                    observaciones_internas.append('INTERNO ERRÓNEO: {}. Se marcó como {}'.format(interno, uniq))
                    self.stdout.write(self.style.ERROR('INTERNO ERRÓNEO: {}. Se marcó como {}'.format(interno, uniq)))
                    interno = uniq
                    
                
                titular = row['Permisionario'].strip()
                cuit = row['Nº de Documento'].strip()
                
                condicion_str = row['Condición'].strip()

                if condicion_str in ['HABILITADO']:
                    condicion = VehiculoTransportePublico.COND_HABILITADO
                elif condicion_str in ['CADUCIDAD', 'CADUCA']:
                    condicion = VehiculoTransportePublico.COND_CADUCIDAD
                elif condicion_str in ['TRANSF', 'TRANSFEREN', 'TRANF']:
                    condicion = VehiculoTransportePublico.COND_TRANSF
                elif condicion_str in ['VACANTE']:
                    condicion = VehiculoTransportePublico.COND_VACANTE
                elif condicion_str in ['ASPIRANTE']:
                    condicion = VehiculoTransportePublico.COND_ASPIRANTE
                elif condicion_str in ['C/AMPARO', 'HABILI/AMP']:
                    condicion = VehiculoTransportePublico.COND_AMPARO
                elif condicion_str in ['DEPOSITO']:
                    condicion = VehiculoTransportePublico.COND_DEPOSITO
                elif condicion_str in ['TRANSF/FALL', 'TARNSF/FALL', 'TR.P/FALLE', 'TRANSF/EDAD']:
                    observaciones_internas.append('La condicion original decía {} y se marcó como TRANSF'.format(condicion_str))
                    condicion = VehiculoTransportePublico.COND_TRANSF
                elif condicion_str in ['', '-']:
                    observaciones_internas.append('La condicion original estaba vacía y se marcó como DESCONOCIDA')
                    condicion = VehiculoTransportePublico.COND_DESCONOCIDO
                else:
                    self.stdout.write(self.style.ERROR('Fila {} CONDICION DESCONOCIDA {}'.format(count, condicion_str)))
                    sys.exit(1)

                # al parecer en remis no esta o no corresponde
                decreto = ''
                decreto_fecha_str = None

                aprobado_fecha_str = row['Fecha Aprobado'].strip()
                if aprobado_fecha_str == '':
                    aprobado_fecha = None
                else:
                    try:
                        aprobado_fecha = datetime.datetime.strptime(aprobado_fecha_str, "%d/%m/%Y").date()
                    except Exception as e:
                        error = 'Fila {} Fecha de aprobado ({}) no es válida. ERROR: {}'.format(count, aprobado_fecha_str, e)
                        observaciones_internas.append('Fecha de Aprobado ({}) no es válida, queda vacía'.format(aprobado_fecha_str))
                        self.stdout.write(self.style.ERROR(error))
                        aprobado_fecha = None
                
                habilitacion_fecha_str = row['Fecha Habilitación'].strip()
                
                if habilitacion_fecha_str == '':
                    habilitacion_fecha = None
                else:
                    try:
                        habilitacion_fecha = datetime.datetime.strptime(habilitacion_fecha_str, "%d/%m/%Y").date()
                    except Exception as e:
                        error = 'Fila {}. Fecha de habilitacion ({}) no es válida. ERROR: {}'.format(count, habilitacion_fecha_str, e)
                        observaciones_internas.append('Fecha de habilitacion ({}) no es válida, queda vacía'.format(habilitacion_fecha_str))
                        self.stdout.write(self.style.ERROR(error))
                        habilitacion_fecha = None

                actualizacion_fecha_str = row['Fecha de Actualización'].strip()
                if actualizacion_fecha_str == '':
                    actualizacion_fecha = None
                else:
                    try:
                        actualizacion_fecha = datetime.datetime.strptime(actualizacion_fecha_str, "%d/%m/%Y").date()
                    except Exception as e:
                        error = 'Fila {} Fecha de actualizacion ({}) no es válida. ERROR: {}'.format(count, actualizacion_fecha_str, e)
                        self.stdout.write(self.style.ERROR(error))
                        observaciones_internas.append('Fecha de Actualizacion ({}) no es válida, queda vacía'.format(actualizacion_fecha_str))
                        actualizacion_fecha = None

                domicilio_calle = row['Calle'].strip()
                domicilio_nro = row['Nº'].strip()
                domicilio_piso = row['Piso'].strip()
                domicilio_dpto = row['Dpto.'].strip()
                domicilio_barrio = row['Barrio'].strip()

                dominio = row['Patente'].strip()

                # se requiere separar el modelo de vehículos en dos, uno de autos y otro de permisos
                # de modo que si no hjay patente entonces no se cargue directamente el auto ...
                if dominio == '':
                    dominio = 'SIN ASIGNAR'  # es el default de patente vacía
                
                marca_str = row['Marca'].strip()

                modelos = {
                    '': {'marca': 'Desconocido', 'modelo': '-'},
                    'CHERY': {'marca': 'Chery', 'modelo': '-'}, 
                    'CHERY FULWIN': {'marca': 'Chery', 'modelo': 'Full Win'}, 
                    'CHEVROLET': {'marca': 'Chevrolet', 'modelo': '-'}, 
                    'CEHVROLET CORSA': {'marca': 'Chevrolet', 'modelo': 'Corsa'}, 
                    'CHEV AVEO': {'marca': 'Chevrolet', 'modelo': 'Aveo'}, 
                    'CHEV CLASSIC': {'marca': 'Chevrolet', 'modelo': 'Classic'}, 
                    'CHHEVROLET CLASSIC': {'marca': 'Chevrolet', 'modelo': 'Classic'}, 
                    'CHEV CORSA': {'marca': 'Chevrolet', 'modelo': 'Corsa'}, 
                    'CHEV PRISMA': {'marca': 'Chevrolet', 'modelo': 'Prisma'}, 
                    'CHEVROLET AGILE': {'marca': 'Chevrolet', 'modelo': 'Agile'}, 
                    'CHEVROLET ASTRA': {'marca': 'Chevrolet', 'modelo': 'Astra'}, 
                    'CHEVROLET AVEO': {'marca': 'Chevrolet', 'modelo': 'Aveo'}, 
                    'CHEVROLET CLASSIC': {'marca': 'Chevrolet', 'modelo': 'Classic'}, 
                    'CHEVROLET CLASSC': {'marca': 'Chevrolet', 'modelo': 'Classic'}, 
                    'CHEVROLET COBALT': {'marca': 'Chevrolet', 'modelo': 'Cobalt'}, 
                    'CHEBROLET COBALT': {'marca': 'Chevrolet', 'modelo': 'Cobalt'}, 
                    'CHEVROLET COLBAT': {'marca': 'Chevrolet', 'modelo': 'Cobalt'}, 
                    'CHEVROLET CORSA': {'marca': 'Chevrolet', 'modelo': 'Corsa'}, 
                    'CHEVROLET CORSA II': {'marca': 'Chevrolet', 'modelo': 'Corsa II'}, 
                    'CHEV CORSA II': {'marca': 'Chevrolet', 'modelo': 'Corsa II'}, 
                    'CHEVROLET MERIVA': {'marca': 'Chevrolet', 'modelo': 'Meriva'}, 
                    'CHEV MERIVA': {'marca': 'Chevrolet', 'modelo': 'Meriva'}, 
                    'CHEVROLET MONZA': {'marca': 'Chevrolet', 'modelo': 'Monza'}, 
                    'CHEVROLET PRISMA': {'marca': 'Chevrolet', 'modelo': 'Prisma'}, 
                    'CHEVROLET WAGON': {'marca': 'Chevrolet', 'modelo': 'Wagon'}, 
                    'CHEVRROLET CORSA': {'marca': 'Chevrolet', 'modelo': 'Corsa'}, 
                    'CHVROLET CLASSIC': {'marca': 'Chevrolet', 'modelo': 'Classic'}, 
                    'CHEVROLET CELTA': {'marca': 'Chevrolet', 'modelo': 'Celta'}, 
                    'CHEVROLET SPIN': {'marca': 'Chevrolet', 'modelo': 'Spin'}, 
                    'CITROEN XSARA': {'marca': 'Citroën', 'modelo': 'Xsara'}, 
                    'DACIA': {'marca': 'Dacia', 'modelo': '-'}, 
                    'FIAT 125': {'marca': 'FIAT', 'modelo': '125'}, 
                    'FIAT 125': {'marca': 'FIAT', 'modelo': '125'}, 
                    'FIAT IDEA': {'marca': 'FIAT', 'modelo': 'Idea'}, 
                    'FIAT IVECO': {'marca': 'FIAT', 'modelo': 'Iveco'}, 
                    'FIAT DUNA': {'marca': 'FIAT', 'modelo': 'Duna'}, 
                    'FIAT PALIO': {'marca': 'FIAT', 'modelo': 'Palio'}, 
                    'FIAT PALIO W': {'marca': 'FIAT', 'modelo': 'Palio'}, 
                    'FIAT PUNTO': {'marca': 'FIAT', 'modelo': 'Punti'}, 
                    'FIAT PUNTO': {'marca': 'FIAT', 'modelo': 'Punti'}, 
                    'FIAT REGATTA': {'marca': 'FIAT', 'modelo': 'Regatta'}, 
                    'FIAT SIENA': {'marca': 'FIAT', 'modelo': 'Siena'}, 
                    'FIAT TEMPRA': {'marca': 'FIAT', 'modelo': 'Tempra'}, 
                    'FIAT TEMPRA I.E.': {'marca': 'FIAT', 'modelo': 'Tempra'}, 
                    'FIAT UNO': {'marca': 'FIAT', 'modelo': 'Uno'}, 
                    'FIAT UNO WAY': {'marca': 'FIAT', 'modelo': 'Uno'}, 
                    'FITA SIENA': {'marca': 'FIAT', 'modelo': 'Siena'}, 
                    'FIAT QUBO': {'marca': 'FIAT', 'modelo': 'Qubo'}, 
                    'FORD': {'marca': 'Ford', 'modelo': '-'}, 
                    'FORD GALAXY': {'marca': 'Ford', 'modelo': 'Galaxy'}, 
                    'FORD ECCO SPORT': {'marca': 'Ford', 'modelo': 'EcoSport'}, 
                    'FORD ECOSPORT': {'marca': 'Ford', 'modelo': 'EcoSport'}, 
                    'FORD ESCORT': {'marca': 'Ford', 'modelo': 'Escort'}, 
                    'FORD FIESTA': {'marca': 'Ford', 'modelo': 'Fiesta'}, 
                    'KIA CLARUS': {'marca': 'Kia', 'modelo': 'Clarus'}, 
                    'MERCEDES BENZ SPRINTER': {'marca': 'Mercedes Benz', 'modelo': 'Sprinter'}, 
                    'MERCEDES BENZ 1314': {'marca': 'Mercedes Benz', 'modelo': '1314'}, 
                    'NISSAN TIIDA': {'marca': 'Nissan', 'modelo': 'Tiida'}, 
                    'PEUGEOT': {'marca': 'Peugeot', 'modelo': '-'}, 
                    'PEUGEOT 206': {'marca': 'Peugeot', 'modelo': '206'}, 
                    'PEUGEOT 306 ST': {'marca': 'Peugeot', 'modelo': '306'}, 
                    'PEUGEOT 207': {'marca': 'Peugeot', 'modelo': '207'}, 
                    'PEUGUEOT 207': {'marca': 'Peugeot', 'modelo': '207'}, 
                    'PEUGEOT 306 D': {'marca': 'Peugeot', 'modelo': '306'}, 
                    'PEUGEOT 307': {'marca': 'Peugeot', 'modelo': '307'}, 
                    'PEUGEOT 405': {'marca': 'Peugeot', 'modelo': '405'}, 
                    'PEUGEOT 405 GRD': {'marca': 'Peugeot', 'modelo': '405'}, 
                    'PEUGUEOT 408': {'marca': 'Peugeot', 'modelo': '408'}, 
                    'PEUGEOT 504': {'marca': 'Peugeot', 'modelo': '504'}, 
                    'PEUGEOT 505': {'marca': 'Peugeot', 'modelo': '505'}, 
                    'PEUGEOT PARTNER': {'marca': 'Peugeot', 'modelo': 'Partner'}, 
                    'PEUGEOT PATNER': {'marca': 'Peugeot', 'modelo': 'Partner'}, 
                    'RENAULT 9': {'marca': 'Renault', 'modelo': '9'}, 
                    'RENAULT 11': {'marca': 'Renault', 'modelo': '11'}, 
                    'RENAULT 12': {'marca': 'Renault', 'modelo': '12'}, 
                    'RENAULT 19': {'marca': 'Renault', 'modelo': '19'}, 
                    'RENAULT CLIO': {'marca': 'Renault', 'modelo': 'Clio'}, 
                    'RENAULT DUSTER': {'marca': 'Renault', 'modelo': 'Duster'}, 
                    'RENAULT LOGAN': {'marca': 'Renault', 'modelo': 'Logan'}, 
                    'LOGAN': {'marca': 'Renault', 'modelo': 'Logan'}, 
                    'KANGOO': {'marca': 'Renault', 'modelo': 'Kangoo'}, 
                    'CHEV LOGAN': {'marca': 'Renault', 'modelo': 'Logan'},  # ????????????????????????????
                    'RENAULT FLUENCE': {'marca': 'Renault', 'modelo': 'Fluence'}, 
                    'RENAULT MEGANE': {'marca': 'Renault', 'modelo': 'Megane'}, 
                    'RENAULT SANDERO': {'marca': 'Renault', 'modelo': 'Sandero'}, 
                    'RENAULT SYMBOL': {'marca': 'Renault', 'modelo': 'Symbol'}, 
                    'SEAT CORDOBA': {'marca': 'Seat', 'modelo': 'Cordoba'}, 
                    'SUZUKI FUN': {'marca': 'Suzuki', 'modelo': 'Fun'}, 
                    'TOYOTA COROLLA': {'marca': 'Toyota', 'modelo': 'Corolla'}, 
                    'TOYOTA ETIOS': {'marca': 'Toyota', 'modelo': 'Etios'}, 
                    'GOL COUNTRY': {'marca': 'Volkswagen', 'modelo': 'Gol Country'}, 
                    'V W GOL COUNTRY': {'marca': 'Volkswagen', 'modelo': 'Gol Country'}, 
                    'V W VOYAGE': {'marca': 'Volkswagen', 'modelo': 'Voyage'}, 
                    'VOLKSWAGEN': {'marca': 'Volkswagen', 'modelo': '-'}, 
                    'VW BORA': {'marca': 'Volkswagen', 'modelo': 'Bora'}, 
                    'VW COUNTRY': {'marca': 'Volkswagen', 'modelo': 'Gol Country'}, 
                    'VW CROSS FOX': {'marca': 'Volkswagen', 'modelo': 'Cross Fox'}, 
                    'VW FOX': {'marca': 'Volkswagen', 'modelo': 'Cross Fox'}, 
                    'VW GOL': {'marca': 'Volkswagen', 'modelo': 'Gol'}, 
                    'WV GOL': {'marca': 'Volkswagen', 'modelo': 'Gol'}, 
                    'VW GOL COUNTRY': {'marca': 'Volkswagen', 'modelo': 'Gol Country'}, 
                    'VW GOL TREND': {'marca': 'Volkswagen', 'modelo': 'Gol Trend'}, 
                    'VW POINTER CLI': {'marca': 'Volkswagen', 'modelo': 'Pointer'}, 
                    'VW POLO': {'marca': 'Volkswagen', 'modelo': 'Polo'}, 
                    'VW POLO D': {'marca': 'Volkswagen', 'modelo': 'Polo'}, 
                    'VW SENDA': {'marca': 'Volkswagen', 'modelo': 'Senda'}, 
                    'VW SURAN': {'marca': 'Volkswagen', 'modelo': 'Suran'}, 
                    'VW VOYAGE': {'marca': 'Volkswagen', 'modelo': 'Voyage'},
                    'VW CROSS UP': {'marca': 'Volkswagen', 'modelo': 'Cross Up'},
                    'FIAT CRONOS': {'marca': 'Fiar', 'modelo': 'Cronos'},
                    'CEVROLET PRISMA': {'marca': 'Chevrolet', 'modelo': 'Prisma'},
                }

                try:
                    este = modelos[marca_str]
                except Exception as e:
                    self.stdout.write(self.style.ERROR('Fila {} ERROR Vehiculo {}: {}'.format(count, marca_str, e)))
                    sys.exit(1)

                try:
                    anio = int(row['Modelo'].strip())
                except Exception as e:
                    observaciones_internas.append('El año del modelo es inválido ({}) queda vacío'.format(row['Modelo'].strip()))
                    anio = 0

                # en base al CUIT, en primer lugar, asegurarse que exista la Empresa (organizacion)
                orgs = Organizacion.objects.filter(nombre=titular, CUIT=cuit)
                if len(orgs) > 1:
                    organizacion = orgs[0]
                    created = False
                else:    
                    organizacion, created = Organizacion.objects.get_or_create(nombre=titular, CUIT=cuit)
                    
                organizacion.domicilio_calle = domicilio_calle
                organizacion.domicilio_nro = domicilio_nro
                organizacion.domicilio_piso = domicilio_piso
                organizacion.domicilio_dpto = domicilio_dpto
                organizacion.domicilio_barrio = domicilio_barrio
                organizacion.save()

                if created:
                    titulares_nuevos += 1
                    # self.stdout.write(self.style.SUCCESS('Orga nueva: {}'.format(organizacion)))
                else:
                    titulares_repetidos += 1

                if created:
                    tipos_nuevos += 1
                else:
                    tipos_repetidos += 1

                marca, created = MarcaVehiculo.objects.get_or_create(nombre=este['marca'])
                marca.save()
                if created:
                    marcas_nuevas += 1
                else:
                    marcas_repetidas += 1

                modelo, created = ModeloVehiculo.objects.get_or_create(marca=marca, nombre=este['modelo'])
                modelo.save()
                if created:
                    modelos_nuevos += 1
                else:
                    modelos_repetidos += 1

                con_este_interno = VehiculoTransportePublico.objects.filter(interno=interno)
                if len(con_este_interno) > 0:
                    self.stdout.write(self.style.ERROR('Ya hay un remis con el interno {} (fila {})'.format(interno, count)))
                    interno2 = '{}-{}'.format(interno, count)
                    observaciones_internas.append('Ya existía un remis con el interno {}. Como no pueden duplicarse se crea el interno temporal {}'.format(interno, interno2))
                    interno = interno2

                vehiculo, created = VehiculoTransportePublico.objects.get_or_create(interno=interno, 
                                                                                titular=organizacion,
                                                                                patente=dominio,
                                                                                tipo_permiso=licencia,
                                                                                modelo=modelo,
                                                                                anio_fabricacion=anio)

                vehiculo.condicion = condicion
                vehiculo.decreto = decreto
                vehiculo.decreto_fecha = None
                vehiculo.fecha_aprobacion = aprobado_fecha
                vehiculo.fecha_habilitacion = habilitacion_fecha
                vehiculo.fecha_actualizacion = actualizacion_fecha
                
                obs_internas = ''
                if len(observaciones_internas) > 0:
                    vehiculo.observado_o_a_revisar = True

                for obs in observaciones_internas:

                    obs_internas = '{}\n{}'.format(obs_internas, obs)

                vehiculo.observaciones_internas = obs_internas
                vehiculo.save()

                if created:
                    vehiculos_nuevos += 1
                    # self.stdout.write(self.style.SUCCESS('FILA {} Vehiculo nuevo: {}'.format(count, vehiculo)))
                else:
                    vehiculos_repetidos += 1
                
                if len(errores) > 0:
                    self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR LA LINEA {}'.format(count)))
                    for e in errores:
                        self.stdout.write(self.style.ERROR(e))
                    sys.exit(1)

        instanceCSV.procesado = True
        instanceCSV.save()

        self.stdout.write(self.style.SUCCESS('Archivo cargado con éxito, se cargaron {} registros'.format(count)))
        self.stdout.write(self.style.SUCCESS('{} titulares de autos nuevos, {} repetidos'.format(titulares_nuevos, titulares_repetidos)))
        self.stdout.write(self.style.SUCCESS('{} tipos de licencia nuevas, {} repetidas'.format(tipos_nuevos, tipos_repetidos)))
        self.stdout.write(self.style.SUCCESS('{} vehiculos nuevos, {} repetidos'.format(vehiculos_nuevos, vehiculos_repetidos)))
        self.stdout.write(self.style.SUCCESS('{} marcas nuevas, {} repetidas'.format(marcas_nuevas, marcas_repetidas)))
        self.stdout.write(self.style.SUCCESS('{} modelos nuevos, {} repetidos'.format(modelos_nuevos, modelos_repetidos)))
