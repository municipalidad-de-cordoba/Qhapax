#!/usr/bin/python
from django.utils.text import slugify
from django.core.management.base import BaseCommand
from transportepublico.models import RecorridoLineaTransportePublico, LineaTransportePublico, FuenteDeDatos
from transportepublico import settings as settings_tp
from django.db import transaction
import sys
import datetime
import requests
import json
import xml.etree.ElementTree as etree
from django.contrib.gis.geos import Point, LineString


class Command(BaseCommand):
    help = """Comando para Importar recorridos de transporte desde webservice del proveedor EFISAT """

    def add_arguments(self, parser):
        parser.add_argument('--linea',
                            type=str,
                            help='Nombre público de la línea de transporte'
                            )

    # @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando actualizacion de recorridos de transporte'))
        fuente, created = FuenteDeDatos.objects.get_or_create(nombre='EFISAT')

        # requiere un POST con user y PASS
        headers = {'Content-Type': 'application/soap+xml; charset=utf-8'}

        errores = []
        tot_nuevos_recorridos = 0
        tot_repetidos_recorridos = 0
        tot_actualizados_recorridos = 0

        #traemos todas las lineas, creadas con el comando de empresas
        linea_str = fecha = options['linea']
        linea_obj = LineaTransportePublico.objects.filter(nombre_publico=linea_str)

        # iterar por todas las líneas
        recorridos_cargados = []
        lineas_falladas = []  #son frecuentes los errores por timeout
        puntos_de_recorridos = {} # diccionario para armar el trazado de los recorridos
        if linea_obj.count() > 0:
            linea = linea_obj[0]
            body = """<?xml version="1.0" encoding="utf-8"?>
                        <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                          <soap12:Body>
                            <RecuperarRecorridoParaMapaPorEntidadYLinea xmlns="http://clsw.smartmovepro.net/">
                              <usuario>{user}</usuario>
                              <clave>{passw}</clave>
                              <codigoLineaParada>{linea}</codigoLineaParada>
                            </RecuperarRecorridoParaMapaPorEntidadYLinea>
                          </soap12:Body>
                        </soap12:Envelope>""".format(user=settings_tp.USER_WS_PROVEEDOR,
                                                        passw=settings_tp.PASS_WS_PROVEEDOR,
                                                        linea=linea.id_externo)

            url = settings_tp.URL_BASE_WS_PROVEEDOR
            # self.stdout.write(self.style.SUCCESS('Linea {} ({}) {} nuevos {} repetidos'.format(linea.nombre_publico, linea.empresa.nombre_publico, tot_nuevos_recorridos, tot_repetidos_recorridos)))
            try:
                response = requests.post(url, data=body, headers=headers, timeout=15.0)
            except Exception as e:
                lineas_falladas.append(linea)
                self.stdout.write(self.style.ERROR('ERROR trayendo datos: {}'.format(e)))

            # self.stdout.write(self.style.SUCCESS('RESPUESTA: {}'.format(response.text)))
            ''' MUESTRA
                <?xml version="1.0" encoding="utf-8"?>
                <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                  <soap12:Body>
                    <RecuperarRecorridoParaMapaAbrevYAmpliPorEntidadYLineaResponse xmlns="http://clsw.smartmovepro.net/">
                      <RecuperarRecorridoParaMapaAbrevYAmpliPorEntidadYLineaResult>
                        {"CodigoEstado":0,"MensajeEstado":"ok","puntos":
                            [{"Descripcion":"431;IC43;IC43","AbreviaturaBanderaSMP":null,"Latitud":-31.403878,"Longitud":-64.269086},
                             {"Descripcion":"431;IC43;IC43","AbreviaturaBanderaSMP":null,"Latitud":-31.406213,"Longitud":-64.269113},
                             .
                             .
                             .
                            ]
                        }
                      </RecuperarRecorridoParaMapaAbrevYAmpliPorEntidadYLineaResult>
                    </RecuperarRecorridoParaMapaAbrevYAmpliPorEntidadYLineaResponse>
                  </soap12:Body>
                </soap12:Envelope>
            '''

            data = etree.fromstring(response.text)

            sopa = data[0]  # soap:Envelope
            recupera = sopa[0]  # RecuperarRecorridoParaMapaPorEntidadYLineaResponse
            results = recupera[0]  # <RecuperarRecorridoParaMapaPorEntidadYLineaResult>
            result_json = json.loads(results.text)

            """
            results_json = {"CodigoEstado: xxxx, MensajeEstado: xxxx, puntos:[{}]"}
            """
            nuevos_recorridos = 0
            actualizados_recorridos = 0
            repetidos_recorridos = 0
            actualizado = False
            contador_while = 0
            control_while = True

            while contador_while <= 1000 and control_while:
                if result_json['MensajeEstado'] == 'ok':
                    for punto in result_json['puntos']:

                        try:
                            este = {}

                            partes = punto['Descripcion'].split(';')

                            if len(partes) == 2:
                                este['id_externo'] = partes[0]
                                este['descr_larga'] = partes[1]
                            else:
                                err = 'PARSE ERROR Descripcion 2 partes: {} (tag:Descripcion)'.format(punto['Descripcion'])
                                errores.append(err)
                                self.stdout.write(self.style.ERROR(err))
                                este['id_externo'] = None
                                continue

                            este['latitud'] = punto['Latitud']

                            este['longitud'] = punto['Longitud']

                        except Exception as e:
                            self.stdout.write(self.style.ERROR('PARSE ERROR <{}>'.format(e)))
                            sys.exit(1)

                        #EFISAT usa esto como identificador de recorrido!!
                        descr_corta = slugify(punto['AbreviaturaBanderaSMP'])
                        self.stdout.write(self.style.SUCCESS('punto {} ({}) - linea {}'.format(este['id_externo'], descr_corta, linea.nombre_publico)))

                        if este['id_externo'] is None:
                            self.stdout.write(self.style.ERROR('IGNORANDO POR ERROR <{}>'.format(punto)))
                            continue

                        recorrido, created = RecorridoLineaTransportePublico.objects.get_or_create(id_externo=int(este['id_externo']), linea=linea, fuente=fuente)

                        if created:
                            nuevos_recorridos += 1
                            recorrido.descripcion_corta = descr_corta
                            recorrido.descripcion_larga = este['descr_larga']
                        else:

                            if recorrido.descripcion_corta != descr_corta:
                                recorrido.descripcion_corta = descr_corta
                                actualizado = True
                                recorrido.save()

                            if recorrido.descripcion_larga != este['descr_larga']:
                                recorrido.descripcion_larga = este['descr_larga']
                                actualizado = True
                                recorrido.save()

                        if actualizado:
                            actualizados_recorridos += 1
                            actualizado = False

                        if recorrido not in recorridos_cargados:
                            self.stdout.write(self.style.SUCCESS('RECORRIDO {}'.format(recorrido.descripcion_larga)))
                            recorridos_cargados.append(recorrido)
                            puntos_de_recorridos[recorrido.id] = []
                            if not created:
                                repetidos_recorridos += 1

                        latitud = este['latitud']
                        longitud = este['longitud']
                        puntos_de_recorridos[recorrido.id].append((longitud, latitud))

                    # Arma el trazado del recorrido uniendo los puntos
                    for r in puntos_de_recorridos:
                        recorrido = RecorridoLineaTransportePublico.objects.get(pk=r)
                        recorrido.trazado = LineString(puntos_de_recorridos[r])
                        recorrido.save()

                    tot_nuevos_recorridos += nuevos_recorridos
                    tot_repetidos_recorridos += repetidos_recorridos
                    tot_actualizados_recorridos += actualizados_recorridos

                    self.stdout.write(self.style.SUCCESS('{} recorridos nuevos, {} recorridos repetidos'.format(nuevos_recorridos, repetidos_recorridos)))
                    control_while = False
                    #si la llamada al api falló pero luego anduvo ok, removemos el error para no mostrarlo en el resumen final
                    if contador_while > 1:
                        errores.pop()

                else:
                    #Hubo error en la solicitud
                    err = 'Error en la solicitud a la línea {}. Respuesta del servidor: {}'.format(linea, result_json)
                    self.stdout.write(self.style.ERROR(err))
                    contador_while += 1
                    if contador_while == 1:
                        errores.append(err)

        if len(errores) > 0:
            self.stdout.write(self.style.ERROR('ERRORES'))
            for er in errores:
                self.stdout.write(self.style.ERROR('ERROR LINEA: {}'.format(er)))

        self.stdout.write(self.style.SUCCESS('******\n FIN \n****** \n'))

        nuevos_recorridos_str = '{} recorridos nuevos'.format(tot_nuevos_recorridos)
        recorridos_actualizados = '{} recorridos actualizados'.format(tot_actualizados_recorridos)
        repetidos_recorridos_str = '{} recorridos repetidos(sin actualizacion)'.format(tot_repetidos_recorridos)
        lineas_falladas_str = '{} lineas falladas !!!'.format(len(lineas_falladas))

        self.stdout.write(self.style.SUCCESS('{}. {}. {}. {}'.format(nuevos_recorridos_str, recorridos_actualizados, repetidos_recorridos_str, lineas_falladas_str)))
