#!/usr/bin/python
"""
Crear los troncales para el transporte público
"""
from django.core.management.base import BaseCommand
from transportepublico.models import EmpresaTransportePublico, LineaTransportePublico, TroncalTransportePublico
from django.db import transaction
import sys


class Command(BaseCommand):
    help = """Comando para crear lineas troncales en la base de datos """

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Creando lineas troncales..'))

        troncales = ['10', '20', '30', '40', '50', '60', '70', '80', 'BARRIALES', 'AEROPUERTO', '600', 'TROLEBUSES']
        troncal_nuevo = 0
        troncal_repetido = 0

        #creamos o agregamos los troncales
        for troncal in troncales:
            t, created = TroncalTransportePublico.objects.get_or_create(nombre_publico=troncal)
            if created:
                troncal_nuevo += 1
            else:
                troncal_repetido += 1

        #asignamos los troncales a las lineas publicadas
        lineas = LineaTransportePublico.objects.filter(publicado=True)
        for linea in lineas:
            if linea.nombre_publico in ['600', '601']:
                linea.troncal = TroncalTransportePublico.objects.get(nombre_publico='600')
            elif linea.nombre_publico in ['B30', 'B50', 'B60', 'B61', 'B70']:
                linea.troncal = TroncalTransportePublico.objects.get(nombre_publico='BARRIALES')
            elif linea.nombre_publico in ['A', 'B', 'C']:
                linea.troncal = TroncalTransportePublico.objects.get(nombre_publico='TROLEBUSES')
            elif linea.nombre_publico == 'AEROBUS':
                linea.troncal = TroncalTransportePublico.objects.get(nombre_publico='AEROPUERTO')
            else:
                if linea.nombre_publico.startswith('1'):
                    linea.troncal = TroncalTransportePublico.objects.get(nombre_publico='10')
                elif linea.nombre_publico.startswith('2'):
                    linea.troncal = TroncalTransportePublico.objects.get(nombre_publico='20')
                elif linea.nombre_publico.startswith('3'):
                    linea.troncal = TroncalTransportePublico.objects.get(nombre_publico='30')
                elif linea.nombre_publico.startswith('4'):
                    linea.troncal = TroncalTransportePublico.objects.get(nombre_publico='40')
                elif linea.nombre_publico.startswith('5'):
                    linea.troncal = TroncalTransportePublico.objects.get(nombre_publico='50')
                elif linea.nombre_publico.startswith('6'):
                    linea.troncal = TroncalTransportePublico.objects.get(nombre_publico='60')
                elif linea.nombre_publico.startswith('7'):
                    linea.troncal = TroncalTransportePublico.objects.get(nombre_publico='70')
                elif linea.nombre_publico.startswith('8'):
                    linea.troncal = TroncalTransportePublico.objects.get(nombre_publico='80')
            linea.save()

        self.stdout.write(self.style.SUCCESS('FIN. {} troncales nuevos, {} troncales repetidos'.format(troncal_nuevo, troncal_repetido)))
