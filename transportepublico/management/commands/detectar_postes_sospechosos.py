from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from django.db import transaction
from transportepublico.models import ParadaTransportePublico, LineaTransportePublico, RecorridoLineaTransportePublico, PosteParadaTransportePublico
# from django.contrib.gis.geos import Point
import sys
from django.conf import settings
from geopy import distance
import django_excel as excel
import os


class Command(BaseCommand):
    help = """Comando para detectar postes anómalos del transporte urbano"""

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Iniciando comando...'))

        postes = PosteParadaTransportePublico.objects.all()

        resumen = []
        for poste in postes:
            cantidad = ParadaTransportePublico.objects.filter(poste=poste).count()
            if cantidad > 14:
                poste_str = 'Poste id {} tiene {} paradas asociadas.'.format(poste.id, cantidad)
                resumen.append(poste_str)

        if len(resumen) > 0:
            for r in resumen:
                self.stdout.write(self.style.SUCCESS(r))
