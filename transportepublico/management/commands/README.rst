Comandos para Transporte Público de pasajeros
=============================================

Importar datos del webserver del proveedor.

.. code-block:: bash

  python manage.py importar_empresas_de_transporte_publico
  
  python manage.py importar_recorridos_transporte
  # en caso de que falle hacer llamadas individuales
  python manage.py importar_recorridos_de_una_linea  --linea NOMBRE_LINEA

  python manage.py importar_paradas_transporte
  # ver si es necesario
  python manage.py importar_paradas_de_una_linea
  
  # crear troncales y conectar automaticamente a las líneas
  python manage.py crear_troncales

  # darle nombres lindos a las descripciones de los recorridos basado en los barrios donde nacen y mueren
  python manage.py importar_origen_destino_a_lineas

  # calcular la distancia entre el poste y el recorrido para detectar aquellos sospechosamente lejanos
  python manage.py asociar_paradas_con_recorridos

  # detectar postes por el que pasa una cantidad sospechosa de recorridos
  python manage.py detectar_postes_sospechosos