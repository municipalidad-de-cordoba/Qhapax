#!/usr/bin/python
from django.core.management.base import BaseCommand
from transportepublico.models import EmpresaTransportePublico, LineaTransportePublico, FuenteDeDatos, RecorridoLineaTransportePublico, PosteParadaTransportePublico, ParadaTransportePublico
from django.db import transaction
import sys
import requests
import json
import csv
from django.contrib.gis.geos import Point


class Command(BaseCommand):
    help = """Comando para Importar paradas de transporte desde archivo excel"""

    def add_arguments(self, parser):
        parser.add_argument('--path', type=str, help='Path del archivo csv local')
        parser.add_argument('--linea', type=str, help='linea de la que quiero obtener las paradas')

    @transaction.atomic
    def handle(self, *args, **options):


        #las claves son las banderas antiguas(tal como vienen en el csv).
        # El valor es el id del recorrido correspondiente actualmente
        match_recorridos = {'PFMF': 1617, 'PFIM': 1618, 'MFPF': 1615, 'MIPF': 1616, #linea 30
                            'IPCO': 1710, 'COIP': 1709, 'COCE': 1708, 'CEMC': 1707, 'CECO': 1706, #linea 80
                            'CACO': 1715, 'COPC': 1714, 'COCH': 1713, 'CPCO': 1712, 'CHCO': 1711, #linea 81
                            'CAAF': 1718, 'LAAF': 1717, 'AFLA': 1716, #linea 82
                            'BEFE': 1720, 'FEBE': 1719, #linea 83
                            'PIMC': 1724, 'PICE': 1723, 'CEPI': 1722, 'CMPI': 1721, #linea 84
                            'CIPE': 1730, 'COCR': 1729, 'COCT': 1728, 'IPEC': 1727, 'CRCO': 1726, 'CTCO': 1725, #linea 85
                            'TEAE': 1734, 'AETE': 1733, #linea AEROBUS
                            'A V. RETIRO': 1742, 'A B. FRAGUEIRO': 1741 #linea B50
                            }

        # local MAYCOL
        # match_recorridos = {'PFMF': 47, 'PFIM': 48, 'MFPF': 45, 'MIPF': 46, #linea 30
        #                     'IPCO': 134, 'COIP': 133, 'COCE': 132, 'CEMC': 131, 'CECO': 130, #linea 80
        #                     'CACO': 139, 'COPC': 138, 'COCH': 137, 'CPCO': 136, 'CHCO': 135, #linea 81
        #                     'CAAF': 142, 'LAAF': 141, 'AFLA': 140, #linea 82
        #                     'BEFE': 144, 'FEBE': 143, #linea 83
        #                     'PIMC': 148, 'PICE': 147, 'CEPI': 146, 'CMPI': 145, #linea 84
        #                     'CIPE': 154, 'COCR': 153, 'COCT': 152, 'IPEC': 151, 'CRCO': 150, 'CTCO': 149, #linea 85
        #                     'TEAE': 158, 'AETE': 157, #linea AEROBUS
        #                     'A V. RETIRO': 166, 'A B. FRAGUEIRO': 165 #linea B50
        #                     }
        lineas = ['30', '80', '81', '82', '83', '84', '85', 'AEROBUS', 'B50']

        self.stdout.write(self.style.SUCCESS('Iniciando actualizacion de paradas de transporte de una ciudad'))
        fuente, created = FuenteDeDatos.objects.get_or_create(nombre='MUNICIPALIDAD DE CÓRDOBA')

        path = options['path']
        linea = options['linea']
        self.stdout.write(self.style.SUCCESS('Archivo ubicado en {} \n'.format(path)))

        # lee el archivo
        with open(path, encoding='latin-1', newline='') as File:
            reader = csv.DictReader(File, delimiter=';')

            total_postes_nuevos = 0
            total_paradas_nuevas = 0
            total_postes_repetidos = 0
            total_paradas_repetidas = 0
            for row in reader:
                #row es un dict de la siguiente forma:
                # {'Descripción': 'C5448',
                #  'Inteligente': 'NO',
                #  'Descripción sentido': 'SIN SENTIDO',
                #  'Calle principal': 'RAMOS MEJÍA',
                #  'Latitud': '-31,427752',
                #  'Descripción bandera': 'EMAC',
                #  'Empresa': 'CORDOBA',
                #  'Identificador': 'C5448',
                #  'Línea': 'B70',
                #  'Intersección': 'AGUSTÍN GARZÓN',
                #  'Localidad': 'CÓRDOBA',
                #  'Longitud': '-64,123303'}

                if row['Línea'] in lineas:
                    self.stdout.write(self.style.SUCCESS('Obteniendo parada de la línea {} \n'.format(row['Línea'])))
                    try:
                        recorrido = RecorridoLineaTransportePublico.objects.get(id=match_recorridos[row['Descripción bandera']])
                    except Exception as e:
                        self.stdout.write(self.style.ERROR('Recorrido {} no encontrado: {} \n.'.format(row['Descripción bandera'])))
                        #si no encuentro el recorrido, paso a la siguiente fila
                        break

                    latitud = float(row['Latitud'].replace(',', '.'))
                    longitud = float(row['Longitud'].replace(',', '.'))
                    ubicacion = Point(longitud, latitud)
                    try:
                        poste = PosteParadaTransportePublico.objects.get(ubicacion=ubicacion)
                        self.stdout.write(self.style.SUCCESS('Poste repetido{} \n'.format(row['Descripción'])))
                        total_postes_repetidos += 1
                    except Exception as e:
                        self.stdout.write(self.style.SUCCESS('Nuevo poste{} \n'.format(row['Descripción'])))
                        poste = PosteParadaTransportePublico.objects.create(fuente=fuente, descripcion=row['Descripción'],
                                sentido=row['Descripción sentido'], ubicacion=ubicacion, calle_principal=row['Calle principal'],
                                calle_secundaria=row['Intersección'])
                        total_postes_nuevos += 1

                    parada, created = ParadaTransportePublico.objects.get_or_create(poste=poste, codigo=row['Identificador'], fuente=fuente, linea=recorrido.linea, recorrido=recorrido)
                    if not created:
                        total_paradas_repetidas += 1
                        self.stdout.write(self.style.SUCCESS('Parada repetida{} \n'.format(row['Descripción'])))
                    else:
                        self.stdout.write(self.style.SUCCESS('Parada nueva{} \n'.format(row['Descripción'])))
                        total_paradas_nuevas += 1

        self.stdout.write(self.style.SUCCESS('********* FIN ********* \n Postes: nuevos:{} - repetidos:{}  \n paradas nuevas:{} - repetidas:{}'.format(total_postes_nuevos,
                                                                                                                                        total_postes_repetidos,
                                                                                                                                        total_paradas_nuevas,
                                                                                                                                        total_paradas_repetidas)))
