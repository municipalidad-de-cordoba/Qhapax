#!/usr/bin/python
from django.core.management.base import BaseCommand

from portaldedatos.models import ArchivoCSV
from organizaciones.models import Organizacion
from transportepublico.models import TipoServicioTransporte, VehiculoTransportePublico, MarcaVehiculo, ModeloVehiculo
from django.db import transaction
import csv
import sys


class Command(BaseCommand):
    help = """Comando para importar lista de transportes privados.
                Requiere un CSV cargado en el sistema. Se marcará como _procesado_
                luego de ejecutarse para evitar duplicados
            """

    def add_arguments(self, parser):
        parser.add_argument('--csv_id', type=int, help='ID del CSV cargado al sistema')
        parser.add_argument('--force', action='store_true', dest='force',
                            default=False, help="forzar a procesar el archivo")

    @transaction.atomic
    def handle(self, *args, **options):
        force = options['force']
        if force:
            self.stdout.write(self.style.WARNING('--- Forzando importación de CSV ---'))

        try:
            if force:
                instanceCSV = ArchivoCSV.objects.get(pk=options['csv_id'])
            else:
                instanceCSV = ArchivoCSV.objects.get(pk=options['csv_id'], procesado=False)
        except ArchivoCSV.DoesNotExist:
            if force:
                self.stdout.write(self.style.ERROR('El CSV id: %s no existe' % options['csv_id']))
            else:
                self.stdout.write(self.style.ERROR('El CSV id: %s no existe o ya está procesado' % options['csv_id']))
            sys.exit(1)

        if instanceCSV is None:
            self.stdout.write(self.style.ERROR('No hay CSV'))
            sys.exit(1)

        self.stdout.write(self.style.SUCCESS('Importando csv (id: {})'.format(instanceCSV.id)))

        #TIENEN QUE TENER ENCABEZADO!
        if not instanceCSV.tiene_fila_encabezado:
            self.stdout.write(self.style.ERROR('El CSV indica que no tiene encabezado. Se procesará igualemente'))

        # asegurarse de que el archivo tenga la misma estructura (encabezados)
        # fieldnames = ['INTERNO', 'LICENCIA', 'DOMINIO', 'TITULAR']
        fieldnames = ['INTERNO', 'SERVICIO', 'DOMINIO', 'MARCA', 'MODELO', 'TITULAR', 'CUIT']

        count = 0
        titulares_nuevos = 0
        titulares_repetidos = 0

        tipos_nuevos = 0
        tipos_repetidos = 0

        marcas_nuevas = 0
        marcas_repetidas = 0

        modelos_nuevos = 0
        modelos_repetidos = 0

        vehiculos_nuevos = 0
        vehiculos_repetidos = 0

        with open(instanceCSV.archivo_local.path) as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=instanceCSV.separado_por,
                                    quotechar=instanceCSV.contenedor_de_texto)

            header = reader.__next__()
            print(header)
            print(fieldnames)
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR('BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            for row in reader:
                count += 1
                # chequear los datos
                errores = []
                self.stdout.write(self.style.SUCCESS('Linea leida: {}'.format(row)))

                interno = row['INTERNO'].strip()
                licencia = row['SERVICIO'].strip()  
                licencia = {'PRIV': 'Privado', 'ESC': 'Escolar', 'E-P': 'Escolar Privado'}[licencia]

                dominio = row['DOMINIO'].strip()
                marca = row['MARCA'].strip()


                modelos = {
                    'VOLARE': {'marca': 'Volkswagen', 'modelo': 'Volare'}, 
                    'TRANSIT': {'marca': 'Ford', 'modelo': 'Transit'}, 
                    'TRANPORTER': {'marca': 'Volkswagen', 'modelo': 'Transporter'}, 
                    'TRAFIC': {'marca': 'Renault', 'modelo': 'Traffic'}, 
                    'TOYOTA': {'marca': 'Toyota', 'modelo': '-'}, 
                    'SPRINTER': {'marca': 'Mercedes Benz', 'modelo': 'Sprinter'}, 
                    'RENAULT MASTER': {'marca': 'Renault', 'modelo': 'Master'}, 
                    'PEUGEOT BOXER': {'marca': 'Peugeot', 'modelo': 'Boxer'}, 
                    'PEU BOXER': {'marca': 'Peugeot', 'modelo': 'Boxer'}, 
                    'METRO': {'marca': 'Metro', 'modelo': 'Metro'}, 
                    'MERCE SPRI': {'marca': 'Mercedes Benz', 'modelo': 'Sprinter'}, 
                    'MERCE BENZ': {'marca': 'Mercedes Benz', 'modelo': '-'}, 
                    'MERC SPRINTER': {'marca': 'Mercedes Benz', 'modelo': 'Sprinter'}, 
                    'MERC BENZ': {'marca': 'Mercedes Benz', 'modelo': '-'}, 
                    'MER BENZ': {'marca': 'Mercedes Benz', 'modelo': '-'}, 
                    'MB SPRINTER': {'marca': 'Mercedes Benz', 'modelo': 'Sprinter'}, 
                    'MASTER': {'marca': 'Renault', 'modelo': 'Master'}, 
                    'MARCOPOLO': {'marca': 'Mercedes Benz', 'modelo': 'Marco Polo'}, 
                    'M BENZ LO 814': {'marca': 'Mercedes Benz', 'modelo': '814'}, 
                    'M BENZ BMO 390': {'marca': 'Mercedes Benz', 'modelo': 'BMO 390'}, 
                    'M BENZ 814': {'marca': 'Mercedes Benz', 'modelo': '814'}, 
                    'M BENZ 311': {'marca': 'Mercedes Benz', 'modelo': '311'}, 
                    'M BENZ': {'marca': 'Mercedes Benz', 'modelo': '-'}, 
                    'KIA PREGIO': {'marca': 'Kia', 'modelo': 'Pregio'}, 
                    'KIA': {'marca': 'Kia', 'modelo': '-'}, 
                    'JUMPER': {'marca': 'Citroën', 'modelo': 'Jumper'}, 
                    'IVECO': {'marca': 'Iveco', 'modelo': '-'}, 
                    'IVECCO DAILY': {'marca': 'Iveco', 'modelo': 'Daily'}, 
                    'HYUNDAI H1': {'marca': 'Hyundai', 'modelo': 'H1'}, 
                    'HYUNDAI': {'marca': 'Hyundai', 'modelo': ''}, 
                    'FIAT DUCATO': {'marca': 'Fiat', 'modelo': 'Ducato'}, 
                    'FIAT DUCAT': {'marca': 'Fiat', 'modelo': 'Ducato'}, 
                    'FIAT DUCA': {'marca': 'Fiat', 'modelo': 'Ducato'}, 
                    'EXPERT': {'marca': 'Peugeot', 'modelo': 'Expert'}, 
                    'DUCATO': {'marca': 'Fiat', 'modelo': 'Ducato'},
                    'DAILY': {'marca': 'Iveco', 'modelo': 'Daily'}, 
                    'CHANG FEI': {'marca': 'Chang Fei', 'modelo': '-'}, 
                    'BRISTOL': {'marca': 'Bristol', 'modelo': ''}, 
                    'BOXER': {'marca': 'Peugeot', 'modelo': 'Boxer'}, 
                    'BENZ SPRINTER': {'marca': 'Mercedes Benz', 'modelo': 'Sprinter'}, 
                    'ASIA': {'marca': 'Asia', 'modelo': '-'}
                    }


                este = modelos[marca]

                anio = row['MODELO'].strip()
                titular = row['TITULAR'].strip()
                cuit = row['CUIT'].strip()
                

                # en base al CUIT, en primer lugar, asegurarse que exista la Empresa (organizacion)
                organizacion, created = Organizacion.objects.get_or_create(nombre=titular, CUIT=cuit)
                organizacion.save()

                if created:
                    titulares_nuevos += 1
                else:
                    titulares_repetidos += 1

                
                tipo, created = TipoServicioTransporte.objects.get_or_create(nombre=licencia)
                tipo.save()

                if created:
                    tipos_nuevos += 1
                else:
                    tipos_repetidos += 1


                marca, created = MarcaVehiculo.objects.get_or_create(nombre=este['marca'])
                marca.save()
                if created:
                    marcas_nuevas += 1
                else:
                    marcas_repetidas += 1


                modelo, created = ModeloVehiculo.objects.get_or_create(marca=marca, nombre=este['modelo'])
                modelo.save()
                if created:
                    modelos_nuevos += 1
                else:
                    modelos_repetidos += 1

                vehiculo, created = VehiculoTransportePublico.objects.get_or_create(interno=interno, 
                                                                                titular=organizacion,
                                                                                patente=dominio,
                                                                                tipo_permiso=tipo,
                                                                                modelo=modelo,
                                                                                anio_fabricacion=anio)
                vehiculo.save()
                if created:
                    vehiculos_nuevos += 1
                else:
                    vehiculos_repetidos += 1
                
                if len(errores) > 0:
                    self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR LA LINEA {}'.format(count)))
                    for e in errores:
                        self.stdout.write(self.style.ERROR(e))
                    sys.exit(1)

        instanceCSV.procesado = True
        instanceCSV.save()

        self.stdout.write(self.style.SUCCESS('Archivo cargado con éxito, se cargaron {} registros'.format(count)))
        self.stdout.write(self.style.SUCCESS('{} titulares de autos nuevos, {} repetidos'.format(titulares_nuevos, titulares_repetidos)))
        self.stdout.write(self.style.SUCCESS('{} tipos de licencia nuevas, {} repetidas'.format(tipos_nuevos, tipos_repetidos)))
        self.stdout.write(self.style.SUCCESS('{} vehiculos nuevos, {} repetidos'.format(vehiculos_nuevos, vehiculos_repetidos)))
        self.stdout.write(self.style.SUCCESS('{} marcas nuevas, {} repetidas'.format(marcas_nuevas, marcas_repetidas)))
        self.stdout.write(self.style.SUCCESS('{} modelos nuevos, {} repetidos'.format(modelos_nuevos, modelos_repetidos)))
