#!/usr/bin/python
from django.core.management.base import BaseCommand
from transportepublico.models import EmpresaTransportePublico, LineaTransportePublico, FuenteDeDatos, RecorridoLineaTransportePublico, PosteParadaTransportePublico, ParadaTransportePublico
from django.db import transaction
import sys
import requests
import json
import csv
from django.contrib.gis.geos import Point
import pandas as pd


class Command(BaseCommand):
    help = """Comando para Importar paradas de transporte desde archivo excel"""

    def add_arguments(self, parser):
        parser.add_argument('--path', type=str, help='Path del archivo csv local')
        parser.add_argument('--linea', type=str, help='linea de la que quiero obtener las paradas')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando actualizacion de paradas de transporte de una ciudad'))
        fuente, created = FuenteDeDatos.objects.get_or_create(nombre='MUNICIPALIDAD DE CÓRDOBA')

        path = options['path']
        linea = options['linea']
        self.stdout.write(self.style.SUCCESS('Archivo ubicado en {} \n'.format(path)))

        # lee el archivo
        # with open(path, encoding='utf-8', newline='') as File:
        reader = pd.read_csv(path, encoding='utf-8', skiprows=1, names=['codigo', 'calle', 'altura', 'interseccion', 'lat', 'lon', 'sentido'])

        total_postes_nuevos = 0
        total_paradas_nuevas = 0
        total_postes_repetidos = 0
        total_paradas_repetidas = 0
        total_paradas_actualizadas = 0

        for row in reader.iterrows():

            self.stdout.write(self.style.SUCCESS('Obteniendo parada de la línea C.\n'))

            if row[1]['sentido'] == 'IDA':
                recorrido = RecorridoLineaTransportePublico.objects.get(id=1750)

            else:
                recorrido = RecorridoLineaTransportePublico.objects.get(id=1749)

            latitud = float(row[1]['lat'].replace(',', '.'))
            longitud = float(row[1]['lon'].replace(',', '.'))
            ubicacion = Point(longitud, latitud)
            try:
                poste = PosteParadaTransportePublico.objects.get(ubicacion=ubicacion)
                self.stdout.write(self.style.SUCCESS('Poste repetido id {} \n'.format(poste.id)))
                total_postes_repetidos += 1
            except Exception as e:
                self.stdout.write(self.style.SUCCESS('Nuevo poste\n'))
                poste = PosteParadaTransportePublico.objects.create(fuente=fuente, descripcion=row[1]['codigo'],
                        sentido=row[1]['sentido'], ubicacion=ubicacion, calle_principal='{} {}'.format(row[1]['calle'], row[1]['altura']),
                        calle_secundaria=row[1]['interseccion'])
                total_postes_nuevos += 1

            try:
                parada = ParadaTransportePublico.objects.get(poste=poste, linea=recorrido.linea)

                if parada.recorrido.descripcion_corta != recorrido.descripcion_corta:
                    total_paradas_actualizadas += 1
                    parada.recorrido = recorrido
                    parada.save()
                    self.stdout.write(self.style.SUCCESS('Parada id {} actualizada.\n'.format(parada.id)))
                else:
                    total_paradas_repetidas += 1
                    self.stdout.write(self.style.SUCCESS('Parada id {} repetida.\n'.format(parada.id)))

            except Exception as e:
                parada = ParadaTransportePublico.objects.create(poste=poste, codigo=row[1]['codigo'], fuente=fuente, linea=recorrido.linea, recorrido=recorrido)
                self.stdout.write(self.style.SUCCESS('Parada id {} nueva.\n'.format(parada.id)))
                total_paradas_nuevas += 1

        self.stdout.write(self.style.SUCCESS('********* FIN ********* \n Postes: nuevos:{} - repetidos:{}  \n paradas nuevas:{} - actualizadas:{} repetidas:{}'.format(total_postes_nuevos,
                                                                                                                                        total_postes_repetidos,
                                                                                                                                        total_paradas_nuevas,
                                                                                                                                        total_paradas_actualizadas,
                                                                                                                                        total_paradas_repetidas)))
