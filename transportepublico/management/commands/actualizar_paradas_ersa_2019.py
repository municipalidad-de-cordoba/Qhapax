#!/usr/bin/python
from django.core.management.base import BaseCommand
from transportepublico.models import EmpresaTransportePublico, LineaTransportePublico, FuenteDeDatos, RecorridoLineaTransportePublico, PosteParadaTransportePublico, ParadaTransportePublico
from django.db import transaction
import sys
import requests
import json
import csv
from django.contrib.gis.geos import Point
import pandas as pd


class Command(BaseCommand):
    help = """Comando para Importar paradas de transporte desde archivo excel"""

    def add_arguments(self, parser):
        parser.add_argument('--path', type=str, help='Path del archivo csv local')
        parser.add_argument('--recorrido_id', type=str, help='linea de la que quiero obtener las paradas')

    @transaction.atomic
    def handle(self, *args, **options):

        fuente, created = FuenteDeDatos.objects.get_or_create(nombre='MUNICIPALIDAD DE CÓRDOBA')

        path = options['path']
        recorrido_id = options['recorrido_id']

        if path is None:
            self.stdout.write(self.style.ERROR('Ruta al archivo no especificada.\n'))
            sys.exit(1)
        if recorrido_id is None:
            self.stdout.write(self.style.ERROR('Id del recorrido no especificado.\n'))
            sys.exit(1)

        self.stdout.write(self.style.SUCCESS('Archivo ubicado en {} \n'.format(path)))
        try:
            recorrido = RecorridoLineaTransportePublico.objects.get(id=int(recorrido_id))
        except Exception:
            self.stdout.write(self.style.ERROR('EL id del recorrido no existe'))
            sys.exit(1)
        self.stdout.write(self.style.SUCCESS('Iniciando actualizacion de paradas de transporte del recorrido {}'.format(recorrido)))

        # lee el archivo
        # with open(path, encoding='utf-8', newline='') as File:
        reader = pd.read_csv(path, encoding='utf-8', skiprows=1,
            names=['numero', 'linea', 'otras_lineas', 'sentido', 'calle', 'altura', 'frente_a',
            'esquina', 'entre_calle1', 'entre_calle2', 'latitud', 'longitud'])

        total_postes_nuevos = 0
        total_paradas_nuevas = 0
        total_postes_repetidos = 0
        total_paradas_repetidas = 0

        postes = PosteParadaTransportePublico.objects.all()
        self.stdout.write(self.style.SUCCESS('Obteniendo lista de paradas.\n'))
        for row in reader.iterrows():

            identificador_poste = row[1]['numero']

            postes = PosteParadaTransportePublico.objects.filter(descripcion__icontains=identificador_poste)
            if postes.count() > 0:
                self.stdout.write(self.style.SUCCESS('Poste existente encontrado por código {}.\n'.format(postes[0].id)))
                poste = postes[0]
                poste.calle_principal = '{} {}'.format(row[1]['calle'], row[1]['altura'])
                poste.calle_secundaria = row[1]['esquina']
                poste.fuente = fuente
                poste.save()
                total_postes_repetidos += 1

                parada, created = ParadaTransportePublico.objects.get_or_create(poste=poste,
                    recorrido=recorrido, linea=recorrido.linea)
                parada.codigo = codigo=row[1]['numero']
                parada.fuente = fuente
                parada.save()

                if created:
                    total_paradas_nuevas += 1
                else:
                    total_paradas_repetidas += 1

            else:

                #se busca por ubicación
                latitud = float(row[1]['latitud'])
                longitud = float(row[1]['longitud'])
                ubicacion = Point(longitud, latitud)
                try:
                    poste = PosteParadaTransportePublico.objects.get(ubicacion=ubicacion)
                    self.stdout.write(self.style.SUCCESS('Poste existente encontrado por su ubicación {} \n'.format(poste.id)))
                    poste.calle_principal = '{} {}'.format(row[1]['calle'], row[1]['altura'])
                    poste.calle_secundaria = row[1]['esquina']
                    poste.fuente = fuente
                    poste.save()
                    total_postes_repetidos += 1

                    parada, created = ParadaTransportePublico.objects.get_or_create(poste=poste,
                        recorrido=recorrido, linea=recorrido.linea)
                    parada.codigo = codigo=row[1]['numero']
                    parada.fuente = fuente
                    parada.save()

                    if created:
                        total_paradas_nuevas += 1
                    else:
                        total_paradas_repetidas += 1

                except Exception as e:
                    self.stdout.write(self.style.SUCCESS('Nuevo poste\n'))
                    poste = PosteParadaTransportePublico.objects.create(fuente=fuente,
                            sentido=row[1]['sentido'], ubicacion=ubicacion,
                            calle_principal='{} {}'.format(row[1]['calle'], row[1]['altura']),
                            calle_secundaria=row[1]['esquina'])
                    total_postes_nuevos += 1

                    parada = ParadaTransportePublico.objects.create(poste=poste,
                        codigo=row[1]['numero'], fuente=fuente, linea=recorrido.linea,
                        recorrido=recorrido)
                    total_paradas_nuevas += 1

        self.stdout.write(self.style.SUCCESS('********* FIN ********* \n Postes: nuevos:{} - repetidos:{}  \n paradas nuevas:{} - repetidas:{}'.format(total_postes_nuevos,
                                                                                                                                        total_postes_repetidos,
                                                                                                                                        total_paradas_nuevas,
                                                                                                                                        total_paradas_repetidas)))
