from organizaciones.models import Organizacion  # no vamos a redefinir como es una empresa.
from simple_history.models import HistoricalRecords
from barrios.models import Barrio
from datetime import date, timedelta
from django.utils import timezone
from django.contrib.gis.db import models
import logging
logger = logging.getLogger(__name__)
from django.contrib.gis.geos import Point, LineString
from django.utils.text import slugify


class FuenteDeDatos(models.Model):
    ''' como podemos tener fuentes de datos variadas (propias + APIs de empresas) aqui asentamos quienes nos dan los datos '''
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre


class EmpresaTransportePublico(models.Model):
    '''Cada una de las empresas proveedores del servicio'''
    # ID en el sistema externo si hubiera un provedor
    id_externo = models.PositiveIntegerField(default=0)
    # debería ser una empresa válida en nuestro sistema
    organizacion = models.ForeignKey(Organizacion, null=True, blank=True,
                                        on_delete=models.SET_NULL)
    # nombre de fantasía o como la conoce el público general
    nombre_publico = models.CharField(max_length=250, null=True, blank=True)
    
    def __str__(self):
        return '' if self.nombre_publico is None else self.nombre_publico
    
    class Meta:
        ordering = ['nombre_publico']


class TroncalTransportePublico(models.Model):
    """
    Linea principal de un conjunto de lineas del transporte publico
    """
    nombre_publico = models.CharField(max_length=50)

    def __str__(self):
        return 'Troncal {}'.format(self.nombre_publico)
    
    class Meta:
        ordering = ['nombre_publico']


class LineaTransportePublico(models.Model):
    """
    cada una de las lineas o recorridos de transporte publico masivo
    """
    empresa = models.ForeignKey(EmpresaTransportePublico, on_delete=models.CASCADE)
    id_externo = models.PositiveIntegerField(default=0)
    nombre_publico = models.CharField(max_length=50)
    troncal = models.ForeignKey(TroncalTransportePublico, on_delete=models.SET_NULL, related_name='lineas', blank=True, null=True)
    # Agregamos el campo publicado por default en True. Cuando no este tildado significa que es porque la linea esta duplicada.
    # Con este campo vamos a poder excluir las lineas duplicadas
    publicado = models.BooleanField(default=True)
    fuente = models.ForeignKey(FuenteDeDatos, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return '{} ({})'.format(self.nombre_publico, self.empresa.nombre_publico if self.empresa.nombre_publico is not None else 'Desconocido')
    class Meta:
        ordering = ['empresa', 'nombre_publico']


class PosteParadaTransportePublico(models.Model):
    ''' cada una de las garitas o postes donde puede haber una o más paradas '''
    id_externo = models.CharField(max_length=90, null=True, blank=True)
    descripcion = models.CharField(max_length=90, null=True, blank=True)
    sentido = models.CharField(max_length=90, null=True, blank=True)
    calle_principal = models.CharField(max_length=90, null=True, blank=True)
    calle_secundaria = models.CharField(max_length=90, null=True, blank=True)
    ubicacion = models.PointField(null=True, blank=True, help_text=
                            'Punto exacto de ubicación del poste')
    fuente = models.ForeignKey(FuenteDeDatos, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return '{} ({})'.format(self.id_externo, self.descripcion)


class RecorridoLineaTransportePublico(models.Model):
    ''' cada uno de los recorridos de las lineas de transporte publico '''
    linea = models.ForeignKey(LineaTransportePublico, on_delete=models.CASCADE, related_name='recorridos')
    id_externo = models.PositiveIntegerField(default=0)
    descripcion_corta = models.CharField(max_length=90, null=True, blank=True, help_text='descripción que viene desde EFISAT')
    descripcion_larga = models.CharField(max_length=255, null=True, blank=True, help_text='descripción que viene desde EFISAT')
    trazado = models.LineStringField(null=True, blank=True, help_text=
                                    'Linea que representa el trazado del recorrido de una linea de transporte')
    descripcion_interna = models.TextField(null=True, blank=True, help_text='descripción más amplia del recorrido agregada manualmente')
    # Agregamos el campo publicado por default en True. Cuando no este tildado significa que es porque el recorrido esta duplicado.
    # Con este campo vamos a poder excluir los recorridos duplicados
    origen = models.ForeignKey(Barrio, null=True, blank=True, related_name='recorridos_origen')
    destino = models.ForeignKey(Barrio, null=True, blank=True, related_name='recorridos_destino')
    publicado = models.BooleanField(default=True)
    fuente = models.ForeignKey(FuenteDeDatos, null=True, blank=True, on_delete=models.SET_NULL)

    trazado_extendido = models.LineStringField(null=True, blank=True, 
                                    help_text='trazado con mas puntos intermedios para mejor análisis')
    
    def __str__(self):
        return '{} {} {}'.format(self.linea, self.id_externo, self.descripcion_corta)

    def save(self, *args, **kwargs):
        te = self.get_trazado_extendido() 
        if te is not None:
            self.trazado_extendido = te
        super().save(*args, **kwargs)
        

    def get_media_point(self, p1, p2):
        # obtener el punto medio entre otros dos
        x = p1[0] + ((p2[0] - p1[0]) / 2)
        y = p1[1] + ((p2[1] - p1[1]) / 2)
        
        punto_nuevo = (x, y)
        linea_tmp = LineString(p1, punto_nuevo)
        nueva_distancia = round(linea_tmp.length, 4)
        
        return punto_nuevo, nueva_distancia
    
    def split_linestring(self, p1, p2, max_length_line, level=0, ret=[]):
        # achicar un punto hasta que ande
        # logger.info('rec level {}'.format(level))
        linea_tmp = LineString(p1, p2)
        if p1 not in ret:
            ret.append(p1)
        
        if linea_tmp.length > max_length_line:
            punto_nuevo, nueva_distancia = self.get_media_point(p1, p2)
            
            if nueva_distancia < max_length_line:
                ret.append(punto_nuevo)
            else:
                # las dos partes deben seguirse dividiendo
                ret1 = self.split_linestring(p1=p1, p2=punto_nuevo,
                                                max_length_line=max_length_line, 
                                                level=level+1,
                                                ret=[])
                ret2 = self.split_linestring(p1=punto_nuevo, p2=p2, 
                                                max_length_line=max_length_line,
                                                level=level+1,
                                                ret=[])
                ret += ret1[1:]
                ret += ret2[1:]
        
        if p2 not in ret:
            ret.append(p2)
        return ret

    
    def get_trazado_extendido(self):
        # logger.info('Extendiendo Trazado')
        # toma el trazado oficial y crea uno nuevo con más puntos intermedios
        # devuelve ese traazado si puede y None si no

        """
rs = RecorridoLineaTransportePublico.objects.all()
c = 0
for r in rs:
    print('{} {} {}'.format(c, r, len(rs)))
    r.save()
    c += 1
        """
        max_length_line = 0.001
        
        if self.trazado is None:
            return None
        puntos_final = []
        c = 0
        agregados = 0
        for punto in self.trazado:
            # logger.info('Punto Trazado {}'.format(c))
            if c == 0:
                puntos_final.append(punto)
                punto_anterior = punto
                c += 1
                continue
            
            # ver si hay que cortarlo:
            ret = self.split_linestring(p1=punto_anterior, p2=punto,
                                        max_length_line=max_length_line, 
                                        level=0,
                                        ret=[])
            # logger.info('*************\n{}, {}\nTRABAJADO en {} partes\n*************'.format(punto_anterior, punto, len(ret)))
            puntos_final += ret[1:]  # quitar el primero que o es el primero de todos o es el último de la lista anterior
            punto_anterior = punto
            c += 1

        # logger.info('Puntos iniciales: {}, ahora: {}'.format(c, len(puntos_final)))
        return LineString(puntos_final)

    class Meta:
        unique_together = (("id_externo", "linea"),)


class ParadaTransportePublico(models.Model):
    ''' cada una de las lineas o recorridos de trasnporte publico masivo '''
    codigo = models.CharField(max_length=50, help_text='Codigo que viene desde el proveedor', null=True, blank=True)
    poste = models.ForeignKey(PosteParadaTransportePublico, related_name='paradas', on_delete=models.CASCADE)
    linea = models.ForeignKey(LineaTransportePublico, related_name='paradas', on_delete=models.CASCADE)
    recorrido = models.ForeignKey(RecorridoLineaTransportePublico, related_name='paradas', on_delete=models.CASCADE, null=True, blank=True)
    fuente = models.ForeignKey(FuenteDeDatos, null=True, blank=True, on_delete=models.SET_NULL)
    distancia_a_recorrido = models.DecimalField(max_digits=10, decimal_places=4, null=True, blank=True,
        help_text='Distancia en metros que calcula el sistema entre el poste y el punto más cercano del recorrido al que pertenece. Si la distancia es muy grande, controlar que esté en el recorrido correcto y marcar como verificado.')
    verificado = models.BooleanField(default=True, help_text='Marcar como verificado cuando se controle que el recorrido es correcto.')

    def __str__(self):
        return '{} ({})'.format(self.poste.id_externo, self.linea.id_externo)

    class Meta:
        unique_together = (("poste", "recorrido"),)
        permissions = (
            ('map_editor', 'Puede editar paradas en mapa'),)


class MarcaVehiculo(models.Model):
    ''' marcas de autos y otros vehiculos '''
    nombre = models.CharField(max_length=90, unique=True)

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']

class ModeloVehiculo(models.Model):
    ''' modelos de autos y otros vehiculos '''
    marca = models.ForeignKey(MarcaVehiculo, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=90)

    def __str__(self):
        return '{} {}'.format(self.marca.nombre, self.nombre)

    class Meta:
        unique_together = (("nombre", "marca"),)
        ordering = ['marca__nombre', 'nombre']


class TipoServicioTransporte(models.Model):
    ''' tipo de transporte que se ofrece al público (taxis, remis, escolar, privado, etc) '''
    nombre = models.CharField(max_length=90) 
    descripcion = models.TextField(null=True, blank=True) 

    def __str__(self):
        return self.nombre


class TipoCombustible(models.Model):
    nombre = models.CharField(max_length=45, unique=True)

    def __str__(self):
        return self.nombre


class TipoAdaptado(models.Model):
    ''' clase de adaptacion para personas con movilidad reducida u otras '''
    nombre = models.CharField(max_length=45, unique=True)

    def __str__(self):
        return self.nombre


class VehiculoTransportePublicoMasivo(models.Model):
    """ colectivos """
    interno = models.CharField(max_length=15, unique=True, 
                    help_text='Numero unico (no repetible en el futuro) de interno dentro del sistema')
    dominio = models.CharField(max_length=15, unique=True, help_text='Chapa patente')
    activo = models.BooleanField(default=True, help_text='El coche esta en período de actividad')
    modelo = models.ForeignKey(ModeloVehiculo, on_delete=models.SET_NULL, null=True, blank=True, related_name='colectivos')
    empresa = models.ForeignKey(EmpresaTransportePublico, on_delete=models.SET_NULL, null=True, blank=True, related_name='unidades')
    chasis_marca = models.CharField(max_length=60, null=True, blank=True)
    chasis_numero = models.CharField(max_length=60, null=True, blank=True, unique=True)
    motor_numero = models.CharField(max_length=60, null=True, blank=True, unique=True)
    adaptado = models.ManyToManyField(TipoAdaptado, blank=True)
    modelo_anio = models.PositiveIntegerField(null=True, blank=True)
    tanque_litros = models.PositiveIntegerField(null=True, blank=True)
    predio = models.CharField(max_length=60, null=True, blank=True)
    combustible = models.ForeignKey(TipoCombustible, null=True, blank=True, on_delete=models.SET_NULL)
    asientos = models.PositiveIntegerField(null=True, blank=True)
    fecha_alta = models.DateField(null=True, blank=True)
    fecha_baja = models.DateField(null=True, blank=True)
    expediente = models.CharField(max_length=60, null=True, blank=True)
    resolucion = models.CharField(max_length=60, null=True, blank=True)

    observaciones = models.TextField(null=True, blank=True)
    hay_que_revisar = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} int {}'.format(self.dominio, self.interno)

    def limpiar_dominio(self):
        dominio = slugify(self.dominio).replace('-', '')
        return dominio

    def limpiar_interno(self):
        interno = slugify(self.interno).replace('-', '')
        return interno

    def save(self, *args, **kwargs):
        """
        Se sobreescribe para que aplique slug al agregar o cambiar un interno
        """
        self.dominio = self.limpiar_dominio()
        self.interno = self.limpiar_interno()
        super(VehiculoTransportePublicoMasivo, self).save(*args, **kwargs)


class VehiculoTransportePublico(models.Model):
    ''' vehiculo al servicio de tranporte (privado, escolar, taxi, remis, etc) '''
    # codigo interno (supongo de habilitacion)
    interno = models.CharField(max_length=15, unique=True)
    titular = models.ForeignKey(Organizacion, null=True, blank=True, on_delete=models.SET_NULL)
    modelo = models.ForeignKey(ModeloVehiculo, on_delete=models.CASCADE, null=True, blank=True)
    anio_fabricacion = models.PositiveIntegerField(default=0)
    tipo_permiso = models.ForeignKey(TipoServicioTransporte, on_delete=models.CASCADE)

    # FIXME este modelo requiere que se separen el AUTOMOVIL y el PERMISO
    patente = models.CharField(max_length=15, default='SIN ASIGNAR')
    publicado = models.BooleanField(default=True)

    # FIXME cuando incluyamos la normativa municipal en Qhapax los decretos deberían ser modelos aquí
    decreto = models.CharField(max_length=40, null=True, blank=True)
    decreto_fecha = models.DateField(null=True, blank=True)

    fecha_aprobacion = models.DateField(null=True, blank=True)
    fecha_habilitacion = models.DateField(null=True, blank=True)
    fecha_actualizacion = models.DateField(null=True, blank=True)

    # FIXME este campo está y no hay datos suficientes para usar el modelo HabilitacionVehiculoTransporte
    # esto debería ser temporal y reemplazarse
    COND_HABILITADO = 10
    COND_CADUCIDAD = 20  # no seguro si es caducado
    COND_TRANSF = 30  # supongo que se esta transfiriendo, no confirmado
    COND_VACANTE = 40  # no estoy seguro lo que representa
    COND_DESCONOCIDO = 50
    COND_ASPIRANTE = 60
    COND_AMPARO = 70
    COND_DEPOSITO = 80

    condiciones = ((COND_HABILITADO, 'Habilitado'),
                        (COND_CADUCIDAD, 'Caducidad'),
                        (COND_TRANSF, 'Transf'),
                        (COND_VACANTE, 'Vacante'),
                        (COND_DESCONOCIDO, 'Desconocido'),
                        (COND_ASPIRANTE, 'Aspirante'),
                        (COND_AMPARO, 'Con amparo'),
                        (COND_DEPOSITO, 'Depósito'),
                        )
    
    condicion = models.PositiveIntegerField(choices=condiciones, default=COND_DESCONOCIDO)

    observaciones_publicas = models.TextField(null=True, blank=True)
    observaciones_internas = models.TextField(null=True, blank=True)

    observado_o_a_revisar = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()

    # No tienen fecha de habilitacion (por ahora), si estan aqui es porque están publicados
    def habilitado(self):
        return True

    def estado_habilitacion(self):
        return 'Habilitado'

    def __str__(self):
        modelo = 'SIN MODELO' if self.modelo is None else '{} {}'.format(self.modelo.nombre, self.modelo.marca.nombre)
        return '{} {} {}'.format(self.interno, modelo, self.patente)


class HabilitacionVehiculoTransporte(models.Model):
    ''' NO USADO, Eliminar si se confirma. Cada habilitacion de un vehiculo '''

    vehiculo = models.ForeignKey(VehiculoTransportePublico, on_delete=models.CASCADE)
    fecha_inicio_habilitacion = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    fecha_vencimiento_habilitacion = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    acta_habilitacion = models.FileField(upload_to='actas-habilitacion-vehicvulos/',
                    null=True, blank=True,
                    verbose_name='Acta de habilitación de cada vehiculo')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()

    def __str__(self):
        nombre = 'SIN TITULAR' if self.vehiculo.titular is None else self.vehiculo.titular.nombre
        return "{} [{}-{}]".format(nombre, self.fecha_inicio_habilitacion, self.fecha_vencimiento_habilitacion)

    class Meta:
        verbose_name_plural = "Habilitaciones de Vehiculos de transporte"
        verbose_name = "Habilitaciones Vehiculo Transporte"


class EstacionAnioFrecuencias(models.Model):
    ''' cada una de las estaciones del año donde son validas las frecuencias exigidas '''
    anio = models.IntegerField()
    nombre = models.CharField(max_length=30)
    activa = models.BooleanField(default=False)

    def __str__(self):
        return '{} {}'.format(self.nombre, self.anio)

    class Meta:
        verbose_name_plural = 'Estación del año para frecuencia'


class DiasGrupoFrecuencias(models.Model):
    ''' agrupación de días para computas las frecuencuas
        En general sera LaV, Sabados, Domingos y Feriados '''
    nombre = models.CharField(max_length=30)

    lunes = models.BooleanField(default=False)
    martes = models.BooleanField(default=False)
    miercoles = models.BooleanField(default=False)
    jueves = models.BooleanField(default=False)
    viernes = models.BooleanField(default=False)
    sabado = models.BooleanField(default=False)
    domingo = models.BooleanField(default=False)
    feriado = models.BooleanField(default=False)
    
    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Grupo de días para frecuencias'


class AgrupadorFranjaHoraria(models.Model):
    ''' Las franjas se agrupan en Horarios y se relacionan con los dias '''
    nombre = models.CharField(max_length=30)
    dias = models.ForeignKey(DiasGrupoFrecuencias, on_delete=models.CASCADE)

    def __str__(self):
        return '{} - {}'.format(self.nombre, self.dias.nombre)


class FranjaHorariaFrecuencias(models.Model):
    ''' agrupación de horas con diferentes frecuencias '''
    agrupador = models.ForeignKey(AgrupadorFranjaHoraria, null=True, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=30)
    hora_desde = models.TimeField()
    hora_hasta = models.TimeField()

    def __str__(self):
        return '{}: {} - {}'.format(self.nombre, self.hora_desde, self.hora_hasta)

    class Meta:
        verbose_name_plural = 'Franja Horaria de Frecuencias'


class Frecuencia(models.Model):
    estacion = models.ForeignKey(EstacionAnioFrecuencias, null=True, on_delete=models.CASCADE)
    agrupador = models.ForeignKey(AgrupadorFranjaHoraria, null=True, on_delete=models.CASCADE)
    linea = models.ForeignKey(LineaTransportePublico, null=True, on_delete=models.CASCADE)
    frecuencia_en_minutos_esperada = models.PositiveIntegerField(default=0)
    unidades_activas_esperadas = models.PositiveIntegerField(default=0)
    descripcion = models.TextField(null=True, blank=True)

    def get_full_data(self):
        return 'Línea: {}, {} minutos, {} unidades.\nEstacion: {}\nAgrupador:{} \n{}'.format(self.linea, self.frecuencia_en_minutos_esperada, self.unidades_activas_esperadas, self.estacion, self.agrupador, self.descripcion)

    def __str__(self):
        return '{} {} {} {}'.format(self.linea, self.frecuencia_en_minutos_esperada, self.agrupador, self.estacion) 

    # @classmethod
    # def esperadas(cls, momento, linea):
    #     # calcular lo esperado ahora o en un momento específico
    #     if momento == None:
    #         momento = timezone.now()
    #     es_feriado = False  #TODO detectar
    #     dia = momento.weekday()
    #     hora = momento.time()
    #     if es_feriado: flt_dia = Q(dias__feriado=True)
    #     elif dia == 0: flt_dia = Q(dias__lunes=True)
    #     elif dia == 1: flt_dia = Q(dias__martes=True)
    #     elif dia == 2: flt_dia = Q(dias__miercoles=True)
    #     elif dia == 3: flt_dia = Q(dias__jueves=True)
    #     elif dia == 4: flt_dia = Q(dias__viernes=True)
    #     elif dia == 5: flt_dia = Q(dias__sabado=True)
    #     elif dia == 6: flt_dia = Q(dias__domingo=True)

    #     qs = cls.objects.filter(flt_dia,
    #                             franja_horaria__hora_desde__lte=hora,
    #                             franja_horaria__hora_hasta__gt=hora,
    #                             linea = linea,
    #                             estacion__activa=True)
        
    #     # debería ser un solo elemento
    #     return qs
    
    # @classmethod
    # def frecuencia_esperada_minutos(cls, momento, linea):
    #     # calcular lo esperado ahora o en un momento específico
    #     qs = cls.frecuencias_esperadas(momento, linea)
    #     if len(qs) == 0:
    #         return (0, 0, 'Desconocido')
    #     else:
    #         q = qs[0]
    #         return (q.frecuencia_en_minutos_esperada, q.unidades_activas_esperadas, q.get_full_data())

    def calculo_unidades_activas_esperadas(self):
        if self.frecuencia_en_minutos_esperada == 0:
            self.unidades_activas_esperadas = 0
            return self.unidades_activas_esperadas
        else:
            # Este es el calculo que usan para unidades activas esperadas
            self.unidades_activas_esperadas = round(60/self.frecuencia_en_minutos_esperada, 0)
            return self.unidades_activas_esperadas

    def save(self, *args, **kwargs):
        self.calculo_unidades_activas_esperadas()
        super(Frecuencia, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Frecuencias'
        verbose_name = 'Frecuencia'
        unique_together = (("estacion", "agrupador", "linea"))
