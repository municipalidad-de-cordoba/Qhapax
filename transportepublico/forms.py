from django import forms
from .models import VehiculoTransportePublico
from organizaciones.forms import OrganizacionWidget


class VehiculoFormSelect2(forms.ModelForm):
    class Meta:
        model = VehiculoTransportePublico

        fields = '__all__'
        widgets = {'titular': OrganizacionWidget}
