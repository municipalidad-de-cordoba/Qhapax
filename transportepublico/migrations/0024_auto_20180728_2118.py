# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-07-29 00:18
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('transportepublico', '0023_auto_20180724_1128'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recorridolineatransportepublico',
            name='linea',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='recorridos', to='transportepublico.LineaTransportePublico'),
        ),
    ]
