# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-07-31 20:35
from __future__ import unicode_literals

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('transportepublico', '0007_auto_20170731_1650'),
    ]

    operations = [
        migrations.CreateModel(
            name='HabilitacionVehiculoTransporte',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_inicio_habilitacion', models.DateField(blank=True, null=True)),
                ('fecha_vencimiento_habilitacion', models.DateField(blank=True, null=True)),
                ('acta_habilitacion', models.FileField(blank=True, null=True, upload_to='actas-habilitacion-vehicvulos/', verbose_name='Acta de habilitación de cada vehiculo')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Habilitaciones Vehiculo Transporte',
                'verbose_name_plural': 'Habilitaciones de Vehiculos de transporte',
            },
        ),
        migrations.CreateModel(
            name='HistoricalHabilitacionVehiculoTransporte',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('fecha_inicio_habilitacion', models.DateField(blank=True, null=True)),
                ('fecha_vencimiento_habilitacion', models.DateField(blank=True, null=True)),
                ('acta_habilitacion', models.TextField(blank=True, max_length=100, null=True, verbose_name='Acta de habilitación de cada vehiculo')),
                ('created', models.DateTimeField(blank=True, editable=False)),
                ('modified', models.DateTimeField(blank=True, editable=False)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-history_date', '-history_id'),
                'verbose_name': 'historical Habilitaciones Vehiculo Transporte',
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='HistoricalVehiculoTransportePublico',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('interno', models.CharField(db_index=True, max_length=15)),
                ('patente', models.CharField(max_length=15)),
                ('created', models.DateTimeField(blank=True, editable=False)),
                ('modified', models.DateTimeField(blank=True, editable=False)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('modelo', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='transportepublico.ModeloVehiculo')),
                ('tipo_permiso', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='transportepublico.TipoServicioTransporte')),
            ],
            options={
                'ordering': ('-history_date', '-history_id'),
                'verbose_name': 'historical vehiculo transporte publico',
                'get_latest_by': 'history_date',
            },
        ),
        migrations.RemoveField(
            model_name='vehiculotransportepublico',
            name='habilitado_hasta',
        ),
        migrations.AddField(
            model_name='vehiculotransportepublico',
            name='created',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2017, 7, 31, 20, 35, 43, 362036, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='vehiculotransportepublico',
            name='modified',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2017, 7, 31, 20, 35, 51, 974929, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='historicalhabilitacionvehiculotransporte',
            name='vehiculo',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='transportepublico.VehiculoTransportePublico'),
        ),
        migrations.AddField(
            model_name='habilitacionvehiculotransporte',
            name='vehiculo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='transportepublico.VehiculoTransportePublico'),
        ),
    ]
