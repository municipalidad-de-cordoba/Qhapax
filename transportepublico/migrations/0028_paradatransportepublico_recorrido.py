# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-08-16 18:48
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('transportepublico', '0027_auto_20180816_1349'),
    ]

    operations = [
        migrations.AddField(
            model_name='paradatransportepublico',
            name='recorrido',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='paradas', to='transportepublico.RecorridoLineaTransportePublico'),
        ),
    ]
