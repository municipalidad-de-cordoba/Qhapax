# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-07-19 18:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('transportepublico', '0021_auto_20180521_1345'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paradatransportepublico',
            name='linea',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='paradas', to='transportepublico.LineaTransportePublico'),
        ),
        migrations.AlterField(
            model_name='paradatransportepublico',
            name='poste',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='paradas', to='transportepublico.PosteParadaTransportePublico'),
        ),
    ]
