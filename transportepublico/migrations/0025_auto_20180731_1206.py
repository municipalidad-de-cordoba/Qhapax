# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-07-31 15:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transportepublico', '0024_auto_20180728_2118'),
    ]

    operations = [
        migrations.AddField(
            model_name='recorridolineatransportepublico',
            name='descripcion_interna',
            field=models.TextField(blank=True, help_text='descripción más amplia del recorrido agregada manualmente', null=True),
        ),
        migrations.AlterField(
            model_name='recorridolineatransportepublico',
            name='descripcion_corta',
            field=models.CharField(blank=True, help_text='descripción que viene desde EFISAT', max_length=90, null=True),
        ),
        migrations.AlterField(
            model_name='recorridolineatransportepublico',
            name='descripcion_larga',
            field=models.CharField(blank=True, help_text='descripción que viene desde EFISAT', max_length=255, null=True),
        ),
    ]
