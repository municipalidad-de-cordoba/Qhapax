from django.apps import AppConfig


class RegistroCivilConfig(AppConfig):
    name = 'registro_civil'
