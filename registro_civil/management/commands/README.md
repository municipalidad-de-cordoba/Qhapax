# Comandos para tomar datos del Registro Civil

Tomar datos del registro civil y construir archivos estáticos y anonimizados con datos de interes

Primero que todo importar datos
```
python manage.py importar_nacimientos_registro_civil --fechaDesde 2017-01-01 --fechaHasta 2018-09-13 --force_update True
```

## Comandos

Con estos comandos se actualizan los archivos en producción
```
python manage.py generar-nacidos-por-fecha
python manage.py generar-ninies-registrados-por-semana
python manage.py registros-por-padres-y-demora-en-anotacion
python manage.py generar-uso-de-apellidos.py
```

python manage.py generar-nacidos-por-fecha && python manage.py generar-ninies-registrados-por-semana && python manage.py registros-por-padres-y-demora-en-anotacion && python manage.py generar-uso-de-apellidos.py
