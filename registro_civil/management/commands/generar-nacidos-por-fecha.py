#!/usr/bin/python
'''
Grabar lista de fechas de nacimiento de los niñes
'''
from django.core.management.base import BaseCommand
from django.conf import settings
import django_excel as excel
from django.db import transaction
import sys
import os
from cordobaplantavida.models import Progenitor, Nacido


class Command(BaseCommand):
    help = """Generar un CSV/XLS con todos las fechas de nacimiento"""
    # generar-nacidos-por-fecha

    def handle(self, *args, **options):
        
        self.stdout.write(self.style.SUCCESS('Niñes nacidos por día'))

        # obtener todos los nacidos y agruparlos por semana
        nacidos = Nacido.objects.all().order_by('fecha_nacimiento')
        
        csv_list = []
        csv_list.append(['fecha de nacimiento', 'fecha de registro', 'días de demora', 'oficina registrante'])
        
        for nacido in nacidos:
            # no vamos a revelar días pero si la semana del año del nacimiento
            oficina = '' if nacido.oficina_registrante is None else nacido.oficina_registrante.nombre

            fecha_nacimiento = nacido.fecha_nacimiento
            fecha_registro = nacido.fecha_registro
            
            if fecha_nacimiento is None or fecha_registro is None:
                # omitir
                continue
            
            # ver cuantos días se tardo en registrarlo
            diff_nac_registro = (fecha_registro - fecha_nacimiento).days
            
            csv_list.append([fecha_nacimiento, fecha_registro, diff_nac_registro, oficina])
        
        dest_csv = os.path.join(settings.MEDIA_ROOT, 'fechas-de-nacimiento.csv')
        dest_xls = os.path.join(settings.MEDIA_ROOT, 'fechas-de-nacimiento.xls')

        excel.pe.save_as(array=csv_list, dest_file_name=dest_csv)
        excel.pe.save_as(array=csv_list, dest_file_name=dest_xls)

        self.stdout.write(self.style.SUCCESS('Finalizado'))
