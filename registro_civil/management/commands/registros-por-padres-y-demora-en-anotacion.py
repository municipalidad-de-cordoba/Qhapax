#!/usr/bin/python
'''
Grabar lista de niños registrados en el registro civil por semana
'''
from django.core.management.base import BaseCommand
from django.conf import settings
import django_excel as excel
from django.db import transaction
import sys
import os
from cordobaplantavida.models import Progenitor, Nacido


class Command(BaseCommand):
    help = """Generar un CSV/XLS con todos los niñes Registrados en el registro Civil de la Ciudad"""
    # registros-por-padres-y-demora-en-anotacion

    def handle(self, *args, **options):
        for anio in range(2016, 2020):
            self.stdout.write(self.style.SUCCESS('Niños registrados en {}'.format(anio)))

            # obtener todos los nacidos y agruparlos por semana
            nacidos = Nacido.objects.filter(fecha_registro__year=anio)
            
            resultados = []
            csv_list = []
            csv_list.append(['padres', 'dias de demora en inscribir'])
            
            for nacido in nacidos:
                # no vamos a revelar días pero si la semana del año del nacimiento
                fecha_nacimiento = nacido.fecha_nacimiento
                fecha_registro = nacido.fecha_registro
                
                if fecha_nacimiento is None or fecha_registro is None:
                    # omitir
                    continue
                
                # ver cuantos días se tardo en registrarlo
                diff_nac_registro = (fecha_registro - fecha_nacimiento).days

                # ver la cantidad y el genero de los padres
                cod_padres = nacido.codigo_conformacion_de_progenitores()
            
                csv_list.append([cod_padres, diff_nac_registro])
            
            dest_csv = os.path.join(settings.MEDIA_ROOT, 'demora-en-registro.por-tipo-de-padres-{}.csv'.format(anio))
            dest_xls = os.path.join(settings.MEDIA_ROOT, 'demora-en-registro.por-tipo-de-padres-{}.xls'.format(anio))

            excel.pe.save_as(array=csv_list, dest_file_name=dest_csv)
            excel.pe.save_as(array=csv_list, dest_file_name=dest_xls)

            self.stdout.write(self.style.SUCCESS('Finalizado'))
