#!/usr/bin/python
'''
Grabar lista de niños registrados en el registro civil por semana
'''
from django.core.management.base import BaseCommand
from django.conf import settings
import django_excel as excel
from django.db import transaction
import sys
import os
from cordobaplantavida.models import Progenitor, Nacido


class Command(BaseCommand):
    help = """Generar un CSV/XLS con todos los niñes Registrados en el registro Civil de la Ciudad"""
    # generar-ninies-registrados-por-semana

    def handle(self, *args, **options):
        for anio in range(2016, 2020):
            self.stdout.write(self.style.SUCCESS('Niños registrados en {}'.format(anio)))

            # obtener todos los nacidos y agruparlos por semana
            nacidos = Nacido.objects.filter(fecha_registro__year=anio)
            
            semanas = {}  # valores agrupados por semana para anonimizar
            for nacido in nacidos:
                # no vamos a revelar días pero si la semana del año del nacimiento
                fecha_nacimiento = nacido.fecha_nacimiento
                fecha_registro = nacido.fecha_registro
                
                if fecha_nacimiento is None or fecha_registro is None:
                    # omitir, algo esta mal
                    continue
                
                anio, semana, dia = fecha_registro.isocalendar()

                # ver cuantos días se tardo en registrarlo
                # diff_nac_registro = (fecha_registro - fecha_nacimiento).days

                key = '{} {}'.format(anio, semana)
                if key not in semanas.keys():
                    semanas[key] = {'registrados': 0,
                                    'dias_de_atraso_acumulados': 0,
                                    # total por grupos de padres
                                    'FM': 0,  # Femenino + Masculino
                                    'F': 0,   # registra solo una mujer
                                    'M': 0,   # registra solo un hombre
                                    'MM': 0,  # registran dos hombres
                                    'FF': 0,  # registran dos mujeres
                                    'Sin datos de padres': 0     # Sin datos de registro
                                    }
                
                semanas[key]['registrados'] += 1
                # semanas[key]['dias_de_atraso_acumulados'] += diff_nac_registro

                # ver la cantidad y el genero de los padres
                p1 = nacido.progenitor1
                p2 = nacido.progenitor2

                # codificar la conformacion del grupo de padres
                padres = []
                if p1 is not None: padres.append(p1.sexo.upper().strip())
                if p2 is not None: padres.append(p2.sexo.upper().strip())
                
                # ordeno la lista final para que no existan grupos distintos MF y FM (masculino y femenino)
                cod_padres = ''.join(sorted(padres))
                if cod_padres == '':
                    cod_padres = 'Sin datos de padres'
                semanas[key][cod_padres] += 1

            csv_list = []
            csv_list.append(['año registro', 'semana registro', 'registrados', 'FM', 'F', 'M', 'MM', 'FF', 'Sin datos de padres'])
            for key in semanas.keys():
                anio, semana = key.split()
                csv_list.append([anio, semana,
                                semanas[key]['registrados'],
                                semanas[key]['FM'],
                                semanas[key]['F'],
                                semanas[key]['M'],
                                semanas[key]['MM'],
                                semanas[key]['FF'],
                                semanas[key]['Sin datos de padres']
                                ])
            
            dest_csv = os.path.join(settings.MEDIA_ROOT, 'niñes-registrados-en-{}.csv'.format(anio))
            dest_xls = os.path.join(settings.MEDIA_ROOT, 'niñes-registrados-en-{}.xls'.format(anio))

            excel.pe.save_as(array=csv_list, dest_file_name=dest_csv)
            excel.pe.save_as(array=csv_list, dest_file_name=dest_xls)

            self.stdout.write(self.style.SUCCESS('Finalizado'))
