#!/usr/bin/python
'''
Grabar lista de niños y el uso de apeelido paterno o materno
TERMINAR
'''
from django.core.management.base import BaseCommand
from django.conf import settings
import django_excel as excel
from django.db import transaction
import sys
import os
from cordobaplantavida.models import Progenitor, Nacido


class Command(BaseCommand):
    help = """Generar un CSV/XLS con todos los niñes Registrados en el registro Civil de la Ciudad"""
    # generar-uso-de-apellidos.py

    def handle(self, *args, **options):
        for anio in range(2016, 2020):
            self.stdout.write(self.style.SUCCESS('Niños registrados en {}'.format(anio)))

            # obtener todos los nacidos y agruparlos por semana
            nacidos = Nacido.objects.filter(fecha_registro__year=anio)
            
            semanas = {}  # valores agrupados por semana para anonimizar
            for nacido in nacidos:
                # no vamos a revelar días pero si la semana del año del nacimiento
                fecha_nacimiento = nacido.fecha_nacimiento
                fecha_registro = nacido.fecha_registro
                
                anio, semana, dia = fecha_registro.isocalendar()

                codigo_apellido = nacido.usa_primero_apellido()
                
                key = '{} {}'.format(anio, semana)
                if key not in semanas.keys():
                    semanas[key] = {'registrados': 0,
                                    'apellido-papa': 0,
                                    'apellido-mama': 0,
                                    }
                
                semanas[key]['registrados'] += 1
                if codigo_apellido == 'PAPA':
                    semanas[key]['apellido-papa'] += 1
                elif codigo_apellido == 'MAMA':
                    semanas[key]['apellido-mama'] += 1

            csv_list = []
            csv_list.append(['año registro', 'semana registro', 'registrados', 'PAPA', 'MAMA'])
            for key in semanas.keys():
                anio, semana = key.split()
                csv_list.append([anio, semana,
                                semanas[key]['registrados'],
                                semanas[key]['apellido-papa'],
                                semanas[key]['apellido-mama']
                                ])
            
            dest_csv = os.path.join(settings.MEDIA_ROOT, 'niñes-apellidos-registrados-{}.csv'.format(anio))
            dest_xls = os.path.join(settings.MEDIA_ROOT, 'niñes-apellidos-registrados-{}.xls'.format(anio))

            excel.pe.save_as(array=csv_list, dest_file_name=dest_csv)
            excel.pe.save_as(array=csv_list, dest_file_name=dest_xls)

            self.stdout.write(self.style.SUCCESS('Finalizado'))

        