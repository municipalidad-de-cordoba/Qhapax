from django.conf.urls import url, include
from rest_framework import routers
from .views import nacimientos_por_semana

urlpatterns = [
    url(r'^nacimientos-por-semana-(?P<anio>[0-9]+).(?P<filetype>csv|xls)$', nacimientos_por_semana, name='nacimientos-por-semana'),
    
    ]