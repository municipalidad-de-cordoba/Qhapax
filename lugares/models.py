'''
    En qhapax se importan datos desde muy diferentes orígenes y
        en general no hay geolocalización formal. Este modulo los
        contiene mientras los transformamos a datos más precisos
    Creado originalmente para centralizar otros modelos que tenían
        la misma funcionalidad
'''
from django.db import models
from catastro.models import ParcelaCatastral
from versatileimagefield.fields import VersatileImageField
from barrios.models import Barrio
from catastro.models import CalleOficial
from ckeditor.fields import RichTextField


class UbicacionGeneral(models.Model):
    '''
    Campos variados para todos los niveles de calidad en los que están los datos
    '''

    # cuando recibo datos a nivel basico de texto
    nivel01_direccion_calle = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        help_text='Nombre de la calle escrita a mano')
    nivel01_direccion_numero = models.CharField(
        max_length=50, null=True, blank=True, help_text='Numero de la casa')
    nivel01_barrio = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        help_text='Barrio a modo de texto simple')

    # Clase para cuando recibimos latitud y longitud pero no la parcela
    nivel02_latlong = models.PointField(null=True, blank=True)

    # Cuando la dirección es en territorio de nuestro catastro y lo tenemos
    # definido
    nivel03_parcela = models.ForeignKey(
        ParcelaCatastral,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        help_text='Use el formato Distrito-Zona-Manzana-Parcela. '
        'Puede buscarlo en https://gobiernoabierto.cordoba.gob.ar/mapas/emap/')

    # si el parcelario entrega calle y nro entonces se cargan
    nivel03_calle_oficial = models.ForeignKey(
        CalleOficial, null=True, blank=True)
    nivel03_numero_oficial = models.CharField(
        max_length=50, null=True, blank=True)

    # barrios oficial desde nuestra base de datos (vía parcela o de lista)
    nivel03_barrio = models.ForeignKey(Barrio, null=True, blank=True)

    # independiente del nivel de geolocalización
    piso = models.CharField(max_length=50, null=True, blank=True, help_text='')
    departamento = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        help_text='Numero o letra del departamento si aplica')


class Lugar(models.Model):
    '''
    lugar con algún interes
    '''
    nombre = models.CharField(max_length=250)
    descripcion_publica = RichTextField(
        verbose_name='Descripcion general y pública del lugar',
        null=True,
        blank=True)

    # cuando los datos se conocen o importan de otras bases vienen de múltiples
    # formas los siguientes campos son opcionales por eso

    ubicacion = models.ForeignKey(UbicacionCalle, null=True, blank=True)

    def __str__(self):
        return self.nombre

    @property
    def fotos(self):
        return self.fotolugar_set.all()

    class Meta:
        verbose_name_plural = "Lugares"
        ordering = ['nombre']


class FotoLugar(models.Model):
    """ fotos del lugar """
    lugar = models.ForeignKey(Lugar, on_delete=models.CASCADE)
    orden = models.PositiveIntegerField(default=100)
    foto = VersatileImageField(upload_to='imagenes/lugares')

    class Meta:
        verbose_name = "Fotos de lugar"
        verbose_name_plural = "Fotos de lugares"
        ordering = ['orden']
