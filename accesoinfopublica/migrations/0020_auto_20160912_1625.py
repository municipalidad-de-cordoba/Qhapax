# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-12 19:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accesoinfopublica', '0019_auto_20160908_1433'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solicitudaccesoinfo',
            name='descripcion',
            field=models.TextField(help_text="Detalle del dato solicitado. <b style='color:red'>Debe solicitarse un dato específico por formulario</b>. No use listas de datos a solicitar, <b style='color:red'>no se aceptarán pedidos de ese tipo.</b>", verbose_name='Descripción del pedido'),
        ),
    ]
