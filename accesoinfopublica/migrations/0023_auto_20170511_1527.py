# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-05-11 18:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accesoinfopublica', '0022_auto_20170511_1147'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalsolicitudaccesoinfo',
            name='fecha_derivacion',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='historicalsolicitudaccesoinfo',
            name='fecha_respuesta',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='solicitudaccesoinfo',
            name='fecha_derivacion',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='solicitudaccesoinfo',
            name='fecha_respuesta',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
