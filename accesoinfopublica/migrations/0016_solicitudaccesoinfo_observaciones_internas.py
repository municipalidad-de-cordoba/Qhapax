# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-08-25 18:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accesoinfopublica', '0015_auto_20160819_0929'),
    ]

    operations = [
        migrations.AddField(
            model_name='solicitudaccesoinfo',
            name='observaciones_internas',
            field=models.TextField(blank=True, null=True),
        ),
    ]
