from django.contrib import admin
from .models import SolicitudAccesoInfo, MotivoSolicitud
from django.core.urlresolvers import reverse
from simple_history.admin import SimpleHistoryAdmin
from import_export.admin import ImportExportModelAdmin
from import_export import resources, fields


class SolicitudAccesoInfoResource(resources.ModelResource):
    ''' recurso a exportarse cuando se descarga  '''
    dni = fields.Field()
    tipo_organizacion = fields.Field()

    def dehydrate_dni(self, obj):
        tipoid = '' if obj.tipo_id is None else obj.tipo_id.nombre
        return '{} {}'.format(tipoid, obj.unique_id)

    def dehydrate_tipo_organizacion(self, obj):
        return obj.get_tipo_org_general_display()

    def dehydrate_estado(self, obj):
        return obj.get_estado_display()

    def dehydrate_oficina_derivada(self, obj):
        if obj.oficina_derivada is None:
            return 'NO DERIVADA'
        else:
            return obj.oficina_derivada.oficina

    class Meta:
        model = SolicitudAccesoInfo
        fields = (
            'id',
            'nombre_persona',
            'tipo_organizacion',
            'nombre_org',
            'dni',
            'email',
            'telefono',
            'domicilio',
            'titulo',
            'descripcion',
            'estado',
            'oficina_derivada',
            'respuesta',
            'observaciones_internas',
            'creado',
            'ultima_modificacion',
            'nro_expediente',
            'fecha_derivacion',
            'fecha_respuesta')
        export_order = ('ultima_modificacion', )


class SolicitudAccesoInfoAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    resource_class = SolicitudAccesoInfoResource

    def pdf_solicitud(self, obj):
        ficha_url = reverse('backend-acceso-info-pdf', kwargs={'pk': obj.id})
        return '<a target="_blank" href="{}">PDF</a>'.format(ficha_url)
    pdf_solicitud.allow_tags = True

    def ultimo_editor(self, obj):
        return obj.history.all()[0].history_user

    list_display = (
        "titulo",
        'id',
        'nro_expediente',
        'creado',
        'ultima_modificacion',
        'fecha_ultimo_movimiento',
        'fecha_derivacion',
        'fecha_respuesta',
        'oficina_derivada',
        'estado',
        'pdf_solicitud',
        'observaciones_internas',
        'nombre_persona',
        'tipo_org_general',
        'nombre_org',
        'ultimo_editor')
    search_fields = [
        "titulo",
        'nro_expediente',
        'tema_general__nombre',
        'nombre_persona',
        'tipo_org_general',
        'nombre_org',
        'estado',
        'motivo__nombre',
        'observaciones_internas']
    list_filter = [
        'oficina_derivada',
        'tema_general',
        'tipo_org_general',
        'estado',
        'motivo',
        'creado',
        'fecha_respuesta']
    date_hierarchy = 'creado'


class MotivoSolicitudAdmin(admin.ModelAdmin):
    list_display = ("nombre", )


admin.site.register(SolicitudAccesoInfo, SolicitudAccesoInfoAdmin)
admin.site.register(MotivoSolicitud, MotivoSolicitudAdmin)
