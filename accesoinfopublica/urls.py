from django.conf.urls import url
from . import views


urlpatterns = [
    # url(r'^$', views.index, name='index'),
    # formulacio de acceso a la info pública para llenar
    url(r'^solicitud/$', views.solicitud, name='accesoinfopublica.solicitud',),
]
