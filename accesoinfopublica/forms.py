from django.forms import ModelForm
from .models import SolicitudAccesoInfo
from captcha.fields import ReCaptchaField


class SolicitudAccesoInfoForm(ModelForm):
    captcha = ReCaptchaField(attrs={'lang': 'es'})

    class Meta:
        model = SolicitudAccesoInfo
        exclude = [
            'canal_de_envio',
            'slug',
            'estado',
            'respuesta',
            'creado',
            'ultima_modificacion',
            'tema',
            'oficina_derivada',
            'fecha_respuesta_fisica_del_memo',
            'tipo_org',
            'fecha_derivacion',
            'fecha_respuesta',
            'tema_general',
            'domicilio',
            'motivo',
            'motivo_txt',
            'fecha_ultimo_movimiento']
