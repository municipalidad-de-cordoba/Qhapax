import logging

from django.shortcuts import render
from django.conf import settings
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_protect
from django.http import JsonResponse
from accesoinfopublica import forms
from core.models import TipoOrganizacion

logger = logging.getLogger(__name__)


@cache_page(60 * 60)  # 1 h
@csrf_protect
def solicitud(request):
    """
    mostrar el formulacio de acceso a la info pública para completar
    """
    form = forms.SolicitudAccesoInfoForm

    context = {
        'titulo': 'Formulario de solicitud de acceso a la información pública',
        'form': form(
            initial={
                'tipo_id': settings.TIPOID_PK_PRINCIPAL,
                'tipo_org_general': TipoOrganizacion.PERSONA_FISICA})}

    if request.method == "POST":
        context['form'] = form(request.POST)
        if context['form'].is_valid():
            context['form'].save()
            return JsonResponse({'ok': True})
        else:
            return JsonResponse(
                {'ok': False, 'errors': context['form'].errors.as_json()})

    url = "website/{}/solicitud-acceso-informacion.html".format(
        request.website.template)
    return render(request, url, context)
