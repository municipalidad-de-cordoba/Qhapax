"""
Los ciudadanos pueden enviar solicitudes de acceso a la información.
La oficina encargada derivará y comunicará respuestas
"""
from django.db import models
from django.utils.text import slugify
from core.models import TipoId, TipoOrganizacion
from gobiernos.models import TemaGeneral
from funcionarios.models import Cargo
from simple_history.models import HistoricalRecords


class MotivoSolicitud(models.Model):
    """
    Motivo por el cual se solicita la información
    """
    nombre = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, null=True, blank=True)

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        super(MotivoSolicitud, self).save(*args, **kwargs)

    class Meta:
        unique_together = (("nombre", "slug"),)
        ordering = ['nombre']


class SolicitudAccesoInfo(models.Model):
    """
    Cada una de las solicitudes de acceso a la información de los ciudadanos y
    su estado actual
    """
    tema_general = models.ForeignKey(
        TemaGeneral,
        on_delete=models.SET_NULL,
        blank=True,
        null=True)  # vincula además la solicitud con el gobierno
    nombre_persona = models.CharField(
        max_length=180,
        verbose_name='Nombre completo del solicitante')
    tipo_org_general = models.PositiveIntegerField(
        choices=TipoOrganizacion.tipos,
        null=True,
        verbose_name='Tipo de solicitante',
        blank=True,
        help_text="Persona física es predeterminado")
    nombre_org = models.CharField(  # si pertenece a ONG, empresa, etc
        max_length=180,
        null=True,
        blank=True,
        verbose_name='Nombre de la organización a la que pertenece',
        help_text="Deje en blanco si no corresponde")
    # tipo y nro de identificacion
    tipo_id = models.ForeignKey(TipoId, on_delete=models.SET_NULL, null=True,
                                verbose_name='Tipo de documento')
    unique_id = models.CharField(
        max_length=90,
        verbose_name='Nro de documento')

    # obligatorio, para validar que es real
    email = models.EmailField(
        help_text=("Usaremos esta vía para cualquier comunicación,"
                   " debe ser real"))
    telefono = models.CharField(max_length=90)
    domicilio = models.CharField(max_length=190)

    VIA_WEB = 10
    VIA_MESA_DE_ENTRADAS = 20
    canales_de_envio = ((VIA_WEB, 'Vía Web'),
                        (VIA_MESA_DE_ENTRADAS, 'Vía mesa de entradas'))
    canal_de_envio = models.PositiveIntegerField(
        choices=canales_de_envio, blank=True, null=True)

    titulo = models.CharField(max_length=130,
                              verbose_name='Titulo del pedido',
                              help_text="Breve resumen del pedido")
    slug = models.SlugField(max_length=140, null=True, blank=True)
    descripcion = models.TextField(
        verbose_name='Descripción del pedido',
        help_text=("Detalle del dato solicitado. "
                   "<b style='color:red'>Debe solicitarse un dato específico"
                   " por formulario</b>. No use listas de datos a solicitar, "
                   "<b style='color:red'>no se aceptarán pedidos de ese tipo."
                   "</b>"))

    NUEVO = 10
    VALIDO = 20  # vio el email y le dio al link de control
    EN_ESTUDIO = 30  # puede ser válido
    DESCARTADO = 40  # se descarto la idea (enviar motivos al ciudadano)
    ACEPTADO = 50  # Se va a realizar
    RESPONDIDO = 60  # Se llevo a cabo

    estados = ((NUEVO, 'Nuevo'), (VALIDO, 'Valido'),
               (EN_ESTUDIO, 'En estudio'), (DESCARTADO, 'Descartado'),
               (ACEPTADO, 'Aceptado'), (RESPONDIDO, 'Hecho'))

    estado = models.PositiveIntegerField(choices=estados, default=NUEVO)
    # por ahora la respuesta se garda como texto, luego podría estar vinculada
    # a algún modelo de la clase datos
    oficina_derivada = models.ForeignKey(
        Cargo, on_delete=models.SET_NULL, blank=True, null=True)
    respuesta = models.TextField(null=True, blank=True)
    observaciones_internas = models.TextField(null=True, blank=True)

    motivo = models.ForeignKey(
        MotivoSolicitud,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name='Motivo de la consulta (no obligatorio)',
        help_text=("El motivo de la información no es obligatorio de aclarar "
                   "para emitir una solicitud de pedido de información "
                   "pública. No obstante si su motivo es para investigación "
                   "académica, publicación y periodismo de datos por favor "
                   "aclare su motivo aquí"))
    motivo_txt = models.TextField(null=True, blank=True,
                                  verbose_name="Motivo ampliado",
                                  help_text="Sólo a fines estadísticos")
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    # si nos conectamos a otro software aquí guardamos el ID
    nro_expediente = models.CharField(max_length=90, null=True, blank=True)
    fecha_derivacion = models.DateTimeField(null=True, blank=True)
    fecha_respuesta = models.DateTimeField(null=True, blank=True)
    fecha_respuesta_fisica_del_memo = models.DateTimeField(
        null=True, blank=True)
    fecha_ultimo_movimiento = models.DateTimeField(null=True, blank=True)
    history = HistoricalRecords()

    def save(self, *args, **kwargs):
        # si es un nuevo objeto DEBO mandarle un email de notificación (DESPUES
        # de grabarlo)
        nuevo = self.pk is None
        self.slug = slugify(self.titulo)
        super(SolicitudAccesoInfo, self).save(*args, **kwargs)

        if nuevo:
            # FIXME esto no deberíá estar acá de esta forma.
            # Tiene que haber opciones de envío de email (o especificar el no
            # envío)
            from django.template.loader import render_to_string
            html_mensaje = render_to_string(
                'core/mails/acceso_info_requerida.html',
                {
                    'usuario': self.nombre_persona,
                    'titulo': self.titulo,
                    'pk': self.pk,
                    'descripcion': self.descripcion})
            from core.lib.comm import QhapaxMail
            qm = QhapaxMail()
            subject = ("Acceso a la Información Pública Municipalidad "
                       "de Córdoba [AI-{}]").format(self.pk)
            qm.send_mail(subject, html_mensaje, to=self.email)

    def __str__(self):
        return self.titulo

    class Meta:
        ordering = ("-creado", "estado")
