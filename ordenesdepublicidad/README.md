# Migración de sistema antiguo de Ordenes de Publicidad

Existe un sistema muy viejo en donde se cargan ordenes de publicidad.
Lamentablemente el sistema no está bien hecho y no se ha usado para cargar el 100% de las contrataciones a medios.

Esta en proceso de reconstrucción de la información sobre como fue usado desde el año 2000 (fecha en la que comienzan los datos).

## Importar datos

En este orden:
```
python manage.py importar_empresas_de_publicidad --path ordenesdepublicidad/resources/EMPRE.DBF
python manage.py importar_ordenes_de_publicidad --path ordenesdepublicidad/resources/ORDEN.DBF
python manage.py importar_items_de_publicidad --path ordenesdepublicidad/resources/ITEM.DBF
```