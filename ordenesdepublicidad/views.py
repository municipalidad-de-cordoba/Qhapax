from django.views.decorators.cache import cache_page
from .models import OrdenDePublicidad, MedioPublicidad, EmpresaPublicidad
import django_excel as excel
from django.db.models import Sum, Count
from django.db.models.functions import TruncYear


@cache_page(60 * 60 * 24 * 7)
def resumen_ordenes_publicidad_por_medio(request, filetype):
    '''
    lista sumarizada de ordenes de publicidad por medio de comunicación
    '''
    resumen = OrdenDePublicidad.objects.values('medio').annotate(
        total=Sum('importe'), anio=TruncYear('fecha'), ordenes=Count('id'))

    csv_list = []
    headers = [
        'CUIT',
        'Empresa',
        'Medio',
        'Medio id',
        'Año',
        'Total dinero',
        'Total ordenes']

    csv_list.append(headers)
    for reg in resumen:
        medio = MedioPublicidad.objects.get(pk=reg['medio'])
        anio = reg['anio'].year
        csv_list.append([medio.empresa.codigo,
                         medio.empresa.nombre,
                         medio.nombre,
                         medio.id,
                         anio,
                         reg['total'],
                         reg['ordenes']])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 24 * 7)
def resumen_ordenes_publicidad_por_empresa(request, filetype):
    '''
    lista sumarizada de ordenes de publicidad por empresa
    '''
    resumen = OrdenDePublicidad.objects.values('medio__empresa').annotate(
        total=Sum('importe'), anio=TruncYear('fecha'), ordenes=Count('id'))

    csv_list = []
    headers = [
        'CUIT',
        'Empresa',
        'Empresa ID',
        'Año',
        'Total dinero',
        'Total ordenes']

    csv_list.append(headers)
    for reg in resumen:
        empresa = EmpresaPublicidad.objects.get(pk=reg['medio__empresa'])
        anio = reg['anio'].year
        csv_list.append([empresa.codigo, empresa.nombre, empresa.id, anio,
                         reg['total'], reg['ordenes']])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 24 * 7)
def ordenes_de_publicidad(request, anio, filetype):
    '''
    lista de ordenes de publicidad
    '''
    resumen = OrdenDePublicidad.objects.filter(fecha__year=anio)

    csv_list = []
    headers = [
        'titulo',
        'CUIT',
        'Empresa',
        'Medio',
        'Medio id',
        'Fecha',
        'Importe',
        'Descuento',
        'IVA']

    csv_list.append(headers)
    for reg in resumen:

        csv_list.append([reg.titulo, reg.medio.empresa.codigo,
                         reg.medio.empresa.nombre, reg.medio.nombre,
                         reg.medio.id, reg.fecha, reg.importe,
                         reg.porcentaje_descuento, reg.iva])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
