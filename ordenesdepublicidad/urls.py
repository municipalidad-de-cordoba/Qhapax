from django.conf.urls import url

from . import views

urlpatterns = [url(r'^resumen-por-medio.(?P<filetype>csv|xls)$',
                   views.resumen_ordenes_publicidad_por_medio,
                   name='ordenes_de_publicidad.resumen-por-medio'),
               url(r'^resumen-por-empresa.(?P<filetype>csv|xls)$',
                   views.resumen_ordenes_publicidad_por_empresa,
                   name='ordenes_de_publicidad.resumen-por-empresa'),
               url(r'^ordenes-de-publicidad-(?P<anio>[0-9]+).(?P<filetype>csv|xls)$',
                   views.ordenes_de_publicidad,
                   name='ordenes_de_publicidad.lista'),
               ]
