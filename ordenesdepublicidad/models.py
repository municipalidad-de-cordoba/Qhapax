from django.db import models


class EmpresaPublicidad(models.Model):
    """
    Empresas dueñas de medios a las cuales se les paga(pagó) por pauta publicitiaria

    ('CODIGO', '20166551642'), ('SC', 0), ('NOMBRE', 'Gonzalez Guillermo'),
        ('TIPO', 1), ('TMED', None), ('EPOBS', None), ('TIVA', '')])
    ('CODIGO', '20166551642'), ('SC', 1), ('NOMBRE', 'guillermogonzalez.com.ar'),
        ('TIPO', 2), ('TMED', 2), ('EPOBS', None), ('TIVA', '')])
    ('CODIGO', '20166551642'), ('SC', 1), ('NOMBRE', 'guillermogonzalez.com.ar'),
        ('TIPO', 3), ('TMED', None), ('EPOBS', None), ('TIVA', '')])
    ('CODIGO', '20166551642'), ('SC', 1), ('NOMBRE', 'Banner'),
        ('TIPO', 3), ('TMED', None), ('EPOBS', None), ('TIVA', '')])
    ('CODIGO', '30636529164'), ('SC', 1), ('NOMBRE', 'Golpe al Corazón'),
        ('TIPO', 3), ('TMED', None), ('EPOBS', None), ('TIVA', '')])
    """
    # el código debería ser único pero parece que no lo es
    codigo = models.CharField(
        max_length=30,
        help_text="suponemos que es el cuit de la empresa",
        unique=True)
    sc = models.PositiveIntegerField(
        default=0,
        null=True,
        blank=True,
        help_text='No sabemos que es todavía')
    nombre = nombre = models.CharField(max_length=190, null=True, blank=True)
    tipo = models.PositiveIntegerField(
        default=0,
        null=True,
        blank=True,
        help_text='No sabemos que es todavía')
    tmed = models.PositiveIntegerField(
        default=0,
        null=True,
        blank=True,
        help_text='No sabemos que es todavía')
    epobs = models.PositiveIntegerField(
        default=0,
        null=True,
        blank=True,
        help_text='No sabemos que es todavía')
    tiva = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text='No sabemos que es todavía')

    def __str__(self):
        result = "{} - {}".format(self.nombre, self.codigo)
        return result

    class Meta:
        ordering = ['nombre']


class MedioPublicidad(models.Model):
    """ un medio de comunicación y una empresa """
    empresa = models.ForeignKey(
        EmpresaPublicidad,
        on_delete=models.CASCADE,
        related_name='medios')
    nombre = models.CharField(max_length=190)

    def total_publicidad(self, anio):
        totales = self.ordenes.filter(
            fecha__year=anio).aggregate(
            models.Sum('importe'))

        return totales['importe__sum']

    class Meta:
        ordering = ['nombre']

    def __str__(self):
        return '{} vía {}'.format(self.nombre, self.empresa.nombre)


class OrdenDePublicidad(models.Model):
    """
    ('ORDEN', '201724670'), ('FECHA', datetime.date(2017, 12, 30)),
        ('EMPR', 'Frecuencia Producciones S.A'),
        ('MEDIO', 'Radio MIA'),
        ('IMPORTE', 172656.0),
        ('DCTO', 30),
        ('TITULO', 'Sentir Córdoba'),
        ('TMED', 1),
        ('CODEMP', '30656058400'),
        ('SC', 1),
        ('OBS', None),
        ('CODCTA', 1),
        ('DECRETO', ''),
        ('PROGRAMA', ''),
        ('IVA', 21.0)])
    ('ORDEN', '201724680'), ('FECHA', datetime.date(2017, 12, 30)),
        ('EMPR', 'Radio Mitre S.A.'),
        ('MEDIO', 'Radio Mitre'),
        ('IMPORTE', 30000.0),
        ('DCTO', 0),
        ('TITULO', 'Municipalidad de Córdoba'),
        ('TMED', 1),
        ('CODEMP', '30598036299'),
        ('SC', 1),
        ('OBS', None),
        ('CODCTA', 1), ('DECRETO', ''), ('PROGRAMA', ''), ('IVA', 21.0)])
    ('ORDEN', '201724690'), ('FECHA', datetime.date(2017, 12, 30)), ('EMPR', 'Radio Mitre S.A.'), ('MEDIO', 'Radio Mitre'), ('IMPORTE', 50000.0), ('DCTO', 0), ('TITULO', 'Municipalidad de Córdoba.-'), ('TMED', 1), ('CODEMP', '30598036299'), ('SC', 1), ('OBS', None), ('CODCTA', 1), ('DECRETO', ''), ('PROGRAMA', ''), ('IVA', 21.0)])
    ('ORDEN', '201724700'), ('FECHA', datetime.date(2017, 12, 30)), ('EMPR', 'Editorial Max-Trade S.R.L.'), ('MEDIO', 'Diario "Hoy Día Córdoba"'), ('IMPORTE', 47000.0), ('DCTO', 25), ('TITULO', 'Lic. Pub. 99/17.-'), ('TMED', 2), ('CODEMP', '30689018536'), ('SC', 1), ('OBS', None), ('CODCTA', 400), ('DECRETO', ''), ('PROGRAMA', ''), ('IVA', 2.5)])
    ('ORDEN', '201724710'), ('FECHA', datetime.date(2017, 12, 30)), ('EMPR', 'Graf Cor S.A'), ('MEDIO', 'Diario El Alfil'), ('IMPORTE', 70000.0), ('DCTO', 0), ('TITULO', 'Municipalidad de Córdoba .-'), ('TMED', 2), ('CODEMP', '30711931186'), ('SC', 1), ('OBS', None), ('CODCTA', 1), ('DECRETO', ''), ('PROGRAMA', ''), ('IVA', 2.5)])
    """
    orden_numero = models.CharField(max_length=90)
    fecha = models.DateField(null=True, blank=True)
    # guardar datos por las dudas
    medio = models.ForeignKey(
        MedioPublicidad,
        on_delete=models.CASCADE,
        related_name='ordenes',
        null=True,
        blank=True)

    importe = models.DecimalField(max_digits=14, decimal_places=2, default=0.0)
    porcentaje_descuento = models.DecimalField(
        max_digits=14, decimal_places=2, default=0.0)
    iva = models.DecimalField(max_digits=14, decimal_places=2, default=0.0)
    titulo = models.TextField(null=True, blank=True)
    tmed = models.PositiveIntegerField(
        default=0, help_text='No sabemos que es todavía')
    sc = models.PositiveIntegerField(
        default=0, help_text='No sabemos que es todavía')
    obs = models.TextField(null=True, blank=True)
    codcta = models.PositiveIntegerField(
        default=0, help_text='No sabemos que es todavía')

    decreto = models.CharField(max_length=130, null=True, blank=True)
    programa = models.CharField(max_length=130, null=True, blank=True)

    def __str__(self):
        return self.orden_numero


class ItemPublicidad(models.Model):
    """ items de la publicidad """
    # ('ORDEN', '201724670'), ('ITEM', 8), ('MES', 12), ('MEDIDA', 48.0),
    # ('MEDI2', 0.0), ('UBICACION', 'La Tarde es Mia'), ('CANTIDAD', 3), ('TARIFA', 42.0),
    # ('TOTAL', 6048.0), ('DIAS', '               X'), ('MARC', '')])
    orden = models.ForeignKey(
        OrdenDePublicidad,
        on_delete=models.CASCADE,
        related_name='items')
    item = models.PositiveIntegerField(
        default=0, help_text='No sabemos que es todavía')
    mes = models.PositiveIntegerField(default=0)
    medida1 = models.DecimalField(max_digits=14, decimal_places=2, default=0.0)
    medida2 = models.DecimalField(max_digits=14, decimal_places=2, default=0.0)
    ubicacion = models.CharField(max_length=90, null=True, blank=True)
    cantidad = models.PositiveIntegerField(default=0)
    tarifa = models.DecimalField(max_digits=14, decimal_places=2, default=0.0)
    total = models.DecimalField(max_digits=14, decimal_places=2, default=0.0)
    dias = models.CharField(
        max_length=90,
        null=True,
        blank=True,
        help_text='No sabemos como interpretarlo')
    marc = models.CharField(
        max_length=90,
        null=True,
        blank=True,
        help_text='No sabemos que es todavía')
