from django.contrib import admin
from .models import (OrdenDePublicidad, ItemPublicidad,
                     EmpresaPublicidad, MedioPublicidad)


@admin.register(EmpresaPublicidad)
class EmpresaPublicidadAdmin(admin.ModelAdmin):
    list_display = ['codigo', 'sc', 'nombre', 'tipo', 'tmed', 'epobs', 'tiva']
    search_fields = ['codigo', 'nombre']


@admin.register(MedioPublicidad)
class MedioPublicidadAdmin(admin.ModelAdmin):

    def total_importes_2017(self, obj):
        return obj.total_publicidad(2017)

    def total_importes_2016(self, obj):
        return obj.total_publicidad(2016)

    def total_importes_2015(self, obj):
        return obj.total_publicidad(2015)

    def total_importes_2014(self, obj):
        return obj.total_publicidad(2014)

    def total_importes_2013(self, obj):
        return obj.total_publicidad(2013)

    def total_importes_2012(self, obj):
        return obj.total_publicidad(2012)

    def total_importes_2011(self, obj):
        return obj.total_publicidad(2011)

    list_display = [
        'nombre',
        'empresa',
        'total_importes_2017',
        'total_importes_2016',
        'total_importes_2015',
        'total_importes_2014',
        'total_importes_2013',
        'total_importes_2012',
        'total_importes_2011']
    search_fields = ['nombre']
    list_filter = ['empresa']


@admin.register(OrdenDePublicidad)
class OrdenDePublicidadAdmin(admin.ModelAdmin):
    list_display = [
        'orden_numero',
        'fecha',
        'medio',
        'importe',
        'porcentaje_descuento',
        'iva',
        'titulo',
        'tmed',
        'sc',
        'obs',
        'codcta',
        'decreto',
        'programa']
    list_filter = ['medio', 'medio__empresa']


@admin.register(ItemPublicidad)
class ItemPublicidadAdmin(admin.ModelAdmin):
    list_display = ['orden', 'item', 'mes', 'medida1', 'medida2', 'ubicacion',
                    'cantidad', 'tarifa', 'total', 'dias', 'marc']
