#!/usr/bin/python
"""
Importar empresas de publicidad
"""
from django.core.management.base import BaseCommand
import sys
from dbfread import DBF, FieldParser
from ordenesdepublicidad.models import ItemPublicidad, OrdenDePublicidad


# Esta clase limpia los datos corruptos que vienen en el archivo DBF
class MyFieldParser(FieldParser):
    def parseN(self, field, data):
        # Had to strip out the other characters first before \x00, as per super
        # function specs.
        data = data.strip().strip(b'*\x00')
        return super(MyFieldParser, self).parseN(field, data)

    def parseD(self, field, data):
        data = data.strip(b'\x00')
        return super(MyFieldParser, self).parseD(field, data)


class Command(BaseCommand):
    help = """Comando para Itemes de ordenes de publicidad """

    """
    manage.py importar_items_de_publicidad --path ITEMS.DBF

    ('ORDEN', '201724670'), ('ITEM', 8), ('MES', 12), ('MEDIDA', 48.0), ('MEDI2', 0.0), ('UBICACION', 'La Tarde es Mia'), ('CANTIDAD', 3), ('TARIFA', 42.0), ('TOTAL', 6048.0), ('DIAS', '               X'), ('MARC', '')])
    ('ORDEN', '201724680'), ('ITEM', 1), ('MES', 12), ('MEDIDA', 1.0), ('MEDI2', 0.0), ('UBICACION', 'La Movida de la Noch'), ('CANTIDAD', 1), ('TARIFA', 30000.0), ('TOTAL', 30000.0), ('DIAS', '      X      X      X      X'), ('MARC', '')])
    ('ORDEN', '201724690'), ('ITEM', 1), ('MES', 12), ('MEDIDA', 1.0), ('MEDI2', 0.0), ('UBICACION', 'La Mañana de Mitre'), ('CANTIDAD', 1), ('TARIFA', 50000.0), ('TOTAL', 50000.0), ('DIAS', '       X      X    XXX   XXX'), ('MARC', '')])
    ('ORDEN', '201724700'), ('ITEM', 1), ('MES', 12), ('MEDIDA', 2.0), ('MEDI2', 10.0), ('UBICACION', 'Licitaciones'), ('CANTIDAD', 1), ('TARIFA', 470.0), ('TOTAL', 47000.0), ('DIAS', '   XXXX   X'), ('MARC', '')])
    ('ORDEN', '201724710'), ('ITEM', 1), ('MES', 12), ('MEDIDA', 1.0), ('MEDI2', 1.0), ('UBICACION', 'Página - color'), ('CANTIDAD', 1), ('TARIFA', 70000.0), ('TOTAL', 70000.0), ('DIAS', '     X      X      X X      X'), ('MARC', '')])

    """

    def add_arguments(self, parser):
        parser.add_argument(
            '--path',
            type=str,
            help='Path del archivo dbf local')

    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación'))

        path = options['path']

        try:
            dfbdata = DBF(
                path,
                ignore_missing_memofile=True,
                parserclass=MyFieldParser)
        except Exception as e:
            self.stdout.write(self.style.ERROR('Dbf Error: {}'.format(e)))
            sys.exit(1)

        errores = []

        items_nuevos = 0
        items_repetidos = 0
        duplicados = []
        for registro in dfbdata:
            # print(registro)
            self.stdout.write(self.style.WARNING('Procesando...'))

            try:
                orden_numero = registro['ORDEN']
                orden = OrdenDePublicidad.objects.get(
                    orden_numero=orden_numero)
                item = int(registro['ITEM'])
                mes = int(registro['MES'])
                dias = registro['DIAS']
                item, created = ItemPublicidad.objects.get_or_create(
                    orden=orden, item=item, mes=mes, dias=dias)

                if created:

                    m1 = float(registro['MEDIDA'])
                    m2 = float(registro['MEDI2'])
                    ubicacion = registro['UBICACION']
                    cant = int(registro['CANTIDAD'])
                    tarifa = float(registro['TARIFA'])
                    total = float(registro['TOTAL'])
                    marc = registro['MARC']

                    item.medida1 = m1
                    item.medida2 = m2
                    item.ubicacion = ubicacion
                    item.cantidad = cant
                    item.tarifa = tarifa
                    item.total = total
                    item.marc = marc
                    item.save()

                    items_nuevos += 1

                else:
                    duplicados.append(item)
                    items_repetidos += 1

            except Exception as e:
                self.stdout.write(self.style.ERROR(e))
                self.stdout.write(self.style.ERROR(registro))
                errores.append(registro)

        self.stdout.write(
            self.style.SUCCESS(
                'Procesados. NUEVOS: {}. REPETIDOS: {}'.format(
                    items_nuevos,
                    items_repetidos)))
        self.stdout.write(
            self.style.SUCCESS(
                'Duplicados {}'.format(
                    len(duplicados))))
        cantidad_errores = len(errores)
        if cantidad_errores > 0:
            self.stdout.write(
                self.style.WARNING(
                    'Hay {} errores'.format(cantidad_errores)))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))
