#!/usr/bin/python
"""
Importar empresas de publicidad
"""
from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from dbfread import DBF
from ordenesdepublicidad.models import EmpresaPublicidad


class Command(BaseCommand):
    help = """Comando para Importar empresas de publicidad """

    """
    manage.py importar_empresas_de_publicidad --path EMPRE.DBF

    ('CODIGO', '20166551642'), ('SC', 0), ('NOMBRE', 'Gonzalez Guillermo'), ('TIPO', 1), ('TMED', None), ('EPOBS', None), ('TIVA', '')])
    ('CODIGO', '20166551642'), ('SC', 1), ('NOMBRE', 'guillermogonzalez.com.ar'), ('TIPO', 2), ('TMED', 2), ('EPOBS', None), ('TIVA', '')])
    ('CODIGO', '20166551642'), ('SC', 1), ('NOMBRE', 'guillermogonzalez.com.ar'), ('TIPO', 3), ('TMED', None), ('EPOBS', None), ('TIVA', '')])
    ('CODIGO', '20166551642'), ('SC', 1), ('NOMBRE', 'Banner'), ('TIPO', 3), ('TMED', None), ('EPOBS', None), ('TIVA', '')])
    ('CODIGO', '30636529164'), ('SC', 1), ('NOMBRE', 'Golpe al Corazón'), ('TIPO', 3), ('TMED', None), ('EPOBS', None), ('TIVA', '')])

    """

    def add_arguments(self, parser):
        parser.add_argument(
            '--path',
            type=str,
            help='Path del archivo dbf local')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación'))

        path = options['path']

        try:
            dfbdata = DBF(path, ignore_missing_memofile=True)
        except Exception as e:
            self.stdout.write(self.style.ERROR('Dbf Error: {}'.format(e)))
            sys.exit(1)

        nuevos = 0
        repetidos = 0

        for registro in dfbdata:
            print(registro)
            cuit = registro['CODIGO'].strip()
            empresa, created = EmpresaPublicidad.objects.get_or_create(
                codigo=cuit)

            if created:
                self.stdout.write(self.style.SUCCESS('Empresa nueva'))
                nuevos += 1
                empresa.sc = registro['SC']
                empresa.tipo = registro['TIPO']
                empresa.tmed = registro['TMED']
                empresa.epobs = registro['EPOBS']
                empresa.tiva = registro['TIVA']
                empresa.save()

            else:
                self.stdout.write(self.style.WARNING('Empresa repetida'))
                repetidos += 1

        self.stdout.write(
            self.style.SUCCESS(
                'FIN importacion. NUEVAS: {}. REPETIDAS: {}.'.format(
                    nuevos, repetidos)))
