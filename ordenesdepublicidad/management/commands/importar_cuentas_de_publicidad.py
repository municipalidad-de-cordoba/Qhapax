#!/usr/bin/python
"""
Importar empresas de publicidad
"""
from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from dbfread import DBF


class Command(BaseCommand):
    help = """Comando para Importar Cuentas de publicidad """

    """
    manage.py importar_cuentas_de_publicidad --path CUENTA.DBF
    OrderedDict([('CUENTA', 1), ('DESCR', 'INFOSSEP'), ('DES2', ''), ('PRESUP', None), ('ACUM', None)])
    OrderedDict([('CUENTA', 962), ('DESCR', 'INFOSSEP'), ('DES2', ''), ('PRESUP', None), ('ACUM', 317397.6)])
    OrderedDict([('CUENTA', 919), ('DESCR', 'Secretaría de Servicios Públicos'), ('DES2', ''), ('PRESUP', None), ('ACUM', 7095898.76)])
    OrderedDict([('CUENTA', 963), ('DESCR', 'Secretaria de Modernizacion, Comunicacion y Desarrollo Estra'), ('DES2', 'tegico'), ('PRESUP', None), ('ACUM', 97082.68)])

    """

    def add_arguments(self, parser):
        parser.add_argument(
            '--path',
            type=str,
            help='Path del archivo dbf local')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación'))

        path = options['path']

        try:
            dfbdata = DBF(path, ignore_missing_memofile=True)
        except Exception as e:
            self.stdout.write(self.style.ERROR('Dbf Error: {}'.format(e)))
            sys.exit(1)

        for registro in dfbdata:
            print(registro)

        self.stdout.write(self.style.SUCCESS('FIN'))
