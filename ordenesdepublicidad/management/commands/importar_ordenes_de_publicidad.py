#!/usr/bin/python
"""
Importar empresas de publicidad
"""
from django.core.management.base import BaseCommand
import sys
from dbfread import DBF
from ordenesdepublicidad.models import (OrdenDePublicidad, EmpresaPublicidad,
                                        MedioPublicidad)


class Command(BaseCommand):
    help = """Comando para Importar Ordenes de publicidad """

    """
    manage.py importar_ordenes_de_publicidad --path ORDEN.DBF

    ('ORDEN', '201724670'), ('FECHA', datetime.date(2017, 12, 30)), ('EMPR', 'Frecuencia Producciones S.A'), ('MEDIO', 'Radio MIA'), ('IMPORTE', 172656.0), ('DCTO', 30), ('TITULO', 'Sentir Córdoba'), ('TMED', 1), ('CODEMP', '30656058400'), ('SC', 1), ('OBS', None), ('CODCTA', 1), ('DECRETO', ''), ('PROGRAMA', ''), ('IVA', 21.0)])
    ('ORDEN', '201724680'), ('FECHA', datetime.date(2017, 12, 30)), ('EMPR', 'Radio Mitre S.A.'), ('MEDIO', 'Radio Mitre'), ('IMPORTE', 30000.0), ('DCTO', 0), ('TITULO', 'Municipalidad de Córdoba'), ('TMED', 1), ('CODEMP', '30598036299'), ('SC', 1), ('OBS', None), ('CODCTA', 1), ('DECRETO', ''), ('PROGRAMA', ''), ('IVA', 21.0)])
    ('ORDEN', '201724690'), ('FECHA', datetime.date(2017, 12, 30)), ('EMPR', 'Radio Mitre S.A.'), ('MEDIO', 'Radio Mitre'), ('IMPORTE', 50000.0), ('DCTO', 0), ('TITULO', 'Municipalidad de Córdoba.-'), ('TMED', 1), ('CODEMP', '30598036299'), ('SC', 1), ('OBS', None), ('CODCTA', 1), ('DECRETO', ''), ('PROGRAMA', ''), ('IVA', 21.0)])
    ('ORDEN', '201724700'), ('FECHA', datetime.date(2017, 12, 30)), ('EMPR', 'Editorial Max-Trade S.R.L.'), ('MEDIO', 'Diario "Hoy Día Córdoba"'), ('IMPORTE', 47000.0), ('DCTO', 25), ('TITULO', 'Lic. Pub. 99/17.-'), ('TMED', 2), ('CODEMP', '30689018536'), ('SC', 1), ('OBS', None), ('CODCTA', 400), ('DECRETO', ''), ('PROGRAMA', ''), ('IVA', 2.5)])
    ('ORDEN', '201724710'), ('FECHA', datetime.date(2017, 12, 30)), ('EMPR', 'Graf Cor S.A'), ('MEDIO', 'Diario El Alfil'), ('IMPORTE', 70000.0), ('DCTO', 0), ('TITULO', 'Municipalidad de Córdoba .-'), ('TMED', 2), ('CODEMP', '30711931186'), ('SC', 1), ('OBS', None), ('CODCTA', 1), ('DECRETO', ''), ('PROGRAMA', ''), ('IVA', 2.5)]) """

    def add_arguments(self, parser):
        parser.add_argument(
            '--path',
            type=str,
            help='Path del archivo dbf local')

    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación'))
        path = options['path']
        try:
            dfbdata = DBF(path, ignore_missing_memofile=True)
        except Exception as e:
            self.stdout.write(self.style.ERROR('Dbf Error: {}'.format(e)))
            sys.exit(1)

        errores = []
        ordenes_nuevas = 0
        ordenes_repetidas = 0

        for registro in dfbdata:
            print(registro)

            orden_numero = registro['ORDEN']
            fecha = registro['FECHA']

            importe = float(registro['IMPORTE'])
            porcentaje_descuento = float(registro['DCTO'])
            titulo = registro['TITULO']
            tmed = int(registro['TMED'])
            codigo_empresa = registro['CODEMP']

            empresa = EmpresaPublicidad.objects.get(codigo=codigo_empresa)
            empresa.nombre = registro['EMPR']
            empresa.save()

            medio, created_medio = MedioPublicidad.objects.get_or_create(
                empresa=empresa, nombre=registro['MEDIO'])

            orden, created = OrdenDePublicidad.objects.get_or_create(
                orden_numero=orden_numero)

            if created:
                sc = int(registro['SC'])
                obs = registro['OBS']
                codcta = int(registro['CODCTA'])
                decreto = registro['DECRETO']
                programa = registro['PROGRAMA']
                iva = float(registro['IVA'])

                orden.fecha = fecha
                orden.medio = medio
                orden.importe = importe
                orden.porcentaje_descuento = porcentaje_descuento
                orden.titulo = titulo
                orden.tmed = tmed
                orden.sc = sc
                orden.obs = obs
                orden.codcta = codcta
                orden.decreto = decreto
                orden.programa = programa
                orden.iva = iva
                orden.save()

                ordenes_nuevas += 1
            else:
                ordenes_repetidas += 1

        self.stdout.write(
            self.style.SUCCESS(
                'FIN importacion. NUEVAS: {}. REPETIDAS: {}'.format(
                    ordenes_nuevas,
                    ordenes_repetidas)))
