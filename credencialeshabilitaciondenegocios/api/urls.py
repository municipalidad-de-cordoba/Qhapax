from django.conf.urls import url, include
from rest_framework import routers
from .views import HabilitadorNegocioViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'^habilitadores-de-negocios',
    HabilitadorNegocioViewSet,
    base_name='habilitador-negocios.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
