from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import HabilitadorNegocioSerializer
from api.pagination import DefaultPagination
from credencialeshabilitaciondenegocios.models import AcreditadoParaHabilitarNegocios


class HabilitadorNegocioViewSet(viewsets.ModelViewSet):
    """
    Lista de habiliatadores de negocicios de la ciudad de Córdoba
    """
    serializer_class = HabilitadorNegocioSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # queryset = AcreditadoParaHabilitarNegocios.objects.filter(publicado=True).order_by('apellido', 'nombre')
        queryset = AcreditadoParaHabilitarNegocios.objects.all().order_by('apellido', 'nombre')
        print(queryset.count())
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
