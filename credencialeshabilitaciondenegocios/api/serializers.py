from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from credencialeshabilitaciondenegocios.models import AcreditadoParaHabilitarNegocios


class HabilitadorNegocioSerializer(CachedSerializerMixin):
    registro = serializers.CharField(source='id')

    class Meta:
        model = AcreditadoParaHabilitarNegocios
        fields = ('registro', 'apellido', 'nombre', 'edad',
                  'detalles_acreditacion')


cache_registry.register(HabilitadorNegocioSerializer)
