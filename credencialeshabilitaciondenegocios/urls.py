from django.conf.urls import url

from . import views

urlpatterns = [url(r'^lista-de-habilitadores-de-negocios.(?P<filetype>csv|xls)$',
                   views.planilla_acreditados_habilitacion_de_negocios,
                   name='credencialeshabilitaciondenegocios.lista'),
               url(r'^habilitador-de-negocios-(?P<pk>[0-9]+).html$',
                   views.ficha_acreditado_de_habilitacion_de_negocios,
                   name='credencialeshabilitaciondenegocios.ficha'),
               ]
