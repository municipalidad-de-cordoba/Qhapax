from django.shortcuts import render
from credencialeshabilitaciondenegocios.models import AcreditadoParaHabilitarNegocios
import django_excel as excel
from django.views.decorators.cache import cache_page
from django.shortcuts import get_object_or_404


@cache_page(60 * 60 * 12)  # 12 h
def ficha_acreditado_de_habilitacion_de_negocios(request, pk):
    '''
    Ficha de un guía turístico
    http://credenciales-prensa/acreditado-prensa-2.html
    '''
    habilitador = get_object_or_404(AcreditadoParaHabilitarNegocios, pk=pk)
    base_url = request.website
    url = '{}{}'.format(base_url, habilitador.get_absolute_url())
    qr_image = habilitador.get_image_qr(url=url)
    qr_download = habilitador.get_download_link_qr(url=url)
    context = {
        'prensa': habilitador,
        'qr_image': qr_image,
        'qr_download': qr_download}
    url = 'ficha.html'
    return render(request, url, context)


@cache_page(60 * 60 * 12)  # 12 h
def planilla_acreditados_habilitacion_de_negocios(request, filetype):
    '''
    Planilla de acreditados de habilitacion para negocios
    http://localhost:8000/credenciales-habilitadores-de-negocios/lista-de-habilitadores-de-negocios.xls
    '''

    habilitadores = AcreditadoParaHabilitarNegocios.objects.filter(
        publicado=True).order_by('apellido', 'nombre')
    csv_list = []
    csv_list.append(['Codigo interno',
                     'Apellido',
                     'Nombre',
                     'DNI',
                     'Genero',
                     'Edad',
                     'url',
                     'url_foto_thumbnail',
                     'acreditación'])

    for habilitador in habilitadores:
        dni = '{} {}'.format(habilitador.tipo_id, habilitador.unique_id)
        url = "{}{}".format(request.website, habilitador.get_absolute_url())

        try:
            foto_thumb = "{}{}".format(
                request.website,
                habilitador.foto.thumbnail['800x800'].url)
        except (ValueError, FileNotFoundError):
            foto_thumb = ""

        csv_list.append([
            habilitador.numero_de_chapa,
            habilitador.apellido,
            habilitador.nombre,
            dni,
            habilitador.get_genero_display(),
            habilitador.edad,
            url,
            foto_thumb,
            habilitador.detalles_acreditacion
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
