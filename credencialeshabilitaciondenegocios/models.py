from django.db import models
from core.models import Persona
from django.core.urlresolvers import reverse


class AcreditadoParaHabilitarNegocios(Persona):

    cv_archivo = models.FileField(
        upload_to='CVs-habilitacion-negocios/',
        null=True,
        blank=True,
        verbose_name='Curriculum en PDF, DOC u otros')
    publicado = models.BooleanField(default=False)
    numero_de_chapa = models.CharField(max_length=30, null=True, blank=True)
    detalles_acreditacion = models.TextField(
        null=True,
        blank=True,
        help_text='Tareas para las que está acreditado u observaciones')

    def get_absolute_url(self):
        return reverse(
            'credencialeshabilitaciondenegocios.ficha',
            kwargs={
                'pk': self.pk})
