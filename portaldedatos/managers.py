from django.db import models
from django.db.models import Max, Min


class DatosManager(models.Manager):
	''' listas de datos específicos
		analizar si se requieren separa por Portales específicos
		'''

	def publicados(self, portal=None):
		''' los que se pueden mostrar '''
		q = self.filter(estado=self.model.PUBLICADO, categoria__portal__publicado=True)
		if portal is not None:
			q = q.filter(categoria__portal=portal)
		return q

	def mas_visitados(self, cantidad=5, portal=None):
		''' datos del portal mas visitados '''

		# FIXME esta versión es de prueba
		q = self.publicados().order_by('-importancia')[:cantidad]

		if portal is not None:
			q = q.filter(categoria__portal=portal)

		return q

	def ultimos_subidos(self, cantidad=5, portal=None):
		''' ultimos subidos. Analizar si necesito datos, versión o recurso '''
		
		q = self.publicados().annotate(ultima_version=Max('versiondato__fecha', distinct=True)).order_by('-ultima_version')[:cantidad]
		
		if portal is not None:
			q = q.filter(categoria__portal=portal)

		return q


class CategoriasManager(models.Manager):

	def tienen_datos(self):
		"""
		Categorias que no tienen subcategorias y tienen directamente datos
		publicados.
		"""
		total_categorias = self.all()
		result = []
		for c in total_categorias:
			if len(c.datos()) != 0:
				result.append(c)
		return result

class RecursosManager(models.Manager):
	''' listas de datos específicos
		analizar si se requieren separa por Portales específicos
		'''

	def ultimos_subidos(self, cantidad=5, portal=None):
		''' ultimos recursos subidos'''
		# FIXME esta versión es de prueba, necesitamos algo que contemple a
		# cual se le subio una nueva versión o cambio un dato
		q = self.order_by('-id')[:cantidad]

		return q
