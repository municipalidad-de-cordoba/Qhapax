from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^(?P<funcionario>[\w-]+)_(?P<anio>[0-9]{4})_(?P<dj_id>[0-9]+).(?P<filetype>csv|xls)$', views.declaracion_jurada, name='portaldedatos.declaraciones_juradas.declaracion_jurada'),
    url(r'^declaraciones-juradas-(?P<anio>[0-9]{4}).(?P<filetype>csv|xls)$', views.declaraciones_juradas, name='portaldedatos.declaraciones_juradas.declaraciones_juradas'),
    url(r'^declaraciones-juradas-historicas.(?P<filetype>csv|xls)$', views.declaraciones_juradas_historicas, name='portaldedatos.declaraciones_juradas.declaraciones_juradas_historicas'),
]
