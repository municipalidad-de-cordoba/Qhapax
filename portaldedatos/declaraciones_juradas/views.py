from decimal import Decimal
from django.conf import settings
from django.http import JsonResponse
from declaracionesjuradas.models import DeclaracionJurada
from funcionarios.models import Funcion

from core.models import Moneda
import django_excel as excel
from django.views.decorators.cache import cache_page


@cache_page(60 * 60 * 12)  # 12 h
def declaracion_jurada(request, funcionario, anio, dj_id, filetype):
    '''
    Declaracion jurada de una persona para un año especifico
    '''

    # obtener una declaración
    dj = DeclaracionJurada.objects.get(pk=dj_id, anio=int(anio), publicada=True)
    csv_list = []
    persona = dj.persona
    csv_list.append(['Nombre', 'Apellido', 'Genero', 'DOC', '', '', ''])
    dni = '{} {}'.format(persona.tipo_id.nombre, persona.unique_id)
    csv_list.append([persona.nombre, persona.apellido, persona.genero, dni, '', '', ''])

    csv_list.append(['', '', '', '', ''])
    csv_list.append(['Bienes', 'Moneda', 'Valor', 'De cónyuge', 'En el país', ''])
    for bien in dj.bien_set.all():
        de_conyuge = 'Si' if bien.es_de_conyuge else 'No'
        en_el_pais = 'Si' if bien.en_el_pais else 'No'
        csv_list.append([bien.tipo.nombre, bien.moneda.simbolo, bien.valor, de_conyuge, en_el_pais, ''])

    csv_list.append(['', '', '', '', '', ''])
    csv_list.append(['Deuda', 'Moneda', 'Valor', 'De cónyuge', 'En el país', 'Con inst financiera'])
    for deuda in dj.deuda_set.all():
        de_conyuge = 'Si' if deuda.es_de_conyuge else 'No'
        en_el_pais = 'Si' if deuda.en_el_pais else 'No'
        con_entidad_financiera = 'Si' if deuda.con_entidad_financiera else 'No'
        csv_list.append(['Deuda', deuda.moneda.simbolo, deuda.valor, de_conyuge, en_el_pais, con_entidad_financiera])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 h
def declaraciones_juradas(request, anio, filetype):
    '''
    Todas las declaraciones juradas de todos los funcionarios de un gobierno para
    un año específico
    '''
    # obtener todas las declaraciones de un gobierno en un año
    ddjj = DeclaracionJurada.objects.filter(anio=int(anio), publicada=True)

    csv_list = []
    mi_moneda = Moneda.objects.get(pk=settings.MONEDA_PK_PRINCIPAL)
    csv_list.append(['Nombre', 'Apellido', 'Genero', 'DOC', 'Bienes ({})'.format(mi_moneda.simbolo), 'Deudas ({})'.format(mi_moneda.simbolo), 'Total', 'PDF'])

    for dj in ddjj:
        persona = dj.persona
        dni = '{} {}'.format(persona.tipo_id.nombre, persona.unique_id)

        # sumar los bienes y las deudas
        bienes = Decimal(0.0)
        for bien in dj.bienes():
            if bien.moneda == mi_moneda:
                bienes += bien.valor
            else:
                # pasar a dolares y luego a la moneda de referencia local (mi_moneda)
                moneda = bien.moneda
                valor_en_mi_moneda = bien.valor * moneda.cantidad_por_dolar * mi_moneda.cantidad_por_dolar
                bienes += valor_en_mi_moneda

        deudas = Decimal(0.0)
        for deuda in dj.deudas():
            if deuda.moneda == mi_moneda:
                deudas += deuda.valor
            else:
                # pasar a dolares y luego a la moneda de referencia local (mi_moneda)
                moneda = deuda.moneda
                deuda_en_mi_moneda = deuda.valor * moneda.cantidad_por_dolar * mi_moneda.cantidad_por_dolar
                deudas += deuda_en_mi_moneda

        total = bienes - deudas
        if dj.archivo:
            dj_pdf = '{}://{}/media/{}'.format(request.website.protocolo, request.website.dominio, dj.archivo)
        else:
            dj_pdf = 'No cargado aún'
        csv_list.append([persona.nombre, persona.apellido, persona.genero, dni, bienes, deudas, total, dj_pdf])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60)  # 60s
def declaraciones_juradas_status(request, anio):
    '''
    Estado general de la presentación de declaraciones juradas.
    Total de funciones y de ddjj presentadas.
    '''
    # ver cuantas declaraciones hay y cuantas funciones (asumimos que todas las funciones requieren declaración)
    ddjj = DeclaracionJurada.objects.filter(anio=int(anio), publicada=True)
    funciones = Funcion.objects.filter(cargo__categoria__requiere_declaracion_jurada=True,
                                        activo=True)

    ret = {
        'ddjj_anio': anio, 'ddjj_gobierno': settings.NOMBRE_GOBIERNO,
        'cargos_activos': len(funciones), 'ddjj_cargadas': len(ddjj)
    }
    return JsonResponse(ret)


@cache_page(60 * 60 * 12)  # 12 h
def declaraciones_juradas_historicas(request, filetype):
    '''
    Todas las declaraciones juradas de todos los funcionarios de un gobierno 
    de todos los años comparativamente
    '''
    # obtener todas las declaraciones de un gobierno en todos los años
    anio_inicia = 2016
    anio_fin = 2018
    
    # DEBE ESTAR ORDENADA para que se calculen las variaciones anuales
    ddjj = DeclaracionJurada.objects.filter(publicada=True).order_by('anio')

    csv_list = []
    mi_moneda = Moneda.objects.get(pk=settings.MONEDA_PK_PRINCIPAL)
    headers = ['Nombre', 'Apellido', 'Genero', 'DOC']
    extra_headers = []
    for anio in range(anio_inicia, anio_fin + 1):
        bsh = 'Bienes {} ({})'.format(anio, mi_moneda.simbolo)
        dsh = 'Deudas {} ({})'.format(anio, mi_moneda.simbolo)
        totalh = 'TOTAL {} ({})'.format(anio, mi_moneda.simbolo)
        extra_headers.append(bsh)
        extra_headers.append(dsh)
        extra_headers.append(totalh)
        if anio > anio_inicia:
            crech = 'Crec % {}-{}'.format(anio, anio - 1)
            extra_headers.append(crech)
    
    headers += extra_headers
    csv_list.append(headers)

    ddjj_final = {}
    
    for dj in ddjj:
        persona = dj.persona
        dni = '{} {}'.format(persona.tipo_id.nombre, persona.unique_id)
        if dni not in ddjj_final.keys():
            ddjj_final[dni] = {}
            for header in headers:
                ddjj_final[dni][header] = '0'
            
        # sumar los bienes y las deudas
        bienes = Decimal(0.0)
        for bien in dj.bienes():
            if bien.moneda == mi_moneda:
                bienes += bien.valor
            else:
                # pasar a dolares y luego a la moneda de referencia local (mi_moneda)
                moneda = bien.moneda
                valor_en_mi_moneda = bien.valor * moneda.cantidad_por_dolar * mi_moneda.cantidad_por_dolar
                bienes += valor_en_mi_moneda

        deudas = Decimal(0.0)
        for deuda in dj.deudas():
            if deuda.moneda == mi_moneda:
                deudas += deuda.valor
            else:
                # pasar a dolares y luego a la moneda de referencia local (mi_moneda)
                moneda = deuda.moneda
                deuda_en_mi_moneda = deuda.valor * moneda.cantidad_por_dolar * mi_moneda.cantidad_por_dolar
                deudas += deuda_en_mi_moneda

        total = bienes - deudas
        
        ddjj_final[dni]['Nombre'] = persona.nombre
        ddjj_final[dni]['Apellido'] = persona.apellido
        ddjj_final[dni]['Genero'] = persona.genero
        ddjj_final[dni]['DOC'] = dni
        
        bsh = 'Bienes {} ({})'.format(dj.anio, mi_moneda.simbolo)
        dsh = 'Deudas {} ({})'.format(dj.anio, mi_moneda.simbolo)
        totalh = 'TOTAL {} ({})'.format(dj.anio, mi_moneda.simbolo)
        ddjj_final[dni][bsh] = bienes
        ddjj_final[dni][dsh] = deudas
        ddjj_final[dni][totalh] = total
        
        if dj.anio > anio_inicia:
            totalh2 = 'TOTAL {} ({})'.format(dj.anio - 1, mi_moneda.simbolo)
            crech = 'Crec % {}-{}'.format(dj.anio, dj.anio - 1)
            d2 = int(ddjj_final[dni][totalh])
            d1 = int(ddjj_final[dni][totalh2])
            if d1 == 0:
                ddjj_final[dni][crech] = 'N/A'
            else:
                ddjj_final[dni][crech] = round(((d2 / d1) - 1) * 100, 2)
                
    for dj in ddjj_final.keys():
        # ordenar para que 
        row = [ddjj_final[dj]['Nombre'], ddjj_final[dj]['Apellido'],
                ddjj_final[dj]['Genero'], ddjj_final[dj]['DOC']]
        for anio in range(anio_inicia, anio_fin + 1):
            bsh = 'Bienes {} ({})'.format(anio, mi_moneda.simbolo)
            dsh = 'Deudas {} ({})'.format(anio, mi_moneda.simbolo)
            totalh = 'TOTAL {} ({})'.format(anio, mi_moneda.simbolo)
            row.append(ddjj_final[dj][bsh])
            row.append(ddjj_final[dj][dsh])
            row.append(ddjj_final[dj][totalh])
            if anio > anio_inicia:
                crech = 'Crec % {}-{}'.format(anio, anio - 1)
                row.append(ddjj_final[dj][crech])
        
        csv_list.append(row)

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
