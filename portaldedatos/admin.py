from django.contrib import admin
from .models import *


class RecursoAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'tipo', 'icon')
    list_filter = ('tipo', 'version_dato', 'version_dato__dato')
    search_fields = ['titulo', 'version_dato__titulo']
    exclude = ('id_externo', 'cod_app_externa')


class CategoriaPortalDatosAdmin(admin.ModelAdmin):
    def file_size(self, obj):
        if obj.icono and hasattr(obj.icono, 'url'):
            sz = obj.icono.file.size / (1024 * 1024)
            return '{:.2f} MB'.format(sz)
        else:
            return "No hay icono"

    def icono_thumb(self, obj):
        if obj.icono:
            try:
                th80 = obj.icono.thumbnail['80x80'].url
            except Exception as e:
                pass
            else:
                return '<img src="{}" style="width: 80px;"/>'.format(obj.icono.thumbnail['80x80'].url)

        return ' <img src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACXBIWXMAAAAnAAAAJwEqCZFPAAAA B3RJTUUH4AcJFDE2B3C7PwAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUH AAAACklEQVQI12P4DwABAQEAG7buVgAAAABJRU5ErkJggg==" alt="blanco " style="width: 80px; height: 120px;" />'

    icono_thumb.short_description = 'thumb'
    icono_thumb.allow_tags = True

    list_display = ['portal', 'nombre', 'slug', 'depende_de', 'icono_thumb', 'file_size']
    search_fields = ['id', 'nombre', 'slug']
    list_filter = ['depende_de',]
    exclude = ('id_externo', 'cod_app_externa')


class ArchivoCSVAdmin(admin.ModelAdmin):
    '''
    def csv_content(self, obj):
        return obj.read(max_rows=2)

    csv_content.short_description = 'Contenido'
    '''
    list_display = ('titulo', 'version_dato', 'archivo_local', 'url_externa', 'encoding')  # TEST, 'csv_content')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    exclude = ('id_externo', 'cod_app_externa')


class ArchivoPDFAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'archivo_local', 'url_externa')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    exclude = ('id_externo', 'cod_app_externa')


class ArchivoODSAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'archivo_local', 'url_externa')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    exclude = ('id_externo', 'cod_app_externa')


class ArchivoODTAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'archivo_local', 'url_externa')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    exclude = ('id_externo', 'cod_app_externa')


class ArchivoKMLAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'archivo_local', 'url_externa')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    exclude = ('id_externo', 'cod_app_externa')


class ArchivoKMZAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'archivo_local', 'url_externa')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    exclude = ('id_externo', 'cod_app_externa')


class ArchivoXLSAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'archivo_local', 'url_externa')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    exclude = ('id_externo', 'cod_app_externa')


class ArchivoDOCAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'archivo_local', 'url_externa')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    exclude = ('id_externo', 'cod_app_externa')


class ArchivoTXTAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'archivo_local', 'url_externa')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    exclude = ('id_externo', 'cod_app_externa')


class ArchivoSHPAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'archivo_local', 'url_externa')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    exclude = ('id_externo', 'cod_app_externa')


class ArchivoJSONAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'archivo_local', 'url_externa')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    exclude = ('id_externo', 'cod_app_externa')


class ArchivoImagenAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'archivo_local', 'url_externa')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    exclude = ('id_externo', 'cod_app_externa')


class ReferenciaLinkAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'url_externa')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    exclude = ('id_externo', 'cod_app_externa')


class VersionDatoAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'dato', 'fecha')
    list_filter = ('fuentes', )
    search_fields = ['titulo', 'dato__titulo']
    exclude = ('id_externo', 'cod_app_externa')


class DatoPortalAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'categoria', 'estado', 'periodicidad')
    search_fields = ['titulo', 'categoria__nombre', 'html']
    list_filter = ('categoria', 'estado', 'periodicidad')
    exclude = ('id_externo', 'cod_app_externa')


class LicenciaDatoAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'descripcion', 'es_licencia_libre')
    search_fields = ['titulo', 'descripcion']
    list_filter = ('es_licencia_libre', )


class CSVExportadoGoogleSheetInline(admin.StackedInline):
    model = CSVExportadoGoogleSheet
    extra = 1
    exclude = ['resource_csv']


class GoogleSheetResourceAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'version_dato', 'mostrar_xlsx', 'mostrar_ods', 'mostrar_pdf')
    list_filter = ('tipo', )
    search_fields = ['titulo', ]
    inlines = [CSVExportadoGoogleSheetInline]
    exclude = ['resource_xlsx', 'resource_ods', 'resource_pdf']


admin.site.register(Portal)
admin.site.register(CategoriaPortalDatos, CategoriaPortalDatosAdmin)
admin.site.register(Fuente)
admin.site.register(TagDatos)
admin.site.register(TipoDato)
admin.site.register(DatoPortal, DatoPortalAdmin)
admin.site.register(VersionDato, VersionDatoAdmin)
admin.site.register(Recurso, RecursoAdmin)
admin.site.register(Archivo)
admin.site.register(ArchivoImagen, ArchivoImagenAdmin)
admin.site.register(ArchivoCSV, ArchivoCSVAdmin)
admin.site.register(ArchivoDOC, ArchivoDOCAdmin)
admin.site.register(ArchivoTXT, ArchivoTXTAdmin)
admin.site.register(ArchivoSHP, ArchivoSHPAdmin)
admin.site.register(ArchivoJSON, ArchivoJSONAdmin)
admin.site.register(ArchivoODS, ArchivoODSAdmin)
admin.site.register(ArchivoODT, ArchivoODTAdmin)
admin.site.register(ArchivoPDF, ArchivoPDFAdmin)
admin.site.register(ArchivoKML, ArchivoKMLAdmin)
admin.site.register(ArchivoKMZ, ArchivoKMZAdmin)
admin.site.register(ArchivoXLS, ArchivoXLSAdmin)
admin.site.register(ReferenciaLink, ReferenciaLinkAdmin)
admin.site.register(Webservice)
admin.site.register(LicenciaDato, LicenciaDatoAdmin)
admin.site.register(GoogleSheetResource, GoogleSheetResourceAdmin)