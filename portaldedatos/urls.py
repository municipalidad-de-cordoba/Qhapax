from django.conf.urls import include, url
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^declaraciones-juradas/', include('portaldedatos.declaraciones_juradas.urls')),
    url(r'^funcionarios/', include('funcionarios.urls')),
    url(r'^sueldos/', include('portaldedatos.sueldos.urls')),
    url(r'^resumen-datos.(?P<filetype>csv|xls)$', views.ResumenDatosDescargable, name='portaldedatos.resumen.datos'),
]