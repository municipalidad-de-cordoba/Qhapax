from django.http import HttpResponse
import django_excel as excel
from portaldedatos.models import DatoPortal
from qhapax.settings import PROTOCOLO_PRODUCCION, DOMINIO_PRODUCCION


def index(request):
    '''
    Mostrar la home page del portal de datos
    '''
    return HttpResponse('Enconstruccion')


def ResumenDatosDescargable(request, filetype):
    """
    Planilla de resumen con todos los datos del portal
    """

    datos = DatoPortal.objects.filter(estado=DatoPortal.PUBLICADO)

    csv_list = []
    csv_list.append(['Titulo', 'Categoria', 'Link'])

    for dato in datos:
        csv_list.append([
            dato.titulo,
            dato.categoria.nombre,
            '{}://{}{}'.format(PROTOCOLO_PRODUCCION,
                               DOMINIO_PRODUCCION,
                               dato.get_absolute_url()
                               )
            ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
