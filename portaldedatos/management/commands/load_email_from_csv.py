#!/usr/bin/python

from django.core.management.base import BaseCommand, CommandError
from django.core.files import File

from portaldedatos.models import ArchivoCSV
from funcionarios.models import Funcion, SueldoReal
from core.models import Persona, Moneda, TipoId, ComunicacionTipo, ComunicacionPersona
from funcionarios.models import Cargo, Funcion, ComunicacionCargo

from django.conf import settings
from django.db.models import Q

import csv
import sys
import os
import re

import datetime
import random

PESO = Moneda.objects.get(pk=settings.MONEDA_PK_PRINCIPAL)

class Command(BaseCommand):
    help = 'Comando para importar csv'
    
    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs='+', type=str)
        parser.add_argument('--force', action='store_true', dest='force', default=False, help="forzar a procesar el archivo")
    
    def _to_dict(self, row):
        _dict={}
        for i in range(0,len(row)):
            _dict[self.__header[i]] = row[i]
        return _dict
            

    def handle(self, *args, **options):

        if options['force']:
            self.stdout.write(self.style.WARNING('---------------------- Forzando importación de CSV ---------------------------'))

        self.stdout.write(self.style.SUCCESS('Importando csv %s ' % (options['csv_file'][0]) ))

        tipoComunicacion = ComunicacionTipo.objects.get(nombre="Email")
        #ComunicacionPersona.objects.filter(tipo=tipoComunicacion).delete()

        email_regex = re.compile("[^@]+@[^@]+\.[^@]+")
        count = 0
        with open(options['csv_file'][0], newline='', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            self.__header = reader.__next__()

            fail=[]
            inactivos=[]
            for row in reader:
                encontrado=True
                datos=self._to_dict(row)

                try:
                    unique_id = Q(unique_id=datos['DNI'].strip())
                    persona = Persona.objects.get(unique_id)
                except (Persona.DoesNotExist, ValueError):
                    fail.append(";".join(row))
                    continue
                    
                # Valida si esta en funciones
                # si no esta en funciones, lo ignora
                if not persona.funcion_set.all()[0].activo:
                    self.stdout.write(self.style.ERROR('Persona "%s (id %s)" no activo' % (persona, datos['persona_id'])))
                    inactivos.append(";".join(row))
                    continue

                try:
                    email = datos['mails'].split("/")[1].split("@")[0].strip()+"@cordoba.gov.ar"
                except IndexError:
                    email = datos['mails'].strip()
    
                if not email_regex.match(email):
                    self.stdout.write(self.style.ERROR('Email no válido "%s"' % email))
                    fail.append(";".join(row))
                    continue

                try:
                    comunicacion = ComunicacionPersona.objects.get(objeto=persona, valor=datos['mails'])
                    self.stdout.write(self.style.ERROR('WARNING: %s ya existe en id_persona %s' %(datos['mails'], persona.id)))
                except ComunicacionPersona.DoesNotExist:
                    comunicacion = ComunicacionPersona(objeto=persona, valor=datos['mails'], privado=False)
                    self.stdout.write(self.style.SUCCESS('SUCCESS: %s OK para id_persona %s' %(datos['mails'], persona.id)))

                comunicacion.privado=False
                comunicacion.tipo=tipoComunicacion

                comunicacion.save()

        
        self.stdout.write(self.style.ERROR('Errores %s:' % len(fail)))
        print(fail)
        self.stdout.write(self.style.ERROR('Inactivos%s:' % len(inactivos)))
        print(inactivos)
