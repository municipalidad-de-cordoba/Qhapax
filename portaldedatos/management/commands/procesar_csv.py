#!/usr/bin/python

from django.core.management.base import BaseCommand, CommandError

from portaldedatos.models import ArchivoCSV

from django.conf import settings

from elasticsearch import Elasticsearch
import csv
import sys

import datetime

class Command(BaseCommand):
    help = 'Comando para importar csv'

    def add_arguments(self, parser):
        parser.add_argument('csv_id', nargs='+', type=int)
        parser.add_argument('--force', action='store_true', dest='force', default=False, help="forzar a procesar el archivo")

    def parse_line(self, row, header=False):
        body = {}
        for i in range(0,len(row)):
            body["field_%03d" % i]=row[i]

        body['header']=header
        return body


    def handle(self, *args, **options):

        if options['force']:
            self.stdout.write(self.style.WARNING('---------------------- Forzando importación de CSV ---------------------------'))

        try:
            if options['force']:
                instance = ArchivoCSV.objects.get(pk=options['csv_id'][0])
            else:
                instance = ArchivoCSV.objects.get(pk=options['csv_id'][0], procesado=False)
        except ArchivoCSV.DoesNotExist:
            if options['force']:
                self.stdout.write(self.style.ERROR('El CSV id: %s no existe' % options['csv_id'][0]))
            else:
                self.stdout.write(self.style.ERROR('El CSV id: %s no existe o ya está procesado' % options['csv_id']))
            sys.exit(1)

        if instance:
            index = "csv_%s" % instance.id

            es = Elasticsearch()
            es.indices.delete(index=index, ignore=[400, 404])
            indices = es.indices.create(index=index, body={}, ignore=400)

            count = 0
            with open(instance.archivo_local.path, newline='') as csvfile:
                reader = csv.reader(csvfile, delimiter=instance.separado_por, quotechar=instance.contenedor_de_texto)
                if instance.tiene_fila_encabezado:
                    header = reader.__next__()
                    es.create(index=index, body=self.parse_line(header,True), doc_type="line_csv", id="line_%d" %count)
                for row in reader:
                    count+=1
                    es.create(index=index, body=self.parse_line(row), doc_type="line_csv", id="line_%d" %count)
#            self.stdout.write(self.style.SUCCESS('Archivo cargado con éxito, se cargaron %s registros' % count))
