#!/usr/bin/python

from django.core.management.base import BaseCommand, CommandError
from django.core.files import File

from portaldedatos.models import ArchivoCSV
from funcionarios.models import Cargo, DireccionCargo

from django.conf import settings
from django.db.models import Q

import csv
import sys
import os
import re

import datetime
import random


class Command(BaseCommand):
    help = 'Comando para importar csv'
    
    WORDS=["subsuelo", "sub suelo", "planta baja", "planta alta", "primer piso", "p.b","p.b.", "pb", "1er piso","mercado de la ciudad"]

    for i in range(0, 50):
        WORDS.append("%i piso"%i)
        WORDS.append("%ipiso"%i)
        WORDS.append("%ito piso"%i)
        WORDS.append("piso %i"%i)

    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs='+', type=str)
        parser.add_argument('--force', action='store_true', dest='force', default=False, help="forzar a procesar el archivo")
    
    def _to_dict(self, row):
        _dict={}
        for i in range(0,len(row)):
            _dict[self.__header[i]] = row[i]
        return _dict

    def parse(self, d):
        piso=None
        #d = d.replace("á","a").replace("é","e").replace("í","i").replace("ó","o").replace("ú","u").replace("ñ","n")
        #d = bytearray(d.strip().replace("-",",").replace(" ,",","), "ascii", "ignore").decode("ascii").lower()
        d = d.replace("º","").replace("–",",").replace("-",",").lower()
        print("pre: %s" %d)
        d=d.replace("bv.","avenida")
        d=d.replace("b°",", barrio")
        d=d.replace("°","")
        d=d.replace("esq.",", esquina")
        d=d.replace("esquina",", esquina")
        d=d.replace("bv","avenida")
        d=d.replace("av.av.","avenida")
        d=d.replace("av.","avenida")
        d=d.replace("simon bolivar", "general simon bolivar")
        d=d.replace("marcelo t de alvear","avenida Avenida Marcelo T. de Alvear")
        d=d.replace("marcelo t alvear","Avenida Marcelo T. de Alvear")
        d=d.replace("marcelo t. alvear","Avenida Marcelo T. de Alvear")
        d=d.replace("marcelo t. de alvear","Avenida Marcelo T. de Alvear")
        d=d.lower()
        d=d.replace("avenida avenida", "avenida")

        for patt in self.WORDS:
            m = re.search(patt, d)
            if m:
                piso = m.group()
                d=d.replace(piso,"")

        print("pos: %s" %d)
        return (d,piso)

    def handle(self, *args, **options):

        if options['force']:
            self.stdout.write(self.style.WARNING('---------------------- Forzando importación de CSV ---------------------------'))

        self.stdout.write(self.style.SUCCESS('Importando csv %s ' % (options['csv_file'][0]) ))

        DireccionCargo.objects.all().delete()
        count = 0
        with open(options['csv_file'][0], newline='', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            self.__header = reader.__next__()

            fail=[]
            inactivos=[]
            for row in reader:
                encontrado=True
                datos=self._to_dict(row)
                (direccion,piso) = self.parse(datos['Dirección - piso'])

                try:
                    cargo_id = Q(pk=datos['c_id'].strip())
                    cargo = Cargo.objects.get(cargo_id)
                except (Cargo.DoesNotExist, ValueError):
                    fail.append(";".join(row))
                    continue

                if direccion:
                    direccion = direccion+", cordoba, departamento capital, cordoba"
                    print("acá: %s" % direccion)
                    DireccionCargo(direccion = direccion.title(), objeto = cargo, piso=piso).save()

        print(fail)
