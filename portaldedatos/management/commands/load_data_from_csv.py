#!/usr/bin/python

from django.core.management.base import BaseCommand, CommandError
from django.core.files import File
from django.core.exceptions import MultipleObjectsReturned

from portaldedatos.models import ArchivoCSV
from funcionarios.models import Funcion, SueldoReal
from core.models import Persona, Moneda, TipoId, ComunicacionTipo, ComunicacionPersona
from funcionarios.models import Cargo, Funcion, ComunicacionCargo

from django.conf import settings
from django.db.models import Q

import csv
import sys
import os

import datetime
import random

PESO = Moneda.objects.get(pk=settings.MONEDA_PK_PRINCIPAL)

class Command(BaseCommand):
    help = 'Comando para importar csv'
    
    DNI = TipoId.objects.get(pk=1)
    COMUNICACION_TIPO={x.nombre.lower().replace(" ","_"): x for x in ComunicacionTipo.objects.all()}

    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs='+', type=str)
        parser.add_argument('--force', action='store_true', dest='force', default=False, help="forzar a procesar el archivo")


    CONVERSION={
        'p_id': 'persona_id',
        'c_id': 'cargo_id',
        'ff_id': 'funcion_id',
        'apellido': 'persona_apellido',
        'Nombre': 'persona_nombre',
        'Dirección - piso': '',
        'Fecha nac': 'persona_fecha_nacimiento',
        'email publico (preguntarle cual es abierto)': 'persona_email_publico',
        'email reparticion (si hubiera)': 'cargo_email_publico',
        'email privado (no se publicará)': 'persona_email_privado',
        'email Corporativo (solo Computos carga)': '',
        'Tel reparticion': 'cargo_telefono_publico',
        'Celular corporativo': 'persona_celular_corporativo_privado',
        'Celular privado (no se publicará)': 'persona_celular_privado',
        'Twitter': 'persona_twitter',
    }

    
    def _to_dict(self, row):
        _dict={}
        for i in range(0,len(row)):
            _dict[self.CONVERSION[self.__header[i]]] = row[i]
        return _dict
            

    def handle(self, *args, **options):

        if options['force']:
            self.stdout.write(self.style.WARNING('---------------------- Forzando importación de CSV ---------------------------'))

        self.stdout.write(self.style.SUCCESS('Importando csv %s ' % (options['csv_file'][0]) ))

        #ComunicacionCargo.objects.all().delete()
        #ComunicacionPersona.objects.all().delete()
        count=0
        with open(options['csv_file'][0], newline='', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            self.__header = reader.__next__()

            fail=[]
            inactivos=[]
            for row in reader:
                encontrado=True
                datos=self._to_dict(row)


                try:
                    fecha=datetime.datetime.strptime(datos['persona_fecha_nacimiento'], "%Y-%m-%d")
                except:
                    fecha=None

                if not datos['persona_id']:
                    datos['persona_id']=None


                pk = Q(pk=datos['persona_id'])

                try:
                    persona = Persona.objects.get(pk)
                except Persona.DoesNotExist:
                    fail.append(";".join(row))
                    continue
                    
                try:
                    # Valida si esta en funciones
                    # si no esta en funciones, lo ignora
                    if not persona.funcion_set.all()[0].activo:
                        self.stdout.write(self.style.ERROR('Persona "%s (id %s)" no activo' % (persona, datos['persona_id'])))
                        inactivos.append(";".join(row))
                except IndexError:
                    self.stdout.write(self.style.ERROR('Persona "%s (id %s)" no tiene funcion' % (persona, datos['persona_id'])))
            
                    
                
                persona.nombre=datos['persona_nombre']
                persona.apellido=datos['persona_apellido']
                persona.fecha_nacimiento=fecha
                persona.tipo_id=self.DNI
                persona.save()

                # lineas de comunicacion para persona
                for i in ("persona_email_publico", "persona_email_privado", "persona_celular_corporativo_privado", "persona_celular_privado", "persona_twitter", "web", "blog"):

                    try:

                        if datos[i]:
                            try:
                                # si ya tiene esa comunicacion
                                ComunicacionPersona.objects.get(valor=datos[i], objeto=persona)
                            except MultipleObjectsReturned:
                                comu = ComunicacionPersona.objects.filter(valor=datos[i], objeto=persona)
                                comu[1].delete()
                            except ComunicacionPersona.DoesNotExist:
                                #si no tiene la comunicacion, la creamos
                                comunicacion = ComunicacionPersona(valor=datos[i], objeto=persona)
                                if "email" in i.split("_"):
                                    comunicacion.tipo=self.COMUNICACION_TIPO["email"]
                                elif i == "persona_celular_corporativo_privado":
                                    comunicacion.tipo=self.COMUNICACION_TIPO["celular_corporativo"]
                                elif i == "persona_celular_privado":
                                    comunicacion.tipo=self.COMUNICACION_TIPO["celular"]
                                else:
                                    comunicacion.tipo=self.COMUNICACION_TIPO[i]
                
                                if "privado" in i.split("_"):
                                    comunicacion.privado=True
                                comunicacion.save()
                    except KeyError:
                        pass

                ### Cargo ################################################################################
                    
                try:
                    cargo = persona.funcion_set.get(pk=datos['funcion_id']).cargo
                except Funcion.DoesNotExist:
                    self.stdout.write(self.style.ERROR('Funcion inexistente: %s' % datos))
                    continue
                except ValueError:
                    self.stdout.write(self.style.ERROR('Sin Funcion id: %s %s' % (datos['funcion_id'], datos)))
                    continue

                for i in datos["cargo_email_publico"].split("-"):
                    if i:
                        ComunicacionCargo(tipo=self.COMUNICACION_TIPO['email'], privado=False, valor=i, objeto=cargo).save()

                for i in datos["cargo_telefono_publico"].split("-"):
                    if i:
                        ComunicacionCargo(tipo=self.COMUNICACION_TIPO['teléfono'], privado=False, valor=i, objeto=cargo).save()
                        
                count+=1
        self.stdout.write(self.style.SUCCESS('Archivo cargado con éxito, se cargaron %s registros' % count))
        self.stdout.write(self.style.ERROR('Registro con errores: %s' % len(fail)))
        self.stdout.write(self.style.ERROR('Registro inactivos: %s' % len(inactivos)))
        print(";".join(self.__header))
        print("\n".join(fail))
        print(";".join(self.__header))
        print("\n".join(inactivos))
