#!/usr/bin/python

from django.core.management.base import BaseCommand

from portaldedatos.models import ArchivoCSV
from funcionarios.models import Funcion, SueldoReal
from core.models import Persona, Moneda

from django.conf import settings

import csv
import sys

import datetime

PESO = Moneda.objects.get(pk=settings.MONEDA_PK_PRINCIPAL)


class Command(BaseCommand):
    help = 'Comando para importar csv a sueldo'

    def add_arguments(self, parser):
        parser.add_argument('csv_id', nargs='+', type=int)
        parser.add_argument('mes', nargs='+', type=int)
        parser.add_argument('anio', nargs='+', type=int)
        parser.add_argument('--force', action='store_true', dest='force',
                            default=False, help="forzar a procesar el archivo")

    def _to_dict(self, row):
        _dict = {}
        for i in range(0, len(row)):
            _dict[self.__header[i]] = row[i]
        return _dict

    def handle(self, *args, **options):
        force = options['force']
        if force:
            self.stdout.write(self.style.WARNING('---------------------- Forzando importación de CSV ---------------------------'))

        try:
            if force:
                instance = ArchivoCSV.objects.get(pk=options['csv_id'][0])
            else:
                instance = ArchivoCSV.objects.get(pk=options['csv_id'][0], procesado=False)
        except ArchivoCSV.DoesNotExist:
            if force:
                self.stdout.write(self.style.ERROR('El CSV id: %s no existe' % options['csv_id']))
            else:
                self.stdout.write(self.style.ERROR('El CSV id: %s no existe o ya está procesado' % options['csv_id']))
            sys.exit(1)

        if int(options['mes'][0]) not in range(1, 13):
            self.stdout.write(self.style.ERROR('El mes %s NO es un mes válido' % options['mes'][0]))
            sys.exit(1)

        activos = Funcion.objects.filter(activo=True).values("funcionario__unique_id")
        dni_en_db = Persona.objects.all().values("unique_id")

        if instance:

            no_dni = []
            no_activos = []

            # anio = datetime.datetime.today().year
            anio = int(options['anio'][0])
            fecha = datetime.date(anio, options['mes'][0], 1)

            self.stdout.write(self.style.SUCCESS('Importando csv (id: %s) para la fecha %s' % (instance.id, fecha)))

            count = 0
            with open(instance.archivo_local.path, newline='') as csvfile:
                reader = csv.reader(csvfile, delimiter=instance.separado_por, quotechar=instance.contenedor_de_texto)
                if instance.tiene_fila_encabezado:
                    self.__header = reader.__next__()
                    header = ';'.join(self.__header)
                for row in reader:
                    line = self._to_dict(row)
                    dni = str(line["DNI"])
                    # si encontramos al funcionario activo:
                    if {'funcionario__unique_id': dni} not in activos:
                        # si no encontramos al funcionario por activo y por dni
                        if {'unique_id': dni} in dni_en_db:
                            no_activos.append(';'.join(row))
                        else:
                            no_dni.append(';'.join(row))
                        continue

                    try:
                        funcion = SueldoReal(mes=fecha, funcion=Funcion.objects.get(funcionario__unique_id=dni, activo=True))
                    except Funcion.MultipleObjectsReturned:
                        self.stdout.write(self.style.ERROR('Falló en cargar el sueldo de DNI %s porque encuentra dos funciones' % dni))
                        continue

                    funcion.sueldo_bruto = line['REMUNERATIVO'].replace(",", ".")
                    funcion.sueldo_bruto_moneda = PESO
                    funcion.sueldo_neto = line['LIQUIDO'].replace(",", ".")
                    funcion.sueldo_neto_moneda = PESO
                    funcion.save()
                    self.stdout.write(self.style.SUCCESS('#{} Sueldo OK {} - {}'.format(count, funcion.funcion.funcionario.apellido, funcion.funcion.cargo.oficina)))
                    count += 1

            instance.errores = "--- NO ACTIVOS ---\n{}\n{}--- SIN DNI ---\n{}".format(header, "\n".join(no_activos), "\n".join(no_dni))
            instance.procesado = True
            try:
                instance.save()
            except Exception as e:
                self.stdout.write(self.style.ERROR('Fallo al grabar los errores de procesamiento del CSV (no crítico): {}'.format(e)))

            self.stdout.write(self.style.SUCCESS('Archivo cargado con éxito, se cargaron {} registros'.format(count)))
