from funcionarios.models import *
import django_excel as excel
from django.views.decorators.cache import cache_page
from datetime import date
from core.models import Moneda


@cache_page(60 * 60 * 12)  # 12 h
def sueldos_funcionario(request, funcionario, funcionario_id, filetype):
    '''
    Todos los sueldos de un funcionario por mes
    '''
    sueldos = SueldoReal.objects.filter(funcion__funcionario__id=funcionario_id)
    csv_list = []
    csv_list.append(['Nombre', 'Apellido', 'Genero', 'DOC', 'Mes', 'Moneda', 'Sueldo Bruto', 'Sueldo Neto'])
    for sueldo in sueldos:
        persona = sueldo.funcion.funcionario
        dni = '{} {}'.format(persona.tipo_id.nombre, persona.unique_id)
        csv_list.append([persona.nombre, persona.apellido, persona.genero, dni, sueldo.mes,
                            sueldo.sueldo_neto_moneda, sueldo.sueldo_bruto, sueldo.sueldo_neto])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 h
def sueldos_mes(request, anio, mes, filetype):
    '''
    Todos los sueldos de todos los funcionarios un mes determinado
    '''
    sueldos = SueldoReal.objects.filter(mes=date(int(anio), int(mes), 1))
    csv_list = []
    csv_list.append(['Nombre', 'Apellido', 'Genero', 'DOC', 'Mes', 'Moneda', 'Sueldo Bruto', 'Sueldo Neto'])
    for sueldo in sueldos:
        persona = sueldo.funcion.funcionario
        dni = '{} {}'.format(persona.tipo_id.nombre, persona.unique_id)
        snm = sueldo.sueldo_neto_moneda.simbolo
        csv_list.append([persona.nombre, persona.apellido, persona.genero, dni, sueldo.mes,
                            snm, sueldo.sueldo_bruto, sueldo.sueldo_neto])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
