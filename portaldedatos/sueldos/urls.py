from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^(?P<funcionario>[\w-]+)_(?P<funcionario_id>[0-9]+).(?P<filetype>csv|xls)$', views.sueldos_funcionario, name='portaldedatos.declaraciones_juradas.declaracion_jurada'),
    url(r'^sueldos-(?P<anio>[0-9]{4})-(?P<mes>[0-9]{2}).(?P<filetype>csv|xls)$', views.sueldos_mes, name='portaldedatos.declaraciones_juradas.declaraciones_juradas'),
]
