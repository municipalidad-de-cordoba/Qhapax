from haystack import indexes
from .models import *


class PortalIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    titulo = indexes.CharField(model_attr='titulo')
    html = indexes.CharField(model_attr='html', null=True)

    def get_model(self):
        return Portal

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(publicado=True)


class CategoriaPortalDatosIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    nombre = indexes.CharField(model_attr='nombre')

    def get_model(self):
        return CategoriaPortalDatos

    def index_queryset(self, using=None):
        return self.get_model().objects.all()


class DatoPortalIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    titulo = indexes.CharField(model_attr='titulo')
    descripcion = indexes.CharField(model_attr='descripcion', null=True)
    html = indexes.CharField(model_attr='html', null=True)

    def get_model(self):
        return DatoPortal

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(estado=DatoPortal.PUBLICADO)
