from django.apps import AppConfig


class PortaldedatosConfig(AppConfig):
    name = 'portaldedatos'
