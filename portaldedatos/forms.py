from django_select2.forms import Select2MultipleWidget, ModelSelect2Widget
from .models import Recurso


class RecursoWidget(Select2MultipleWidget):
    model = Recurso
    search_fields = [
                        'version_dato__dato__titulo__icontains',
                        'version_dato__titulo__icontains',
                        'icon__icontains',
                        'titulo__icontains'
                    ]
    max_results = 5

    def label_from_instance(self, obj):
        return '{} : {} : {}.{} ({})'.format(obj.version_dato.dato.titulo, obj.version_dato.titulo, obj.titulo, obj.icon, obj.tipo)

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault('data-placeholder', 'Ingrese recurso o parte de ella')
        self.attrs.setdefault('data-minimum-input-length', 3)
        self.attrs.setdefault('data-width', '25em')

        return super(RecursoWidget, self).build_attrs(*args, **kwargs)


class RecursoWidgetSelect(ModelSelect2Widget):
    model = Recurso
    search_fields = [
                        'version_dato__dato__titulo__icontains',
                        'version_dato__titulo__icontains',
                        'icon__icontains',
                        'titulo__icontains'
                    ]
    max_results = 5

    def label_from_instance(self, obj):
        return '{} : {} : {}.{} ({})'.format(obj.version_dato.dato.titulo, obj.version_dato.titulo, obj.titulo, obj.icon, obj.tipo)

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault('data-placeholder', 'Ingrese recurso o parte de ella')
        self.attrs.setdefault('data-minimum-input-length', 3)
        self.attrs.setdefault('data-width', '25em')

        return super(RecursoWidgetSelect, self).build_attrs(*args, **kwargs)