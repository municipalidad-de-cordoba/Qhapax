# -*- coding: utf-8 -*-
from ckeditor.fields import RichTextField
from django.utils.text import slugify
from django.db import models
from django.utils import timezone
from datetime import timedelta
from django.core.urlresolvers import reverse
from portaldedatos.managers import DatosManager, CategoriasManager, RecursosManager
from core.models import Comunicacion
from funcionarios.models import Cargo
import logging
from versatileimagefield.fields import VersatileImageField, PPOIField
logger = logging.getLogger(__name__)


class Portal(models.Model):
    '''
    Cada uno de los portales de datos de un gobierno
    '''
    titulo = models.CharField(max_length=90)
    publicado = models.BooleanField(default=False)
    slug = models.SlugField(max_length=140, null=True, blank=True)
    html = RichTextField(null=True, blank=True)
    imagen = models.ImageField(upload_to='imagenes/portal-de-datos', null=True, blank=True)
    color_1 = models.CharField(max_length=200, null=True, blank=True)
    color_2 = models.CharField(max_length=200, null=True, blank=True)
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        super(Portal, self).save(*args, **kwargs)

    def __str__(self):
        return 'Portal de datos {}'.format(self.titulo)

    def get_absolute_url(self):
        return reverse('website.portaldatos.home', kwargs={'portal': self.slug})

    class Meta:
        verbose_name_plural = "Portales de datos"
        verbose_name = "Portal de datos"
        ordering = ['titulo']


class CategoriaPortalDatos(models.Model):
    '''
    Categorías de un portal de datos en particular
    '''
    portal = models.ForeignKey(Portal, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=95)
    slug = models.SlugField(max_length=140, null=True, blank=True)
    # permitir anidar en jerarquias
    depende_de = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    importancia = models.PositiveIntegerField(default=100)

    icono = VersatileImageField(
        upload_to='iconos/portaldatos_categorias',
        ppoi_field='icono_ppoi',
        null=True,
        blank=True
    )
    icono_ppoi = PPOIField('Image PPOI')

    objects = CategoriasManager()

    def __str__(self):
        return self.nombre

    def get_absolute_url(self):
        return reverse('website.portaldatos.categoria', kwargs={'categoria': self.slug, 'portal': self.portal.slug})

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        super(CategoriaPortalDatos, self).save(*args, **kwargs)

    def datos(self, solo_publicados=True):
        return self.datoportal_set.filter(estado=DatoPortal.PUBLICADO) if solo_publicados else self.datoportal_set.all()

    def versiones(self, solo_publicados=True):
        versiones = []
        datos = self.datos(solo_publicados)
        for dato in datos:
            versiones += dato.versiones()
        return versiones

    def recursos(self, solo_publicados=True):
        recursos = []
        datos = self.datos(solo_publicados)
        for dato in datos:
            versiones = dato.versiones()
            for version in versiones:
                recursos += version.recursos()
        return recursos

    def dependencias(self, recursive=False):
        """
        Obtener árbol de datos dependientes.
        """
        deps = []
        for categoria in self.categoriaportaldatos_set.all():
            deps.append(categoria)
            if recursive:
                d = categoria.dependencias(recursive=recursive)
                if d is not None:
                    for dep in d:
                        deps.append(dep)

        return deps or None

    class Meta:
        unique_together = (("portal", "nombre"),)
        ordering = ['nombre']
        verbose_name_plural = "Categorías de datos"
        verbose_name = "Categoría de datos"


class Fuente(models.Model):
    '''
    Cada una de las fuentes de los datos usados. En general serán oficinas de gobiernos,
    investigadores, medios de comunicación, organizaciones, etc.
    Deben poder ser reutilizables entre diferentes clientes/gobiernos
    '''
    nombre = models.CharField(max_length=95)
    # para los casos que sea una fuente formal
    oficina_local = models.ForeignKey(Cargo, null=True, on_delete=models.SET_NULL, blank=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Fuentes de datos"
        verbose_name = "Fuente de datos"
        ordering = ['nombre']


class TagDatos(models.Model):
    '''
    Etiquetas que luego ayudaran a encontrar los datos. Compartidas entre todos los gobiernos
    '''

    nombre = models.CharField(max_length=45, unique=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Tags de datos"
        verbose_name = "Tag de datos"
        ordering = ['nombre']


class ComunicacionFuente(Comunicacion):
    objeto = models.ForeignKey(Fuente)


class TipoDato(models.Model):
    '''
    Cada uno de los tipos de datos que tenemos. Desde simples PDFs hasta complejos webservices
    '''
    ARCHIVO = 10
    WEBSERVICE = 20
    BASE_DE_DATOS = 30
    LINK = 40  # Cuando todavía no es un dato real, solo referencia a otra web con datos
    GOOGLE_DOC = 50  # los archivos en Google tienen capacidades especiales que no encajan exactamente en las demás
    tipos = (
                (ARCHIVO, 'Archivo'),
                (WEBSERVICE, 'Web Service'),
                (BASE_DE_DATOS, 'Base de datos'),
                (LINK, 'Referencia externa'),
                (GOOGLE_DOC, 'Documento en Google Drive'))
    tipo = models.PositiveIntegerField(choices=tipos, default=ARCHIVO)
    subtipo = models.CharField(max_length=45)  # para archivo local o web, para webservice SOAP o REST, etc
    # icono = #TODO poner en el template los iconos del tipo de archivo para mostrar.
    # cada subclase lo implementará a gusto
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = (("tipo", "subtipo"),)
        verbose_name_plural = "Tipos de datos"
        verbose_name = "Tipo de datos"
        ordering = ['tipo', 'subtipo']

    def __str__(self):
        return '{} ({})'.format(self.subtipo, self.get_tipo_display())


class DatoPortal(models.Model):
    '''
    Cada serie datos sobre un tema especifico.
    Es a modo descriptivo, no es un dato usable, el recurso que contiene internamente si los es.
    '''
    # identifica a este dato dentro de una categoria, portal y gobierno
    categoria = models.ForeignKey(CategoriaPortalDatos, on_delete=models.CASCADE, blank=True)

    NO_PUBLICADO = 10
    PUBLICADO = 20
    PRIVADO = 30
    estados = ((NO_PUBLICADO, 'No Publicado'), (PUBLICADO, 'Publicado'), (PRIVADO, 'Privado'))
    estado = models.PositiveIntegerField(choices=estados, default=NO_PUBLICADO)

    MENSUAL = 10
    ANUAL = 20
    A_DEMANDA = 30
    SEMESTRAL = 40
    DIARIO = 50
    ONLINE = 60
    TRIMESTRAL = 70
    BIMENSUAL = 80
    QUINCENAL = 90

    DELTAS = {MENSUAL: 30, BIMENSUAL: 60, SEMESTRAL: 180,
              ANUAL: 365, TRIMESTRAL: 90, QUINCENAL: 15, DIARIO: 1,
              ONLINE: 5000,  # Online quiere decir que lee la base de datos y no modifica fecha
              A_DEMANDA: 5000  # A demanda no tiene especificado nada
              }

    periodicidades = (
                        (MENSUAL, 'Mensual'),
                        (ANUAL, 'Anual'),
                        (SEMESTRAL, 'Semestral'),
                        (A_DEMANDA, 'A demanda'),
                        (DIARIO, 'Diario'),
                        (ONLINE, 'En tiempo real'),
                        (BIMENSUAL, 'Cada dos meses'),
                        (QUINCENAL, 'Cada quince días'),
                        (TRIMESTRAL, 'Trimestral')
                        )
    periodicidad = models.PositiveIntegerField(choices=periodicidades, default=A_DEMANDA)

    titulo = models.CharField(max_length=90)
    slug = models.SlugField(max_length=90, null=True, blank=True)
    descripcion = models.CharField(max_length=200, null=True, blank=True)
    html = RichTextField(null=True, blank=True)  # Opcional para mostrar contenidos mas complejos
    datos_relacionados = models.ManyToManyField('self', blank=True)
    tags = models.ManyToManyField(TagDatos, blank=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    importancia = models.PositiveIntegerField(default=100)

    # el dato puede venir de otro producto de softwa y requerir sincronizacion con el.
    # marcar esto para poder sincronizar periódicamente.
    id_externo = models.PositiveIntegerField(default=0)
    cod_app_externa = models.CharField(max_length=30, null=True, blank=True)

    objects = DatosManager()

    def __str__(self):
        return self.titulo

    @classmethod
    def desactualizados(cls):
        datasets = []
        for dataset in DatoPortal.objects.filter(periodicidad__in=cls.DELTAS.keys(), estado=cls.PUBLICADO):

            if dataset.retraso_nueva_version > 0:
                datasets.append(dataset)

        return datasets

    def get_absolute_url(self):
        return reverse('website.portaldatos.dato', kwargs={'dato': self.slug, 'dato_id': self.id, 'categoria': self.categoria.slug, 'portal': self.categoria.portal.slug})

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        super(DatoPortal, self).save(*args, **kwargs)

    @property
    def fecha_ultima_version(self):
        versiones = self.versiondato_set.all()
        if len(versiones) > 0:
            versiones = versiones.order_by('-fecha').values('fecha')
            fecha = versiones[0]['fecha']
        else:
            fecha = None
        return fecha

    @property
    def fecha_esperada_nueva_version(self):
        delta = DatoPortal.DELTAS.get(self.periodicidad)
        if delta:
            return None if not self.fecha_ultima_version else self.fecha_ultima_version + timedelta(days=delta)


    @property
    def retraso_nueva_version(self):
        fecha = self.fecha_esperada_nueva_version
        if fecha:
            return (timezone.now() - fecha).days
        else:
            return 0

    def versiones(self):
        # FIXME para que pusiste esta línea Gaitán? La comento por ahora
        # delta = DatoPortal.DELTAS[self.periodicidad]
        return self.versiondato_set.all()

    def recursos(self):
        recursos = []
        for version in self.versiones():
            recursos += version.recursos()
        return recursos

    class Meta:
        permissions = (
            ('unpublish', 'Puede despublicar'),
            ('publish', 'Puede publicar'),
            )
        unique_together = (("categoria", "slug"),)
        verbose_name_plural = "Datos del portal"
        verbose_name = "Dato de portal"
        ordering = ['titulo']


class VersionDato(models.Model):
    '''
    Cada una de las versiones o fechas en las que se publico un dato
    la fuente se puede cambiar, se pueden anotar novedades
    '''
    dato = models.ForeignKey(DatoPortal)
    titulo = models.CharField(max_length=90)
    fuentes = models.ManyToManyField(Fuente, blank=True)
    novedades = models.CharField(max_length=200, null=True, blank=True)
    html = RichTextField(null=True, blank=True)  # Opcional para mostrar contenidos mas complejos
    fecha = models.DateTimeField(default=timezone.now, auto_now=False, auto_now_add=False)  # fecha para publicar
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    # el dato puede venir de otro producto de softwa y requerir sincronizacion con el.
    # marcar esto para poder sincronizar periódicamente.
    id_externo = models.PositiveIntegerField(default=0)
    cod_app_externa = models.CharField(max_length=30, null=True, blank=True)
    
    def __str__(self):
        return "{} {}".format(self.dato.titulo, self.titulo)

    def recursos(self):
        return self.recurso_set.all().order_by('orden', 'titulo')

    class Meta:
        verbose_name_plural = "Versiones de datos"
        verbose_name = "Versio de dato"
        ordering = ['dato', 'titulo']


class LicenciaDato(models.Model):
    """ licencias de publicación de los datos """
    titulo = models.CharField(max_length=90)
    descripcion = models.TextField(null=True, blank=True)
    # marcar si la licencia es "libre", desconocido o definitivamente cerrada
    es_licencia_libre = models.NullBooleanField(default=True, null=True)

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name_plural = "Licencias de datos"
        verbose_name = "Licencia de dato"
        ordering = ['titulo']


class Recurso(models.Model):
    '''
    Definicion general de los recursos, cada tipo implementa a su manera.
    Agrupador general de datos.
    '''
    titulo = models.CharField(max_length=90, null=True, blank=True)
    version_dato = models.ForeignKey(VersionDato, on_delete=models.CASCADE)
    tipo = models.ForeignKey(TipoDato, on_delete=models.CASCADE)
    licencia = models.ForeignKey(LicenciaDato, null=True, blank=True)

    # el dato puede venir de otro producto de software y requerir sincronizacion con el.
    # marcar esto para poder sincronizar periódicamente.
    id_externo = models.PositiveIntegerField(default=0)
    cod_app_externa = models.CharField(max_length=30, null=True, blank=True)
    
    # orden para mostrar los recursos por tipo de manera predeterminada
    ORDER_DEFAULT = 1000

    ORDER_GOOGLE = 100
    ORDER_XLS = 200
    ORDER_ODS = 250
    ORDER_GOOGLE_CSV = 295
    ORDER_CSV = 300
    ORDER_WEBSERVICE = 350
    ORDER_KML = 400
    ORDER_KMZ = 450
    ORDER_DOC = 600
    ORDER_ODT = 700
    ORDER_SHP = 800
    ORDER_TXT = 900
    ORDER_JSON = 950
    ORDER_DB = 1100
    ORDER_LINK = 1200
    ORDER_PDF = 1500
    ORDER_IMAGEN = 2000

    orden = models.PositiveIntegerField(default=ORDER_DEFAULT,
            help_text=('Predeterminado se ordena por tipo de archivo '
                'dentro de un dato. Se ordena de menor a mayor.'
                'Modificar solo si es necesario un orden especial'))

    # el tipo es dinamico, puedo no tener icono definido para el
    # la extension del archivo muchas veces no resuelve la idea de lo que queremos mostrar del archivo
    # algunas URLs no tienen extensión pero si son archivo de una clase específica
    icon = models.CharField(max_length=20, null=True, blank=True)

    objects = RecursosManager()

    @property
    def url(self):
        if hasattr(self, 'archivo'):
            url = self.archivo.url
        elif hasattr(self, 'webservice'):
            url = self.webservice.url_externa
        elif hasattr(self, 'referencialink'):
            url = self.referencialink.url_externa
        elif hasattr(self, 'googlesheetresource'):
            url = self.googlesheetresource.url_externa
        else:
            url = None

        return url

    def __str__(self):
        return '{} : {} : {}.{} ({})'.format(self.version_dato.dato.titulo, self.version_dato.titulo, self.titulo, self.icon, self.tipo)

    def certificacion_blockchain(self):
        # si tenemos registro de este dato en blockchain mostrar aquí
        certificaciones = self.documentocertificadoblockchain_set.filter(publicado=True).order_by('-id')  # devolver la última
        if len(certificaciones) > 0:
            return certificaciones[0]
        return None

    class Meta:
        verbose_name_plural = "Recursos de datos"
        verbose_name = "Recurso de datos"
        ordering = ['version_dato__dato', 'version_dato', 'titulo']


class Archivo(Recurso):
    '''
    Un archivo local o web. O usa file, o usa url
    '''
    archivo_local = models.FileField(upload_to='datos/', blank=True)  # #TODO incluir gobierno (y algo mas) en el path para separar
    url_externa = models.URLField(null=True, blank=True)  # solo en recursos externos. Si hay un CDN para archivos internos la URL publica se construira basado en file
    # algunos archivos XLS o CSV en realidad son dinámicos y no deben descargarse ya que la info cambia
    dinamico = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        '''
        al grabar si es una URL externa debe analizarse el tamaño y
        bajar a nuestro disco para analisis y procesos posteriores
        '''
        if not self.icon:  # si no define ícono uso la extension
            self.icon = self.extension()

        #TODO hacer un test case para saber si solo se descarga una vez
        if self.url_externa and (not self.archivo_local or self.archivo_local == ''):
            if not self.dinamico:
                self.download()

        super(Archivo, self).save(*args, **kwargs)


    def download(self):
        '''
        descargar lo que este en URL externa y grabarlo como local
        preferentemente para usar cuando se define por primera vez como URL externa
        '''
        import requests
        import os
        import tempfile
        from django.core.files import File
        from urllib.parse import urlparse

        r = requests.get(self.url_externa)
        filename = os.path.basename(urlparse(self.url_externa).path)

        tf = tempfile.SpooledTemporaryFile(max_size=50000)  # hasta 50K queda en memoria, no escribe a /tmp
        tf.write(r.content)
        django_file = File(tf)

        self.archivo_local.save(filename, django_file, save=True)
        tf.close()  # borra el temporal

    @property
    def url(self):
        return self.url_externa if self.dinamico else "/media/{}".format(self.archivo_local.name)

    def extension(self):
        return self.url.split('.')[-1]

    class Meta:
        verbose_name_plural = "Archivos de datos"
        verbose_name = "Archivo de datos"


class ArchivoImagen(Archivo):
    '''
    Un simple archivo de imagen
    '''
    alt = models.CharField(max_length=145, null=True, blank=True)  # intervenr el save para esto: defaul=self.version_dato.dato.titulo)  # texto alternativo de la imagen
    title = models.CharField(max_length=145, null=True, blank=True)  # intervenr el save para esto:,defaul=self.version_dato.dato.titulo)  # titulo de la imagen para html

    def save(self, *args, **kwargs):
        ''' tomar info del dato por defecto'''
        if self.alt is None or self.alt == '':
            self.alt = self.version_dato.dato.titulo

        if not self.title or self.title == '':
            self.title = self.version_dato.dato.titulo

        if self.orden == Recurso.ORDER_DEFAULT:  # el valor por defecto
            self.orden = Recurso.ORDER_PDF

        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'jpg'

        super(ArchivoImagen, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Archivos de imagenes"
        verbose_name = "Archivo de imagen"


class ArchivoPDF(Archivo):
    '''
    Un archivo PDF
    '''
    # archivo = models.OneToOneField(Archivo, on_delete=models.CASCADE, related_name='archivo_pdf')
    solo_imagenes = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.orden == Recurso.ORDER_DEFAULT:  # el valor por defecto
            self.orden = Recurso.ORDER_PDF

        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'pdf'

        super(ArchivoPDF, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Archivos PDF"
        verbose_name = "Archivo PDF"


class ArchivoCSV(Archivo):
    '''
    un archivo CSV
    '''
    separado_por = models.CharField(max_length=5, default=',')
    contenedor_de_texto = models.CharField(max_length=5, default='"')  # quotechar
    tiene_fila_encabezado = models.BooleanField(default=True)
    encodings = (('utf-8', 'UTF8 (estándar)'), ('latin-1', 'ISO 8859 1 (latino)'))
    encoding = models.CharField(max_length=25, choices=encodings, default='utf-8')
    # algunos CSV se suben para ser importados o procesador por el sistema
    # esta es una marca para esos casos
    procesado = models.BooleanField(default=False)
    # errores del ultimo procesamiento registrados para revision
    errores = models.TextField(blank=True, null=True)

    def read(self, max_rows=None):
        '''
        leer todo o una parte del CSV
        #TODO no esta optimizado para archivos de muchas filas
        '''
        import csv
        results = {'fieldnames': [], 'rows': []}

        fieldnames = None
        with open(self.archivo_local.path, newline='', encoding=self.encoding) as csvfile:
            reader = csv.reader(csvfile, delimiter=self.separado_por, quotechar=self.contenedor_de_texto)
            if self.tiene_fila_encabezado:
                fieldnames = next(reader)
                row_n = 0
            else:
                row = next(reader)
                results['rows'].append(row)
                # darle nombre generico a las columnas si no están
                fieldnames = list(range(len(row)))
                row_n = 1

            for row in reader:
                row_n += 1
                results['rows'].append(row)

                if max_rows and row_n >= max_rows:
                    break

        results['fieldnames'] = fieldnames
        return results

    def save(self, *args, **kwargs):
        if self.orden == Recurso.ORDER_DEFAULT:  # el valor por defecto
            self.orden = Recurso.ORDER_CSV

        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'csv'

        super(ArchivoCSV, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Archivos CSV"
        verbose_name = "Archivo CSV"


class ArchivoXLS(Archivo):
    '''
    Un archivo XLS
    '''

    def save(self, *args, **kwargs):
        if self.orden == Recurso.ORDER_DEFAULT:  # el valor por defecto
            self.orden = Recurso.ORDER_XLS

        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'xls'

        super(ArchivoXLS, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Archivos XLS (Excel)"
        verbose_name = "Archivo XLS (Excel)"


class ArchivoKML(Archivo):
    '''
    Un archivo KML
    '''
    # algunos CSV se suben para ser importados o procesador por el sistema
    # esta es una marca para esos casos
    procesado = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.orden == self.ORDER_DEFAULT:  # el valor por defecto
            self.orden = self.ORDER_KML

        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'kml'

        super(ArchivoKML, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Archivos KML (mapa)"
        verbose_name = "Archivo KML (mapa)"


class ArchivoKMZ(Archivo):
    '''
    Un archivo KMZ
    '''

    def save(self, *args, **kwargs):
        if self.orden == self.ORDER_DEFAULT:  # el valor por defecto
            self.orden = self.ORDER_KMZ

        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'kmz'

        super(ArchivoKMZ, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Archivos KMZ (mapa)"
        verbose_name = "Archivo KMZ (mapa)"


class ArchivoDOC(Archivo):
    '''
    Un archivo DOC
    '''

    def save(self, *args, **kwargs):
        if self.orden == self.ORDER_DEFAULT:  # el valor por defecto
            self.orden = self.ORDER_DOC

        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'doc'

        super(ArchivoDOC, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Archivos DOC (Word)"
        verbose_name = "Archivo DOC (Word)"


class ArchivoODS(Archivo):
    '''
    Una planilla de cálculo de LibreOffice
    '''

    def save(self, *args, **kwargs):
        if self.orden == self.ORDER_DEFAULT:  # el valor por defecto
            self.orden = self.ORDER_ODS

        if self.icon is None:  # si no define ícono uso la extension
            self.icon = 'ods'

        super(ArchivoODS, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Archivos ODS (LibreOffice)"
        verbose_name = "Archivo ODS (LibreOffice)"


class ArchivoODT(Archivo):
    '''
    Un archivo de texto de LibreOffice
    '''

    def save(self, *args, **kwargs):
        if self.orden == self.ORDER_DEFAULT:  # el valor por defecto
            self.orden = self.ORDER_ODT

        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'odt'

        super(ArchivoODT, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Archivos ODT (LibreOffice)"
        verbose_name = "Archivo ODT (LibreOffice)"


class ArchivoTXT(Archivo):
    '''
    Un archivo TXT
    '''

    def save(self, *args, **kwargs):
        if self.orden == self.ORDER_DEFAULT:  # el valor por defecto
            self.orden = self.ORDER_TXT

        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'txt'

        super(ArchivoTXT, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Archivos TXT"
        verbose_name = "Archivo TXT"


class ArchivoSHP(Archivo):
    '''
    Un archivo SHP (empaquetado en un XZIP o un RAR)
    '''

    def save(self, *args, **kwargs):
        if self.orden == self.ORDER_DEFAULT:  # el valor por defecto
            self.orden = self.ORDER_SHP

        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'shp'

        super(ArchivoSHP, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Archivos SHP (mapa)"
        verbose_name = "Archivo SHP (mapa)"


class ArchivoJSON(Archivo):
    '''
    Un archivo JSON
    '''

    def save(self, *args, **kwargs):
        if self.orden == self.ORDER_DEFAULT:  # el valor por defecto
            self.orden = self.ORDER_JSON

        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'json'

        super(ArchivoJSON, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Archivos JSON"
        verbose_name = "Archivo JSON"


class Webservice(Recurso):
    '''
    JSON por ahora solamente
    '''
    url_externa = models.URLField(null=True, blank=True)
    #TODO agregar métodos de validación y parámetros posibles

    def save(self, *args, **kwargs):
        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'json'

        if self.orden == self.ORDER_DEFAULT:  # el valor por defecto
            self.orden = self.ORDER_WEBSERVICE
        super(Webservice, self).save(*args, **kwargs)


class BaseDeDatos(Recurso):
    '''
    Sin definir por ahora
    '''
    def save(self, *args, **kwargs):
        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'database'

        if self.orden == self.ORDER_DEFAULT:  # el valor por defecto
            self.orden = self.ORDER_DB

        super(BaseDeDatos, self).save(*args, **kwargs)


class ReferenciaLink(Recurso):
    '''
    URL externa con algo que se puede considerar dato u información
    '''
    url_externa = models.URLField(null=True, blank=True)  # solo en recursos externos. Si hay un CDN para archivos internos la URL publica se construira basado en file

    def save(self, *args, **kwargs):
        if not self.icon:  # si no define ícono uso la extension
            self.icon = 'web'

        if self.orden == self.ORDER_DEFAULT:  # el valor por defecto
            self.orden = self.ORDER_LINK

        super(Recurso, self).save(*args, **kwargs)


class GoogleSheetResource(Recurso):
    '''
    Una planilla de Google Drive es un tipo de recurso especial.
    Permite tener automáticamente un Excel, un CSV, un ODS y un PDF con los links de exportacion
    Además permite visualizaciones que se pueden embeber
    '''

    # todos los documentos tienen un ID especial (de 44 caracteres aprox)
    file_id = models.CharField(max_length=100)

    # definir si se muestran o no los links a la versión dinamica a exportar de este recurso
    mostrar_xlsx = models.BooleanField(default=True)
    mostrar_pdf = models.BooleanField(default=False)
    mostrar_ods = models.BooleanField(default=True)

    BASE_URL = "https://docs.google.com/spreadsheets/d/{key}"
    BASE_EXPORT_URL = "https://docs.google.com/feeds/download/spreadsheets/Export?key={key}&exportFormat={format}"
    # el csv requiere el GID de la pestaña a exportar. CSV es siempre solo una página de la planilla
    BASE_EXPORT_CSV_URL = "https://docs.google.com/feeds/download/spreadsheets/Export?key={key}&exportFormat={format}&gid={gid}"

    # campos internos con los recursos conectactos a este que se generan automáticamente
    # NO SE CARGA DESDE EL ADMIN, SE CONSTRUYE AUTOMATICAMENTE al grabar este sheet
    resource_xlsx = models.ForeignKey(ArchivoXLS, on_delete=models.CASCADE, null=True, blank=True)
    resource_ods = models.ForeignKey(ArchivoODS, on_delete=models.CASCADE, null=True, blank=True)
    resource_pdf = models.ForeignKey(ArchivoPDF, on_delete=models.CASCADE, null=True, blank=True)

    _resource_xlsx = None
    _resource_ods = None
    _resource_pdf = None

    def __init__(self, *args, **kwargs):
        super(GoogleSheetResource, self).__init__(*args, **kwargs)
        self._resource_xlsx = self.resource_xlsx
        self._resource_ods = self.resource_ods
        self._resource_pdf = self.resource_pdf

    @property
    def url_externa(self):
        ''' por compatibilidad con otros recursos. Link al drive público '''
        return self.BASE_URL.format(key=self.file_id)

    @property
    def dinamico(self):
        ''' por compatibilidad archivos dinamicos (construidos al momento del pedido) '''
        return True

    def __str__(self):
        return "GOOGLE SHEET {}".format(self.titulo)

    def get_xlsx_url(self):
        ''' Obtener el link para exportar a XLSX '''
        return self.BASE_EXPORT_URL.format(key=self.file_id, format="xlsx")

    def get_pdf_url(self):
        ''' Obtener el link para exportar a PDF '''
        return self.BASE_EXPORT_URL.format(key=self.file_id, format="pdf")

    def get_ods_url(self):
        ''' Obtener el link para exportar a ODS '''
        return self.BASE_EXPORT_URL.format(key=self.file_id, format="ods")

    def save(self, *args, **kwargs):
        ''' definir el icono si no se hizo y asegurarse de que se construyan automáticamente
            los derivados XLSX, CSV, ODS y PDF '''
        if not self.icon:  # el None no
            self.icon = 'googlesheet'

        if self.orden == Recurso.ORDER_DEFAULT:  # el valor por defecto
            self.orden = Recurso.ORDER_GOOGLE

        # no se puede cambiar el tipo de este
        tipoG, created = TipoDato.objects.get_or_create(tipo=TipoDato.GOOGLE_DOC, subtipo="Google Sheet")
        self.tipo = tipoG

        # revisar los recursos automáticos relacionados. Son siempre de tipo archivo web
        tipo, created = TipoDato.objects.get_or_create(tipo=TipoDato.ARCHIVO, subtipo="Web")

        # ---------------------------------
        # EXCEL ---------------------------
        # ---------------------------------
        titulo = 'Excel {}'.format(self.titulo)
        # modificar el que estaba y no generar duplicados
        recurso = None
        if self._resource_xlsx is None:
            if self.mostrar_xlsx:
                recurso = ArchivoXLS(titulo=titulo, version_dato=self.version_dato, tipo=tipo,
                                                licencia=self.licencia, archivo_local=None, dinamico=True,
                                                url_externa=self.get_xlsx_url(), icon='xls')
                recurso.save()
        else:
            if self.mostrar_xlsx == False:
                self._resource_xlsx.delete()
            else:
                recurso = self._resource_xlsx

                recurso.titulo = 'Excel {}'.format(self.titulo)
                recurso.version_dato=self.version_dato
                recurso.tipo=tipo
                recurso.licencia=self.licencia
                recurso.archivo_local=None
                recurso.dinamico=True
                recurso.url_externa=self.get_xlsx_url()
                recurso.icon='xls'

                recurso.save()

        self.resource_xlsx = recurso

        # ---------------------------------
        # Open Office ---------------------
        # ---------------------------------
        titulo = 'OpenOffice {}'.format(self.titulo)
        recurso = None
        if self._resource_ods is None:
            if self.mostrar_ods:
                recurso = ArchivoODS(titulo=titulo, version_dato=self.version_dato, tipo=tipo,
                                                licencia=self.licencia, archivo_local=None, dinamico=True,
                                                url_externa=self.get_ods_url(), icon='ods')
                recurso.save()

        else:
            if self.mostrar_ods == False:
                self._resource_ods.delete()
            else:
                recurso = self._resource_ods
                recurso.titulo=titulo
                recurso.version_dato=self.version_dato
                recurso.tipo=tipo
                recurso.licencia=self.licencia
                recurso.archivo_local=None
                recurso.dinamico=True
                recurso.url_externa=self.get_ods_url()
                recurso.icon='ods'

                recurso.save()

        self.resource_ods = recurso
        # ---------------------------------
        # PDF -----------------------------
        # ---------------------------------
        titulo = 'PDF {}'.format(self.titulo)
        recurso = None
        if self._resource_pdf is None:
            if self.mostrar_pdf:
                recurso = ArchivoPDF(titulo=titulo, version_dato=self.version_dato, tipo=tipo,
                                        licencia=self.licencia, archivo_local=None, dinamico=True,
                                        url_externa=self.get_pdf_url(), icon='pdf')
                recurso.save()
        else:
            if self.mostrar_pdf == False:
                self._resource_pdf.delete()
            else:
                recurso = self._resource_pdf
                recurso.titulo=titulo
                recurso.version_dato=self.version_dato
                recurso.tipo=tipo
                recurso.licencia=self.licencia
                recurso.archivo_local=None
                recurso.dinamico=True
                recurso.url_externa=self.get_pdf_url()
                recurso.icon='pdf'

                recurso.save()

        self.resource_pdf = recurso

        logger.info('Saving Google')
        super(GoogleSheetResource, self).save(*args, **kwargs)


class CSVExportadoGoogleSheet(models.Model):
    ''' una planilla de cálculo de google puede exportar a CSV de a una página solamente
        estas paginas pueden ser mas de una por plailla de drive, por eso el módulo aparte.
        Tienen titulo propio porque no son necesariamente el mismo que la planilla que los origino'''

    google_sheet = models.ForeignKey(GoogleSheetResource, on_delete=models.CASCADE)
    gid = models.CharField(max_length=100, help_text="Identificador único de la página", default="0")
    titulo = models.CharField(max_length=90)

    # campo interno con el recursos conectacto a este generado automáticamente
    resource_csv = models.ForeignKey(ArchivoCSV, on_delete=models.CASCADE, null=True, blank=True)

    _resource_csv = None

    def __init__(self, *args, **kwargs):
        super(CSVExportadoGoogleSheet, self).__init__(*args, **kwargs)
        ''' anotar el estado para cuando lo grabe registrar el canbio '''
        self._resource_csv = self.resource_csv

    def __str__(self):
        return "CSV SHEET {} (gid:{})".format(self.titulo, self.gid)

    def get_csv_url(self, page=1):
        ''' Obtener el link para exportar a CSV una pestaña interna '''
        return self.google_sheet.BASE_EXPORT_CSV_URL.format(key=self.google_sheet.file_id, format="csv", gid=self.gid)

    def save(self, *args, **kwargs):

        tipo, created = TipoDato.objects.get_or_create(tipo=TipoDato.ARCHIVO, subtipo="Web")
        titulo = 'CSV {}'.format(self.titulo)
        recurso = None
        if self._resource_csv is None:
            recurso = ArchivoCSV(titulo=titulo, version_dato=self.google_sheet.version_dato, tipo=tipo,
                                    licencia=self.google_sheet.licencia, archivo_local=None, dinamico=True,
                                    url_externa=self.get_csv_url(), icon='csv')

        else:
            recurso = self._resource_csv

            recurso.titulo=titulo
            recurso.version_dato=self.google_sheet.version_dato
            recurso.tipo=tipo
            recurso.licencia=self.google_sheet.licencia
            recurso.archivo_local=None
            recurso.dinamico=True
            recurso.url_externa=self.get_csv_url()
            recurso.icon='csv'

        recurso.save()
        self.resource_csv = recurso

        super(CSVExportadoGoogleSheet, self).save(*args, **kwargs)
