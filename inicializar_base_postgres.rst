Inicializar base Postgres 
=========================

Qhapax usa postgres para optimizar el uso con GeoDjango. 
Para esto se requiere `Postgis <https://postgis.net/install/>`_.

.. code:: bash 
    $ sudo su - postgres
    $ psql

y ejecutar la siguientes directivas sql

.. code:: sql

    CREATE EXTENSION postgis;
    CREATE DATABASE qhapax;
    // elegir otra clave!
    CREATE USER qhapax_dba WITH PASSWORD 'password';
    ALTER ROLE qhapax_dba SET client_encoding TO 'utf8';
    ALTER ROLE qhapax_dba SET default_transaction_isolation TO 'read committed';
    ALTER ROLE qhapax_dba SET timezone TO 'UTC';
    GRANT ALL PRIVILEGES ON DATABASE qhapax TO qhapax_dba;

