# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-02-24 14:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('propuestaciudadana', '0011_auto_20170224_0917'),
    ]

    operations = [
        migrations.AlterField(
            model_name='propuesta',
            name='estado',
            field=models.PositiveIntegerField(choices=[(10, 'Nuevo'), (20, 'Valido'), (30, 'En estudio'), (40, 'Descartado'), (45, 'Derivado y cerrado'), (50, 'Aceptado'), (55, 'Aceptada como idea destacada o innovadora'), (60, 'Hecho')], default=10),
        ),
    ]
