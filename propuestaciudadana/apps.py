from django.apps import AppConfig


class PropuestaciudadanaConfig(AppConfig):
    name = 'propuestaciudadana'
