# -*- coding: utf-8 -*-
'''
Los ciudadanos pueden enviar propuestas para que sean
canalizadas por alguna oficina del gobierno
Estas propuestas están ordenadas por temas.
Si la propuesta esta bien considerada podrá ser asignada
a alguna repartición en particular.
Se debería poder votar online (con usuarios registrados)
y seguir vía web la evolución de esta idea.
'''
from django.db import models
from django.utils.text import slugify
from plandemetas.models import Lineamiento
from gobiernos.models import TemaGeneral


class Propuesta(models.Model):
    """
    Cada una de las propuestas de los ciudadanos y su estado actual
    """
    # opcionalmente puede estar relacionada a los lineamientos
    lineamiento = models.ForeignKey(Lineamiento, on_delete=models.SET_NULL, null=True, blank=True)
    # a modo más general se puede elegir un tema
    tema_general = models.ForeignKey(TemaGeneral, on_delete=models.SET_NULL, null=True)  # vincula además la idea con el gobierno
    titulo = models.CharField(max_length=130, verbose_name='Titulo de la propuesta')
    slug = models.SlugField(max_length=140, null=True, blank=True)
    descripcion = models.TextField(verbose_name='Descricin de la propuesta')
    email = models.EmailField(help_text="Obligatorio")  # obligatorio, para validar que es real
    nombre_persona = models.CharField(max_length=180, verbose_name='Tu nombre completo')
    NUEVO = 10
    VALIDO = 20  # vio el email y le dio al link de control
    EN_ESTUDIO = 30  # puede ser válido
    DESCARTADO = 40  # se descarto la idea (enviar motivos al ciudadano)
    RESPONDIDO = 45  # se le respondio al solicitante agradeciéndole
    DERIVADO_Y_CERRADO = 45  # se le paso a algun funcionario como reclamo, audiencia o para estudio
    ACEPTADO = 50  # Se va a realizar
    ACEPTADO_INNOVADOR = 55  # Es una idea destacada o innovadora
    HECHO = 60  # Se llevo a cabo

    estados = ((NUEVO, 'Nuevo'), (VALIDO, 'Valido'), (EN_ESTUDIO, 'En estudio'),
        (DESCARTADO, 'Descartado'), (RESPONDIDO, 'Respondido al solicitante'),
        (DERIVADO_Y_CERRADO, 'Derivado y cerrado'),
        (ACEPTADO, 'Aceptado'),
        (ACEPTADO_INNOVADOR, 'Aceptada como idea destacada o innovadora'),
        (HECHO, 'Hecho'))

    estado = models.PositiveIntegerField(choices=estados, default=NUEVO)
    respuesta = models.TextField(null=True, blank=True)
    observaciones_internas = models.TextField(null=True, blank=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        # si es un nuevo objeto DEBO mandarle un email de notificación (DESPUES de grabarlo)
        nuevo = self.pk is None
        self.slug = slugify(self.titulo)
        super(Propuesta, self).save(*args, **kwargs)

        if nuevo:
            from django.template.loader import render_to_string
            html_mensaje = render_to_string('core/mails/nueva_idea.html', {'usuario': self.nombre_persona,
                                                                                        'titulo': self.titulo,
                                                                                        'pk': self.pk,
                                                                                        'descripcion': self.descripcion})
            from core.lib.comm import QhapaxMail
            qm = QhapaxMail()
            subject = "Nueva idea para la Municipalidad de Córdoba [IDEA-{}]".format(self.pk)
            qm.send_mail(subject, html_mensaje, to=self.email)

    def __str__(self):
        return self.titulo

    class Meta:
        ordering = ['titulo']
