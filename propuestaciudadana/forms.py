from django.forms import ModelForm
from .models import Propuesta
from captcha.fields import ReCaptchaField


class SolicitudPropuestaForm(ModelForm):
    captcha = ReCaptchaField(attrs={'lang': 'es'})

    class Meta:
        model = Propuesta
        exclude = ['canal_de_envio', 'slug', 'estado', 'respuesta',
                    'creado', 'ultima_modificacion', 'funcion',
                    'tema', 'tipo_org', 'observaciones_internas']
