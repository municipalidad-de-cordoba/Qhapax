from django.shortcuts import render
from django.conf import settings
from django.views.decorators.cache import cache_page
from . import forms
from plandemetas.models import PlanDeMetas
from django.http import JsonResponse
from core.models import TipoOrganizacion
from propuestaciudadana.models import Propuesta
from iniciativas.models import Iniciativa
import logging

from django.core.paginator import Paginator, EmptyPage

logger = logging.getLogger(__name__)


@cache_page(60 * 60)  # 1 h
def solicitud(request):
    '''
    mostrar el formulario de acceso a la audiencia
    '''
    form = forms.SolicitudPropuestaForm
    ideas_totales = Propuesta.objects.count()

    iniciativas = Iniciativa.objects.filter(publicado=True)
    
    number_of_item = 3
    paginatorr = Paginator(iniciativas, number_of_item)

    first_page = paginatorr.page(1).object_list
    page_range = paginatorr.page_range
  
    context = {
        'titulo': 'Formulario de solicitud de propuesta',
        'form': form(initial={'tipo_id': settings.TIPOID_PK_PRINCIPAL,
        'tipo_org_general': TipoOrganizacion.PERSONA_FISICA}),
        'ideas': ideas_totales,
        'iniciativas': list(paginatorr.page(1)),
        'paginatorr': paginatorr,
        'first_page': first_page,
        'page_range': page_range
    }

    if request.method == "POST":
        context['form'] = form(request.POST)
        if context['form'].is_valid():
            context['form'].save()
            return JsonResponse({'ok': True})
        else:
            return JsonResponse({'ok': False, 'errors': context['form'].errors.as_json()})


    planes_de_metas = PlanDeMetas.objects.filter(publicado=True).order_by('-id')
    plan_de_metas = None if len(planes_de_metas) == 0 else planes_de_metas[0]
    context['plan_de_metas'] = plan_de_metas

    url = "website/{}/solicitud-propuesta.html".format(request.website.template)

    return render(request, url, context)


def iniciativas(request):
    iniciativas = Iniciativa.objects.filter(publicado=True)
    number_of_item = 3
    paginatorr = Paginator(iniciativas, number_of_item)
    first_page = paginatorr.page(1).object_list
    page_range = paginatorr.page_range

    page_n = request.GET.get('page', None)

    results = list()
    try:
        results = list(paginatorr.page(page_n).object_list.values('titulo', 'descripcion', 'fecha', 'url', 'imagen'))
    except EmptyPage:
        results = list()

    return JsonResponse({"results":results})

