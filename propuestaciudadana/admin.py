from django.contrib import admin
from .models import *
from simple_history.admin import SimpleHistoryAdmin
from import_export.admin import ImportExportModelAdmin
from import_export import resources, fields
from import_export.fields import Field


class PropuestaAdminResource(resources.ModelResource):
    '''
    recurso a exportarse cuando se descarga
    '''
    estado = Field()

    class Meta:
        model = Propuesta
        fields = ('id', 'lineamiento__titulo', 'tema_general__nombre', 'titulo',
                  'slug', 'descripcion', 'email', 'nombre_persona', 'estado'
                 )

    def dehydrate_estado(self, obj):
        return obj.get_estado_display()

class PropuestaAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    list_display = ('id', 'lineamiento', 'tema_general', 'titulo', 'estado',
                    'nombre_persona', 'creado', 'ultima_modificacion')
    search_fields = ['lineamiento__titulo', 'tema_general__nombre', 'titulo', 'nombre_persona']
    list_filter = ['lineamiento', 'tema_general', 'estado']


admin.site.register(Propuesta, PropuestaAdmin)
