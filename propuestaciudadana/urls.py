from django.conf.urls import url
from . import views

urlpatterns = [
    #url(r'^$', views.index, name='index'),
    url(r'^pagination/', views.iniciativas, name='propuestaciudadana.iniciativas'),
    url(r'^nueva-idea/$', views.solicitud, name='propuestaciudadana.solicitud'),
]
