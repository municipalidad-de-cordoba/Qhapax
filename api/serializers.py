"""
Serializacion de objetos
http://www.django-rest-framework.org/tutorial/1-serialization/
"""
from PIL import ImageFile
import bleach
from html import unescape
from datetime import date

from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from django.urls import reverse
from versatileimagefield.serializers import VersatileImageFieldSerializer

from core.models import Persona, Moneda
from funcionarios.models import CargoCategoria, Cargo, Funcion, DireccionCargo
from accesoinfopublica.models import SolicitudAccesoInfo

from declaracionesjuradas.models import DeclaracionJurada, Bien, Deuda, TipoBien
from plandemetas.models import (PlanDeMetas, Lineamiento, Componente,
                                Objetivo, Meta, IndicadorMeta)
from eventospublicos.models import (
    LugarActividad,
    OrganizadorEvento,
    ParticipanteActividad,
    EventoGeneral,
    AgrupadorActividad,
    TipoActividad,
    DisciplinaActividad,
    AudienciaActividad,
    PrecioActividad,
    RepeticionActividad,
    ActividadPublica,
    FotoLugarActividad,
    GrupoPrecioActividad,
    CategoriaLugarActividad,
    FotoParticipanteActividad,
    CircuitoLugarActividad)
from geo.models import Pais
from softwaremunicipal.models import AppMovil
from portaldemapas.models import PortalDeMapas, CategoriaPortalMapas, CapaMapaPortal
from portaldedatos.models import (
    DatoPortal,
    Recurso,
    CategoriaPortalDatos,
    Portal,
    TagDatos,
    Fuente,
    VersionDato,
    TipoDato,
    LicenciaDato)
from alertas.models import Alerta, TipoAlerta, LugarAlerta, InicioFinAlerta, FotoAlerta

import re
# Creamos las expresiones regulares para limpiar los campos html
regex_tag = re.compile(r'<[^>]+>')
regex_spaces = re.compile(r'[\t\r\v]')


ImageFile.LOAD_TRUNCATED_IMAGES = True


class PersonaSimpleSerializer(CachedSerializerMixin):

    class Meta:
        model = Persona
        fields = ('nombre', 'apellido', 'nombrepublico')


class PersonaSerializer(CachedSerializerMixin):
    franjaetaria = serializers.SerializerMethodField()
    edad = serializers.SerializerMethodField()
    url = serializers.SerializerMethodField()
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail", 'thumbnail__125x125')])
    uniqueid = serializers.SerializerMethodField()

    def get_franjaetaria(self, obj):
        return obj.franja_etaria

    def get_uniqueid(self, obj):
        return '{} {}'.format(obj.tipo_id, obj.unique_id)

    def get_edad(self, obj):
        if obj.fecha_nacimiento:
            today = date.today()
            years = (
                (today.year - obj.fecha_nacimiento.year - 1) + (
                    1 if (
                        today.month,
                        today.day) >= (
                        obj.fecha_nacimiento.month,
                        obj.fecha_nacimiento.day) else 0))
            return years

    def get_url(self, obj):
        return obj.get_absolute_url()

    class Meta:
        model = Persona
        fields = ('nombre', 'apellido', 'nombrepublico', 'franjaetaria',
                  'genero', 'edad', 'url', 'foto', 'uniqueid')


class PaisSerializer(CachedSerializerMixin):

    class Meta:
        model = Pais
        fields = ('nombre',)


class CargoCategoriaSerializer(CachedSerializerMixin):
    class Meta:
        model = CargoCategoria
        fields = ('id', 'nombre', 'requiere_declaracion_jurada',
                  'nombre_corto', 'orden')


class CargoSerializer(CachedSerializerMixin):
    categoria = CargoCategoriaSerializer(read_only=True)
    depende_de = serializers.PrimaryKeyRelatedField(read_only=True)
    superioresids = serializers.SerializerMethodField()

    def get_superioresids(self, obj):
        ret = []
        superiores = obj.superiores()
        if superiores:
            for cargo in superiores:
                ret.append(cargo.id)
        return ret

    class Meta:
        model = Cargo
        fields = ('id', 'categoria', 'nombre', 'depende_de',
                  'electivo', 'superioresids', 'oficina')


class FuncionSerializer(CachedSerializerMixin):
    funcionario = PersonaSerializer(read_only=True)
    cargo = CargoSerializer(read_only=True)

    class Meta:
        model = Funcion
        fields = ('id', 'funcionario', 'cargo', 'fecha_inicio',
                  'fecha_fin', 'decreto_nro', 'decreto_pdf')


class MonedaSerializer(CachedSerializerMixin):
    '''
    tipo de bienes cargados en las DDJJ
    '''
    class Meta:
        model = Moneda
        fields = ('nombre', 'simbolo', )


class TipoBienesDDJJSerializer(CachedSerializerMixin):
    '''
    tipo de bienes cargados en las DDJJ
    '''
    class Meta:
        model = TipoBien
        fields = ('nombre', )


class BienesDDJJSerializer(CachedSerializerMixin):
    '''
    bienes cargados en las DDJJ
    '''
    tipo = TipoBienesDDJJSerializer()
    moneda = MonedaSerializer()

    class Meta:
        model = Bien
        fields = ('tipo', 'en_el_pais', 'es_de_conyuge', 'moneda', 'valor')


class DeudasDDJJSerializer(CachedSerializerMixin):
    '''
    deudas cargados en las DDJJ
    '''
    class Meta:
        model = Deuda
        fields = ('en_el_pais', 'con_entidad_financiera',
                  'es_de_conyuge', 'moneda', 'valor')


class DeclaracionJuradaSerializer(CachedSerializerMixin):
    persona = PersonaSerializer(read_only=True)
    bienes = BienesDDJJSerializer(many=True)
    deudas = DeudasDDJJSerializer(many=True)

    class Meta:
        model = DeclaracionJurada
        fields = ('persona', 'anio', 'archivo', 'bienes', 'deudas')


class PlanDeMetasSerializer(CachedSerializerMixin):

    class Meta:
        model = PlanDeMetas
        fields = ('id', 'titulo',)


class LineamientoSerializer(CachedSerializerMixin):

    class Meta:
        model = Lineamiento
        fields = ('id', 'titulo', 'plan_de_metas')


class ComponenteSerializer(CachedSerializerMixin):

    class Meta:
        model = Componente
        fields = ('id', 'titulo', 'lineamiento')


class ObjetivoSerializer(CachedSerializerMixin):

    class Meta:
        model = Objetivo
        fields = ('id', 'titulo', 'componente')


class MetaSerializer(CachedSerializerMixin):

    class Meta:
        model = Meta
        fields = ('id', 'titulo', 'estado_de_avance', 'linea_base', 'objetivo')


class IndicadorMetaSerializer(CachedSerializerMixin):

    class Meta:
        model = IndicadorMeta
        fields = ('id', 'indicador', 'valor_indicador', 'meta')


class OrganizadorEventoSerializer(CachedSerializerMixin):
    imagen = VersatileImageFieldSerializer([("original", 'url'),
                                            ("thumbnail_32x32",
                                             'thumbnail__32x32'),
                                            ("thumbnail", 'thumbnail__125x125')])

    class Meta:
        model = OrganizadorEvento
        fields = ('id', 'nombre', 'descripcion', 'imagen')


class EventosPublicosSerializer(CachedSerializerMixin):
    imagen = VersatileImageFieldSerializer([("original", 'url'),
                                            ("thumbnail_32x32",
                                             'thumbnail__32x32'),
                                            ("thumbnail", 'thumbnail__125x125')])
    flyer = VersatileImageFieldSerializer([("original", 'url'),
                                           ("thumbnail_400x400",
                                            'thumbnail__400x400'),
                                           ("thumbnail", 'thumbnail__125x125')])
    organizador = OrganizadorEventoSerializer(read_only=True)

    class Meta:
        model = EventoGeneral
        fields = ('id', 'nombre', 'imagen', 'flyer',
                  'descripcion', 'organizador',
                  'header_actividades', 'footer_actividades')


class AgrupadorActividadSerializer(CachedSerializerMixin):
    imagen = VersatileImageFieldSerializer([("original", 'url'),
                                            ("thumbnail_32x32",
                                             'thumbnail__32x32'),
                                            ("thumbnail", 'thumbnail__125x125')])
    flyer = VersatileImageFieldSerializer([("original", 'url'),
                                           ("thumbnail_400x400",
                                            'thumbnail__400x400'),
                                           ("thumbnail", 'thumbnail__125x125')])

    class Meta:
        model = AgrupadorActividad
        fields = ('id', 'nombre', 'descripcion', 'imagen',
                  'flyer', 'header_actividades', 'footer_actividades')


class FotoLugarActividadSerializer(CachedSerializerMixin):
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail", 'thumbnail__125x125'),
                                          ("thumbnail_350x350", 'thumbnail__350x350')])

    class Meta:
        model = FotoLugarActividad
        fields = ('id', 'orden', 'foto')


class CategoriaLugarActividadSerializer(CachedSerializerMixin):

    class Meta:
        model = CategoriaLugarActividad
        fields = ('id', 'nombre')


class CircuitoLugarActividadSerializer(CachedSerializerMixin):

    class Meta:
        model = CircuitoLugarActividad
        fields = ('id', 'nombre')


class LugarActividadSerializer(CachedSerializerMixin):
    fotos = FotoLugarActividadSerializer(many=True, read_only=True)
    categorias = CategoriaLugarActividadSerializer(many=True, read_only=True)
    circuitos = CircuitoLugarActividadSerializer(many=True, read_only=True)
    latitud = serializers.SerializerMethodField()
    longitud = serializers.SerializerMethodField()

    def get_latitud(self, obj):
        # se crea para mantener compatibilidad hacia atras. Antes habías campos
        # float en lugar de un PointField
        return obj.ubicacion.coords[1] if obj.ubicacion is not None else ''

    def get_longitud(self, obj):
        # se crea para mantener compatibilidad hacia atras. Antes habías campos
        # float en lugar de un PointField
        return obj.ubicacion.coords[0] if obj.ubicacion is not None else ''

    class Meta:
        model = LugarActividad
        fields = ('id', 'nombre', 'direccion',
                  'email', 'telefono',
                  'descripcion', 'latitud',
                  'longitud', 'mas_info_url',
                  'estrellas', 'relevancia',
                  'fotos', 'categorias', 'circuitos', 'zonas')


class FotoParticipanteActividadSerializer(CachedSerializerMixin):
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail", 'thumbnail__125x125')])

    class Meta:
        model = FotoParticipanteActividad
        fields = ('id', 'orden', 'foto')


class ParticipanteActividadSerializer(CachedSerializerMixin):
    fotos = FotoParticipanteActividadSerializer(many=True)

    class Meta:
        model = ParticipanteActividad
        fields = ('id', 'grupo_o_persona', 'fotos', 'descripcion',
                  'telefono', 'email')


class TipoActividadSerializer(CachedSerializerMixin):

    class Meta:
        model = TipoActividad
        fields = ('id', 'nombre')


class DisciplinaActividadSerializer(CachedSerializerMixin):

    class Meta:
        model = DisciplinaActividad
        fields = ('id', 'nombre')


class AudienciaActividadSerializer(CachedSerializerMixin):

    class Meta:
        model = AudienciaActividad
        fields = ('id', 'nombre')


class AccesoInfoPublicaSerializer(CachedSerializerMixin):
    estado = serializers.SerializerMethodField()
    backend_url = serializers.SerializerMethodField()

    class Meta:
        model = SolicitudAccesoInfo
        fields = ('id', 'titulo', 'nro_expediente', 'creado',
                  'tema_general', 'estado', 'canal_de_envio', "backend_url")
        depth = 1

    def get_estado(self, obj):
        return obj.get_estado_display()

    def get_backend_url(self, obj):
        return reverse("backend.accesoinfopublica.edit", args=[obj.id])


class GrupoPrecioActividadSerializer(CachedSerializerMixin):

    class Meta:
        model = GrupoPrecioActividad
        fields = ['nombre']


class PrecioActividadSerializer(CachedSerializerMixin):
    grupo = GrupoPrecioActividadSerializer(read_only=True)
    moneda = MonedaSerializer()

    class Meta:
        model = PrecioActividad
        fields = ['grupo', 'moneda', 'valor', 'detalles']


class RepeticionesActividadPublicaSerializer(CachedSerializerMixin):

    class Meta:
        model = RepeticionActividad
        fields = ('inicia', 'termina')


class ActividadPublicaSerializer(CachedSerializerMixin):
    agrupador = AgrupadorActividadSerializer(read_only=True)
    lugar = LugarActividadSerializer(read_only=True)
    imagen = VersatileImageFieldSerializer([("original", 'url'),
                                            ("thumbnail_32x32",
                                             'thumbnail__32x32'),
                                            ("thumbnail", 'thumbnail__125x125')])
    flyer = VersatileImageFieldSerializer([("original", 'url'),
                                           ("thumbnail_400x400",
                                            'thumbnail__400x400'),
                                           ("thumbnail", 'thumbnail__125x125')])
    organizador = OrganizadorEventoSerializer(read_only=True)
    tipos = TipoActividadSerializer(many=True)
    disciplinas = DisciplinaActividadSerializer(many=True)
    participantes = ParticipanteActividadSerializer(many=True)
    audiencias = AudienciaActividadSerializer(many=True)
    precios = PrecioActividadSerializer(many=True)
    repeticiones = RepeticionesActividadPublicaSerializer(many=True)
    # zonas = serializers.SerializerMethodField()
    descripcion_txt = serializers.SerializerMethodField()

    # Falta pasar texto en crudo y limpio mediante un parametro. Leer ticket
    def get_descripcion_txt(self, obj):
        str_limpio = unescape(bleach.clean(obj.descripcion, strip=True))
        regex_t = regex_tag.sub('', str_limpio)
        regex_s = regex_spaces.sub('', regex_t)
        return regex_s.replace('\n', ' ')

    # def get_zonas(self, obj):
    # return ZonasSerializer(data=core_serializers.serialize('json',
    # obj.zonas))

    class Meta:
        model = ActividadPublica
        fields = (
            'id',
            'agrupador',
            'titulo',
            'imagen',
            'flyer',
            'descripcion',
            'descripcion_txt',
            'lugar',
            'inicia',
            'termina',
            'organizador',
            'tipos',
            'audiencias',
            'disciplinas',
            'participantes',
            'mas_info_url',
            'precios',
            'repeticiones')


class RepeticionActividadPublicaSerializer(CachedSerializerMixin):
    actividad = ActividadPublicaSerializer(read_only=True)

    class Meta:
        model = RepeticionActividad
        fields = ('inicia', 'termina', 'actividad')


class AplicacionesMovilesSerializer(CachedSerializerMixin):
    imagen = VersatileImageFieldSerializer([("original", 'url'),
                                            ("thumbnail_32x32",
                                             'thumbnail__32x32'),
                                            ("thumbnail", 'thumbnail__125x125')])
    logo = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail", 'thumbnail__125x125')])

    class Meta:
        model = AppMovil
        fields = (
            'id',
            'titulo',
            'descripcion',
            'logo',
            'imagen',
            'mas_info_url',
            'android_market',
            'ios_market',
            'windows_market',
            'web_version')


class PortalDeMapasSerializer(CachedSerializerMixin):
    imagen = VersatileImageFieldSerializer([("original", 'url'),
                                            ("thumbnail_32x32",
                                             'thumbnail__32x32'),
                                            ("thumbnail", 'thumbnail__125x125')])

    class Meta:
        model = PortalDeMapas
        fields = ('id', 'titulo', 'descripcion', 'imagen')


class CategoriaPortalMapasSerializer(CachedSerializerMixin):
    portal = PortalDeMapasSerializer(read_only=True)

    class Meta:
        model = CategoriaPortalMapas
        fields = ('id', 'nombre', 'descripcion', 'portal')


class RecursoURLSerializer(CachedSerializerMixin):
    url = serializers.ReadOnlyField()

    class Meta:
        model = Recurso
        fields = ('id', 'titulo', 'url')


class CapaMapaPortalSerializer(CachedSerializerMixin):
    categoria = CategoriaPortalMapasSerializer(read_only=True)
    recurso = RecursoURLSerializer(read_only=True)

    class Meta:
        model = CapaMapaPortal
        fields = ('id', 'recurso', 'titulo', 'descripcion', 'categoria')


class LugarAlertaSerializer(CachedSerializerMixin):

    class Meta:
        model = LugarAlerta
        fields = ('id', 'nombre', 'descripcion', 'latitud', 'longitud')


class TipoAlertaSerializer(CachedSerializerMixin):

    class Meta:
        model = TipoAlerta
        fields = ('id', 'nombre')


class InicioFinAlertaSimpleSerializer(CachedSerializerMixin):

    class Meta:
        model = InicioFinAlerta
        fields = ('inicia', 'termina')


class FotoAlertaSerializer(CachedSerializerMixin):
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail", 'thumbnail__125x125')])

    class Meta:
        model = FotoAlerta
        fields = ('id', 'descripcion', 'foto')


class AlertaSerializer(CachedSerializerMixin):
    tipo = TipoAlertaSerializer(read_only=True)
    lugar = LugarAlertaSerializer(read_only=True)
    gravedad = serializers.SerializerMethodField()
    momentos = InicioFinAlertaSimpleSerializer(many=True)
    fotos = FotoAlertaSerializer(many=True)

    def get_gravedad(self, obj):
        return obj.get_gravedad_display()

    class Meta:
        model = Alerta
        fields = ('id', 'titulo', 'descripcion', 'tipo', 'lugar',
                  'mas_info_url', 'gravedad', 'momentos', 'fotos')


class AlertaSinMomentosSerializer(CachedSerializerMixin):
    tipo = TipoAlertaSerializer(read_only=True)
    lugar = LugarAlertaSerializer(read_only=True)
    gravedad = serializers.SerializerMethodField()
    fotos = FotoAlertaSerializer(many=True)

    def get_gravedad(self, obj):
        return obj.get_gravedad_display()

    class Meta:
        model = Alerta
        fields = ('id', 'titulo', 'descripcion', 'tipo',
                  'lugar', 'mas_info_url', 'gravedad', 'fotos')


class InicioFinAlertaSerializer(CachedSerializerMixin):
    alerta = AlertaSinMomentosSerializer(read_only=True)

    class Meta:
        model = InicioFinAlerta
        fields = ('inicia', 'termina', 'alerta')


class PortalSerializer(CachedSerializerMixin):
    ''' portales de datos '''
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        return obj.get_absolute_url()

    class Meta:
        model = Portal
        fields = ('id', 'titulo', 'url')


class CategoriaPortalDatosSerializer(CachedSerializerMixin):
    portal = PortalSerializer(read_only=True)
    url = serializers.SerializerMethodField()
    depende_de = serializers.SerializerMethodField()

    def get_url(self, obj):
        return obj.get_absolute_url()

    def get_depende_de(self, obj):
        return obj.depende_de.nombre if obj.depende_de is not None else None

    class Meta:
        model = CategoriaPortalDatos
        fields = ('portal', 'nombre', 'depende_de', 'url')


class TagDatosSerializer(CachedSerializerMixin):
    ''' tags de los datos del portal '''

    class Meta:
        model = TagDatos
        fields = ('nombre', )


class FuenteSerializer(CachedSerializerMixin):
    ''' fuentes de los datos del portal '''
    es_fuente_propia = serializers.SerializerMethodField()

    def get_es_fuente_propia(self, obj):
        # saber si la fuente es una oficina local o no
        es_fuente_propia = obj.oficina_local is not None
        return es_fuente_propia

    class Meta:
        model = Fuente
        fields = ('nombre', 'es_fuente_propia')


class TipoDatoSerializer(CachedSerializerMixin):
    ''' tipos de datos de los datos del portal '''
    tipo = serializers.SerializerMethodField()

    def get_tipo(self, obj):
        return obj.get_tipo_display()

    class Meta:
        model = TipoDato
        fields = ('tipo', 'subtipo')


class LicenciaDatoSerializer(CachedSerializerMixin):
    ''' Licencia de los datos '''

    class Meta:
        model = LicenciaDato
        fields = ('titulo', 'descripcion', 'es_licencia_libre')


class RecursoSerializer(CachedSerializerMixin):
    ''' Recursos de los datos '''
    tipo = TipoDatoSerializer(read_only=True)
    url = serializers.SerializerMethodField()
    licencia = LicenciaDatoSerializer(read_only=True)

    def get_url(self, obj):
        return obj.url

    class Meta:
        model = Recurso
        fields = ('titulo', 'tipo', 'url', 'icon', 'licencia')


class VersionDatoSerializer(CachedSerializerMixin):
    ''' Versiones de los datos '''
    fuentes = FuenteSerializer(many=True)
    recursos = RecursoSerializer(many=True)

    class Meta:
        model = VersionDato
        fields = ('fecha', 'titulo', 'fuentes', 'recursos')


class DireccionCargoSerializer(CachedSerializerMixin):
    """
    Direcciones físicas de las oficinas(cargos)
    """
    class Meta:
        model = DireccionCargo
        fields = ('calle', 'numero', 'piso', 'localidad', 'provincia', 'pais',
                  'cp')


class DatoOficinasSerializer(CachedSerializerMixin):
    """
    Todas las oficinas que dependen de algún área
    """
    contacto = serializers.StringRelatedField(
        many=True, source='comunicacioncargo_set')
    direccion = serializers.SerializerMethodField()
    area = serializers.SerializerMethodField()
    superioresids = serializers.SerializerMethodField()

    def get_direccion(self, container):
        direcciones = DireccionCargo.objects.filter(objeto=container)
        serializers = DireccionCargoSerializer(instance=direcciones, many=True)
        return serializers.data

    def get_superioresids(self, obj):
        ret = []
        superiores = obj.superiores()
        if superiores:
            for cargo in superiores:
                ret.append(cargo.id)
        return ret

    def get_area(self, obj):
        return str(obj.area_gobierno.oficina)

    class Meta:
        model = Cargo
        fields = ('id', 'oficina', 'area', 'direccion',
                  'contacto', 'superioresids')


class DatoPortalSerializer(CachedSerializerMixin):
    ''' Datos del portal de Gobierno Abierto '''
    categoria = CategoriaPortalDatosSerializer(read_only=True)
    periodicidad = serializers.SerializerMethodField()
    tags = TagDatosSerializer(many=True)
    versiones = VersionDatoSerializer(many=True)
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        return obj.get_absolute_url()

    def get_periodicidad(self, obj):
        return obj.get_periodicidad_display()

    class Meta:
        model = DatoPortal
        fields = ('titulo', 'creado', 'periodicidad', 'url',
                  'versiones', 'categoria', 'tags')


# Registro los serializadores en la cache de DRF
cache_registry.register(PortalSerializer)
cache_registry.register(CategoriaPortalDatosSerializer)
cache_registry.register(TagDatosSerializer)
cache_registry.register(FuenteSerializer)
cache_registry.register(VersionDatoSerializer)
cache_registry.register(DireccionCargoSerializer)
cache_registry.register(DatoOficinasSerializer)
cache_registry.register(DatoPortalSerializer)
cache_registry.register(PersonaSerializer)
cache_registry.register(PaisSerializer)
cache_registry.register(CargoCategoriaSerializer)
cache_registry.register(CargoSerializer)
cache_registry.register(FuncionSerializer)
cache_registry.register(DeclaracionJuradaSerializer)
cache_registry.register(PlanDeMetasSerializer)
cache_registry.register(LineamientoSerializer)
cache_registry.register(ComponenteSerializer)
cache_registry.register(ObjetivoSerializer)
cache_registry.register(MetaSerializer)
cache_registry.register(IndicadorMetaSerializer)
cache_registry.register(EventosPublicosSerializer)
cache_registry.register(AgrupadorActividadSerializer)
cache_registry.register(FotoParticipanteActividadSerializer)
cache_registry.register(ParticipanteActividadSerializer)
cache_registry.register(ActividadPublicaSerializer)
cache_registry.register(OrganizadorEventoSerializer)
cache_registry.register(DisciplinaActividadSerializer)
cache_registry.register(AudienciaActividadSerializer)
cache_registry.register(AplicacionesMovilesSerializer)
cache_registry.register(PortalDeMapasSerializer)
cache_registry.register(CategoriaPortalMapasSerializer)
cache_registry.register(CapaMapaPortalSerializer)
cache_registry.register(LugarAlertaSerializer)
cache_registry.register(TipoAlertaSerializer)
cache_registry.register(AlertaSerializer)
cache_registry.register(InicioFinAlertaSerializer)
cache_registry.register(FotoLugarActividadSerializer)
cache_registry.register(LugarActividadSerializer)
cache_registry.register(CircuitoLugarActividadSerializer)
