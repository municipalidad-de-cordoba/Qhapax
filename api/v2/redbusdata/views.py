from rest_framework import authentication, status, viewsets
from rest_framework.permissions import (IsAuthenticatedOrReadOnly,
                                        DjangoModelPermissions)
from .serializers import (SaldoTarjetaRedBusSerializer,
                          ErrorLecturaSaldoRedBusSerializer, AppRedBusSerializer)
from api.pagination import DefaultPagination
from redbusdata.models import (TarjetaRedBus, SaldoTarjetaRedBus,
                               NombreTarjetaRedBus, ErrorLecturaSaldoRedBus,
                               AppRedBus)
from rest_framework.response import Response
# statuses http://www.django-rest-framework.org/api-guide/status-codes/
from django.views.decorators.csrf import csrf_exempt


class SaldoTarjetaRedBusViewSet(viewsets.ModelViewSet):
    """
    Ver o Notificar saldo para guardar dato estadísitco.

    POST require fields:
     - nombre_tarjeta.tarjeta.codigo: codigo de la tarjeta RedBus (numero)
     - nombre_tarjeta.uid: Identificador único de la app instalada. Debe tener el prefijo CT- (identifica a la app Cuanto Tengo)
     - momento_dato: Fecha que RedBus informa como la del dato tomado
     - saldo: $ de saldo informado por RedBus
    """
    authentication_classes = (authentication.TokenAuthentication,)
    serializer_class = SaldoTarjetaRedBusSerializer
    permission_classes = [DjangoModelPermissions]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = SaldoTarjetaRedBus.objects.all()
        return queryset

    @csrf_exempt
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @csrf_exempt
    def create(self, request, *args, **kwargs):
        data = request.data
        if 'nombre_tarjeta.tarjeta.codigo' not in data.keys():
            errors = {'error': 'No se incluye el código de la tarjeta'}
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)
        # codigo de la tarjeta redbus
        codigo = data['nombre_tarjeta.tarjeta.codigo']
        # identificador único creado en la app cuanto tengo
        if 'nombre_tarjeta.uid' not in data.keys():
            errors = {'error': 'No se incluye el UID'}
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)

        uid = data['nombre_tarjeta.uid']
        if not uid.startswith("CT-"):
            errors = {'error': 'Prefijo del UID no reconocido'}
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)

        # nombre que le da este usuario a esa tarjeta
        if 'nombre_tarjeta.nombre' not in data.keys():
            errors = {'error': 'No se incluye el nombre de la tarjeta'}
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)
        nombre = data['nombre_tarjeta.nombre']

        tarjeta, created = TarjetaRedBus.objects.get_or_create(codigo=codigo)
        nombre_tarjeta, created = NombreTarjetaRedBus.objects.get_or_create(
            uid=uid, tarjeta=tarjeta)
        nombre_tarjeta.nombre = nombre  # actualizar el nombre si fuera necesario
        nombre_tarjeta.save()

        saldo = SaldoTarjetaRedBus(nombre_tarjeta=nombre_tarjeta)
        saldo.momento_dato = data['momento_dato']
        saldo.saldo = data['saldo']
        saldo.save()

        # res = {'error': None, 'OK': True, 'id': saldo.id}
        return Response(status=status.HTTP_201_CREATED)


class ErrorLecturaSaldoRedBusViewSet(viewsets.ModelViewSet):
    """
    Ver o Notificar errores al leer el saldo de una tarjeta RedBus

    POST require fields:
     - nombre_tarjeta.tarjeta.codigo: codigo de la tarjeta RedBus (numero)
     - nombre_tarjeta.uid: Identificador único de la app instalada. Debe tener el prefijo CT- (identifica a la app Cuanto Tengo)
     - error_code: Codigo de error HTTP que devolvio Redus
     - error_details: Detalles del error
     - error_redbus_code: Codigo de error de RedBus
    """
    authentication_classes = (authentication.TokenAuthentication,)
    serializer_class = ErrorLecturaSaldoRedBusSerializer
    permission_classes = [DjangoModelPermissions]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = ErrorLecturaSaldoRedBus.objects.all()
        return queryset

    @csrf_exempt
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @csrf_exempt
    def create(self, request, *args, **kwargs):
        data = request.data
        if 'nombre_tarjeta.tarjeta.codigo' not in data.keys():
            errors = {'error': 'No se incluye el código de la tarjeta'}
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)
        # codigo de la tarjeta redbus
        codigo = data['nombre_tarjeta.tarjeta.codigo']
        # identificador único creado en la app cuanto tengo
        if 'nombre_tarjeta.uid' not in data.keys():
            errors = {'error': 'No se incluye el UID'}
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)

        uid = data['nombre_tarjeta.uid']
        if not uid.startswith("CT-"):
            errors = {'error': 'Prefijo del UID no reconocido'}
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)

        tarjeta, created = TarjetaRedBus.objects.get_or_create(codigo=codigo)
        nombre_tarjeta, created = NombreTarjetaRedBus.objects.get_or_create(
            uid=uid, tarjeta=tarjeta)

        if 'error_code' not in data.keys():
            errors = {'error': 'No se incluye el código de error HTTP'}
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)

        if 'error_details' not in data.keys():
            errors = {'error': 'No se incluye el detalle del error'}
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)

        error_code = data['error_code']
        error_details = data['error_details']
        error_redbus_code = data['error_redbus_code']

        error = ErrorLecturaSaldoRedBus(nombre_tarjeta=nombre_tarjeta)
        error.error_code = error_code
        error.error_details = error_details
        error.error_redbus_code = error_redbus_code
        error.save()

        # res = {'error': None, 'OK': True, 'id': error.id}
        return Response(status=status.HTTP_201_CREATED)


class AppRedBusViewSet(viewsets.ModelViewSet):
    """ solo lectura, las aplicaciones y versiones activas """
    # authentication_classes = NINGUNA, ES API ABIERTA
    serializer_class = AppRedBusSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        queryset = AppRedBus.objects.filter()  # (app__publicado=True)
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
