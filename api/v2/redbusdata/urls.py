from django.conf.urls import url, include
from rest_framework import routers
from .views import (SaldoTarjetaRedBusViewSet, ErrorLecturaSaldoRedBusViewSet,
                    AppRedBusViewSet)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'saldo-tarjeta-redbus',
    SaldoTarjetaRedBusViewSet,
    base_name='saldo-tarjeta-redbus')
router.register(
    r'error-saldo-redbus',
    ErrorLecturaSaldoRedBusViewSet,
    base_name='error-saldo-redbus')
router.register(r'app-redbus', AppRedBusViewSet, base_name='app-redbus')

urlpatterns = [
    url(r'^', include(router.urls)),
]
