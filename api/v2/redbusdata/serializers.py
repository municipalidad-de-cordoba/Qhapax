from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from redbusdata.models import (TarjetaRedBus, NombreTarjetaRedBus,
                               SaldoTarjetaRedBus, ErrorLecturaSaldoRedBus,
                               AppRedBus)
from softwaremunicipal.models import AppMovil


class TarjetaRedBusSerializer(CachedSerializerMixin):
    class Meta:
        model = TarjetaRedBus
        fields = ('codigo', 'id')


class NombreTarjetaRedBusSerializer(CachedSerializerMixin):
    tarjeta = TarjetaRedBusSerializer()

    class Meta:
        model = NombreTarjetaRedBus
        fields = ('uid', 'tarjeta', 'nombre', 'id')


class SaldoTarjetaRedBusSerializer(CachedSerializerMixin):
    nombre_tarjeta = NombreTarjetaRedBusSerializer()

    class Meta:
        model = SaldoTarjetaRedBus
        fields = (
            'nombre_tarjeta',
            'momento_consulta',
            'momento_dato',
            'saldo')


class ErrorLecturaSaldoRedBusSerializer(CachedSerializerMixin):
    nombre_tarjeta = NombreTarjetaRedBusSerializer()

    class Meta:
        model = ErrorLecturaSaldoRedBus
        fields = (
            'nombre_tarjeta',
            'momento_consulta',
            'error_redbus_code',
            'error_code',
            'error_details')


class AppMovilSerializer(CachedSerializerMixin):

    class Meta:
        model = AppMovil
        fields = (
            'id',
            'titulo',
            'android_market',
            'ios_market',
            'windows_market',
            'web_version')


class AppRedBusSerializer(CachedSerializerMixin):
    app = AppMovilSerializer(read_only=True)

    class Meta:
        model = AppRedBus
        fields = ('id', 'app', 'version_minima', 'txt_sino_hay_version_minima',
                  'version_recomendada', 'txt_sino_hay_version_recomendada',
                  'terminos_y_condiciones', 'politica_de_privacidad',
                  'about_app', 'about_data')


cache_registry.register(TarjetaRedBusSerializer)
cache_registry.register(SaldoTarjetaRedBusSerializer)
cache_registry.register(NombreTarjetaRedBusSerializer)
cache_registry.register(ErrorLecturaSaldoRedBusSerializer)
cache_registry.register(AppMovilSerializer)
cache_registry.register(AppRedBusSerializer)
