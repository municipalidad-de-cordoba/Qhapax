# -*- coding: utf-8 -*-
"""
Doc de API
"""
from rest_framework import viewsets
from rest_framework.permissions import (DjangoModelPermissionsOrAnonReadOnly,
                                        DjangoModelPermissions)
from rest_framework.filters import OrderingFilter
from funcionarios.models import CargoCategoria, Cargo, Funcion
from declaracionesjuradas.models import DeclaracionJurada
from plandemetas.models import (IndicadorMeta, Meta, PlanDeMetas, Lineamiento,
                                Componente, Objetivo)
from django.shortcuts import get_object_or_404
from accesoinfopublica.models import SolicitudAccesoInfo
from .serializers import (CargoCategoriaSerializer, FuncionSerializer,
                          DeclaracionJuradaSerializer, PlanDeMetasSerializer,
                          LineamientoSerializer, ComponenteSerializer,
                          ObjetivoSerializer, MetaSerializer,
                          IndicadorMetaSerializer, EventosPublicosSerializer,
                          AgrupadorActividadSerializer,
                          ParticipanteActividadSerializer,
                          ActividadPublicaSerializer,
                          RepeticionActividadPublicaSerializer,
                          OrganizadorEventoSerializer, TipoActividadSerializer,
                          CircuitoLugarActividadSerializer,
                          LugarActividadSerializer, AudienciaActividadSerializer,
                          DisciplinaActividadSerializer,
                          AccesoInfoPublicaSerializer,
                          AplicacionesMovilesSerializer, PortalDeMapasSerializer,
                          CapaMapaPortalSerializer, InicioFinAlertaSerializer,
                          AlertaSerializer, DatoOficinasSerializer,
                          CategoriaPortalDatosSerializer, DatoPortalSerializer,
                          VersionDatoSerializer)
from .pagination import DefaultPagination
from django.db.models import Q
from eventospublicos.models import (ActividadPublica, RepeticionActividad,
                                    CircuitoLugarActividad, LugarActividad,
                                    AudienciaActividad, DisciplinaActividad,
                                    TipoActividad, OrganizadorEvento,
                                    EventoGeneral, AgrupadorActividad,
                                    ParticipanteActividad)
from django.utils import timezone
from datetime import datetime, timedelta
from softwaremunicipal.models import AppMovil
from alertas.models import Alerta, InicioFinAlerta
from portaldedatos.models import DatoPortal, VersionDato, CategoriaPortalDatos
from portaldemapas.models import PortalDeMapas, CapaMapaPortal
from core.utils import normalizar
import logging
logger = logging.getLogger(__name__)


class CargoCategoriaViewSet(viewsets.ModelViewSet):
    """
    Categorias o rangos de los funcionarios políticos.
    """
    serializer_class = CargoCategoriaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CargoCategoria.objects.filter(publicado=True)
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class FuncionViewSet(viewsets.ModelViewSet):
    """
    Una función es la cruza que surge de la combinación de un cargo y el
    funcionario que lo ejerce actualmente.

    Puede filtrarse por:
    categoria_id
    cargo_id
    funcionario_id
    q (texto en nombre_publico del funcionario, nombre, descripcion y oficina
    del cargo, nombre y descripcion de la categoria)
    """
    serializer_class = FuncionSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Funcion.objects.filter(
            activo=True)  # , cargo__visible=True)

        # filtro por categoría
        categoria_id = self.request.query_params.get('categoria_id', None)
        if categoria_id is not None:
            queryset = queryset.filter(cargo__categoria__id=categoria_id)

        # filtro por cargo
        cargo_id = self.request.query_params.get('cargo_id', None)
        if cargo_id is not None:
            queryset = queryset.filter(cargo__id=cargo_id)

        # filtro por funcionario
        funcionario_id = self.request.query_params.get('funcionario_id', None)
        if funcionario_id is not None:
            queryset = queryset.filter(funcionario__id=funcionario_id)

        # filtro por texto
        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(funcionario__nombre_publico__icontains=q) |
                Q(cargo__nombre__icontains=q) |
                Q(cargo__descripcion__icontains=q) |
                Q(cargo__oficina__icontains=q) |
                Q(cargo__categoria__nombre__icontains=q) |
                Q(cargo__categoria__descripcion__icontains=q))

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class DeclaracionJuradaViewSet(viewsets.ModelViewSet):
    """
    Funcionarios que tienen DDJJ presentada y el año de publicación.

    Puede filtrarse por:
    persona_id
    anio (año)
    """
    serializer_class = DeclaracionJuradaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = DeclaracionJurada.objects.filter(publicada=True)

        # filtro por persona
        persona_id = self.request.query_params.get('persona_id', None)
        if persona_id is not None:
            queryset = queryset.filter(persona__id=persona_id)

        # filtro por año
        anio = self.request.query_params.get('anio', None)
        if anio is not None:
            queryset = queryset.filter(anio=anio)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class PlanDeMetasViewSet(viewsets.ModelViewSet):
    """
    Planes de metas del gobierno que están activos.
    """
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = PlanDeMetasSerializer
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = PlanDeMetas.objects.filter(publicado=True)
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class LineamientoViewSet(viewsets.ModelViewSet):
    """
    Lineamientos de los planes de metas.

    Puede filtrarse por:
    plan_de_metas_id
    """
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = LineamientoSerializer
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Lineamiento.objects.filter(publicado=True)

        # filtro por plan de metas
        plan_de_metas_id = self.request.query_params.get(
            'plan_de_metas_id', None)
        if plan_de_metas_id is not None:
            queryset = queryset.filter(plan_de_metas__id=plan_de_metas_id)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ComponenteViewSet(viewsets.ModelViewSet):
    """
    Componentes de cada lineamiento de un plan de metas.

    Puede filtrarse por:
    plan_de_metas_id
    lineamiento_id
    """
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = ComponenteSerializer
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Componente.objects.filter(publicado=True)

        # filtro por plan de metas
        plan_de_metas_id = self.request.query_params.get(
            'plan_de_metas_id', None)
        if plan_de_metas_id is not None:
            queryset = queryset.filter(
                lineamiento__plan_de_metas__id=plan_de_metas_id)

        # filtro por lineamiento
        lineamiento_id = self.request.query_params.get('lineamiento_id', None)
        if lineamiento_id is not None:
            queryset = queryset.filter(lineamiento__id=lineamiento_id)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ObjetivoViewSet(viewsets.ModelViewSet):
    """
    Objetivos incluidos en un Componente.

    Puede filtrarse por:
    plan_de_metas_id
    lineamiento_id
    componente_id
    """
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = ObjetivoSerializer
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Objetivo.objects.filter(publicado=True)

        # filtro por plan de metas
        plan_de_metas_id = self.request.query_params.get(
            'plan_de_metas_id', None)
        if plan_de_metas_id is not None:
            queryset = queryset.filter(
                componente__lineamiento__plan_de_metas__id=plan_de_metas_id)

        # filtro por lineamiento
        lineamiento_id = self.request.query_params.get('lineamiento_id', None)
        if lineamiento_id is not None:
            queryset = queryset.filter(
                componente__lineamiento__id=lineamiento_id)

        # filtro por componente
        componente_id = self.request.query_params.get('componente_id', None)
        if componente_id is not None:
            queryset = queryset.filter(componente__id=componente_id)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class MetaViewSet(viewsets.ModelViewSet):
    """
    Metas incluidas en cada componente.

    Puede filtrarse por:
    plan_de_metas_id
    lineamiento_id
    componente_id
    objetivo_id
    """
    serializer_class = MetaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Meta.objects.filter(publicado=True)

        # filtro por plan de metas
        plan_de_metas_id = self.request.query_params.get(
            'plan_de_metas_id', None)
        if plan_de_metas_id is not None:
            queryset = queryset.filter(
                objetivo__componente__lineamiento__plan_de_metas__id=plan_de_metas_id)

        # filtro por lineamiento
        lineamiento_id = self.request.query_params.get('lineamiento_id', None)
        if lineamiento_id is not None:
            queryset = queryset.filter(
                objetivo__componente__lineamiento__id=lineamiento_id)

        # filtro por componente
        componente_id = self.request.query_params.get('componente_id', None)
        if componente_id is not None:
            queryset = queryset.filter(objetivo__componente__id=componente_id)

        # filtro por objetivo
        objetivo_id = self.request.query_params.get('objetivo_id', None)
        if objetivo_id is not None:
            queryset = queryset.filter(objetivo__id=objetivo_id)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class IndicadorMetaViewSet(viewsets.ModelViewSet):
    """
    Cada uno de los indicadores a usar en las metas.

    Puede filtrarse por:
    plan_de_metas_id
    lineamiento_id
    componente_id
    objetivo_id
    meta_id
    """
    serializer_class = IndicadorMetaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = IndicadorMeta.objects.filter(publicado=True)

        # filtro por plan de metas
        plan_de_metas_id = self.request.query_params.get(
            'plan_de_metas_id', None)
        if plan_de_metas_id is not None:
            queryset = queryset.filter(
                meta__objetivo__componente__lineamiento__plan_de_metas__id=plan_de_metas_id)

        # filtro por lineamiento
        lineamiento_id = self.request.query_params.get('lineamiento_id', None)
        if lineamiento_id is not None:
            queryset = queryset.filter(
                meta__objetivo__componente__lineamiento__id=lineamiento_id)

        # filtro por componente
        componente_id = self.request.query_params.get('componente_id', None)
        if componente_id is not None:
            queryset = queryset.filter(
                meta__objetivo__componente__id=componente_id)

        # filtro por objetivo
        objetivo_id = self.request.query_params.get('objetivo_id', None)
        if objetivo_id is not None:
            queryset = queryset.filter(meta__objetivo__id=objetivo_id)

        # filtro por meta
        meta_id = self.request.query_params.get('meta_id', None)
        if meta_id is not None:
            queryset = queryset.filter(meta__id=meta_id)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class EventosPublicosViewSet(viewsets.ModelViewSet):
    """
    Cada uno de los Eventos públicos del Gobierno.
    Es un agrupador de actividades, pueden ser culturales pero
    tambien deportivas, turísticas, etc.

    Puede filtrarse por:
    audiencia_id (valores separados por comas)
    """
    serializer_class = EventosPublicosSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        now = timezone.now()
        queryset = EventoGeneral.objects.filter(
            publicado_desde__lte=now,
            publicado=True,

        )

        # filtro por tipo de audiencia (solo se sabe en las actividades)
        audiencia_id = self.request.query_params.get('audiencia_id', None)
        if audiencia_id is not None:
            audiencias_id = audiencia_id.split(",")
            one_hour_ago = now - timedelta(hours=1)
            queryset = queryset.filter(
                Q(
                    agrupadoractividad__actividadpublica__audiencias__in=audiencias_id), Q(
                    agrupadoractividad__actividadpublica__inicia__gte=one_hour_ago) | Q(
                    agrupadoractividad__actividadpublica__termina__gte=now), )
            # queda duplicado una vez por cada actividad
            queryset = queryset.distinct()

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class AgrupadorActividadViewSet(viewsets.ModelViewSet):
    """
    Cada uno de los agrupadores de actividades de Eventos públicos del Gobierno.

    Puede filtrarse por:
    evento_id
    """
    serializer_class = AgrupadorActividadSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = AgrupadorActividad.objects.filter(
            evento__publicado_desde__lte=timezone.now(),
            evento__publicado=True,

        )

        # filtro por evento
        evento_id = self.request.query_params.get('evento_id', None)
        if evento_id is not None:
            queryset = queryset.filter(evento__id=evento_id)

        queryset = queryset.order_by('orden')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ParticipanteActividadViewSet(viewsets.ModelViewSet):
    """
    Cada participante en un evento público.

    Se puede filtrar por actividad con el parámetro 'actividad_id'.
    Pueden ser varios ids, separados por comas. Por ejemplo:
    "actividad_id=1,2,3"

    Se puede filtrar por agrupador con el parámetro 'agrupador_id'.
    Pueden ser varios, separados por comas. Por ejemplo:
    "agrupador_id=2,3"

    Se puede filtrar por eventos con el parámetro 'evento_id'.
    Pueden ser varios, separados por comas. Por ejemplo:
    "evento_id=2,3"

    Se puede filtrar por categorias de los participantes con el parámetro 'categoria_id'.
    Pueden ser varios, separados por comas. Por ejemplo:
    "categoria_id=2,3"

    Se puede filtrar por circuitos de los participantes con el parámetro 'circuito_id'.
    Pueden ser varios, separados por comas. Por ejemplo:
    "circuito_id=2,3"

    Se puede filtrar por texto en la descripción con el parámetro 'q'.
    "q=Banda"

    """
    serializer_class = ParticipanteActividadSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # filtra por actividad, por eventos o agrupador
        queryset = ParticipanteActividad.objects.all().order_by('id')

        actividad_id = self.request.query_params.get('actividad_id', None)
        if actividad_id is not None:
            actividad_id = actividad_id.split(",")
            queryset = queryset.filter(actividadpublica__id__in=actividad_id)

        agrupador_id = self.request.query_params.get('agrupador_id', None)
        if agrupador_id is not None:
            agrupador_id = agrupador_id.split(",")
            queryset = queryset.filter(
                actividadpublica__agrupador__id__in=agrupador_id)

        evento_id = self.request.query_params.get('evento_id', None)
        if evento_id is not None:
            evento_id = evento_id.split(",")
            queryset = queryset.filter(
                actividadpublica__agrupador__evento__id__in=evento_id)

        categoria_id = self.request.query_params.get('categoria_id', None)
        if categoria_id is not None:
            categoria_id = categoria_id.split(",")
            queryset = queryset.filter(categorias__id__in=categoria_id)

        circuito_id = self.request.query_params.get('circuito_id', None)
        if circuito_id is not None:
            circuito_id = circuito_id.split(",")
            queryset = queryset.filter(circuitos__id__in=circuito_id)

        q = self.request.query_params.get('q', None)
        if q is not None:
            # FIXME: acá se busca el texto que quiere el usuario, por más que en la bd tenga acento lo mismo lo encuentra.
            # Si Django trae algo predefinido, usarlo acá
            copia_queryset = queryset.values('id', 'descripcion')
            list_ids = []
            texto_norm = normalizar(q)
            for participante in copia_queryset:
                if texto_norm in normalizar(participante['descripcion']):
                    list_ids.append(participante['id'])
            queryset = queryset.filter(id__in=list_ids)

        return queryset.distinct()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ActividadPublicaViewSet(viewsets.ModelViewSet):
    """
    Cada actividad pública.
    Las que todavía no empezaron (o lo hicieron máximo hace una hora)
    Mostrar tambien las que no terminaron.

    Puede filtrarse por:
    evento_id
    agrupador_id
    lugar_id
    tipo_id (opcionalmente valores separados por comas)
    disciplina_id (opcionalmente valores separados por comas)
    participante_id (opcionalmente valores separados por comas)
    audiencia_id (valores separados por comas)
    q (texto en titulo y descripcion, nombre del lugar, nombre y descripcion del agrupador)
    inicia_GTE & inicia_LTE: fecha de inicio del evento.
      Ejemplos:
        &inicia_GTE=02-04-2017-04-03-00 ==> Fecha de inicio MAYOR o igual que (GTE) 2/4/2017 04:03:00
        &inicia_LTE=02-04-2017-04-03-00 ==> Fecha de inicio MENOR o igual que (LTE) 2/4/2017 04:03:00

    termina_GTE & termina_LTE: fecha de inicio del evento.
      Ejemplos:
        &termina_GTE=02-04-2017-04-03-00 ==> Fecha de finalizacion MAYOR o igual que (GTE) 2/4/2017 04:03:00
        &termina_LTE=02-04-2017-04-03-00 ==> Fecha de finalizacion MENOR o igual que (LTE) 2/4/2017 04:03:00

    Pueden filtrarse actividades por zonas, categorías(de zonas) o grupos(de categorias de zonas)
    indicando los correspondientes id's. Pueden ser varios id's separados por coma:
    Ejemplos:
        ids_zonas ==> retorna las actividades que se realizan en la zona indicada por los id's
        ids_zonas_categorias ==> retorna las actividades correspondientes a las categorias de las zonas indicadas
        ids_grupos_cat ==> retorna las actividades correspondientes a los grupos de categorias de zonas indicadas
    (Para ver el api de zonas, ingresar en https://gobiernoabierto.cordoba.gob.ar/api/v2/zonas/).

    Puede ordenarse por:
        fecha de inicio: &ordering=inicio o DESC &ordering=-inicio
        Titulo: &ordering=titulo
        ID: &ordering=id o DESC &ordering=-id

    pueden aplicarse mas de un orden: &ordering=-titulo,inicia
    """
    serializer_class = ActividadPublicaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    filter_backends = (OrderingFilter,)
    ordering_fields = ('inicia', 'termina', 'titulo', 'id')

    def get_queryset(self):

        now = timezone.now()
        one_hour_ago = now - timedelta(hours=1)
        queryset = ActividadPublica.objects.filter(
            Q(inicia__gte=one_hour_ago) | Q(termina__gte=now),
            Q(agrupador__evento__publicado_desde__lte=now),
            Q(agrupador__evento__publicado=True),
            Q(publicado_desde__lte=now),
            Q(publicado=True),

        )
        # filtro evento general id
        evento_id = self.request.query_params.get('evento_id', None)
        if evento_id is not None:
            queryset = queryset.filter(agrupador__evento__id=evento_id)

        # filtro por agrupador
        agrupador_id = self.request.query_params.get('agrupador_id', None)
        if agrupador_id is not None:
            queryset = queryset.filter(agrupador__id=agrupador_id)

        # filtro por lugar
        lugar_id = self.request.query_params.get('lugar_id', None)
        if lugar_id is not None:
            queryset = queryset.filter(lugar__id=lugar_id)

        # filtro por tipos
        tipo_id = self.request.query_params.get('tipo_id', None)
        if tipo_id is not None:
            tipos_id = tipo_id.split(",")
            queryset = queryset.filter(tipos__in=tipos_id)

        # filtro por tipo de audiencia
        audiencia_id = self.request.query_params.get('audiencia_id', None)
        if audiencia_id is not None:
            audiencias_id = audiencia_id.split(",")
            queryset = queryset.filter(audiencias__in=audiencias_id)

        # filtro por tipo de audiencia
        participante_id = self.request.query_params.get(
            'participante_id', None)
        if participante_id is not None:
            participante_id = participante_id.split(",")
            queryset = queryset.filter(participantes__in=participante_id)

        # filtro por tipo de disciplina
        disciplina_id = self.request.query_params.get('disciplina_id', None)
        if disciplina_id is not None:
            disciplinas_id = disciplina_id.split(",")
            queryset = queryset.filter(disciplinas__in=disciplinas_id)

        # TODO: refactorear el parsing de fechas usando datetime's strptime()
        inicia_GTE = self.request.query_params.get('inicia_GTE', None)
        if inicia_GTE is not None:
            partes = inicia_GTE.split('-')
            fecha = datetime(
                int(
                    partes[2]), int(
                    partes[1]), int(
                    partes[0]), int(
                    partes[3]), int(
                        partes[4]), int(
                            partes[5]))
            fecha = timezone.make_aware(fecha)
            queryset = queryset.filter(inicia__gte=fecha)

        inicia_LTE = self.request.query_params.get('inicia_LTE', None)
        if inicia_LTE is not None:
            partes = inicia_LTE.split('-')
            fecha = datetime(
                int(
                    partes[2]), int(
                    partes[1]), int(
                    partes[0]), int(
                    partes[3]), int(
                        partes[4]), int(
                            partes[5]))
            fecha = timezone.make_aware(fecha)
            queryset = queryset.filter(inicia__lte=fecha)

        termina_GTE = self.request.query_params.get('termina_GTE', None)
        if termina_GTE is not None:
            partes = termina_GTE.split('-')
            fecha = datetime(
                int(
                    partes[2]), int(
                    partes[1]), int(
                    partes[0]), int(
                    partes[3]), int(
                        partes[4]), int(
                            partes[5]))
            fecha = timezone.make_aware(fecha)
            queryset = queryset.filter(termina__gte=fecha)

        termina_LTE = self.request.query_params.get('termina_LTE', None)
        if termina_LTE is not None:
            partes = termina_LTE.split('-')
            fecha = datetime(
                int(
                    partes[2]), int(
                    partes[1]), int(
                    partes[0]), int(
                    partes[3]), int(
                        partes[4]), int(
                            partes[5]))
            fecha = timezone.make_aware(fecha)
            queryset = queryset.filter(termina__lte=fecha)

        # filtro por texto
        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(descripcion__icontains=q) |
                Q(titulo__icontains=q) |
                Q(lugar__nombre__icontains=q) |
                Q(agrupador__nombre__icontains=q) |
                Q(agrupador__descripcion__icontains=q) |
                Q(agrupador__evento__nombre__icontains=q)
            )

        # filtro por zonas
        ids_zonas = self.request.query_params.get('ids_zonas', None)
        if ids_zonas is not None:
            ids_zonas = ids_zonas.split(',')
            queryset = queryset.zonas_coincidentes(ids_zonas=ids_zonas)

        # filtro por categorias de zonas
        ids_zonas_categorias = self.request.query_params.get(
            'ids_zonas_categorias', None)
        if ids_zonas_categorias is not None:
            ids_zonas_categorias = ids_zonas_categorias.split(',')
            queryset = queryset.categorias_zonas(
                ids_categorias=ids_zonas_categorias)

        # filtro por ids de agrupador de categorias de zonas
        ids_grupos_cat = self.request.query_params.get('ids_grupos_cat', None)
        if ids_grupos_cat is not None:
            ids_grupos_cat = ids_grupos_cat.split(',')
            queryset = queryset.agrupador_categorias_zonas(
                ids_grupos=ids_grupos_cat)

        queryset = queryset.order_by('orden', 'inicia')
        # logger.info("QUERY API ACTIVIDADES PUBLICAS: {}".format(queryset.query))

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class RepeticionActividadPublicaViewSet(viewsets.ModelViewSet):
    """
    Cada una de las repeticiones de cada actividad pública
    Las que todavía no empezaron (o lo hicieron máximo hace una hora)
    Mostrar tambien las que no terminaron.

    Puede filtrarse por:
    evento_id
    agrupador_id
    lugar_id
    tipo_id (valores separados por comas)
    disciplina_id (valores separados por comas)
    audiencia_id (valores separados por comas)
    q (texto en titulo y descripcion, nombre del lugar, nombre y descripcion del agrupador)
    """
    serializer_class = RepeticionActividadPublicaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        now = timezone.now()
        queryset = RepeticionActividad.objects.filter(
            Q(
                actividad__publicado=True), Q(
                actividad__agrupador__evento__publicado_desde__lte=now), Q(
                actividad__agrupador__evento__publicado=True), Q(
                    actividad__publicado_desde__lte=now), Q(
                        actividad__publicado=True))

        one_hour_ago = now - timedelta(hours=1)
        queryset = queryset.filter(
            Q(inicia__gte=one_hour_ago) | Q(termina__gte=now)
        )

        # filtro evento general id
        evento_id = self.request.query_params.get('evento_id', None)
        if evento_id is not None:
            queryset = queryset.filter(
                actividad__agrupador__evento__id=evento_id)

        # filtro por agrupador
        agrupador_id = self.request.query_params.get('agrupador_id', None)
        if agrupador_id is not None:
            queryset = queryset.filter(actividad__agrupador__id=agrupador_id)

        # filtro por lugar
        lugar_id = self.request.query_params.get('lugar_id', None)
        if lugar_id is not None:
            queryset = queryset.filter(actividad__lugar__id=lugar_id)

        # filtro por tipos
        tipo_id = self.request.query_params.get('tipo_id', None)
        if tipo_id is not None:
            tipos_id = tipo_id.split(",")
            queryset = queryset.filter(actividad__tipos__in=tipos_id)

        # filtro por tipo de audiencia
        audiencia_id = self.request.query_params.get('audiencia_id', None)
        if audiencia_id is not None:
            audiencias_id = audiencia_id.split(",")
            queryset = queryset.filter(actividad__audiencias__in=audiencias_id)

        # filtro por tipo de disciplina
        disciplina_id = self.request.query_params.get('disciplina_id', None)
        if disciplina_id is not None:
            queryset = queryset.filter(
                actividad__disciplinas__in=disciplina_id)

        # filtro por texto
        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(actividad__descripcion__icontains=q) |
                Q(actividad__titulo__icontains=q) |
                Q(actividad__lugar__nombre__icontains=q) |
                Q(actividad__agrupador__nombre__icontains=q) |
                Q(actividad__agrupador__descripcion__icontains=q))

        queryset.order_by('inicia')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class OrganizadorEventoViewSet(viewsets.ModelViewSet):
    """
    Cada organizador de un evento público.
    """
    serializer_class = OrganizadorEventoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = OrganizadorEvento.objects.all()
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class TipoActividadViewSet(viewsets.ModelViewSet):
    """
    Los tipos de actividades que pueden describir a una actividad.

    Puede filtrarse por:
    audiencia_id (valores separados por comas)
    evento_id - Id del evento del que se desean obtener los tipos
    """
    serializer_class = TipoActividadSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = TipoActividad.objects.all()

        # filtro por tipo de audiencia (solo se sabe en las actividades)
        audiencia_id = self.request.query_params.get('audiencia_id', None)

        if audiencia_id is not None:
            audiencias_id = audiencia_id.split(",")
            now = timezone.now()
            one_hour_ago = now - timedelta(hours=1)
            queryset = queryset.filter(Q(actividadpublica__audiencias__in=audiencias_id), Q(
                actividadpublica__inicia__gte=one_hour_ago) | Q(actividadpublica__termina__gte=now), )
            # queda duplicado una vez por cada actividad
            queryset = queryset.distinct()

        evento_id = self.request.query_params.get('evento_id', None)
        if evento_id is not None:
            queryset = queryset.filter(
                actividadpublica__agrupador__evento__id=evento_id)

            queryset = queryset.distinct()

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CircuitoActividadViewSet(viewsets.ModelViewSet):
    """
    Circuitos temáticos en los que circunscribimos a los lugares.
    Parámetros:
    id = Id del circuito, pueden ser varios separados por comas
    nombre = Nombre o parte del nombre del circuito.
    """
    serializer_class = CircuitoLugarActividadSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CircuitoLugarActividad.objects.all()

        # filtro por texto
        ids = self.request.query_params.get('id', None)
        if ids is not None:
            circuitos_id = ids.split(',')
            queryset = queryset.filter(id__in=circuitos_id)

        nombre = self.request.query_params.get('nombre', None)
        if nombre is not None:
            queryset = queryset.filter(nombre__icontains=nombre)

        return queryset


class LugarActividadViewSet(viewsets.ModelViewSet):
    """
    Los lugares donde se realizan las actividades.
    Parámetros:
    categorias_id -- Id de la categoria/s (pueden ser varias separadas por coma) de lugares a filtrar
    q -- filtro para buscar lugares por nombre o descripción
    audiencia_id -- filtro para listar lugares donde allá actividades
        (futuras, a realizarse) para esta audiencia.
    circuito_id -- id del circuito(pueden ser varios separados por comas) al que pertenece el lugar.
    estrellas -- 1 a 5 cantidad de estrellas (cero si no aplica)
    orden -- puede tomar el valor "nombre". Si se aplica, los lugares se ordenan alfabeticamente. Si no se usa se ordena por relevancia como es predeterminado

    Versión en GeoJSON: https://gobiernoabierto.cordoba.gob.ar/api/v2/eventos-publicos/lugar-actividad-geo/
    """
    serializer_class = LugarActividadSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = LugarActividad.objects.filter(publicado=True)

        # filtro por texto
        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(descripcion__icontains=q) |
                Q(nombre__icontains=q))

        # filtro por tipo de audiencia (solo se sabe en las actividades)
        audiencia_id = self.request.query_params.get('audiencia_id', None)
        if audiencia_id is not None:
            audiencias_id = audiencia_id.split(",")
            now = timezone.now()
            one_hour_ago = now - timedelta(hours=1)
            queryset = queryset.filter(Q(actividadpublica__audiencias__in=audiencias_id), Q(
                actividadpublica__inicia__gte=one_hour_ago) | Q(actividadpublica__termina__gte=now), )
            # queda duplicado una vez por cada actividad
            queryset = queryset.distinct()

        # filtro por tipo de audiencia
        categorias_id = self.request.query_params.get('categorias_id', None)
        if categorias_id is not None:
            categorias_id = categorias_id.split(",")
            queryset = queryset.filter(categorias__in=categorias_id)

        # estrellas
        estrellas = self.request.query_params.get('estrellas', None)
        if estrellas is not None:
            queryset = queryset.filter(estrellas=int(estrellas))

        # fitro por circuitos
        circuito_id = self.request.query_params.get('circuito_id', None)
        if circuito_id is not None:
            circuito_id = circuito_id.split(",")
            queryset = queryset.filter(
                circuitos__id__in=circuito_id).distinct()

        orden = self.request.query_params.get('orden', None)
        if orden == 'nombre':
            queryset = queryset.order_by('nombre')
        else:
            queryset = queryset.order_by('relevancia')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class AudienciaActividadViewSet(viewsets.ModelViewSet):
    """
    Los tipos de actividades que pueden describir a una actividad
    """
    serializer_class = AudienciaActividadSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = AudienciaActividad.objects.all()

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class DisciplinaActividadViewSet(viewsets.ModelViewSet):
    """
    Los tipos de actividades que pueden describir a una actividad.

    Puede filtrarse por:
    audiencia_id (valores separados por comas)
    """
    serializer_class = DisciplinaActividadSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = DisciplinaActividad.objects.all()

        # filtro por tipo de audiencia (solo se sabe en las actividades)
        audiencia_id = self.request.query_params.get('audiencia_id', None)
        if audiencia_id is not None:
            audiencias_id = audiencia_id.split(",")
            now = timezone.now()
            one_hour_ago = now - timedelta(hours=1)
            queryset = queryset.filter(Q(actividadpublica__audiencias__in=audiencias_id), Q(
                actividadpublica__inicia__gte=one_hour_ago) | Q(actividadpublica__termina__gte=now), )
            # queda duplicado una vez por cada actividad
            queryset = queryset.distinct()

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class AccesoInfoPublicaViewSet(viewsets.ModelViewSet):
    """
    Los tipos de actividades que pueden describir a una actividad
    """
    serializer_class = AccesoInfoPublicaSerializer
    permission_classes = [DjangoModelPermissions]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = SolicitudAccesoInfo.objects.all()
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class AplicacionesMovilesViewSet(viewsets.ModelViewSet):
    """
    Aplicaciones móviles
    """
    serializer_class = AplicacionesMovilesSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = AppMovil.objects.filter(publicado=True, activado=True)
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class PortalDeMapasViewSet(viewsets.ModelViewSet):
    """
    Portales de mapas activos
    """
    serializer_class = PortalDeMapasSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = PortalDeMapas.objects.filter()
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CapaMapaPortalViewSet(viewsets.ModelViewSet):
    """
    Capas de mapas activos.
    Parámetros:
        portal_id -- id del portal por el cual quiero filtrar
        categoria_id -- id de la categoria a filtrar
    """
    serializer_class = CapaMapaPortalSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CapaMapaPortal.objects.filter(publicado=True)
        portal_id = self.request.query_params.get('portal_id', None)
        if portal_id is not None:
            queryset = queryset.filter(categoria__portal__id=portal_id)
        categoria_id = self.request.query_params.get('categoria_id', None)
        if categoria_id is not None:
            queryset = queryset.filter(categoria__id=categoria_id)
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class InicioFinAlertaViewSet(viewsets.ModelViewSet):
    """
    Cada una de las repeticiones de una alerta en el municipio
    """
    serializer_class = InicioFinAlertaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = InicioFinAlerta.objects.filter(alerta__publicado=True)
        now = timezone.now()
        one_hour_ago = now - timedelta(hours=1)
        queryset = queryset.filter(
            Q(inicia__gte=one_hour_ago) | Q(termina__gte=now)
        )
        queryset.order_by('inicia')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class AlertaViewSet(viewsets.ModelViewSet):
    """
    Cada una de las repeticiones de una alerta en el municipio
    """
    serializer_class = AlertaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Alerta.objects.filter(publicado=True)
        now = timezone.now()
        one_hour_ago = timezone.now() - timedelta(hours=1)
        queryset = queryset.filter(Q(iniciofinalerta__inicia__gte=one_hour_ago) | Q(
            iniciofinalerta__termina__gte=now))
        queryset.order_by('inicia')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class DatoOficinasViewSet(viewsets.ModelViewSet):
    """
    Cada una de las oficinas del gobierno.
    Por ejemplo: Direccion de sistemas de informacion, Direccion de gobierno
    abierto, etc

    Puede filtrarse por categoría con el parametro categoria_id.

    Pueden filtrarse las dependecias(de un nivel debajo)de un cargo con el parametro 'd', por
    ejemplo: d=1 listaria los cargos del nivel de abajo que dependen del cargo con id 1.

    Pueden filtrarse todas las dependencias(todos los niveles debajo) con el parametro
    'd' pero se debe setear a 1 el parámetro 'recursivo', por ejemplo:
    d=1&recursivo=1 listaria todos los cargos que dependen del cargo con id 1.
    El parámetro 'recursivo' sólo se usa junto al parámetro 'd'.
    """
    serializer_class = DatoOficinasSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Cargo.objects.filter(
            publicado=True, activado=True)

        d = self.request.query_params.get('d', None)
        recursivo = self.request.query_params.get('recursivo', False)
        if d is not None:
            cargo = get_object_or_404(Cargo, pk=d)
            dependencias = cargo.dependencias(recursive=recursivo)

            if dependencias:
                queryset = queryset.filter(pk__in=[x.pk for x in dependencias])
            else:
                # si el cargo no tiene dependencias, se devuelve lista vacía
                queryset = queryset.filter(pk=0)

        # filtro por categoría
        categoria_id = self.request.query_params.get('categoria_id', None)
        if categoria_id is not None:
            queryset = queryset.filter(categoria__id=categoria_id)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CategoriaPortalDatosViewSet(viewsets.ModelViewSet):
    """
    Cada una de las categorías de datos del portal.
    Se puede filtrar por id de la categoría con el parámetro 'id_categoria'.
    Pueden ser varios ids separados por comas.
    """
    serializer_class = CategoriaPortalDatosSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CategoriaPortalDatos.objects.filter(portal__publicado=True)

        id_categoria = self.request.query_params.get('id_categoria', None)
        if id_categoria is not None:
            id_categoria = id_categoria.split(',')
            queryset = queryset.filter(id__in=id_categoria)

        queryset.order_by('nombre')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class DatoPortalViewSet(viewsets.ModelViewSet):
    """
    Cada una de los datos del portal
    """
    serializer_class = DatoPortalSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = DatoPortal.objects.filter(estado=DatoPortal.PUBLICADO,
                                             categoria__portal__publicado=True)
        queryset.order_by('titulo')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class VersionDatoViewSet(viewsets.ModelViewSet):
    """
    Cada una de las versiones de los datos del portal
    """
    serializer_class = VersionDatoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = VersionDato.objects.filter(
            dato__estado=DatoPortal.PUBLICADO,
            dato__categoria__portal__publicado=True)
        queryset.order_by('titulo')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
