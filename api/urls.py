# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from rest_framework import routers
from .views import *


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'categorias-cargo',
    CargoCategoriaViewSet,
    base_name='cargo-categorias')
router.register(r'cargos', DatoOficinasViewSet, base_name='cargos')
router.register(r'funciones', FuncionViewSet, base_name='funciones')
router.register(r'ddjj', DeclaracionJuradaViewSet,
                base_name='declaraciones-juradas')

router.register(
    r'plan_de_metas',
    PlanDeMetasViewSet,
    base_name='plan_de_metas')
router.register(r'lineamiento', LineamientoViewSet, base_name='lineamiento')
router.register(r'componente', ComponenteViewSet, base_name='componente')
router.register(r'objetivo', ObjetivoViewSet, base_name='objetivo')
router.register(r'meta', MetaViewSet, base_name='meta')
router.register(
    r'indicador_de_metas',
    IndicadorMetaViewSet,
    base_name='indicador_de_metas')

router.register(
    r'eventos-publicos',
    EventosPublicosViewSet,
    base_name='eventos_publicos')
router.register(
    r'agrupador-actividad',
    AgrupadorActividadViewSet,
    base_name='agrupador_actividad')
router.register(
    r'participante',
    ParticipanteActividadViewSet,
    base_name='participante')
router.register(
    r'actividad-publica',
    ActividadPublicaViewSet,
    base_name='actividad_publica')
router.register(
    r'repeticion-actividad-publica',
    RepeticionActividadPublicaViewSet,
    base_name='repeticion_actividad_publica')

router.register(
    r'organizador-evento',
    OrganizadorEventoViewSet,
    base_name='organizador-evento')
router.register(
    r'tipo-actividad',
    TipoActividadViewSet,
    base_name='tipo-actividad')
router.register(
    r'audiencia-actividad',
    AudienciaActividadViewSet,
    base_name='audiencia-actividad')
router.register(
    r'disciplina-actividad',
    DisciplinaActividadViewSet,
    base_name='disciplina-actividad')
router.register(
    r'lugar-actividad',
    LugarActividadViewSet,
    base_name='lugar-actividad')
router.register(
    r'circuito-actividad',
    CircuitoActividadViewSet,
    base_name='circuito-actividad')

router.register(
    r'aplicaciones-moviles',
    AplicacionesMovilesViewSet,
    base_name='aplicaciones-moviles')

# portales de mapas
router.register(
    r'portales-de-mapas',
    PortalDeMapasViewSet,
    base_name='portales_de_mapas')
router.register(
    r'capas-de-mapas',
    CapaMapaPortalViewSet,
    base_name='capas_de_mapas')

# Alertas
router.register(r'alertas', AlertaViewSet, base_name='alertas')
router.register(
    r'alertas-fechas',
    InicioFinAlertaViewSet,
    base_name='alertas_fechas')

# Si va a usarse desde el backend debe tenerse como API privada con alguna
# medida de seguridad y que no se vea en la lista de llamadas públicas
# router.register(r'accesoinfopublica', AccesoInfoPublicaViewSet, base_name='accesoinfopublica')

# Datos de los portales
router.register(
    r'datos-abiertos',
    DatoPortalViewSet,
    base_name='datos_abiertos')
# Categorías de los datos
router.register(
    r'categorias-datos-abiertos',
    CategoriaPortalDatosViewSet,
    base_name='categorias_datos_abiertos')
# no se necesita router.register(r'versiones-datos-abiertos',
# VersionDatoViewSet, base_name='versiones_datos_abiertos')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^v2/', include('api.v2.urls')),
    url(r'^v3/', include('api.v3.urls')),
]
