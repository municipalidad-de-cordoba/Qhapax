= ================================================================================= =
! Este repositorio se encuentra en modo solo lectura                                !
= ================================================================================= =
! El desarrollo continuará en: https://git.dtec.cordoba.gov.ar/modernizacion/qhapax !
= ================================================================================= =


Qhapax
======

*Qhapax* [1] es un conjunto de aplicaciones pensadas para sistematizar e interconectar
los datos de la gestión de Gobierno.

Si bien se incluye un portal de datos básico podríá conectarse a otras iniciativas (Andino-CKAN, Junar, etc).

El enfoque de *Qhapax* atiende primero a la recolección y sistematización de datos que hoy existen en muy diferentes fuentes de datos.
Normalmente los datos en un gobierno se encuentran en:

 - Papel (expedientes, contratos, normas, etc).
 - Planillas de cálculo (principalmente Excel y ante la ausencia de sistemas informáticos).
 - Sistemas informáticos propios (generalmente sin un web service para interconectar datos).
 - Sistemas informáticos en proveedores externos (generalmente sin un web service para interconectar datos).

Qhapax ha sido efectivo en dar de alta módulos que reemplazan gestión de datos en Excel o en papel.

Estado
------

Actualmente está en activo desarrollo. Está en proceso de transformarse en un producto fácilmente reutilizable
con aplicaciones seleccionables.

Qhapax no es (*solo*) un Portal de datos cómo CKAN, Junar o Andino. Estos portales suponen
que los datos existen y solo deben ser publicados. Qhapax *es* un portal de datos pero su fuerte es
el proceso anterior de administración, gestión e interconexión de los datos. Qhapax está
pensado para permitir muy fácil y muy rápido:

 - Modelar datos de muy diferentes temáticas.
 - Trabajar con datos espaciales.
 - Exponer web services muy rapida y fácilmente.
 - Conectarse a otras fuentes de datos.
 - Desplegar tableros internos de control.

.. figure:: otros/images/screen-datos-abiertos.png?raw=true
   :alt: Alt Datos abiertos
   Alt Plataforma Qhapax

Tecnología usada
~~~~~~~~~~~~~~~~

Qhapax esta desarrollado sobre Django 1.11 + Python 3.
Utiliza una base de datos Postgres con PostGis para el uso de GeoDjango.
Incluye tambien Django-Rest-Framework y Haystack/Elasticsearch

Colaboración y participación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si queres implementar *Qhapax* en tu gobierno podes contactarnos por
cualquier consulta. Es posible simplemente descargar el código y hacerlo
correr en tu servidor o tambien usar nuestra infraestructura. La
Municipalidad de Córdoba apoyará a otros gobiernos que deseen participar
de esta iniciativa.

Instalar Qhapax 
---------------

Primero debes `inicializar la base Postgres <inicializar_base_postgres.rst>`_.
Luego Qhapax puede correr `con Docker <instalar_via_docker.rst>`_ 
o `en un entorno local <instalar_en_entorno_local.rst>`_

Colaboración y dudas
--------------------

Si queres implementar *Qhapax* en tu gobierno o participar del desarrollo de la plataforma podes
 - Escribirnos a gobiernoabiertocba@gmail.com
 - Enviarnos tu *issue* o *PR* vía este repositorio.

La Municipalidad de Córdoba tiene la intención de apoyar a otros gobiernos que deseen participar de esta iniciativa.  

::

    [1] *Qhapax* es una voz quechua para describir al que tiene el poder.
    En este caso, en nuestra época, el gobierno elegido democráticamente.
    *Qhapax huasi* se usaba para describir al palacio de gobierno.