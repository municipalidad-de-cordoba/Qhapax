# mientras activamos celery

HOY=`date +%Y-%m-%d`
FECHAATRAS=`date --date='-60 days' +%Y-%m-%d`

python manage.py importar_nacimientos_registro_civil --fechaDesde $FECHAATRAS --fechaHasta $HOY --force_update True

python manage.py importar_obras_publicas --fecha 01/01/1981
python manage.py importar_trazados_obras_publicas --fecha 01/01/1981

python manage.py importar_obras_de_terceros
python manage.py importar_trazados_obras_de_terceros

python manage.py importar_arbolado --fecha $FECHAATRAS
# ver si hacen falta
# python manage.py grabar_lista_arboles
# python manage.py grabar_lista_de_actividades_comerciales_geolocalizadas
# python manage.py grabar_lista_de_actividades_comerciales_geolocalizadas incluir_datos_privados True

python manage.py grabar_lista_obras_de_terceros
python manage.py grabar_lista_frente_de_obras_de_terceros

