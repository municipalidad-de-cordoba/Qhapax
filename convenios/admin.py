from django.contrib import admin
from .models import (Convenio, MultimediaConvenio,
                     TipoConvenio, ParticipantesConvenio)


class ParticipantesInline(admin.StackedInline):
    model = ParticipantesConvenio
    extra = 1


class TipoConvenioAdmin(admin.ModelAdmin):
    list_display = ("nombre", )
    search_fields = ['nombre', 'descripcion']


class ConvenioAdmin(admin.ModelAdmin):
    list_display = ("nombre", "tipo", "url_noticia")
    search_fields = ['nombre', 'observaciones_internas', 'detalles']
    list_filter = ['tipo']
    inlines = [ParticipantesInline]


class MultimediaConvenioAdmin(admin.ModelAdmin):
    def tipo(self, obj):
        return obj.multimedia.tipo

    list_display = ("convenio", "tipo")
    list_filter = ['multimedia__tipo']


admin.site.register(Convenio, ConvenioAdmin)
admin.site.register(TipoConvenio, TipoConvenioAdmin)
admin.site.register(MultimediaConvenio, MultimediaConvenioAdmin)
