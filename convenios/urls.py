from django.conf.urls import url
from . import views


urlpatterns = [url(r'^$',
                   views.home,
                   name='convenios.home'),
               url(r'^convenio/(?P<convenio_nombre>[\w-]+)/(?P<convenio_id>[0-9]+)$',
                   views.convenio,
                   name='convenios.convenio'),
               ]
