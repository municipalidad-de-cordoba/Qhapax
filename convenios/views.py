from django.shortcuts import render, redirect
from convenios.models import Convenio
from django.views.decorators.cache import cache_page
from django.utils.text import slugify


@cache_page(60 * 60 * 12)  # 12 h
def home(request):

    # obtenemos 10 convenios nomás
    convenios = Convenio.objects.all().order_by('-importancia')[:10]

    context = {'convenios': convenios}
    url = 'website/template2017/convenios.html'
    return render(request, url, context)


@cache_page(60 * 60 * 12)  # 12 h
def convenio(request, convenio_nombre, convenio_id):

    # redirigimos si es necesario a la nueva URL correcta
    convenioobj = Convenio.objects.get(pk=convenio_id)

    bad_url = False
    if slugify(convenioobj.nombre) != slugify(convenio_nombre):
        bad_url = True

    if bad_url:
        return redirect(convenioobj)  # tomara el get_absolute_url del objeto

    context = {'convenio': convenioobj}
    url = 'website/template2017/convenio.html'
    return render(request, url, context)
