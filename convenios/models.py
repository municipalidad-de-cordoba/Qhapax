from django.db import models
from organizaciones.models import Organizacion
from core.models import MultimediaObj
from ckeditor.fields import RichTextField
from django.core.urlresolvers import reverse
from django.utils.text import slugify


class TipoConvenio(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Tipo de convenio'
        verbose_name_plural = 'Tipos de convenio'


class Convenio(models.Model):
    """ convenio """
    nombre = models.CharField(max_length=250)
    tipo = models.ForeignKey(TipoConvenio)
    observaciones_internas = models.TextField(null=True, blank=True)
    documento = models.FileField(upload_to='convenios/')
    fecha_alta_convenio = models.DateField(auto_now=False,
                                           auto_now_add=False,
                                           null=True,
                                           blank=True)
    observaciones_publicas = RichTextField(null=True, blank=True)
    url_en_digesto = models.URLField(
        null=True,
        blank=True,
        help_text='URL al Decreto de designación en el Boletín Oficial')
    url_noticia = models.URLField(null=True, blank=True)
    importancia = models.PositiveIntegerField(
        default=100,
        help_text='Mas alto es más importante, para mostrar los mas relevantes en listas')

    def __str__(self):
        return self.nombre

    @property
    def organizaciones(self):
        return self.participantesconvenio_set.all()

    def get_absolute_url(self):
        return reverse(
            'convenios.convenio',
            kwargs={
                'convenio_id': self.id,
                'convenio_nombre': slugify(
                    self.nombre)})

    class Meta:
        verbose_name = 'Convenio de colaboración'
        verbose_name_plural = 'Convenios de colaboración'
        ordering = ['-importancia', '-fecha_alta_convenio']


class ParticipantesConvenio(models.Model):
    """ cada participante y detalles de su particpación """
    convenio = models.ForeignKey(Convenio)
    organizacion = models.ForeignKey(Organizacion)
    detalles = models.TextField(
        null=True,
        blank=True,
        help_text='Observaciones publicas')
    observaciones_internas = models.TextField(null=True, blank=True)

    def __str__(self):
        return "{} con {}".format(
            self.convenio.nombre,
            self.organizacion.nombre)

    class Meta:
        verbose_name = 'Participante del convenio'
        verbose_name_plural = 'Participantes del convenio'


class MultimediaConvenio(models.Model):
    """contenidos multimedia varios que referencian a la forma del convenio """
    convenio = models.ForeignKey(Convenio)
    multimedia = models.ForeignKey(MultimediaObj)

    class Meta:
        verbose_name = 'Archivo multimedia del convenio'
        verbose_name_plural = 'Archivos multimedia del convenio'
