from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import IdiomaSerializer, GuiaTuristicoSerializer
from api.pagination import DefaultPagination
from django.db.models import Q
from guiasturisticos.models import GuiaTuristico
from core.models import Idioma


class IdiomasViewSet(viewsets.ModelViewSet):
    """ lista de idiomas """
    serializer_class = IdiomaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Idioma.objects.all().order_by('nombre')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class GuiasTuristicosViewSet(viewsets.ModelViewSet):
    """
    Lista de Guias Turísticos habilitados
    Se puede buscar por patente, titular y CUIT con el param "q"
    Por ejemplo "q=perez"
    Se puede buscar por idioma con idioma_id=1
    """
    serializer_class = GuiaTuristicoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # entregar los publicados y los habilitados
        queryset = GuiaTuristico.objects.filter(publicado=True)

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(nombre__icontains=q) |
                Q(apellido__icontains=q) |
                Q(unique_id__icontains=q) |
                Q(idiomas__nombre__icontains=q)
            )

        idioma_id = self.request.query_params.get('idioma_id', None)
        if idioma_id is not None:
            queryset = queryset.filter(idiomas__idioma=idioma_id)

        queryset.order_by('titular__nombre')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
