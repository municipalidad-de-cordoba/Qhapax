from django.conf.urls import url

from . import views

urlpatterns = [url(r'^lista-de-acreditados-prensa.(?P<filetype>csv|xls)$',
                   views.planilla_acreditados_prensa,
                   name='credencialesprensa.lista'),
               url(r'^acreditado-prensa-(?P<pk>[0-9]+).html$',
                   views.ficha_acreditado_prensa,
                   name='credencialesprensa.ficha'),
               ]
