from django.db import models
from core.models import Persona
from django.core.urlresolvers import reverse


class AcreditadoPrensa(Persona):
    """
    Guia turistico no hereda de 'Core.Persona' porque aquel esta pensado
    para funcionarios #TODO
    """
    cv_archivo = models.FileField(
        upload_to='CVs-prensa/',
        null=True,
        blank=True,
        verbose_name='Curriculum en PDF, DOC u otros')
    publicado = models.BooleanField(default=False)
    detalles_acreditacion = models.TextField(
        null=True,
        blank=True,
        help_text='Tareas para las que está acreditado u observaciones')

    def get_absolute_url(self):
        return reverse('credencialesprensa.ficha', kwargs={'pk': self.pk})
