#!/usr/bin/python
from django.core.management.base import BaseCommand
from django.conf import settings
from portaldedatos.models import ArchivoCSV
from guiasturisticos.models import NivelIdioma, GuiaTuristico, NivelIdiomaGuia
from core.models import Idioma, TipoId
from django.db import transaction
import csv
import sys


class Command(BaseCommand):
    help = """Comando para importar lista de guías habilitados para el registro.
                Requiere un CSV cargado en el sistema. Se marcará como _procesado_
                luego de ejecutarse para evitar duplicados
            """

    def add_arguments(self, parser):
        parser.add_argument(
            '--csv_id',
            type=int,
            help='ID del CSV cargado al sistema')
        parser.add_argument('--force', action='store_true', dest='force',
                            default=False, help="forzar a procesar el archivo")

    @transaction.atomic
    def handle(self, *args, **options):
        force = options['force']
        if force:
            self.stdout.write(self.style.WARNING(
                '--- Forzando importación de CSV ---'))

        try:
            if force:
                instanceCSV = ArchivoCSV.objects.get(pk=options['csv_id'])
            else:
                instanceCSV = ArchivoCSV.objects.get(
                    pk=options['csv_id'], procesado=False)
        except ArchivoCSV.DoesNotExist:
            if force:
                self.stdout.write(
                    self.style.ERROR(
                        'El CSV id: %s no existe' %
                        options['csv_id']))
            else:
                self.stdout.write(
                    self.style.ERROR(
                        'El CSV id: %s no existe o ya está procesado' %
                        options['csv_id']))
            sys.exit(1)

        if instanceCSV is None:
            self.stdout.write(self.style.ERROR('No hay CSV'))
            sys.exit(1)

        self.stdout.write(
            self.style.SUCCESS(
                'Importando csv (id: {})'.format(
                    instanceCSV.id)))

        # TIENEN QUE TENER ENCABEZADO!
        if not instanceCSV.tiene_fila_encabezado:
            self.stdout.write(self.style.ERROR(
                'El CSV indica que no tiene encabezado. Se procesará igualemente'))

        # asegurarse de que el archivo tenga la misma estructura (encabezados)

        fieldnames = ['Nº', 'DNI', 'GUIAS', 'IDIOMAS']

        dni_obj = TipoId.objects.get(pk=settings.TIPOID_PK_PRINCIPAL)
        nivel_idioma_obj, created = NivelIdioma.objects.get_or_create(
            nombre='Profesional')

        count = 0
        guias_nuevos = 0
        guias_repetidos = 0

        with open(instanceCSV.archivo_local.path) as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=instanceCSV.separado_por,
                                    quotechar=instanceCSV.contenedor_de_texto)

            header = reader.__next__()

            sorted_fieldnames = sorted(fieldnames)
            self.stdout.write(
                self.style.SUCCESS(
                    'Fieldnames {}'.format(sorted_fieldnames)))

            sorted_header = sorted(list(header.values()))
            self.stdout.write(
                self.style.SUCCESS(
                    'Headers {}'.format(sorted_header)))

            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR(
                    'BAD FIELDNAMES [{}] -> [{}]'.format(header.values(), fieldnames)))
                sys.exit(1)

            errores = []
            for row in reader:
                count += 1
                # self.stdout.write(self.style.SUCCESS('Linea leida: {}'.format(row)))

                nro = row['Nº'].strip()
                dni = row['DNI'].strip().replace(',', '')

                guia = row['GUIAS'].strip()
                p = guia.split(',')
                apellido = p[0].strip()
                nombre = p[1].strip()

                guia, created = GuiaTuristico.objects.get_or_create(
                    tipo_id=dni_obj, unique_id=dni, nombre=nombre, apellido=apellido)

                if created:
                    guias_nuevos += 1
                    # self.stdout.write(self.style.SUCCESS('Orga nueva: {}'.format(organizacion)))
                else:
                    guias_repetidos += 1

                idiomas_str = row['IDIOMAS'].strip()

                if idiomas_str == '':
                    continue

                p = idiomas_str.split('-')
                for idioma_str in p:
                    idioma_str = idioma_str.strip().capitalize()

                    idioma, created = Idioma.objects.get_or_create(
                        nombre=idioma_str)
                    nivel_idioma_guia, created = NivelIdiomaGuia.objects.get_or_create(
                        nivel=nivel_idioma_obj, idioma=idioma)
                    guia.idiomas.add(nivel_idioma_guia)

            if len(errores) > 0:
                self.stdout.write(
                    self.style.ERROR(
                        'ERRORES AL PROCESAR LA LINEA {}'.format(count)))
                for e in errores:
                    self.stdout.write(self.style.ERROR(e))
                sys.exit(1)

        instanceCSV.procesado = True
        instanceCSV.save()

        self.stdout.write(
            self.style.SUCCESS(
                'Archivo cargado con éxito, se cargaron {} guias ({} nuevos, {} repetidos)'.format(
                    count,
                    guias_nuevos,
                    guias_repetidos)))
