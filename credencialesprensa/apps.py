from django.apps import AppConfig


class CredencialesprensaConfig(AppConfig):
    name = 'credencialesprensa'
