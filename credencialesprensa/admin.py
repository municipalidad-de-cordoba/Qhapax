from django.contrib import admin
from core.models import ComunicacionPersona
from .models import AcreditadoPrensa


class ComunicacionInline(admin.StackedInline):
    model = ComunicacionPersona
    extra = 1


class AcreditadoPrensaAdmin(admin.ModelAdmin):
    def foto_thumb(self, instance):
        if instance.foto:
            return '<img src="%s" style="width: 80px;"/>' % (instance.foto.url)
        return ' <img src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACXBIWXMAAAAnAAAAJwEqCZFPAAAA B3RJTUUH4AcJFDE2B3C7PwAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUH AAAACklEQVQI12P4DwABAQEAG7buVgAAAABJRU5ErkJggg==" alt="blanco " style="width: 80px; height: 120px;" />'
    foto_thumb.short_description = 'foto thumb'
    foto_thumb.allow_tags = True

    list_display = (
        'nombre',
        'apellido',
        'foto_thumb',
        'detalles_acreditacion')
    search_fields = [
        'nombre',
        'apellido',
        'unique_id',
        'detalles_acreditacion']
    # list_filter = []
    inlines = [ComunicacionInline]


admin.site.register(AcreditadoPrensa, AcreditadoPrensaAdmin)
