from django.shortcuts import render
from credencialesprensa.models import AcreditadoPrensa
import django_excel as excel
from django.views.decorators.cache import cache_page
from django.shortcuts import get_object_or_404


@cache_page(60 * 60 * 12)  # 12 h
def ficha_acreditado_prensa(request, pk):
    '''
    Ficha de un guía turístico
    http://credenciales-prensa/acreditado-prensa-2.html
    '''
    prensa = get_object_or_404(AcreditadoPrensa, pk=pk)
    base_url = request.website
    url = '{}{}'.format(base_url, prensa.get_absolute_url())
    qr_image = prensa.get_image_qr(url=url)
    qr_download = prensa.get_download_link_qr(url=url)
    context = {
        'prensa': prensa,
        'qr_image': qr_image,
        'qr_download': qr_download}
    url = 'credencialesprensa/ficha.html'
    return render(request, url, context)


@cache_page(60 * 60 * 12)  # 12 h
def planilla_acreditados_prensa(request, filetype):
    '''
    Planilla de guías turísticos
    http://localhost:8000/credenciales-prensa/lista-de-acreditados-prensa.xls
    '''

    prensas = AcreditadoPrensa.objects.filter(
        publicado=True).order_by(
        'apellido', 'nombre')
    csv_list = []
    csv_list.append(['registro',
                     'Apellido',
                     'Nombre',
                     'DNI',
                     'Genero',
                     'Edad',
                     'url',
                     'url_foto_thumbnail',
                     'url_foto_original',
                     'acreditación'])

    for prensa in prensas:
        dni = '{} {}'.format(prensa.tipo_id, prensa.unique_id)
        url = "{}{}".format(request.website, prensa.get_absolute_url())

        try:
            foto = "{}{}".format(request.website, prensa.foto.url)
            foto_thumb = "{}{}".format(
                request.website, prensa.foto.thumbnail['125x125'].url)
        except (ValueError, FileNotFoundError):
            foto = ""
            foto_thumb = ""

        csv_list.append([
            prensa.id,
            prensa.apellido,
            prensa.nombre,
            dni,
            prensa.get_genero_display(),
            prensa.edad,
            url,
            foto_thumb,
            foto,
            prensa.detalles_acreditacion
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
