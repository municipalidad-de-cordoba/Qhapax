from django.contrib import admin
from .models import *


class BoletinOficialAdmin(admin.ModelAdmin):
    list_display = ('codigo', 'fecha', 'publicado', 'archivo')
    search_fields = ['codigo']
    list_filter = ['publicado']


admin.site.register(BoletinOficial, BoletinOficialAdmin)