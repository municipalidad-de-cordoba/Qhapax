from django.apps import AppConfig


class BoletinoficialConfig(AppConfig):
    name = 'boletinoficial'
