from django.conf.urls import url

from . import views

urlpatterns = [url(r'^lista-de-boletines-municipales-(?P<anio>[0-9]+).(?P<filetype>csv|xls)$',
                   views.lista_boletines,
                   name='boletinesmunicipales.lista'),
               url(r'^lista-de-boletines-municipales.(?P<filetype>csv|xls)$',
                   views.lista_boletines_todos,
                   name='boletinesmunicipales.lista_todos'),
               url(r'^boletin-municipal-(?P<codigo>[\w-]+).pdf$',
                   views.boletin,
                   name='boletinesmunicipales.boletin'),
               ]
