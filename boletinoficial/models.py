from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse


class BoletinOficial(models.Model):
    """ Cada uno de los boletines municipales """
    codigo = models.CharField(max_length=30)
    fecha = models.DateField(null=True, blank=True)
    archivo = models.FileField(upload_to='boletines-municipales/')
    publicado = models.BooleanField(default=True)

    def get_absolute_url(self):
        return reverse(
            'boletinesmunicipales.boletin', kwargs={
                'codigo': self.codigo})

    def get_url_produccion(self):
        return '{}://{}{}'.format(settings.PROTOCOLO_PRODUCCION,
                                  settings.DOMINIO_PRODUCCION,
                                  self.get_absolute_url())

    def __str__(self):
        fecha = '' if self.fecha is None else ' - {}'.format(self.fecha)
        return 'Boletin Municipal {} - {}'.format(self.codigo, fecha)

    class Meta:
        verbose_name = "Boletin Oficial"
        verbose_name_plural = "Boletines Oficiales"
