from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import BoletinOficialSerializer
from api.pagination import DefaultPagination
from boletinoficial.models import BoletinOficial


class BoletinOficialViewSet(viewsets.ModelViewSet):
    """
    Cada una de las versiones de los datos del portal
    """
    serializer_class = BoletinOficialSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = BoletinOficial.objects.filter(publicado=True)
        queryset.order_by('-fecha')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
