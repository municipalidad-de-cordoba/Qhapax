from django.conf.urls import url, include
from rest_framework import routers
from .views import BoletinOficialViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'boletines-municipales',
    BoletinOficialViewSet,
    base_name='boletin-municipal')

urlpatterns = [
    url(r'^', include(router.urls)),
]
