from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from boletinoficial.models import BoletinOficial


class BoletinOficialSerializer(CachedSerializerMixin):
    my_absolute_url = serializers.URLField(
        source='get_url_produccion', read_only=True)

    class Meta:
        model = BoletinOficial
        fields = ('codigo', 'fecha', 'my_absolute_url')


# Registro los serializadores en la cache de DRF
cache_registry.register(BoletinOficialSerializer)
