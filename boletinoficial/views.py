from django.views.decorators.cache import cache_page
from boletinoficial.models import BoletinOficial
import django_excel as excel
from django.shortcuts import get_object_or_404
from django.http import HttpResponse


@cache_page(60 * 60 * 12)  # 12 horas
def lista_boletines(request, anio, filetype):
    '''
    lista de entes públicos según tipo
    localhost:8000/boletines-municipales/lista-de-boletines-municipales-anio.csv
    '''
    bos = BoletinOficial.objects.filter(
        publicado=True, fecha__year=anio).order_by('-fecha')
    csv_list = []
    csv_list.append(['codigo', 'fecha', 'archivo', 'ID interno'])
    for bo in bos:
        csv_list.append([bo.codigo, bo.fecha, bo.get_absolute_url(), bo.id])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 horas
def lista_boletines_todos(request, filetype):
    '''
    lista de TODOS los boletines publicos
    localhost:8000/boletines-municipales/lista-de-boletines-municipales.csv
    '''
    bos = BoletinOficial.objects.filter(publicado=True).order_by('-fecha')
    csv_list = []
    csv_list.append(['codigo', 'fecha', 'archivo', 'ID interno'])
    for bo in bos:
        csv_list.append([bo.codigo, bo.fecha, bo.get_absolute_url(), bo.id])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 horas
def boletin(request, codigo):
    '''
    lista de entes públicos según tipo
    '''
    bo = get_object_or_404(BoletinOficial, publicado=True, codigo=codigo)

    path = bo.archivo.path
    f = open(path, 'rb').read()
    response = HttpResponse(f, content_type='application/pdf')

    return response
