#!/usr/bin/python
'''
Generar una lista de comercios geolocalizados.
Tiene una versión al público y una con algunos campos privados.
'''
from django.core.management.base import BaseCommand
from django.conf import settings
from organizaciones.models import Organizacion, SucursalOrganizacion
from inscripcionempresas.models import InscripcionActividadEmpresa
import django_excel as excel
import os


class Command(BaseCommand):
    help = """Generar un CSV con todos los comercios que están
                geolocalizados y tienen una actividad comercial definida"""

    def add_arguments(self, parser):
        parser.add_argument(
            '--incluir_datos_privados',
            type=bool,
            default=False,
            help='Indicar si se incluyen datos privados de los comercios')

    def handle(self, *args, **options):
        incluir_datos_privados = options['incluir_datos_privados']
        self.stdout.write(self.style.SUCCESS('Comenzando generación de CSV'))

        self.stdout.write(self.style.SUCCESS('Buscando sucursales'))
        sucursales = SucursalOrganizacion.objects.filter(
            parcela__isnull=False).filter(
            parcela__poligono__isnull=False)
        self.stdout.write(
            self.style.SUCCESS(
                '{} sucursales encontradas'.format(
                    len(sucursales))))

        self.stdout.write(self.style.SUCCESS('Contando comercios'))
        comercios = sucursales.order_by('organizacion_id').distinct(
            'organizacion').values('organizacion')
        self.stdout.write(
            self.style.SUCCESS(
                '{} comercios encontradas'.format(
                    len(comercios))))

        self.stdout.write(self.style.SUCCESS('Buscando inscripciones'))
        inscripciones = InscripcionActividadEmpresa.objects.filter(
            organizacion__in=comercios)
        self.stdout.write(
            self.style.SUCCESS(
                '{} inscripciones encontradas'.format(
                    len(inscripciones))))

        csv_list = []

        encabezados = [
            'comercio_id',
            'Categoria Actividad ID',
            'Categoria Actividad',
            'Rama Actividad ID',
            'Rama Actividad',
            'latitud',
            'longitud',
            'Codigo Actividad',
            'Actividad']

        if incluir_datos_privados:
            encabezados += ['nombre comercio', 'CUIT']

        csv_list.append(encabezados)
        c = 0
        for inscripcion in inscripciones:
            c += 1
            if c % 1000 == 0:
                self.stdout.write(
                    self.style.SUCCESS(
                        '{} filas grabadas'.format(c)))
            sucursales_geo = inscripcion.organizacion.sucursales(solo_geo=True)
            for sucursal in sucursales_geo:
                if sucursal.parcela.centroide is None:
                    sucursal.parcela.centroide = sucursal.parcela.poligono.centroid
                    sucursal.parcela.save()
                fila = [sucursal.organizacion.id,
                        inscripcion.actividad.categoria.id,
                        inscripcion.actividad.categoria.nombre,
                        inscripcion.actividad.categoria.rama.id,
                        inscripcion.actividad.categoria.rama.nombre,
                        sucursal.parcela.centroide.y,
                        sucursal.parcela.centroide.x,
                        inscripcion.actividad.codigo,
                        inscripcion.actividad.nombre,
                        ]
                if incluir_datos_privados:
                    fila += [sucursal.organizacion.nombre,
                             sucursal.organizacion.CUIT]
                csv_list.append(fila)

        # FIXME #THINKME pensar una forma centralizada de guardar datos que no
        # se generan en timpo real

        dest = os.path.join(
            settings.MEDIA_ROOT,
            'comercios_geolocalizados.csv')
        if incluir_datos_privados:
            dest = os.path.join(
                settings.PRIVATE_FILES_ROOT,
                'comercios_geolocalizados_full.csv')

        excel.pe.save_as(array=csv_list, dest_file_name=dest)
        # DA ERRRO POR QUE SON MAS DE 65000 filas
        # excel.pe.save_as(array=csv_list,
        # dest_file_name=os.path.join(settings.MEDIA_ROOT,
        # 'comercios_geolocalizados.xls'))

        self.stdout.write(
            self.style.SUCCESS(
                'Archivo creado con {}'.format(c)))
