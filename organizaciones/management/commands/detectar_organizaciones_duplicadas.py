#!/usr/bin/python
"""
Comando para detectar organizaciones duplicadas en la bd
"""
from django.core.management.base import BaseCommand
from organizaciones.models import Organizacion
from django.db import transaction


class Command(BaseCommand):
    help = """Comando para detectar organizaciones duplicadas en la bd"""

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando comando'))

        # lista de objetos que referencian al modelo Organizacion
        objetos_relacionados = [f for f in Organizacion._meta.get_fields()
                                if (f.one_to_many or f.one_to_one)
                                and f.auto_created and not f.concrete
                                ]

        # acá se obtienen las organizaciones duplicadas(las que tienen el mismo
        # CUIT)
        organizaciones = Organizacion.objects.all().values('id', 'CUIT')
        result = {}

        for org in organizaciones:
            organizacion = Organizacion.objects.get(id=org['id'])
            try:
                result[org['CUIT']].append(
                    {'id': org['id'], 'cantidad_sucursales': len(organizacion.sucursales())})
            except KeyError:
                result[org['CUIT']] = [{'id': org['id'],
                                        'cantidad_sucursales': len(organizacion.sucursales())
                                        }]

            # Se agregan la cantidad de objetos relacionados
            # hardcodeo la lista de objetos, corregir esto!
            for dic in result[org['CUIT']]:
                dic[objetos_relacionados[0].name] = len(
                    organizacion.comunicacionorganizacion_set.all())
                dic[objetos_relacionados[1].name] = len(
                    organizacion.sucursalorganizacion_set.all())
                dic[objetos_relacionados[2].name] = len(
                    organizacion.inscripcionempresa_set.all())
                dic[objetos_relacionados[3].name] = len(
                    organizacion.inscripcionactividadempresa_set.all())
                dic[objetos_relacionados[4].name] = len(
                    organizacion.participantesconvenio_set.all())
                dic[objetos_relacionados[5].name] = len(
                    organizacion.empresatransportepublico_set.all())
                dic[objetos_relacionados[6].name] = len(
                    organizacion.vehiculotransportepublico_set.all())
                dic[objetos_relacionados[7].name] = len(
                    organizacion.obrapublica_set.all())
                dic[objetos_relacionados[8].name] = len(
                    organizacion.tituloguia_set.all())
                dic[objetos_relacionados[9].name] = len(
                    organizacion.obradeterceros_set.all())
                dic[objetos_relacionados[10].name] = len(
                    organizacion.espacioverde_set.all())

        self.stdout.write(self.style.SUCCESS('Organizaciones Duplicadas:'))
        od = 0
        for k, v in result.items():
            # si hay mas de un id, es porque el CUIT está repetido
            if len(v) > 1:
                od += 1
                self.stdout.write(self.style.SUCCESS(
                    'CUIT: {} --> ids: {}'.format(k, v)))

        self.stdout.write(
            self.style.SUCCESS(
                'cantidad de organizaciones duplicadas: {}'.format(od)))

        # VOLCAR RESULTADO EN UN ARCHIVO?????

        self.stdout.write(self.style.SUCCESS('FIN'))
