#!/usr/bin/python
'''
Importar comercios e industrias de un CSV local
Esta base trae comercios, vias de comunicación y ramas de actividades conectadas
'''
from django.core.management.base import BaseCommand

from portaldedatos.models import ArchivoCSV
from ordenanzatributaria.models import ActividadOTA
from organizaciones.models import (Organizacion, SucursalOrganizacion,
                                   ComunicacionOrganizacion)
from catastro.models import (DistritoCatastral, ZonaCatastral,
                             ManzanaCatastral, ParcelaCatastral)
from core.models import ComunicacionTipo
from inscripcionempresas.models import InscripcionActividadEmpresa
import csv
import sys


class Command(BaseCommand):
    help = """Comando para importar comercios del dump de comercios del sistema interno"""

    def add_arguments(self, parser):
        parser.add_argument(
            '--csv_id',
            type=int,
            help='ID del CSV cargado al sistema')

        parser.add_argument('--force', action='store_true', dest='force',
                            default=False, help="forzar a procesar el archivo")

    def handle(self, *args, **options):
        force = options['force']
        if force:
            self.stdout.write(self.style.WARNING(
                '--- Forzando importación de CSV ---'))

        try:
            if force:
                instanceCSV = ArchivoCSV.objects.get(pk=options['csv_id'])
            else:
                instanceCSV = ArchivoCSV.objects.get(
                    pk=options['csv_id'], procesado=False)
        except ArchivoCSV.DoesNotExist:
            if force:
                self.stdout.write(
                    self.style.ERROR(
                        'El CSV id: %s no existe' %
                        options['csv_id']))
            else:
                self.stdout.write(
                    self.style.ERROR(
                        'El CSV id: %s no existe o ya está procesado' %
                        options['csv_id']))
            sys.exit(1)

        comunicacion_email = ComunicacionTipo.objects.get(
            nombre__icontains='email')
        comunicacion_telefono = ComunicacionTipo.objects.get(
            nombre__icontains='teléfono')

        if instanceCSV:
            self.stdout.write(
                self.style.SUCCESS(
                    'Importando csv (id: {})'.format(
                        instanceCSV.id)))

            # TIENEN QUE TENER ENCABEZADO!
            if not instanceCSV.tiene_fila_encabezado:
                self.stdout.write(self.style.ERROR(
                    'El CSV indica que no tiene encabezado. Se procesará igualemente'))

            # asegurarse de que el archivo tenga la misma estructura
            # (encabezados)
            fieldnames = [
                'Rubro',
                'CUIT',
                'Tipo',
                'CodAct',
                'Nombre o Razon Social',
                'Calle',
                'Nro',
                'Piso',
                'Dpto',
                'Barrio',
                'C Postal',
                'Cod Catastral',
                'E-Mail',
                'Telefono']

            count = 0
            rubros_fallados = []
            errores = []
            cantidad_rubros_ok = 0
            cantidad_rubros_fallados = 0
            empresas_nuevas = 0
            empresas_repetidas = 0
            parcelas_ok = 0
            parcelas_falladas = 0
            parcelas_desconocidas = 0
            comercios_inactivos = 0
            sucursales_nuevas = 0
            sucursales_repetidas = 0
            with open(instanceCSV.archivo_local.path) as csvfile:
                reader = csv.DictReader(
                    csvfile,
                    fieldnames=fieldnames,
                    delimiter=instanceCSV.separado_por,
                    quotechar=instanceCSV.contenedor_de_texto)

                header = reader.__next__()
                if sorted(fieldnames) != sorted(list(header.values())):
                    self.stdout.write(self.style.ERROR(
                        'BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                    sys.exit(1)

                for row in reader:
                    count += 1
                    if count % 100 == 0:
                        status = '{} inact {} emp {} {} sucur {} {} parc {} {} {} errs {}'.format(
                            count,
                            comercios_inactivos,
                            empresas_nuevas,
                            empresas_repetidas,
                            sucursales_nuevas,
                            sucursales_repetidas,
                            parcelas_ok,
                            parcelas_desconocidas,
                            parcelas_falladas,
                            len(errores))
                        self.stdout.write(self.style.SUCCESS(status))

                    # chequear los datos
                    # número de 9 o 10 dígitos con el codigo de actividad
                    rubro = row['Rubro'].strip()
                    cuit = row['CUIT'].strip()  # cuit sin guiones
                    # tipo de contricuyente, nro del 1 al 9
                    tipo = int(row['Tipo'].strip())
                    # CodAct: solo 1 o 5.
                    # - 1: contribuyente Activo.
                    # - 5: aquellos que no presentaron la baja pero no
                    #      registran ni Declaración Jurada ni Pagos en los
                    #      últimos 18 meses.
                    codigo_actividad = int(row['CodAct'].strip())
                    nombre = row['Nombre o Razon Social'].strip()
                    # domicilio fical o legal para notificaciones, no el del
                    # comercio o sucursal, OJO
                    calle = row['Calle'].strip()
                    nro = row['Nro'].strip()
                    piso = row['Piso'].strip()
                    depto = row['Dpto'].strip()
                    barrio = row['Barrio'].strip()
                    codigo_postal = row['C Postal'].strip()
                    # 0 o numeros de 14 o 15 digitos
                    codigo_catastral = row['Cod Catastral'].strip()
                    email = row['E-Mail'].strip()
                    # en algunos casos es un segundo email (?)
                    telefono = row['Telefono'].strip()

                    # validaciones
                    if codigo_actividad == 5:
                        # ignorar por ahora los inactivos
                        comercios_inactivos += 1
                        continue

                    # en nuestra OTA tenemos los codigos con punto separados,
                    # aca viene con miles al final en lugar de un punto por
                    # ejemplo en el sistema si el código es 171200.2 en este
                    # CSV es 1712000002
                    # self.stdout.write(self.style.SUCCESS('Check rubro {} {}'.format(rubro, count)))
                    c1 = int(rubro[:-4])
                    c2 = int(rubro[-4:])
                    es_convenio_multilateral = c2 >= 9000
                    if es_convenio_multilateral:
                        c2 = c2 - 9000
                    if c2 > 0:
                        c2 = '.{}'.format(c2)
                    else:
                        c2 = ''
                    codigo_modo_punto = '{}{}'.format(c1, c2)

                    try:
                        actividad = ActividadOTA.objects.get(
                            codigo=codigo_modo_punto)
                    except ActividadOTA.DoesNotExist:
                        cantidad_rubros_fallados += 1
                        err = '{}|{}|No existe la actividad|{}|{}|{}'.format(
                            count, cantidad_rubros_fallados, rubro, codigo_modo_punto, nombre)
                        self.stdout.write(self.style.ERROR(err))
                        errores.append(err)
                        if rubro not in rubros_fallados:
                            rubros_fallados.append(rubro)
                        continue

                    # self.stdout.write(self.style.SUCCESS('{} Actividad OK {} {} {}'.format(count, actividad.codigo, actividad.categoria.nombre, actividad.categoria.rama.nombre)))

                    # ahora que se la actividad, cargar la empresa.
                    # hay duplicadas ...
                    empresas = Organizacion.objects.filter(CUIT=cuit)
                    if len(empresas) == 0:
                        empresa = Organizacion.objects.create(CUIT=cuit)
                        created = True
                    else:
                        # tomo alguna, despues habrá algún script de merge y limpieza
                        empresa = empresas[0]
                        created = False

                    if created:
                        empresas_nuevas += 1
                        empresa.nombre = nombre
                        empresa.save()
                        # self.stdout.write(self.style.SUCCESS('Empresa nueva {}'.format(empresa)))
                    else:
                        empresas_repetidas += 1
                        # self.stdout.write(self.style.SUCCESS('Empresa repetida {}'.format(empresa)))

                    # actualziar el domicilio fiscal
                    empresa.domicilio_calle = calle
                    empresa.domicilio_nro = nro
                    empresa.domicilio_piso = piso
                    empresa.domicilio_dpto = depto
                    empresa.domicilio_barrio = barrio
                    empresa.domicilio_codigo_postal = codigo_postal

                    empresa.save()

                    # revisar el email y el teléfono
                    email_empresa, created = ComunicacionOrganizacion.objects.get_or_create(
                        objeto=empresa, tipo=comunicacion_email, valor=email)
                    email_empresa.privado = True
                    email_empresa.save()

                    tel_empresa, created = ComunicacionOrganizacion.objects.get_or_create(
                        objeto=empresa, tipo=comunicacion_telefono, valor=telefono)
                    tel_empresa.privado = True
                    tel_empresa.save()

                    # grabar este comercio en esta actividad
                    inscripcion_actividad, created = InscripcionActividadEmpresa.objects.get_or_create(
                        organizacion=empresa, actividad=actividad)

                    # lo único que hay de la sucursal es la parcela del
                    # comercio en "codigo_catastral"
                    # algunos valores aparecen mucho, deber algo que no
                    # corresponde a la sucursal
                    parcelas_malas = [
                        '0',
                        '010100100100000',
                        '010100100100001',
                        '010100100100010',
                        '010101101100000']

                    if codigo_catastral in parcelas_malas:
                        parcelas_desconocidas += 1
                    else:
                        parcela = None
                        # pasar del código a una parcel real del sistema
                        # en el sistema los distritos son de 01 a 35
                        # las zonas son de 01 a 47 (repetidas en cada distrito)
                        # las manzanas son de 001 a 269 (repetidas)
                        # las parcelas son de 001 a 240
                        # entonces por ejemplo: 311404901600000 es
                        # distrito 31
                        # zona 14
                        # manzana 049
                        # parcela 016
                        # 00000 los últimos 5 digitos serían subparcelas o
                        # similar, se usan muy poco
                        try:
                            distrito = DistritoCatastral.objects.get(
                                codigo=codigo_catastral[-15:-13])
                        except DistritoCatastral.DoesNotExist:
                            err = 'No existe el Distrito Catastral {} {}'.format(
                                codigo_catastral[-15:-13], codigo_catastral)
                            self.stdout.write(self.style.ERROR(err))
                            errores.append(err)

                        try:
                            zona = ZonaCatastral.objects.get(
                                codigo=codigo_catastral[-13:-11], distrito=distrito)
                        except ZonaCatastral.DoesNotExist:
                            err = 'No existe la Zona Catastral {} en {} {}'.format(
                                codigo_catastral[-13:-11], distrito, codigo_catastral)
                            self.stdout.write(self.style.ERROR(err))
                            errores.append(err)

                        try:
                            manzana = ManzanaCatastral.objects.get(
                                codigo=codigo_catastral[-11:-8], zona=zona)
                        except ManzanaCatastral.DoesNotExist:
                            err = 'No existe la Manzana Catastral {} en {} {}'.format(
                                codigo_catastral[-11:-8], zona, codigo_catastral)
                            self.stdout.write(self.style.ERROR(err))
                            errores.append(err)

                        try:
                            parcela = ParcelaCatastral.objects.get(
                                codigo=codigo_catastral[-8:-5], manzana=manzana)
                        except ParcelaCatastral.DoesNotExist:
                            err = 'No existe la Parcela Catastral {} en {} {}'.format(
                                codigo_catastral[-8:-5], manzana, codigo_catastral)
                            self.stdout.write(self.style.ERROR(err))
                            errores.append(err)

                        if parcela is not None:
                            parcelas_ok += 1
                            # self.stdout.write(self.style.SUCCESS('PARCELA OK {}'.format(parcela)))
                            # podría haber más de uno
                            sucursales = SucursalOrganizacion.objects.filter(
                                organizacion=empresa, parcela=parcela)
                            if len(sucursales) > 0:
                                # eloijo uno, despues mezclare duplicados
                                sucursal = sucursales[0]
                                created = False
                            else:
                                sucursal = SucursalOrganizacion.objects.create(
                                    organizacion=empresa, parcela=parcela)
                                created = True

                            if created:
                                sucursales_nuevas += 1
                            else:
                                sucursales_repetidas += 1
                        else:
                            parcelas_falladas += 1

            if len(errores) > 0:
                self.stdout.write(
                    self.style.ERROR(
                        'ERRORES AL PROCESAR LA LINEA {}'.format(count)))
                for e in errores:
                    self.stdout.write(self.style.ERROR(e))

            # instanceCSV.procesado = True
            # instanceCSV.save()

            self.stdout.write(
                self.style.SUCCESS(
                    'Archivo cargado con éxito, se leyeron {} registros. {}'.format(
                        count, status)))
