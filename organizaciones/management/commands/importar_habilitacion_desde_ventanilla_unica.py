#!/usr/bin/python
'''
Importar lista de comercios desde la base de habilitacion (ventanilla única)
El proceso consiste en descargar un archivo RAR de 120MB, descomprimirlo y
levantar el dump

'''
from django.core.management.base import BaseCommand

from portaldedatos.models import ArchivoCSV
from ordenanzatributaria.models import (RamaActividad, CategoriaActividad,
                                        ActividadOTA)
from organizaciones.models import (Organizacion, SucursalOrganizacion,
                                   ComunicacionOrganizacion)
from catastro.models import (DistritoCatastral, ZonaCatastral,
                             ManzanaCatastral, ParcelaCatastral)
from core.models import ComunicacionTipo
from inscripcionempresas.models import InscripcionActividadEmpresa
from django.db import transaction
import csv
import sys
import datetime
from datetime import date
import rarfile
import tempfile


class Command(BaseCommand):
    help = """Comando para importar comercios del dump de comercios del sistema interno"""

    def add_arguments(self, parser):
        parser.add_argument('--url', type=str, help='URL del dump de la base')

    # @transaction.atomic
    def handle(self, *args, **options):
        url = options['url']

        self.stdout.write(self.style.SUCCESS('Descargando BASE'))
        try:
            basedata = requests.get(url).content  # es tipo byte
        except Exception as e:
            self.stdout.write(
                self.style.ERROR(
                    'Error {}. Al leer el archivo Online {}'.format(
                        e, url)))
            sys.exit(1)

        self.stdout.write(self.style.SUCCESS('Descomprimiendo'))
        tf = tempfile.NamedTemporaryFile()
        tf.write(basedata)
        # descomprimir
        rf = rarfile.RarFile(tf.name)
        for f in rf.infolist():
            self.stdout.write(
                self.style.SUCCESS(
                    'INFO RAR {} {}'.format(
                        f.filename,
                        f.file_size)))
            if f.filename == 'sistvu_test_bkp.sql':
                self.stdout.write(self.style.SUCCESS('Archivo encontrado'))
                tf2 = tempfile.NamedTemporaryFile()
                tf2.write(rf.read(f))

        c = 0

        self.stdout.write(
            self.style.SUCCESS(
                'Procesado OK. {} registros.'.format(c)))
