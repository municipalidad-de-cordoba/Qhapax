"""
Modulo destinado a los ciudadanos en general, se incluye a todo tipo de
organizacines (sociales, comerciales, del estado, etc)
Como regla general, una organización es aquella que tiene (o debería tener) CUIT
"""
from django.db import models
from core.models import Comunicacion
from catastro.models import ParcelaCatastral
from simple_history.models import HistoricalRecords


class TipoOrganizacion(models.Model):
    """ reemplazo de Core.models.TipoOrganizacion """
    nombre = models.CharField(max_length=250)
    descripcion = models.TextField(null=True, blank=True)
    depende_de = models.ForeignKey(
        'self',
        null=True,
        blank=True,
        on_delete=models.SET_NULL)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()

    def __str__(self):
        if self.depende_de is not None:
            return '{} ({})'.format(self.nombre, self.depende_de.nombre)
        else:
            return self.nombre

    class Meta:
        ordering = ['nombre']
        verbose_name_plural = "Tipos de Organizaciones"
        verbose_name = "Tipo de Organización"


class Organizacion(models.Model):
    '''
    Organizaciones externas. ONGs, EMPRESAS, partidos, universidades, oficinas
    de gobiernos
    Esta lista aparece en los pedidos de acceso a la info y otros forms
    públicos para que los interesados se identifiquen
    '''

    nombre = models.CharField(max_length=250)
    tipo = models.ForeignKey(
        TipoOrganizacion,
        null=True,
        on_delete=models.SET_NULL)
    # FIXME en algunos casos serán personas que como no tengo el CUIT, pongo
    # el DNI
    CUIT = models.CharField(max_length=30, null=True, blank=True)
    # se refiere al domicilio legal del responsable o la empresa, no
    # necesariamente una sucursal de ella
    domicilio_calle = models.CharField(max_length=70, null=True, blank=True)
    domicilio_nro = models.CharField(max_length=20, null=True, blank=True)
    domicilio_piso = models.CharField(max_length=20, null=True, blank=True)
    domicilio_dpto = models.CharField(max_length=20, null=True, blank=True)
    domicilio_barrio = models.CharField(max_length=70, null=True, blank=True)
    domicilio_codigo_postal = models.CharField(
        max_length=15, null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()

    def clean_cuit(self):
        ''' limpiar el CUIT de caracteres extraños '''
        if self.CUIT is not None:
            self.CUIT = self.CUIT.strip().replace(
                '-',
                '').replace(
                ' ',
                '').replace(
                '.',
                '').replace(
                ',',
                '')

    def sucursales(self, solo_geo=False):
        if solo_geo:
            sucursales = self.sucursalorganizacion_set.filter(
                parcela__isnull=False).filter(
                parcela__poligono__isnull=False)
        else:
            sucursales = self.sucursalorganizacion_set.all()
        return sucursales

    def ubicaciones_comerciales(self):
        ''' lista de sucursales geolocalizadas correctamente'''
        sucursales_geolocalizadas = self.sucursales(solo_geo=True)
        ret = []
        for sucursal in sucursales_geolocalizadas:
            punto = {
                'latitud': sucursal.parcela.centroide.y,
                'longitud': sucursal.parcela.centroide.x}
            ret.append(punto)

        return ret

    def save(self, *args, **kwargs):
        self.clean_cuit()
        super(Organizacion, self).save(*args, **kwargs)

    def __str__(self):
        if self.CUIT is not None:
            nombre = '{} {}'.format(self.nombre, self.CUIT)
        else:
            nombre = self.nombre

        return nombre

    class Meta:
        ordering = ['nombre']
        verbose_name_plural = "Organizaciones"
        verbose_name = "Organización"


class ComunicacionOrganizacion(Comunicacion):
    objeto = models.ForeignKey(Organizacion, on_delete=models.CASCADE)


class SucursalOrganizacion(models.Model):
    """
    Cada una de las ubicaciones físiscas de una organizacion.
    Luego servirá tambien para habilitaciones de locales comerciales o de otro tipo
    """
    organizacion = models.ForeignKey(Organizacion)
    # por si quisiera indicarse algo más
    nombre = models.CharField(max_length=250, null=True, blank=True)
    # orden de importancia de cada sucursal
    orden = models.PositiveIntegerField(default=100)
    parcela = models.ForeignKey(
        ParcelaCatastral,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        help_text='Use el formato Distrito-Zona-Manzana-Parcela. '
        'Puede buscarlo en https://gobiernoabierto.cordoba.gob.ar/mapas/emap/')
    direccion = models.CharField(max_length=250, null=True, blank=True)
    direccion_nro = models.CharField(max_length=15, null=True, blank=True)
    barrio = models.CharField(max_length=250, null=True, blank=True)
    codigo_postal = models.CharField(max_length=25, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()

    def __str__(self):
        dire = 'S/D' if self.direccion is None else self.direccion
        cuit = '' if self.organizacion.CUIT is None else self.organizacion.CUIT
        return "{} {} {} {} ".format(
            self.organizacion.nombre, cuit, self.nombre, dire)

    class Meta:
        ordering = ['organizacion__nombre']
        verbose_name_plural = "Sucursales de Organizaciones"
        verbose_name = "Sucursal de Organización"


class ComunicacionSucursalOrganizacion(Comunicacion):
    """ Comunicacion mas particular con una sucursal en particular """
    objeto = models.ForeignKey(SucursalOrganizacion, on_delete=models.CASCADE)
