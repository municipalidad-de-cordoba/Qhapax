from django.contrib import admin
from .models import (TipoOrganizacion, Organizacion,
                     SucursalOrganizacion, ComunicacionOrganizacion)
from simple_history.admin import SimpleHistoryAdmin
from .forms import SucursalOrganizacionFormSelect2


class TipoOrganizacionAdmin(SimpleHistoryAdmin):
    def ultimo_editor(self, obj):
        if len(obj.history.all()) > 0:
            return obj.history.all()[0].history_user
        else:
            return ''

    list_display = ["nombre", "depende_de", "ultimo_editor"]
    search_fields = ['nombre']
    list_filter = ['depende_de']


class ComunicacionOrganizacionInline(admin.StackedInline):
    model = ComunicacionOrganizacion
    extra = 1


class OrganizacionAdmin(SimpleHistoryAdmin):
    def ultimo_editor(self, obj):
        if len(obj.history.all()) > 0:
            return obj.history.all()[0].history_user
        else:
            return ''

    list_display = (
        "id",
        "nombre",
        "CUIT",
        "tipo",
        "domicilio_calle",
        "domicilio_nro",
        "domicilio_barrio",
        "ultimo_editor")
    inlines = [ComunicacionOrganizacionInline]
    search_fields = ['nombre', 'CUIT']
    list_filter = ['tipo']


class SucursalOrganizacionAdmin(SimpleHistoryAdmin):
    form = SucursalOrganizacionFormSelect2

    def ultimo_editor(self, obj):
        if len(obj.history.all()) > 0:
            return obj.history.all()[0].history_user
        else:
            return ''

    list_display = (
        'id',
        'organizacion',
        'nombre',
        'orden',
        'direccion',
        'barrio',
        'ultimo_editor')
    search_fields = [
        'organizacion__nombre',
        'organizacion__CUIT',
        'nombre',
        'direccion',
        'barrio']
    list_filter = ['barrio']

    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


admin.site.register(TipoOrganizacion, TipoOrganizacionAdmin)
admin.site.register(Organizacion, OrganizacionAdmin)
admin.site.register(SucursalOrganizacion, SucursalOrganizacionAdmin)
