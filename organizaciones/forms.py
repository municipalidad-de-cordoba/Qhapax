from django import forms
from django_select2.forms import ModelSelect2Widget
from organizaciones.models import Organizacion, SucursalOrganizacion
from desagotesavecinos.forms import ParcelaWidget


class OrganizacionWidget(ModelSelect2Widget):
    model = Organizacion
    search_fields = ['nombre__icontains', 'CUIT__contains']
    max_results = 5

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault(
            'data-placeholder',
            'Ingrese parte del CUIT o nombre')
        self.attrs.setdefault('data-minimum-input-length', 3)
        self.attrs.setdefault('data-width', '25em')

        return super(OrganizacionWidget, self).build_attrs(*args, **kwargs)


class SucursalOrganizacionFormSelect2(forms.ModelForm):
    class Meta:
        model = SucursalOrganizacion

        fields = ['organizacion', 'nombre', 'parcela', 'direccion', 'barrio']
        widgets = {
            'organizacion': OrganizacionWidget,
            'parcela': ParcelaWidget}
