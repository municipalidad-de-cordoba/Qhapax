from django.db import models
from zonas.models import Zonas
from django.contrib.gis.geos import MultiPolygon


class ActividadPublicaQuerySet(models.QuerySet):

    def zonas_coincidentes(self, ids_zonas=[]):
        """
        Retorna las actividades filtradas por los ids de las zonas indicadas
        en el parámetro ids_zonas
        """

        if len(ids_zonas) > 0:
            poligonos_zonas = [
                zona.poligono for zona in Zonas.objects.filter(
                    id__in=ids_zonas)]
            multipoligono = MultiPolygon(poligonos_zonas)
            qs = self.filter(lugar__ubicacion__within=multipoligono)
        else:
            qs = self.none()
        return qs

    def categorias_zonas(self, ids_categorias=[]):
        """
        Retorna las actividades filtradas por los ids de las categorias de las
        zonas indicadas en el parámetro
        """

        if len(ids_categorias) > 0:
            poligonos_zonas = [
                zona.poligono for zona in Zonas.objects.filter(
                    categoria__id__in=ids_categorias)]
            multipoligono = MultiPolygon(poligonos_zonas)
            qs = self.filter(lugar__ubicacion__within=multipoligono)
        else:
            qs = self.none()
        return qs

    def agrupador_categorias_zonas(self, ids_grupos=[]):
        """
        Retorna las actividades filtradas por los ids de los agrupadores de categorias
        """

        if len(ids_grupos) > 0:
            poligonos_zonas = [
                zona.poligono for zona in Zonas.objects.filter(
                    categoria__grupo__id__in=ids_grupos)]
            multipoligono = MultiPolygon(poligonos_zonas)
            qs = self.filter(lugar__ubicacion__within=multipoligono)
        else:
            qs = self.none()
        return qs


class ActividadPublicaManager(models.Manager):

    def get_queryset(self):
        return ActividadPublicaQuerySet(self.model, using=self._db)

    def zonas_coincidentes(self, ids_zonas):
        return self.get_queryset.zonas_coincidentes(ids_zonas)

    def categorias_zonas(self, ids_categorias):
        return self.get_queryset.categorias_zonas(ids_categorias)

    def agrupador_categorias_zonas(self, ids_grupos):
        return self.get_queryset.agrupador_categorias_zonas(ids_grupos)
