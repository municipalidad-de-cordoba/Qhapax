from django.contrib import admin
from .models import (EventoGeneral, AgrupadorActividad, TipoActividad,
                     ParticipanteActividad, ActividadPublica, LugarActividad,
                     OrganizadorEvento, DisciplinaActividad, AudienciaActividad,
                     GrupoPrecioActividad, CategoriaLugarActividad,
                     CircuitoLugarActividad, CategoriaParticipante,
                     CircuitoParticipante, RepeticionActividad, PrecioActividad,
                     FotoLugarActividad, FotoParticipanteActividad)
from simple_history.admin import SimpleHistoryAdmin
from .forms import ActividadPublicaForm
from import_export.admin import ImportExportModelAdmin
from core.admin import QhapaxOSMGeoAdmin


class EventoGeneralAdmin(ImportExportModelAdmin):
    def ultimo_editor(self, obj):
        return obj.history.all()[0].history_user

    search_fields = ['nombre']
    list_display = (
        'id',
        'nombre',
        'publicado',
        'publicado_desde',
        'ultimo_editor')


class RepeticionActividadInline(admin.StackedInline):
    model = RepeticionActividad
    extra = 1


class PrecioActividadInline(admin.StackedInline):
    model = PrecioActividad
    extra = 1


class ActividadPublicaAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    form = ActividadPublicaForm
    fields = (('titulo', 'agrupador'), 'lugar', 'descripcion',
              ('inicia', 'termina'), ('tipos', 'disciplinas', 'audiencias'),
              ('participantes', 'organizador'),
              'publicado', ('mas_info_url', 'orden'),
              ('imagen', 'flyer'))

    def ultimo_editor(self, obj):
        return obj.history.all()[0].history_user

    def get_tipos(self, obj):
        return ', '.join([x.nombre for x in obj.tipos.all()])

    def get_disciplinas(self, obj):
        return ', '.join([x.nombre for x in obj.disciplinas.all()])

    search_fields = [
        'agrupador__nombre',
        'titulo',
        'descripcion',
        'lugar__nombre']
    list_display = (
        'id',
        'orden',
        'agrupador',
        'titulo',
        'lugar',
        'publicado',
        'inicia',
        'termina',
        'mas_info_url',
        'publicado_desde',
        'ultimo_editor',
        'get_tipos',
        'get_disciplinas')
    list_filter = [
        'audiencias',
        'organizador',
        'disciplinas',
        'tipos',
        'agrupador',
        'agrupador__evento']
    ordering = ['-inicia']
    inlines = [RepeticionActividadInline, PrecioActividadInline]


def unificar_disciplinas(modeladmin, request, queryset):
    """ pasar varias disciplinas a una sola """
    short_description = "Unificar Disciplinas seleccionadas y reasignar actividades"

    # el primero encontrado sera el usado para pasar todo allí
    disc_final = None
    for disciplina in queryset:
        if disc_final is None:
            disc_final = disciplina
        else:
            for actividad in disciplina.actividadpublica_set.all():
                disciplina.actividadpublica_set.remove(actividad)
                disc_final.actividadpublica_set.add(actividad)
            disciplina.delete()


class DisciplinaActividadAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    def ultimo_editor(self, obj):
        return obj.history.all()[0].history_user

    search_fields = ['nombre']
    list_display = ('id', 'nombre', 'ultimo_editor')
    actions = [unificar_disciplinas]


def unificar_tipos(modeladmin, request, queryset):
    """ pasar varias disciplinas a una sola """
    short_description = "Unificar Tipos de actividades seleccionadas y reasignar actividades"

    # el primero encontrado sera el usado para pasar todo allí
    tipo_final = None
    for tipo in queryset:
        if tipo_final is None:
            tipo_final = tipo
        else:
            for actividad in tipo.actividadpublica_set.all():
                tipo.actividadpublica_set.remove(actividad)
                tipo_final.actividadpublica_set.add(actividad)
            tipo.delete()


class TipoActividadAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    def ultimo_editor(self, obj):
        return obj.history.all()[0].history_user

    search_fields = ['nombre']
    list_display = ('id', 'nombre', 'ultimo_editor')
    # list_filter = ['evento']
    actions = [unificar_tipos]


class AgrupadorActividadAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    def ultimo_editor(self, obj):
        return obj.history.all()[0].history_user

    search_fields = ['nombre']
    list_display = ('id', 'nombre', 'evento', 'ultimo_editor')
    list_filter = ['evento']


class AudienciaActividadAdmin(SimpleHistoryAdmin):
    def ultimo_editor(self, obj):
        return obj.history.all()[0].history_user
    search_fields = ['nombre']
    list_display = ('nombre', 'ultimo_editor')


class GrupoPrecioActividadAdmin(admin.ModelAdmin):
    list_display = ['nombre']


class FotoLugarActividadInline(admin.StackedInline):
    model = FotoLugarActividad
    extra = 1


def unificar_lugar(modeladmin, request, queryset):
    """ pasar varias disciplinas a una sola """
    short_description = "Unificar lugares seleccionadas y reasignar actividades"

    # el primero encontrado sera el usado para pasar todo allí
    obj_final = None
    for obj in queryset:
        if obj_final is None:
            obj_final = obj
        else:
            for actividad in obj.actividadpublica_set.all():
                obj.actividadpublica_set.remove(actividad)
                obj_final.actividadpublica_set.add(actividad)
            obj.delete()


class CategoriaLugarActividadAdmin(admin.ModelAdmin):
    list_display = ['nombre']
    search_fields = ['nombre']


class CircuitoLugarActividadAdmin(admin.ModelAdmin):
    list_display = ['nombre']
    search_fields = ['nombre']


class LugarActividadAdmin(ImportExportModelAdmin, QhapaxOSMGeoAdmin):
    list_display = [
        'nombre',
        'relevancia',
        'estrellas',
        'direccion',
        'mas_info_url',
        'email',
        'telefono']
    list_filter = ['categorias', 'circuitos', 'estrellas']
    inlines = [FotoLugarActividadInline]
    actions = [unificar_lugar]
    search_fields = ['nombre', 'direccion', 'email', 'telefono']


class FotoParticipanteActividadInline(admin.StackedInline):
    model = FotoParticipanteActividad
    extra = 1


class CategoriaParticipanteAdmin(admin.ModelAdmin):
    list_display = ['nombre']
    search_fields = ['nombre']


class CircuitoParticipanteAdmin(admin.ModelAdmin):
    list_display = ['nombre']
    search_fields = ['nombre']


class ParticipanteActividadAdmin(admin.ModelAdmin):

    def lista_circuitos(self, obj):
        lista = [circuito.nombre for circuito in obj.circuitos.all()]
        return ', '.join(lista)

    def lista_categorias(self, obj):
        lista = [categoria.nombre for categoria in obj.categorias.all()]
        return ', '.join(lista)

    list_display = ['grupo_o_persona', 'mas_info_url', 'email', 'telefono',
                    'lista_circuitos', 'lista_categorias']
    inlines = [FotoParticipanteActividadInline]


admin.site.register(EventoGeneral, EventoGeneralAdmin)
admin.site.register(AgrupadorActividad, AgrupadorActividadAdmin)
admin.site.register(TipoActividad, TipoActividadAdmin)
admin.site.register(ParticipanteActividad, ParticipanteActividadAdmin)
admin.site.register(ActividadPublica, ActividadPublicaAdmin)
admin.site.register(LugarActividad, LugarActividadAdmin)
admin.site.register(OrganizadorEvento)
admin.site.register(DisciplinaActividad, DisciplinaActividadAdmin)
admin.site.register(AudienciaActividad, AudienciaActividadAdmin)
admin.site.register(GrupoPrecioActividad, GrupoPrecioActividadAdmin)
admin.site.register(CategoriaLugarActividad, CategoriaLugarActividadAdmin)
admin.site.register(CircuitoLugarActividad, CircuitoLugarActividadAdmin)
admin.site.register(CategoriaParticipante, CategoriaParticipanteAdmin)
admin.site.register(CircuitoParticipante, CircuitoParticipanteAdmin)
