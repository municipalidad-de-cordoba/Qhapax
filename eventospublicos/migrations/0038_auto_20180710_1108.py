# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-07-10 14:08
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventospublicos', '0037_auto_20180419_1417'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='categorialugaractividad',
            options={'ordering': ['nombre'], 'verbose_name': 'Categoria de lugar', 'verbose_name_plural': 'Categorias de lugar'},
        ),
    ]
