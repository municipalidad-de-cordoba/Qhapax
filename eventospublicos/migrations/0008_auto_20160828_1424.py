# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-08-28 17:24
from __future__ import unicode_literals

from django.db import migrations, models
import versatileimagefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('eventospublicos', '0007_auto_20160825_1037'),
    ]

    operations = [
        migrations.AddField(
            model_name='actividadpublica',
            name='flyer',
            field=versatileimagefield.fields.VersatileImageField(blank=True, null=True, upload_to='imagenes/eventos-publicos/actividades-flyer'),
        ),
        migrations.AddField(
            model_name='agrupadoractividad',
            name='flyer',
            field=versatileimagefield.fields.VersatileImageField(blank=True, null=True, upload_to='imagenes/eventos-publicos/espacios-flyer'),
        ),
        migrations.AddField(
            model_name='eventogeneral',
            name='descripcion',
            field=models.TextField(blank=True, null=True, verbose_name='Descripcion general del evento'),
        ),
        migrations.AddField(
            model_name='eventogeneral',
            name='flyer',
            field=versatileimagefield.fields.VersatileImageField(blank=True, null=True, upload_to='imagenes/eventos-publicos-flyer'),
        ),
        migrations.AddField(
            model_name='lugaractividad',
            name='descripcion',
            field=models.TextField(blank=True, null=True, verbose_name='Descripcion del lugar'),
        ),
        migrations.AlterField(
            model_name='actividadpublica',
            name='descripcion',
            field=models.TextField(blank=True, help_text='Detalle completo', null=True, verbose_name='Descripción de la actividad'),
        ),
        migrations.AlterField(
            model_name='agrupadoractividad',
            name='descripcion',
            field=models.TextField(blank=True, help_text='Detalles generales de las actividades agrupadas', null=True, verbose_name='Descripcion del agrupador'),
        ),
    ]
