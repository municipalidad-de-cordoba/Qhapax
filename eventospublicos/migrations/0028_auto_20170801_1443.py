# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-08-01 17:43
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventospublicos', '0027_auto_20170801_1015'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='historicalparticipante',
            name='history_user',
        ),
        migrations.RemoveField(
            model_name='historicalparticipante',
            name='persona',
        ),
        migrations.RemoveField(
            model_name='participante',
            name='persona',
        ),
        migrations.DeleteModel(
            name='HistoricalParticipante',
        ),
        migrations.DeleteModel(
            name='Participante',
        ),
    ]
