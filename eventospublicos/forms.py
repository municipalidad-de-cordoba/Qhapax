from django import forms
from .models import ActividadPublica


class ActividadPublicaForm(forms.ModelForm):
    class Meta:
        model = ActividadPublica
        fields = '__all__'
        widgets = {'disciplinas': forms.CheckboxSelectMultiple,
                   'audiencias': forms.CheckboxSelectMultiple,
                   'tipos': forms.CheckboxSelectMultiple}
