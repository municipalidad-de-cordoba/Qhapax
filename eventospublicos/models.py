from django.db import models
from django.contrib.gis.db import models as geo_models
from django.utils import timezone
from versatileimagefield.fields import VersatileImageField
from funcionarios.models import Cargo
from ckeditor.fields import RichTextField
from core.models import Moneda
from .managers import ActividadPublicaManager
from zonas.models import Zonas
from django.conf import settings
from simple_history.models import HistoricalRecords
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
import os


class CategoriaLugarActividad(models.Model):
    ''' Tipo o categorización de los lugares
        Por ejemplo, los "espacios culturales" son un agrupador necesario.
        "Museo" puede ser otro ejemplo  '''
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Categoria de lugar'
        verbose_name_plural = 'Categorias de lugar'
        ordering = ['nombre']


class CircuitoLugarActividad(models.Model):
    ''' Circuitos temáticos en los que circunscribimos a los lugares '''
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Circuito temático'
        verbose_name_plural = 'Circuitos temáticos'


class LugarActividad(models.Model):
    '''
    lugar temporal, solo texto por ahora
    '''
    nombre = models.CharField(max_length=250)
    categorias = models.ManyToManyField(
        CategoriaLugarActividad,
        blank=True,
        help_text='Categorías a las que corresponde')
    circuitos = models.ManyToManyField(
        CircuitoLugarActividad,
        blank=True,
        help_text='Circuitos temáticos a los que corresponde')
    direccion = models.CharField(max_length=250, null=True, blank=True)
    descripcion = RichTextField(
        verbose_name='Descripcion del lugar',
        null=True,
        blank=True)
    ubicacion = geo_models.PointField(null=True, blank=True)
    mas_info_url = models.URLField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    telefono = models.CharField(max_length=90, null=True, blank=True)

    # propiedades para luego exportar a tipologías de lugares.
    estrellas = models.PositiveIntegerField(
        default=0,
        help_text='Cantidad de estrellas si es un alojamiento. Cero si NO APICA.')
    relevancia = models.PositiveIntegerField(
        default=1000, help_text='Mientras mas bajo, más importante')
    publicado = models.BooleanField(default=True)

    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    @property
    def fotos(self):
        return self.fotolugaractividad_set.all()

    @property
    def disciplinas(self):
        return DisciplinaActividad.objects.filter(
            actividadpublica__lugar=self).distinct()

    @property
    def zonas(self):
        """
        zonas en las cuales está geolocalizada la actividad
        """
        return Zonas.objects.coincidentes(
            punto=self.ubicacion).values(
            'id', 'nombre', 'categoria')

    class Meta:
        verbose_name_plural = "Lugares"
        ordering = ['nombre']


class FotoLugarActividad(models.Model):
    """ fotos del lugar """
    lugar = models.ForeignKey(LugarActividad, on_delete=models.CASCADE)
    orden = models.PositiveIntegerField(default=100)
    foto = VersatileImageField(upload_to='imagenes/lugares-actividades')

    def basename(self):
        return os.path.basename(self.foto.name)

    class Meta:
        verbose_name_plural = "Fotos de lugares para actividades"
        ordering = ['orden']

    def __str__(self):
        return '{} de {}'.format(self.basename(), self.lugar.nombre)


class OrganizadorEvento(models.Model):
    '''
    quien organiza el evento
    '''
    nombre = models.CharField(max_length=250)
    descripcion = models.TextField(
        verbose_name='Organizador del evento',
        null=True,
        blank=True)
    # para los casos que sea una fuente formal dentro de este gobierno
    oficina_local = models.ForeignKey(
        Cargo, null=True, on_delete=models.SET_NULL, blank=True)
    imagen = VersatileImageField(
        upload_to='imagenes/organizador-eventos',
        null=True,
        blank=True)

    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Organizadores"
        ordering = ['nombre']


class EventoGeneral(models.Model):
    '''
    Evento principal de donde dependen las actividades
    '''
    nombre = models.CharField(max_length=250)
    descripcion = models.TextField(
        verbose_name='Descripcion general del evento',
        null=True,
        blank=True)
    publicado_desde = models.DateTimeField(
        auto_now=False, auto_now_add=True, null=True, blank=True)
    imagen = VersatileImageField(
        upload_to='imagenes/eventos-publicos',
        null=True,
        blank=True)
    flyer = VersatileImageField(
        upload_to='imagenes/eventos-publicos-flyer',
        null=True,
        blank=True)
    publicado = models.BooleanField(default=True)
    organizador = models.ForeignKey(
        OrganizadorEvento,
        null=True,
        on_delete=models.SET_NULL,
        blank=True)
    header_actividades = RichTextField(
        help_text='Info extra para mostrar en actividades',
        null=True,
        blank=True)
    footer_actividades = RichTextField(
        help_text='Info extra para mostrar en actividades',
        null=True,
        blank=True)

    history = HistoricalRecords()

    @property
    def publico(self):
        return self.publicado and self.publicado_desde > timezone.now()

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Eventos Generales"
        ordering = ['nombre']


class AgrupadorActividad(models.Model):
    '''
    Categoría o agrupación general (no es tipo)
    '''
    evento = models.ForeignKey(EventoGeneral, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=250)
    descripcion = models.TextField(
        verbose_name='Descripcion del agrupador',
        help_text="Detalles generales de las actividades agrupadas",
        null=True,
        blank=True)
    imagen = VersatileImageField(
        upload_to='imagenes/eventos-publicos/espacios',
        null=True,
        blank=True)
    flyer = VersatileImageField(
        upload_to='imagenes/eventos-publicos/espacios-flyer',
        null=True,
        blank=True)
    orden = models.PositiveIntegerField(default=100)
    header_actividades = RichTextField(
        help_text='Info extra para mostrar en actividades',
        null=True,
        blank=True)
    footer_actividades = RichTextField(
        help_text='Info extra para mostrar en actividades',
        null=True,
        blank=True)

    history = HistoricalRecords()

    def __str__(self):
        return '{} - {}'.format(self.evento, self.nombre)

    class Meta:
        verbose_name_plural = "Agrupadores de actividades"
        ordering = ['evento', 'nombre']


class TipoActividad(models.Model):
    # Charla, Taller, Musica en vivo Uno a varios
    nombre = models.CharField(max_length=250)

    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Tipos de actividades"
        ordering = ['nombre']


class DisciplinaActividad(models.Model):
    """ pedido por cultura para especificar la rama de actividad """
    nombre = models.CharField(max_length=250)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    @property
    def actividades(self):
        return self.actividadpublica_set.all()

    class Meta:
        verbose_name_plural = "Disciplinas de actividades"
        ordering = ['nombre']


class AudienciaActividad(models.Model):
    """
    propuesta para grupos específicos.
    Permitirá definir actividades para Jóvenes, Turistas u otros
    En el API debería poder filtrarse por esta
    """
    nombre = models.CharField(max_length=250)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Audiencia sugerida de actividades"
        ordering = ['nombre']


class CategoriaParticipante(models.Model):
    ''' Tipo o categorización de los participantes  '''
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Categoria de Participante'
        verbose_name_plural = 'Categorias de participante'
        ordering = ['nombre']


class CircuitoParticipante(models.Model):
    ''' Circuitos temáticos de los participantes '''
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Circuito Participantes'
        verbose_name_plural = 'Circuitos de Participantes'


class ParticipanteActividad(models.Model):
    grupo_o_persona = models.CharField(max_length=250)
    categorias = models.ManyToManyField(
        CategoriaParticipante,
        blank=True,
        help_text='Categorías a las que corresponde',
        related_name='categorias')
    circuitos = models.ManyToManyField(
        CircuitoParticipante,
        blank=True,
        help_text='Circuitos temáticos a los que corresponde',
        related_name='circuitos')

    descripcion = RichTextField(verbose_name='Bio del participante',
                                help_text="Descripción del grupo o persona")
    orden = models.PositiveIntegerField(default=100)

    mas_info_url = models.URLField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    telefono = models.CharField(max_length=90, null=True, blank=True)

    history = HistoricalRecords()

    def __str__(self):
        return self.grupo_o_persona

    class Meta:
        verbose_name_plural = "Participantes de actividades"
        ordering = ['orden']


class FotoParticipanteActividad(models.Model):
    """ fotos del lugar """
    participante = models.ForeignKey(
        ParticipanteActividad,
        on_delete=models.CASCADE,
        related_name='fotos')
    orden = models.PositiveIntegerField(default=100)
    foto = VersatileImageField(
        upload_to='imagenes/foto-participantes-actividad')

    def __str__(self):
        return 'Foto {}{}'.format(self.participante.grupo_o_persona, self.foto)

    class Meta:
        verbose_name_plural = "Fotos de participantes de actividades"
        ordering = ['orden']


class ActividadPublica(models.Model):
    agrupador = models.ForeignKey(
        AgrupadorActividad,
        null=True,
        blank=True,
        on_delete=models.SET_NULL)
    titulo = models.CharField(max_length=250)
    lugar = models.ForeignKey(LugarActividad, null=True, blank=True,
                              on_delete=models.SET_NULL,
                              limit_choices_to={'publicado': True})
    descripcion = RichTextField(verbose_name='Descripción de la actividad',
                                help_text="Detalle completo",
                                null=True, blank=True)
    inicia = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True)
    termina = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True)
    tipos = models.ManyToManyField(TipoActividad, blank=True)
    participantes = models.ManyToManyField(ParticipanteActividad, blank=True)
    publicado_desde = models.DateTimeField(
        auto_now=False, auto_now_add=True, null=True, blank=True)
    publicado = models.BooleanField(default=True)
    imagen = VersatileImageField(
        upload_to='imagenes/eventos-publicos/actividades',
        null=True,
        blank=True)
    flyer = VersatileImageField(
        upload_to='imagenes/eventos-publicos/actividades-flyer',
        null=True,
        blank=True)
    organizador = models.ForeignKey(
        OrganizadorEvento,
        null=True,
        on_delete=models.SET_NULL,
        blank=True)
    disciplinas = models.ManyToManyField(DisciplinaActividad, blank=True)
    audiencias = models.ManyToManyField(AudienciaActividad, blank=True)
    mas_info_url = models.URLField(null=True, blank=True)
    orden = models.PositiveIntegerField(default=100)

    objects = ActividadPublicaManager()

    history = HistoricalRecords()

    @property
    def publico(self):
        return self.publicado and self.publicado_desde > timezone.now()

    @property
    def precios(self):
        return self.precioactividad_set.all()

    @property
    def repeticiones(self):
        return self.repeticionactividad_set.all()

    def clean(self):
        """
        Valida que las fechas de las actividades sean correctas
        """
        if self.termina is not None and self.inicia > self.termina:
            raise ValidationError(
                _('Fecha de inicio mayor a fecha de finalización'),
            )

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name_plural = "Actividades"
        ordering = ['titulo']


class GrupoPrecioActividad(models.Model):
    """ grupo que puede tener diferentes precios de entradas (jubilados, estudianes, etc) """
    nombre = models.CharField(max_length=250)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = "Grupo para precios de actividades"
        verbose_name_plural = "Grupos para precios de actividades"
        ordering = ['nombre']


class PrecioActividad(models.Model):
    """ cada precio para un grupo específico de personas """
    actividad = models.ForeignKey(ActividadPublica)
    grupo = models.ForeignKey(GrupoPrecioActividad)
    moneda = models.ForeignKey(Moneda, default=settings.MONEDA_PK_PRINCIPAL)
    valor = models.DecimalField(max_digits=14, decimal_places=2)
    detalles = models.TextField(
        verbose_name='Consideraciones del precio para este grupo',
        null=True,
        blank=True)

    history = HistoricalRecords()

    def __str__(self):
        return "{} {} {}".format(
            self.grupo.nombre,
            self.moneda.simbolo,
            self.valor)

    class Meta:
        verbose_name = "Precio de la actividad"
        verbose_name_plural = "Precios de la actividad"
        ordering = ['valor']


class RepeticionActividad(models.Model):
    """ cada alerta puede tener repeticiones """
    actividad = models.ForeignKey(ActividadPublica)
    inicia = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        help_text='Fecha y hora de inicio de la repetición')
    termina = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        help_text='Fecha y hora de fin de la repetición')

    history = HistoricalRecords()

    def __str__(self):
        return "Actividad {}: {} - {}".format(
            self.actividad.titulo, self.inicia, self.termina)

    class Meta:
        ordering = ['-inicia']
