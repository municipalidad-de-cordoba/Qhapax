Generador de pdf de actividades públicas
========================================

El pdf generado acá_ muestra las actividades en curso o que iniciarán a partir del día de la fecha.

.. _acá: https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf


Filtros
-------
Se pueden filtrar las distintas actividades especificando parámetros en la url:

-por fecha de inicio: se debe usar el parámetro *desde* con una fecha en formato año-mes-día
	- ejemplo: https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?desde=2017-01-30
	Esto generará un pdf con todas las actividades que iniciaron a partir del 31 de enero de 2017.

-por fecha de finalización: se debe usar el parámetro *hasta* con una fecha en formato año-mes-día
	- ejemplo: https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?hasta=2018-08-30
	Esto generará un pdf con todas las actividades que finalizan el 30 de agosto de 2018(y que inician en el día de la fecha y antes del 30 de agosto de 2018).

-por rango de fechas: retorna las actividades que iniciaron en un rango de fechas o finalizaron en un rango de fecha. Se deben usar los parámetros *empieza_desde* en conjunto con *empieza_hasta* para buscar actividades que iniciaron en el rango de fechas determinado ó usar *finaliza_desde* en conjunto con *finaliza_hasta* para buscar actividades que finalicen en el rango de fechas determinado.
	- ejemplo 1: https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?empieza_desde=2019-07-01&empieza_hasta=2019-08-30
	Esto generará las actividades que iniciaron entre 01 de julio de 2019 y el 30 de agosto de 2019. Si se omite el parámetro *empieza_hasta* se toma por defecto la fecha de hoy. Es decir podría filtrar las actividades que iniciaron desde una fecha anterior al día de hoy hasta hoy inclusive de la siguiente manera:
	- https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?empieza_desde=2019-07-01
	De igual forma, podría omitir el parámetro *empieza_desde* y tomar las actividades que iniciaron el día de hoy hasta una fecha determinada de la siguiente manera:
	- https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?empieza_hasta=2021-07-01

	- ejemplo 2: https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?finaliza_desde=2019-07-01&finaliza_hasta=2019-08-30
	Esto generará las actividades que finalizaron entre 01 de julio de 2019 y el 30 de agosto de 2019. Si se omite el parámetro *finaliza_hasta* se toma por defecto la fecha de hoy. Es decir podría filtrar las actividades que finalizaron desde una fecha anterior al día de hoy hasta hoy inclusive de la siguiente manera:
	- https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?finaliza_desde=2019-07-01
	De igual forma, podría omitir el parámetro *finaliza_desde* y tomar las actividades que finalizaron el día de hoy hasta una fecha determinada de la siguiente manera:
	- https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?finaliza_hasta=2021-07-01

-por tipo: se debe usar el parámetro *tipo* con el id del tipo de la actividad(esta información se puede obtener desde el admin del sitio). Se puede filtrar por varios tipos, separando cada id por comas.
	- ejemplo: https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?tipo=12
	- ejemplo: https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?tipo=2,12

-por disciplina: se debe usar el parámetro *disciplina* con el id de la disciplina(esta información se puede obtener desde el admin del sitio). Se puede filtrar por varias disciplinas, separando cada id por comas.
	- ejemplo: https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?disciplina=15
	- ejemplo: https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?disciplina=2,15

-por agrupador: se debe usar el parámetro *agrupador* con el id del agrupador del agrupador. Se puede filtrar por varios agrupadores, separando cada id por comas.
	- ejemplo: https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?agrupador=18
	- ejemplo: https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?agrupador=2,18

-por zonas: El sistema tiene cargado diferentes zonas de interés. Se puede filtrar las actividades de acuerdo a estas zonas(para ver detalles de las zonas, consulte el siguiente API https://gobiernoabierto.cordoba.gob.ar/api/v2/zonas/). Se debe usar el parámetro *ids_zonas* con el id de la zona. Pueden ser varios id's de zonas separados por comas.
	- ejemplo: https://gobiernoabierto.cordoba.gob.ar/api/actividad-publica/?ids_zonas=2,9

-por categorias de zonas o agrupadores de categorias de zonas: las zonas pueden pertenecer a una categoría en particular que a su vez puede pertener a un agrupador. Por ejemplo, tenemos la categoría 'distrito verde' que pertenece al agrupador 'distritos'. Se debe usar el parámetro *ids_zonas_categorias* con el id de la categoría deseada y *ids_grupos_cat* para los agrupadores de categorias.
	- ejemplo: https://gobiernoabierto.cordoba.gob.ar/api/actividad-publica/?ids_zonas_categorias=2,3
	- ejemplo: https://gobiernoabierto.cordoba.gob.ar/api/actividad-publica/?ids_grupos_cat=1,2

-combinando los distintos parámetros anteriores, se pueden combinar todos los parámetros.
	- ejemplo: https://gobiernoabierto.cordoba.gob.ar/eventos-publicos/actividades.pdf?disciplina=3,1&tipos=5&desde=2017-02-01&hasta=2017-02-02
