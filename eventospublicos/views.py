# -*- coding: utf-8 -*-
from django.shortcuts import render
from wkhtmltopdf.views import PDFTemplateResponse
from .models import ActividadPublica
import bleach
from django.views.decorators.cache import cache_page
from datetime import timedelta, datetime, date
from django.db.models import Q
import django_excel as excel
from django.utils import timezone
from html import unescape
from django.http import Http404


@cache_page(60 * 60)  # 1 h
def proximas_actividades_html(request):
    '''
    Texto con las próximas actividades
    '''
    one_hour_ago = datetime.now() - timedelta(hours=1)
    actividades = ActividadPublica.objects.filter(
        Q(inicia__gte=one_hour_ago) | Q(termina__gte=datetime.now()),
        Q(agrupador__evento__publicado_desde__lte=datetime.now()),
        Q(agrupador__evento__publicado=True),
        Q(publicado_desde__lte=datetime.now()),
        Q(publicado=True))

    context = {'actividades': actividades}
    url = 'eventospublicos/proximas-actividades.html'
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def proximas_actividades_lista(request, filetype):
    '''
    Texto con las próximas actividades
    '''
    one_hour_ago = datetime.now() - timedelta(hours=1)
    actividades = ActividadPublica.objects.filter(
        Q(inicia__gte=one_hour_ago) | Q(termina__gte=datetime.now()),
        Q(agrupador__evento__publicado_desde__lte=datetime.now()),
        Q(agrupador__evento__publicado=True),
        Q(publicado_desde__lte=datetime.now()),
        Q(publicado=True))

    csv_list = []
    csv_list.append(['Titulo',
                     'Descripcion',
                     'Inicia',
                     'Termina',
                     'Lugar',
                     'Email Lugar',
                     'Teléfono Lugar',
                     'Direccion Lugar',
                     'Descripcion Lugar'])
    for act in actividades:
        lugar_nombre = 'Sin cargar' if act.lugar is None else act.lugar.nombre
        lugar_email = '' if act.lugar is None else act.lugar.email
        lugar_telefono = '' if act.lugar is None else act.lugar.telefono
        lugar_direccion = '' if act.lugar is None else act.lugar.direccion
        lugar_descripcion = '' if act.lugar is None else unescape(
            bleach.clean(act.lugar.descripcion, strip=True))

        descr = unescape(bleach.clean(act.descripcion, strip=True))

        inicia = '' if act.inicia is None else timezone.localtime(act.inicia)
        termina = '' if act.termina is None else timezone.localtime(
            act.termina)
        csv_list.append([act.titulo,
                         descr,
                         inicia,
                         termina,
                         lugar_nombre,
                         lugar_email,
                         lugar_telefono,
                         lugar_direccion,
                         lugar_descripcion])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60)  # 1 h
def actividades_lista(request, filetype):
    '''
    Texto con TODAS las actividades publicadas
    '''
    # one_hour_ago = datetime.now() - timedelta(hours=1)
    actividades = ActividadPublica.objects.filter(publicado=True)

    csv_list = []
    csv_list.append(['ID', 'Titulo', 'Inicia', 'Termina',
                     'Lugar', 'Evento', 'Agrupador'])
    for act in actividades:
        lugar_nombre = 'Sin cargar' if act.lugar is None else act.lugar.nombre

        inicia = '' if act.inicia is None else timezone.localtime(act.inicia)
        termina = '' if act.termina is None else timezone.localtime(
            act.termina)

        csv_list.append([act.id,
                         act.titulo,
                         inicia,
                         termina,
                         lugar_nombre,
                         act.agrupador.nombre,
                         act.agrupador.evento.nombre])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 h
def proximas_actividades_pdf(request):
    """
    Pdf SAMI
    """

    queryset = ActividadPublica.objects.filter(
        publicado=True, agrupador__evento__publicado=True)
    context = {}
    # filtros desde y hasta especificados
    if request.GET.get('desde') is not None and request.GET.get(
            'hasta') is not None:
        desde = datetime.strptime(request.GET.get('desde'), "%Y-%m-%d").date()
        hasta = datetime.strptime(request.GET.get('hasta'), "%Y-%m-%d").date()
        # controla que no haya errores en las fechas
        context['desde'] = desde
        context['hasta'] = hasta
        if desde > hasta:
            raise Http404("Fecha mal especificada.")
        else:
            queryset = ActividadPublica.objects.filter(
                Q(inicia__gte=desde),
                Q(termina__lte=hasta)
            )

    # sólo está especificado la fecha de inicio
    elif request.GET.get('desde') is not None:
        desde = datetime.strptime(request.GET.get('desde'), "%Y-%m-%d").date()
        context['desde'] = desde
        context['hasta'] = None
        queryset = queryset.filter(inicia__gte=desde)

    # sólo está especificado la fecha de finalización
    elif request.GET.get('hasta') is not None:
        hasta = datetime.strptime(request.GET.get('hasta'), "%Y-%m-%d").date()
        context['desde'] = None
        context['hasta'] = hasta
        queryset = queryset.filter(termina__lte=hasta)

    else:
        # por defecto devolvemos las actividades que iniciaron hace una hora en
        # adelante y las que no finalizaron todavía
        context['desde'] = datetime.now().date()
        context['hasta'] = None
        queryset = queryset.filter(  # inicia__gte=datetime.now().date())
            Q(inicia__gte=datetime.now().date()) |
            Q(termina__gte=datetime.now().date())
        )

    # rango de fechas en inicio de actividades
    if request.GET.get('empieza_desde') is not None and request.GET.get(
            'empieza_hasta') is not None:
        empieza_desde = datetime.strptime(
            request.GET.get('empieza_desde'), "%Y-%m-%d").date()
        empieza_hasta = datetime.strptime(
            request.GET.get('empieza_hasta'), "%Y-%m-%d").date()
        context['desde'] = empieza_desde
        context['hasta'] = empieza_hasta

        if empieza_desde > empieza_hasta:
            raise Http404('Fecha mal especificada.')
        else:
            queryset = queryset.filter(
                inicia__range=[
                    empieza_desde,
                    empieza_hasta])

    elif request.GET.get('empieza_desde') is not None:
        empieza_desde = datetime.strptime(
            request.GET.get('empieza_desde'), "%Y-%m-%d").date()
        hoy = datetime.now().date()
        context['desde'] = empieza_desde
        context['hasta'] = hoy
        if empieza_desde > hoy:
            raise Http404('Fecha mal especificada.')
        else:
            queryset = queryset.filter(inicia__range=[empieza_desde, hoy])

    elif request.GET.get('empieza_hasta') is not None:
        empieza_hasta = datetime.strptime(
            request.GET.get('empieza_hasta'), "%Y-%m-%d").date()
        hoy = datetime.now().date()
        context['desde'] = hoy
        context['hasta'] = empieza_hasta
        if hoy > empieza_hasta:
            raise Http404('Fecha mal especificada.')
        else:
            queryset = queryset.filter(inicia__range=[hoy, empieza_hasta])

    # rango de fechas en finalización de actividades
    if request.GET.get('finaliza_desde') is not None and request.GET.get(
            'finaliza_hasta') is not None:
        finaliza_desde = datetime.strptime(
            request.GET.get('finaliza_desde'), "%Y-%m-%d").date()
        finaliza_hasta = datetime.strptime(
            request.GET.get('finaliza_hasta'), "%Y-%m-%d").date()
        context['desde'] = finaliza_desde
        context['hasta'] = finaliza_hasta

        if finaliza_desde > finaliza_hasta:
            raise Http404('Fecha mal especificada.')
        else:
            queryset = queryset.filter(
                termina__range=[
                    finaliza_desde,
                    finaliza_hasta])

    elif request.GET.get('finaliza_desde') is not None:
        finaliza_desde = datetime.strptime(
            request.GET.get('finaliza_desde'), "%Y-%m-%d").date()
        hoy = datetime.now().date()
        context['desde'] = finaliza_desde
        context['hasta'] = hoy
        if finaliza_desde > hoy:
            raise Http404('Fecha mal especificada.')
        else:
            queryset = queryset.filter(inicia__range=[finaliza_desde, hoy])

    elif request.GET.get('finaliza_hasta') is not None:
        finaliza_hasta = datetime.strptime(
            request.GET.get('finaliza_hasta'), "%Y-%m-%d").date()
        hoy = datetime.now().date()
        context['desde'] = hoy
        context['hasta'] = finaliza_hasta
        if hoy > finaliza_hasta:
            raise Http404('Fecha mal especificada.')
        else:
            queryset = queryset.filter(inicia__range=[hoy, finaliza_hasta])

    if request.GET.get('disciplina') is not None:
        disciplinas_id = request.GET.get('disciplina').split(',')
        queryset = queryset.filter(disciplinas__id__in=disciplinas_id)

    if request.GET.get('tipo') is not None:
        tipos_id = request.GET.get('tipo').split(',')
        queryset = queryset.filter(tipos__id__in=tipos_id)

    if request.GET.get('agrupador') is not None:
        agrupadores = request.GET.get('agrupador').split(',')
        queryset = queryset.filter(agrupador__id__in=agrupadores)

    titulo = 'Actividades Públicas'
    template = 'eventospublicos/actividades_pdf.html'
    context['actividades'] = queryset.order_by('inicia')
    context['titulo'] = titulo
    response = PDFTemplateResponse(
        request=request,
        template=template,
        context=context,
        show_content_in_browser=True,
        header_template='eventospublicos/actividades_header.html',
        footer_template='eventospublicos/actividades_footer.html',
        cmd_options={
            'page-size': 'A4',
            'title': 'Actividades Públicas Municipalidad de Córdoba',
            'margin-top': 25,
            'margin-bottom': 25,
            'margin-left': 15,
            'margin-right': 15,
            'footer-line': None})
    return response
