#!/usr/bin/python
from django.core.management.base import BaseCommand
from django.db import transaction
from django.contrib.gis.geos import Point
from eventospublicos.models import LugarActividad


class Command(BaseCommand):
    help = """Comando para migrar coordenadas a un campo Point en lugares de eventis públicos"""

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando migracion...'))
        lugares = LugarActividad.objects.all()
        for lugar in lugares:
            ubicacion = Point(float(lugar.longitud), float(lugar.latitud))
            lugar.ubicacion = ubicacion
            lugar.save()
        self.stdout.write(self.style.SUCCESS('FIN.'))
        self.stdout.write(
            self.style.SUCCESS(
                'Total de lugares: {}'.format(
                    lugares.count())))
