#!/usr/bin/python
from django.core.management.base import BaseCommand
from django.db import transaction
from eventospublicos.models import (ActividadPublica, AgrupadorActividad,
                                    EventoGeneral)


class Command(BaseCommand):
    help = """Fix. Actividades Feria del libro 2018 mal cargadas como 2017"""

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando fix...'))
        feria_2017 = EventoGeneral.objects.get(pk=16)
        feria_2018 = EventoGeneral.objects.get(pk=34)
        # desde el ID 4451 en adelante
        actividades_to_fix = ActividadPublica.objects.filter(
            agrupador__evento=feria_2017, id__gte=4451)
        self.stdout.write(
            self.style.ERROR(
                'Actividades a corregir: {}'.format(
                    len(actividades_to_fix))))
        for actividad in actividades_to_fix:
            self.stdout.write(
                self.style.WARNING(
                    'Revisando: {} {}'.format(
                        actividad.id,
                        actividad.titulo)))
            agrupador_2017 = actividad.agrupador
            agrupador_2018, created = AgrupadorActividad.objects.get_or_create(
                evento=feria_2018, nombre=agrupador_2017.nombre)
            if created:
                self.stdout.write(
                    self.style.SUCCESS(
                        'Agrupador creado: {} {}'.format(
                            agrupador_2018.id,
                            agrupador_2018.nombre)))
            else:
                self.stdout.write(
                    self.style.WARNING(
                        'Agrupador ya existe: {} {}'.format(
                            agrupador_2018.id,
                            agrupador_2018.nombre)))

            actividad.agrupador = agrupador_2018
            actividad.save()

        self.stdout.write(self.style.SUCCESS('FIN.'))
