from rest_framework import viewsets
from .serializers import (ActividadesLugarDisciplinaSerializer,
                          LugarActividadGeoSerializer,
                          CategoriaLugarActividadSerializer,
                          CircuitoLugarActividadSerializer,
                          RepeticionActividadSerializer)
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from api.pagination import DefaultPagination
from eventospublicos.models import (LugarActividad, CategoriaLugarActividad,
                                    CircuitoLugarActividad, RepeticionActividad)
from django.db.models import Q
from datetime import timedelta
from django.utils import timezone


class ActividadesLugarDisciplinaViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = ActividadesLugarDisciplinaSerializer
    pagination_class = DefaultPagination
    queryset = LugarActividad.objects.filter(publicado=True)


class LugarActividadGeoViewSet(viewsets.ModelViewSet):
    """
    Los lugares donde se realizan las actividades.
    Parámetros:
    categorias_id -- Id de la categoria/s (pueden ser varias separadas por coma) de lugares a filtrar
    q -- filtro para buscar lugares por nombre o descripción
    audiencia_id -- filtro para listar lugares donde allá actividades
        (futuras, a realizarse) para esta audiencia.
    circuito_id -- id del circuito(pueden ser varios separados por comas) al que pertenece el lugar.
    estrellas -- 1 a 5 cantidad de estrellas (cero si no aplica)
    orden -- puede tomar el valor "nombre". Si se aplica, los lugares se ordenan alfabeticamente. Si no se usa se ordena por relevancia como es predeterminado
    """
    serializer_class = LugarActividadGeoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = LugarActividad.objects.filter(publicado=True)

        # filtro por texto
        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(descripcion__icontains=q) |
                Q(nombre__icontains=q))

        # filtro por tipo de audiencia (solo se sabe en las actividades)
        audiencia_id = self.request.query_params.get('audiencia_id', None)
        if audiencia_id is not None:
            audiencias_id = audiencia_id.split(",")
            now = timezone.now()
            one_hour_ago = now - timedelta(hours=1)
            queryset = queryset.filter(Q(actividadpublica__audiencias__in=audiencias_id), Q(
                actividadpublica__inicia__gte=one_hour_ago) | Q(actividadpublica__termina__gte=now), )
            # queda duplicado una vez por cada actividad
            queryset = queryset.distinct()

        # filtro por tipo de audiencia
        categorias_id = self.request.query_params.get('categorias_id', None)
        if categorias_id is not None:
            categorias_id = categorias_id.split(",")
            queryset = queryset.filter(categorias__in=categorias_id)

        # estrellas
        estrellas = self.request.query_params.get('estrellas', None)
        if estrellas is not None:
            queryset = queryset.filter(estrellas=int(estrellas))

        # fitro por circuitos
        circuito_id = self.request.query_params.get('circuito_id', None)
        if circuito_id is not None:
            circuito_id = circuito_id.split(",")
            queryset = queryset.filter(
                circuitos__id__in=circuito_id).distinct()

        orden = self.request.query_params.get('orden', None)
        if orden == 'nombre':
            queryset = queryset.order_by('nombre')
        else:
            queryset = queryset.order_by('relevancia')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CategoriaLugarActividadViewSet(viewsets.ModelViewSet):
    serializer_class = CategoriaLugarActividadSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CategoriaLugarActividad.objects.all()
        return queryset.order_by('nombre')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CircuitoLugarActividadViewSet(viewsets.ModelViewSet):
    serializer_class = CircuitoLugarActividadSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CircuitoLugarActividad.objects.all()
        return queryset.order_by('nombre')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class RepeticionActividadViewSet(viewsets.ModelViewSet):
    """
    Con el parametro 'q' se puede filtrar por id de actividad publica.
    Ej: API/?q=4
    """

    serializer_class = RepeticionActividadSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = RepeticionActividad.objects.all()

        # filtro por id de actividad publica
        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(actividad__id__icontains=q)

        return queryset.order_by('id')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
