from rest_framework import serializers
from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from eventospublicos.models import (ActividadPublica, DisciplinaActividad,
                                    LugarActividad, CategoriaLugarActividad,
                                    CircuitoLugarActividad, RepeticionActividad)
from api.serializers import (ActividadPublicaSerializer,
                             FotoLugarActividadSerializer,
                             CategoriaLugarActividadSerializer,
                             CircuitoLugarActividadSerializer)


class SimplerActividadSerializer(ActividadPublicaSerializer):
    class Meta:
        model = ActividadPublica
        fields = ('id', 'agrupador', 'titulo', 'imagen', 'flyer',
                  'descripcion', 'inicia', 'termina',
                  'organizador', 'tipos', 'audiencias',
                  'participantes', 'mas_info_url',
                  'precios', 'repeticiones')


class DisciplinaActividadSerializer(CachedSerializerMixin):
    actividades = SimplerActividadSerializer(many=True, read_only=True)

    class Meta:
        model = DisciplinaActividad
        fields = ['nombre', 'actividades']


class ActividadesLugarDisciplinaSerializer(CachedSerializerMixin):
    disciplinas = DisciplinaActividadSerializer(many=True)
    latitud = serializers.SerializerMethodField()
    longitud = serializers.SerializerMethodField()

    def get_latitud(self, obj):
        # se crea para mantener compatibilidad hacia atras. Antes habías campos
        # float en lugar de un PointField
        return obj.ubicacion.coords[1] if obj.ubicacion is not None else ''

    def get_longitud(self, obj):
        # se crea para mantener compatibilidad hacia atras. Antes habías campos
        # float en lugar de un PointField
        return obj.ubicacion.coords[0] if obj.ubicacion is not None else ''

    class Meta:
        model = LugarActividad
        fields = ('id', 'nombre', 'direccion',
                  'email', 'telefono',
                  'descripcion', 'latitud',
                  'longitud', 'mas_info_url',
                  'disciplinas')


class LugarActividadGeoSerializer(
        GeoFeatureModelSerializer,
        CachedSerializerMixin):
    fotos = FotoLugarActividadSerializer(many=True, read_only=True)
    categorias = CategoriaLugarActividadSerializer(many=True, read_only=True)
    circuitos = CircuitoLugarActividadSerializer(many=True, read_only=True)

    class Meta:
        model = LugarActividad
        geo_field = 'ubicacion'
        fields = ('id', 'nombre', 'direccion',
                  'email', 'telefono',
                  'descripcion', 'mas_info_url',
                  'estrellas', 'relevancia',
                  'fotos', 'categorias', 'circuitos')


class CategoriaLugarActividadSerializer(CachedSerializerMixin):

    class Meta:
        model = CategoriaLugarActividad
        fields = ('id', 'nombre')


class CircuitoLugarActividadSerializer(CachedSerializerMixin):

    class Meta:
        model = CircuitoLugarActividad
        fields = ('id', 'nombre')


class RepeticionActividadSerializer(CachedSerializerMixin):
    actividad = SimplerActividadSerializer()

    class Meta:
        model = RepeticionActividad
        fields = ['id', 'actividad', 'inicia', 'termina']
