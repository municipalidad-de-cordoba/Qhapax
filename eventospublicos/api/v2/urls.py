from django.conf.urls import url, include
from rest_framework import routers
from .views import (ActividadesLugarDisciplinaViewSet,
                    LugarActividadGeoViewSet, CategoriaLugarActividadViewSet,
                    CircuitoLugarActividadViewSet, RepeticionActividadViewSet)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'lugar-disciplina',
    ActividadesLugarDisciplinaViewSet,
    base_name='lugar-disciplina-v2')
router.register(
    r'lugar-actividad-geo',
    LugarActividadGeoViewSet,
    base_name='lugar-actividad-geo-v2')

router.register(
    r'categoria-lugar',
    CategoriaLugarActividadViewSet,
    base_name='categoria-lugar')
router.register(
    r'circuito-lugar',
    CircuitoLugarActividadViewSet,
    base_name='circuito-lugar')

router.register(
    r'repeticion-disciplina',
    RepeticionActividadViewSet,
    base_name='repeticion-disciplina-v2')

urlpatterns = [
    url(r'^', include(router.urls)),
]
