from django.conf.urls import url

from . import views

urlpatterns = [url(r'^proximas-actividades-publicas.html$',
                   views.proximas_actividades_html,
                   name='eventospublicos.proximas-actividades-html'),
               url(r'^proximas-actividades-publicas.(?P<filetype>csv|xls)$',
                   views.proximas_actividades_lista,
                   name='eventospublicos.proximas-actividades-lista'),
               url(r'^actividades-publicas.(?P<filetype>csv|xls)$',
                   views.actividades_lista,
                   name='eventospublicos.actividades-lista'),
               url(r'^actividades.pdf$',
                   views.proximas_actividades_pdf,
                   name='eventospublicos.actividades-pdf'),
               ]
