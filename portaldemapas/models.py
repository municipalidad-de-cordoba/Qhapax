from django.db import models
from portaldedatos.models import Recurso
from versatileimagefield.fields import VersatileImageField


class PortalDeMapas(models.Model):
    '''
    Cada uno de los portales de mapas de un gobierno.
    Un portal es una colección de capas GIS
    '''
    titulo = models.CharField(max_length=90)
    descripcion = models.TextField(blank=True, null=True)
    imagen = VersatileImageField(upload_to='imagenes/portal-de-mapas', null=True, blank=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'Portal de mapas {}'.format(self.titulo)

    class Meta:
        verbose_name_plural = "Portales de mapas"
        verbose_name = "Portal de mapas"
        ordering = ['titulo']


class CategoriaPortalMapas(models.Model):
    '''
    Categorías de un portal de mapas en particular
    '''
    portal = models.ForeignKey(PortalDeMapas, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=95)
    descripcion = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        unique_together = (("portal", "nombre"),)
        ordering = ['nombre']
        verbose_name_plural = "Categorías de mapas"
        verbose_name = "Categoría de mapas"


class CapaMapaPortal(models.Model):
    '''
    Una capa de datos GIS específica.
    Como el portal de datos ya tiene como recursos a estos archivos solo los linkeamos.
    '''
    # identifica a este dato dentro de una categoria, portal y gobierno
    categoria = models.ForeignKey(CategoriaPortalMapas, on_delete=models.CASCADE, blank=True)
    recurso = models.ForeignKey(Recurso, blank=True)
    titulo = models.CharField(max_length=90)
    descripcion = models.CharField(max_length=200, null=True, blank=True)
    publicado = models.BooleanField(default=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name_plural = "Capas de mapas"
        verbose_name = "Capa de mapas"
        ordering = ['titulo']