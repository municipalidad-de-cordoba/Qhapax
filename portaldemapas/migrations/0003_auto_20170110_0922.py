# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-01-10 12:22
from __future__ import unicode_literals

from django.db import migrations, models
import versatileimagefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('portaldemapas', '0002_auto_20161220_0929'),
    ]

    operations = [
        migrations.AddField(
            model_name='capamapaportal',
            name='publicado',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='portaldemapas',
            name='imagen',
            field=versatileimagefield.fields.VersatileImageField(blank=True, null=True, upload_to='imagenes/portal-de-mapas'),
        ),
    ]
