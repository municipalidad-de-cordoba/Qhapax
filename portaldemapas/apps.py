from django.apps import AppConfig


class PortaldemapasConfig(AppConfig):
    name = 'portaldemapas'
