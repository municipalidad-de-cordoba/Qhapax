from django.contrib import admin
from .models import *


class PortalDeMapasAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'imagen')
    search_fields = ['titulo', 'descripcion']


class CategoriaPortalMapasAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'portal')
    search_fields = ['nombre', 'descripcion']
    list_filter = ('portal', )


class CapaMapaPortalAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'categoria', 'recurso')
    search_fields = ['titulo', 'categoria__nombre', 'descripcion']
    list_filter = ('categoria', )


admin.site.register(PortalDeMapas, PortalDeMapasAdmin)
admin.site.register(CategoriaPortalMapas, CategoriaPortalMapasAdmin)
admin.site.register(CapaMapaPortal, CapaMapaPortalAdmin)