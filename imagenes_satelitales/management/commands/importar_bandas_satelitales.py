#!/usr/bin/python
"""
Importar bandas de imagenes satelitales
"""
from django.core.management.base import BaseCommand
import sys
import csv
from imagenes_satelitales.models import (BandaImagenSatelital, ImagenSatelital,
                                         TipoBandaImagen)
from django.core.files import File
import tempfile
import requests
from PIL import Image


class Command(BaseCommand):
    help = """Comando para Importar bandas de imágenes satelitales"""

    def add_arguments(self, parser):
        parser.add_argument(
            '--path',
            type=str,
            help='Path del archivo KML local')

    # es muy largo y se corta por fallas de internet @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación de bandas '
                                             'de imágenes satelitales'))

        path = options['path']

        try:
            csv_archivo = open(path)
        except Exception as e:
            self.stdout.write(self.style.ERROR('CSV Error: {}'.format(e)))
            sys.exit(1)

        dato = csv.reader(csv_archivo)
        datos = next(dato)  # omite la primer linea

        errores = []
        imagenes_nuevas = 0
        imagenes_repetidas = 0
        for reg in dato:
            try:
                img = ImagenSatelital.objects.get(id=reg[1])
                banda = TipoBandaImagen.objects.get(id=reg[2])
                imagen, created = BandaImagenSatelital.objects.get_or_create(
                    id=reg[0], imagen=img, banda=banda)
            except Exception as e:
                self.stdout.write(
                    self.style.ERROR(
                        'No se pudo crear la imagen: {}'.format(e)))
                print(reg[2])
                # sys.exit(1)

            if created:
                url = reg[3]

                # se conecta al host de la imagen
                self.stdout.write(
                    self.style.SUCCESS(
                        'Descargando imagen {}'.format(url)))
                contenido = requests.get(url).content  # es tipo byte

                # creamos un archivo temporal con la imagen
                tf = tempfile.NamedTemporaryFile()
                tf.write(contenido)

                # archivo definitivo en nuestro servidor
                img = File(tf)

                # controlamos que la imagen sea valida
                try:
                    foto = Image.open(img)
                    nombre = "{}_{}".format(reg[0], url.split('/')[-1])
                    imagen.imagen_banda.save(nombre, img)
                except Exception as e:
                    err = 'ERROR con el formato de la IMAGEN:{}'.format(url)
                    self.stdout.write(self.style.ERROR(err))
                    errores.append(err)
                    imagen.delete()
                    continue

                # obtuvimos una imagen correcta, se guarda en el sistema
                imagen.save()
                imagenes_nuevas += 1
            else:
                imagenes_repetidas += 1
                self.stdout.write(
                    self.style.SUCCESS('Omitiendo descargar imagen'))

            self.stdout.write(
                self.style.SUCCESS(
                    'IMAGENES NUEVAS: {} Repetidas: {}'.format(
                        imagenes_nuevas,
                        imagenes_repetidas)))

        if len(errores) > 0:
            self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR'))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))

        else:
            self.stdout.write(self.style.SUCCESS('SIN ERRORES'))

        self.stdout.write(
            self.style.SUCCESS(
                'IMAGENES NUEVAS: {}'.format(imagenes_nuevas)))
        self.stdout.write(
            self.style.SUCCESS(
                'IMAGENES REPETIDAS: {}'.format(imagenes_repetidas)))
        self.stdout.write(self.style.SUCCESS('FIN'))
