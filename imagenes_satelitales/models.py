from django.db import models
from django.contrib.gis.db import models as models_geo
from versatileimagefield.fields import VersatileImageField


class Satelite(models.Model):
    ''' cada satelite que provee imagenes '''
    nombre = models.CharField(max_length=35)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Satelites'


class SensorSatelite(models.Model):
    ''' cada sensor en un satelite '''
    nombre = models.CharField(max_length=75)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Sensores en Satelite'
        verbose_name_plural = 'Sensores en Satelites'


class ImagenSatelital(models.Model):
    ''' imagenes satelitales de la ciudad. '''

    nombre = models.CharField(max_length=90)
    satelite = models.ForeignKey(Satelite, null=True, blank=True)
    sensor = models.ForeignKey(SensorSatelite, null=True, blank=True)
    escena_id = models.CharField(max_length=90, blank=True, null=True)
    fecha_captura = models.DateTimeField(blank=True, null=True)
    publicado = models.BooleanField(default=True)
    imagen = VersatileImageField(upload_to='imagenes/satelitales')
    cobertura = models_geo.PolygonField(null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Imágenes satelitales'


class TipoBandaImagen(models.Model):
    ''' cada uno de los tipos de bandas (Rojo, verde, infrarrojo, etc) '''
    nombre = models.CharField(max_length=35)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Tipos de bandas de imágenes satelitales'


class BandaImagenSatelital(models.Model):
    banda = models.ForeignKey(TipoBandaImagen)
    imagen = models.ForeignKey(ImagenSatelital)
    imagen_banda = VersatileImageField(upload_to='imagenes/satelitales-bandas')

    def __str__(self):
        return '{} {}'.format(self.banda.nombre, self.imagen.nombre)

    class Meta:
        verbose_name_plural = 'Bandas de imágenes satelitales'
