from django.apps import AppConfig


class ImagenesSatelitalesConfig(AppConfig):
    name = 'imagenes_satelitales'
