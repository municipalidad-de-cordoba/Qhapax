from django.contrib import admin
from core.admin import QhapaxOSMGeoAdmin
from .models import (ImagenSatelital, TipoBandaImagen, BandaImagenSatelital,
                     SensorSatelite, Satelite)


class ImagenSatelitalAdmin(QhapaxOSMGeoAdmin):
    list_display = [
        'nombre',
        'publicado',
        'id',
        'satelite',
        'sensor',
        'escena_id',
        'fecha_captura']
    search_fields = ['nombre', 'id', 'satelite', 'sensor', 'escena_id']
    list_filter = ['publicado', 'satelite', 'sensor']


class TipoBandaImagenAdmin(admin.ModelAdmin):
    list_display = ["nombre", "id"]
    search_fields = ['nombre', 'id']


class SateliteAdmin(admin.ModelAdmin):
    list_display = ["nombre", "id"]
    search_fields = ['nombre', 'id']


class SensorSateliteAdmin(admin.ModelAdmin):
    list_display = ["nombre", "id"]
    search_fields = ['nombre', 'id']


class BandaImagenSatelitalAdmin(admin.ModelAdmin):
    list_display = ["banda", "imagen", "id"]
    search_fields = ['nombre', 'id']
    list_filter = ['banda']


admin.site.register(ImagenSatelital, ImagenSatelitalAdmin)
admin.site.register(TipoBandaImagen, TipoBandaImagenAdmin)
admin.site.register(BandaImagenSatelital, BandaImagenSatelitalAdmin)
admin.site.register(SensorSatelite, SensorSateliteAdmin)
admin.site.register(Satelite, SateliteAdmin)
