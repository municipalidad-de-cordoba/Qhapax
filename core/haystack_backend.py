from haystack.backends.elasticsearch_backend import ElasticsearchSearchBackend
from haystack.backends.elasticsearch_backend import ElasticsearchSearchEngine
 
class SpanishESBackend(ElasticsearchSearchBackend):
 
    DEFAULT_SETTINGS = { "settings": {
                "analysis": {
                  "filter": {
                    "english_stop": {
                      "type":       "stop",
                      "stopwords":  "_english_"
                    },
                    "light_english_stemmer": {
                      "type":       "stemmer",
                      "language":   "light_english"
                    },
                    "english_possessive_stemmer": {
                      "type":       "stemmer",
                      "language":   "english"
                    },

                    "light_spanish_stemmer": {
                      "type":       "stemmer",
                      "language":   "light_spanish"
                    },
                    "spanish_possessive_stemmer": {
                      "type":       "stemmer",
                      "language":   "spanish"
                    }
                  },
                    "analyzer": {
                        "case_insensitive_sort": {
                            "tokenizer": "keyword",
                            "filter":  [ "lowercase" ]
                        },
                        "english": {
                          "tokenizer":  "standard",
                          "filter": [
                            "english_possessive_stemmer",
                            "lowercase",
                            "english_stop",
                            "light_english_stemmer",
                            "asciifolding"
                          ]
                        },
                        "spanish": {
                          "tokenizer":  "standard",
                          "filter": [
                            "spanish_possessive_stemmer",
                            "lowercase",
                            "light_spanish_stemmer",
                          ]
                        }

                    }
                }
            } }

 
class SpanishElasticsearchSearchEngine(ElasticsearchSearchEngine):
    backend = SpanishESBackend
