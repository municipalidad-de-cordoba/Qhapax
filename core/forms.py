from django_select2.forms import ModelSelect2Widget
from .models import Persona


class PersonaWidget(ModelSelect2Widget):
    model = Persona
    search_fields = [
        'nombre__icontains',
        'apellido__icontains',
        'unique_id__contains']
    max_results = 5

    def label_from_instance(self, obj):
        return '{} {} ({})'.format(obj.nombre, obj.apellido, obj.unique_id)

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault(
            'data-placeholder',
            'Ingrese parte del DNI o nombre')
        self.attrs.setdefault('data-minimum-input-length', 3)
        self.attrs.setdefault('data-width', '25em')

        return super(PersonaWidget, self).build_attrs(*args, **kwargs)
