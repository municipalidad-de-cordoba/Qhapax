.. _personas_index:

Manejo de Personas en Qhapax
============================

En el núcleo de la plataforma se incluye la administración de datos de personas. Es posible cargar en el sistema datos
de personas que pueden después conectarse como `funcionarios <../funcionarios/README.rst>`__ u otras entidades en
este sistema.

Al agregar una nueva persona al sistema es necesario cargar los campos:

 - Nombre: Todos los nombres de la persona.
 - Apellido: Todos los apellidos de la persona.
 - Nombre público: El sistema generará automáticamente como nombre al público una combinación del primer nombre + primer apellido salvo que se cargue algo diferente en este campo opcional. Por ejemplo si la persona se llama Eufrasio Jose / Venegas Garcia y todos los conocen como *Jose García* entonces puede ponerse esto en *Nombre público* y no se mostrará el valor predeterminado *Eufrasio Venegas*.
 - Tipo de Documento: DNI, CUIT (cuando lo tengamos), Libreta enrolamiento, etc
 - Documento: el número del documento según el tipo que se haya elegido.
 - Foto: si la persona que se esta cargando es funcionario es muy importante que esté cargada ya que se mostrará en la web publica. El *Primary Point of Interes* mostrado es el punto mas importante de la foto. Predeterminado es en el centro de la foto pero si la persona esta a la derecha de la imagen subida es buena idea hacer un click sobre la cara del funcionario en la foto. Esto indicará el *punto de interes* de la foto para que cuando se creen *thumbs* (miniaturas) o se hagan recortes automatizados de la imágenes estas se hagan sin arruinar la imagen resultante.
 - Fecha de nacimiento
 - Genero
 - HTML: Es un texto enriquecido (permite negrita, cursiva, links, etc) que se va a colocar al público cuando se haga referencia a esa persona. Si es un funcionario será el texto que se verá en su ficha personal.

Vías de comunicación con cada persona
-------------------------------------

Con cada persona (más abajo en el mismo formulario) se pueden cargar vías de comunicación públicas o privadas.
Pueden cargarse aquí teléfonos y emails pero tambien sitios webs, blogs, redes sociales o linkedin de cada persona.
En todos los casos (se pueden cargar más de una vía de comunicación) se puede indicar si la vía de comunicación es privada.
En caso de que no lo sea se publicará en la web. Si es un funcionario automáticamente estarán en su ficha y si se incluyen
redes sociales estas se desplegarán.

.. image:: /docs/img/redes-en-ficha.png

Relacionado: `Administrar Funcionarios <funcionarios_index>`_.
