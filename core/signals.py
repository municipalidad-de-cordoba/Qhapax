from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.contrib.redirects.models import Redirect
from django.contrib.sites.models import Site


@receiver(pre_save, dispatch_uid="001")
def create_redirect(sender, instance, **kwargs):
    """
    si un objeto modificado tiene un campo `slug` y un método `get_absolute_url`
    se verifica si la url cambió y en tal caso se crea una redirección
    para mantener el soporte de la url original.
    """
    if not getattr(
        instance,
        'slug',
        None) or not getattr(
        instance,
        'get_absolute_url',
            None):
        return
    try:
        o = sender.objects.get(id=instance.id)
        old_path = o.get_absolute_url()
        new_path = instance.get_absolute_url()
        if old_path != new_path:

            # actualizar todas las redirecciones previa que apuntaban a
            # old_path
            for redirect in Redirect.objects.filter(new_path=old_path):
                redirect.new_path = new_path

                # eliminar la que redirijan a sí mismo
                if redirect.new_path == redirect.old_path:
                    redirect.delete()
                else:
                    redirect.save()

            # crear la nueva redireccion
            Redirect.objects.create(
                site=Site.objects.get_current(),
                old_path=old_path,
                new_path=new_path)
    except sender.DoesNotExist:
        pass
