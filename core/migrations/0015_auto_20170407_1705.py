# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-04-07 20:05
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_tipoorganizacion'),
    ]

    operations = [
        migrations.CreateModel(
            name='MultimediaObj',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', models.PositiveIntegerField(choices=[(10, 'Imagen'), (20, 'audio'), (30, 'Video')], default=10)),
            ],
        ),
        migrations.AddField(
            model_name='cdngenerico',
            name='descripcion',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='multimediaobj',
            name='cdn',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.CDNGenerico'),
        ),
    ]
