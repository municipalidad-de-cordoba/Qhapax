import factory
from factory.django import DjangoModelFactory



class TipoIdFactory(DjangoModelFactory):
    class Meta:
        model = 'core.TipoId'
        django_get_or_create = ('nombre',)
    nombre = 'DNI'



class PersonaFactory(DjangoModelFactory):
    class Meta:
        model = 'core.Persona'

    nombre = factory.Faker('first_name', locale='es_ES')
    apellido = factory.Faker('last_name', locale='es_ES')
    tipo_id = factory.SubFactory(TipoIdFactory)


class MonedaFactory(DjangoModelFactory):
    class Meta:
        model = 'core.Persona'
        django_get_or_create = ('nombre',)

    nombre = 'Pesos Argentinos'
    simbolo = '$'
    # tomamos el dolar como moneda de referencia para unificar valores
    cantidad_por_dolar = '16.30'



class ComunicacionTipoFactory(DjangoModelFactory):
    class Meta:
        model = 'core.ComunicacionTipo'

    nombre = factory.Iterator(['telefono', 'email', 'twitter', 'web', 'facebook'])
    base_url = factory.Iterator(['phone:', 'mailto:', 'http://twitter.com/', 'http://', 'http://facebook.com/'])


class ComunicacionFactory(DjangoModelFactory):
    class Meta:
        abstract = True
    tipo = factory.SubFactory(ComunicacionTipoFactory)
    valor = factory.Iterator(['351 891828', 'juan@gobierno.com', '@gobierno'])


class ComunicacionPersonaFactory(ComunicacionFactory):
    class Meta:
        model = 'core.ComunicacionPersona'
    objeto = factory.SubFactory(PersonaFactory)
