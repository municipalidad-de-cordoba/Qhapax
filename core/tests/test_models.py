import pytest
from datetime import date
from freezegun import freeze_time
from .factories import PersonaFactory


@freeze_time("2017-06-20")
@pytest.mark.parametrize('nacimiento, edad', [
    (date(1982, 3, 22), 35),
    (date(1982, 7, 22), 34),
])
def test_edad(nacimiento, edad):
    p = PersonaFactory.build(fecha_nacimiento=nacimiento)
    assert p.edad == edad

@freeze_time("2017-06-20")
@pytest.mark.parametrize('nacimiento, franja', [
    (date(1982, 3, 22), "30's"),
    (date(1952, 5, 16), "60's"),
    (date(1947, 8, 1), "60's"),
    (date(1947, 1, 1), "70's"),
])
def test_franja_etaria(nacimiento, franja):
    p = PersonaFactory.build(fecha_nacimiento=nacimiento)
    assert p.franja_etaria == franja


def test_nombre_publico_definido():
    p = PersonaFactory.build(nombre_publico='Chipi Castillo', apellido='Castillo')
    assert p.nombrepublico == 'Chipi Castillo'


def test_nombre_publico_no_definido():
    p = PersonaFactory.build(nombre='Juan Carlos', apellido='Messi')
    p2 = PersonaFactory.build(nombre='María', apellido='Magdalena')
    assert p.nombrepublico == 'Juan Messi'
    assert p2.nombrepublico == 'María Magdalena'