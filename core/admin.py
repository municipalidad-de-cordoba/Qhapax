from django.contrib import admin
from django.contrib.gis import admin as gisadmin
from django.core.urlresolvers import reverse
from django import forms
from guiasturisticos.forms import ComunicacioGuiaTuristicoForm
from .models import (Persona, TipoId, ComunicacionTipo, ComunicacionPersona,
                     CDNGenerico, MultimediaObj, Idioma, TituloEducativo)
from import_export.admin import ImportExportModelAdmin


class QhapaxOSMGeoAdmin(gisadmin.OSMGeoAdmin):
    """ admin para modelos con GIS """

    # el template de admin.OSMGeoAdmin que anda OK
    # map_srid = 3857  # al parecer la que usa gmaps
    # map_srid = 4326  # es otra opcion usada por google
    map_template = 'gis/admin/osm.html'  # el que incluye calles y ciudades de OSM
    # map_template = 'gis/admin/openlayers.html'  # vacio, el original
    # default_lat = -31
    # default_lon = -64
    default_lon = -7144296
    default_lat = -3682101
    # TODO: Eliminar en la migracion a Django v1.11
    openlayers_url = "https://cdnjs.cloudflare.com/ajax/libs/openlayers/2.13.1/OpenLayers.js"
    # Fin
    default_zoom = 12
    map_width = 1200
    map_height = 500


class PersonaFotoForm(forms.ModelForm):
    """
    Solo edicion foto, permiso especial (temporal)
    """
    class Meta:
        model = Persona
        fields = ['nombre', 'apellido', 'foto']
        readonly = ['nombre', 'apellido']

        widgets = {
            'nombre': forms.TextInput(
                attrs={
                    'readonly': True,
                    'style': 'background-color: gray'}),
            'apellido': forms.TextInput(
                attrs={
                    'readonly': True,
                    'style': 'background-color: gray'})}


class ComunicacionInline(admin.StackedInline):
    model = ComunicacionPersona
    extra = 1


class PersonaAdmin(admin.ModelAdmin):
    list_display = (
        'nombre',
        'apellido',
        'fecha_nacimiento',
        'foto_thumb',
        'ficha_funcionario')
    search_fields = ['nombre', 'apellido', 'unique_id']
    exclude = ('slug', 'user')
    # filter_horizontal = ('comunicacion', )

    def foto_thumb(self, instance):
        if instance.foto:
            return '<img src="%s" style="width: 80px;"/>' % (instance.foto.url)
        return ' <img src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACXBIWXMAAAAnAAAAJwEqCZFPAAAA B3RJTUUH4AcJFDE2B3C7PwAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUH AAAACklEQVQI12P4DwABAQEAG7buVgAAAABJRU5ErkJggg==" alt="blanco " style="width: 80px; height: 120px;" />'
    foto_thumb.short_description = 'foto_thumb'
    foto_thumb.allow_tags = True

    def get_form(self, request, obj=None, **kwargs):
        '''
        Si es alguien que tiene un permiso limitado solo permitir esa carga
        '''
        if request.user.has_perm(
                'core.foto_persona') and not request.user.has_perm('core.add_persona'):
            kwargs['form'] = PersonaFotoForm
        return super(PersonaAdmin, self).get_form(request, obj, **kwargs)

    def ficha_funcionario(self, obj):
        ficha_url = reverse(
            'website.funcionarios.persona',
            kwargs={
                'persona': obj.slug,
                'persona_id': obj.id})
        return '<a target="_blank" href="{}">{} {}</a>'.format(
            ficha_url, obj.nombre, obj.apellido)
    ficha_funcionario.allow_tags = True

    inlines = [ComunicacionInline]


class ComunicacionTipoAdmin(admin.ModelAdmin):
    list_display = ("nombre", 'base_url')


class ComunicacionPersonaAdmin(admin.ModelAdmin):

    def get_form(self, request, obj=None, **kwargs):
        """
        Form de carga para los editores de guias turisticos solo puedan cargar guias
        """
        if request.user.has_perm(
                'core.editar_solo_guias') and not request.user.has_perm('core.add_persona'):
            # permiso especial, solo puede tocar comunicación de guías
            # turísticos
            kwargs['form'] = ComunicacioGuiaTuristicoForm
        return super(
            ComunicacionPersonaAdmin,
            self).get_form(
            request,
            obj,
            **kwargs)

    def get_queryset(self, request):
        """
        Los editores de guias sólo pueden editar guias.
        """
        qs = super(ComunicacionPersonaAdmin, self).get_queryset(request)
        if request.user.has_perm(
                'core.editar_solo_guias') and not request.user.has_perm('core.add_persona'):
            return qs.filter(objeto__guiaturistico__isnull=False)
        return qs

    list_display = (
        "objeto",
        'tipo',
        'valor',
        'descripcion',
        'privado',
        'absolute_url')
    search_fields = ['valor', 'descripcion']
    list_filter = ['privado', 'tipo']


class CDNGenericoAdmin(ImportExportModelAdmin):
    list_display = ['titulo', 'archivo', 'id', 'created', 'modified']
    search_fields = ['titulo', 'archivo']


class MultimediaObjAdmin(admin.ModelAdmin):
    list_display = ("tipo", "cdn")
    list_filter = ['tipo']


class IdiomaAdmin(admin.ModelAdmin):
    list_display = ("nombre", )


class TituloEducativoAdmin(admin.ModelAdmin):
    def tipos(self, obj):
        return obj.get_tipo_display()
    list_display = ("titulo", "tipos")
    search_fields = ['titulo']
    list_filter = ['tipo']


admin.site.register(Persona, PersonaAdmin)
admin.site.register(TipoId)
admin.site.register(ComunicacionTipo, ComunicacionTipoAdmin)
admin.site.register(ComunicacionPersona, ComunicacionPersonaAdmin)
admin.site.register(CDNGenerico, CDNGenericoAdmin)
admin.site.register(MultimediaObj, MultimediaObjAdmin)
admin.site.register(Idioma, IdiomaAdmin)
admin.site.register(TituloEducativo, TituloEducativoAdmin)
