Módulo Core
===========

Este módulo es general y se encarga de administrar algunos modelos usados internamente por las otras aplicaciones.

Se incluyen:
 - Manejo de monedas
 - Vías de comunicación
 - Manejo de `Personas <personas.rst>`__
 - Otros
