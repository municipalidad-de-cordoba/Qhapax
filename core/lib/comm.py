from django.conf import settings


class QhapaxMail:
    """
    Enviador de emails
    """
    def __init__(self, my_host=settings.EMAIL_HOST, my_port=settings.EMAIL_PORT,
                    my_username=settings.EMAIL_HOST_USER,
                    my_password=settings.EMAIL_HOST_PASSWORD,
                    my_use_tls=settings.EMAIL_USE_TLS,
                    my_use_ssl=settings.EMAIL_USE_SSL,
                    from_email=settings.EMAIL_FROM):
        self.my_host = my_host
        self.my_port = my_port
        self.my_username = my_username
        self.my_password = my_password
        self.my_use_tls = my_use_tls
        self.my_use_ssl = my_use_ssl
        self.from_email = from_email

    def send_mail(self, titulo, html_mensaje, to):
        from django.core.mail import get_connection, send_mail
        connection = get_connection(host=self.my_host,
                                    port=self.my_port,
                                    username=self.my_username,
                                    password=self.my_password,
                                    use_tls=self.my_use_tls,
                                    use_ssl=self.my_use_ssl)

        to = to.split(',')
        
        sended = send_mail(subject=titulo, message='',
                            from_email=self.from_email,
                            recipient_list=to, 
                            connection=connection, 
                            html_message=html_mensaje, 
                            fail_silently=False)

        return sended