import unicodedata


def normalizar(text):
    """
    Devuelve una version del texto normalizada en ascii y en minuscula.
    (análogo a slugify)
    Es util para realizar comparaciones o búsquedas.

        >>> normalizar('El Ñandú')
        'el nandu'
    """
    if text is not None:
        text = text.lower()
        return unicodedata.normalize('NFKD', text).encode('ascii', 'ignore').decode('ascii')
    else:
        return ''