#!/usr/bin/python

from django.core.management.base import BaseCommand
from core.models import ComunicacionPersona
from funcionarios.models import ComunicacionCargo


class Command(BaseCommand):
    help = 'Comando para trabajar con mails mal formados'

    def command(self, model):

        todos = model.objects.all().values('objeto').distinct()

        for i in todos:
            objeto = i['objeto']
            for j in model.objects.filter(objeto__id=objeto):
                try:
                    model.objects.get(
                        objeto=j.objeto, tipo=j.tipo, valor=j.valor)
                except BaseException:
                    print("uno solo")
                    for x in model.objects.filter(
                            objeto=j.objeto,
                            tipo=j.tipo,
                            valor=j.valor)[
                            1:]:
                        self.stdout.write(
                            self.style.WARNING(
                                'Eliminando comunicacion %s porque esta duplicado' %
                                x.id))
                        x.delete()

    def handle(self, *args, **options):
        self.command(ComunicacionCargo)
        self.command(ComunicacionPersona)
