#!/usr/bin/python

from django.core.management.base import BaseCommand
from django.db import IntegrityError
from core.models import ComunicacionTipo, ComunicacionPersona
from funcionarios.models import ComunicacionCargo
from django.utils.text import slugify


class Command(BaseCommand):
    help = 'Comando para trabajar con mails mal formados'

    def parse(self, email):
        try:
            email = email.split("/")[1]
        except IndexError:
            pass

        email = email.split("@")
        nombre = slugify(email[0])

        try:
            if email[1][0:7] == "cordoba":
                domain = "cordoba.gov.ar"
            else:
                domain = email[1]
        except BaseException:
            self.stdout.write(self.style.WARNING('Email RARO %s ' % email))
            return email

        return "%s@%s" % (nombre, domain)

    def handle(self, *args, **options):

        tipo = ComunicacionTipo.objects.get(pk=7)

        for i in ComunicacionPersona.objects.filter(tipo=tipo):
            email = self.parse(i.valor)
            i.valor = email
            try:
                i.save()
            except IntegrityError:
                self.stdout.write(
                    self.style.WARNING(
                        'Eliminando email %s porque esta duplicado' %
                        email))
                i.delete()

        for i in ComunicacionCargo.objects.filter(tipo=tipo):
            email = self.parse(i.valor)
            i.valor = email
            try:
                i.save()
            except IntegrityError:
                self.stdout.write(
                    self.style.WARNING(
                        'Eliminando email %s porque esta duplicado' %
                        email))
                i.delete()
