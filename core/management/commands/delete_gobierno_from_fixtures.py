import json

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Elimino campo gobierno de los fixtures. Solo valido para migracion, luego eliminar."
    data = None
    cleaned = []

    def handle(self, *args, **options):
        self.clean_gob()
        self.clean_django_data()
        self.save()

    def clean_gob(self):
        """
        Elimino el campo gobierno de los fixtures
        :return:
        """
        FILE = "fixtures/dump.json"
        self.data = json.load(open(FILE, "r"))
        for index, fixture in enumerate(self.data):
            # Elimino gobierno de los modelos
            fixture["fields"].pop("gobierno", None)

    def clean_django_data(self):
        for index, fixture in enumerate(self.data):
            if fixture["model"] in []:
                pass
            else:
                self.cleaned.append(fixture)

    def save(self):
        """
        Guardo el nuevo archivo de fixtures
        :return:
        """
        FILE = "fixtures/out.json"
        fl = json.dump(self.cleaned, open(FILE, "w"), indent=4)
