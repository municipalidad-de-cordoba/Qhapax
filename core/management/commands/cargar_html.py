#!/usr/bin/python

from django.core.management.base import BaseCommand
from funcionarios.models import Funcion
from core.models import Moneda, TipoId, ComunicacionTipo
from django.conf import settings
import csv

PESO = Moneda.objects.get(pk=settings.MONEDA_PK_PRINCIPAL)


class Command(BaseCommand):
    help = 'Comando para importar csv'

    DNI = TipoId.objects.get(pk=1)
    COMUNICACION_TIPO = {x.nombre.lower().replace(
        " ", "_"): x for x in ComunicacionTipo.objects.all()}

    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs='+', type=str)

    def _to_dict(self, row):
        _dict = {}
        for i in range(0, len(row)):
            _dict[self.__header[i]] = row[i]
        return _dict

    def handle(self, *args, **options):

        self.stdout.write(
            self.style.SUCCESS(
                'Importando csv %s ' %
                (options['csv_file'][0])))

        count = 0
        with open(options['csv_file'][0], newline='', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            self.__header = reader.__next__()

            for row in reader:
                datos = self._to_dict(row)

                if datos['cargar'] == "x":
                    continue

                try:
                    funcion = Funcion.objects.get(
                        funcionario__unique_id=datos['unique_id'])
                except BaseException:
                    print("no se encuentra: %s" % datos['unique_id'])

                funcion.cargo.html = datos['reparticion_html']
                funcion.cargo.save()
                funcion.funcionario.html = datos['persona_html']
                funcion.funcionario.save()

        self.stdout.write(
            self.style.SUCCESS(
                'Archivo cargado con éxito, se cargaron %s registros' %
                count))
