import datetime
import base64
import qrcode
from django.conf import settings
from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField
from io import BytesIO
from versatileimagefield.fields import VersatileImageField, PPOIField
from core.utils import normalizar
from django.core.urlresolvers import reverse
from django.core.files.storage import FileSystemStorage
from datetime import date
from django.core.cache import cache
import requests
import json
import logging
logger = logging.getLogger(__name__)


class ComunicacionTipo(models.Model):
    # telefono, email, web, facebook, twitter, youtube, etc
    nombre = models.CharField(max_length=120)
    # ej.: https://twitter.com o phone: o mailto:
    base_url = models.CharField(max_length=120)

    @property
    def nombre_normalizado(self):
        return normalizar(self.nombre)

    def __str__(self):
        return self.nombre

    class Meta:
        unique_together = (("nombre", "base_url"), )


class Comunicacion(models.Model):
    """
    Los gobiernos, los cargos y los funcionarios podrán publicar telefonos,
    sitios webs, redes sociales, emails, etc
    """
    # telefono, email, web, facebook, twitter, youtube, etc
    tipo = models.ForeignKey(
        ComunicacionTipo,
        on_delete=models.CASCADE,
        related_name="tipo_comunicacion_%(app_label)s_%(class)s")
    # 351 891828, juan@gobierno.com, @gobierno
    valor = models.CharField(max_length=220)
    descripcion = models.TextField(null=True, blank=True)
    # ver si se puede publicar en la web
    privado = models.BooleanField(default=False)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    @property
    def absolute_url(self):

        if self.tipo.nombre_normalizado == "email":
            try:
                valor = self.valor.split("/")[1]
            except BaseException:
                valor = self.valor

        elif self.tipo.nombre_normalizado in ("blog", "web"):
            try:
                valor = self.valor.split("://")[1]
            except BaseException:
                valor = self.valor
        elif self.tipo.nombre_normalizado == "twitter":
            valor = self.valor.replace("@", "")
            valor = valor.replace('https://twitter.com/', '')
        elif self.tipo.nombre_normalizado == "facebook fanpage":
            valor = self.valor.replace('https://facebook.com/', '')
        elif self.tipo.nombre_normalizado == "facebook personal":
            valor = self.valor.replace('https://facebook.com/', '')
        else:
            valor = self.valor

        if self.tipo.nombre_normalizado != "linkedin":
            # linkedin a veces empieza con ar.linkedin u otros
            ret = "%s%s" % (self.tipo.base_url, valor)
        else:
            ret = valor

        return ret

    class Meta:
        abstract = True

    def __str__(self):
        return '{}: {}'.format(self.tipo, self.valor)


class Moneda(models.Model):
    """
    Las monedas para describir los sueldos
    """
    nombre = models.CharField(max_length=50)
    simbolo = models.CharField(max_length=10)
    # tomamos el dolar como moneda de referencia para unificar valores
    cantidad_por_dolar = models.DecimalField(
        max_digits=14, decimal_places=2, default=1.0)

    def __str__(self):
        return '{} ({})'.format(self.simbolo, self.nombre)


class TipoId(models.Model):
    """
    Tipo de identificacion personal de la persona/funcionario
    DNI (AR), RUT (CL), etc
    """
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre


class TipoOrganizacion:
    """
    Esta lista se usa como desplegable para que los vecinos se idendifiquen en
    diferentes formularios. Podría hacerse editable o pasar a Settings.
    Hay que definirle un futuro manteniendo compatibilidad con los datos cargados """

    PERSONA_FISICA = 5
    UNIVERSIDAD = 10
    ONG = 20
    EMPRESA = 30
    COOPERATIVA = 40
    PARTIDO_POLITICO = 50
    GOBIERNO = 60

    tipos = ((PERSONA_FISICA, "Persona física"), (UNIVERSIDAD, "Universidad"),
             (ONG, "ONG"), (EMPRESA, "Empresa"), (COOPERATIVA, "Cooperativa"),
             (PARTIDO_POLITICO, "Partido político"), (GOBIERNO, "Gobierno"))


class GeneroPersona(models.Model):
    """  """

    MASCULINO = "M"
    FEMENINO = "F"
    OTROS = "O"

    generos = ((MASCULINO, "Masculino"), (FEMENINO, "Femenino"),
               (OTROS, "Otros"))


class Persona(models.Model):
    user = models.OneToOneField(User,
                                on_delete=models.SET_NULL,
                                blank=True,
                                null=True,
                                verbose_name='Usuario de sistema')
    nombre = models.CharField(max_length=150)
    apellido = models.CharField(max_length=150)
    # evitar segundos nombres o apellidos molestos. Los funcionarios pueden
    # pedir una version simplificada
    nombre_publico = models.CharField(max_length=300, null=True, blank=True)
    slug = models.SlugField(max_length=300, null=True, blank=True)
    tipo_id = models.ForeignKey(
        TipoId,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name='Tipo de Documento')
    # nro de DNI o RUT u otro según país
    unique_id = models.CharField(
        max_length=90, null=True, blank=True, verbose_name='Documento')
    foto = VersatileImageField(
        upload_to='imagenes/personas',
        null=True,
        blank=True,
        ppoi_field='ppoi')
    fecha_nacimiento = models.DateField(auto_now=False,
                                        auto_now_add=False,
                                        null=True,
                                        blank=True)
    genero = models.CharField(max_length=2, null=True,
                              blank=True, choices=GeneroPersona.generos)
    html = RichTextField(null=True, blank=True)  # texto explicativo abierto
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    ppoi = PPOIField('Image PPOI')

    def get_absolute_url(self):
        return reverse('website.funcionarios.persona',
                       kwargs={'persona': self.slug,
                               'persona_id': self.id})

    def __str__(self):
        # por default devuelve el nombre publico
        return self.nombrepublico

    def save(self, *args, **kwargs):
        # lo mismo que con el __str__, el slug lo arma con el nombre público
        self.slug = slugify(self.nombrepublico)
        super(Persona, self).save(*args, **kwargs)

    def make_qr(self, url=None, tipo='imagen'):
        # FIXME . esto no tiene sentido que sea un metodo.
        # generalizar como funcion.
        if url is None:
            url = self.get_absolute_url()
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=6,
            border=0)
        qr.add_data(url)
        image = qr.make_image()
        buff = BytesIO()
        image.save(buff, format="PNG")

        if tipo == 'imagen':  # texto para el SRC de un tag IMG
            str_tipo = 'data:image'
        elif tipo == 'donwload-link':  # link para descarga en un tag A
            str_tipo = 'data:application/octet-stream'
        return "{}/png;base64,{}".format(str_tipo,
                                         base64.b64encode(buff.getvalue()).decode("utf-8"))

    def get_image_qr(self, url=None):
        return self.make_qr(url=url)

    def get_download_link_qr(self, url=None):
        return self.make_qr(url=url, tipo='donwload-link')

    @property
    def franja_etaria(self):
        if not self.fecha_nacimiento:
            return None

        hoy = datetime.date.today()
        nacimiento = self.fecha_nacimiento
        vida = hoy - nacimiento
        dias = vida.days
        anios = dias / 365
        decada = anios // 10
        return "{}0's".format(int(decada))

    @property
    def nombrepublico(self):
        if self.nombre_publico is None or self.nombre_publico == '':
            return '{} {}'.format(self.nombre.split()[0],
                                  self.apellido.split()[0])
        else:
            return self.nombre_publico

    @property
    def edad(self):
        """ Calcula la edad"""

        if not self.fecha_nacimiento:
            return None

        today = date.today()
        return today.year - self.fecha_nacimiento.year - (
            (today.month, today.day) <
            (self.fecha_nacimiento.month, self.fecha_nacimiento.day))

    @property
    def en_funciones(self):
        '''
        detectar si ejerce algún cargo
        '''
        from funcionarios.models import Funcion
        funciones = Funcion.objects.filter(funcionario=self, activo=True)
        if len(funciones) == 0:
            return None
        elif len(funciones) == 1:
            return funciones[0]
        else:
            # TODO quizas sea un error, no debería haber mas de uno en un cargo
            return funciones

    class Meta:
        unique_together = (("tipo_id", "unique_id"), )
        ordering = ['nombre', 'apellido']
        permissions = (('foto_persona', 'Puede modificar foto'), )


class ComunicacionPersona(Comunicacion):
    objeto = models.ForeignKey(Persona, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("tipo", "valor", "objeto"),)
        permissions = (
            ('editar_solo_guias', 'Solo modifica comunicacion guias turisticos'),)


class CDNGenerico(models.Model):
    '''
    Archivos varios para usar en el website que no estan contemplados aquí
    '''
    titulo = models.CharField(max_length=150)
    archivo = models.FileField(upload_to='cdn/')
    descripcion = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name = "Archivo en CDN público"
        verbose_name_plural = "Archivos en CDN público"


class MyPrivateFileSystemStorage(FileSystemStorage):
    """Almacenamiento de archivos privados"""

    def __init__(self):
        super(MyPrivateFileSystemStorage, self).__init__(
            location=settings.PRIVATE_FILES_ROOT)

    """
    aqui podría interverirse si todas las URLs fueran iguales
    pero cada objeto que usa esto permite acceder a el con URLs diferentes
    def _get_url(self):
    """


class MultimediaObj(models.Model):
    """ Multimedia genérica para otros módulos. Se suben directos al CDN """
    IMAGEN = 10
    AUDIO = 20
    VIDEO = 30

    tipos = ((IMAGEN, 'Imagen'), (AUDIO, 'audio'), (VIDEO, 'Video'))
    tipo = models.PositiveIntegerField(choices=tipos, default=IMAGEN)

    cdn = models.ForeignKey(CDNGenerico)

    def __str__(self):
        return "{} tipo {}".format(self.cdn.titulo, self.get_tipo_display)


class Idioma(models.Model):
    """ Idiomas en general """
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre


class TituloEducativo(models.Model):
    """ un titulo universitario / terciario o de cualquier tipo entreado a
        una persona (funcionarios, guias turísticos, etc) """
    titulo = models.CharField(max_length=150)
    observaciones = models.TextField(null=True, blank=True)

    TIPO_PRIMARIO = 10
    TIPO_SECUNDARIO = 20
    TIPO_TECNICATURA = 30
    TIPO_TERCIARIO = 40
    TIPO_UNIVERSITARIO = 50
    TIPO_ESPECIALIZACION = 60
    TIPO_POSGRADO = 70
    TIPO_MAESTRIA = 80
    TIPO_DOCTORADO = 90
    TIPO_POSDOCTORADO = 100
    TIPO_OTROS = 110

    tipos = ((TIPO_PRIMARIO, 'Primario'),
             (TIPO_SECUNDARIO, 'Secundario'),
             (TIPO_TECNICATURA, 'Tecnicatura'),
             (TIPO_TERCIARIO, 'Terciario'),
             (TIPO_UNIVERSITARIO, 'Universitario'),
             (TIPO_ESPECIALIZACION, 'Especializacion'),
             (TIPO_POSGRADO, 'Posgrado'),
             (TIPO_MAESTRIA, 'Maestría'),
             (TIPO_DOCTORADO, 'Doctorado'),
             (TIPO_POSDOCTORADO, 'Postdoctorado'),
             (TIPO_OTROS, 'Otros no listados'))

    tipo = models.PositiveIntegerField(choices=tipos)

    def __str__(self):
        return '{} ({})'.format(self.titulo, self.get_tipo_display())


""" para pensar en Centros de Salud o repeticion de actividades publicas
class DiasYHorarios(models.Model):
    ''' dias y horarios para diferentes modelos '''
    lunes = models.BooleanField(default=False)
    martes = models.BooleanField(default=False)
    miercoles = models.BooleanField(default=False)
    jueves = models.BooleanField(default=False)
    viernes = models.BooleanField(default=False)
    sabado = models.BooleanField(default=False)
    domingo = models.BooleanField(default=False)

    desde_hora = model.IntegerField(min_value=0, max_value=23, null=True, blank=True)
    desde_minuto = model.IntegerField(min_value=0, max_value=59, default=0, null=True, blank=True)
    hasta_hora = model.IntegerField(min_value=0, max_value=23, null=True, blank=True)
    hasta_minuto = model.IntegerField(min_value=0, max_value=59, default=0, null=True, blank=True)

    def __str__(self):
        dias = []
        if self.lunes: dias.append('Lunes')
        if self.martes: dias.append('Martes')
        if self.miercoles: dias.append('Miércoles')
        if self.jueves: dias.append('Jueves')
        if self.viernes: dias.append('Viernes')
        if self.sabado: dias.append('Sábado')
        if self.domingo: dias.append('Domingo')
        if len(dias) == 1:
            dias_str = dias[0]
        else:
            todos_menos_el_ultimo = ', '.join(dias[:-1])
            ultimo = dias[-1]
            dias_str = '{} y {}'.format(todos_menos_el_ultimo, ultimo)

        de_hasta = 'de {}:{} a {}:{}'.format(self.desde_hora, self.desde_minuto, self.hasta_hora, self.hasta_minuto)
        return '{} {}'.format(dias_str, de_hasta)
"""


def recorridos_cordobus():
    """
    Los recorridos de colectivo estaban en Qhapax al principio,
    ahora son parte del proyuecto Cordobus
    """
    cacheado = cache.get('cache_recorridos_cordobus')

    if cacheado:
        logger.info('cache_recorridos_cordobus OK')
        return cacheado

    recorridos_url = 'https://cordobus.apps.cordoba.gob.ar/lineas/api/recorrido/?page_size=300'
    req = requests.get(recorridos_url)
    basedata = req.content.decode('utf-8')
    jdata = json.loads(basedata)
    # logger.info('JDATA {}'.format(jdata))
    recorridos_data = jdata['results']['features']
    # logger.info('REC DATA {}'.format(recorridos_data))
    cache.set('cache_recorridos_cordobus', recorridos_data, 3600 * 24)

    return recorridos_data


def get_recorrido_cordobus(pk):
    recorridos = recorridos_cordobus()
    return [recorrido for recorrido in recorridos if recorrido['id'] == pk]
