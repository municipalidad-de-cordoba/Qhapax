from django.contrib import admin
from .models import Preferencias, TemaGeneral


class PreferenciasAdmin(admin.ModelAdmin):
    list_display = ('preferencia', 'valor')
    search_fields = ['preferencia', 'valor']


admin.site.register(Preferencias, PreferenciasAdmin)
admin.site.register(TemaGeneral)
