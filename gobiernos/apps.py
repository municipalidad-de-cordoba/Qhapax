from django.apps import AppConfig


class GobiernosConfig(AppConfig):
    name = 'gobiernos'
    verbose_name = 'Gobiernos'
