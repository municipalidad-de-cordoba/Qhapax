from django.db import models


class PreferenciasManager(models.Manager):
    """
    administrador de las preferencias
    """

    def as_dict(self):
        preferencias = self.all()
        ret = {}
        for p in preferencias:
            ret[p.preferencia] = p.valor

        return ret


class Preferencias(models.Model):
    """
    preferencias generales del gobierno
    #TODO listar las necesarias:
        default_persona_imagen: imagen de persona anonima cuando no tenga
    """
    preferencia = models.CharField(max_length=150)
    valor = models.CharField(max_length=150, null=True, blank=True)

    objects = PreferenciasManager()


class TemaGeneral(models.Model):
    '''
    Temas generales para marcar solicitud de audiencia,
    pedidos de acceso a la información e ideas ciudadanas
    Cada gobierno/oficina tiene sus propios temas
    '''
    nombre = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']
