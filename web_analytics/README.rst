API de Google Analytics
=======================

Conexión a los datos de Google Analytics.

Requisitos
----------

Tener acceso a una propiedad de Google Analytics (de la que se tomarán los datos).
Tener un proyecto en la consola de desarrolladores de Google
https://console.developers.google.com/


Procedimiento
-------------

1) Habilitar Analytics API desde la biblioteca de apis del proyecto.
2) Generar credencial de clave de cuenta de servicio.
3) Crear un nuevo usuario dentro de la propiedad de Google Analytics cuyo email sea el id de la cuenta de servicio obtenido en el paso anterior.
4) Para poder realizar la auntenticación a través de python, se debe instalar la siguiente librería: google-api-python-client(incluida ya en el requirements de Qhapax).
5) Descargar la clave de la cuenta de servicio en formato json. Los datos que trae este
archivo json son los necesarios para crear una nueva conexión.
6) Realizar las querys desde https://ga-dev-tools.appspot.com/query-explorer/ .


GoogleAnalyticsConnection
-------------------------

Para crear conexiones desde el admin, se necesitan los siguientes datos:

-project_id
-private_key_id
-private_key
-client_email
-client_id
-client_x509_cert_url

Todos estos datos se obtienen del archivo json obtenido en el paso (5) del procedimiento.
Una vez creada una conexión, puede borrar este archivo.


QueryGoogleAnalytics
--------------------

Obtenemos la url desde https://ga-dev-tools.appspot.com/query-explorer/. Aquí se
crea la query necesaria.

Para crear querys desde el admin, se necesita referenciar a que conexión pertenece. Luego:

- crear un título representativo
- descripción de los datos de resultados de la query
- url: obtenida al principio


Detalles
--------
Este módulo permite administrar conexiones con sitios que usan Google Analytics para obtener sus datos.
Cada conexión permite almacenar una o mas *queries* con informacón específica.

Queries de Ejemplo
~~~~~~~~~~~~~~~~~~
Las consultas se pueden almacenar en la base de datos para ejecutarlas cuando sea necesario.
El comando de django ejecutar_query_analytics realiza este trabajo con todas las querys guardadas. Si le indicamos el parametro "--id" ejecutará esa consulta en particular únicamente.
Ejemplo de query con los *pageviews* por día en los últimos 30 días:

::

  https://www.googleapis.com/analytics/v3/data/ga
    ?ids=ga%3A124506008      # ID de la vista (En la jerarquía Account, Property, View) donde consultar 
    &start-date=30daysAgo
    &end-date=yesterday
    &metrics=ga%3Apageviews  # variable de medición
    &dimensions=ga%3Adate    # dimension a agrupar
    &sort=-ga%3Apageviews    # orden de los datos
    &access_token=xxxxxxxxxxxxx
