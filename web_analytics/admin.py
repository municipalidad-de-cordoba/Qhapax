from django.contrib import admin
from .models import *


@admin.register(GoogleAnalyticsConnection)
class GoogleAnalyticsConnectionAdmin(admin.ModelAdmin):
    list_display = ['id', 'project_id', 'client_email']
    search_fields = ['id', 'project_id', 'client_email']


@admin.register(QueryGoogleAnalytics)
class QueryGoogleAnalyticsAdmin(admin.ModelAdmin):
    list_display = ['id', 'titulo', 'descripcion_publica']
    search_fields = ['id', 'titulo']


@admin.register(ResultadosQuery)
class ResultadosQueryAdmin(admin.ModelAdmin):
    list_display = ['query', 'fecha']
    search_fields = ['id', 'query', 'fecha']
