from django.db import models
from oauth2client.service_account import ServiceAccountCredentials
import requests
import json
import codecs


class GoogleAnalyticsConnection(models.Model):
    """
    Clase para administrar la conexion con una cuenta de
    Google Analytics y sus datos.
    Puesto en DB para permitir más de una conexión
    """

    SCOPE_READONLY = 'https://www.googleapis.com/auth/analytics.readonly'

    project_id = models.CharField(max_length=90)
    private_key_id = models.CharField(max_length=90)
    private_key = models.TextField(help_text='KEY privada completa')
    client_email = models.CharField(max_length=90)
    client_id = models.CharField(max_length=90)
    client_x509_cert_url = models.CharField(max_length=90)

    def get_dict_access(self):
        ''' obtener el diccionario de validacion para acceder a los datos '''
        dc = {
            'type': 'service_account',
            'project_id': self.project_id,
            'private_key_id': self.private_key_id,
            'private_key': codecs.decode(
                self.private_key,
                'unicode_escape'),
            'client_email': self.client_email,
            'client_id': self.client_id,
            'auth_uri': 'https://accounts.google.com/o/oauth2/auth',
            'token_uri': 'https://accounts.google.com/o/oauth2/token',
            'auth_provider_x509_cert_url': 'https://www.googleapis.com/oauth2/'
            'v1/certs',
            'client_x509_cert_url': self.client_x509_cert_url}
        return dc

    # Defines a method to get an access token from the ServiceAccount object.
    def get_access_token(self):
        # http://oauth2client.readthedocs.io/en/latest/source/oauth2client.service_account.html
        dict_access = self.get_dict_access()

        scope = self.SCOPE_READONLY
        return ServiceAccountCredentials.from_json_keyfile_dict(
            dict_access, scope).get_access_token().access_token

    def __str__(self):
        return self.project_id


class QueryGoogleAnalytics(models.Model):
    ''' Cada uno de los queries de interes genero la query desde la API de google
        https://ga-dev-tools.appspot.com/query-explorer/ '''

    connection = models.ForeignKey(GoogleAnalyticsConnection)
    titulo = models.CharField(max_length=90,
                              help_text='Titulo de lo que representa la query')
    descripcion_publica = models.TextField(
        help_text='Descripcion pública del dato que releva esta Query')
    url = models.URLField(
        max_length=450, help_text=(
            'URL completa obtenida en la herramienta '
            'https://ga-dev-tools.appspot.com/query-explorer/'
            ' SIN EL PARAMETRO ACCESS_TOKEN. '
            'Ejemplo: https://www.googleapis.com/analytics/v3/data/'
            'ga?ids=ga%3A124506008&start-date=2018-01-01&end-date=2018-03-14'
            '&metrics=ga%3Apageviews&dimensions=ga%3ApagePath&sort='
            '-ga%3Apageviews'))

    def get_url_with_token(self):
        token = self.connection.get_access_token()
        return '{}&access_token={}'.format(self.url, token)

    def get_query_data(self):
        """
        Conectarse con token a Google Analytics y obtener los datos que sean.
        """
        url = self.get_url_with_token()
        basedata = requests.get(url)
        # pasamos los datos a string
        basedata = basedata.content.decode('utf-8')
        try:
            # ahora los datos están en formato json
            jsondata = json.loads(basedata)
        except Exception as e:
            return None

        return basedata

    def __str__(self):
        return self.titulo


class ResultadosQuery(models.Model):
    """
    Histórico de resultados de las querys generadas
    """
    query = models.ForeignKey(QueryGoogleAnalytics)
    fecha = models.DateTimeField(auto_now=True)
    resultados = models.TextField(
        help_text=("resultado de una query, usar el metodo get_query_data "
                   "para rellenarlo"))
