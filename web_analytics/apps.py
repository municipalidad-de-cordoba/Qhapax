from django.apps import AppConfig


class WebAnalyticsConfig(AppConfig):
    name = 'web_analytics'
