#!/usr/bin/python
"""
Comando que crea una conexión con Google Analytics
"""
from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from web_analytics.models import *


class Command(BaseCommand):
    help = """Comando para crear y actualizar querys en Google Analytics"""

    def add_arguments(self, parser):
        parser.add_argument(
            '--id', type=str, help='Id de la query que queremos ejecutar. Se puede ver este valor desde el admin.')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Conectando con Analytics...'))
        id_query = options.get('id', None)

        if id_query is not None:
            querys = [QueryGoogleAnalytics.objects.get(id=id_query)]
        else:
            querys = QueryGoogleAnalytics.objects.all()
            
        for q in querys:
            self.stdout.write(self.style.SUCCESS('Buscando datos')) 
            data = q.get_query_data()
            if data is None:
                self.stdout.write(self.style.ERROR('ERROR AL TRAER JSON DE ANALYTICS')) 
                sys.exit(1)
            self.stdout.write(self.style.SUCCESS('Datos OK')) 
            ResultadosQuery.objects.create(query=q, resultados=data)
        
        self.stdout.write(self.style.SUCCESS('FIN'))

        