from django.shortcuts import render
from django.views.decorators.cache import cache_page
from iniciativas.models import Iniciativa


@cache_page(60 * 60)  # 1 h
def ver_iniciativas(request):
    '''
    mostrar la lista de iniciativas participativas 
    '''
    iniciativas = Iniciativa.objects.filter(publicado=True)

    context = {'iniciativas': iniciativas}
    url = "website/{}/iniciativas.html".format(request.website.template)
    return render(request, url, context)
