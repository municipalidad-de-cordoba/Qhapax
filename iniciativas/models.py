'''
Iniciativas Participativas para
la sección específica del portal de Gobierno Abierto
'''
from django.db import models
from portaldedatos.models import Recurso
from versatileimagefield.fields import VersatileImageField

class Iniciativa(models.Model):
    '''
    Cada iniciativa participativa de un dato de nuestro portal
    '''
    titulo = models.CharField(max_length=250)
    descripcion = models.TextField(null=True, blank=True)
    imagen = VersatileImageField(
        upload_to='imagenes/software-img',
        null=True,
        blank=True)
    publicado = models.BooleanField(default=False)
    fecha = models.DateTimeField(auto_now_add=False)
    url = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.titulo

    class Meta:
        ordering = ['-fecha']
