from django.contrib import admin
from .models import Iniciativa
from .forms import IniciativaForm


class IniciativaAdmin(admin.ModelAdmin):
    form = IniciativaForm
    list_display = ('titulo', 'publicado', 'url', 'fecha')
    search_fields = ('titulo', 'url')
    list_filter = ['publicado']

    # sin esto no funciona el JS de select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


admin.site.register(Iniciativa, IniciativaAdmin)
