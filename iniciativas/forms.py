from portaldedatos.forms import RecursoWidgetSelect
from .models import Iniciativa
from django import forms


class IniciativaForm(forms.ModelForm):

    class Meta:
        model = Iniciativa
        fields = '__all__'
