from django.db import models
# cada empresa es antes de todo, una organización
from organizaciones.models import Organizacion
from ordenanzatributaria.models import ActividadOTA
# actividades en la que está inscripto
# from ordenanzatributaria.models import ActividadOTA

from simple_history.models import HistoricalRecords


class TipoContribuyente(models.Model):
    """ Clasificación general de los contribuyentes. Hoy es
    Tipo 1: Contribuyentes pequeños que pagan un monto fijo por mes.
    Tipo 2: La generalidad de los pequeños comercios.
    Tipo 3: Grandes Contribuyentes.
    Tipo 4: Contribuyentes de Interés Fiscal (vendrían a ser los medianos).
    (5 quizas sea profesionales)
    """
    # codigo del tipo de contribuyente del sistema
    id_externo = models.CharField(max_length=3, default=0)
    nombre = models.CharField(max_length=250)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']
        verbose_name_plural = "Tipos de contribuyentes"
        verbose_name = "Tipo de contribuyente"


class InscripcionEmpresa(models.Model):
    """ Inscripcion de empresas en la Contribución que incide sobre la
    Actividad Comercial, Industrial y de Servicios """

    organizacion = models.ForeignKey(Organizacion)
    tipo = models.ForeignKey(TipoContribuyente, null=True, blank=True)
    fecha_inscripcion = models.DateField(
        auto_now=False, auto_now_add=False, null=True, blank=True)
    acta_inscripcion = models.FileField(
        upload_to='actas-inscripcion-recursos-tributarios/',
        null=True,
        blank=True,
        verbose_name='Acta de inscripcion de comercio')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()

    def __str__(self):
        nombre = 'Sin Nombre' if self.organizacion.nombre is None else self.organizacion.nombre
        tipo = '' if self.tipo.nombre is None else self.tipo.nombre
        return '{} {}'.format(nombre, tipo)

    class Meta:
        ordering = ['organizacion__nombre']
        verbose_name_plural = "Inscripciones en Recursos Tributario"
        verbose_name = "Inscripción en Recursos Tributario"


class InscripcionActividadEmpresa(models.Model):
    '''
    Vinculacion entre un comercio inscripto y una categoria de actividad comercial
    Cada comercio puede estar inscripto en más de una categoría
    '''

    organizacion = models.ForeignKey(Organizacion)
    actividad = models.ForeignKey(ActividadOTA)
    fecha_inicio_actividad = models.DateField(null=True, blank=True)
    fecha_fin_actividad = models.DateField(null=True, blank=True)
    # en los codigos de actividad se le agrega un 9000 al final
    # (op se suma 9000) cuando hay declarado un convenio multilaterial
    # al traerlo aquí lo separamos como esperamos quen sea
    es_convenio_multilateral = models.NullBooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    history = HistoricalRecords()

    def __str__(self):
        nombre = 'Sin Nombre' if self.organizacion.nombre is None else self.organizacion.nombre
        actividad = '' if self.actividad.codigo is None else self.actividad.codigo
        return '{} {}'.format(nombre, actividad)
