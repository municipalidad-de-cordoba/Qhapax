from django.contrib import admin
from .models import (InscripcionEmpresa, TipoContribuyente,
                     InscripcionActividadEmpresa)
from simple_history.admin import SimpleHistoryAdmin


class TipoContribuyenteAdmin(SimpleHistoryAdmin):
    def ultimo_editor(self, obj):
        if len(obj.history.all()) > 0:
            return obj.history.all()[0].history_user
        else:
            return ''
    list_display = ("nombre", "id_externo", "ultimo_editor")


class InscripcionEmpresaAdmin(SimpleHistoryAdmin):
    def ultimo_editor(self, obj):
        if len(obj.history.all()) > 0:
            return obj.history.all()[0].history_user
        else:
            return ''

    def organizacion_cuit(self, obj):
        return obj.organizacion.CUIT

    list_display = (
        'organizacion',
        'organizacion_cuit',
        'tipo',
        'fecha_inscripcion',
        'ultimo_editor')
    search_fields = ['organizacion__nombre', 'organizacion__CUIT']
    list_filter = ['tipo']


class InscripcionActividadEmpresaAdmin(SimpleHistoryAdmin):
    def ultimo_editor(self, obj):
        if len(obj.history.all()) > 0:
            return obj.history.all()[0].history_user
        else:
            return ''

    def organizacion_cuit(self, obj):
        return obj.organizacion.CUIT

    list_display = (
        'organizacion',
        'organizacion_cuit',
        'actividad',
        'ultimo_editor')
    search_fields = [
        'organizacion__nombre',
        'organizacion__CUIT',
        'actividad__nombre',
        'actividad__codigo']
    list_filter = ['actividad__categoria']


admin.site.register(TipoContribuyente, TipoContribuyenteAdmin)
admin.site.register(InscripcionEmpresa, InscripcionEmpresaAdmin)
admin.site.register(
    InscripcionActividadEmpresa,
    InscripcionActividadEmpresaAdmin)
