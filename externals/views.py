from django.shortcuts import render
from django.views.decorators.cache import cache_page
from django.shortcuts import get_object_or_404


@cache_page(60 * 60)  # 1 h
def externals(request):
    context = {'target': request.GET.get('target')}
    url = 'website/{}/externals.html'.format(request.website.template)
    return render(request, url, context)

