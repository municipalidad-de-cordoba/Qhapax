from django.contrib import admin
from .models import (DistritoCatastral, ZonaCatastral,
                     ManzanaCatastral, ParcelaCatastral)


class DistritoCatastralAdmin(admin.ModelAdmin):
    search_fields = ['codigo']
    list_display = ('codigo', )
    ordering = ['codigo']


class ZonaCatastralAdmin(admin.ModelAdmin):
    search_fields = ['codigo', 'distrito']
    list_display = ('codigo', 'distrito')
    ordering = ['distrito', 'codigo']
    list_filter = ['distrito']


class ManzanaCatastralAdmin(admin.ModelAdmin):
    search_fields = ['codigo']
    list_display = ('codigo', 'zona')
    ordering = ['zona', 'codigo']
    list_filter = ['zona']


class ParcelaCatastralAdmin(admin.ModelAdmin):
    def centroide_latitud(self, obj):
        if obj.centroide is not None:
            return obj.centroide.y
        else:
            return 0

    def centroide_longitud(self, obj):
        if obj.centroide is not None:
            return obj.centroide.x
        else:
            return 0

    def mostrar_en_OSM(self, obj):
        url = ''
        if obj.centroide is not None:
            lat = str(obj.centroide.y).replace(',', '.')
            lng = str(obj.centroide.x).replace(',', '.')
            url = '<a target="_blank" href="http://www.openstreetmap.org/?mlat={}&mlon={}&zoom=18">Ver en OSM</a>'.format(
                lat, lng)
        return url
    mostrar_en_OSM.allow_tags = True
    search_fields = ['codigo']
    list_display = (
        'codigo',
        'manzana',
        'centroide_latitud',
        'centroide_longitud',
        'mostrar_en_OSM')
    ordering = ['manzana', 'codigo']


admin.site.register(DistritoCatastral, DistritoCatastralAdmin)
admin.site.register(ZonaCatastral, ZonaCatastralAdmin)
admin.site.register(ManzanaCatastral, ManzanaCatastralAdmin)
admin.site.register(ParcelaCatastral, ParcelaCatastralAdmin)
