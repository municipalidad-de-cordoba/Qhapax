from django.contrib.gis.db import models
import logging
logger = logging.getLogger(__name__)


class DistritoCatastral(models.Model):
    codigo = models.CharField(max_length=30)
    poligono = models.GeometryField(null=True, blank=True)

    def __str__(self):
        return self.codigo


class ZonaCatastral(models.Model):
    distrito = models.ForeignKey(DistritoCatastral)
    codigo = models.CharField(max_length=30)
    poligono = models.GeometryField(null=True, blank=True)

    def __str__(self):
        return '{}-{}'.format(self.distrito.codigo, self.codigo)


class ManzanaCatastral(models.Model):
    zona = models.ForeignKey(ZonaCatastral)
    codigo = models.CharField(max_length=30)
    poligono = models.GeometryField(null=True, blank=True)

    def __str__(self):
        return '{}-{}-{}'.format(self.zona.distrito.codigo,
                                 self.zona.codigo, self.codigo)


class ParcelaCatastral(models.Model):
    manzana = models.ForeignKey(ManzanaCatastral)
    codigo = models.CharField(max_length=30)
    '''
    FID destino definitiva utilidadpu nombreofic numeroparc nomenclatu shape_area shape_len geom
    catastro-sde-parcela.1 1 2 2 11 15-21-066-011 205.78977956 60.11347773
    MULTIPOLYGON (
    ((-64.19248861839402 -31.370804883934674,
    -64.19246395512862 -31.37064672356491,
    -64.19246181447504 -31.370633015549995,
    -64.19235383808824 -31.370645269657558,
    -64.19237718180646 -31.37082220114619,
    -64.19248861839402 -31.370804883934674))
    )
    catastro-sde-parcela.2 1 2 2 3 15-21-066-003 169.56973796 54.73002035
    MULTIPOLYGON (
    ((-64.19235383808824 -31.370645269657558,
    -64.19233265392774 -31.37048470003686,
    -64.19223302951684 -31.370496729417646,
    -64.19225590031029 -31.370656384701864,
    -64.19235383808824 -31.370645269657558))
    )
    '''
    fid = models.CharField(max_length=50, default='', blank=True)
    # Destino (1,2,3) (ParcelaUsoPrivado, EspacioVerde, PasilloComun)
    destino = models.CharField(max_length=5, default='', blank=True)
    # Definitiva (1,2) si o no (Depende si el plano que le dio origen está
    # protocolizado y la parcela está inscripta en el Registro de la
    # Propiedad)
    definitiva = models.CharField(max_length=5, default='', blank=True)
    # Utilidadpu (1,2) si o no la parcela es de utilidad pública
    utilidadpu = models.CharField(max_length=5, default='', blank=True)
    # Nombreofic  si la parcela es espacio verde, acá está el nombre oficial
    nombreofic = models.CharField(max_length=95, default='', blank=True)
    shape_area = models.DecimalField(
        max_digits=14, decimal_places=8, default=0.0)
    shape_len = models.DecimalField(
        max_digits=14, decimal_places=8, default=0.0)

    poligono = models.GeometryField(null=True, blank=True)
    # centroide calculado para agilizar la publicación de estos datos, se
    # actualiza al grabarse
    centroide = models.PointField(null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.poligono is not None:
            try:
                self.centroide = self.poligono.centroid
            except Exception as e:
                logger.error(
                    "Error al obtener centroide de parcela {}. Detalle: {}".format(
                        self.nomenclatura, e))
        super(ParcelaCatastral, self).save(*args, **kwargs)

    """
    Otros no usados como campos
    numeroparc numero de parcela es la ultima parte de la nomenclatura en valor numerico
    nomenclatu nomenclatura (DD-ZZ-MMM-PPP) ej 04-04-001-001
    """

    @property
    def nomenclatura(self):
        return '{}-{}-{}-{}'.format(self.manzana.zona.distrito.codigo,
                                    self.manzana.zona.codigo,
                                    self.manzana.codigo,
                                    self.codigo)

    def __str__(self):
        return '{}-{}-{}-{}'.format(self.manzana.zona.distrito.codigo,
                                    self.manzana.zona.codigo,
                                    self.manzana.codigo,
                                    self.codigo)

    class CalleOficial(models.Model):
        # todas las tramos de calles oficiales de catastro
        nombre = models.CharField(max_length=60)
        # trazado completo del tramo de la calle
        geo = models.GeometryField(null=True, blank=True)

        def __str__(self):
            return '{}-{}'.format(self.distrito.codigo, self.codigo)
