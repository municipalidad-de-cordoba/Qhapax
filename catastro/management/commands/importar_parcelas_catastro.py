#!/usr/bin/python
from django.core.management.base import BaseCommand

# from django.db import transaction
import csv
import sys
import os
from catastro.models import (DistritoCatastral, ZonaCatastral,
                             ManzanaCatastral, ParcelaCatastral)
from catastro import settings as settings_ct
from django.contrib.gis.geos import GEOSGeometry
import tempfile
import requests


class Command(BaseCommand):
    help = """Comando para importar desde CSV las parcelas de catastro """

    IMPORTAR_TODOS = 'TODOS'

    def add_arguments(self, parser):
        parser.add_argument(
            '--path',
            type=str,
            help='Path del archivo CSV local')
        parser.add_argument('--distrito', type=str,
                            default=self.IMPORTAR_TODOS,
                            help='Permite importar solo un distrito')
        parser.add_argument(
            '--download',
            type=str,
            default=settings_ct.URL_PARCELAS,
            help='Permite descargar desde el servidor para actualizar')

        parser.add_argument('--offset', type=int,
                            default=0,
                            help='Omitir las primeras N filas')

    # @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('--- Comenzando importacion ---'))

        path = options['path']
        if path is None or not os.path.isfile(path):
            # descargar entonces el CSV a usar
            download = options['download']
            self.stdout.write(self.style.SUCCESS('Descargando CSV'))
            try:
                basedata = requests.get(download).content  # es tipo byte
            except Exception as e:
                self.stdout.write(
                    self.style.ERROR(
                        'Error {}. Al leer el CSV Online {}'.format(
                            e, download)))
                sys.exit(1)

            tf = tempfile.NamedTemporaryFile()
            tf.write(basedata)

            path = tf.name

        distrito_pedido = options['distrito']
        offset = options['offset']

        # asegurarse de que el archivo tenga la misma estructura (encabezados)
        fieldnames = [
            'FID',
            'destino',
            'definitiva',
            'utilidadpu',
            'nombreofic',
            'numeroparc',
            'nomenclatu',
            'shape_area',
            'shape_len',
            'geom']

        ''' ejemplo de datos
        FID destino definitiva  utilidadpu  nombreofic  numeroparc  nomenclatu
        catastro-sde-parcela.1  1   2     2           11          15-21-066-011
        catastro-sde-parcela.2  1   2     2           3           15-21-066-003

        shape_area  shape_len   geom
        205.78977956    60.11347773 MULTIPOLYGON (
        ((-64.19248861839402 -31.370804883934674,
        -64.19246395512862 -31.37064672356491,
        -64.19246181447504 -31.370633015549995,
        -64.19235383808824 -31.370645269657558,
        -64.19237718180646 -31.37082220114619,
        -64.19248861839402 -31.370804883934674))
        )
        169.56973796    54.73002035 MULTIPOLYGON (
        ((-64.19235383808824 -31.370645269657558,
        -64.19233265392774 -31.37048470003686,
        -64.19223302951684 -31.370496729417646,
        -64.19225590031029 -31.370656384701864,
        -64.19235383808824 -31.370645269657558))
        )

        El que me interesa es nomenclatu que viene con Dist + zona + manzana +
        parcela (ej 15-21-066-011)
        '''
        nuevos = 0
        repetidos = 0
        omitidos = 0
        errores = []

        self.stdout.write(self.style.SUCCESS('Abriendo CSV {}'.format(path)))
        with open(path) as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=',',
                                    quotechar='"')

            header = reader.__next__()
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR(
                    'BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            c = 0
            for row in reader:
                c += 1
                if offset > c:
                    omitidos += 1
                    continue

                # chequear los datos
                nomenclatura = row['nomenclatu'].strip()
                partes = nomenclatura.split('-')
                if len(partes) != 4:
                    self.stdout.write(
                        self.style.ERROR(
                            'Parcela con mala nomenclatura {} ROW:{}'.format(
                                nomenclatura, row)))
                    row['ERROR'] = 'Parcela con mala nomenclatura'
                    errores.append(row)
                    continue

                cod_distrito = partes[0]
                # ver si es el distrito requerido o van todos
                if distrito_pedido != self.IMPORTAR_TODOS:
                    if distrito_pedido != cod_distrito:
                        omitidos += 1
                        continue

                cod_zona = partes[1]
                cod_manzana = partes[2]
                cod_parcela = partes[3]

                distrito, created = DistritoCatastral.objects.get_or_create(
                    codigo=cod_distrito)
                zona, created = ZonaCatastral.objects.get_or_create(
                    codigo=cod_zona, distrito=distrito)
                manzana, created = ManzanaCatastral.objects.get_or_create(
                    codigo=cod_manzana, zona=zona)
                parcela, created = ParcelaCatastral.objects.get_or_create(
                    codigo=cod_parcela, manzana=manzana)

                parcela.fid = row['FID'].strip()
                parcela.destino = row['destino'].strip()
                parcela.definitiva = row['definitiva'].strip()
                parcela.utilidadpu = row['utilidadpu'].strip()
                parcela.nombreofic = row['nombreofic'].strip()
                parcela.shape_area = float(row['shape_area'].strip())
                parcela.shape_len = float(row['shape_len'].strip())

                if row['geom'].strip() != '':
                    parcela.poligono = GEOSGeometry(row['geom'])

                try:
                    parcela.save()
                except Exception as e:
                    self.stdout.write(
                        self.style.ERROR(
                            'Error {}. Al grabar parcela de ROW:{}'.format(
                                e, row)))
                    row['ERROR'] = 'Error al grabar parcela'
                    errores.append(row)
                    continue

                if created:
                    nuevos += 1
                else:
                    repetidos += 1

                tot = nuevos + repetidos

                if tot % 1000 == 0:
                    txt = '{} registros. {} nuevos. {} repetidos. {} omitidos. {} errores'.format(
                        tot, nuevos, repetidos, omitidos, len(errores))
                    self.stdout.write(self.style.SUCCESS(txt))

            if len(errores) > 0:
                self.stdout.write(
                    self.style.SUCCESS(
                        'Archivo cargado con {} ERRORES. Se cargaron {} nuevos y {} repetidos. {} omitidos'.format(
                            len(errores),
                            nuevos,
                            repetidos,
                            omitidos)))
                # self.stdout.write(self.style.ERROR(' ***** ERRORES *******'))
                # for er in errores:
                #    self.stdout.write(self.style.ERROR('ERROR LINEA: {}'.format(er)))
            else:
                self.stdout.write(
                    self.style.SUCCESS(
                        'Archivo cargado con éxito sin errores. Se cargaron {} nuevos y {} repetidos. {} omitidos'.format(
                            nuevos,
                            repetidos,
                            omitidos)))

        self.stdout.write(self.style.SUCCESS('FIN'))
