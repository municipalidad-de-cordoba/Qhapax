# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-04 21:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catastro', '0006_auto_20171204_1759'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parcelacatastral',
            name='nombreofic',
            field=models.CharField(blank=True, default='', max_length=95),
        ),
    ]
