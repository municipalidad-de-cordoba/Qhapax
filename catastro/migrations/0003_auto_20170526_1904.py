# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-05-26 22:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catastro', '0002_auto_20170526_1859'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parcelacatastral',
            name='nombreofic',
            field=models.CharField(blank=True, default='', max_length=15),
        ),
    ]
