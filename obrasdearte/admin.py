from django.contrib import admin
from .models import (DisciplinaArtistica, TecnicaArtistica, MaterialArtistico,
                     ModoincorporacionObra, Artista, ObraDeArte, ImagenesObras,
                     ComunicacionArtista)


class DisciplinaArtisticaAdmin(admin.ModelAdmin):
    list_display = ["nombre"]
    search_fields = ['nombre']


class TecnicaArtisticaAdmin(admin.ModelAdmin):
    list_display = ["nombre"]
    search_fields = ['nombre']


class MaterialArtisticoAdmin(admin.ModelAdmin):
    list_display = ["nombre"]
    search_fields = ['nombre']


class ModoincorporacionObraAdmin(admin.ModelAdmin):
    list_display = ["nombre"]
    search_fields = ['nombre']


class ComunicacionInline(admin.StackedInline):
    model = ComunicacionArtista
    extra = 1


class ArtistaAdmin(admin.ModelAdmin):
    list_display = [
        'nombres',
        'apellidos',
        'alias',
        'se_puede_mostrar_el_nombre']
    search_fields = ['nombres', 'apellidos', 'alias']

    inlines = [ComunicacionInline]


class ImagenesObrasInline(admin.StackedInline):
    model = ImagenesObras
    extra = 1


class ImagenesObrasAdmin(admin.ModelAdmin):
    def titulo_obra(self, instance):
        return instance.obra.titulo
    list_display = ['id', 'orden', 'titulo_obra']


class ObraDeArteAdmin(admin.ModelAdmin):
    list_display = [
        'titulo',
        'publicado',
        'anio_de_realizacion',
        'anio_de_incorporacion',
        'propiedad']
    search_fields = ['titulo']
    list_filter = [
        'autores',
        'disciplinas',
        'tecnicas',
        'materiales',
        'propiedad']
    inlines = [ImagenesObrasInline]


admin.site.register(DisciplinaArtistica, DisciplinaArtisticaAdmin)
admin.site.register(TecnicaArtistica, TecnicaArtisticaAdmin)
admin.site.register(MaterialArtistico, MaterialArtisticoAdmin)
admin.site.register(ModoincorporacionObra, ModoincorporacionObraAdmin)
admin.site.register(Artista, ArtistaAdmin)
admin.site.register(ObraDeArte, ObraDeArteAdmin)
admin.site.register(ImagenesObras, ImagenesObrasAdmin)
