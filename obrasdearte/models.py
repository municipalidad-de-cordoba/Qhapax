from django.db import models
from versatileimagefield.fields import VersatileImageField, PPOIField
from ckeditor.fields import RichTextField
from core.models import Comunicacion


class Artista(models.Model):
    ''' cada uno de los artistas registrados en la ciudad '''
    nombres = models.CharField(max_length=80)
    apellidos = models.CharField(max_length=80)
    alias = models.CharField(max_length=180, null=True, blank=True)
    se_puede_mostrar_el_nombre = models.BooleanField(default=True)

    observaciones_publicas = RichTextField(
        help_text='Info extra para mostrar', null=True, blank=True)
    observaciones_privadas = models.TextField(null=True, blank=True)

    def __str__(self):
        return '{} {}'.format(self.nombres, self.apellidos)


class ComunicacionArtista(Comunicacion):
    objeto = models.ForeignKey(Artista, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("tipo", "valor", "objeto"),)


class DisciplinaArtistica(models.Model):
    '''
    Disciplinas artisitas a la que corresponden las obras
    Ejemplos: Dibujo  / Fotografía directa analógica / diapositiva /
    Fotografíá digital
    '''
    nombre = models.CharField(max_length=80)

    def __str__(self):
        return self.nombre


class TecnicaArtistica(models.Model):
    ''' técnicas artisitas usadas en las obras
        Ejemplos: Lápiz '''
    nombre = models.CharField(max_length=80)

    def __str__(self):
        return self.nombre


class MaterialArtistico(models.Model):
    ''' materiales usados para la obra
        Ejemplos: Yeso / Bronce / etc '''
    nombre = models.CharField(max_length=80)

    def __str__(self):
        return self.nombre


class ModoincorporacionObra(models.Model):
    ''' Modo de acceso de la obra
        ejemplos: Compra / Donacion / Premio concurso'''

    nombre = models.CharField(max_length=80)

    def __str__(self):
        return self.nombre


class ObraDeArte(models.Model):
    '''
    Cada obra de arte registrada. En principio en propiedad del municipio
    pero podríá haber otras
    '''
    titulo = models.CharField(max_length=180)
    autores = models.ManyToManyField(Artista, blank=True)
    observaciones_de_los_autores = models.TextField(null=True, blank=True)

    disciplinas = models.ManyToManyField(DisciplinaArtistica, blank=True)

    tecnicas = models.ManyToManyField(TecnicaArtistica, blank=True)
    observaciones_de_las_tecnicas = models.TextField(null=True, blank=True)

    materiales = models.ManyToManyField(MaterialArtistico, blank=True)
    observaciones_de_los_materiales = models.TextField(null=True, blank=True)

    anio_de_realizacion = models.PositiveIntegerField(default=0)
    anio_de_incorporacion = models.PositiveIntegerField(default=0)
    observaciones_de_las_incorporacion = models.TextField(
        null=True, blank=True)

    publicado = models.BooleanField(default=True)

    observaciones_publicas = RichTextField(
        help_text='Info extra para mostrar', null=True, blank=True)
    observaciones_privadas = models.TextField(null=True, blank=True)

    modo_incorporacion = models.ForeignKey(
        ModoincorporacionObra,
        blank=True,
        null=True,
        on_delete=models.SET_NULL)
    # en que medida es propiedad del municipio
    PROP_COMPLETA = 10
    PROP_PARCIAL = 20
    PROP_NO = 30
    PROP_PRESTAMO = 40

    propiedades = (
        (PROP_COMPLETA,
         'La obra es completamente propiedad del gobierno'),
        (PROP_PARCIAL,
         'La obra es parcialmente propiedad del gobierno'),
        (PROP_NO,
         'La obra NO es propiedad del gobierno ni esta en su poder'),
        (PROP_PRESTAMO,
         'La obra esta a prestamos en el gobierno'),
    )

    propiedad = models.PositiveIntegerField(
        choices=propiedades, default=PROP_COMPLETA)

    observaciones_de_la_propiedad_de_la_obra = models.TextField(
        null=True, blank=True)

    # en caso de que corresponda
    dimensiones_ancho_centimetros = models.PositiveIntegerField(default=0)
    dimensiones_alto_centimetros = models.PositiveIntegerField(default=0)
    dimensiones_profundidad_centimetros = models.PositiveIntegerField(
        default=0)

    def __str__(self):
        return self.titulo


class ImagenesObras(models.Model):
    ''' imagenes de la obra de arte '''
    obra = models.ForeignKey(ObraDeArte, on_delete=models.CASCADE)
    foto = VersatileImageField(
        upload_to='imagenes/obras-de-arte',
        ppoi_field='ppoi')
    ppoi = PPOIField('Punto de interes de la imagenes')
    orden = models.PositiveIntegerField(default=100)

    def __str__(self):
        return 'foto de {}'.format(self.obra.titulo)
