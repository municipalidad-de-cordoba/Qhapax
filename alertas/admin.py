from django.contrib import admin
from .models import (Alerta, FotoAlerta, InicioFinAlerta,
                     TipoAlerta, LugarAlerta)


class LugarAlertaAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    list_display = ('nombre', 'latitud', 'longitud')
    ordering = ['nombre']


class TipoAlertaAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    list_display = ('nombre', )
    ordering = ['nombre']


class InicioFinAlertaInline(admin.StackedInline):
    model = InicioFinAlerta
    extra = 1


class FotoAlertaInline(admin.StackedInline):
    model = FotoAlerta
    extra = 1


class AlertaAdmin(admin.ModelAdmin):
    search_fields = ['titulo', 'descripcion']
    list_display = (
        'titulo',
        'publicado',
        'gravedad',
        'tipo',
        'lugar',
        'mas_info_url')
    list_filter = ['tipo', 'lugar', 'gravedad']
    ordering = ['titulo']
    inlines = [InicioFinAlertaInline, FotoAlertaInline]


admin.site.register(LugarAlerta, LugarAlertaAdmin)
admin.site.register(TipoAlerta, TipoAlertaAdmin)
admin.site.register(Alerta, AlertaAdmin)
