from django.db import models
from versatileimagefield.fields import VersatileImageField, PPOIField


class LugarAlerta(models.Model):
    '''
    lugar donde se indica el alerta
    '''
    nombre = models.CharField(max_length=250)
    descripcion = models.TextField(
        verbose_name='Descripcion del lugar',
        null=True,
        blank=True)
    latitud = models.DecimalField(max_digits=11, decimal_places=8, default=0.0)
    longitud = models.DecimalField(
        max_digits=11, decimal_places=8, default=0.0)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Lugares de Alertas"
        verbose_name = "Lugar de Alerta"
        ordering = ['nombre']


class TipoAlerta(models.Model):
    # Paro, corte en calle, asamblea municipal, etc
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Tipos de alertas"
        verbose_name = "Tipo de Alerta"
        ordering = ['nombre']


class Alerta(models.Model):
    URGENTE = 10
    IMPORTANTE = 20
    NORMAL = 30

    gravedades = ((URGENTE, 'Urgente'), (IMPORTANTE,
                                         'Importante'), (NORMAL, 'Normal'))

    titulo = models.CharField(max_length=250)
    descripcion = models.TextField(
        verbose_name='Descripcion de Alerta',
        null=True,
        blank=True)
    tipo = models.ForeignKey(TipoAlerta)
    lugar = models.ForeignKey(LugarAlerta)
    mas_info_url = models.URLField(null=True, blank=True)
    gravedad = models.PositiveIntegerField(
        choices=gravedades, default=IMPORTANTE)
    publicado = models.BooleanField(default=True)

    def momentos(self):
        return self.iniciofinalerta_set.all()

    def fotos(self):
        return self.fotoalerta_set.all()

    def __str__(self):
        return self.titulo

    class Meta:
        ordering = ['titulo']


class InicioFinAlerta(models.Model):
    """ cada alerta puede tener repeticiones """
    alerta = models.ForeignKey(Alerta)
    inicia = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True)
    termina = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True)

    def __str__(self):
        return "Alerta {} - {}".format(self.inicia, self.termina)

    class Meta:
        ordering = ['-inicia']


class FotoAlerta(models.Model):
    alerta = models.ForeignKey(Alerta)
    descripcion = models.CharField(
        max_length=250,
        verbose_name='Descripcion del foto',
        null=True,
        blank=True)
    ppoi = PPOIField('Punto de interes de la imagenes')
    foto = VersatileImageField(upload_to='imagenes/alertas', ppoi_field='ppoi')

    def __str__(self):
        return "Foto de Alerta {} - {}".format(
            self.alerta.titulo, self.descripcion)

    class Meta:
        ordering = ['descripcion']
