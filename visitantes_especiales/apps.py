from django.apps import AppConfig


class VisitantesEspecialesConfig(AppConfig):
    name = 'visitantes_especiales'
