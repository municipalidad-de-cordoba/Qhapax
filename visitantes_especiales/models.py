from django.db import models
# los visitantes heredan de la clase Persona
from core.models import Persona, Comunicacion
# las visitas de estos visitantes podrán estar vinculada a algunas
# actividades cargadas al sistema
from eventospublicos.models import EventoGeneral, ActividadPublica
from ckeditor.fields import RichTextField
from django.utils import timezone
from django.contrib.gis.db import models as models_geo


class VisitanteImportante(Persona):
    """
    visitante importante a la ciudad. Lo agregamos para colaborar en su agenda
    y darle servicios
    """

    cargo_rango_o_profesion = models.CharField(
        max_length=90, null=True, blank=True)
    # identificar si se publica una ficha pública
    publicado = models.BooleanField(default=False)
    observaciones_internas = models.TextField(
        null=True, blank=True, help_text=('Notas internas (nuestras) sobre el '
                                          'visitante'))
    observaciones_publicas = models.TextField(
        null=True,
        blank=True,
        help_text=('Detalles públicos de porque lo consideramos vistante '
                   'importante'))

    '''
    def get_absolute_url(self):
        return reverse('visitanteimportante.ficha', kwargs={'pk': self.pk})
    '''

    def __str__(self):
        return 'Visitante: {} {}. {}'.format(
            self.nombre, self.apellido, self.cargo_rango_o_profesion)


class EventoVisita(models.Model):
    ''' evento o motivo de la vista '''
    titulo_evento = models.CharField(max_length=90, null=True, blank=True)

    # opcionalmente puede estar conectado a un evento de nuestra agenda
    evento = models.ForeignKey(EventoGeneral, null=True, blank=True)

    notas_privadas_generales_a_visitantes = models.TextField(
        null=True, blank=True, help_text=(
            'Aquí colocamos datos '
            'de referencia general sobre traslados, organizadores internos '
            'y otra información de interés'))

    def __str__(self):
        nombre = self.titulo_visita if self.evento is None else self.evento.nombre
        return nombre


class VisitaVisitante(models.Model):
    """ cada una de las visitas programadas por el visitante """

    evento = models.ForeignKey(EventoVisita)
    visitante = models.ForeignKey(VisitanteImportante)

    # relevancia de la vista
    REL_MUY_IMPORTANTE = 10
    REL_IMPORTANTE = 20
    REL_NORMAL = 30
    REL_BAJA = 40
    relevancias = ((REL_MUY_IMPORTANTE, 'Muy importante'),
                   (REL_IMPORTANTE, 'Importante'),
                   (REL_NORMAL, 'Normal'),
                   (REL_BAJA, 'Baja')
                   )

    relevancia = models.PositiveIntegerField(
        choices=relevancias, default=REL_IMPORTANTE)

    # identificar si se publica una ficha del visitante
    publicado = models.BooleanField(default=False)
    observaciones_internas = models.TextField(
        null=True, blank=True, help_text=('Notas internas sobre esta '
                                          'participación'))
    observaciones_publicas = models.TextField(
        null=True,
        blank=True,
        help_text='Detalles públicos de porque participa del evento')

    datos_de_ayuda_al_visitante = models.TextField(
        null=True,
        blank=True,
        help_text=('Info de contactos, traslados y detalles de utilidad '
                   'especifica a ESTE visitante'))

    def __str__(self):
        return 'Visita de {} {}'.format(
            self.visitante.nombre, self.visitante.apellido)


class Alojamiento(models.Model):
    """ Cada alojamiento """
    nombre = models.CharField(max_length=90)
    ubicacion = models_geo.PointField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class ComunicacionAlojamiento(Comunicacion):
    objeto = models.ForeignKey(Alojamiento, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("tipo", "valor", "objeto"),)


class AlojamientoVisitante(models.Model):
    """ Cada alojamiento de un visitante """
    visita = models.ForeignKey(VisitaVisitante)
    alojamiento = models.ForeignKey(Alojamiento)

    momento_ingreso = models.DateTimeField(null=True, blank=True)
    momento_egreso = models.DateTimeField(null=True, blank=True)

    observaciones_internas = models.TextField(
        null=True, blank=True, help_text='Notas internas sobre el alojamiento')

    def __str__(self):
        return '{} {} en {}'.format(
            self.visita.visitante.nombre,
            self.visita.visitante.apellido,
            self.alojamiento.nombre)


class VisitanteActividad(models.Model):
    """ cada evento al que el visitante asiste """

    visita = models.ForeignKey(VisitaVisitante)
    # la visita debe ser en el marco de un evento
    actividad = models.ForeignKey(ActividadPublica)

    # tipo de participación
    PAR_DESCONOCIDO = 10
    PAR_OYENTE = 20
    PAR_DISERTANTE = 30

    participaciones = ((PAR_DESCONOCIDO, 'Desconocido'),
                       (PAR_OYENTE, 'Oyente'),
                       (PAR_DISERTANTE, 'Disertante'),
                       )

    participacion = models.PositiveIntegerField(
        choices=participaciones, default=PAR_DESCONOCIDO)

    # identificar si se publica una ficha del visitante
    publicado = models.BooleanField(default=False)
    observaciones_internas = models.TextField(
        null=True, blank=True, help_text='Notas internas sobre esta participación')
    observaciones_publicas = models.TextField(
        null=True,
        blank=True,
        help_text='Detalles públicos de porque participa del evento')

    informacion_para_el_traslado = models.TextField(
        null=True,
        blank=True,
        help_text='Detalles del traslado (empresa, vias de contacto, etc)')

    def __str__(self):
        return 'Visita de {} {} a actividad {}'.format(
            self.visita.visitante.nombre,
            self.visita.visitante.apellido,
            self.actividad.titulo)


class MensajeGeneral(models.Model):
    ''' mensaje a visitantes de un evento (y otros filtros) '''

    evento = models.ForeignKey(EventoVisita)
    titulo = models.CharField(max_length=35)
    mensaje = RichTextField(null=True, blank=True)
    visible_desde = models.DateTimeField(default=timezone.now)

    solo_relevancia_mayor_o_igual_que = models.PositiveIntegerField(
        choices=VisitaVisitante.relevancias,
        null=True,
        blank=True,
        default=None,
        help_text='Deje vacío si es para todos')

    solo_en_alojamiento = models.ForeignKey(
        Alojamiento,
        null=True,
        blank=True,
        default=None,
        help_text='Deje vacío si es para todos')

    solo_asistentes_a_actividad = models.ForeignKey(
        ActividadPublica,
        null=True,
        blank=True,
        default=None,
        help_text='Deje vacío si es para todos')
