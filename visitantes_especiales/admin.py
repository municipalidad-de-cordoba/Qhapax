from django.contrib import admin
from core.models import ComunicacionPersona
from .models import (
    ComunicacionAlojamiento,
    Alojamiento,
    VisitaVisitante,
    AlojamientoVisitante,
    VisitanteActividad,
    VisitanteImportante,
    EventoVisita,
    MensajeGeneral
)
from import_export.admin import ImportExportModelAdmin
from core.admin import QhapaxOSMGeoAdmin


class ComunicacionInline(admin.StackedInline):
    ''' víás de comunicación con el visitante '''
    model = ComunicacionPersona
    extra = 1


class ComunicacionAlojamientoInLine(admin.StackedInline):
    ''' víás de comunicación con el visitante '''
    model = ComunicacionAlojamiento
    extra = 1


class AlojamientoInLine(admin.StackedInline):
    model = Alojamiento
    extra = 1


class VisitaVisitanteInLine(admin.StackedInline):
    model = VisitaVisitante
    extra = 1


class AlojamientoVisitanteInLine(admin.StackedInline):
    model = AlojamientoVisitante
    extra = 1


class VisitanteActividadInLine(admin.StackedInline):
    model = VisitanteActividad
    extra = 1


@admin.register(VisitanteImportante)
class VisitanteImportanteAdmin(ImportExportModelAdmin):
    list_display = [
        'id',
        'cargo_rango_o_profesion',
        'nombre',
        'apellido',
        'unique_id']
    search_fields = [
        'nombre',
        'apellido',
        'unique_id',
        'cargo_rango_o_profesion']
    list_filter = []
    exclude = ['slug']
    ordering = ['-id']
    inlines = [ComunicacionInline]  # , VisitaVisitanteInLine]


@admin.register(EventoVisita)
class EventoVisitaAdmin(admin.ModelAdmin):
    list_display = ['id', 'titulo_evento', 'evento']
    search_fields = ['titulo_evento', 'evento__titulo', 'evento__descripcion']
    list_filter = ['evento']
    ordering = ['-id']
    inlines = []


@admin.register(VisitaVisitante)
class VisitaVisitanteAdmin(admin.ModelAdmin):
    list_display = ['id', 'evento', 'visitante', 'relevancia']
    search_fields = ['evento', 'visitante']
    list_filter = ['evento']
    ordering = ['-id']
    inlines = [AlojamientoVisitanteInLine]  # , VisitanteActividadInLine]


@admin.register(Alojamiento)
class AlojamientoAdmin(QhapaxOSMGeoAdmin):
    list_display = ['id', 'nombre']
    search_fields = ['nombre']
    list_filter = []
    ordering = ['-id']
    inlines = [ComunicacionAlojamientoInLine]


@admin.register(AlojamientoVisitante)
class AlojamientoVisitanteAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'visita',
        'alojamiento',
        'momento_ingreso',
        'momento_egreso']
    search_fields = ['visita', 'alojamiento']
    list_filter = ['alojamiento']
    ordering = ['-id']
    inlines = []


@admin.register(VisitanteActividad)
class VisitanteActividadAdmin(admin.ModelAdmin):
    list_display = ['id', 'visita', 'actividad', 'participacion', ]
    search_fields = []
    list_filter = []
    ordering = ['-id']
    inlines = []


@admin.register(MensajeGeneral)
class MensajeGeneralAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'evento',
        'titulo',
        'visible_desde',
        'solo_relevancia_mayor_o_igual_que',
        'solo_en_alojamiento',
        'solo_asistentes_a_actividad']
    search_fields = ['titulo', 'mensaje']
    list_filter = [
        'solo_relevancia_mayor_o_igual_que',
        'solo_en_alojamiento',
        'solo_asistentes_a_actividad']
    ordering = ['-id']
    inlines = []
