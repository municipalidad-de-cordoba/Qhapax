# Visitantes importantes

Modulo de visitantes importantes y su agenda.
Desarrollado para brindarles el servicio de información necesaria para que sus actividades puedan desempeñarse lo mejor posible.


## Instrucciones para la carga

Antes del comienzo de un evento y a los fines de notificar e informar a los visitantes correctamente se deben seguir los siguientes pasos

Cargar los alojamientos (ubicándolos en el mapa) que serán utilizados.  
Revisar si habrá un evento general en el módulo de actividades públicas. Si es así, cargarlo.  
Cargar todos los visitantes que vendrán
Cargar un evento en este módulo conectado al evento (si corresponde) de actividades públicas.  
Cargar a cada visitante en este nuevo evento en _"visita visitante"_
Cargar el alojamiento que usará cada visitante en _"alojamiento visitante"_
