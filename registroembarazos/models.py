from django.db import models
from django.contrib.gis.db import models as models_geo
from django.utils import timezone
from core.models import Persona
from barrios.models import Barrio


class Embarazo(models.Model):
    ''' cada embarazo particular '''
    madre = models.ForeignKey(Persona)  # aqui tenemos su email y su celular
    fecha_estimada_ultima_menstruacion = models.DateField(null=True, blank=True)
    # necesito ubicar aproximadamente (para respetar privacidad) para 
    # indicarle actividades cercanas 
    barrio = models.ForeignKey(Barrio, null=True, blank=True)
    cantidad_de_embarazos_anteriores = models.PositiveIntegerField(null=True, default=None)

    creado = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Embarazo {}'.format(self.madre.nombre)

    class Meta:
        ordering = ['-fecha_estimada_ultima_menstruacion']
        verbose_name_plural = "Embarazos"
        verbose_name = "Embarazo"


class CompaneroEmbarazo(models.Model):
    ''' personas que acompañan el embarazo y recibiran todas las notificaciones (pareja, madre, amigas, etc) '''
    embarazo = models.ForeignKey(Embarazo)
    companero = models.ForeignKey(Persona)  # aqui tenemos su email y su celular

    # relación con la embarazada
    REL_PAREJA = 10
    REL_PADRE = 20
    REL_HERMANO = 30
    REL_OTROS_PARIENTES = 40
    REL_AMIGO = 50

    relaciones_con_la_madre = ((REL_PAREJA, 'Pareja de la embarazada'),
                                (REL_PADRE, 'Madre o Padre de la embarazada'),
                                (REL_HERMANO, 'Hermana o Hermano de la embarazada'),
                                (REL_OTROS_PARIENTES, 'Otros parientes de la embarazada'),
                                (REL_AMIGO, 'Amiga o amigo de la pareja'))

    relacion_con_la_madre = models.PositiveIntegerField(choices=relaciones_con_la_madre)
    creado = models.DateTimeField(auto_now_add=True)


class ControlEmbarazo(models.Model):
    ''' cada control que detectamos del embarazo '''
    embarazo = models.ForeignKey(Embarazo)
    fecha = models.DateField(default=timezone.now, null=True, blank=True)
    observaciones = models.TextField(null=True, blank=True)
    incluye_ecografia = models.NullBooleanField(default=None)
    incluye_laboratorio = models.NullBooleanField(default=None)
    incluye_ecografia = models.NullBooleanField(default=None)
    incluye_odontologia = models.NullBooleanField(default=None)
    peso = models.PositiveIntegerField(null=True, default=None)

    creado = models.DateTimeField(auto_now_add=True)


class TipoNovedadEmbarazada(models.Model):
    ''' tipo de novedades a enviar: cursos preparto, entregas de leche, etc '''
    nombre = models.CharField(max_length=90)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre

class NotificacionEmbarazada(models.Model):
    ''' ABSTRACTA novedades en general, hay de tipo calendario (siempre se envían) 
        y particulares para entregas de leche o gimnasia preparto, etc '''
    tipo = models.ForeignKey(TipoNovedadEmbarazada)
    titulo = models.CharField(max_length=90)
    descripcion = models.TextField(null=True, blank=True)
    mas_info_url = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.titulo


class NovedadParticularEmbarazada(NotificacionEmbarazada):
    ''' tipo de novedades a enviar: cursos preparto, ecografías móviles, entregas de leche, etc '''
    #FIXME en algunos casos van a ser centros de salud
    lugar_ubicacion = models_geo.PointField(null=True, blank=True)
    nombre_ubicacion = models.CharField(max_length=90)
    direccion_ubicacion = models.CharField(max_length=190)
    fecha_novedad = models.DateTimeField()

    notificar_desde_semana = models.PositiveIntegerField(default=1)
    notificar_hasta_semana = models.PositiveIntegerField(default=40)


class CalendarioEmbarazada(NotificacionEmbarazada):
    ''' tipo de novedades a enviar: cursos preparto, ecografías móviles, entregas de leche, etc '''
    notificar_en_semana = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ['notificar_en_semana']


class StatusNotificacionNovedadEmbarazo(models.Model):
    ''' recepcion y respuesta a la notificacion de una embarazada o sus compañeros '''
    notificacion = models.ForeignKey(NotificacionEmbarazada)
    embarazo = models.ForeignKey(Embarazo)
    companero = models.ForeignKey(CompaneroEmbarazo, null=True, blank=True)  # solo cuando no es la embarazada misma la que responde
    enviada = models.DateTimeField(null=True, blank=True)
    recibida = models.DateTimeField(null=True, blank=True)

    creado = models.DateTimeField(auto_now_add=True)

    # siempre preguntamos opinion de las notificaciones
    OP_NO_RESPONDIDO = 10
    OP_MUY_UTIL = 20
    OP_UTIL = 30
    OP_POCO_UTIL = 40
    
    opiniones_utilidad = ((OP_MUY_UTIL, 'Muy útil'),
                        (OP_UTIL, 'Util'),
                        (OP_POCO_UTIL, 'Poco útil'))

    opinion_utilidad = models.PositiveIntegerField(choices=opiniones_utilidad, default=OP_NO_RESPONDIDO)