#!/usr/bin/python
from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from datetime import datetime
from django.contrib.gis.geos import Point, Polygon
from fastkml import kml
from geo.models import Provincia, Departamento


class Command(BaseCommand):
    """
    Particularmente, vamos a importar los departamentos de la provincia de
    Córdoba
    """
    help = """Comando para importar departamentos de una provincia al sistema desde un .kml"""

    def add_arguments(self, parser):
        parser.add_argument('--path', type=str, help='Path del archivo KML local')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación de departamentos'))
        url = options['path']

        try:
            with open(url, 'rt', encoding="utf-8") as myfile: doc=myfile.read().encode('utf-8')
            self.stdout.write(self.style.SUCCESS('Buscando datos en {}'.format(url)))
        except Exception as e:
            self.stdout.write(self.style.ERROR('Error al leer de {}'.format(url)))
            sys.exit(1)

        provincia, prov_created = Provincia.objects.get_or_create(nombre='Córdoba')
        # Create the KML object to store the parsed result
        k = kml.KML()

        # Read in the KML string
        k.from_string(doc)
        features = list(k.features())

        dptos = 0 #areas descargados a la base de datos
        dptos_actualizados = 0
        for feature in features:
            self.stdout.write("=============================")
            self.stdout.write("Doc: {}".format(feature.name))
            self.stdout.write("=============================")
            folders = list(feature.features())
            for folder in folders:
                self.stdout.write("=============================")
                self.stdout.write("Folder: {}".format(folder.name))
                self.stdout.write("=============================")
                places = list(folder.features())
                for place in places:
                    self.stdout.write("=============================")
                    self.stdout.write("Importando {}".format(place.name))
                    self.stdout.write("=============================")
                    dpto, created = Departamento.objects.get_or_create(nombre=place.name)
                    dpto.poligono = Polygon([xy[0:2] for xy in place.geometry.exterior.coords])
                    dpto.provincia = provincia
                    dpto.save()
                    if created:
                        dptos += 1
                    else:
                        dptos_actualizados += 1

        self.stdout.write(self.style.SUCCESS('Departamentos importados nuevos: {}'.format(dptos)))
        self.stdout.write(self.style.SUCCESS('Departamentos actualizados: {}'.format(dptos_actualizados)))
        self.stdout.write("FIN")
