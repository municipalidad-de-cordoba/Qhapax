from django.contrib import admin
from .models import Ciudad, Provincia, Pais, Departamento
from core.admin import QhapaxOSMGeoAdmin


class ProvinciaAdmin(QhapaxOSMGeoAdmin):
    list_display = ("nombre",)


class DepartamentoAdmin(QhapaxOSMGeoAdmin):
    list_display = ("nombre",)


class PaisAdmin(QhapaxOSMGeoAdmin):
    list_display = ("nombre",)


class CiudadAdmin(admin.ModelAdmin):
    list_display = ("nombre",)


admin.site.register(Provincia, ProvinciaAdmin)
admin.site.register(Pais, PaisAdmin)
admin.site.register(Ciudad, CiudadAdmin)
admin.site.register(Departamento, DepartamentoAdmin)
