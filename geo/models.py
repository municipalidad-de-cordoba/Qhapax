from django.db import models
from django.contrib.gis.db import models as geo_models
#from nominatim import Nominatim
from geopy.geocoders import Nominatim
import re

class Pais(models.Model):
    nombre = models.CharField(max_length=250)
    poligono = geo_models.PolygonField(
    null=True,
    blank=True,
    help_text='Poligono que representa el perímetro nacional')

    def __str__(self):
        return self.nombre
    class Meta:
        verbose_name= "País"
        verbose_name_plural = "Países"


class Provincia(models.Model):
    pais = models.ForeignKey(Pais, null=True, blank=True, on_delete=models.SET_NULL)
    nombre = models.CharField(max_length=250)
    poligono = geo_models.PolygonField(
        null=True,
        blank=True,
        help_text='Poligono que representa el perímetro provincial')

    def __str__(self):
        return self.nombre


class Departamento(models.Model):
    provincia = models.ForeignKey(Provincia, null=True, blank=True, on_delete=models.SET_NULL)
    nombre = models.CharField(max_length=250)
    poligono = geo_models.PolygonField(
        null=True,
        blank=True,
        help_text='Poligono que representa el perímetro departamental')

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']


class Ciudad(models.Model):
    departamento = models.ForeignKey(Departamento, null=True, blank=True, on_delete=models.SET_NULL)
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = "Ciudad"
        verbose_name_plural = "Ciudades"

# ----------------------------------
# ----------------------------------
# LO QUE SIGUE ESTA SUJETO A REVISION
# ----------------------------------
# ----------------------------------
class Direccion(models.Model):
    """
    Modelo Genérico para almacenar direcciones
    """
    direccion = models.CharField(max_length=250)
    calle = models.CharField(max_length=250, null=True, blank=True)
    piso = models.CharField(max_length=250, null=True, blank=True)
    numero = models.PositiveSmallIntegerField(null=True, blank=True)
    localidad = models.CharField(max_length=250, null=True, blank=True)
    provincia = models.ForeignKey(Provincia, on_delete=models.CASCADE, null=True, blank=True)
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE, null=True, blank=True)
    cp = models.CharField(max_length=250, null=True, blank=True)

    descripcion = models.TextField(verbose_name='Descripcion del lugar', null=True, blank=True)

    latitud = models.DecimalField(max_digits=11, decimal_places=8, default=0.0)
    longitud = models.DecimalField(max_digits=11, decimal_places=8, default=0.0)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if self.latitud == 0.0:
            nom = Nominatim()
            try:
                print("dire: %s" % self.direccion)
                result = nom.geocode(self.direccion, "ES", 5)
                if result:
                    result=result[0]
                    print("Result: %s" % result)

                    self.latitud = result.latitude
                    self.longitud = result.longitude
                    print("Display --> %s" % result.address)
                    matc = re.match(r"(?P<numero>\d+, )?(?P<calle>.*), (?P<localidad>.*), (?P<provincia_abreviada>.*), (?P<comuna>.*), (?P<provincia>.*) (?P<cp>\w+), (?P<pais>.*)",result.address)
                    if matc is not None:
                        display_name = matc.groupdict()
                    else:  # en algunos casos devuelve None! #TODO, esta mal hacer esto en el save de esta forma.
                        display_name = result.address

                    try:
                        self.numero = int(display_name.get("numero").replace(",","").strip())
                    # si no viene el número, viene None
                    except AttributeError:
                        pass

                    self.calle = display_name.get("calle")
                    self.localidad = display_name.get("localidad")
                    self.provincia = Provincia.objects.get_or_create(nombre=display_name.get("provincia"))[0]
                    self.cp = display_name.get("cp")
                    self.pais = Pais.objects.get_or_create(nombre=display_name.get("pais"))[0]
            except IndexError:
                pass

        super(Direccion, self).save(*args, **kwargs)


    def __str__(self):
        if self.latitud == 0.0:
            return self.direccion

        return "%s %s, %s, %s, %s, %s" %(self.calle, self.numero, self.localidad, self.provincia, self.cp, self.pais)

    class Meta:
        verbose_name = "Dirección"
        verbose_name_plural = "Direcciones"
