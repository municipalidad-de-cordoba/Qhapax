from django.views.generic.detail import DetailView
from django.views.generic import DetailView
from braces.views import StaffuserRequiredMixin
from .models import Direccion


class GeoCodeAddressView(StaffuserRequiredMixin, DetailView):
    model = Direccion
    template_name = 'geo/direccion.html'
