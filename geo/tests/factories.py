import factory
from factory.django import DjangoModelFactory


class PaisFactory(DjangoModelFactory):
    class Meta:
        model = 'geo.Pais'

    nombre = factory.Faker('country', locale='es_ES')


class PaisFactory(DjangoModelFactory):
    class Meta:
        model = 'geo.Provincia'

    nombre = factory.Faker('state_name', locale='es_ES')


class DireccionFactory(DjangoModelFactory):
    class Meta:
        abstract = True
    direccion = factory.Sequence(lambda n: 'Agustín Tosco {}00, Córdoba, Argentina'.format(n))
    latitud = '-31.417301'   # skip openstreetmap
    longitud = '-64.191026'

