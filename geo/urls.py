from django.conf.urls import url
from . import views


urlpatterns = [url(r'direccion/(?P<pk>[0-9]+)/$',
                   views.GeoCodeAddressView.as_view(),
                   name='geo.direccion.json'),
               ]
