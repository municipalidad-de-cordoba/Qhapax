from .models import RamaActividad, CategoriaActividad, ActividadOTA
from django.http import Http404
from django.views.decorators.cache import cache_page
import django_excel as excel


@cache_page(60 * 60 * 12)  # 12 h
def planilla_tributaria(request, anio, filetype):
    '''
    Planilla de tarifaria
    http://localhost:8000/tarifaria/lista-de-rubros-A.csv (A=anio)
    '''
    ramas = RamaActividad.objects.all().filter(ordenanza__anio=anio)

    # En caso de que el año no exista, muestro error 404.
    if ramas.count() == 0:
        raise Http404("No existe el año!")

    csv_list = []
    csv_list.append(['Nº Código',
                     'Alíc.',
                     'Mínimo',
                     'Actividad ID',
                     'Descripción',
                     'Rama ID',
                     'Rama',
                     'Categoria ID',
                     'Categoria'])

    for rama in ramas:
        categorias = CategoriaActividad.objects.all().filter(rama=rama)
        for categoria in categorias:
            actividades = ActividadOTA.objects.all().filter(categoria=categoria)
            for actividad in actividades:
                csv_list.append([
                    actividad.codigo,
                    actividad.alicuota_num,
                    actividad.minimo,
                    actividad.id,
                    actividad.nombre,
                    rama.id,
                    rama.nombre,
                    categoria.id,
                    categoria.nombre
                ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
