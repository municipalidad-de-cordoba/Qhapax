# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-04-05 20:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ActividadOTA',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', models.CharField(max_length=100)),
                ('nombre', models.TextField()),
                ('alicuota', models.CharField(max_length=100)),
                ('minimo', models.DecimalField(decimal_places=2, default=0.0, max_digits=14)),
            ],
            options={
                'ordering': ['nombre'],
            },
        ),
        migrations.CreateModel(
            name='CategoriaActividad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=250)),
            ],
            options={
                'ordering': ['nombre'],
            },
        ),
        migrations.CreateModel(
            name='RamaActividad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=250)),
            ],
            options={
                'ordering': ['nombre'],
            },
        ),
        migrations.AddField(
            model_name='categoriaactividad',
            name='rama',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='ordenanzatributaria.RamaActividad'),
        ),
        migrations.AddField(
            model_name='actividadota',
            name='categoria',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='ordenanzatributaria.CategoriaActividad'),
        ),
    ]
