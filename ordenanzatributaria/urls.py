from django.conf.urls import url

from . import views

urlpatterns = [url(r'^lista-de-rubros-(?P<anio>[0-9]+).(?P<filetype>csv|xls)$',
                   views.planilla_tributaria, name='tributario.lista'), ]
