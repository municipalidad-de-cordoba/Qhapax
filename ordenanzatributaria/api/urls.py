from django.conf.urls import url, include
from rest_framework import routers
from .views import CategoriaActividadViewSet, RamaActividadViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'ordenanzatributaria',
    CategoriaActividadViewSet,
    base_name='ordenanza.tributaria.api.lista')
router.register(
    r'ordenanzatributaria-ramas',
    RamaActividadViewSet,
    base_name='ordenanza.tributaria.ramas.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
