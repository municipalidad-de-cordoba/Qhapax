from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import CategoriaActividadSerializer, RamaActividadSerializer
from api.pagination import DefaultPagination
from ordenanzatributaria.models import CategoriaActividad, RamaActividad
from django.db.models import Count

class CategoriaActividadViewSet(viewsets.ModelViewSet):
    """
    Categorías de Ordenanza tributaria.
    Se puede filtrar por:
    -id_rama: obtenido de https://gobiernoabierto.cordoba.gob.ar/api/v2/ordenanzatributaria/ordenanzatributaria-ramas/
     Pueden ser varios ids separados por comas.
    """
    serializer_class = CategoriaActividadSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):

        # excluyo actividades que no tienen asociados locales
        queryset = CategoriaActividad.objects.filter(
                rama__ordenanza__anio=2017).annotate(
                cantidad_locales=Count('categorias_actividades__locales')
                ).filter(cantidad_locales__gt=0)

    # -año: 2017|2018 Filtra por las categorías según los años de la ordenanza.
    #  Solo tenemos registradas del 2017 y 2018. Ej:
    #  /?año=2017
        # año = self.request.query_params.get('año', None)
        # if año is not None:
        #     queryset = queryset.filter(rama__ordenanza__anio=año)

        id_rama = self.request.query_params.get('id_rama', None)
        if id_rama is not None:
            ids_ramas = id_rama.split(',')
            queryset = queryset.filter(rama__id__in=ids_ramas)

        # codigo = self.request.query_params.get('codigo', None)
        # if codigo is not None:
        #     queryset = queryset.filter(codigo=codigo)

        # id_categoria = self.request.query_params.get('id_categoria', None)
        # if id_categoria is not None:
        #     queryset = queryset.filter(categoria__id=id_categoria)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class RamaActividadViewSet(viewsets.ModelViewSet):
    """
    Ramas de ordenanzas tributarias

    Párametros que se pueden usar:
    -excluir: toma el valor 'si'. Excluye las ramas que no tienen asociados
    (por el momento) locales habilitados.
    """

    serializer_class = RamaActividadSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = RamaActividad.objects.filter(ordenanza__anio=2017)

    # -año: 2017|2018 Filtra por las ramas según los años de la ordenanza.
    #  Solo tenemos registradas del 2017 y 2018. Ej:
    #  /?año=2017
    #     año = self.request.query_params.get('año', None)
    #     if año is not None:
    #         queryset = queryset.filter(ordenanza__anio=año)

        excluir = self.request.query_params.get('excluir', None)
        if excluir == 'si':
            queryset = queryset.annotate(cantidad_locales=
                Count('rama_actividades__categorias_actividades__locales')
                ).filter(cantidad_locales__gt=0)

        # codigo = self.request.query_params.get('codigo', None)
        # if codigo is not None:
        #     queryset = queryset.filter(codigo=codigo)

        # id_categoria = self.request.query_params.get('id_categoria', None)
        # if id_categoria is not None:
        #     queryset = queryset.filter(categoria__id=id_categoria)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
