from rest_framework import serializers
from ordenanzatributaria.models import (CategoriaActividad, ActividadOTA,
                                        RamaActividad)


class ActividadOTASerializer(serializers.ModelSerializer):

    class Meta:
        model = ActividadOTA
        fields = ('id', 'codigo', 'nombre')


class CategoriaActividadSerializer(serializers.ModelSerializer):
    actividades = ActividadOTASerializer(
        source='categorias_actividades', many=True)

    class Meta:
        model = CategoriaActividad
        fields = ('id', 'nombre', 'actividades')


class RamaActividadSerializer(serializers.ModelSerializer):
    categoria = CategoriaActividadSerializer(
        source='rama_actividades', many=True)

    class Meta:
        model = RamaActividad
        fields = ('id', 'nombre', 'categoria')
