from django.apps import AppConfig


class OrdenanzatributariaConfig(AppConfig):
    name = 'ordenanzatributaria'
