from django.db import models
from simple_history.models import HistoricalRecords


class OrdenanzaTributaria(models.Model):
    anio = models.PositiveIntegerField(default=2018)
    pdf_ordenanza = models.FileField(
        upload_to='ordenanza-tributaria/', null=True, blank=True)
    url = models.URLField(null=True, blank=True)

    def __str__(self):
        return 'Ordenanza {}'.format(self.anio)


class RamaActividad(models.Model):
    '''
    Agrupador principal de la actividad comercial, industrial o de servicios
    '''
    ordenanza = models.ForeignKey(OrdenanzaTributaria, null=True)
    nombre = models.CharField(max_length=250)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']


class CategoriaActividad(models.Model):
    '''
    Agrupador principal de la actividad comercial, industrial o de servicios
    '''
    rama = models.ForeignKey(
        RamaActividad,
        null=True,
        on_delete=models.SET_NULL,
        related_name='rama_actividades')
    nombre = models.CharField(max_length=250)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']


class ActividadOTA(models.Model):
    '''
    Agrupador principal de la actividad comercial, industrial o de servicios
    '''
    categoria = models.ForeignKey(
        CategoriaActividad,
        null=True,
        on_delete=models.SET_NULL,
        related_name='categorias_actividades')
    codigo = models.CharField(max_length=100)  # código unico de la actividad
    nombre = models.TextField()  # descripciones muy largas
    minimo = models.DecimalField(decimal_places=2, default=0.0, max_digits=14)
    alicuota_num = models.DecimalField(
        decimal_places=2,
        default=0.0,
        max_digits=14,
        help_text="valor expresado por miles (porcentaje / 10)")
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']
