from django.contrib import admin
from .models import (RamaActividad, CategoriaActividad, ActividadOTA,
                     OrdenanzaTributaria)
from import_export.admin import ImportExportModelAdmin


class OrdenanzaTributariaAdmin(ImportExportModelAdmin):
    list_display = ('anio', 'pdf_ordenanza', 'url')


class RamaActividadAdmin(ImportExportModelAdmin):
    def ultimo_editor(self, obj):
        return obj.history.all()[0].history_user

    search_fields = ['nombre', 'ordenanza']
    list_display = ('nombre', 'ultimo_editor')
    list_filter = ['ordenanza']


class CategoriaActividadAdmin(ImportExportModelAdmin):
    def get_rama(self, obj):
        return obj.rama.nombre

    def ultimo_editor(self, obj):
        return obj.history.all()[0].history_user

    search_fields = ['nombre', 'rama__nombre']
    list_display = ('nombre', 'get_rama', 'ultimo_editor')
    list_filter = ['rama', 'rama__ordenanza']


class ActividadOTAAdmin(ImportExportModelAdmin):
    def get_rama(self, obj):
        return obj.categoria.rama.nombre

    def get_categoria(self, obj):
        return obj.categoria.nombre

    def ultimo_editor(self, obj):
        return obj.history.all()[0].history_user

    search_fields = ['codigo', 'nombre', 'categoria__nombre']
    list_display = (
        'codigo',
        'nombre',
        'get_categoria',
        'get_rama',
        'alicuota_num',
        'minimo',
        'ultimo_editor')
    list_filter = [
        'categoria__rama__ordenanza',
        'categoria__rama',
        'categoria']


admin.site.register(RamaActividad, RamaActividadAdmin)
admin.site.register(CategoriaActividad, CategoriaActividadAdmin)
admin.site.register(ActividadOTA, ActividadOTAAdmin)
admin.site.register(OrdenanzaTributaria, OrdenanzaTributariaAdmin)
