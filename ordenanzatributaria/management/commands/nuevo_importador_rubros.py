#!/usr/bin/python
from django.core.management.base import BaseCommand

from portaldedatos.models import ArchivoCSV
from ordenanzatributaria.models import (RamaActividad, CategoriaActividad,
                                        ActividadOTA, OrdenanzaTributaria)
from django.db import transaction
import csv
import sys


class Command(BaseCommand):
    help = """Comando para importar lista de rubros de actividad de
                la ordenanza tariafaria anual
            """

    def add_arguments(self, parser):
        parser.add_argument(
            '--csv_id',
            type=int,
            help='ID del CSV cargado al sistema')

        parser.add_argument('--force', action='store_true', dest='force',
                            default=False, help="forzar a procesar el archivo")

    @transaction.atomic
    def handle(self, *args, **options):
        force = options['force']
        if force:
            self.stdout.write(self.style.WARNING(
                '--- Forzando importación de CSV ---'))

        try:
            if force:
                instanceCSV = ArchivoCSV.objects.get(pk=options['csv_id'])
            else:
                instanceCSV = ArchivoCSV.objects.get(
                    pk=options['csv_id'], procesado=False)
        except ArchivoCSV.DoesNotExist:
            if force:
                self.stdout.write(
                    self.style.ERROR(
                        'El CSV id: %s no existe' %
                        options['csv_id']))
            else:
                self.stdout.write(
                    self.style.ERROR(
                        'El CSV id: %s no existe o ya está procesado' %
                        options['csv_id']))
            sys.exit(1)

        if instanceCSV:
            self.stdout.write(
                self.style.SUCCESS(
                    'Importando csv (id: {})'.format(
                        instanceCSV.id)))

            # TIENEN QUE TENER ENCABEZADO!
            if not instanceCSV.tiene_fila_encabezado:
                self.stdout.write(self.style.ERROR(
                    'El CSV indica que no tiene encabezado. Se procesará igualmente'))

            # asegurarse de que el archivo tenga la misma estructura
            # (encabezados)
            fieldnames = [
                'Nº Código',
                'Alíc.',
                'Mínimo',
                'Descripción',
                'Rama',
                'Categoria']

            # Importo con año 2018. Cambiar esto por otro año en caso
            # necesario.
            ordenanza_trib, created = OrdenanzaTributaria.objects.get_or_create(
                anio=2018)

            count = 0
            with open(instanceCSV.archivo_local.path) as csvfile:
                reader = csv.DictReader(
                    csvfile,
                    fieldnames=fieldnames,
                    delimiter=instanceCSV.separado_por,
                    quotechar=instanceCSV.contenedor_de_texto)

                header = reader.__next__()
                if sorted(fieldnames) != sorted(list(header.values())):
                    self.stdout.write(self.style.ERROR(
                        'BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                    sys.exit(1)

                codigo_old = 0
                for row in reader:
                    # chequear los datos
                    errores = []

                    codigo = row['Nº Código'].strip()
                    if codigo == '':
                        errores.append('No tiene codigo')
                    elif codigo == codigo_old:
                        # Cuando hay duplicados en realidad es porque son varios
                        # items que pusieron en diferentes filas.
                        # El fix para esto es modificar el csv para que sea una fila sola
                        # con el codigo unico. Como deberia ser!
                        self.stdout.write(
                            self.style.ERROR(
                                'Este codigo ya existe: {}. Arreglar y volver a subir archivo corregido.'.format(codigo)))
                        sys.exit(1)

                    codigo_old = codigo

                    rama_actividad = row['Rama'].strip()
                    if rama_actividad == '':
                        errores.append('No tiene Rama')

                    categoria = row['Categoria'].strip()
                    if categoria == '':
                        errores.append('No hay Categoria')

                    descripcion = row['Descripción'].strip()
                    if descripcion == '':
                        errores.append('No hay descripcion')

                    # Tuve que reemplazar las comas por puntos y sacar el
                    # simbolo de porcentaje
                    alicuota = 0.00 if row['Alíc.'].strip() == '' else float(
                        row['Alíc.'].replace(",", ".").replace("‰", "").strip())
                    minimo = 0.00 if row['Mínimo'].strip(
                    ) == '' else float(row['Mínimo'].strip())

                    print(
                        codigo,
                        rama_actividad,
                        categoria,
                        alicuota,
                        minimo,
                        descripcion)

                    if len(errores) > 0:
                        self.stdout.write(
                            self.style.ERROR(
                                'ERRORES AL PROCESAR LA LINEA {}'.format(count)))
                        for e in errores:
                            self.stdout.write(self.style.ERROR(e))
                        sys.exit(1)

                    orama, created = RamaActividad.objects.get_or_create(
                        nombre=rama_actividad, ordenanza=ordenanza_trib)
                    ocategoria, created = CategoriaActividad.objects.get_or_create(
                        rama=orama, nombre=categoria)
                    oactividad, created = ActividadOTA.objects.get_or_create(
                        categoria=ocategoria, codigo=codigo, nombre=descripcion, alicuota_num=alicuota, minimo=minimo)

                    count += 1

            instanceCSV.procesado = True
            instanceCSV.save()

            self.stdout.write(
                self.style.SUCCESS(
                    'Archivo cargado con éxito, se cargaron {} registros'.format(count)))
