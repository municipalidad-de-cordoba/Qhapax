from django.db import models
from obras.models import Obra
from versatileimagefield.fields import VersatileImageField, PPOIField
from django.contrib.gis.db import models as models_geo
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


class EspacioVerde(Obra):
    """
    Modelo para guardar obras de espacios verdes.
    """

    # Por ahora alcanza con heredar sólo de Obra
    pass

    class Meta:
        verbose_name = 'Espacio Verde'
        verbose_name_plural = 'Espacios Verdes'


class TipoDeTrabajoEnEspacioVerde(models.Model):
    """
    En cada trazado(es decir, un trabajo en particular), se realizan distintos
    tipos de trabajos.
    """
    nombre = models.CharField(max_length=90)
    # Los trabajos traen fecha, pero se omiten y se usan para filtrar en las
    # apis las fechas de las fotos
    # fecha = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class TrazadoEspacioVerde(models.Model):
    """
    Info geo de cada parte de la obra
    """
    obra = models.ForeignKey(EspacioVerde, related_name='trazados')

    id_externo = models.PositiveIntegerField(default=0)
    descripcion_publica = models.TextField(null=True, blank=True)
    estado = models.CharField(max_length=50, null=True, blank=True)
    tipos = models.ManyToManyField(
        TipoDeTrabajoEnEspacioVerde,
        blank=True,
        related_name='Trazados')

    # campo generico espacial, puede contener puntos, líneas o polígonos
    trazado = models_geo.GeometryField(null=True)

    observaciones_internas = models.TextField(null=True, blank=True)
    publicado = models.BooleanField(default=True)
    publicado_gobierno = models.BooleanField(
        default=True, help_text='Campo para editar y aprobar localmente')

    def __str__(self):
        return '{} {}'.format(self.obra.nombre, self.descripcion_publica)

    class Meta:
        ordering = ['obra__nombre', 'descripcion_publica']
        verbose_name = 'Trazado de Espacios Verdes'
        verbose_name_plural = 'Trazados de Espacios Verdes'


class FotoDeEspacioVerde(models.Model):
    """
    Fotos de Espacios Verdes y Trazados de Espacios Verdes
    """
    obra = models.ForeignKey(
        EspacioVerde, related_name='adjuntos', null=True, blank=True,
        help_text="la foto corresponde a una obra en caso de que no existan trazados")
    trazado = models.ForeignKey(
        TrazadoEspacioVerde, related_name='adjuntos', null=True, blank=True,
        help_text="si la obra tiene trazado, no es necesario llenar el campo obra")

    id_externo = models.PositiveIntegerField(default=0)
    url = models.CharField(max_length=320, null=True, blank=True)
    fecha = models.DateTimeField(null=True, blank=True)
    publicado = models.BooleanField(default=True)
    publicado_gobierno = models.BooleanField(
        default=True, help_text='Campo para editar y aprobar localmente')

    foto = VersatileImageField(
        upload_to='imagenes/obras',
        ppoi_field='foto_ppoi',
        null=True,
        blank=True,
        max_length=200
    )
    foto_ppoi = PPOIField('Image PPOI')

    def __str__(self):
        return "Foto {} {}".format(self.id, self.id_externo)

    def clean(self):
        """
        Tanto obra como trazado pueden ser null, pero
        necesitamos que la foto tenga una referencia a obra o al trazado de
        tal obra.
        """
        if (self.obra and self.trazado) is None:
            raise ValidationError(
                _('La foto debe corresponder a un espacio verde o a un trazado'), )

    class Meta:
        verbose_name = 'Foto de Espacio Verde'
        verbose_name_plural = 'Fotos de Espacios Verdes'
