from django.apps import AppConfig


class EspaciosVerdesConfig(AppConfig):
    name = 'espacios_verdes'
