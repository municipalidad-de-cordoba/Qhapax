from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from rest_framework_gis.serializers import (GeoFeatureModelSerializer,
                                            GeometrySerializerMethodField)
from versatileimagefield.serializers import VersatileImageFieldSerializer
from espacios_verdes.models import (EspacioVerde, TrazadoEspacioVerde,
                                    FotoDeEspacioVerde)
from obras.api.serializers import BarrioObraSerializer


class FechaDeFotoDeEspacioVerdeSerializer(CachedSerializerMixin):

    class Meta:
        model = FotoDeEspacioVerde
        fields = ('fecha', )


class FotoDeEspacioVerdeSerializer(CachedSerializerMixin):
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail_125", 'thumbnail__125x125'),
                                          ("thumbnail_500", 'thumbnail__500x500')])

    class Meta:
        model = FotoDeEspacioVerde
        fields = ('id', 'foto', 'fecha')


class TrazadoEspacioVerdeSerializer(
        GeoFeatureModelSerializer,
        CachedSerializerMixin):
    obra = serializers.CharField(source='obra.nombre')
    id_obra = serializers.CharField(source='obra.id')
    tipo = serializers.StringRelatedField(
        source='obra.tipo.nombre', read_only=True)
    tipo_id = serializers.StringRelatedField(
        source='obra.tipo.id', read_only=True)
    fechas_de_trabajos = serializers.SerializerMethodField()
    descripcion_frente = serializers.CharField(source='descripcion_publica')
    punto_geografico = GeometrySerializerMethodField()

    def get_fechas_de_trabajos(self, obj):
        fotos = FotoDeEspacioVerde.objects.filter(
            trazado=obj,
            publicado=True,
            publicado_gobierno=True,
            fecha__isnull=False).distinct('fecha')
        serializer = FechaDeFotoDeEspacioVerdeSerializer(instance=fotos,
                                                         many=True)
        return serializer.data

    def get_punto_geografico(self, obj):
        return obj.trazado.centroid

    class Meta:
        model = TrazadoEspacioVerde
        geo_field = "punto_geografico"
        fields = ('id', 'descripcion_frente', 'obra', 'id_obra', 'tipo',
                  'tipo_id', 'fechas_de_trabajos')


class TrazadoEspacioVerdeConFotosSerializer(CachedSerializerMixin):
    adjuntos = serializers.SerializerMethodField('get_fotos')
    obra = serializers.CharField(source='obra.nombre')
    id_obra = serializers.CharField(source='obra.id')
    tipo = serializers.StringRelatedField(
        source='obra.tipo.nombre', read_only=True)
    tipo_id = serializers.StringRelatedField(
        source='obra.tipo.id', read_only=True)
    descripcion_frente = serializers.CharField(source='descripcion_publica')

    def get_fotos(self, container):
        fotos = FotoDeEspacioVerde.objects.filter(
            trazado=container,
            publicado=True,
            publicado_gobierno=True,
            fecha__isnull=False)
        serializer = FotoDeEspacioVerdeSerializer(instance=fotos, many=True)
        return serializer.data

    class Meta:
        model = TrazadoEspacioVerde
        fields = ('id', 'descripcion_frente', 'obra', 'id_obra', 'tipo',
                  'tipo_id', 'adjuntos')


class TrazadoEspacioVerdeSinGeoSerializer(CachedSerializerMixin):
    # adjuntos = serializers.SerializerMethodField('get_fotos')

    # def get_fotos(self, container):
    #     fotos = FotoDeEspacioVerde.objects.filter(trazado=container, publicado=True, publicado_gobierno=True)
    #     serializer = FotoDeEspacioVerdeSerializer(instance=fotos, many=True)
    #     return serializer.data

    class Meta:
        model = TrazadoEspacioVerde
        fields = ('id',)


class EspacioVerdeSerializer(CachedSerializerMixin):
    # adjuntos = serializers.SerializerMethodField('get_fotos')
    tipo = serializers.CharField(source='tipo.nombre')
    tipo_id = serializers.CharField(source='tipo.id')
    organizacion = serializers.SerializerMethodField()
    CUIT = serializers.SerializerMethodField()
    trazados = TrazadoEspacioVerdeSinGeoSerializer(many=True, read_only=True)
    barrios = BarrioObraSerializer(many=True, read_only=True)
    estado = serializers.SerializerMethodField()

    def get_estado(self, obj):
        return obj.get_estado_display()

    # def get_fotos(self, obj):
    #     fotos = FotoDeEspacioVerde.objects.filter(obra=obj, publicado=True, publicado_gobierno=True)
    #     serializer = FotoDeEspacioVerdeSerializer(instance=fotos, many=True)
    #     return serializer.data

    def get_organizacion(self, obj):
        return obj.contratista.nombre if obj.contratista is not None else 'Sin especificar'

    def get_CUIT(self, obj):
        return obj.contratista.CUIT if obj.contratista is not None else 'Sin especificar'

    class Meta:
        model = EspacioVerde
        fields = (
            'id',
            'nombre',
            'decreto',
            'estado',
            'expediente_origen',
            'descripcion_publica',
            'porcentaje_completado',
            'tipo',
            'tipo_id',
            'organizacion',
            'CUIT',
            'fecha_inicio',
            'monto',
            'fecha_finalizacion_estimada',
            'trazados',
            'barrios',
            'adjuntos')


# Registro los serializadores en la cache de DRF
cache_registry.register(EspacioVerdeSerializer)
cache_registry.register(TrazadoEspacioVerdeSerializer)
cache_registry.register(TrazadoEspacioVerdeSinGeoSerializer)
cache_registry.register(FotoDeEspacioVerdeSerializer)
