from rest_framework import viewsets
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from rest_framework.pagination import PageNumberPagination
from .serializers import (EspacioVerdeSerializer, TrazadoEspacioVerdeSerializer,
                          FotoDeEspacioVerdeSerializer,
                          TrazadoEspacioVerdeConFotosSerializer)
from api.pagination import DefaultPagination
from espacios_verdes.models import (EspacioVerde, TrazadoEspacioVerde,
                                    FotoDeEspacioVerde)
from core.utils import normalizar
from datetime import timedelta, datetime, date
from django.http import Http404


# La view de espacio verde demora mucho en cargar la gran cantidad de coordenadas
# que tiene, para solucionar esto limitamos la cantidad de objetos por página
class StandardResultsSetPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'


class EspacioVerdeViewSet(viewsets.ModelViewSet):
    """
    Lista de obras de ambiente geolocalizadas con sus datos y trazados
    Puede filtrarse por:
     - tipo_id (identificador del tipo de obra), pueden ser varios separados por comas
     - avance_desde (entero vinculado al campo "porcentaje_completado")
     - avance_hasta (entero vinculado al campo "porcentaje_completado")
     - barrios_ids: lista de barrios separados por comas
     - texto: filtra aquellas obras que contienen el texto indicado en el nombre
        de la obra o en el nombre de la org
    """
    serializer_class = EspacioVerdeSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        queryset = EspacioVerde.objects.filter(
            publicado=True, publicado_gobierno=True).order_by('nombre')

        tipos_id_str = self.request.query_params.get('tipo_id', None)
        if tipos_id_str is not None:
            tipos_id = tipos_id_str.split(',')
            queryset = queryset.filter(tipo__id__in=tipos_id)

        avance_desde = self.request.query_params.get('avance_desde', None)
        if avance_desde is not None:
            queryset = queryset.filter(
                porcentaje_completado__gte=int(avance_desde))

        avance_hasta = self.request.query_params.get('avance_hasta', None)
        if avance_hasta is not None:
            queryset = queryset.filter(
                porcentaje_completado__lte=int(avance_hasta))

        barrios_str = self.request.query_params.get('barrios_ids', None)
        if barrios_str is not None:
            barrios_ids = barrios_str.split(',')
            queryset = queryset.filter(barrios__in=barrios_ids)

        texto = self.request.query_params.get('texto', None)
        if texto is not None:
            # FIXME: acá se busca el texto que quiere el usuario, por más que
            # en la bd tenga acento lo mismo lo encuentra.
            # Si Django trae algo predefinido, usarlo acá
            copia_queryset = queryset.values(
                'id', 'nombre', 'organizacion__nombre', 'descripcion_publica')
            list_ids = []
            for obra in copia_queryset:
                if normalizar(texto) in (
                    normalizar(
                        obra['nombre']) or normalizar(
                        obra['organizacion__nombre']) or normalizar(
                        obra['descripcion_publica'])):
                    list_ids.append(obra['id'])
            queryset = queryset.filter(id__in=list_ids)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class TrazadoEspacioVerdeViewSet(viewsets.ModelViewSet):
    """
    Trazados de obras de ambiente
    Puede fitrarse:
     - solo para una obra con el parámetro id_obra
     - por tipo_id (identificador del tipo de obra), pueden ser varios separados por comas
     - obra_avance_desde (entero vinculado al campo "porcentaje_completado")
     - obra_avance_hasta (entero vinculado al campo "porcentaje_completado")
     - barrios_ids: lista de barrios separados por comas
     - texto: filtra aquellos frentes de obras que contienen el texto indicado
        en la descripcion del frente o en el nombre de la obra

      Por fechas:
     - dias(representado por un número entero): retorna los trazados que tienen
       fotos registradas en los últimos x días.
     - fecha_exacta(formato dd-mm-AAAA): retorna los trazados que tienen fotos
       registradas en la fecha ingresada.
     - desde(formato dd-mm-AAAA): retorna los trazados que tienen fotos
       registradas desde la fecha ingresada hasta la actualidad.
     - hasta(formato dd-mm-AAAA): retorna los trazados que tienen fotos
       registradas hasta la fecha ingresada.
     - Los filtros desde y hasta se pueden combinar para formar un rango de
       fechas en el cual filtrar los trazados que contengan fotos en dicho
       rango.
    """

    serializer_class = TrazadoEspacioVerdeSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_serializer_class(self):
        if self.request.path == '/api/v2/espacios-verdes/frentes-espacios-verdes/':
            serializer = TrazadoEspacioVerdeSerializer
        elif self.request.path == '/api/v2/espacios-verdes/frentes-espacios-verdes-con-fotos/':
            serializer = TrazadoEspacioVerdeConFotosSerializer
        return serializer

    def get_queryset(self):

        if "id_zona" not in self.request.query_params.keys():
            raise ValidationError(
                "Debe consultar por zona con el parámetro id_zona")

        id_trabajo = self.request.query_params.get('id_trabajo', None)
        id_obra = self.request.query_params.get('id_zona', None)

        desde = self.request.query_params.get('desde', None)
        hasta = self.request.query_params.get('hasta', None)

        queryset = TrazadoEspacioVerde.objects.filter(
            obra__publicado=True,
            obra__publicado_gobierno=True,
            publicado=True,
            publicado_gobierno=True,
            obra__id=int(id_obra),
            trazado__isnull=False,
            adjuntos__fecha__isnull=False).distinct()

        if id_trabajo is not None:
            queryset = queryset.filter(pk=id_trabajo).distinct()

        if desde is not None or hasta is not None:
            # filtros desde y hasta especificados
            if desde is not None and hasta is not None:
                desde = datetime.strptime(desde, "%d-%m-%Y").date()
                hasta = datetime.strptime(hasta, "%d-%m-%Y").date()

                if desde > hasta:
                    raise Http404("Fecha mal especificada.")
                else:
                    queryset = queryset.filter(
                        adjuntos__fecha__range=[desde, hasta])

            # el filtro desde está especificado
            elif desde is not None:
                desde = datetime.strptime(desde, "%d-%m-%Y").date()
                queryset = queryset.filter(adjuntos__fecha__gte=desde)

            # el filtro hasta está especificado
            elif hasta is not None:
                hasta = datetime.strptime(hasta, "%d-%m-%Y").date()
                queryset = queryset.filter(adjuntos__fecha__lte=hasta)

        fecha_exacta = self.request.query_params.get('fecha_exacta', None)
        if fecha_exacta is not None:
            fecha_exacta = datetime.strptime(fecha_exacta, "%d-%m-%Y").date()
            queryset = queryset.filter(adjuntos__fecha=fecha_exacta)

        dias = self.request.query_params.get('dias', None)
        if dias is not None:
            fecha_ahora = datetime.now()
            fecha_desde = fecha_ahora - timedelta(days=int(dias))
            queryset = queryset.filter(adjuntos__fecha__gte=fecha_desde)

        tipos_id_str = self.request.query_params.get('tipo_id', None)
        if tipos_id_str is not None:
            tipos_id = tipos_id_str.split(',')
            queryset = queryset.filter(obra__tipo__id__in=tipos_id)

        obra_avance_desde = self.request.query_params.get(
            'obra_avance_desde', None)
        if obra_avance_desde is not None:
            queryset = queryset.filter(
                obra__porcentaje_completado__gte=int(obra_avance_desde))

        obra_avance_hasta = self.request.query_params.get(
            'obra_avance_hasta', None)
        if obra_avance_hasta is not None:
            queryset = queryset.filter(
                obra__porcentaje_completado__lte=int(obra_avance_hasta))

        barrios_str = self.request.query_params.get('barrios_ids', None)
        if barrios_str is not None:
            barrios_ids = barrios_str.split(',')
            queryset = queryset.filter(obra__barrios__in=barrios_ids)

        texto = self.request.query_params.get('texto', None)
        if texto is not None:
            # FIXME: acá se busca el texto que quiere el usuario, por más que
            # en la bd tenga acento lo mismo lo encuentra.
            # Si Django trae algo predefinido, usarlo acá
            copia_queryset = queryset.values(
                'id', 'obra__nombre', 'descripcion_publica')
            list_ids = []
            for trazado in copia_queryset:
                if normalizar(texto) in (
                    normalizar(
                        trazado['obra__nombre']) or normalizar(
                        trazado['descripcion_publica'])):
                    list_ids.append(trazado['id'])
            queryset = queryset.filter(id__in=list_ids)

        return queryset.order_by('obra')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class FotoDeTrazadoEspacioVerdeViewSet(viewsets.ModelViewSet):
    """
    Fotos de Trazados de obras de ambiente
    Puede fitrarse:
     - solo para una obra con el parámetro id_obra

      Por fechas:
     - dias(representado por un número entero): retorna los trazados que tienen
       fotos registradas en los últimos x días.
     - fecha_exacta(formato dd-mm-AAAA): retorna los trazados que tienen fotos
       registradas en la fecha ingresada.
     - desde(formato dd-mm-AAAA): retorna los trazados que tienen fotos
       registradas desde la fecha ingresada hasta la actualidad.
     - hasta(formato dd-mm-AAAA): retorna los trazados que tienen fotos
       registradas hasta la fecha ingresada.
     - Los filtros desde y hasta se pueden combinar para formar un rango de
       fechas en el cual filtrar los trazados que contengan fotos en dicho
       rango.
    """

    serializer_class = FotoDeEspacioVerdeSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):

        # if "id_trabajo" not in self.request.query_params.keys():
        #     raise ValidationError("Debe consultar por trazado con el parámetro id_trabajo")

        id_trabajo = self.request.query_params.get('id_trabajo', None)

        desde = self.request.query_params.get('desde', None)
        hasta = self.request.query_params.get('hasta', None)

        queryset = FotoDeEspacioVerde.objects.filter(
            trazado__obra__publicado=True,
            trazado__obra__publicado_gobierno=True,
            publicado=True,
            trazado__publicado=True,
            trazado__id=int(id_trabajo),
            trazado__isnull=False,
            fecha__isnull=False).distinct()

        if desde is not None or hasta is not None:
            # filtros desde y hasta especificados
            if desde is not None and hasta is not None:
                desde = datetime.strptime(desde, "%d-%m-%Y").date()
                hasta = datetime.strptime(hasta, "%d-%m-%Y").date()

                if desde > hasta:
                    raise Http404("Fecha mal especificada.")
                else:
                    queryset = queryset.filter(fecha__range=[desde, hasta])

            # el filtro desde está especificado
            elif desde is not None:
                desde = datetime.strptime(desde, "%d-%m-%Y").date()
                queryset = queryset.filter(fecha__gte=desde)

            # el filtro hasta está especificado
            elif hasta is not None:
                hasta = datetime.strptime(hasta, "%d-%m-%Y").date()
                queryset = queryset.filter(fecha__lte=hasta)

        fecha_exacta = self.request.query_params.get('fecha_exacta', None)
        if fecha_exacta is not None:
            fecha_exacta = datetime.strptime(fecha_exacta, "%d-%m-%Y").date()
            queryset = queryset.filter(fecha=fecha_exacta)

        dias = self.request.query_params.get('dias', None)
        if dias is not None:
            fecha_ahora = datetime.now()
            fecha_desde = fecha_ahora - timedelta(days=int(dias))
            queryset = queryset.filter(fecha__gte=fecha_desde)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
