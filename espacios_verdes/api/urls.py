from django.conf.urls import url, include
from rest_framework import routers
from .views import (EspacioVerdeViewSet, FotoDeTrazadoEspacioVerdeViewSet,
                    TrazadoEspacioVerdeViewSet)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'^espacios-verdes',
    EspacioVerdeViewSet,
    base_name='espacios-verdes.api.lista')
router.register(r'^frentes-espacios-verdes', TrazadoEspacioVerdeViewSet,
                base_name='frentes-espacios-verdes.api.lista')
router.register(
    r'^frentes-espacios-verdes-con-fotos',
    TrazadoEspacioVerdeViewSet,
    base_name='frentes-espacios-verdes-fotos.api.lista')
router.register(
    r'^fotos-frentes-espacios-verdes',
    FotoDeTrazadoEspacioVerdeViewSet,
    base_name='fotos-frentes-espacios-verdes.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
