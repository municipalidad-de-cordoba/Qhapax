from django.contrib import admin
from .models import (EspacioVerde, FotoDeEspacioVerde, TrazadoEspacioVerde,
                     TipoDeTrabajoEnEspacioVerde)
from core.admin import QhapaxOSMGeoAdmin


class FotoDeEspacioVerdeInLine(admin.StackedInline):
    model = FotoDeEspacioVerde
    extra = 1


class EspacioVerdeAdmin(QhapaxOSMGeoAdmin):
    list_display = ['nombre', 'id', 'id_externo', 'estado',
                    'publicado', 'publicado_gobierno', 'contratista']
    search_fields = ['nombre', 'id', 'id_externo']
    list_filter = [
        'tipo',
        'contratista',
        'estado',
        'publicado',
        'publicado_gobierno']
    inlines = [FotoDeEspacioVerdeInLine]

    def get_queryset(self, request):
        queryset = EspacioVerde.objects.all().order_by('-id')
        return queryset


class TrazadoEspacioVerdeAdmin(QhapaxOSMGeoAdmin):

    def id_obra(self, obj):
        return obj.obra.id
    list_display = ['obra', 'id_obra', 'id', 'id_externo',
                    'descripcion_publica', 'publicado', 'publicado_gobierno']
    search_fields = ['obra__nombre', 'obra__id',
                     'descripcion_publica', 'id', 'id_externo']
    list_filter = ['obra', 'publicado', 'publicado_gobierno']
    inlines = [FotoDeEspacioVerdeInLine]


class FotoDeEspacioVerdeAdmin(QhapaxOSMGeoAdmin):

    def file_size(self, obj):
        if obj.foto and hasattr(obj.foto, 'url'):
            sz = obj.foto.file.size / (1024 * 1024)
            return '{:.2f} MB'.format(sz)
        else:
            return "No hay foto"
    # file_size.admin_order_field = 'foto__file__size'

    def id_obra(self, obj):
        if obj.trazado is not None:
            return obj.trazado.obra.id

    def foto_thumb(self, obj):
        if obj.foto:
            try:
                th80 = obj.foto.thumbnail['80x80'].url
            except Exception as e:
                pass
            else:
                return '<img src="{}" style="width: 80px;"/>'.format(
                    obj.foto.thumbnail['80x80'].url)

        return ' <img src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACXBIWXMAAAAnAAAAJwEqCZFPAAAA B3RJTUUH4AcJFDE2B3C7PwAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUH AAAACklEQVQI12P4DwABAQEAG7buVgAAAABJRU5ErkJggg==" alt="blanco " style="width: 80px; height: 120px;" />'

    foto_thumb.short_description = 'thumb'
    foto_thumb.allow_tags = True

    list_display = [
        'obra',
        'trazado',
        'id_obra',
        'id_externo',
        'publicado',
        'publicado_gobierno',
        'foto_thumb',
        'url',
        'file_size']
    search_fields = ['trazado__descripcion_publica',
                     'id_externo', 'trazado__obra__id', 'url']
    list_filter = ['trazado__obra', 'trazado',
                   'publicado', 'publicado_gobierno']


admin.site.register(EspacioVerde, EspacioVerdeAdmin)
admin.site.register(TrazadoEspacioVerde, TrazadoEspacioVerdeAdmin)
admin.site.register(FotoDeEspacioVerde, FotoDeEspacioVerdeAdmin)
admin.site.register(TipoDeTrabajoEnEspacioVerde)
