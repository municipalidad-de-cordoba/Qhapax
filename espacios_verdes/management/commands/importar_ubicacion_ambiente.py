#!/usr/bin/python
"""
Importar nuevas obras de terceros(espacios verdes)
"""
from django.core.management.base import BaseCommand
# from django.db import transaction
from datetime import datetime
import json
import sys
import requests
from espacios_verdes.settings import (URL_API_TRAZADOS_ESPACIOS_VERDES,
                                      URL_AUTHORIZATION)
from espacios_verdes.models import (
    EspacioVerde,
    TrazadoEspacioVerde,
    FotoDeEspacioVerde,
    TipoDeTrabajoEnEspacioVerde)
from obras.models import OrigenDatoObra
from django.contrib.gis.geos import GEOSGeometry
from django.core.files import File
from django.utils import timezone
import tempfile
from PIL import Image


class Command(BaseCommand):
    help = """Comando para Importar obras de Espacios Verdes en ejecución """

    def add_arguments(self, parser):
        parser.add_argument(
            '--fecha',
            type=str,
            help=('fecha desde la cual se importarán nuevas obras y cambios a',
                  'las existentes. Formato dd/mm/aaaa'),
            default='01/01/2016')

    # @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación de datos '
                                             'de obras de espacios verdes'))

        fecha = options['fecha']
        url_base = URL_API_TRAZADOS_ESPACIOS_VERDES

        url = '{}&fecha={}'.format(url_base, fecha)

        self.stdout.write(self.style.SUCCESS(
            'Buscando datos en {}'.format(url)))

        headers = {'Authorization': URL_AUTHORIZATION, 'Connection': 'close'}
        session = requests.Session()
        basedata = session.get(url, headers=headers).content  # es tipo byte
        # basedata = basedata.decode("utf-8")
        basedata = basedata.decode("iso-8859-1")

        try:
            jsondata = json.loads(basedata)
        except Exception as e:
            self.stdout.write(self.style.ERROR('JSON Error: {}'.format(url)))
            self.stdout.write(self.style.ERROR(e))
            sys.exit(1)

        # marcar como el origen que tienen
        origen, created = OrigenDatoObra.objects.get_or_create(
            nombre='MuniDigital.com')
        # despublicar todo para ver que vuelve aquí (y que queden ocultos los
        # que no vengan)
        TrazadoEspacioVerde.objects.filter(
            obra__origen=origen).update(publicado=False)

        trazados = len(jsondata['features'])
        self.stdout.write(self.style.SUCCESS(
            'Se obtuvieron {} trazados'.format(trazados)))

        trazados_nuevos = 0
        trazados_repetidos = 0
        errores = []
        total_fotos = 0
        ciclos = 0

        for ftrazado in jsondata['features']:
            ciclos += 1
            dato = ftrazado['properties']
            """ muestra
            self.stdout.write(self.style.SUCCESS('OBRA: {}'.format(dato)))
            {
            "type": "Feature",
            "properties": {
                "descripcion": "MORENO MARIANO - Nro. 7011 - 02/11/020/001/0000\tSan Jerónimo. PZA. M.MORENO EMBLEMATICA - Barrio: San Vicente",
                "estado": "Realizado",
                "contratista": "Lord Green",
                "id_trazado": 2166,
                "id_obra": 147,
                "id_contratista": 5,
                "trabajos": [
                    [
                        "15/07/2019 11:24",
                        "Corte de pasto"
                    ],
                    [
                        "19/07/2019 09:10",
                        "Corte de pasto"
                    ],
                    [
                        "19/07/2019 12:19",
                        "Retiro de basura"
                    ],
                    [
                        "29/07/2019 12:19",
                        "Retiro de basura"
                    ]
                ],
                "adjuntos": [
                        {
                        "id": 109796,
                        "url": "https://munidigital.com/MuniDigitalCore/munidigitalcdn/adjuntos/1/92/documentoObra/77496/imagen1.jpg",
                        "fecha": "15/07/2019 11:24",
                        "paraPublicar": "SI"
                                },
                        {
                        "id": 110545,
                        "url": "http://munidigital.com/MuniDigitalCore/munidigitalcdn/adjuntos/1/92/documentoObra/78128/imagen1.jpg",
                        "fecha": "19/07/2019 09:10",
                        "paraPublicar": "SI"
                                },
                            ]
                        },
            }
            """

            id_obra = dato['id_obra']
            try:
                obra = EspacioVerde.objects.get(id_externo=id_obra)
            except EspacioVerde.DoesNotExist:
                err = 'NO EXISTE la obra ID:{}'.format(id_obra)
                self.stdout.write(self.style.ERROR(err))
                errores.append(err)
                continue

            id_externo = dato['id_trazado']
            trazado, created_trazado = TrazadoEspacioVerde.objects.get_or_create(
                obra=obra, id_externo=id_externo)
            if created_trazado:
                trazados_nuevos += 1
            else:
                trazados_repetidos += 1
            trazado.estado = dato['estado']

            # fotos de las obras
            self.stdout.write(self.style.SUCCESS(
                'Fotos del trazado n°:{}'.format(ciclos)))
            adjuntos = dato['adjuntos']
            self.stdout.write(self.style.SUCCESS(
                'Hay {} fotos'.format(len(adjuntos))))

            # datos geoespaciales
            geometry = ftrazado['geometry']
            try:
                geo_str = str(geometry)
                geometry_data = GEOSGeometry(geo_str)
                trazado.trazado = geometry_data
                if trazado.trazado is None:
                    trazado.observaciones_internas = "No está geolocalizado. Consultar con MuniDigital"
            except Exception as e:
                self.stdout.write(self.style.ERROR(
                    'GeoJSON Error: {}'.format(geometry)))
                # error = 'Obra {}-{}: GeoJSON Error - {}'.format(
                # id_externo, nombre, e)
                error = 'Obra {}'.format(id_externo)
                errores.append(error)

            trazado.publicado = True
            if created_trazado:  # si la obra viene por primera vez la publico, despues puedo revisar
                trazado.publicado_gobierno = True

            # descripcion
            trazado.descripcion_publica = dato['descripcion']

            fotos = 0
            for adjunto in adjuntos:
                foto, created = FotoDeEspacioVerde.objects.get_or_create(
                    trazado=trazado, id_externo=adjunto['id'])

                foto.url = adjunto['url']
                current_tz = timezone.get_current_timezone()
                foto.fecha = datetime.strptime(
                    adjunto['fecha'].strip(), "%d/%m/%Y %H:%M")
                foto.fecha = current_tz.localize(foto.fecha)

                # el campo paraPublicar tiene un string, se pasa a booleano
                if adjunto['paraPublicar'] == 'SI':
                    foto.publicado = True
                else:
                    foto.publicado = False
                # importo las fotos solo si es la primera vez
                if created:
                    fotos += 1
                    self.stdout.write(self.style.SUCCESS(
                        'foto n°: {}'.format(fotos)))
                    try:
                        self.stdout.write(
                            self.style.SUCCESS(
                                'Nueva Foto: {} Trayendo imagen {}'.format(
                                    adjunto, foto.url)))
                        url_foto = adjunto['url']
                        contenido = session.get(
                            url_foto).content  # es tipo byte
                    except requests.exceptions.ConnectionError as e:
                        err = 'Connection to {} failed. La imagen no pudo guardarse.'.format(
                            adjunto['url'])
                        self.stdout.write(self.style.ERROR(err))
                        errores.append(err)

                    tf = tempfile.NamedTemporaryFile()
                    tf.write(contenido)

                    img = File(tf)
                    try:
                        # controlamos que el archivo sea una imagen válida
                        img = Image.open(img)
                        if img.size != 0:
                            foto.foto.save(
                                str(foto.url.split('/')[-1]), File(tf))
                    except Exception as e:
                        err = 'ERROR con el formato de la FOTO:{}'.format(
                            foto.url)
                        self.stdout.write(self.style.ERROR(err))
                        errores.append(err)
                        foto.delete()
                        continue

                        foto.save()
                else:
                    self.stdout.write(self.style.SUCCESS(
                        'Omitiendo descargar imagen {}'.format(foto.url)))
                    foto.save()

            total_fotos += fotos

            trabajos = dato['trabajos']
            lista_trabajos = []

            if trabajos is not None:
                for l in trabajos:
                    trabajo, creado = TipoDeTrabajoEnEspacioVerde.objects.get_or_create(
                        nombre=l[1])
                    lista_trabajos.append(trabajo)
            else:
                trabajo = TipoDeTrabajoEnEspacioVerde.objects.create(
                    nombre='Trabajo no especificado')
                lista_trabajos.append(trabajo)

            trazado.tipos = lista_trabajos

            trazado.save()

        if len(errores) > 0:
            self.stdout.write(self.style.SUCCESS(
                'LOS SIGUIENTES TRAZADOS TUVIERON ALGÚN TIPO DE ERROR:'))
            self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR'))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))

        self.stdout.write(self.style.SUCCESS('RESULTADOS:'))
        self.stdout.write(self.style.SUCCESS(
            'TRAZADOS NUEVOS: {}'.format(trazados_nuevos)))
        self.stdout.write(self.style.SUCCESS(
            'TRAZADOS REPETIDOS: {}'.format(trazados_repetidos)))
        self.stdout.write(self.style.SUCCESS(
            'TOTAL DE FOTOS: {}'.format(total_fotos)))

        self.stdout.write(self.style.SUCCESS("FIN"))
