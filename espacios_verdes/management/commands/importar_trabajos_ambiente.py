#!/usr/bin/python
"""
Importar nuevas obras de terceros(espacios verdes)
"""
from django.core.management.base import BaseCommand
from django.db import transaction
from datetime import datetime
import json
import pytz
import random
import sys
from espacios_verdes.settings import URL_API_ESPACIOS_VERDES, URL_AUTHORIZATION
from barrios.models import Barrio
from espacios_verdes.models import EspacioVerde, FotoDeEspacioVerde
from obras.models import OrigenDatoObra, TipoObra
from organizaciones.models import Organizacion
from django.contrib.gis.geos import GEOSGeometry, Point
import requests
from django.core.files import File
import tempfile
from PIL import Image
from core.utils import normalizar
import difflib


class Command(BaseCommand):
    help = """Comando para Importar obras de Espacios Verdes en ejecución """

    def add_arguments(self, parser):
        parser.add_argument(
            '--fecha',
            type=str,
            help=('fecha desde la cual se importarán nuevas obras y cambios a',
                  'las existentes. Formato dd/mm/aaaa'),
            default='01/01/2016')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación de datos '
                                             'de obras de espacios verdes'))

        url_base = URL_API_ESPACIOS_VERDES
        fecha = options['fecha']
        url = '{}&fecha={}'.format(url_base, fecha)

        self.stdout.write(self.style.SUCCESS(
            'Buscando datos en {}'.format(url)))

        session = requests.Session()
        headers = {'Authorization': URL_AUTHORIZATION}
        basedata = session.get(url, headers=headers).content  # es tipo byte
        # basedata = basedata.decode("utf-8")
        basedata = basedata.decode("iso-8859-1")

        try:
            jsondata = json.loads(basedata)
        except Exception as e:
            self.stdout.write(self.style.ERROR('JSON Error: {}'.format(url)))
            self.stdout.write(self.style.ERROR(e))
            sys.exit(1)

        # marcar como el origen que tienen
        origen, created = OrigenDatoObra.objects.get_or_create(
            nombre='MuniDigital.com')

        obras = len(jsondata['features'])
        self.stdout.write(self.style.SUCCESS(
            'Se obtuvieron datos de {} obras'.format(obras)))

        obras_nuevas = 0
        obras_repetidas = 0
        tipos_nuevos = 0
        tipos_repetidos = 0
        organizaciones_nuevas = 0
        organizaciones_repetidas = 0
        errores = []

        # datos para barrios
        barrios = Barrio.objects.values_list('nombre', flat=True)
        poligonos_barrios = Barrio.objects.values_list('poligono', flat=True)

        for fobra in jsondata['features']:
            dato = fobra['properties']
            """
            self.stdout.write(self.style.SUCCESS('OBRA: {}'.format(dato)))
            {
            'nombreObra': 'Zona 01 CPC Centro America',
            'id_obra': 118,
            'fechaTerminacion': '31/12/2017 00:00',
            'fechaIniciacion': '01/01/2017 00:00',
            'color': '',
            'contratista': 'Ecopoda',
            'cuitContratista': '30-71199800-9',
            'porcentajeCompletado': 0.0,
            'estadoObra': 'En ejecucion',
            'decreto': 'S/D',
            'adjuntos': [{
                    'url': 'https://munidigital.com/MuniDigitalCore/munidigitalcdn/adjuntos/1/92/documentoObra/3241/IMG_3424.JPG',
                    'id': 14341,
                    'paraPublicar': 'SI'
                    'fecha': '28/10/2017 00:00'
                }, {
                    'url': 'https://munidigital.com/MuniDigitalCore/munidigitalcdn/adjuntos/1/92/documentoObra/3239/IMG_3432.JPG',
                    'id': 14337,
                    'paraPublicar': 'SI'
                    'fecha': '28/10/2017 00:00'
                }],
            'tipoObra': 'Espacios Verdes',
            'monto': 6.0,
            'barrios': [],
            'expedienteOriginal': 'S/D'
            }
            """

            nombre = dato['nombreObra'].strip()
            if 'id_obra' not in dato.keys():
                self.stdout.write(self.style.ERROR(
                    'La obra "{} no tiene ID_OBRA'.format(nombre)))
                continue

            id_externo = dato['id_obra']
            obra, created_obra = EspacioVerde.objects.get_or_create(
                id_externo=id_externo)
            if created_obra:
                obras_nuevas += 1
                self.stdout.write(self.style.SUCCESS(
                    'Obra número: {}'.format(obras_nuevas)))
            else:
                obras_repetidas += 1

            obra.nombre = nombre
            obra.decreto = dato['decreto'].strip()
            obra.expediente_origen = dato['expedienteOriginal'].strip()
            tipo_str = dato['tipoObra'].strip()
            if tipo_str == '':
                tipo_str = 'Desconocido'
            tipo, created = TipoObra.objects.get_or_create(
                nombre=tipo_str)
            if created:
                tipos_nuevos += 1
            else:
                tipos_repetidos += 1

            # Si el tipo fue creado, se le da un color aleatorio
            if tipo.color == 'ffffffff':
                tipo.color = ''.join(random.choice(
                    '0123456789abcdef') for n in range(8))
                tipo.save()
            obra.tipo = tipo
            obra.origen = origen

            # fotos de las obras
            adjuntos = dato['adjuntos']
            for adjunto in adjuntos:
                foto, created = FotoDeEspacioVerde.objects.get_or_create(
                    obra=obra, id_externo=adjunto['id'])

                foto.url = adjunto['url']
                # el campo paraPublicar tiene un string, se pasa a booleano
                if adjunto['paraPublicar'] == 'SI':
                    foto.publicado = True
                else:
                    foto.publicado = False

                foto.fecha = datetime.strptime(
                    adjunto['fecha'].strip(), "%d/%m/%Y %H:%M")
                # importo las fotos solo si es la primera vez
                if created:
                    self.stdout.write(
                        self.style.SUCCESS(
                            'Nueva Foto: {} Trayendo imagen {}'.format(
                                adjunto, foto.url)))
                    url_foto = adjunto['url']
                    contenido = session.get(url_foto).content  # es tipo byte

                    tf = tempfile.NamedTemporaryFile()
                    tf.write(contenido)

                    img = File(tf)
                    try:
                        # controlamos que el archivo sea una imagen válida
                        img = Image.open(img)
                        if img.size != 0:
                            foto.foto.save(
                                str(foto.url.split('/')[-1]), File(tf))
                    except Exception as e:
                        err = 'ERROR con el formato de la FOTO:{}'.format(
                            foto.url)
                        self.stdout.write(self.style.ERROR(err))
                        errores.append(err)
                        foto.delete()
                        continue

                        foto.save()
                else:
                    self.stdout.write(
                        self.style.SUCCESS(
                            'Omitiendo descargar imagen {}'.format(
                                foto.url)))

            # datos geoespaciales
            geometry = fobra['geometry']
            try:
                geo_str = str(geometry)
                geometry_data = GEOSGeometry(geo_str)
                obra.ubicacion = geometry_data
            except Exception as e:
                self.stdout.write(self.style.ERROR(
                    'GeoJSON Error: {}'.format(geometry)))
                error = 'Obra {}-{}-{}'.format(nombre, id_externo, e)
                errores.append(error)

            # Datos Contratista
            organizacion_str = dato['contratista'].strip()
            if organizacion_str == '':
                organizacion_str = 'Desconocido'

            cuit_str = dato['cuitContratista'].strip()
            # limpio el cuit de caracteres extraños
            cuit_str = cuit_str.replace(
                '-',
                '').replace(
                ' ',
                '').replace(
                '.',
                '').replace(
                ',',
                '')
            if cuit_str == '':
                cuit_str = 'Desconocido'

            try:
                organizacion, created = Organizacion.objects.get_or_create(
                    nombre=organizacion_str, CUIT=cuit_str)
                if created:
                    organizaciones_nuevas += 1
                else:
                    organizaciones_repetidas += 1
            except Organizacion.MultipleObjectsReturned:
                # Fix para cuando la organizacion está duplicada
                organizaciones = Organizacion.objects.filter(
                    nombre=organizacion_str, CUIT=cuit_str)

                max_obras_relacionadas = 0
                organizacion = None
                # por cada organización, obtengo las obras que tiene a su cargo
                for org in organizaciones:
                    cant = len(org.obrapublica_set.all())
                    if cant > max_obras_relacionadas:
                        organizacion = org
                # elijo la organizacion que tiene mas obras
                organizaciones_repetidas += 1

            obra.contratista = organizacion

            obra.monto = 0.0 if dato['monto'] == '' else dato['monto']
            fecha_inicio = datetime.strptime(
                dato['fechaIniciacion'].strip(), "%d/%m/%Y %H:%M")
            fecha_finalizacion_estimada = datetime.strptime(
                dato['fechaTerminacion'].strip(), "%d/%m/%Y %H:%M")

            obra.fecha_inicio = pytz.utc.localize(fecha_inicio)
            obra.fecha_finalizacion_estimada = pytz.utc.localize(
                fecha_finalizacion_estimada)

            # barrio
            barrios_str = dato['barrios']
            barrios_encontrados = []

            # el dato no viene con barrio incluido
            if len(barrios_str) == 0:
                # si la ubicacion es punto, basta con encontrar un barrio
                if isinstance(obra.ubicacion, Point):
                    for poligono in poligonos_barrios:
                        if poligono is not None and poligono.contains(
                                obra.ubicacion):
                            barrios_encontrados.append(
                                Barrio.objects.get(poligono=poligono))
                            break
                # la ubicacion no es un punto, puede ocupar más de un barrio
                else:
                    for poligono in poligonos_barrios:
                        if poligono is not None and poligono.intersection(
                                obra.ubicacion):
                            b = Barrio.objects.get(poligono=poligono)
                            barrios_encontrados.append(b)

            # el dato trae por lo menos un barrio
            else:
                for b in barrios_str:
                    match = difflib.get_close_matches(
                        b.upper(), possibilities=barrios, n=1)
                    if match == []:
                        # barrio = None
                        for poligono in poligonos_barrios:
                            if poligono is not None and poligono.contains(
                                    obra.ubicacion):
                                barrios_encontrados.append(
                                    Barrio.objects.get(poligono=poligono))
                                break
                    else:
                        barrios_encontrados.append(
                            Barrio.objects.get(nombre=match[0]))
            obra.barrios = barrios_encontrados

            obra.color = dato['color'].strip()

            obra.porcentaje_completado = 0.0 if dato[
                'porcentajeCompletado'] == '' else dato['porcentajeCompletado']
            obra.publicado = True
            if created_obra:
                # si la obra viene por primera vez la publico, despues puedo revisar
                obra.publicado_gobierno = True

            estado = normalizar(dato['estadoObra'])
            if estado == normalizar('Proyectada'):
                obra.estado = EspacioVerde.PROYECTADA
            elif estado == normalizar('Licitada'):
                obra.estado = EspacioVerde.LICITADA
            elif estado == normalizar('Adjudicada'):
                obra.estado = EspacioVerde.ADJUDICADA
            elif estado == normalizar('Contratada'):
                obra.estado = EspacioVerde.CONTRATADA
            elif estado == normalizar('Replanteada'):
                obra.estado = EspacioVerde.REPLANTEADA
            elif estado == normalizar('En ejecucion'):
                obra.estado = EspacioVerde.EN_EJECUCION
            elif estado == normalizar('En ejecucion(por ampliacion de plazo)'):
                obra.estado = EspacioVerde.EN_EJECUCION_POR_AMPLIACION
            elif estado == normalizar('Finalizada'):
                obra.estado = EspacioVerde.FINALIZADA
            elif estado == normalizar('Rescindida'):
                obra.estado = EspacioVerde.RESCINDIDA

            obra.save()

        if len(errores) > 0:
            self.stdout.write(self.style.SUCCESS(
                'LAS SIGUIENTES OBRAS TUVIERON ALGÚN TIPO DE ERROR:'))
            self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR'))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))

        self.stdout.write(self.style.SUCCESS('RESULTADOS:'))
        self.stdout.write(self.style.SUCCESS(
            'OBRAS NUEVAS: {}'.format(obras_nuevas)))
        self.stdout.write(self.style.SUCCESS(
            'OBRAS REPETIDAS: {}'.format(obras_repetidas)))

        self.stdout.write(self.style.SUCCESS("FIN"))
