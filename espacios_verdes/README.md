## Obras de Espacios Verdes

El servicio de Espacios Verdes son todos los trabajos que realizan empresas contratistas para el mantenimiento de los Espacios Verdes.
Estos son aproximadamente 150 registros diarios.
En primera instancia se conecta a un software externo. Se importan
automáticamente todos los días las novedades

Actualmente estamos conectados al sistema de un proveedor externo e importamos los datos periódicamente

```
python manage.py importar_trabajos_ambiente
python manage.py importar_ubicacion_ambiente
```
