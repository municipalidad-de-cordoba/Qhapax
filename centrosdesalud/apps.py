from django.apps import AppConfig


class CentrosdesaludConfig(AppConfig):
    name = 'centrosdesalud'
