from django.contrib.gis.db import models
from core.models import Comunicacion
from lugares.models import Lugar


class AreasDeSalud(models.Model):
    '''
    Mas alla de los centros de salud hay áreas programáticas de salud definidas
    por la Secretaría
    '''
    nombre = models.CharField(max_length=120)
    descripcion = models.TextField(null=True, blank=True)
    poligono = models.PolygonField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class TipoCentroDeSalud(models.Model):
    '''Hay centros de Salud, Direcciones de Especialidades Médicas y Hospitales'''
    nombre = models.CharField(max_length=120)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class ServiciosDeSalud(models.Model):
    ''' servicios de salud posibles para brindar en los centros '''
    nombre = models.CharField(max_length=120)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class CentroDeSalud(Lugar):
    codigo_interno = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        help_text='Si es un Centro de Salud se usa su numeracion')

    fecha_de_inicio_actividades = models.DateField(null=True, blank=True)
    # FIXME cuando este listo core.models.DiasYHorarios podría usarse
    horarios_de_atencion = models.TextField(null=True, blank=True)
    servicios_brindados = models.ManyToManyField(ServiciosDeSalud, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']
        verbose_name_plural = "Centros de Salud"
        verbose_name = "Centro de Salud"


class ComunicacionCentroDeSalud(Comunicacion):
    objeto = models.ForeignKey(CentroDeSalud, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("tipo", "valor", "objeto"),)
        verbose_name = "Vía de comunicacion con Centro de Salud"
        verbose_name_plural = "Vías de comunicacion con Centro de Salud"
