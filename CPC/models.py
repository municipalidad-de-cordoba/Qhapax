from django.contrib.gis.db import models


class Cpc(models.Model):
    nombre = models.CharField(max_length=120)
    fecha_de_oficializacion = models.DateField(null=True, blank=True)
    zona = models.PolygonField(
        null=True,
        blank=True,
        help_text='Poligono que representa la zona del CPC')
    sede = models.PointField(
        null=True,
        blank=True,
        help_text='Punto exacto de ubicación del CPC')

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']
        verbose_name_plural = "Cpcs"
        verbose_name = "Cpc"
