from django.contrib import admin
from .models import Cpc
from core.admin import QhapaxOSMGeoAdmin


class CpcAdmin(QhapaxOSMGeoAdmin):
    list_display = ['nombre', 'fecha_de_oficializacion']
    search_fields = ['nombre']


admin.site.register(Cpc, CpcAdmin)
