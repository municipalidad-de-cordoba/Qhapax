from CPC.models import Cpc
from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from rest_framework_cache.registry import cache_registry


class CpcGeoSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):
    class Meta:
        model = Cpc
        geo_field = "zona"
        fields = ['id', 'nombre', 'zona']


class CpcSerializer(CachedSerializerMixin):
    class Meta:
        model = Cpc
        fields = ['id', 'nombre']


cache_registry.register(CpcGeoSerializer)
cache_registry.register(CpcSerializer)
