from django.conf.urls import url, include
from rest_framework import routers
from .views import CpcGeoViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'cpc-geo', CpcGeoViewSet, base_name='cpc.api')

urlpatterns = [
    url(r'^', include(router.urls)),
]
