from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import CpcGeoSerializer
from api.pagination import DefaultPagination
from CPC.models import Cpc


class CpcGeoViewSet(viewsets.ModelViewSet):
    """
    Filtro:

    ids = id del barrio. Pueden ser varios ids separados por comas.
    Ej: api/?ids=2, api/?ids=5,47,51, 3

    """
    serializer_class = CpcGeoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):

        queryset = Cpc.objects.all()

        ids = self.request.query_params.get('ids', None)
        if ids is not None:
            ids = ids.split(',')
            queryset = queryset.filter(id__in=ids)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
