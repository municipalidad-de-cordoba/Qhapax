#!/usr/bin/python
from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from datetime import datetime
from django.contrib.gis.geos import Point, Polygon
from fastkml import kml
from CPC.models import Cpc


class Command(BaseCommand):
    help = """Comando para importar sedes de los CPC's al sistema desde un .kml"""

    def add_arguments(self, parser):
        parser.add_argument('--path', type=str, help='Path del archivo KML local')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación de sedes'))
        url = options['path']

        try:
            with open(url, 'rt', encoding="utf-8") as myfile: doc=myfile.read().encode('utf-8')
            self.stdout.write(self.style.SUCCESS('Buscando datos en {}'.format(url)))
        except Exception as e:
            self.stdout.write(self.style.ERROR('Error al leer de {}'.format(url)))
            sys.exit(1)

        # Create the KML object to store the parsed result
        k = kml.KML()

        # Read in the KML string
        k.from_string(doc)
        features = list(k.features())

        sedes = 0 #areas descargados a la base de datos
        for feature in features:
            self.stdout.write("=============================")
            self.stdout.write("Doc: {}".format(feature.name))
            self.stdout.write("=============================")

            folders = list(feature.features())
            for folder in folders:
                self.stdout.write("=============================")
                self.stdout.write("Folder: {}".format(folder.name))
                self.stdout.write("=============================")

                places = list(folder.features())
                for place in places:
                    cpc, created = Cpc.objects.get_or_create(nombre=place.name)
                    sedes += 1
                    self.stdout.write("=============================")
                    self.stdout.write("Cpc: {}".format(cpc))
                    self.stdout.write("=============================")
                    print(place.geometry.coords[0][0:2])
                    cpc.sede = Point(place.geometry.coords[0][0:2])
                    cpc.save()
        self.stdout.write("Total de sedes: {}".format(sedes))