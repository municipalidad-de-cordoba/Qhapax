#!/usr/bin/python
from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from datetime import datetime
from django.contrib.gis.geos import Point, Polygon
from fastkml import kml
from CPC.models import Cpc


class Command(BaseCommand):
    help = """Comando para importar areas de los CPC's al sistema desde un .kml"""

    def add_arguments(self, parser):
        parser.add_argument('--path', type=str, help='Path del archivo KML local')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación de areas'))
        url = options['path']

        try:
            with open(url, 'rt', encoding="utf-8") as myfile: doc=myfile.read().encode('utf-8')
            self.stdout.write(self.style.SUCCESS('Buscando datos en {}'.format(url)))
        except Exception as e:
            self.stdout.write(self.style.ERROR('Error al leer de {}'.format(url)))
            sys.exit(1)

        # Create the KML object to store the parsed result
        k = kml.KML()

        # Read in the KML string
        k.from_string(doc)
        features = list(k.features())

        areas = 0 #areas descargados a la base de datos
        for feature in features:
            self.stdout.write("=============================")
            self.stdout.write("Doc: {}".format(feature.name))
            self.stdout.write("=============================")

            folders = list(feature.features())
            for folder in folders:
                self.stdout.write("=============================")
                self.stdout.write("Folder: {}".format(folder.name))
                self.stdout.write("=============================")

                places = list(folder.features())
                for place in places:
                    #obtengo los cps correspondientes
                    if place.name == 'RUTA 20':
                        try:
                            cpc = Cpc.objects.get(nombre='CPC Ruta 20')
                            cpc.zona = Polygon([xy[0:2] for xy in place.geometry.exterior.coords])
                            cpc.save()
                        except cpc.DoesNotExist:
                            self.stdout.write(self.style.ERROR('No existe CPC {}'.format(place.name)))
                            sys.exit(1)

                    elif place.name == 'PUEYRREDON':
                        try:
                            cpc = Cpc.objects.get(nombre='CPC Pueyrredón')
                            cpc.zona = Polygon([xy[0:2] for xy in place.geometry.exterior.coords])
                            cpc.save()
                        except cpc.DoesNotExist:
                            self.stdout.write(self.style.ERROR('No existe CPC {}'.format(place.name)))
                            sys.exit(1)

                    elif place.name == 'CENTRO AMERICA':
                        try:
                            cpc = Cpc.objects.get(nombre='CPC Centro América')
                            cpc.zona = Polygon([xy[0:2] for xy in place.geometry.exterior.coords])
                            cpc.save()
                        except cpc.DoesNotExist:
                            self.stdout.write(self.style.ERROR('No existe CPC {}'.format(place.name)))
                            sys.exit(1)

                    elif place.name == 'MONSEÑOR PABLO CABRERA':
                        try:
                            cpc = Cpc.objects.get(nombre='CPC Monseñor Pablo Cabrera')
                            cpc.zona = Polygon([xy[0:2] for xy in place.geometry.exterior.coords])
                            cpc.save()
                        except cpc.DoesNotExist:
                            self.stdout.write(self.style.ERROR('No existe CPC {}'.format(place.name)))
                            sys.exit(1)

                    elif place.name == 'CENTRAL':
                        try:
                            cpc = Cpc.objects.get(nombre='CPC Mercado de la Ciudad')
                            cpc.zona = Polygon([xy[0:2] for xy in place.geometry.exterior.coords])
                            cpc.save()
                        except cpc.DoesNotExist:
                            self.stdout.write(self.style.ERROR('No existe CPC {}'.format(place.name)))
                            sys.exit(1)

                    elif place.name == 'EMPALME':
                        try:
                            cpc = Cpc.objects.get(nombre='CPC Empalme')
                            cpc.zona = Polygon([xy[0:2] for xy in place.geometry.exterior.coords])
                            cpc.save()
                        except cpc.DoesNotExist:
                            self.stdout.write(self.style.ERROR('No existe CPC {}'.format(place.name)))
                            sys.exit(1)

                    elif place.name == 'VILLA EL LIBERTADOR':
                        try:
                            #hay un error en el kml y se guarda así el nombre
                            cpc = Cpc.objects.get(nombre='CPC Villa El Libertador\n')
                            cpc.zona = Polygon([xy[0:2] for xy in place.geometry.exterior.coords])
                            cpc.save()
                        except cpc.DoesNotExist:
                            self.stdout.write(self.style.ERROR('No existe CPC {}'.format(place.name)))
                            sys.exit(1)

                    elif place.name == 'RANCAGUA':
                        try:
                            cpc = Cpc.objects.get(nombre='CPC Rancagua')
                            cpc.zona = Polygon([xy[0:2] for xy in place.geometry.exterior.coords])
                            cpc.save()
                        except cpc.DoesNotExist:
                            self.stdout.write(self.style.ERROR('No existe CPC {}'.format(place.name)))
                            sys.exit(1)

                    elif place.name == 'ARGUELLO':
                        try:
                            cpc = Cpc.objects.get(nombre='CPC Argüello')
                            cpc.zona = Polygon([xy[0:2] for xy in place.geometry.exterior.coords])
                            cpc.save()
                        except cpc.DoesNotExist:
                            self.stdout.write(self.style.ERROR('No existe CPC {}'.format(place.name)))
                            sys.exit(1)

                    elif place.name == 'COLON':
                        try:
                            cpc = Cpc.objects.get(nombre='CPC Colón')
                            cpc.zona = Polygon([xy[0:2] for xy in place.geometry.exterior.coords])
                            cpc.save()
                        except cpc.DoesNotExist:
                            self.stdout.write(self.style.ERROR('No existe CPC {}'.format(place.name)))
                            sys.exit(1)

                    else:
                        self.stdout.write(self.style.ERROR(
                            'No se encontró cpc de la zona {}'.format(place.name)))
                        sys.exit(1)
        self.stdout.write("FIN")
