from django.conf.urls import url

from . import views

urlpatterns = [url(r'^$',
                   views.index,
                   name='index'),
               url(r'^funcionarios.(?P<filetype>csv|xls)$',
                   views.planilla_funcionarios,
                   name='portaldedatos.funcionarios.planilla'),
               url(r'^comunicacion-(?P<persona_id>[0-9]+)$',
                   views.comunicacion_funcionario,
                   name='portaldedatos.comunicacion_funcionario'),
               url(r'^comunicacion-funcionarios.(?P<filetype>csv|xls)$',
                   views.comunicaciones_funcionarios,
                   name='portaldedatos.comunicaciones_funcionarios.planilla'),
               url(r'^corporativo-funcionarios.(?P<filetype>csv|xls)$',
                   views.celular_corporativo_funcionarios,
                   name='portaldedatos.celular_corporativo_funcionarios.planilla'),
               url(r'^celulares-privados-funcionarios.(?P<filetype>csv|xls)$',
                   views.celular_privado_funcionarios,
                   name='portaldedatos.celular_privado_funcionarios.planilla'),
               url(r'^protocolo-(?P<desde_cargo_id>[0-9]+).pdf$',
                   views.protocolo_pdf,
                   name='portaldedatos.protocolo_pdf'),
               url(r'^oficinas-publicas-formato-google.(?P<filetype>csv|xls)$',
                   views.oficinas_publicas_a_google_maps,
                   name='funcionarios.oficinas_publicas_a_google_maps'),
               ]
