.. _funcionarios_index:

Manejo de Funcionarios públicos
===============================

Luego de cargar `personas <../core/personas.rst>`__ es posible asignarlas como funcionarios.

Esto significa asignar un cargo (que debe existir o ser creado).

Crear un cargo para un funcionario
----------------------------------

.. image:: /docs/img/administracion_funcionarios.png

Primero debemos crear una Categoria de los puestos de funcion pública, nos dirigimos a `Cargo categorias`. Estos son los campos a completar:

 - Nombre: nombre de la categoria. Ej: Director de área.
 - Nombre fem: nombre en femenino de la categoria. Ej: Directora de área.
 - Nombre corto: nombre corto en masculino. Ej: Director.
 - Nombre corto fem: nombre corto en femenino. Ej: Directora.
 - Descripción: una breve descripción por si se quiere agregar algo mas.
 - Requiere declaración jurada: para saber si es un cargo al que después tengo que exigirle una declaración jurada anual.
 - Publicado: hay categorías que no se publican porque no son funcionarios públicos.
 - Permite audiencia: permite que la gente solicite audiencias vía web.
 - Orden: para asignar prioridad. Ej: el 5 se va a mostrar antes que el 100.

Luego hay que crear los `Cargos` específicos dentro de la categoría correspondiente, para eso vamos hacia el apartado `Cargos.` Campos a completar:

 - Nombre: nombre del cargo en masculino. Ej: Director de sistemas de la categoría Director de área.
 - Nombre fem: nombre del cargo en femenino. Ej: Directora de sistemas.
 - Oficina: por ejemplo Dirección de sistemas.
 - Descripción: una breve descripción para uso interno.
 - Html: texto explicativo abierto.
 - Depende de: salvo el cargo principal (Intendente, por ejemplo) todos los demás dependen de otro funcionario (y solo de uno).
 - Electivo: si el cargo se gano en elecciones.
 - Publicado: hay cargos que no se publican porque no son funcionarios públicos.
 - Activado: Para saber si el cargo ya tiene funcionarios designados.
 - Orden: para asignar prioridad. Ej: el 5 se va a mostrar antes que el 100.

Asignar un cargo a un funcionario
---------------------------------

Finalmente, con los pasos anteriores ya hechos, podremos ir al apartado `Funciones` y asignar un funcionario al cargo especificado.
Todos los campos de este apartado contienen un help text autoexplanatorio.

Cambiar de cargo a un funcionario
---------------------------------

En el caso que se desee cambiar de cargo a un funcionario se debe ir al apartado de `Funciones` y cambiamos al cargo nuevo que querramos (el cargo ya debe estar creado). Tener en cuenta que si cambiamos el cargo deberemos volver a revisar todos los campos de nuevo para que no hayan quedado cargado con los datos viejos; fecha inicio, fecha fin, etc.
