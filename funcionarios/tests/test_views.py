import csv
import io
from django.core.urlresolvers import reverse
import pytest
from funcionarios.views import protocolo_pdf, comunicaciones_funcionarios
from core.tests.helpers import probable_bug
from .factories import FuncionFactory, CargoFactory, ComunicacionCargoFactory
from core.tests.factories import PersonaFactory, ComunicacionPersonaFactory




@pytest.fixture
def planilla_csv():
    return reverse('portaldedatos.funcionarios.planilla', kwargs={'filetype': 'csv'})

@pytest.fixture
def planilla_xls():
    return reverse('portaldedatos.funcionarios.planilla', kwargs={'filetype': 'xls'})


@pytest.mark.django_db
def test_planilla_funcionario_csv_simple(website, client, planilla_csv):
    func1 = FuncionFactory(
        funcionario__nombre='María',
        funcionario__apellido='Gómez',
        cargo__nombre='Secretario'
    )
    # alto rango
    func2 = FuncionFactory(
        funcionario__nombre='Juan',
        funcionario__apellido='Pérez',
        cargo__orden=10,
        cargo__nombre='Intendente'
    )
    response = client.get(planilla_csv)
    assert response.status_code == 200
    assert response['content-type'] == 'text/csv'
    data = response.content.decode('utf-8')
    reader = csv.reader(io.StringIO(data))

    # csv incluye headers
    assert next(reader) == ['id', 'Apellido', 'Nombre', 'Area', 'DNI', 'Franja Etarea',
        'Genero', 'Edad', 'url', 'url_foto_original', 'url_foto_thumbnail', 'cargo_id',
        'cargo_categoria_nombre', 'cargo_nombre', 'fecha_inicio',
        'decreto_nro', 'decreto_pdf'
    ]
    # primera fila el funcionario de más alto rango
    assert next(reader)[:4] == [str(func2.funcionario.id), 'Pérez', 'Juan', 'Intendente']
    assert next(reader)[:4] == [str(func1.funcionario.id), 'Gómez', 'María', 'Secretario']


@pytest.mark.django_db
def test_planilla_funcionario_xls_simple(website, client, planilla_xls):
    FuncionFactory()
    response = client.get(planilla_xls)
    assert response.status_code == 200
    assert response['content-type'] == 'application/vnd.ms-excel'


@pytest.mark.django_db
def test_planilla_funcionario_no_figura_si_cargo_es_invisible(client, planilla_csv):
    f = FuncionFactory(cargo__publicado=False)
    assert not f.cargo.visible
    response = client.get(planilla_csv)
    data = response.content.decode('utf-8')
    # sólo los headers, ningun funcionario
    assert len([l for l in csv.reader(io.StringIO(data))]) == 1


@pytest.mark.django_db
def test_planilla_funcionario_no_figura_si_esta_inactivo(client, planilla_csv):
    FuncionFactory(activo=False)
    response = client.get(planilla_csv)
    data = response.content.decode('utf-8')
    # sólo los headers, ningun funcionario
    assert len([l for l in csv.reader(io.StringIO(data))]) == 1


@probable_bug
@pytest.mark.django_db
def test_protocolo_pdf(mocker, rf):
    pdftemp = mocker.patch('funcionarios.views.PDFTemplateResponse')
    f = FuncionFactory()
    response = protocolo_pdf(rf.get('/'), f.cargo_id)

    # >       titulo = funcion.cargo.oficina if funcion.cargo.depende_de else funcion.cargo.categoria.gobierno.nombre
    # E       AttributeError: 'CargoCategoria' object has no attribute 'gobierno'

    # response es intancia de lo que patcheamos
    assert response._mock_new_parent._mock_name == 'PDFTemplateResponse'
    kwargs = pdftemp.call_args_list[0][-1]
    assert kwargs['template'] == 'funcionarios/protocolo_pdf.html'
    assert kwargs['context']['funcion'] == f




@pytest.mark.django_db
def test_comunicaciones_funcionarios(rf):

    # persona con varios medios de comunicacion asociados
    persona = PersonaFactory(apellido="Marolio", nombre='Atún', genero='M')
    ComunicacionPersonaFactory(objeto=persona, tipo__nombre='Twitter', valor='@persona')
    ComunicacionPersonaFactory(objeto=persona, tipo__nombre='Linkedin', valor='http://linkedin.com/persona')
    ComunicacionPersonaFactory(
        objeto=persona,
        tipo__nombre='Email',
        valor='funcionario@gobierno.com'
    )
    ComunicacionPersonaFactory(objeto=persona, tipo__nombre='telefono', valor='01')     # no se muestra
    ComunicacionPersonaFactory(
        objeto=persona,
        tipo__nombre='email',
        privado=True,                # no se muestra por ser privado
        valor='privado@hotmail.com'
    )
    # cargo con varios medios de comunicacion asociados
    cargo = CargoFactory(nombre='Dirección de ofertas')
    ComunicacionCargoFactory(objeto=cargo, tipo__nombre='Teléfono', valor='1313456')
    ComunicacionCargoFactory(objeto=cargo, tipo__nombre='twitter', valor='@area_gobierno')
    # pisa el mail personal
    ComunicacionCargoFactory(objeto=cargo, tipo__nombre='email', valor='area@gobierno.com')
    funcion = FuncionFactory(funcionario=persona, cargo=cargo)

    response = comunicaciones_funcionarios(rf.get('/'), 'csv')

    assert response.status_code == 200
    assert response['content-type'] == 'text/csv'
    data = response.content.decode('utf-8')
    reader = csv.reader(io.StringIO(data))
    assert next(reader) == [
        'id', 'Apellido', 'Nombre', 'Email', 'twitter', 'linkedin',
        'teléfono', 'twitter_oficina', 'Area', 'Genero', 'cargo_nombre',
    ]
    # primera fila el funcionario de más alto rango
    assert next(reader) == [
        str(funcion.funcionario.id), 'Marolio', 'Atún',
        'area@gobierno.com',  # no es el personal 'funcionario@gobierno.com',
        '@persona',
        'http://linkedin.com/persona', '1313456', '@area_gobierno', 'Dirección de ofertas',
        'M', 'Dirección de ofertas'
    ]
