import datetime
from decimal import Decimal
from django.utils import timezone
import factory
import factory.fuzzy
from factory.django import DjangoModelFactory
from geo.tests.factories import DireccionFactory
from core.tests.factories import (
    ComunicacionFactory, PersonaFactory, MonedaFactory
)


class CargoCategoriaFactory(DjangoModelFactory):
    class Meta:
        model = 'funcionarios.CargoCategoria'

    nombre = factory.Sequence(lambda n: 'Secretaria {}'.format(n + 1))


class CargoFactory(DjangoModelFactory):
    class Meta:
        model = 'funcionarios.Cargo'
    categoria = factory.SubFactory(CargoCategoriaFactory)
    nombre = factory.Sequence(lambda n: 'Director {}º'.format(n + 1))


class DireccionCargoFactory(DireccionFactory):
    class Meta:
        model = 'funcionarios.DireccionCargo'
    objeto = factory.SubFactory(CargoFactory)


class ComunicacionCargoFactory(ComunicacionFactory):
    class Meta:
        model = 'funcionarios.ComunicacionCargo'
    objeto = factory.SubFactory(CargoFactory)


class FuncionFactory(DjangoModelFactory):
    class Meta:
        model = 'funcionarios.Funcion'
    funcionario = factory.SubFactory(PersonaFactory)
    cargo = factory.SubFactory(CargoFactory)
    activo = True
    fecha_inicio = factory.Maybe(
        'activo',
        yes_declaration=factory.fuzzy.FuzzyDateTime(
            timezone.now() - datetime.timedelta(days=10)
        ),
        no_declaration=None
    )


class SueldoFactory(DjangoModelFactory):
    class Meta:
        model = 'funcionarios.Sueldo'

    cargo_categoria = factory.SubFactory(CargoCategoriaFactory)
    activo = True
    fecha_inicio = factory.Maybe(
        'activo',
        yes_declaration=factory.fuzzy.FuzzyDateTime(
            timezone.now() - datetime.timedelta(days=10)
        ),
        no_declaration=None
    )
    sueldo_bruto = Decimal('40000.00')
    sueldo_bruto_moneda = factory.SubFactory(MonedaFactory)


class SueldoRealFactory(DjangoModelFactory):
    class Meta:
        model = 'funcionarios.Sueldo'

    funcion = factory.SubFactory(FuncionFactory)

    mes = factory.Iterator([datetime.date(2017, m, 1) for m in range(1, 13)])

    sueldo_bruto = Decimal('40000.00')
    sueldo_bruto_moneda = factory.SubFactory(MonedaFactory)
    sueldo_neto = factory.LazyAttribute(
        lambda o: o.sueldo_bruto * Decimal('0.75')
    )
    sueldo_neto_moneda = factory.LazyAttribute(lambda o: o.sueldo_bruto_moneda)


class WebFuncionariosFactory(DjangoModelFactory):
    class Meta:
        model = 'funcionarios.WebFuncionarios'

    cargo = factory.SubFactory(CargoFactory)
    publicado = True
    imagen = factory.django.ImageField(color='blue')
