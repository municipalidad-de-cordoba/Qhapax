from unittest.mock import Mock
import pytest
from ..factories import CargoFactory, FuncionFactory
from core.tests.helpers import probable_bug


@pytest.fixture()
def jerarquia(db):
    capanga = CargoFactory()
    subordinado = CargoFactory(depende_de=capanga)
    subordinado2 = CargoFactory(depende_de=subordinado)
    # devuelvo como mock para usar via atributos (syntactic sugar)
    return Mock(**locals())


def test_superiores(jerarquia):
    assert jerarquia.subordinado2.superiores() == [jerarquia.subordinado, jerarquia.capanga]


@probable_bug
def test_superiores_hasta_cat(jerarquia):
    resultado = jerarquia.subordinado2.superiores(hasta_categoria=jerarquia.subordinado.categoria)
    assert resultado == [jerarquia.subordinado]


def test_dependencias_recursivo(jerarquia):
    assert jerarquia.capanga.dependencias() == [jerarquia.subordinado, [jerarquia.subordinado2]]


def test_dependencias_no_recursivo(jerarquia):
    assert jerarquia.capanga.dependencias(recursive=False) == [jerarquia.subordinado]
    assert jerarquia.subordinado.dependencias(recursive=False) == [jerarquia.subordinado2]


def test_dependencias_no_publicadas(jerarquia):
    jerarquia.subordinado.publicado = False
    jerarquia.subordinado.save()
    assert jerarquia.capanga.dependencias() is None
    assert jerarquia.capanga.dependencias(solo_publicados=False) == [jerarquia.subordinado, [jerarquia.subordinado2]]


def test_dependencias_categoria_no_publicadas(jerarquia):
    jerarquia.subordinado.categoria.publicado = False
    jerarquia.subordinado.categoria.save()
    assert jerarquia.capanga.dependencias() is None
    assert jerarquia.capanga.dependencias(solo_publicados=False) == [jerarquia.subordinado, [jerarquia.subordinado2]]


def test_dependencias_no_activado(jerarquia):
    jerarquia.subordinado.activado = False
    jerarquia.subordinado.save()
    assert jerarquia.capanga.dependencias() is None
    assert jerarquia.capanga.dependencias(solo_publicados=False) == [jerarquia.subordinado, [jerarquia.subordinado2]]


def test_cargo_maximo(jerarquia):
    assert jerarquia.capanga.es_cargo_maximo
    assert not jerarquia.subordinado.es_cargo_maximo
    assert not jerarquia.subordinado2.es_cargo_maximo


def test_cargo_secundario(jerarquia):
    assert not jerarquia.capanga.es_cargo_secundario
    assert jerarquia.subordinado.es_cargo_secundario
    assert not jerarquia.subordinado2.es_cargo_secundario


def test_no_visible_x_no_publicado():
    cargo = CargoFactory(publicado=False)
    assert not cargo.visible

def test_no_visible_x_no_publicado():
    cargo = CargoFactory.build(publicado=False)
    assert not cargo.visible

@probable_bug
@pytest.mark.django_db
def test_no_visible_x_superior_invisible():
    capanga = CargoFactory(publicado=False)
    subordinado = CargoFactory(depende_de=capanga)
    assert not subordinado.visible


def test_area_gobierno(jerarquia):
    assert jerarquia.capanga.area_gobierno == jerarquia.capanga
    assert jerarquia.subordinado.area_gobierno == jerarquia.subordinado
    assert jerarquia.subordinado2.area_gobierno == jerarquia.subordinado


@pytest.mark.django_db
def test_en_funcion_activa():
    funcion = FuncionFactory(activo=True)
    assert funcion.cargo.en_funcion_activa == funcion


@pytest.mark.django_db
def test_en_funcion_activa_uno_inactivo():
    viejo = FuncionFactory(activo=False)
    funcion = FuncionFactory(activo=True, cargo=viejo.cargo)
    assert funcion.cargo.en_funcion_activa == funcion
