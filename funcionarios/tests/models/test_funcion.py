import pytest
from ..factories import CargoFactory, FuncionFactory, ComunicacionCargoFactory
from core.tests.factories import PersonaFactory, ComunicacionPersonaFactory
from core.tests.helpers import probable_bug


@probable_bug
@pytest.mark.parametrize('genero, nombre_fem, nombre, expected', [
    ('M', None, 'Secretario', 'Secretario'),
    ('F', 'Secretaria', 'Secretario', 'Secretaria'),
])
def test_categoria_gen(genero, nombre_fem, nombre, expected):
    """
    la property se basa en un atributo "categoria" del modelo funcion que es inexistente.
    ¿hubo un refactor y quedó desactualizado?
    de cualquier manera no parece usarse en ningun lado

    """
    f = FuncionFactory.build(
        categoria__nombre=nombre, categoria__nombre_fem=nombre_fem, funcionario__genero=genero
    )
    assert f.categoria_gen == expected


@probable_bug
def test_categoria_corto_gen():
    # idem caso anterior.
    assert False


@pytest.mark.parametrize('genero, nombre_fem, nombre, expected', [
    ('M', None, 'Secretario', 'Secretario'),
    ('F', 'Secretaria', 'Secretario', 'Secretaria'),
])
def test_cargo_gen(genero, nombre_fem, nombre, expected):
    f = FuncionFactory.build(cargo__nombre=nombre, cargo__nombre_fem=nombre_fem, funcionario__genero=genero)
    assert f.cargo_gen == expected


@pytest.mark.django_db
def test_get_all_email():

    # persona con varios medios de comunicacion asociados
    persona = PersonaFactory()
    ComunicacionPersonaFactory(objeto=persona, tipo__nombre='web')
    ComunicacionPersonaFactory(objeto=persona, tipo__nombre='email', valor='funcionario@gobierno.com')
    ComunicacionPersonaFactory(objeto=persona,
                               tipo__nombre='email',
                               privado=True,           #  get_all_email se usa sólo en vistas con login
                               valor='privado@hotmail.com')

    # cargo con varios medios de comunicacion asociados
    cargo = CargoFactory()
    ComunicacionCargoFactory(objeto=cargo, tipo__nombre='web')
    ComunicacionCargoFactory(objeto=cargo, tipo__nombre='email', valor='area@gobierno.com')

    funcion = FuncionFactory(funcionario=persona, cargo=cargo)
    # FIXME: no se garantiza orden. Es correcto ?
    assert set(funcion.get_all_emails()) == {'funcionario@gobierno.com', 'privado@hotmail.com', 'area@gobierno.com'}

