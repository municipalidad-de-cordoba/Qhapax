# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-08-11 01:53
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('funcionarios', '0008_cargo_activado'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='cargo',
            options={'ordering': ['categoria__orden', 'orden', 'nombre']},
        ),
    ]
