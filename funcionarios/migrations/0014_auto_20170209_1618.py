# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-02-09 19:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('funcionarios', '0013_auto_20161006_1057'),
    ]

    operations = [
        migrations.AddField(
            model_name='funcion',
            name='boletin_renuncia_fecha_publicacion',
            field=models.DateField(blank=True, null=True, verbose_name='Fecha de publicación en boletín del Decreto de baja o renuncia'),
        ),
        migrations.AddField(
            model_name='funcion',
            name='boletin_renuncia_url',
            field=models.URLField(blank=True, null=True, verbose_name='URL al Decreto de baja o renuncia en el Boletín Oficial'),
        ),
        migrations.AddField(
            model_name='funcion',
            name='decreto_renuncia_nro',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Nro del Decreto de baja o renuncia'),
        ),
        migrations.AddField(
            model_name='funcion',
            name='decreto_renuncia_pdf',
            field=models.FileField(blank=True, null=True, upload_to='decretos-funcionarios/', verbose_name='Decreto de baja o renuncia en PDF o JPG'),
        ),
        migrations.AlterField(
            model_name='funcion',
            name='boletin_fecha_publicacion',
            field=models.DateField(blank=True, null=True, verbose_name='Fecha de publicación en boletín del Decreto de designación'),
        ),
        migrations.AlterField(
            model_name='funcion',
            name='boletin_url',
            field=models.URLField(blank=True, null=True, verbose_name='URL al Decreto de designación en el Boletín Oficial'),
        ),
        migrations.AlterField(
            model_name='funcion',
            name='decreto_nro',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Nro del Decreto de designación'),
        ),
        migrations.AlterField(
            model_name='funcion',
            name='decreto_pdf',
            field=models.FileField(blank=True, null=True, upload_to='decretos-funcionarios/', verbose_name='Decreto de designación en PDF o JPG'),
        ),
    ]
