# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-07-04 19:51
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('core', '0003_auto_20160704_1651'),
        ('gobiernos', '0003_auto_20160704_1651'),
        ('funcionarios', '0002_auto_20160704_1650'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cargo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=150)),
                ('slug', models.SlugField(max_length=150)),
                ('descripcion', models.TextField(blank=True, null=True)),
                ('html', ckeditor.fields.RichTextField(blank=True, null=True)),
                ('electivo', models.BooleanField(default=False)),
                ('publicado', models.BooleanField(default=True)),
                ('orden', models.PositiveIntegerField(default=100)),
                ('creado', models.DateTimeField(auto_now_add=True)),
                ('ultima_modificacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['orden', 'categoria__orden', 'nombre'],
            },
        ),
        migrations.CreateModel(
            name='CargoCategoria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=150)),
                ('nombre_corto', models.CharField(blank=True, max_length=50, null=True)),
                ('slug', models.SlugField(max_length=150)),
                ('descripcion', models.TextField(blank=True, null=True)),
                ('requiere_declaracion_jurada', models.BooleanField(default=True)),
                ('publicado', models.BooleanField(default=True)),
                ('orden', models.PositiveIntegerField(default=100)),
                ('creado', models.DateTimeField(auto_now_add=True)),
                ('ultima_modificacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['orden', 'nombre'],
            },
        ),
        migrations.CreateModel(
            name='ComunicacionCargo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('valor', models.CharField(max_length=220)),
                ('descripcion', models.TextField(blank=True, null=True)),
                ('privado', models.BooleanField(default=False)),
                ('creado', models.DateTimeField(auto_now_add=True)),
                ('ultima_modificacion', models.DateTimeField(auto_now=True)),
                ('objeto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='funcionarios.Cargo')),
                ('tipo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tipo_comunicacion_funcionarios_comunicacioncargo', to='core.ComunicacionTipo')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Funcion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_inicio', models.DateField(blank=True, null=True)),
                ('fecha_fin', models.DateField(blank=True, null=True)),
                ('activo', models.BooleanField()),
                ('decreto_pdf', models.FileField(blank=True, null=True, upload_to='decretos-funcionarios/')),
                ('boletin_fecha_publicacion', models.DateField(blank=True, null=True)),
                ('boletin_url', models.URLField(blank=True, null=True)),
                ('creado', models.DateTimeField(auto_now_add=True)),
                ('ultima_modificacion', models.DateTimeField(auto_now=True)),
                ('cargo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='funcionarios.Cargo')),
                ('funcionario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Persona')),
            ],
            options={
                'verbose_name_plural': 'Funciones',
                'permissions': (('decreto_funcion', 'Puede Decreto, fecha y publicación'),),
            },
        ),
        migrations.CreateModel(
            name='Sueldo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_inicio', models.DateField(blank=True, null=True)),
                ('fecha_fin', models.DateField(blank=True, null=True)),
                ('activo', models.BooleanField()),
                ('sueldo_bruto', models.DecimalField(decimal_places=2, default=0.0, max_digits=12)),
                ('creado', models.DateTimeField(auto_now_add=True)),
                ('ultima_modificacion', models.DateTimeField(auto_now=True)),
                ('cargo_categoria', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='funcionarios.CargoCategoria')),
                ('sueldo_bruto_moneda', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Moneda_Bruto', to='core.Moneda')),
            ],
        ),
        migrations.CreateModel(
            name='SueldoReal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mes', models.DateField()),
                ('sueldo_bruto', models.DecimalField(decimal_places=2, default=0.0, max_digits=12)),
                ('sueldo_neto', models.DecimalField(decimal_places=2, default=0.0, max_digits=12)),
                ('creado', models.DateTimeField(auto_now_add=True)),
                ('ultima_modificacion', models.DateTimeField(auto_now=True)),
                ('funcion', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='funcionarios.Funcion')),
                ('sueldo_bruto_moneda', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Moneda_Bruto_Real', to='core.Moneda')),
                ('sueldo_neto_moneda', models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='Moneda_Neto_Real', to='core.Moneda')),
            ],
        ),
        migrations.CreateModel(
            name='WebFuncionarios',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(default='Mapa de la función pública municipal', max_length=120)),
                ('slug', models.SlugField(max_length=120)),
                ('sub_titulo', models.CharField(default='Organigrama de funcionarios', max_length=120)),
                ('html', ckeditor.fields.RichTextField(blank=True, null=True)),
                ('publicado', models.BooleanField(default=False)),
                ('imagen', models.ImageField(blank=True, null=True, upload_to='imagenes/organigrama')),
                ('creado', models.DateTimeField(auto_now_add=True)),
                ('ultima_modificacion', models.DateTimeField(auto_now=True)),
                ('cargo', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='funcionarios.Cargo')),
                ('hasta_categoria', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='funcionarios.CargoCategoria')),
            ],
            options={
                'verbose_name_plural': 'Organigramas',
            },
        ),
        migrations.AddField(
            model_name='cargo',
            name='categoria',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='funcionarios.CargoCategoria'),
        ),
        migrations.AddField(
            model_name='cargo',
            name='depende_de',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='funcionarios.Cargo'),
        ),
        migrations.AlterUniqueTogether(
            name='webfuncionarios',
            unique_together=set([('slug', )]),
        ),
    ]
