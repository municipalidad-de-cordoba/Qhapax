from django.conf import settings
from funcionarios.models import (Cargo, Funcion, DireccionCargo,
                                 ComunicacionCargo)
from django.shortcuts import render, get_object_or_404
import django_excel as excel
from django.views.decorators.cache import cache_page
from core.models import Persona, ComunicacionPersona, ComunicacionTipo
from django.contrib.auth.decorators import login_required
from django.http import Http404
from wkhtmltopdf.views import PDFTemplateResponse
import os
from datetime import datetime


@cache_page(60 * 60 * 12)  # 12 h
def protocolo_pdf(request, desde_cargo_id, save_to_disk=False):
    '''
    Planilla de funcionarios excel/csv
    '''

    # obtener una declaración
    funcion = get_object_or_404(Funcion, cargo_id=desde_cargo_id, activo=True)
    if not funcion.activo or not funcion.cargo.publicado:
        raise Http404("Cargo no existente")

    # el titulo es el nombre de la oficina en caso de que no sea la autoridad 1
    # en cuyo caso es el nombre del gobierno
    titulo = funcion.cargo.oficina if funcion.cargo.depende_de else settings.NOMBRE_GOBIERNO
    dependencias = funcion.cargo.dependencias_directas()

    template = 'funcionarios/protocolo_pdf.html'
    context = {'funcion': funcion, 'titulo': titulo,
               'dependencias': dependencias}

    response = PDFTemplateResponse(request=request,
                                   template=template,
                                   # fuerza descarga OK
                                   # filename="plan-{}.pdf".format(plan_de_metas_id),
                                   context=context,
                                   header_template='funcionarios/protocolo_header.html',
                                   footer_template='funcionarios/protocolo_footer.html',
                                   show_content_in_browser=False,
                                   cmd_options={'page-size': 'A4',
                                                'title': 'Protocolo Municipalidad de Córdoba',
                                                   'margin-top': 20,
                                                   'margin-bottom': 15,
                                                   'margin-left': 0,
                                                   'margin-right': 0,
                                                   'footer-line': None
                                                })

    if save_to_disk:
        d = datetime.now()
        hoy = d.strftime("%y-%m-%d")
        filename = 'protocolo-{}-{}.pdf'.format(funcion.cargo.oficina, hoy)
        dest = os.path.join(settings.MEDIA_ROOT, filename)
        f = open(dest, 'wb')
        f.write(response.rendered_content)
        f.close()

    return response


@cache_page(60 * 60 * 12)  # 12 h
def planilla_funcionarios(request, filetype):
    '''
    Planilla de funcionarios excel/csv
    '''

    # obtener una declaración
    funciones = Funcion.objects.filter(
        activo=True).order_by(
        'cargo__categoria__orden',
        'cargo__orden')
    csv_list = []
    csv_list.append(['id',
                     'Apellido',
                     'Nombre',
                     'Area',
                     'DNI',
                     'Franja Etarea',
                     'Genero',
                     'Edad',
                     'url',
                     'url_foto_original',
                     'url_foto_thumbnail',
                     'cargo_id',
                     'cargo_categoria_nombre',
                     'cargo_nombre',
                     'fecha_inicio',
                     'decreto_nro',
                     'decreto_pdf',
                     'Fecha designacion'])
    for funcion in funciones:
        if not funcion.cargo.visible:
            continue
        funcionario = funcion.funcionario
        dni = '{} {}'.format(funcionario.tipo_id, funcionario.unique_id)
        url = "{}{}".format(request.website, funcionario.get_absolute_url())
        decreto_pdf = "" if not funcion.decreto_pdf else "{}{}".format(
            request.website, funcion.decreto_pdf)
        try:
            foto = "{}{}".format(request.website, funcionario.foto.url)
            foto_thumb = "{}{}".format(
                request.website,
                funcionario.foto.thumbnail['125x125'].url)
        except (ValueError, FileNotFoundError):
            foto = ""
            foto_thumb = ""

        area = funcion.cargo.area_gobierno.nombre
        csv_list.append([
            funcionario.id,
            funcionario.apellido,
            funcionario.nombre,
            area,
            dni,
            funcionario.franja_etaria,
            funcionario.genero,
            funcionario.edad,
            url,
            foto,
            foto_thumb,
            funcion.cargo.id,
            funcion.cargo.categoria.nombre,
            funcion.cargo.nombre,
            funcion.fecha_inicio,
            funcion.decreto_nro,
            decreto_pdf,
            funcion.fecha_inicio
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 h
def comunicaciones_funcionarios(request, filetype):
    '''
    Planilla de funcionarios excel/csv
    '''

    # obtener una declaración
    funciones = Funcion.objects.filter(
        activo=True).order_by(
        'cargo__categoria__orden',
        'cargo__orden')
    csv_list = []
    csv_list.append(['id',
                     'Apellido',
                     'Nombre',
                     'Email',
                     'twitter',
                     'linkedin',
                     'teléfono',
                     'twitter_oficina',
                     'Area',
                     'Genero',
                     'cargo_nombre',
                     ])
    for funcion in funciones:
        if not funcion.cargo.visible:
            continue
        funcionario = funcion.funcionario
        area = funcion.cargo.area_gobierno.nombre

        comunicaciones_cargo = funcion.cargo.comunicacioncargo_set.filter(
            privado=False)
        comunicaciones_persona = ComunicacionPersona.objects.filter(
            privado=False, objeto=funcionario)

        twitter = ''
        twitter_oficina = ''
        linkedin = ''
        tel_oficina = ''
        email = ''
        for c in comunicaciones_persona:
            if c.tipo.nombre_normalizado == 'twitter':
                twitter = c.valor
            elif c.tipo.nombre_normalizado == 'linkedin':
                linkedin = c.valor
            elif c.tipo.nombre_normalizado == 'email':
                email = c.valor
        for c in comunicaciones_cargo:
            if c.tipo.nombre_normalizado == 'twitter':
                twitter_oficina = c.valor
            elif c.tipo.nombre_normalizado == 'telefono':
                tel_oficina = c.valor
            elif c.tipo.nombre_normalizado == 'email':
                email = c.valor

        csv_list.append([
            funcionario.id,
            funcionario.apellido,
            funcionario.nombre,
            email,
            twitter, linkedin, tel_oficina, twitter_oficina,
            area,
            funcionario.genero,
            funcion.cargo.nombre,
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 h
def celular_corporativo_funcionarios(request, filetype):
    '''
    Planilla de telefonos móviles corporativos de funcionarios
    '''

    # obtener una declaración
    funciones = Funcion.objects.filter(
        activo=True).order_by(
        'cargo__categoria__orden',
        'cargo__orden')
    csv_list = []
    csv_list.append(['id', 'Apellido', 'Nombre',
                     'Corporativo', 'Area', 'cargo_nombre'])
    for funcion in funciones:
        if not funcion.cargo.visible:
            continue
        funcionario = funcion.funcionario
        area = funcion.cargo.area_gobierno.nombre

        comunicaciones_cargo = funcion.cargo.comunicacioncargo_set.filter()
        comunicaciones_persona = ComunicacionPersona.objects.filter(
            objeto=funcionario)

        corpo = ''
        for c in comunicaciones_persona:
            if c.tipo.nombre == 'Celular Corporativo':
                corpo = c.valor
        for c in comunicaciones_cargo:
            if c.tipo.nombre == 'Celular Corporativo':
                corpo = c.valor

        csv_list.append([
            funcionario.id,
            funcionario.apellido,
            funcionario.nombre,
            corpo,
            area,
            funcion.cargo.nombre,
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@login_required
@cache_page(60 * 60 * 12)  # 12 h
def celular_privado_funcionarios(request, filetype):
    '''
    Planilla de telefonos móviles corporativos de funcionarios
    '''

    # obtener una declaración
    funciones = Funcion.objects.filter(
        activo=True).order_by(
        'cargo__categoria__orden',
        'cargo__orden')
    csv_list = []
    csv_list.append(['id',
                     'Apellido',
                     'Nombre',
                     'Celular privado',
                     'Email privado',
                     'Area',
                     'cargo_nombre'])
    for funcion in funciones:
        if not funcion.cargo.visible:
            continue
        funcionario = funcion.funcionario
        area = funcion.cargo.area_gobierno.nombre

        comunicaciones_persona = ComunicacionPersona.objects.filter(
            privado=True, objeto=funcionario)

        celu = ''
        email = ''
        for c in comunicaciones_persona:
            if c.tipo.nombre == 'Celular':
                celu = c.valor
            elif c.tipo.nombre == 'Email':
                email = c.valor

        csv_list.append([
            funcionario.id,
            funcionario.apellido,
            funcionario.nombre,
            celu,
            email,
            area,
            funcion.cargo.nombre,
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@login_required
@cache_page(60 * 60 * 12)  # 12 h
def comunicacion_funcionario(request, persona_id):
    '''
    Todas las vías de comunicación publicas y privadas de un funcionario
    '''

    persona = get_object_or_404(Persona, pk=persona_id)
    funciones = Funcion.objects.filter(funcionario=persona, activo=True)
    if len(funciones) == 1:
        funcion = funciones[0]
    else:
        funcion = None

    comunicaciones_persona = ComunicacionPersona.objects.filter(objeto=persona)
    if funcion:
        area = funcion.cargo.area_gobierno.nombre
        comunicaciones_cargo = funcion.cargo.comunicacioncargo_set.filter()
    else:
        area = None
        comunicaciones_cargo = []

    context = {'persona': persona, 'funcion': funcion, 'area': area,
               'comunicaciones_cargo': comunicaciones_cargo,
               'comunicaciones_persona': comunicaciones_persona}
    url = 'funcionarios/comunicacion-privada.html'
    return render(request, url, context)


def index(request):
    raise Http404("Página no existente")


@cache_page(60 * 60 * 12)  # 12 h
def oficinas_publicas_a_google_maps(request, filetype):
    '''
    Exportar una listas de las oficinas públicas a CSV para que Google lo
    muestre en Maps
    Estas son las especificaciones que recibimos
    '''

    csv_list = []
    csv_list.append(['Código de tienda',
                     'Nombre de la empresa',
                     'Línea de dirección 1',
                     'Línea de dirección 2',
                     'Línea de dirección 3',
                     'Línea de dirección 4',
                     'Línea de dirección 5',
                     'Sublocalidad',
                     'Localidad',
                     'Área administrativa',
                     'País',
                     'Código postal',
                     'Latitud',
                     'Longitud',
                     'Teléfono principal',
                     'Teléfonos adicionales',
                     'Sitio web',
                     'Categoría principal',
                     'Categorías adicionales',
                     'Horario del domingo',
                     'Horario del lunes',
                     'Horario del martes',
                     'Horario del miércoles',
                     'Horario del jueves',
                     'Horario del viernes',
                     'Horario del sábado',
                     'Horario especial',
                     'Foto de perfil',
                     'Foto de logotipo',
                     'Foto de portada',
                     'Otras fotos',
                     'Foto preferida',
                     'Etiquetas',
                     'Teléfono para las extensiones de lugar de AdWords'])

    # solo las que sean "visibles" (es una funcion que consulta otras cosas y
    # no una propiedad)
    oficinas = [oficina for oficina in Cargo.objects.all() if oficina.visible]

    for oficina in oficinas:
        # este número es un identificador único, lo piden de 3 cifras (?)
        codigo = '{:0>3}'.format(oficina.id)
        direcciones = oficina.direccioncargo_set.all()
        direccion = '' if len(direcciones) == 0 else direcciones[0].direccion
        barrio = ''
        # debería obtenerse cruzando el lat, long contra la base de polígonos
        # de barrios

        # FIXME, hay que acomodar los tipos de comunicacion
        COMUNICACION_TIPO = {x.nombre.lower().replace(
            " ", "_"): x for x in ComunicacionTipo.objects.all()}
        telefonos = oficina.comunicacioncargo_set.filter(
            privado=False, tipo=COMUNICACION_TIPO['teléfono'])

        tel_principal = ''
        tel_otros = ''

        if len(telefonos) > 0:
            tel_principal = telefonos[0].valor

        if len(telefonos) > 1:
            tels = [tel.valor for tel in telefonos[1:]]
            tel_otros = ', '.join(tels)

        web = '{}://{}{}'.format(settings.PROTOCOLO_PRODUCCION,
                                 settings.DOMINIO_PRODUCCION, oficina.get_absolute_url())

        csv_list.append([codigo, oficina.oficina, direccion,
                         '', '', '', '',  # líneas de direccion 2, 3, 4 y 5
                         '',  # sublocalidad no se usa
                         barrio,  # localidad es barrio (? eso dicen)
                         'Córdoba',  # Área administrativa
                         'AR',  # código de país
                         '',  # CP
                         '', '',  # lat y long
                         tel_principal, tel_otros,  # teléfonos
                         web,  #
                         'Oficina de administracion municipal', '',
                         # categoría y sub
                         # hotrarios de domingoi, lunes .... sabado, horario
                         # especial
                         '', '', '', '', '', '', '', '',
                         '', '', '', '', '',
                         # foto de perfil, portada, logotipo, otras y preferida
                         '',  # etiquetas
                         '',  # tel de adwords
                         ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


# pass
