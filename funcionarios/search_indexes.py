import datetime
from haystack import indexes
from .models import Cargo


class CargoIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    nombre = indexes.CharField(model_attr='nombre')
    descripcion = indexes.CharField(model_attr='descripcion')
    depende_de = indexes.CharField(model_attr='depende_de', null=True)
    electivo = indexes.BooleanField(model_attr='electivo')
    publicado = indexes.BooleanField(model_attr='publicado')
    categoria = indexes.CharField(model_attr='categoria', null=True)
    
    comunicacion = indexes.MultiValueField()
    visible = indexes.BooleanField()

    def prepare_comunicacion(self, obj):
        return [comu.absolute_url for comu in obj.comunicacioncargo_set.filter(privado=False)]

    def prepare_visible(self, obj):
        return obj.visible

    def get_model(self):
        return Cargo

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(funcion__activo=True, publicado=True)

    def prepare(self, object):
        self.prepared_data = super(CargoIndex, self).prepare(object)

        return self.prepared_data
