from django.contrib import admin
from django.core.urlresolvers import reverse
from .models import (DireccionCargo, CargoCategoria, Cargo, Funcion, Sueldo,
                     SueldoReal, Moneda, WebFuncionarios, ComunicacionCargo)
from .forms import Funcion2Form, FuncionSoloDecretoForm
from simple_history.admin import SimpleHistoryAdmin


class ComunicacionInline(admin.StackedInline):
    model = ComunicacionCargo
    extra = 1


class CargoCategoriaAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'nombre_corto', 'orden')
    exclude = ('slug',)
    search_fields = ['nombre', 'nombre_corto']


class CargoAdmin(SimpleHistoryAdmin):
    def ultimo_editor(self, obj):
        if len(obj.history.all()) > 0:
            return obj.history.all()[0].history_user
        else:
            return ''

    def en_funciones(self, obj):
        func = obj.en_funcion_activa
        res = "Sin funcionario" if func is None else func.funcionario.nombrepublico
        return res

    exclude = ('slug',)
    search_fields = ('nombre', 'categoria__nombre')
    list_display = (
        'nombre',
        'publicado',
        'activado',
        'categoria',
        'depende_de',
        'en_funciones',
        'orden',
        'ultimo_editor')
    # filter_horizontal = ('comunicacion', )
    inlines = [ComunicacionInline]
    list_filter = ['publicado', 'activado', 'requiere_decreto_designacion']


class SueldoAdmin(admin.ModelAdmin):
    search_fields = ['cargo_categoria']
    list_display = ('cargo_categoria', 'fecha_inicio', 'fecha_fin', 'activo',
                    'sueldo_bruto_moneda', 'sueldo_bruto')


class SueldoRealAdmin(admin.ModelAdmin):
    list_filter = ['mes']
    search_fields = [
        'funcion__funcionario__nombre',
        'funcion__funcionario__apellido',
        'funcion__cargo__nombre',
        'funcion__cargo__categoria__nombre',
        'funcion__cargo__categoria__nombre_corto']

    def sueldo_bruto_ok(self, obj):
        """ sueldo bruto y moneda """
        return '{} {}'.format(
            obj.sueldo_bruto_moneda.simbolo,
            obj.sueldo_bruto)
    sueldo_bruto_ok.short_description = 'Bruto'

    def sueldo_neto_ok(self, obj):
        """ sueldo neto y moneda """
        return '{} {}'.format(obj.sueldo_neto_moneda.simbolo, obj.sueldo_neto)
    sueldo_neto_ok.short_description = 'Neto'

    def ficha_funcionario(self, obj):
        ficha_url = reverse(
            'website.funcionarios.persona',
            kwargs={
                'persona': obj.funcion.funcionario.slug,
                'persona_id': obj.funcion.funcionario.id})
        return '<a target="_blank" href="{}">{} {}</a>'.format(
            ficha_url, obj.funcion.funcionario.nombre, obj.funcion.funcionario.apellido)
    ficha_funcionario.allow_tags = True

    list_display = (
        'funcion',
        'mes',
        'ficha_funcionario',
        'sueldo_bruto_ok',
        'sueldo_neto_ok')


class WebFuncionariosAdmin(admin.ModelAdmin):
    list_display = ('titulo', )
    exclude = ('slug',)


class FuncionAdmin(SimpleHistoryAdmin):
    def ultimo_editor(self, obj):
        return obj.history.all()[0].history_user

    form = Funcion2Form

    def ficha_funcionario(self, obj):
        ficha_url = reverse(
            'website.funcionarios.persona',
            kwargs={
                'persona': obj.funcionario.slug,
                'persona_id': obj.funcionario.id})
        return '<a target="_blank" href="{}">{} {}</a>'.format(
            ficha_url, obj.funcionario.nombre, obj.funcionario.apellido)
    ficha_funcionario.allow_tags = True

    def get_form(self, request, obj=None, **kwargs):
        ''' si es alguien que tiene un permiso limitado solo permitir esa carga '''
        if request.user.has_perm('funcionarios.decreto_funcion') and not request.user.has_perm(
                'funcionarios.add_funcion'):
            # permiso especial, solo puede tocar decreto y vinculados
            kwargs['form'] = FuncionSoloDecretoForm
        return super(FuncionAdmin, self).get_form(request, obj, **kwargs)

    def depende(self, obj):
        res = ""
        if obj.cargo.depende_de is not None:
            res = obj.cargo.depende_de.nombre
        return res

    def id_cargo(self, obj):
        return obj.cargo.id

    list_filter = ('cargo__categoria', 'activo')
    list_display = (
        'funcionario',
        'activo',
        'id_cargo',
        'cargo',
        'depende',
        'ficha_funcionario',
        'fecha_inicio',
        'fecha_fin',
        'decreto_nro',
        'decreto_pdf',
        'ultimo_editor')
    search_fields = ('funcionario__nombre', 'funcionario__apellido',
                     'cargo__nombre', 'cargo__categoria__nombre',
                     'funcionario__unique_id')

    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


class DireccionAdmin(admin.ModelAdmin):
    list_display = ("direccion", "calle", "numero", "localidad", "provincia",
                    "pais", "cp", "latitud", "longitud")
    search_fields = ["direccion", ]
    list_filter = ['provincia', 'pais']


admin.site.register(DireccionCargo, DireccionAdmin)
admin.site.register(CargoCategoria, CargoCategoriaAdmin)
admin.site.register(Cargo, CargoAdmin)
admin.site.register(Funcion, FuncionAdmin)
admin.site.register(Sueldo, SueldoAdmin)
admin.site.register(SueldoReal, SueldoRealAdmin)
admin.site.register(Moneda)
admin.site.register(WebFuncionarios, WebFuncionariosAdmin)
