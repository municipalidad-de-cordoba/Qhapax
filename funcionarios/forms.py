from django.forms import ModelChoiceField
from django import forms
from .models import Cargo, Funcion
# from core.models import Persona
from core.forms import PersonaWidget


class CargoChoiceField(ModelChoiceField):
    """
    Para mostrar los cargos con la referencia a quien depende para evitar
    errores en cargos que se llaman igual
    """

    def label_from_instance(self, obj):
        res = obj.nombre
        if obj.depende_de is not None:
            res = "{} (en {})".format(obj.nombre, obj.depende_de.nombre)

        res += " (Sin funcionario)" if obj.en_funcion_activa is None else " (ACTIVO: {})".format(
            obj.en_funcion_activa.funcionario.nombrepublico)
        return res


class PersonaChoiceField(ModelChoiceField):
    """
    Para mostrar los cargos con la referencia a quien depende para evitar
    errores en cargos que se llaman igual
    """

    def label_from_instance(self, obj):
        res = "{}, {} ({} {})".format(
            obj.apellido,
            obj.nombre,
            obj.tipo_id.nombre,
            obj.unique_id)
        return res


class Funcion2Form(forms.ModelForm):
    '''
    Funcion con la seleccion de cargo mejorada
    '''
    cargo = CargoChoiceField(queryset=Cargo.objects.filter(activado=True))
    # funcionario = PersonaChoiceField(queryset = Persona.objects.all().order_by('apellido'))

    class Meta:
        model = Funcion
        fields = [
            'funcionario',
            'cargo',
            'fecha_inicio',
            'fecha_fin',
            'activo',
            'is_adhonorem',
            'decreto_pdf',
            'decreto_nro',
            'boletin_fecha_publicacion',
            'boletin_url',
            'decreto_renuncia_pdf',
            'decreto_renuncia_nro',
            'boletin_renuncia_fecha_publicacion',
            'boletin_renuncia_url']
        widgets = {'funcionario': PersonaWidget}


class FuncionSoloDecretoForm(forms.ModelForm):
    '''
    Solo edicion decreto, permiso especial (temporal)
    '''
    class Meta:
        model = Funcion
        fields = [
            'funcionario',
            'cargo',
            'fecha_inicio',
            'decreto_pdf',
            'boletin_fecha_publicacion',
            'boletin_url']
        readonly = ['funcionario', 'cargo']

        widgets = {
            'funcionario': forms.TextInput(
                attrs={
                    'readonly': True,
                    'style': 'background-color: gray'}),
            'cargo': forms.TextInput(
                attrs={
                    'readonly': True,
                    'style': 'background-color: gray'})}
