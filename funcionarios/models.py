from django.db import models
from django.utils.text import slugify
from ckeditor.fields import RichTextField
from core.models import Moneda, Comunicacion, Persona, ComunicacionPersona
from django.core.urlresolvers import reverse
from django.http import Http404
from geo.models import Direccion
from simple_history.models import HistoricalRecords
from django.conf import settings
from versatileimagefield.fields import VersatileImageField


class CargoCategoria(models.Model):
    """
    Categorias de los puestos de función pública. De aqui se definen los salarios.
    Secretario, Subsecretario, Director, SubDirector, etc.
    """
    # ej: SecretariO del departamento ejecutivo
    nombre = models.CharField(max_length=150)
    # ej: SecretariA del departamento ejecutivo
    nombre_fem = models.CharField(max_length=150, null=True)
    # ej: SecretariO
    nombre_corto = models.CharField(max_length=50, null=True, blank=True)
    # ej: SecretariA
    nombre_corto_fem = models.CharField(max_length=50, null=True, blank=True)

    slug = models.SlugField(max_length=150)
    descripcion = models.TextField(null=True, blank=True)
    # saber si es un cargo al que despues tengo que exigirle una declaración jurada anual
    requiere_declaracion_jurada = models.BooleanField(default=True)
    # hay categorias que no se publican porque no son funcionarios públicos
    publicado = models.BooleanField(default=True)
    # ver si permite que la gente solicite audiencias vía web
    permite_audiencia = models.BooleanField(default=True)
    # para ordenar en listas en general
    orden = models.PositiveIntegerField(default=100)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.nombre)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        super(CargoCategoria, self).save(*args, **kwargs)

    class Meta:
        ordering = ['orden', 'nombre']


class Cargo(models.Model):
    '''
    Cargos específicos en una categoría.
    Por ejemplo <Director de Sistemas> de la categoría <Director de área>
    '''
    categoria = models.ForeignKey(CargoCategoria, on_delete=models.CASCADE)
    # ej: Director de Sistemas
    nombre = models.CharField(max_length=150)
    # ej: DirectorA de Sistemas
    nombre_fem = models.CharField(max_length=150, null=True)
    # ej: Dirección de sistemas
    oficina = models.CharField(max_length=150, null=True)

    slug = models.SlugField(max_length=150)
    descripcion = models.TextField(null=True, blank=True)  # texto de uso interno
    
    logo = VersatileImageField(upload_to='imagenes/cargos', null=True, blank=True,
                                help_text='Imagen para usar cuando el cargo no esté ocupado o para representar el cargo')
        
    html = RichTextField(null=True, blank=True)  # texto explicativo abierto
    # salvo el cargo principal (Intendente por ejemplo) todos los demás
    # dependen de otro funcionario (y solo de uno)
    depende_de = models.ForeignKey('self', on_delete=models.CASCADE,
                                    null=True, blank=True)
    electivo = models.BooleanField(default=False)  # el cargo se gano en elecciones?
    publicado = models.BooleanField(default=True)
    activado = models.BooleanField(default=True)
    # para ordenar en listas en general
    orden = models.PositiveIntegerField(default=100)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    # ver si requiere decreto de designación
    requiere_decreto_designacion = models.BooleanField(default=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    def get_absolute_url(self):
        return reverse('website.funcionarios.oficina', kwargs={'oficina': self.slug, 'oficina_id': self.id})

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        super(Cargo, self).save(*args, **kwargs)

    def dependencias(self, hasta_categoria=None, solo_publicados=True, recursive=True):
        '''
        obtener arbol de cargos dependientes
        opcionalmente para al llegar a cargos de una
        categoría específica
        '''
        if self.categoria == hasta_categoria:
            return None

        if solo_publicados and (not self.publicado or not self.categoria.publicado or not self.activado):
            return None

        deps = []
        for cargo in self.cargo_set.all():
            if (cargo.publicado and cargo.categoria.publicado and cargo.activado) or not solo_publicados:
                deps.append(cargo)
                if recursive:
                    d = cargo.dependencias(hasta_categoria, solo_publicados, recursive)
                    if d is not None:
                        for dep in d:
                            deps.append(dep)

        return deps or None
    
    def dependencias_directas(self):
        ''' equivalente a self.dependencias(recursivo=False) para usar en templates '''
        return self.dependencias(recursive=False)

    def superiores(self, hasta_categoria=None, solo_publicados=True):
        '''
        obtener arbol de cargos de donde depende esta oficina
        opcionalmente para al llegar a cargos de una
        categoría específica. Siempre recursivo
        '''
        if self.categoria == hasta_categoria:
            return []

        if solo_publicados and (not self.publicado or not self.categoria.publicado):
            return []

        deps = []
        depende_de = self.depende_de
        while depende_de:
            if (not depende_de.publicado or not depende_de.categoria.publicado) and solo_publicados:
                depende_de = None
            else:
                deps.append(depende_de)
                depende_de = depende_de.depende_de

        return deps

    @property
    def es_cargo_maximo(self):
        """ indica si es EL cargo máximo """
        return self.depende_de is None


    @property
    def es_cargo_secundario(self):
        """ indica si es uno de los cargos justo debajo del intendente """
        return len(self.superiores()) == 1


    @property
    def area_gobierno(self):
        '''
        tipo: Cargo.
        cada oficina depende de algun área de gobierno entendida como la oficina justo debajo
        del responsable principal (intendente por ejemplo). En el caso de Córdoba son secretaríás.
        '''
        superiores = self.superiores()
        if len(superiores) > 1:
            return superiores[-2:-1][0]  # el la última oficina antes de la del intendente
        else:
            return self

    @property
    def visible(self):
        '''
        si cualquiera de sus superiores no está publicado entonces
        este no debe visibilizarse.
        '''
        if not self.publicado:
            return False
        superiores = self.superiores()
        for superior in superiores:
            if not superior.publicado:
                return False

        return True

    @property
    def en_funcion_activa(self):
        '''
        detectar la persona en este cargo activo actualmente
        '''
        return self.funcion_set.filter(activo=True).first()

    @property
    def cargo_gen(self):
        '''
        Devolver el nombre del cargo con el GENERO que corresponde a
        la personas que esta ejerciendo la funciona
        '''
        try:
            funcion = self.funcion_set.get(activo=True, cargo=self)
        except Exception:
            return self.nombre
        return self.nombre_fem if funcion.funcionario.genero == 'F' else self.nombre

    class Meta:
        ordering = ['categoria__orden', 'orden', 'nombre']


class DireccionCargo(Direccion):
    objeto = models.ForeignKey(Cargo)


class ComunicacionCargo(Comunicacion):
    objeto = models.ForeignKey(Cargo)

    class Meta:
        unique_together = (("tipo", "valor", "objeto"),)


class Funcion(models.Model):
    """
    Cada una de las funciones que históricamente desempeña un funcionario
    """
    funcionario = models.ForeignKey(Persona, on_delete=models.CASCADE)
    cargo = models.ForeignKey(Cargo, on_delete=models.CASCADE)
    fecha_inicio = models.DateField(auto_now=False,
                                    auto_now_add=False, null=True, blank=True)
    fecha_fin = models.DateField(auto_now=False,
                                    auto_now_add=False, null=True, blank=True)
    activo = models.BooleanField()  # saber si esta en funciones o no

    # datos del alta del funcionario en el cargo
    decreto_pdf = models.FileField(upload_to='decretos-funcionarios/', null=True, blank=True,
                    verbose_name='Decreto de designación en PDF o JPG')
    decreto_nro = models.CharField(max_length=50, null=True, blank=True,
                    verbose_name='Nro del Decreto de designación')
    # fecha y link a la publicación en Boletin Oficial (o medio oficial de comunicacion)
    boletin_fecha_publicacion = models.DateField(null=True, blank=True,
                    verbose_name='Fecha de publicación en boletín del Decreto de designación')
    boletin_url = models.URLField(null=True, blank=True,
                    verbose_name='URL al Decreto de designación en el Boletín Oficial')

    # datos de la baja del funcionario en el cargo
    decreto_renuncia_pdf = models.FileField(upload_to='decretos-funcionarios/',
                    null=True, blank=True,
                    verbose_name='Decreto de baja o renuncia en PDF o JPG')
    decreto_renuncia_nro = models.CharField(max_length=50, null=True, blank=True,
                    verbose_name='Nro del Decreto de baja o renuncia')
    # fecha y link a la publicación en Boletin Oficial (o medio oficial de comunicacion)
    boletin_renuncia_fecha_publicacion = models.DateField(null=True, blank=True,
                    verbose_name='Fecha de publicación en boletín del Decreto de baja o renuncia')
    boletin_renuncia_url = models.URLField(null=True, blank=True,
                    verbose_name='URL al Decreto de baja o renuncia en el Boletín Oficial')


    is_adhonorem = models.BooleanField(default=False)

    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()

    def __str__(self):
        return '{} - {}'.format(self.funcionario, self.cargo_gen)

    def get_absolute_url(self):
        if self.activo:
            return reverse('website.funcionarios.oficina', kwargs={'oficina': self.cargo.slug, 'oficina_id': self.cargo.id})
        else:
            # FIXME: no es competencia del modelo manejar lógica de HTTP
            raise Http404("Funcionario sin función activa")

    def get_url_produccion(self):
        return '{}://{}{}'.format(settings.PROTOCOLO_PRODUCCION, settings.DOMINIO_PRODUCCION, self.get_absolute_url())

    @property
    def categoria_gen(self):
        '''
        Devolver el nombre de la categoria con el GENERO que corresponde a
        la personas que esta ejerciendo la funciona
        '''
        return self.categoria.nombre_fem if self.funcionario.genero == 'F' else self.categoria.nombre

    @property
    def categoria_corto_gen(self):
        '''
        Devolver el nombre CORTO de la categoria con el GENERO que corresponde a
        la personas que esta ejerciendo la funciona
        '''
        return self.categoria.nombre_corto_fem if self.funcionario.genero == 'F' else self.categoria.nombre_corto

    @property
    def cargo_gen(self):
        '''
        Devolver el nombre del cargo con el GENERO que corresponde a
        la personas que esta ejerciendo la funciona
        '''
        return self.cargo.nombre_fem if self.funcionario.genero == 'F' else self.cargo.nombre

    def get_all_emails(self):
        """ obtener todos los emails de la persona y de su cargo, tiene usos específicos """
        com_persona = ComunicacionPersona.objects.filter(objeto=self.funcionario)
        coms = self.cargo.comunicacioncargo_set.all()
        res = []
        for c in com_persona:
            if c.tipo.nombre.lower() == 'email':  # TODO esta comparación de texto nj corresponde, un campo isEmail en la comunicación podría andar
                res.append(c.valor)
        for c in coms:
            if c.tipo.nombre.lower() == 'email':  # TODO esta comparación de texto nj corresponde, un campo isEmail en la comunicación podría andar
                res.append(c.valor)
        return res

    def comunicaciones(self):
        """ obtener las víás de comuncación de la persona y del cargo """
        com_cargo = self.cargo.comunicacioncargo_set.filter(privado=False)
        com_persona = self.funcionario.comunicacionpersona_set.filter(privado=False)

        res = {'persona': com_persona, 'cargo': com_cargo}

        return res

    class Meta:
        verbose_name_plural = "Funciones"
        permissions = (
                ('decreto_funcion', 'Puede Decreto, fecha y publicación'),
                ('can_view', 'Puede ver'),
                )


class Sueldo(models.Model):
    '''
    Sueldo para una categoria específica en un período de tiempo.
    Es un sueldo de ordenanza, no el real de bolsillo
    '''
    cargo_categoria = models.ForeignKey(CargoCategoria, on_delete=models.PROTECT)
    fecha_inicio = models.DateField(auto_now=False,
                                    auto_now_add=False, null=True, blank=True)
    fecha_fin = models.DateField(auto_now=False,
                                    auto_now_add=False, null=True, blank=True)
    activo = models.BooleanField()  # saber si esta en funciones o no
    sueldo_bruto = models.DecimalField(default=0.00, decimal_places=2,
                                        max_digits=12)
    sueldo_bruto_moneda = models.ForeignKey(Moneda, on_delete=models.CASCADE,
                                            related_name='Moneda_Bruto')
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} = {} {}'.format(self.cargo_categoria,
                                        self.sueldo_bruto_moneda,
                                        self.sueldo_bruto)


class SueldoReal(models.Model):
    '''
    Sueldo real (bruto y neto) cobrado un mes específico por una persona en un cargo
    '''
    funcion = models.ForeignKey(Funcion, on_delete=models.PROTECT)
    mes = models.DateField(auto_now=False, auto_now_add=False)
    sueldo_bruto = models.DecimalField(default=0.00, decimal_places=2,
                                        max_digits=12)
    sueldo_bruto_moneda = models.ForeignKey(Moneda, on_delete=models.CASCADE,
                                            related_name='Moneda_Bruto_Real')
    sueldo_neto = models.DecimalField(default=0.00, decimal_places=2,
                                        max_digits=12)
    sueldo_neto_moneda = models.ForeignKey(Moneda, default=1, on_delete=models.CASCADE,
                                            related_name='Moneda_Neto_Real')
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)


class WebFuncionarios(models.Model):
    """
    Información para la web pública. Será complementada con los cargos en orden de prioridad
    Debe definirse a partir de que cargo se desea describir. Un gobierno podría querer mostrar
    más de un organigrama.
    """
    # definir desde que cargo se muestra este organigrama
    cargo = models.ForeignKey(Cargo, on_delete=models.PROTECT, null=True)
    # hasta que categoria se muestra. Es posible que haya cargada más información
    # en la base de datos de la que debe ser pública (cargos políticos
    # solamente por ejemplo)
    hasta_categoria = models.ForeignKey(CargoCategoria, on_delete=models.PROTECT, null=True)
    titulo = models.CharField(max_length=120, default='Mapa de la función pública municipal')
    slug = models.SlugField(max_length=120)
    sub_titulo = models.CharField(max_length=120, default='Organigrama de funcionarios')
    html = RichTextField(null=True, blank=True)  # texto explicativo abierto
    publicado = models.BooleanField(default=False)
    imagen = models.ImageField(upload_to='imagenes/organigrama', null=True, blank=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        super(WebFuncionarios, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Organigramas"
        unique_together = (('slug'),)
