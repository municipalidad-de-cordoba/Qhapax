from django.db import models
from funcionarios.models import Cargo
from django.conf import settings
from django.core.urlresolvers import reverse
import os
import uuid
from core.models import MyPrivateFileSystemStorage
from django.contrib.auth.models import User


class TipoProveedor(models.Model):
    """ Tipos de proveedores segun su condición con el fisco
    Monotributista, autonomos, SA, SRL, etc
    """
    nombre = models.CharField(max_length=90)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Tipos de proveedores"
        verbose_name = "Tipo de proveedor"
        ordering = ['nombre']


class Proveedor(models.Model):
    """ Cada proveedor del gobierno """
    CUIT = models.CharField(max_length=30, unique=True)
    nombre_legal = models.CharField(
        max_length=190,
        verbose_name='Nombre legal',
        help_text="Nombre formal de la empresa o el monotributista/autonomo")
    nombre_interno = models.CharField(
        max_length=190,
        null=True,
        blank=True,
        verbose_name='Nombre interno',
        help_text="Nombre informal, no obligatorio")
    tipo = models.ForeignKey(
        TipoProveedor,
        on_delete=models.SET_NULL,
        null=True,
        blank=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre_legal

    class Meta:
        verbose_name_plural = "Proveedores"
        verbose_name = "Proveedor"
        ordering = ['nombre_legal']


class TipoFactura(models.Model):
    """ Tipos de factura (A, B, C, E, etc) """
    nombre = models.CharField(max_length=90)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Tipos de Facturas"
        verbose_name = "Tipo de factura"
        ordering = ['nombre']


class CategoriaFactura(models.Model):
    """ categoria de las facturas para filtrar por temas facturas """
    nombre = models.CharField(max_length=90)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Categorías de Facturas"
        verbose_name = "Categoría de factura"
        ordering = ['nombre']


class TagFactura(models.Model):
    """ Marcas para categorizar u ordenar facturas """
    nombre = models.CharField(max_length=90)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Tags de Facturas"
        verbose_name = "Tag de factura"
        ordering = ['nombre']


class Factura(models.Model):
    """ Facturas de compras o servicios provistos al gobierno
        Comienzan como ordenes de compra hasta que se ingresan como facturas
        y finalmente se pagan """
    proveedor = models.ForeignKey(Proveedor, on_delete=models.CASCADE)
    numero_factura = models.CharField(
        max_length=30, verbose_name='Numero de factura')
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    monto = models.DecimalField(max_digits=14, decimal_places=2, default=0.0)
    descripcion = models.TextField(
        verbose_name='Descripción',
        help_text="Detalle breve de lo que representa esta factura",
        null=True,
        blank=True)
    observacion_interna = models.TextField(
        help_text="Detalles internos de la factura", null=True, blank=True)
    tipo = models.ForeignKey(
        TipoFactura,
        on_delete=models.SET_NULL,
        null=True,
        blank=True)
    oficina_destinataria = models.ForeignKey(
        Cargo,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='Oficina_Destinataria',
        help_text="Oficina que solicita el producto o servicio")
    oficina_autoriza = models.ForeignKey(
        Cargo,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='Oficina_Autoriza',
        help_text="Oficina que autoriza")
    categoria = models.ForeignKey(
        CategoriaFactura,
        on_delete=models.SET_NULL,
        null=True,
        blank=True)
    fecha = models.DateField(verbose_name="Fecha de emisión")
    fecha_inicio_periodo_facturado = models.DateField(null=True, blank=True)
    fecha_fin_periodo_facturado = models.DateField(null=True, blank=True)

    es_redeterminacion_de_precios = models.NullBooleanField(
        null=True, default=None)

    tags = models.ManyToManyField(TagFactura, blank=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    documento = models.FileField(
        storage=MyPrivateFileSystemStorage(),
        upload_to='facturas',
        null=True,
        blank=True)

    FC_ORDEN_DE_COMPRA = 10
    FC_INGRESADA = 20
    FC_ANALISIS = 30
    FC_ACEPTADA = 40
    FC_RECHAZADA = 50
    FC_ESPERA_FIRMA = 60
    FC_AUTORIZADA = 70
    FC_EN_HABILITACION = 100
    FC_EN_CONTADURIA = 200
    FC_EN_ECONOMIA = 300
    FC_EN_PRENSA = 400
    FC_EN_LEGAL_Y_TECNICA = 500
    FC_TRIBUNAL_DE_CUENTAS = 600
    FC_ESPERA_PAGO = 700
    FC_PAGADO_PARCIAL = 800
    FC_PAGADA = 900

    estados_factura = ((FC_ORDEN_DE_COMPRA, 'Orden de compra'),
                       (FC_INGRESADA, 'Factura Ingresada'),
                       (FC_ANALISIS, 'Factura en analisis administrativo'),
                       (FC_ACEPTADA, 'Factura Aceptada'),
                       (FC_RECHAZADA, 'Factura Rechazada'),
                       (FC_ESPERA_FIRMA, 'Factura esperando firma autorización'),
                       (FC_AUTORIZADA, 'Autorización factura firmada'),
                       (FC_EN_HABILITACION, 'En oficina de Habilitacion'),
                       (FC_EN_CONTADURIA, 'En oficina de contaduría'),
                       (FC_EN_ECONOMIA, 'En oficina de economía'),
                       (FC_EN_PRENSA, 'En oficina de Prensa'),
                       (FC_EN_LEGAL_Y_TECNICA, 'En oficina de Legal y Tecnica'),
                       (FC_TRIBUNAL_DE_CUENTAS, 'En Tribuna de Cuentas'),
                       (FC_ESPERA_PAGO, 'Factura entregada a oficina de pagos'),
                       (FC_PAGADO_PARCIAL, 'Pagada parcialmente'),
                       (FC_PAGADA, 'Pagada completamente'),)

    estado = models.PositiveIntegerField(choices=estados_factura,
                                         default=FC_ORDEN_DE_COMPRA,
                                         null=True, blank=True)
    numero_expediente = models.CharField(
        max_length=30,
        null=True,
        blank=True,
        help_text='Si hubiera algún código de seguimiento en otro sistema, ucargar aquí.')

    user = models.ForeignKey(User, editable=False, null=True, blank=True)

    def get_documento_url(self):
        return reverse('pagos.factura', kwargs={'factura_uuid': self.uuid})

    def get_documento_path(self):
        return os.path.join(settings.PRIVATE_FILES_ROOT, self.documento.name)

    def get_documento_ext(self):
        path = self.get_documento_path()
        return path.lower().split('.')[-1]

    def get_url_cambios(self):
        return "/admin/pagos/facturacambios/?factura__id__exact={}".format(
            self.id)

    def __str__(self):
        return "{} {} {}".format(
            self.proveedor,
            self.numero_factura,
            self.descripcion)

    class Meta:
        verbose_name_plural = "Facturas"
        verbose_name = "Factura"
        ordering = ['-fecha']
        unique_together = (("proveedor", "numero_factura"),)
        permissions = (('is_pagos_amin', 'Puede ver las facturas de todos'),)


class FacturaCambios(models.Model):
    """ cambios en las facturas para seguimiento """
    factura = models.ForeignKey(Factura, on_delete=models.CASCADE)
    user = models.ForeignKey(User, editable=False)
    fecha = models.DateTimeField(auto_now=True)
    descripcion = models.TextField(blank=True, null=True)
    estado_anterior = models.PositiveIntegerField(
        blank=True, null=True, choices=Factura.estados_factura)
    estado_nuevo = models.PositiveIntegerField(
        blank=True, null=True, choices=Factura.estados_factura)

    class Meta:
        verbose_name_plural = "Cambios en facturas"
        verbose_name = "Cambio en factura"
        ordering = ['-id']


class ModosPago(models.Model):
    """ Modos de pago. Efectivo, transferencia, cheque, etc """
    nombre = models.CharField(max_length=90)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Modos de pagos"
        verbose_name = "Modo de pago"
        ordering = ['nombre']


class Pagos(models.Model):
    """ Pagos de cada factura. Puede haber mas de un pago por factura """
    modo = models.ForeignKey(
        ModosPago,
        on_delete=models.SET_NULL,
        null=True,
        blank=True)
    factura = models.ForeignKey(Factura, on_delete=models.CASCADE)
    monto = models.DecimalField(max_digits=14, decimal_places=2, default=0.0)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    descripcion = models.CharField(max_length=190,
                                   verbose_name='Descripción',
                                   help_text="Anotacion breve",
                                   null=True, blank=True)
    observacion_interna = models.TextField(
        help_text="Detalles internos del pago", null=True, blank=True)
    fecha = models.DateField()
    ultima_modificacion = models.DateTimeField(auto_now=True)
    comprobante = models.FileField(
        storage=MyPrivateFileSystemStorage(),
        upload_to='comprobante_pago',
        null=True,
        blank=True)

    def __str__(self):
        return "{}. {} de {}".format(
            self.factura, self.monto, self.factura.monto)

    def get_comprobante_url(self):
        return reverse('pagos.pago', kwargs={'pago_uuid': self.uuid})

    def get_comprobante_path(self):
        return os.path.join(settings.PRIVATE_FILES_ROOT, self.comprobante.name)

    def get_comprobante_ext(self):
        path = self.get_comprobante_path()
        return path.lower().split('.')[-1]

    class Meta:
        verbose_name_plural = "Pagos"
        verbose_name = "Pago"
        ordering = ['-fecha']
