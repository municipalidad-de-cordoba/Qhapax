from django.contrib import admin
from .models import *
from import_export import resources
from import_export.admin import ImportExportModelAdmin


class ProveedorAdmin(admin.ModelAdmin):
    list_display = (
        'CUIT',
        'nombre_legal',
        'nombre_interno',
        'tipo',
        'ultima_modificacion')
    search_fields = ['CUIT', 'nombre_legal', 'nombre_interno']
    list_filter = ['tipo']


# -----------------------------------------
# -----------------------------------------
# -----------------------------------------
# funcionaes masivas sobre facturas

def cambiar_estado(request, queryset, ESTADO_NUEVO):
    """ funcion general de cambio de estado """
    estado_nuevo = dict(Factura.estados_factura)[ESTADO_NUEVO]
    for q in queryset:
        estado_anterior = q.get_estado_display()
        descr = 'Cambia del estado {} al estado {}'.format(
            estado_anterior, estado_nuevo)
        f = FacturaCambios(
            factura=q,
            user=request.user,
            descripcion=descr,
            estado_anterior=q.estado,
            estado_nuevo=ESTADO_NUEVO)
        f.save()
    queryset.update(estado=ESTADO_NUEVO)


def marcar_como_orden_prefactura(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_ORDEN_DE_COMPRA)


marcar_como_orden_prefactura.short_description = "10 Marcar como orden de compra (previo a factura)"


def marcar_factura_ingresada(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_INGRESADA)


marcar_factura_ingresada.short_description = "20 Marcar las facturas como ingresada"


def marcar_factura_analisis(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_ANALISIS)


marcar_factura_analisis.short_description = "30 Marcar las facturas como en analisis"


def marcar_factura_aceptada(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_ACEPTADA)


marcar_factura_aceptada.short_description = "40 Marcar las facturas como aceptada para pagar"


def marcar_factura_rechazada(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_RECHAZADA)


marcar_factura_rechazada.short_description = "50 Marcar las facturas como rechazada"


def marcar_factura_a_firmar(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_ESPERA_FIRMA)


marcar_factura_a_firmar.short_description = "60 Marcar las facturas como esperando firma autoridad"


def marcar_factura_autorizada(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_AUTORIZADA)


marcar_factura_autorizada.short_description = "70 Marcar las facturas como autorizada y firmada"


def marcar_factura_habilitacion(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_EN_HABILITACION)


marcar_factura_habilitacion.short_description = "100 Factura en Habilitación"


def marcar_factura_contaduria(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_EN_CONTADURIA)


marcar_factura_contaduria.short_description = "200 Factura en  Contaduría"


def marcar_factura_economia(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_EN_ECONOMIA)


marcar_factura_economia.short_description = "300 Factura en Economía"


def marcar_factura_prensa(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_EN_PRENSA)


marcar_factura_prensa.short_description = "400 Factura en Prensa"


def marcar_factura_lyt(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_EN_LEGAL_Y_TECNICA)


marcar_factura_lyt.short_description = "500 Factura en Oficina de Legal y Técnica"


def marcar_factura_tribcuentas(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_TRIBUNAL_DE_CUENTAS)


marcar_factura_tribcuentas.short_description = "600 Factura en el Tribunal de Cuentas"


def marcar_factura_espera_pago(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_ESPERA_PAGO)


marcar_factura_espera_pago.short_description = "700 Marcar las facturas como enviadas a la oficina de pagos"


def marcar_factura_pagada_parcial(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_PAGADO_PARCIAL)


marcar_factura_pagada_parcial.short_description = "800 Marcar las facturas como pagadas parcialmente"


def marcar_factura_pagada(modeladmin, request, queryset):
    cambiar_estado(request, queryset, Factura.FC_PAGADA)


marcar_factura_pagada.short_description = "900 Marcar las facturas como pagadas completamente"


# -----------------------------------------
# -----------------------------------------
# -----------------------------------------
class FacturaResource(resources.ModelResource):
    # proveedor = fields.Field(column_name='proveedor__nombre_legal')

    class Meta:
        model = Factura
        fields = (
            'proveedor__CUIT',
            'proveedor__nombre_legal',
            'proveedor__nombre_interno',
            'numero_factura',
            'monto',
            'descripcion',
            'tipo__nombre',
            'categoria__nombre',
            'fecha',
            'estado',
            'numero_expediente')
        # export_order = FAILS (?) ('-id', )
        widgets = {
            'fecha': {'format': '%d/%m/%Y'},
        }


class FacturaAdmin(ImportExportModelAdmin):
    resource_class = FacturaResource

    def get_queryset(self, request):
        ''' permitir que cada usuario vea solo sus facturas '''
        qs = super(FacturaAdmin, self).get_queryset(request)
        if request.user.is_superuser or request.user.has_perm(
                'pagos.is_pagos_amin'):
            return qs
        return qs.filter(user=request.user)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super(FacturaAdmin, self).save_model(request, obj, form, change)

    def documento_url(self, instance):
        return '<a target="_blank" href="{}">Ver</a>'.format(
            instance.get_documento_url())
    documento_url.short_description = 'documento url'
    documento_url.allow_tags = True

    def registro_de_cambios(self, instance):
        return '<a target="_blank" href="{}">Ver cambios</a>'.format(
            instance.get_url_cambios())
    registro_de_cambios.allow_tags = True

    exclude = ['estado']
    list_display = (
        'proveedor',
        'numero_factura',
        'monto',
        'estado',
        'descripcion',
        'tipo',
        'categoria',
        'oficina_destinataria',
        'fecha',
        'documento_url',
        'registro_de_cambios')
    search_fields = [
        'proveedor__nombre_legal',
        'proveedor__nombre_interno',
        'numero_factura',
        'descripcion']
    list_filter = ['tipo', 'categoria', 'proveedor', 'oficina_destinataria']
    actions = [
        marcar_como_orden_prefactura,
        marcar_factura_ingresada,
        marcar_factura_analisis,
        marcar_factura_aceptada,
        marcar_factura_rechazada,
        marcar_factura_a_firmar,
        marcar_factura_autorizada,
        marcar_factura_habilitacion,
        marcar_factura_contaduria,
        marcar_factura_economia,
        marcar_factura_prensa,
        marcar_factura_lyt,
        marcar_factura_tribcuentas,
        marcar_factura_espera_pago,
        marcar_factura_pagada_parcial,
        marcar_factura_pagada]


class PagosAdmin(admin.ModelAdmin):
    def comprobante_url(self, instance):
        return '<a target="_blank" href="{}">Ver</a>'.format(
            instance.get_comprobante_url())
    comprobante_url.short_description = 'comprobante url'
    comprobante_url.allow_tags = True
    list_display = (
        'modo',
        'factura',
        'monto',
        'descripcion',
        'fecha',
        'comprobante_url')
    search_fields = ['factura', 'descripcion']
    list_filter = ['modo']


class FacturaCambiosAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'factura',
        'fecha',
        'estado_anterior',
        'estado_nuevo',
        'descripcion')
    search_fields = ['factura', 'descripcion']
    list_filter = ['user', 'factura', 'estado_anterior', 'estado_nuevo']


admin.site.register(TipoProveedor)
admin.site.register(Proveedor, ProveedorAdmin)
admin.site.register(TipoFactura)
admin.site.register(CategoriaFactura)
admin.site.register(TagFactura)
admin.site.register(Factura, FacturaAdmin)
admin.site.register(FacturaCambios, FacturaCambiosAdmin)
admin.site.register(ModosPago)
admin.site.register(Pagos, PagosAdmin)
