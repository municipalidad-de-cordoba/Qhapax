# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-06-30 13:05
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pagos', '0013_auto_20170613_1258'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='factura',
            options={'ordering': ['-fecha'], 'permissions': (('is_pagos_amin', 'Puede ver las facturas de todos'),), 'verbose_name': 'Factura', 'verbose_name_plural': 'Facturas'},
        ),
    ]
