# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2016-12-23 13:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pagos', '0010_auto_20161223_0949'),
    ]

    operations = [
        migrations.AddField(
            model_name='facturacambios',
            name='estado_anterior',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='facturacambios',
            name='estado_nuevo',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
