# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2016-12-20 12:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pagos', '0008_auto_20161207_1622'),
    ]

    operations = [
        migrations.AlterField(
            model_name='factura',
            name='estado',
            field=models.PositiveIntegerField(blank=True, choices=[(10, 'Orden de compra'), (20, 'Factura Ingresada'), (30, 'Factura en analisis administrativo'), (40, 'Factura Aceptada'), (50, 'Factura Rechazada'), (60, 'Factura esperando firma autorización'), (70, 'Autorización factura firmada'), (80, 'Factura entregada a oficina de pagos'), (90, 'Pagada parcialmente'), (100, 'Pagada completamente')], default=10, null=True),
        ),
    ]
