from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.decorators import permission_required
from .models import Factura, Pagos


@permission_required('pagos.change_factura')
def ver_factura(request, factura_uuid):
    """ ver el documento de la factura """
    factura = get_object_or_404(Factura, uuid=factura_uuid)

    path = factura.get_documento_path()
    ext = factura.get_documento_ext()
    f = open(path, 'rb').read()

    if ext == 'pdf':
        response = HttpResponse(f, content_type='application/pdf')
    elif ext in ['png', 'jpg', 'jpeg', 'tiff']:
        response = HttpResponse(f, content_type='image/png')
    else:
        response = HttpResponse(f, content_type='application/octet-stream')
        response['Content-Disposition'] = 'attachment; filename="factura-{}.{}"'.format(
            factura_uuid, ext)
    return response


@permission_required('pagos.change_pagos')
def ver_pago(request, pago_uuid):
    """ ver un comprobante de pago """
    pago = get_object_or_404(Pagos, uuid=pago_uuid)

    path = pago.get_comprobante_path()
    ext = pago.get_comprobante_ext()
    f = open(path, 'rb').read()

    if ext == 'pdf':
        response = HttpResponse(f, content_type='application/pdf')
    elif ext in ['png', 'jpg', 'jpeg', 'tiff']:
        response = HttpResponse(f, content_type='image/png')
    else:
        response = HttpResponse(f, content_type='application/octet-stream')
        response['Content-Disposition'] = 'attachment; filename="factura-{}.{}"'.format(
            pago_uuid, ext)
    return response
