from django.conf.urls import url
from . import views


urlpatterns = [url(r'^factura/(?P<factura_uuid>[\w-]+)$',
                   views.ver_factura,
                   name='pagos.factura',
                   ),
               url(r'^pago/(?P<pago_uuid>[\w-]+)$',
                   views.ver_pago,
                   name='pagos.pago',
                   ),
               ]
