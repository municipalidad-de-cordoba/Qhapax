from django.core.management.base import BaseCommand
from django.utils import timezone
from django.db import transaction
from datetime import datetime
import json
import sys
from basurales.settings import *
from basurales.models import *
from barrios.models import Barrio
from CPC.models import Cpc
from django.contrib.gis.geos import Point
import requests
from django.core.files import File
import tempfile
from PIL import Image


class Command(BaseCommand):
    help = """Comando para importar trabajos hechos en basurales"""

    def add_arguments(self, parser):
        parser.add_argument('--desde',
                            type=str,
                            help=('fecha desde la cual se importarán nuevas'
                                  ' obras y cambios a las existentes. '
                                  'Formato dd/MM/aaaa.'),
                            default='01/01/2017')
        parser.add_argument('--hasta',
                            type=str,
                            help=('fecha hasta la cual se importarán nuevas'
                                  ' obras y cambios a las existentes. '
                                  'Formato dd/MM/aaaa.'),
                            default=datetime.now().
                                    strftime('%d/%M/%Y'))

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación de datos '
                                             'de trabajos sobre basurales.'))

        url_base = URL_API_BASURALES
        desde = options['desde']
        hasta = options['hasta']
        url = '{}?fechaDesde={}&fechaHasta={}'.format(url_base, desde, hasta)

        self.stdout.write(self.style.SUCCESS(
            'Buscando datos en {}'.format(url)))

        session = requests.Session()
        headers = {'Authorization': TOKEN_ACCESO}
        basedata = session.get(url, headers=headers).content  # es tipo byte
        basedata = basedata.decode("utf-8")
        # basedata = basedata.decode("iso-8859-1")

        poligonos_barrios = Barrio.objects.values_list('poligono', flat=True)

        poligonos_cpcs = Cpc.objects.values_list('zona', flat=True)

        try:
            jsondata = json.loads(basedata)
        except Exception as e:
            self.stdout.write(self.style.ERROR('JSON Error: {}'.format(url)))
            self.stdout.write(self.style.ERROR(e))
            sys.exit(1)

        obras = jsondata['result']
        self.stdout.write(self.style.SUCCESS(
            'Se obtuvieron datos de {} obras'.format(len(obras))))

        organizaciones_nuevas = 0
        organizaciones_repetidas = 0
        incidentes_nuevos = 0
        incidentes_repetidos = 0
        lugar_nuevo = 0
        lugar_repetido = 0
        errores = []

        for obra in obras:
            self.stdout.write(self.style.SUCCESS('OBRA: {}'.format(obra)))
            """
            respuesta tipo JSON:
            {
              status: "ok",
              result: [
                {
                  fecha: "01/01/2019",
                  usuario: "usuario",
                  observaciones: "observaciones",
                  areaServicioDescripcion: "areaServicioDescripcion",
                  tipoIncidenteDescripcion: "tipoIncidenteDescripcion",
                  lugar: "lugar",
                  metrosCubicos: "metrosCubicos",
                  coordenadas: "coordenadas",
                  adjuntos: [],
                  numeroAuditoria: 809
                }
              ]
              exception: "",
              exceptionMessage: "",
            }
            """
            coords = obra['coordenadas'].split(';')
            longitud = coords[1]
            latitud = coords[0]

            point = Point(float(longitud), float(latitud))
            ubicacion, created = UbicacionBasural.objects.get_or_create(ubicacion=point)
            ubicacion.lugar = obra['lugar']

            if created:
                lugar_nuevo += 1
            else:
                lugar_repetido += 1

            barrio = None
            for poligono in poligonos_barrios:
                if poligono is not None and poligono.contains(point):
                    barrio = Barrio.objects.get(poligono=poligono)
                    break

            # detectamos cpc al cual pertenece
            cpc = None
            for poligono in poligonos_cpcs:
                if poligono is not None and poligono.contains(point):
                    cpc = Cpc.objects.get(zona=poligono)
                    break

            trabajo, t_created = TrabajoBasural.objects.get_or_create(
                numero_auditoria=obra['numeroAuditoria'])
            trabajo.lugar = ubicacion

            if t_created:
                incidentes_nuevos += 1
            else:
                incidentes_repetidos += 1

            ubicacion.barrio = barrio
            ubicacion.cpc = cpc
            ubicacion.save()

            # tipo de trabajo hecho
            tipo, t_created = TipoTrabajoEnBasural.objects.get_or_create(descripcion=obra['tipoIncidenteDescripcion'])
            trabajo.tipo = tipo
            trabajo.metros_cubicos = obra['metrosCubicos']

            #fotos de los trabajos
            adjuntos = obra['adjuntos']
            cantidad_de_fotos = len(adjuntos) if adjuntos is not None else 0
            self.stdout.write(self.style.SUCCESS('Fotos: {}'.format(cantidad_de_fotos)))

            if cantidad_de_fotos > 0:
                for adjunto in adjuntos:
                    foto, created = FotoDeBasural.objects.get_or_create(
                        trabajo=trabajo, url=adjunto)

                    #importo las fotos solo si es la primera vez
                    if created:
                        self.stdout.write(self.style.SUCCESS('Nueva Foto: {} Trayendo imagen {}'.format(adjunto, foto.url)))
                        url_foto = adjunto
                        contenido = session.get(url_foto).content # es tipo byte

                        tf = tempfile.NamedTemporaryFile()
                        tf.write(contenido)

                        img = File(tf)
                        try:
                            #controlamos que el archivo sea una imagen válida
                            # print(img)
                            # sys.exit(1)
                            img = Image.open(img)
                            if img.size != 0:
                                foto.foto.save(str(foto.url.split('/')[-1]), File(tf))
                        except Exception as e:
                            err = 'ERROR con el formato de la FOTO: {}'.format(foto.url)
                            self.stdout.write(self.style.ERROR(err))
                            errores.append(err)
                            foto.delete()
                            continue

                            foto.save()
                    else:
                        self.stdout.write(self.style.SUCCESS('Omitiendo descargar imagen {}'.format(foto.url)))

            # # current_tz = timezone.get_current_timezone()
            trabajo.fecha = datetime.strptime(
                            obra['fecha'].strip(), "%d/%m/%Y %H:%M")
            # incidente.fecha = current_tz.localize(incidente.fecha_actualizacion)


            # Datos Contratista
            organizacion_str = obra['usuario'].strip()
            if organizacion_str == '':
                organizacion_str = 'Desconocido'
            organizacion = None
            try:
                organizacion, created = Organizacion.objects.get_or_create(
                    nombre=organizacion_str)
                if created:
                    organizaciones_nuevas += 1
                else:
                    organizaciones_repetidas += 1
            except Organizacion.MultipleObjectsReturned:
                #Fix para cuando la organizacion está duplicada
                organizaciones = Organizacion.objects.filter(nombre=organizacion_str)

                max_obras_relacionadas = 0
                # por cada organización, obtengo las obras que tiene a su cargo
                for org in organizaciones:
                    cant = len(org.obrapublica_set.all())
                    if cant > max_obras_relacionadas:
                        organizacion = org
                        max_obras_relacionadas = cant
                #elijo la organizacion que tiene mas obras
                organizaciones_repetidas += 1

            trabajo.organizacion = organizacion

            area_servicio, area_creada = AreaServicio.objects.get_or_create(descripcion=obra['areaServicioDescripcion'].strip())
            trabajo.area_servicio = area_servicio

            trabajo.observaciones = obra['observaciones']

            trabajo.save()

        self.stdout.write(self.style.SUCCESS('FIN.'))
        self.stdout.write(self.style.SUCCESS('Lugares nuevos: {}.'.format(lugar_nuevo)))
        self.stdout.write(self.style.SUCCESS('Lugares repetidos: {}.'.format(lugar_repetido)))
        self.stdout.write(self.style.SUCCESS('Trabajos nuevos: {}.'.format(incidentes_nuevos)))
        self.stdout.write(self.style.SUCCESS('Trabajos repetidos: {}.'.format(incidentes_repetidos)))
        self.stdout.write(self.style.SUCCESS('Organizaciones nuevas: {}.'.format(organizaciones_nuevas)))
        self.stdout.write(self.style.SUCCESS('Organizaciones repetidas: {}.'.format(organizaciones_repetidas)))

        if len(errores)>0:
            self.stdout.write(self.style.ERROR('ERRORES:'))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))
