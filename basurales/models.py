from django.db import models
from django.contrib.gis.db import models as models_geo
from barrios.models import Barrio
from CPC.models import Cpc
from organizaciones.models import Organizacion
from versatileimagefield.fields import VersatileImageField, PPOIField


class TipoTrabajoEnBasural(models.Model):
    """
    Tipo de trabajo realizado
    """
    descripcion = models.CharField(max_length=90, unique=True)

    def __str__(self):
        return self.descripcion


class AreaServicio(models.Model):
    """
    Area de servicio del trabajo realizado
    """
    descripcion = models.CharField(max_length=90, unique=True)

    def __str__(self):
        return self.descripcion


class UbicacionBasural(models.Model):
    """
    Ubicacion del lugar
    """
    ubicacion = models_geo.PointField(null=True, blank=True)
    lugar = models.CharField(max_length=90, blank=True, null=True)
    barrio = models.ForeignKey(Barrio, blank=True, null=True)
    cpc = models.ForeignKey(Cpc, blank=True, null=True)

    def __str__(self):
        return 'Id: {} - lugar: {}'.format(self.id, self.lugar)


class TrabajoBasural(models.Model):
    """
    Reporte de trabajo hecho en basurales de la ciudad.
    """
    numero_auditoria = models.PositiveIntegerField(unique=True)
    lugar = models.ForeignKey('UbicacionBasural', null=True, on_delete=models.SET_NULL)
    metros_cubicos = models.PositiveIntegerField(default=0)
    tipo = models.ForeignKey(TipoTrabajoEnBasural, null=True, on_delete=models.SET_NULL)
    observaciones = models.TextField(null=True, blank=True)
    fecha = models.DateTimeField(null=True, blank=True)
    organizacion = models.ForeignKey(Organizacion, null=True, blank=True)
    area_servicio = models.ForeignKey(AreaServicio, null=True, blank=True)
    publicado = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Trabajo en basural'
        verbose_name_plural = 'Trabajos en basurales'

    def __str__(self):
        return 'Id: {}'.format(self.id)


class FotoDeBasural(models.Model):
    """
    Fotos de trabajos en basurales.
    """

    # Por ahora sólo tenemos una foto por trabajo, pero el modelo está pensado
    # para tener cualquier cantidad de fotos.
    trabajo = models.ForeignKey(TrabajoBasural, related_name='adjuntos')
    url = models.CharField(max_length=320, null=True, blank=True)

    foto = VersatileImageField(
        upload_to='imagenes/alumbrado',
        ppoi_field='foto_ppoi',
        null=True,
        blank=True,
        max_length=200
    )
    foto_ppoi = PPOIField('Image PPOI')

    def __str__(self):
        return "Foto {}".format(self.id)

    class Meta:
        verbose_name = 'Foto de trabajo en un basural'
        verbose_name_plural = 'Fotos de trabajos en basurales'
