from django.contrib import admin
from .models import *
from core.admin import QhapaxOSMGeoAdmin


class FotoDeBasuralInLine(admin.StackedInline):
    model = FotoDeBasural
    extra = 1


class TrabajoBasuralAdmin(admin.ModelAdmin):
    list_display = ['id', 'numero_auditoria', 'tipo', 'lugar','fecha']
    search_fields = ['numero_auditoria', 'id', 'observaciones']
    list_filter = ['tipo', 'organizacion', 'lugar__barrio', 'lugar__cpc']
    inlines = [FotoDeBasuralInLine]

    def get_queryset(self, request):
        queryset = TrabajoBasural.objects.all().order_by('-id')
        return queryset


class UbicacionBasuralAdmin(QhapaxOSMGeoAdmin):
    list_display = ['id', 'lugar', 'barrio', 'cpc']
    search_fields = ['id', ]
    list_filter = ['barrio', 'cpc']


class FotoDeBasuralAdmin(admin.ModelAdmin):

    def file_size(self, obj):
        if obj.foto and hasattr(obj.foto, 'url'):
            sz = obj.foto.file.size / (1024 * 1024)
            return '{:.2f} MB'.format(sz)
        else:
            return "No hay foto"
    # file_size.admin_order_field = 'foto__file__size'

    def foto_thumb(self, obj):
        if obj.foto:
            try:
                th80 = obj.foto.thumbnail['80x80'].url
            except Exception as e:
                pass
            else:
                return '<img src="{}" style="width: 80px;"/>'.format(obj.foto.thumbnail['80x80'].url)

        return ' <img src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACXBIWXMAAAAnAAAAJwEqCZFPAAAA B3RJTUUH4AcJFDE2B3C7PwAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUH AAAACklEQVQI12P4DwABAQEAG7buVgAAAABJRU5ErkJggg==" alt="blanco " style="width: 80px; height: 120px;" />'

    foto_thumb.short_description = 'thumb'
    foto_thumb.allow_tags = True

    list_display = ['id', 'trabajo', 'foto_thumb',
                    'url', 'file_size']
    search_fields = ['id', 'trabajo', 'url']
    list_filter = ['trabajo__tipo',]


admin.site.register(TrabajoBasural, TrabajoBasuralAdmin)
admin.site.register(FotoDeBasural, FotoDeBasuralAdmin)
admin.site.register(UbicacionBasural, UbicacionBasuralAdmin)
