from django.conf.urls import url, include
from rest_framework import routers
from .views import *


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register('trabajos', TrabajoBasuralViewSet, base_name='basurales.api.trabajos')
router.register('barrios-geo-incidentes', BarriosGeoBasuralViewSet, base_name='basurales.barrios-geo-incidentes')
router.register('barrios-incidentes', BarriosBasuralViewSet, base_name='barrios-incidentes')


urlpatterns = [
    url(r'^', include(router.urls)),
]
