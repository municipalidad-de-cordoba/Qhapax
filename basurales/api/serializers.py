from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from rest_framework_gis.serializers import (GeoFeatureModelSerializer,
                                            GeometrySerializerMethodField)
from versatileimagefield.serializers import VersatileImageFieldSerializer
from basurales.models import TrabajoBasural, FotoDeBasural


class FotoDeBasuralSerializer(CachedSerializerMixin):
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                        ("thumbnail_32x32", 'thumbnail__32x32'),
                                        ("thumbnail_125", 'thumbnail__125x125'),
                                        ("thumbnail_500", 'thumbnail__500x500')])

    class Meta:
        model = FotoDeBasural
        fields = ('id', 'foto', 'url')


class TrabajoBasuralSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):

    # adjuntos = FotoDeBasuralSerializer(many=True, read_only=True)
    barrio = serializers.SerializerMethodField()
    id_barrio = serializers.SerializerMethodField()
    tipo = serializers.SerializerMethodField()
    empresa = serializers.SerializerMethodField()
    ubicacion = GeometrySerializerMethodField()
    fecha = serializers.DateTimeField(format="%d-%m-%Y")
    cpc = serializers.SerializerMethodField()
    id_cpc = serializers.SerializerMethodField()

    def get_cpc(self, obj):
        return 'Desconocido' if obj.lugar.cpc is None else obj.lugar.cpc.nombre

    def get_ubicacion(self, obj):
        return obj.lugar.ubicacion

    def get_empresa(self, obj):
        return 'Desconocida' if obj.organizacion is None else obj.organizacion.nombre

    def get_barrio(self, obj):
        return 'Desconocido' if obj.lugar.barrio is None else obj.lugar.barrio.nombre

    def get_id_barrio(self, obj):
        return None if obj.lugar.barrio is None else obj.lugar.barrio.id

    def get_tipo(self, obj):
        return 'Desconocido' if obj.tipo is None else obj.tipo.descripcion

    def get_id_cpc(self, obj):
        return None if obj.lugar.cpc is None else obj.lugar.cpc.id

    class Meta:
        model = TrabajoBasural
        geo_field = 'ubicacion'
        fields = ('id', 'metros_cubicos', 'tipo', 'barrio', 'id_barrio',
                  'observaciones', 'fecha', 'area_servicio', 'empresa',
                  'cpc', 'id_cpc'
                 )


# Registro los serializadores en la cache de DRF
cache_registry.register(TrabajoBasuralSerializer)
cache_registry.register(FotoDeBasuralSerializer)
