
DATABASE_NAME="qhapax"
DATABASE_USER="qhapax"
DATABASE_PASS="qhapax"
ENV_NAME="qhapax_env"

echo "Instalando Python 3 y pip3"
sudo apt-get install python3 python3-pip libmysqlclient-dev python-dev libjpeg-dev libfreetype6-dev zlib1g-dev elasticsearch git
echo "Instalando VirtualEnv"
sudo pip3 install virtualenv
echo "Creando entorno"
virtualenv-3.3 $ENV_NAME
echo "Activando entorno"
source $ENV_NAME/bin/activate 
echo "Clonando repositorio"
git clone https://github.com/avdata99/Qhapax
cd Qhapax
echo "Instalando requerimientos"
pip install -r requirements.txt

echo "Creando base de datos"
CREATE_DB="CREATE DATABASE $DATABASE_NAME DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;"
echo $CREATE_DB
mysql -u root -p -e "$CREATE_DB"

echo "Privilegios en la base"
GRANT="GRANT ALL PRIVILEGES ON $DATABASE_NAME.* TO '$DATABASE_USER'@'localhost' IDENTIFIED BY '$DATABASE_PASS';"
echo $GRANT
mysql -u root -p -e "$GRANT"

printf "[client]\ndatabase = $DATABASE_NAME \nuser = $DATABASE_USER \npassword = $DATABASE_PASS \ndefault-character-set = utf8 \nhost = localhost" > qhapax/database.cfg
echo "Migrando base de datos"
python manage.py migrate



