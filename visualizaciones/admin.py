from django.contrib import admin
from .models import ScreenShotViz, Visualizacion
from .forms import VisualizacionForm


class VisualizacionAdmin(admin.ModelAdmin):
    form = VisualizacionForm
    list_display = ('titulo', 'publicado', 'activado', 'url')
    search_fields = ('titulo', 'url')
    list_filter = ['publicado', 'activado']

    # sin esto no funciona el JS de select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


class ScreenShotVizAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'imagen')


admin.site.register(ScreenShotViz, ScreenShotVizAdmin)
admin.site.register(Visualizacion, VisualizacionAdmin)
