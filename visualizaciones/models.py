'''
Visualziaciones basadas en datos de la municipalidad para
la sección específica del portal de Gobierno Abierto
'''
from django.db import models
from portaldedatos.models import Recurso
from versatileimagefield.fields import VersatileImageField


class ScreenShotViz(models.Model):
    '''
    Pantallazaos de la visualización funcionando
    '''
    titulo = models.CharField(max_length=250, null=True, blank=True)
    imagen = VersatileImageField(
        upload_to='imagenes/visualizaciones-screen',
        null=True,
        blank=True)

    def __str__(self):
        return "{} {}".format(self.titulo, self.imagen)


class Visualizacion(models.Model):
    '''
    Cada visualzaicion de un dato de nuestro portal
    '''
    titulo = models.CharField(max_length=250)
    descripcion = models.TextField(null=True, blank=True)
    logo = VersatileImageField(
        upload_to='imagenes/software-logos',
        null=True,
        blank=True)
    imagen = VersatileImageField(
        upload_to='imagenes/software-img',
        null=True,
        blank=True)
    # se puede mostrar en listas al público
    publicado = models.BooleanField(default=False)
    # casos donde queremos desactivar pero seguir mostrando algun producto
    activado = models.BooleanField(default=False)
    interna = models.BooleanField(default=False)
    url = models.URLField(null=True, blank=True)
    screen_shots = models.ManyToManyField(ScreenShotViz, blank=True)
    recurso = models.ForeignKey(Recurso)
    orden = models.PositiveIntegerField(default=100)
    # código para embeber en tu web
    embed_code = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.titulo

    class Meta:
        ordering = ['orden']
