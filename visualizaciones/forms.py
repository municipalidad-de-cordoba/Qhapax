from portaldedatos.forms import RecursoWidgetSelect
from .models import Visualizacion
from django import forms


class VisualizacionForm(forms.ModelForm):

    class Meta:
        model = Visualizacion
        fields = '__all__'
        widgets = {'recurso': RecursoWidgetSelect}
