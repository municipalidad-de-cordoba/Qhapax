from django.shortcuts import render
from django.views.decorators.cache import cache_page
from visualizaciones.models import Visualizacion


@cache_page(60 * 60)  # 1 h
def ver_visualizaciones(request):
    '''
    mostrar la lista de apps moviles publicadas
    '''
    visualizaciones = Visualizacion.objects.filter(publicado=True)

    context = {'visualizaciones': visualizaciones}
    url = "website/{}/visualizaciones.html".format(request.website.template)
    return render(request, url, context)
