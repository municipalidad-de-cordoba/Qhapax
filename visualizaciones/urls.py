from django.conf.urls import url
from . import views
from django.views.generic.base import RedirectView


urlpatterns = [
    url(r'^$', RedirectView.as_view(url='lista/'), name='seccion-apps'),
    url(r'^lista/$', views.ver_visualizaciones, name='visualizaciones.lista',),

]
