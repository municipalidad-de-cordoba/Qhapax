from simple_history.admin import SimpleHistoryAdmin
from django.contrib import admin
from .models import *
from .forms import *


class EquipoDeDesarrolloAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'descripcion')
    search_fields = ('nombre', 'descripcion')


class RepositorioSoftwareAdmin(admin.ModelAdmin):
    list_display = ('software', 'tipo', 'es_software_libre', 'url')
    search_fields = ('tipo', 'es_software_libre', 'url')
    list_filter = ['tipo']


class RepositorioSoftwareInline(admin.StackedInline):
    model = RepositorioSoftware
    extra = 1


class ScreenShotAdmin(admin.ModelAdmin):
    list_display = ('software', 'titulo', 'imagen')


class ScreenShotInline(admin.StackedInline):
    model = ScreenShot
    extra = 1


class SoftwarePublicoAdmin(SimpleHistoryAdmin):
    list_display = ('titulo', 'publicado', 'activado', 'orden', 'mas_info_url')
    search_fields = ('titulo', 'mas_info_url')
    list_filter = ['publicado', 'activado']
    inlines = [RepositorioSoftwareInline, ScreenShotInline]


class AppMovilAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'publicado', 'activado', 'orden', 'mas_info_url')
    search_fields = ('titulo', 'mas_info_url')
    list_filter = ['publicado', 'activado']
    inlines = [RepositorioSoftwareInline, ScreenShotInline]


class TecnologiaSoftwareAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'tipo')


class TecnologiaBaseDeDatosAdmin(admin.ModelAdmin):
    list_display = ('nombre', )


class VersionTecnologiaSoftwareAdmin(admin.ModelAdmin):
    list_display = ('tecnologia', 'nombre_o_numero_version', 'es_obsoleta')
    list_filter = ['tecnologia']


class VersionTecnologiaBaseDeDatosAdmin(admin.ModelAdmin):
    list_display = ('tecnologia', 'nombre_o_numero_version', 'es_obsoleta')
    list_filter = ['tecnologia']


class BaseDeDatosAdmin(SimpleHistoryAdmin):
    list_display = ('nombre', 'version_tecnologia', 'publicado', 'orden')
    search_fields = ('nombre', 'descripcion', 'observaciones_internas')
    list_filter = ['publicado', 'version_tecnologia']


class UsuarioSoftwareAdmin(admin.ModelAdmin):
    list_display = ['software', 'user', 'created', 'modified', 'accesos']
    list_filter = ['software']
    search_fields = ('software__titulo', 'software__descripcion', 'user__username')


class DefaultSettingsSoftwareAdmin(admin.ModelAdmin):
    list_display = ['software', 'nombre', 'valor']
    list_filter = ['software']
    search_fields = ('software__titulo', 'nombre', 'valor')


class GrupoUsuariosSoftwareAdmin(admin.ModelAdmin):
    def cantidad_usuarios(self, obj):
        return obj.cantidad_usuarios()
    list_display = ['software', 'nombre', 'codigo_interno', 'cantidad_usuarios', 'cod_num_1', 'cod_num_2', 'cod_str_1', 'cod_str_2']
    list_filter = ['software', 'codigo_interno']
    search_fields = ('software__titulo', 'nombre')


class UsuarioEnGrupoSoftwareAdmin(admin.ModelAdmin):
    form = UsuarioEnGrupoSoftwareForm
    list_display = ['user', 'grupo', 'ponderador']
    search_fields = ('grupo__software__titulo', 'user__username')

    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


class UsuariosPorDiasAdmin(admin.ModelAdmin):
    list_display = ['app', 'cant_usuarios', 'fecha']


admin.site.register(GrupoUsuariosSoftware, GrupoUsuariosSoftwareAdmin)
admin.site.register(UsuarioEnGrupoSoftware, UsuarioEnGrupoSoftwareAdmin)
admin.site.register(RepositorioSoftware, RepositorioSoftwareAdmin)
admin.site.register(SoftwarePublico, SoftwarePublicoAdmin)
admin.site.register(AppMovil, AppMovilAdmin)
admin.site.register(ScreenShot, ScreenShotAdmin)
admin.site.register(VersionTecnologiaSoftware, VersionTecnologiaSoftwareAdmin)
admin.site.register(VersionTecnologiaBaseDeDatos, VersionTecnologiaBaseDeDatosAdmin)
admin.site.register(TecnologiaSoftware, TecnologiaSoftwareAdmin)
admin.site.register(TecnologiaBaseDeDatos, TecnologiaBaseDeDatosAdmin)
admin.site.register(BaseDeDatos, BaseDeDatosAdmin)
admin.site.register(EquipoDeDesarrollo, EquipoDeDesarrolloAdmin)
admin.site.register(UsuarioSoftware, UsuarioSoftwareAdmin)
admin.site.register(DefaultSettingsSoftware, DefaultSettingsSoftwareAdmin)
admin.site.register(UsuariosPorDias, UsuariosPorDiasAdmin)
