from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^aplicaciones-moviles/$', views.ver_apps_moviles, name='software_publico.apps',),
    url(r'^app/(?P<pk>[0-9]+)$', views.ficha_app_movil, name='software_publico.app',),

]
