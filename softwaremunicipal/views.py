from django.shortcuts import render
from django.views.decorators.cache import cache_page
from softwaremunicipal.models import AppMovil
from django.http import Http404


@cache_page(60 * 60)  # 1 h
def ver_apps_moviles(request):
    '''
    mostrar la lista de apps moviles publicadas
    '''
    apps = AppMovil.objects.filter(activado=True, publicado=True)

    context = {'apps': apps, 'cantidad_apps': apps.count()}
    url = "website/{}/apps-moviles.html".format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def ficha_app_movil(request, pk):
    '''
    Mostrar los datos básicos de una app
    '''
    app = AppMovil.objects.get(pk=pk)
    if not app.publicado:
        raise Http404("Esta app no está publicada aún")
    
    if not app.activado:
        raise Http404("Esta app no está activada aún")

    context = {'app': app}
    url = "website/{}/ficha-app-movil.html".format(request.website.template)
    return render(request, url, context)
