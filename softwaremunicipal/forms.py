from django import forms
from .models import *
from mensajes_a_usuarios.forms import EmisorWidget
from softwaremunicipal.models import UsuarioEnGrupoSoftware


class UsuarioEnGrupoSoftwareForm(forms.ModelForm):

    class Meta:
        model = UsuarioEnGrupoSoftware
        fields = '__all__'
        widgets = {
                    'user': EmisorWidget
                    }
