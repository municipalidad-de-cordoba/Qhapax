# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-10 15:02
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('softwaremunicipal', '0007_auto_20171209_1936'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='softwarepublico',
            name='repositorio',
        ),
        migrations.AddField(
            model_name='repositoriosoftware',
            name='software',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='softwaremunicipal.SoftwarePublico'),
        ),
    ]
