# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-09-26 03:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('softwaremunicipal', '0017_usuariosoftware_created'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuariosoftware',
            name='accesos',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='usuariosoftware',
            name='modified',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
