# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-11-17 14:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('softwaremunicipal', '0022_grupousuariossoftware_usuarioengruposoftware'),
    ]

    operations = [
        migrations.AddField(
            model_name='grupousuariossoftware',
            name='codigo_interno',
            field=models.CharField(blank=True, help_text='Codigo para uso especial en generadores automáticos de grupos para filtrar', max_length=30, null=True),
        ),
        migrations.AddField(
            model_name='usuarioengruposoftware',
            name='ponderador',
            field=models.PositiveIntegerField(default=0, help_text='Opcional, que tanto pertenece el usuario a este grupo'),
        ),
        migrations.AlterField(
            model_name='usuarioengruposoftware',
            name='grupo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='usuarios', to='softwaremunicipal.GrupoUsuariosSoftware'),
        ),
    ]
