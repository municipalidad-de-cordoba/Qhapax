# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-02 13:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('softwaremunicipal', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='softwarepublico',
            name='documentacion_interna_url',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='repositoriosoftware',
            name='tipo',
            field=models.PositiveIntegerField(choices=[(1, 'Desconocido'), (10, 'GitHub'), (20, 'GitLab'), (30, 'BitBucket'), (40, 'SVN'), (50, 'Team Foundation Server'), (60, 'Mercurial')], default=1),
        ),
    ]
