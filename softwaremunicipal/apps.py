from django.apps import AppConfig


class SoftwaremunicipalConfig(AppConfig):
    name = 'softwaremunicipal'
