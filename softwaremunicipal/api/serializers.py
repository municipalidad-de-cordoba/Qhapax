from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from django.core.serializers import serialize
from django.core.urlresolvers import reverse

from softwaremunicipal.models import AppMovil, DefaultSettingsSoftware


class DefaultSettingsSoftwareSerializer(CachedSerializerMixin):
    class Meta:
        model = DefaultSettingsSoftware
        fields = ('nombre', 'valor')


class AppMovilSerializer(CachedSerializerMixin):
    configuraciones = DefaultSettingsSoftwareSerializer(many=True, read_only=True)
    
    class Meta:
        model = AppMovil
        fields = ('id', 'android_market', 'ios_market', 'windows_market', 'web_version',
            'terminos_y_condiciones', 'politica_de_privacidad', 'about_app',
            'version_minima', 'txt_sino_hay_version_minima', 'version_recomendada',
            'txt_sino_hay_version_recomendada', 'configuraciones')
