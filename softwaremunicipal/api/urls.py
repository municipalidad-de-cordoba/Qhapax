from django.conf.urls import url, include
from rest_framework import routers
from .views import *

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register('apps-moviles', AppMovilViewSet, base_name='apps-moviles.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]