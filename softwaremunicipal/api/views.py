from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import *
from api.pagination import DefaultPagination
from softwaremunicipal.models import AppMovil

class AppMovilViewSet(viewsets.ModelViewSet):
    """
    Lista de productos de software que son aplicaciones móviles
    """
    serializer_class = AppMovilSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = AppMovil.objects.filter(publicado=True)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
