from django.db import transaction
from qhapax.celeryconf import app
from .models import UsuariosPorDias, SoftwarePublico, UsuarioSoftware
from datetime import date, timedelta, datetime


"""
NOTA IMPORTANTE: este script esta hardcodeado siempre al dia de ayer y a que se
ejecute en celery despues de las 00:00
"""
@app.task
@transaction.atomic
def grabarusuariospordias():
    # Obtenemos el pk de GO
    app_go = SoftwarePublico.objects.get(pk=settings.APP_GO_PK)
    # TODOS los usuarios de GO
    qsu = UsuarioSoftware.objects.filter(software=app_go)
    # Tomamos siempre el dia de ayer
    ayer = date.today() - timedelta(days=1)
    # Cuento desde las 00:01 del dia de ayer
    ayer_desde = datetime.combine(ayer, datetime.time(0, 1))
    # Hasta las 23:59 del dia de ayer
    ayer_hasta = datetime.combine(ayer, datetime.time(23, 59))
    # Filtramos los usuarios que usaron GO solo en el dia de ayer
    qsu_ayer = qsu.filter(modified__range=[ayer_desde, ayer_hasta])
    # Si bien no los tenemos en UTC-3 igual los cuenta bien.
    cant_users_ayer = qsu_ayer.count()

    # Grabamos los datos en la base
    usuaurios_de_ayer = UsuariosPorDias(app=app_go, cant_usuarios=cant_users_ayer, fecha=ayer)
    usuaurios_de_ayer.save()

    return
