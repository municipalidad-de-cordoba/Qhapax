#!/usr/bin/python
"""
python manage.py crear_usuarios_activos_app --id_software 2
"""
from django.core.management.base import BaseCommand
from django.db import transaction
from softwaremunicipal.models import SoftwarePublico, GrupoUsuariosSoftware, UsuarioSoftware, UsuarioEnGrupoSoftware
from log_usuarios.models import LogUsuario, DetalleLogUsuario
from transportepublico.models import RecorridoLineaTransportePublico, LineaTransportePublico
from django.contrib.auth.models import User
import datetime
import sys
from django.utils import timezone
import logging
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = """Creación y actualización del grupo de usuarios activos de un software particular.
                Permite eliminar y recrear el grupo (recomendado)"""

    def add_arguments(self, parser):
        parser.add_argument('--clean_all', nargs='?', type=bool, default=True, help='Borrar todos los registros anteriores del grupo y refrescar')
        parser.add_argument('--id_software', nargs='?', type=bool, default=None, help='Id del software del cual se quiere obtener los usuarios')

    @transaction.atomic
    def handle(self, *args, **options):
        clean_all = options['clean_all']
        id_software = options['id_software']

        if id_software is None:
            self.stdout.write(self.style.ERROR('Es necesario el id del software'))
            sys.exit(1)

        app = SoftwarePublico.objects.get(pk=id_software)

        # Grupo de todos los usuarios de la app
        grupo_nombre = 'Todos los usuarios de {}'.format(app.titulo)
        codigo_interno = 'USUARIOS_{}'.format(app.titulo)
        self.stdout.write(self.style.SUCCESS('Constuyendo grupo de {}'.format(grupo_nombre)))
        grupo, created = GrupoUsuariosSoftware.objects.get_or_create(software=app,
                            nombre=grupo_nombre, codigo_interno=codigo_interno
                            )

        usuarios_app = User.objects.filter(usuariosoftware__software=app)

        self.stdout.write(self.style.SUCCESS('Agregando {} usuarios al grupo'.format(usuarios_app.count() - grupo.usuarios.all().count())))
        if clean_all:
            grupo.usuarios.all().delete()

        for usuario in usuarios_app:
            u, creado = UsuarioEnGrupoSoftware.objects.get_or_create(user_id=usuario.id, grupo=grupo)
