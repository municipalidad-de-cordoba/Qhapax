#!/usr/bin/python
"""
Actualizar los grupos de usuarios frecuentes y muy frecuentes de cada línea

python manage.py crear_usuarios_activos_lineas_go --dias_atras 7 --usos 2 --prefijo_nombre_grupo "Usuarios de la línea" --codigo_interno_grupo "USUARIOS_BONDI"
python manage.py crear_usuarios_activos_lineas_go --dias_atras 7 --usos 6 --prefijo_nombre_grupo "Usuarios muy frecuentes de la línea" --codigo_interno_grupo "USUARIOS_MUY_FRECUENTES_BONDI"

"""
from django.core.management.base import BaseCommand
from django.db import transaction
from django.conf import settings
from softwaremunicipal.models import SoftwarePublico, GrupoUsuariosSoftware, UsuarioSoftware, UsuarioEnGrupoSoftware
from log_usuarios.models import LogUsuario, DetalleLogUsuario
from transportepublico.models import RecorridoLineaTransportePublico, LineaTransportePublico
from django.contrib.auth.models import User
import datetime
from django.utils import timezone
import logging
logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = """Creación y actualización de los grupos de usuarios activos de una línea de transporte
                Obtener los usuarios que piden frecuentemente una linea de colectivo 
                predeterminado los usuarios que esperan dos veces una línea en los últimos 7 días
                Permite eliminar y recrear los grupos (recomendado)"""

    def add_arguments(self, parser):
        parser.add_argument('--dias_atras', nargs='?', type=int, default=7, help='Hasta cuantos dias atras se mira de logs')
        parser.add_argument('--usos', nargs='?', type=int, default=2, help='Usos para considerar al usuario activo en una linea')
        parser.add_argument('--clean_all', nargs='?', type=bool, default=True, help='Borrar todos los registros anteriores de cada grupo y refrescar')
        # se recomienda crear grupos de usuarios normales (1 o 2 x semana) y otros de usuarios MUY frecuentes (6 o más por semana)
        parser.add_argument('--prefijo_nombre_grupo', nargs='?', type=str, default='Usuarios de la línea',
                                help='Los grupos a construirse deben tener un nombre con el nro de la línea y un prefijo')
        parser.add_argument('--codigo_interno_grupo', nargs='?', type=str, default='USUARIOS_BONDI',
                                help='Cada grupo debe tener un codigo_interno que nos permita llegar rápido a estos grupos')
        

    @transaction.atomic
    def handle(self, *args, **options):
        dias_atras = options['dias_atras']
        usos = options['usos']
        clean_all = options['clean_all']
        prefijo_nombre_grupo = options['prefijo_nombre_grupo']
        codigo_interno_grupo = options['codigo_interno_grupo']
        self.stdout.write(self.style.SUCCESS('Revisando {} días de logs para encotrar usuarios que pidieron al menos {} bondis de una línea'))

        app_go = SoftwarePublico.objects.get(pk=settings.APP_GO_PK)
        ult_dias = timezone.now() - datetime.timedelta(days=dias_atras)
        # ultimos_logs = LogUsuario.objects.filter(sistema=app_go, created__gt=ult_dias, codigo_accion='PIDEBONDI')
        ultimos_logs = DetalleLogUsuario.objects.filter(log_usuario__sistema=app_go,
                                                        log_usuario__created__gt=ult_dias,
                                                        log_usuario__codigo_accion='PIDEBONDI',
                                                        codigo='recorrido_id').order_by('log_usuario__created')

        resultados = {}
        # iterar por todas las líneas activas
        lineas = LineaTransportePublico.objects.filter(publicado=True)
        for linea in lineas:
            self.stdout.write(self.style.SUCCESS('Revisando logs línea {}'.format(linea.nombre_publico)))
            for recorrido in linea.recorridos.filter(publicado=True):
                
                logs = ultimos_logs.filter(int_val=recorrido.id)
                self.stdout.write(self.style.SUCCESS('Revisando logs recorrido {} ({} logs)'.format(recorrido.descripcion_interna, logs.count())))

                # eliminar las consultas de un mismo usuario que están muy pegadas (no son pedidos nuevos)
                last_log = {}
                for reg in logs:
                    user = reg.log_usuario.user
                    key = '{}-{}'.format(user.id, recorrido.id)
                    if key not in last_log.keys():
                        last_log[key] = reg.log_usuario.created
                    else:
                        dif = reg.log_usuario.created - last_log[key]
                        # self.stdout.write(self.style.SUCCESS('diff {} - {} = {}'.format(reg.log_usuario.created, last_log[key], dif)))
                        if dif < datetime.timedelta(hours=1):  # una hora
                            continue  # ignorar, log muy seguido

                    if linea.id not in resultados.keys():
                        resultados[linea.id] = {}
                    if user.id not in resultados[linea.id].keys():
                        resultados[linea.id][user.id] = 0

                    resultados[linea.id][user.id] += 1  # contador de usos

        for linea_id in resultados.keys():
            linea = LineaTransportePublico.objects.get(pk=linea_id)
            # crear un "grupo de usuarios activos" de cada linea
            nombre = '{} {}'.format(prefijo_nombre_grupo, linea.nombre_publico)
            self.stdout.write(self.style.SUCCESS('Constuyendo grupo {}'.format(nombre)))
            grupo, created = GrupoUsuariosSoftware.objects.get_or_create(software=app_go, nombre=nombre,
                                                                            codigo_interno=codigo_interno_grupo,
                                                                            cod_num_1=linea.id  # para despues filtrar x linea y generar mensajes
                                                                            )
            if clean_all:
                grupo.usuarios.all().delete()

            self.stdout.write(self.style.SUCCESS('Agregando {} usuarios al grupo'.format(len(resultados[linea_id].keys()))))
            for user_id in resultados[linea_id].keys():
                total = resultados[linea_id][user_id]
                if total >= usos:
                    UsuarioEnGrupoSoftware.objects.create(user_id=user_id, grupo=grupo, ponderador=total)

        # Grupo de todos los usuarios de GO
        self.stdout.write(self.style.SUCCESS('Constuyendo grupo de Todos los usuarios de GO'))
        grupo, created = GrupoUsuariosSoftware.objects.get_or_create(software=app_go,
                            nombre='Todos los usuarios de GO', codigo_interno='USUARIOS_GO'
                            )

        usuarios_go = User.objects.filter(usuariosoftware__software=app_go)

        self.stdout.write(self.style.SUCCESS('Agregando {} usuarios al grupo'.format(usuarios_go.count() - grupo.usuarios.all().count())))
        if clean_all:
            grupo.usuarios.all().delete()

        for usuario in usuarios_go:
            u, creado = UsuarioEnGrupoSoftware.objects.get_or_create(user_id=usuario.id, grupo=grupo)
