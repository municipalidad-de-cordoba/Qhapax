'''
Productos de software que usa o desarrolla el municipio
Comienza como guía al publico de aplicaciones móviles
Pensado para liberar portales de apps o compartir software abierto
'''
from django.db import models
from versatileimagefield.fields import VersatileImageField
from simple_history.models import HistoricalRecords
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User


class EquipoDeDesarrollo(models.Model):
    ''' equipos de desarrollo de software '''
    nombre = models.CharField(max_length=250)
    descripcion = models.TextField(null=True, blank=True)
    observaciones_internas = models.TextField(null=True, blank=True)
    es_proveedor_externo = models.BooleanField(default=False, help_text='Marcar siempre que no sea un equipo interno de desarrollo')

    def __str__(self):
        return self.nombre


class TecnologiaSoftware(models.Model):
    """ tecnologías usadas, a fines estadísticos y se saber las capacidades de los recursos que tenemos """
    LENGUAJE = 10
    NO_SE_USA_MAS = 20  # fixme, eliminar sin que caiga occidente
    FRAMEWORK = 30

    tipos = ((LENGUAJE, 'Lenguaje de progración'),
                (NO_SE_USA_MAS, 'No usar'),
                (FRAMEWORK, 'Framework de desarrollo')
                )

    nombre = models.CharField(max_length=120)
    tipo = models.PositiveIntegerField(choices=tipos)

    def __str__(self):
        return "{} {}".format(self.nombre, self.get_tipo_display())

    class Meta:
        ordering = ['tipo', 'nombre']


class VersionTecnologiaSoftware(models.Model):
    """ versiones de las tecnologias usadas """
    tecnologia = models.ForeignKey(TecnologiaSoftware, on_delete=models.CASCADE, null=True, blank=True)
    nombre_o_numero_version = models.CharField(max_length=40)
    observaciones = models.TextField(null=True, blank=True)
    es_obsoleta = models.NullBooleanField(default=False, null=True)  # para saber que nos queda fuera de servicio y debe migrarse

    def __str__(self):
        return "{} v{} {}".format(self.tecnologia.nombre, self.nombre_o_numero_version, self.tecnologia.get_tipo_display())

    class Meta:
        ordering = ['tecnologia', 'nombre_o_numero_version']


class SoftwarePublico(models.Model):
    '''
    Cada software desarrollado o usado por la municipalidad
    '''
    titulo = models.CharField(max_length=250)
    descripcion = models.TextField(null=True, blank=True)
    observaciones_internas = models.TextField(null=True, blank=True)
    logo = VersatileImageField(upload_to='imagenes/software-logos', null=True, blank=True)
    imagen = VersatileImageField(upload_to='imagenes/software-img', null=True, blank=True)
    # se puede mostrar en listas al público
    publicado = models.BooleanField(default=False)
    # casos donde queremos desactivar pero seguir mostrando algun producto
    activado = models.BooleanField(default=False)
    url_publica = models.URLField(null=True, blank=True)
    url_privada = models.URLField(null=True, blank=True)
    mas_info_url = models.URLField(null=True, blank=True)
    documentacion_interna_url = models.URLField(null=True, blank=True)
    orden = models.PositiveIntegerField(default=100)
    tecnologias_usadas = models.ManyToManyField(VersionTecnologiaSoftware, blank=True)
    equipos_responsables = models.ManyToManyField(EquipoDeDesarrollo, blank=True, help_text='marcar los equipos que participan del desarrollo')
    observaciones_equipos = models.TextField(null=True, blank=True, help_text='Detalles de los equipos que trabajan en el desarrollo')

    history = HistoricalRecords()

    def __str__(self):
        return self.titulo

    class Meta:
        ordering = ['orden', 'titulo']


class AppMovil(SoftwarePublico):
    '''
    Productos de software que son aplicaciones móviles
    '''
    android_market = models.URLField(null=True, blank=True)
    ios_market = models.URLField(null=True, blank=True)
    windows_market = models.URLField(null=True, blank=True)
    web_version = models.URLField(null=True, blank=True)

    # datos para nutrirt a la app desde afuera sin tener que recompilar y subir al market
    terminos_y_condiciones = RichTextField(null=True, blank=True, help_text="Texto para la sección de la APP Terminos y Condiciones")
    politica_de_privacidad = RichTextField(null=True, blank=True, help_text="Texto para la sección de la APP Política de privacidad")
    about_app = RichTextField(null=True, blank=True, help_text="Texto para la sección de la APP Acerca de")

    # datos para anuncios de versiones viejas o por deprecar en la APP
    # si el cliente es menor que lo indicado acá la app no debería funcionar enviando un mensaje
    version_minima = models.CharField(max_length=250, null=True, blank=True)
    txt_sino_hay_version_minima = models.TextField(default="Tu versión de esta aplicación ha quedado obsleta. Actualízala para poder continuar")
    # si el cliente no tiene la última versión y quiero avisarle que podría actualizar
    version_recomendada = models.CharField(max_length=250, null=True, blank=True)
    txt_sino_hay_version_recomendada = models.TextField(default="Te recomendamos actualizar esta aplicación, esta por quedar obsoleta")


class UsuarioSoftware(models.Model):
    """ Dejamos asentado aquí los usuarios registrados
        que usan algun sistema (por ejemplo usuarios de una app) """
    
    software = models.ForeignKey(SoftwarePublico, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    # a los fines de saber cuantas veces accede
    accesos = models.PositiveIntegerField(default=0)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def touch(self):
        self.accesos += 1
        self.save()

    def __str__(self):
        return '{} en {}'.format(self.user.username, self.software.titulo)
    
    class Meta:
        unique_together = (("software", "user"),)


class GrupoUsuariosSoftware(models.Model):
    ''' grupos de usuarios de una app o sistema con características especiales.
        Pueden ser dinámicos y actualizarse periódicamente
        Por ejemplo: usuarios de la app "Go" que toman el colectivo 43 regularmente 
        Esto sirve para enviar mensajes a grupos en particular '''

    software = models.ForeignKey(SoftwarePublico, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=90)
    descripcion = models.TextField(null=True, blank=True)
    codigo_interno = models.CharField(max_length=30, null=True, blank=True,
                                        help_text='Codigo para uso especial en generadores automáticos de grupos para filtrar')
    # estos grupos podrían querer referenciarse por otros códigos. Un objecto generico quizas sirv tambien
    # bases da datos no relacionales ayudaría aquí, quizás algo haystack/elastic sería mejor
    cod_num_1 = models.IntegerField(null=True, blank=True)  # en los grupos de usuarios de colectivos aca por ejemplo se pone el ID de la línea
    cod_num_2 = models.IntegerField(null=True, blank=True)
    cod_str_1 = models.CharField(null=True, blank=True, max_length=40)
    cod_str_2 = models.CharField(null=True, blank=True, max_length=40)

    def cantidad_usuarios(self):
        return self.usuarios.count()

    def __str__(self):
        return self.nombre


class UsuarioEnGrupoSoftware(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='en_grupos_software')
    grupo = models.ForeignKey(GrupoUsuariosSoftware, on_delete=models.CASCADE, related_name='usuarios')
    ponderador = models.PositiveIntegerField(default=0, help_text='Opcional, que tanto pertenece el usuario a este grupo')

    def __str__(self):
        return '{} en {}'.format(self.user.username, self.grupo.nombre)


class DefaultSettingsSoftware(models.Model):
    """ configuracion predeterminada de la aplicacion
        (para enviar vía API y cambiarla sin recompilar) """
    software = models.ForeignKey(SoftwarePublico, on_delete=models.CASCADE, related_name='configuraciones')
    nombre = models.CharField(max_length=90)
    valor = models.TextField()
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return '{} {} '.format(self.software.titulo, self.nombre)

class RepositorioSoftware(models.Model):
    '''
    repositorio donde esta administrado el código fuente
    '''
    software = models.ForeignKey(SoftwarePublico, null=True, blank=True)
    descripcion = models.TextField(null=True, blank=True)
    observaciones_internas = models.TextField(null=True, blank=True)

    DESCONOCIDO = 1
    GITHUB = 10
    GITLAB = 20
    BITBUCKET = 30
    SVN = 40
    TEAM_FUNDATION_SERVER = 50
    MERCURIAL = 60
    tipos = (
            (DESCONOCIDO, 'Desconocido'),
            (GITHUB, 'GitHub'),
            (GITLAB, 'GitLab'),
            (BITBUCKET, 'BitBucket'),
            (SVN, 'SVN'),
            (TEAM_FUNDATION_SERVER, 'Team Foundation Server'),
            (MERCURIAL, 'Mercurial'),
            )
    tipo = models.PositiveIntegerField(choices=tipos, default=DESCONOCIDO)
    es_software_libre = models.BooleanField(default=False)
    url = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.url


class ScreenShot(models.Model):
    '''
    Pantallazaos del producto funcionando.
    '''
    software = models.ForeignKey(SoftwarePublico)
    titulo = models.CharField(max_length=250, null=True, blank=True)
    imagen = VersatileImageField(upload_to='imagenes/software-screen', null=True, blank=True)

    def __str__(self):
        return "{} {}".format(self.titulo, self.imagen)


class TecnologiaBaseDeDatos(models.Model):
    """ tecnologías usadas, a fines estadísticos y se saber las capacidades de los recursos que tenemos """
    nombre = models.CharField(max_length=120)

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']


class VersionTecnologiaBaseDeDatos(models.Model):
    """ versiones de las tecnologias usadas """
    tecnologia = models.ForeignKey(TecnologiaBaseDeDatos, on_delete=models.CASCADE)
    nombre_o_numero_version = models.CharField(max_length=40)
    observaciones = models.TextField(null=True, blank=True)
    es_obsoleta = models.NullBooleanField(default=False, null=True)  # para saber que nos queda fuera de servicio y debe migrarse

    def __str__(self):
        return "{} v{}".format(self.tecnologia.nombre, self.nombre_o_numero_version)

    class Meta:
        ordering = ['tecnologia', 'nombre_o_numero_version']


class BaseDeDatos(models.Model):
    '''
    Cada base de datos conectada a un software
    '''
    nombre = models.CharField(max_length=250)
    software = models.ForeignKey(SoftwarePublico, null=True, blank=True)
    version_tecnologia = models.ForeignKey(VersionTecnologiaBaseDeDatos)
    descripcion = models.TextField(null=True, blank=True)
    observaciones_internas = models.TextField(null=True, blank=True)
    documentacion_interna_url = models.URLField(null=True, blank=True)
    publicado = models.BooleanField(default=False, help_text='¿puede publicarse en listas abiertas?')
    orden = models.PositiveIntegerField(default=100)

    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['orden', 'nombre']


class UsuariosPorDias(models.Model):
    '''
    Clase para registrar la cantidad de usuarios de las aplicaciones que tengamos
    '''
    app = models.ForeignKey(SoftwarePublico, null=True, blank=True)
    cant_usuarios = models.PositiveIntegerField(default=0)
    fecha = models.DateField(null=True, blank=True)

    def __str__(self):
        return '{} usuarios en la app {} el dia {}'.format(self.cant_usuarios, self.app.titulo, self.fecha)
