from django.conf.urls import url

from . import views


urlpatterns = [url(r'^obras-de-terceros.(?P<filetype>kml|json)$',
                   views.mapa_obra_terceros,
                   name='obras-terceros.mapa'),
               url(r'^trazado-obras-publicas.(?P<filetype>kml|json)$',
                   views.mapa_trazado_obras_de_terceros,
                   name='trazado-obras-terceros.mapa'),
               ]
