from django.contrib import admin
from .models import (ObraDeTerceros, TrazadoObraDeTercero, FotoDeObraDeTercero,
                     FotoDeTrazadoDeObraDeTercero)
from core.admin import QhapaxOSMGeoAdmin
from .forms import ObraDeTercerosFormSelect2, TrazadoObraDeTercerosFormSelect2


# class FotoDeObraDeTerceroInLine(admin.StackedInline):
#    model = FotoDeObraDeTercero
#    extra = 1


class ObraDeTercerosAdmin(QhapaxOSMGeoAdmin):
    form = ObraDeTercerosFormSelect2
    list_display = ['nombre', 'id', 'id_externo', 'estado',
                    'publicado', 'publicado_gobierno', 'contratista']
    search_fields = ['nombre', 'id', 'id_externo', 'contratista__nombre']
    list_filter = ['tipo', 'estado', 'publicado', 'publicado_gobierno']
#    inlines = [FotoDeObraDeTerceroInLine]

    # def get_queryset(self, request):
    #     queryset = ObraDeTerceros.objects.all().order_by('-id')
    #     return queryset

    # sin esto no funciona el JS de select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


class TrazadoObraDeTerceroAdmin(QhapaxOSMGeoAdmin):
    form = TrazadoObraDeTercerosFormSelect2

    def id_obra(self, obj):
        return obj.obra.id
    list_display = ['obra', 'id_obra', 'id', 'id_externo',
                    'descripcion_publica', 'publicado', 'publicado_gobierno']
    search_fields = ['obra__nombre', 'obra__id',
                     'descripcion_publica', 'id', 'id_externo']
    list_filter = ['publicado', 'publicado_gobierno']
#    inlines = [FotoDeObraDeTerceroInLine]

    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


class FotoDeObraDeTerceroAdmin(QhapaxOSMGeoAdmin):

    def file_size(self, obj):
        if obj.foto and hasattr(obj.foto, 'url'):
            sz = obj.foto.file.size / (1024 * 1024)
            return '{:.2f} MB'.format(sz)
        else:
            return "No hay foto"
    # file_size.admin_order_field = 'foto__file__size'

    def id_obra(self, obj):
        return obj.obra.id

    def foto_thumb(self, obj):
        if obj.foto:
            try:
                th80 = obj.foto.thumbnail['80x80'].url
            except Exception as e:
                pass
            else:
                return '<img src="{}" style="width: 80px;"/>'.format(
                    obj.foto.thumbnail['80x80'].url)

        return ' <img src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACXBIWXMAAAAnAAAAJwEqCZFPAAAA B3RJTUUH4AcJFDE2B3C7PwAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUH AAAACklEQVQI12P4DwABAQEAG7buVgAAAABJRU5ErkJggg==" alt="blanco " style="width: 80px; height: 120px;" />'

    foto_thumb.short_description = 'thumb'
    foto_thumb.allow_tags = True

    fields = ['obra', 'id_externo', 'url', 'foto']
    list_display = [
        'obra',
        'id_obra',
        'id_externo',
        'publicado',
        'publicado_gobierno',
        'foto_thumb',
        'url',
        'file_size']
    search_fields = ['obra__nombre', 'id_externo', 'url']
    list_filter = ['publicado', 'publicado_gobierno']


class FotoDeTrazadoObraDeTerceroAdmin(QhapaxOSMGeoAdmin):

    def file_size(self, obj):
        if obj.foto and hasattr(obj.foto, 'url'):
            sz = obj.foto.file.size / (1024 * 1024)
            return '{:.2f} MB'.format(sz)
        else:
            return "No hay foto"
    # file_size.admin_order_field = 'foto__file__size'

    def id_obra(self, obj):
        if obj.trazado is not None:
            return obj.trazado.obra.id

    def foto_thumb(self, obj):
        if obj.foto:
            try:
                th80 = obj.foto.thumbnail['80x80'].url
            except Exception as e:
                pass
            else:
                return '<img src="{}" style="width: 80px;"/>'.format(
                    obj.foto.thumbnail['80x80'].url)

        return ' <img src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACXBIWXMAAAAnAAAAJwEqCZFPAAAA B3RJTUUH4AcJFDE2B3C7PwAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUH AAAACklEQVQI12P4DwABAQEAG7buVgAAAABJRU5ErkJggg==" alt="blanco " style="width: 80px; height: 120px;" />'

    foto_thumb.short_description = 'thumb'
    foto_thumb.allow_tags = True

    fields = ['trazado', 'id_externo', 'url', 'foto']
    list_display = [
        'id_obra',
        'trazado',
        'id_externo',
        'publicado',
        'publicado_gobierno',
        'foto_thumb',
        'url',
        'file_size']
    search_fields = ['trazado__descripcion_publica', 'obra__nombre',
                     'id_externo', 'trazado__obra__id', 'url']
    list_filter = ['publicado', 'publicado_gobierno']


admin.site.register(ObraDeTerceros, ObraDeTercerosAdmin)
admin.site.register(TrazadoObraDeTercero, TrazadoObraDeTerceroAdmin)
admin.site.register(FotoDeObraDeTercero, FotoDeObraDeTerceroAdmin)
admin.site.register(
    FotoDeTrazadoDeObraDeTercero,
    FotoDeTrazadoObraDeTerceroAdmin)
