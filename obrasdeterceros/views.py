from django.views.decorators.cache import cache_page
from django.http import HttpResponse
from django.utils.text import slugify
from .models import ObraDeTerceros, TrazadoObraDeTercero
from obras.models import TipoObra


@cache_page(60 * 60)  # 1 hora
def mapa_obra_terceros(request, filetype):
    ''' mapa de las obras de terceros en KML o en (geo)JSON '''

    content_type = "application/json" if filetype == 'json' else 'application/vnd.google-earth.kml+xml'
    # FIXME agregar la salida geoJson por si fuera necesaria

    obras = ObraDeTerceros.objects.filter(
        publicado=True, publicado_gobierno=True)
    # referencias del formato KML
    # https://developers.google.com/kml/documentation/kmlreference

    kml = ('<?xml version="1.0" encoding="UTF-8"?>'
           '<kml xmlns="http://www.opengis.net/kml/2.2">'
           '<Document>'
           '<name>ObrasPublicasCordoba.kml</name>'
           '{styles}'
           '{doc}'
           '</Document>'
           '</kml>')

    tipos = TipoObra.objects.all()
    styles = []
    for tipo in tipos:
        tipo_slug = slugify(tipo.nombre)
        style = ('<Style id="{tipo_slug}">\n'
                 '<LineStyle>\n'
                 '<color>{color}</color>\n'
                 '<width>4</width>\n'
                 '</LineStyle>\n'
                 '</Style>\n'.format(tipo_slug=tipo_slug, color=tipo.color))

        styles.append(style)

    styles_str = ''.join(styles)

    places = []
    for obra in obras:
        if obra.ubicacion is None:
            continue
        tipo_slug = slugify(obra.tipo.nombre)
        place = (
            '<Placemark>\n'
            '<name><![CDATA[{nombre}]]></name>\n'
            '<styleUrl>#{tipo_slug}</styleUrl>\n'
            '<ExtendedData>'
            '<Data name="Decreto"><value><![CDATA[{decreto}]]></value></Data>'
            '<Data name="Expediente"><value><![CDATA[{expediente_origen}]]></value></Data>'
            '<Data name="Porcentaje de avance"><value><![CDATA[{porcentaje_completado}]]></value></Data>'
            '<Data name="Tipo"><value><![CDATA[{tipo}]]></value></Data>'
            '<Data name="Organización"><value><![CDATA[{organizacion}]]></value></Data>'
            '<Data name="Monto"><value><![CDATA[{monto}]]></value></Data>'
            '<Data name="Fecha de inicio"><value><![CDATA[{fecha_inicio}]]></value></Data>'
            '<Data name="Fecha de finalización estimada"><value><![CDATA[{fecha_finalizacion_estimada}]]></value></Data>'
            '<Data name="Barrios"><value><![CDATA[{barrios}]]></value></Data>'
            '</ExtendedData>'
            '{geom}'
            '</Placemark>\n')

        nombre = 'Sin cargar aún' if obra.contratista is None else obra.contratista.nombre
        cuit = '' if obra.contratista is None else obra.contratista.CUIT
        org = '{} {}'.format(nombre, cuit)

        monto = '{} {}'.format(obra.moneda_monto, obra.monto)
        barrios = ''
        for barrio in obra.barrios.all():
            barrios += '{} - '.format(barrio.nombre)
        geo = place.format(
            nombre=obra.nombre,
            decreto=obra.decreto,
            expediente_origen=obra.expediente_origen,
            porcentaje_completado=obra.porcentaje_completado,
            geom=obra.ubicacion.kml,
            tipo=obra.tipo.nombre,
            organizacion=org,
            fecha_inicio=obra.fecha_inicio,
            monto=monto,
            fecha_finalizacion_estimada=obra.fecha_finalizacion_estimada,
            barrios=barrios,
            tipo_slug=tipo_slug)

        places.append(geo)

    places_str = ''.join(places)
    data = kml.format(doc=places_str, styles=styles_str)
    return HttpResponse(data, content_type=content_type)


@cache_page(60 * 60)  # 1 hora
def mapa_trazado_obras_de_terceros(request, filetype):
    ''' mapa de las obras públcias en KML o en (geo)JSON '''

    content_type = "application/json" if filetype == 'json' else 'application/vnd.google-earth.kml+xml'
    # FIXME agregar la salida geoJson por si fuera necesaria

    obras = ObraDeTerceros.objects.filter(
        publicado=True, publicado_gobierno=True)
    # referencias del formato KML
    # https://developers.google.com/kml/documentation/kmlreference

    kml = ('<?xml version="1.0" encoding="UTF-8"?>'
           '<kml xmlns="http://www.opengis.net/kml/2.2">'
           '<Document>'
           '<name>TrazadoObrasPublicasCordoba.kml</name>'
           '{styles}'
           '{folders}'
           '</Document>'
           '</kml>')

    tipos = TipoObra.objects.all()
    styles = []
    for tipo in tipos:
        tipo_slug = slugify(tipo.nombre)
        style = ('<Style id="{tipo_slug}">\n'
                 '<LineStyle>\n'
                 '<color>{color}</color>\n'
                 '<width>4</width>\n'
                 '</LineStyle>\n'
                 '</Style>\n'.format(tipo_slug=tipo_slug, color=tipo.color))

        styles.append(style)

    styles_str = ''.join(styles)

    folders = []
    for obra in obras:
        trazado_obras = TrazadoObraDeTercero.objects.filter(
            publicado=True, publicado_gobierno=True, obra=obra)

        obra_slug = slugify(obra.nombre)
        tipo_slug = slugify(obra.tipo.nombre)

        nombre = 'Sin cargar aún' if obra.contratista is None else obra.contratista.nombre
        cuit = '' if obra.contratista is None else obra.contratista.CUIT
        org = '{} {}'.format(nombre, cuit)

        monto = '{} {}'.format(obra.moneda_monto, obra.monto)
        barrios = ''
        for barrio in obra.barrios.all():
            barrios += '{} - '.format(barrio.nombre)

        folder = (
            '<Folder>\n'
            '<name><![CDATA[{nombre}]]></name>\n'
            '<description><![CDATA[{tipo}: OBRA: {nombre}]]></name>\n'
            '<open>1</open>'
            '<ExtendedData>'
            '<Data name="Decreto"><value><![CDATA[{decreto}]]></value></Data>'
            '<Data name="Expediente"><value><![CDATA[{expediente_origen}]]></value></Data>'
            '<Data name="Porcentaje de avance"><value><![CDATA[{porcentaje_completado}]]></value></Data>'
            '<Data name="Tipo"><value><![CDATA[{tipo}]]></value></Data>'
            '<Data name="Organización"><value><![CDATA[{organizacion}]]></value></Data>'
            '<Data name="Monto"><value><![CDATA[{monto}]]></value></Data>'
            '<Data name="Fecha de inicio"><value><![CDATA[{fecha_inicio}]]></value></Data>'
            '<Data name="Fecha de finalización estimada"><value><![CDATA[{fecha_finalizacion_estimada}]]></value></Data>'
            '<Data name="Barrios"><value><![CDATA[{barrios}]]></value></Data>'
            '</ExtendedData>'
            '{docs}'
            '</Folder>\n')

        places = []
        for trazado in trazado_obras:
            place = (
                '<Placemark>\n'
                '<name><![CDATA[{descripcion_publica}]]></name>\n'
                '<description><![CDATA[{tipo_nombre}: OBRA: {nombre}]]></name>\n'
                '<styleUrl>#{tipo_slug}</styleUrl>\n'
                '{geom}'
                '</Placemark>\n')
            geo = place.format(
                descripcion_publica=trazado.descripcion_publica,
                tipo_nombre=obra.tipo.nombre,
                nombre=obra.nombre,
                geom=trazado.trazado.kml,
                tipo=trazado.obra.tipo.nombre,
                tipo_slug=tipo_slug)

            places.append(geo)

        places_str = ''.join(places)
        folder_final = folder.format(
            nombre=obra.nombre,
            decreto=obra.decreto,
            expediente_origen=obra.expediente_origen,
            porcentaje_completado=obra.porcentaje_completado,
            tipo=obra.tipo.nombre,
            organizacion=org,
            fecha_inicio=obra.fecha_inicio,
            monto=monto,
            fecha_finalizacion_estimada=obra.fecha_finalizacion_estimada,
            barrios=barrios,
            docs=places_str)

        folders.append(folder_final)

    folders_str = ''.join(folders)
    data = kml.format(folders=folders_str, styles=styles_str)
    return HttpResponse(data, content_type=content_type)
