#!/usr/bin/python
"""
Importar nuevas obras de terceros
"""
from django.core.management.base import BaseCommand
from django.db import transaction
from datetime import datetime
import json
import pytz
import random
import sys
import requests
import difflib
from obrasdeterceros.settings import URL_AUTHORIZATION, URL_API_OBRAS_TERCEROS
from obrasdeterceros.models import ObraDeTerceros, FotoDeObraDeTercero
from obras.models import OrigenDatoObra, TipoObra
from barrios.models import Barrio
from organizaciones.models import Organizacion
from django.contrib.gis.geos import GEOSGeometry, Point
from django.core.files import File
import tempfile
from PIL import Image
from core.utils import normalizar


class Command(BaseCommand):
    help = """Comando para Importar obras de terceros en ejecución """

    def add_arguments(self, parser):
        parser.add_argument(
            '--fecha',
            type=str,
            help=('fecha desde la cual se importarán nuevas obras y cambios a',
                  'las existentes. Formato dd/mm/aaaa'),
            default='01/01/2016')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS(
            'Iniciando importación de datos de obras de terceros'))

        url_base = URL_API_OBRAS_TERCEROS

        # url = url_base
        fecha = options['fecha']
        url = '{}&fecha={}'.format(url_base, fecha)
        session = requests.Session()

        self.stdout.write(
            self.style.SUCCESS(
                'Buscando datos en {}'.format(url)))

        headers = {'Authorization': URL_AUTHORIZATION}
        basedata = session.get(url, headers=headers).content  # es tipo byte
        # basedata = basedata.decode("utf-8")
        basedata = basedata.decode("iso-8859-1")

        try:
            jsondata = json.loads(basedata)
        except Exception as e:
            self.stdout.write(self.style.ERROR('JSON Error: {}'.format(url)))
            self.stdout.write(self.style.ERROR(e))
            sys.exit(1)

        # marcar como el origen que tienen
        origen, created = OrigenDatoObra.objects.get_or_create(
            nombre='MuniDigital.com')

        obras = len(jsondata['features'])
        self.stdout.write(
            self.style.SUCCESS(
                'Se obtuvieron datos de {} obras'.format(obras)))

        obras_nuevas = 0
        obras_repetidas = 0
        tipos_nuevos = 0
        tipos_repetidos = 0
        organizaciones_nuevas = 0
        organizaciones_repetidas = 0
        total_fotos = 0
        fotos_nuevas = 0
        fotos_repetidas = 0
        errores = []
        ciclos = 0

        # datos para barrios
        barrios = Barrio.objects.values_list('nombre', flat=True)
        poligonos_barrios = Barrio.objects.values_list('poligono', flat=True)

        for fobra in jsondata['features']:
            ciclos += 1
            dato = fobra['properties']

            nombre = dato['nombreObra'].strip()
            if 'id_obra' not in dato.keys():
                self.stdout.write(
                    self.style.ERROR(
                        'La obra "{} no tiene ID_OBRA'.format(nombre)))
                continue

            id_externo = dato['id_obra']
            obra, created_obra = ObraDeTerceros.objects.get_or_create(
                id_externo=id_externo)
            if created_obra:
                obras_nuevas += 1
            else:
                obras_repetidas += 1

            obra.nombre = nombre
            obra.decreto = dato['decreto'].strip()
            obra.expediente_origen = dato['expedienteOriginal'].strip()
            tipo_str = dato['tipoObra'].strip()
            if tipo_str == '':
                tipo_str = 'Desconocido'
            tipo, created = TipoObra.objects.get_or_create(
                nombre=tipo_str)
            if created:
                tipos_nuevos += 1
            else:
                tipos_repetidos += 1

            # Si el tipo fue creado, se le da un color aleatorio
            if tipo.color == 'ffffffff':
                tipo.color = ''.join(random.choice(
                    '0123456789abcdef') for n in range(8))
                tipo.save()
            obra.tipo = tipo
            obra.origen = origen

            # fotos de las obras
            # self.stdout.write(self.style.SUCCESS('Fotos de obra n°:{}'.format(ciclos)))
            adjuntos = dato['adjuntos']
            # self.stdout.write(self.style.SUCCESS('Hay {} fotos'.format(len(adjuntos))))
            fotos = 0
            for adjunto in adjuntos:
                foto, created = FotoDeObraDeTercero.objects.get_or_create(
                    obra=obra, id_externo=adjunto['id'])

                foto.url = adjunto['url']
                # el campo paraPublicar tiene un string, se pasa a booleano
                if adjunto['paraPublicar'] == 'SI':
                    foto.publicado = True
                else:
                    foto.publicado = False
                # importo las fotos solo si es la primera vez
                if created:
                    fotos_nuevas += 1

                    fotos += 1
                    # self.stdout.write(self.style.SUCCESS('Nueva Foto n°: {} Trayendo imagen {}'.format(fotos, foto.url)))
                    url_foto = adjunto['url']
                    contenido = session.get(url_foto).content  # es tipo byte

                    tf = tempfile.NamedTemporaryFile()
                    tf.write(contenido)

                    img = File(tf)
                    try:
                        # controlamos que el archivo sea una imagen válida
                        img = Image.open(img)
                        if img.size != 0:
                            foto.foto.save(
                                str(foto.url.split('/')[-1]), File(tf))
                    except Exception as e:
                        err = 'ERROR con el formato de la FOTO:{}'.format(
                            foto.url)
                        self.stdout.write(self.style.ERROR(err))
                        errores.append(err)
                        foto.delete()
                        continue

                        foto.save()
                else:
                    fotos_repetidas += 1
                #    self.stdout.write(self.style.SUCCESS('Omitiendo descargar imagen {}'.format(foto.url)))
            total_fotos += fotos
            # datos geoespaciales
            geometry = fobra['geometry']
            # self.stdout.write(self.style.SUCCESS('{} GEO {}'.format(nombre, geometry)))

            # if dato['id_obra'] == 171:
            #     print("obra 171 con geometrycollection")
            #     print(geometry)

            try:
                if geometry['type'] == 'GeometryCollection':
                    # Hay varios features(gralmente linestring)
                    # print("geometrycollection")
                    # print(geometry)
                    features = geometry['geometries']
                    for feature in features:
                        geo_str = str(feature['geometry'])
                        geometry_data = GEOSGeometry(geo_str)
                        obra.ubicacion = geometry_data
                elif geometry['type'] != 'Feature':
                    # Hay una sola geometria
                    geo_str = str(geometry)
                    geometry_data = GEOSGeometry(geo_str)
                    obra.ubicacion = geometry_data
                else:
                    # geometry['type'] == 'Feature'
                    geometry = geometry['geometry']
                    geo_str = str(geometry)
                    geometry_data = GEOSGeometry(geo_str)
                    obra.ubicacion = geometry_data

            except Exception as e:
                self.stdout.write(
                    self.style.ERROR(
                        'GeoJSON Error: {}'.format(geometry)))
                error = 'Obra {}'.format(id_externo)
                errores.append(error)

            # Datos Contratista
            organizacion_str = dato['contratista'].strip()
            if organizacion_str == '':
                organizacion_str = 'Desconocido'

            cuit_str = dato['cuitContratista'].strip()
            # limpio el cuit de caracteres extraños
            cuit_str = cuit_str.replace(
                '-',
                '').replace(
                ' ',
                '').replace(
                '.',
                '').replace(
                ',',
                '')
            if cuit_str == '':
                cuit_str = 'Desconocido'

            try:
                organizacion, created = Organizacion.objects.get_or_create(
                    nombre=organizacion_str, CUIT=cuit_str)
                if created:
                    organizaciones_nuevas += 1
                else:
                    organizaciones_repetidas += 1
            except Organizacion.MultipleObjectsReturned:
                # Fix para cuando la organizacion está duplicada
                organizaciones = Organizacion.objects.filter(
                    nombre=organizacion_str, CUIT=cuit_str)

                max_obras_relacionadas = 0
                organizacion = None
                # por cada organización, obtengo las obras que tiene a su cargo
                for org in organizaciones:
                    cant = len(org.obrapublica_set.all())
                    if cant > max_obras_relacionadas:
                        organizacion = org
                # elijo la organizacion que tiene mas obras
                organizaciones_repetidas += 1

            obra.contratista = organizacion

            obra.monto = 0.0 if dato['monto'] == '' else dato['monto']
            fecha_inicio = datetime.strptime(
                dato['fechaIniciacion'].strip(), "%d/%m/%Y %H:%M")
            fecha_finalizacion_estimada = datetime.strptime(
                dato['fechaTerminacion'].strip(), "%d/%m/%Y %H:%M")

            obra.fecha_inicio = pytz.utc.localize(fecha_inicio)
            obra.fecha_finalizacion_estimada = pytz.utc.localize(
                fecha_finalizacion_estimada)

            # barrio
            barrios_str = dato['barrios']
            barrios_encontrados = []

            # el dato no viene con barrio incluido
            if len(barrios_str) == 0:
                # si la ubicacion es punto, basta con encontrar un barrio
                if isinstance(obra.ubicacion, Point):
                    for poligono in poligonos_barrios:
                        if poligono is not None and poligono.contains(
                                obra.ubicacion):
                            barrios_encontrados.append(
                                Barrio.objects.get(poligono=poligono))
                            break
                # la ubicacion no es un punto, puede ocupar más de un barrio
                else:
                    for poligono in poligonos_barrios:
                        if poligono is not None and poligono.intersection(
                                obra.ubicacion):
                            barrios_encontrados.append(
                                Barrio.objects.get(poligono=poligono))

            # el dato trae por lo menos un barrio
            else:
                for b in barrios_str:
                    match = difflib.get_close_matches(
                        b.upper(), possibilities=barrios, n=1)
                    if match == []:
                        barrio = None
                        for poligono in poligonos_barrios:
                            if poligono is not None and poligono.contains(
                                    obra.ubicacion):
                                barrios_encontrados.append(
                                    Barrio.objects.get(poligono=poligono))
                                break
                    else:
                        barrios_encontrados.append(
                            Barrio.objects.get(nombre=match[0]))
            obra.barrios = barrios_encontrados

            obra.color = dato['color'].strip()

            obra.porcentaje_completado = 0.0 if dato['porcentajeCompletado'] == '' else dato['porcentajeCompletado']
            obra.publicado = True

            if created_obra:  # si la obra viene por primera vez la publico, despues puedo revisar
                obra.publicado_gobierno = True

            estado = normalizar(dato['estadoObra'])
            if estado == normalizar('En ejecucion'):
                obra.estado = ObraDeTerceros.EN_EJECUCION
            elif estado == normalizar('Finalizada'):
                obra.estado = ObraDeTerceros.FINALIZADA

            obra.save()
            self.stdout.write(
                self.style.SUCCESS(
                    'Obra {} de {}'.format(
                        ciclos, obras)))

        if len(errores) > 0:
            self.stdout.write(self.style.SUCCESS(
                'LAS SIGUIENTES OBRAS TUVIERON ALGÚN TIPO DE ERROR:'))
            self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR'))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))
        else:
            self.stdout.write(self.style.SUCCESS('SIN ERRORES'))

        self.stdout.write(self.style.SUCCESS('RESULTADOS:'))
        self.stdout.write(
            self.style.SUCCESS(
                'OBRAS NUEVAS: {}'.format(obras_nuevas)))
        self.stdout.write(
            self.style.SUCCESS(
                'OBRAS REPETIDAS: {}'.format(obras_repetidas)))
        self.stdout.write(
            self.style.SUCCESS(
                'TOTAL DE FOTOS: {}'.format(total_fotos)))

        self.stdout.write(self.style.SUCCESS('FIN'))
