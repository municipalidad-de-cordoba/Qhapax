#!/usr/bin/python
'''
Grabar lista de obras de terceros.
Da timeout si se hace en real time
'''
from django.core.management.base import BaseCommand
from django.conf import settings
import django_excel as excel
from django.db import transaction
import os
from obrasdeterceros.models import ObraDeTerceros


class Command(BaseCommand):
    help = """Generar un CSV/XLS con todas las obras de terceros"""

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS(
            'Comenzando generación de CSV y XLS'))

        obras = ObraDeTerceros.objects.filter(
            publicado=True, publicado_gobierno=True)

        csv_list = []
        csv_list.append(['ID obra',
                         'Obra de tercero',
                         'Decreto',
                         'Expediente',
                         '% completado',
                         'tipo',
                         'Organización',
                         'Fecha inicio',
                         'Monto $',
                         'Fecha finalización estimada',
                         'Barrios'])

        c = 0
        for obra in obras:
            c += 1
            nombre = 'Sin cargar aún' if obra.contratista is None else obra.contratista.nombre
            cuit = '' if obra.contratista is None else obra.contratista.CUIT
            org = '{} {}'.format(nombre, cuit)
            barrios = ''
            for barrio in obra.barrios.all():
                barrios += '{} - '.format(barrio.nombre)

            fecha_inicio = '' if obra.fecha_inicio is None else obra.fecha_inicio.date()
            fecha_fin = '' if obra.fecha_finalizacion_estimada is None else obra.fecha_finalizacion_estimada.date()
            csv_list.append([obra.id,
                             obra.nombre,
                             obra.decreto,
                             obra.expediente_origen,
                             obra.porcentaje_completado,
                             obra.tipo.nombre,
                             org,
                             fecha_inicio,
                             obra.monto,
                             fecha_fin,
                             barrios])
            self.stdout.write(
                self.style.SUCCESS(
                    '{} Obra: {} {} {}'.format(
                        c, obra.nombre, nombre, cuit)))

        dest_csv = os.path.join(
            settings.MEDIA_ROOT,
            'lista-obras-de-terceros.csv')
        dest_xls = os.path.join(
            settings.MEDIA_ROOT,
            'lista-obras-de-terceros.xls')

        excel.pe.save_as(array=csv_list, dest_file_name=dest_csv)
        excel.pe.save_as(array=csv_list, dest_file_name=dest_xls)

        self.stdout.write(
            self.style.SUCCESS(
                'Archivos creados con {} obras'.format(c)))
