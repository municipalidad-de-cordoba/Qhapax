#!/usr/bin/python
"""
Importar trazados de obras de terceros
"""
from django.core.management.base import BaseCommand
from django.db import transaction
import json
import sys
import requests
from obrasdeterceros.settings import (URL_API_TRAZADOS_OBRAS_TERCEROS,
                                      URL_AUTHORIZATION)
from obrasdeterceros.models import (ObraDeTerceros, TrazadoObraDeTercero,
                                    FotoDeTrazadoDeObraDeTercero)
from obras.models import OrigenDatoObra
from django.contrib.gis.geos import GEOSGeometry
from django.core.files import File
import tempfile
from PIL import Image


class Command(BaseCommand):
    help = """Comando para Importar obras de Espacios Verdes en ejecución """

    def add_arguments(self, parser):
        parser.add_argument(
            '--fecha',
            type=str,
            help=('fecha desde la cual se importarán nuevas obras y cambios a',
                  'las existentes. Formato dd/mm/aaaa'),
            default='01/01/2016')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS(
            'Iniciando importación de datos de trazados de obras de espacios verdes'))

        fecha = options['fecha']
        url_base = URL_API_TRAZADOS_OBRAS_TERCEROS

        url = '{}&fecha={}'.format(url_base, fecha)

        self.stdout.write(
            self.style.SUCCESS(
                'Buscando datos en {}'.format(url)))

        headers = {'Authorization': URL_AUTHORIZATION}
        session = requests.Session()
        basedata = session.get(url, headers=headers).content  # es tipo byte
        # basedata = basedata.decode("utf-8")
        basedata = basedata.decode("iso-8859-1")

        try:
            jsondata = json.loads(basedata)
        except Exception as e:
            self.stdout.write(self.style.ERROR('JSON Error: {}'.format(url)))
            self.stdout.write(self.style.ERROR(e))
            sys.exit(1)

        # marcar como el origen que tienen
        origen, created = OrigenDatoObra.objects.get_or_create(
            nombre='MuniDigital.com')
        # despublicar todo para ver que vuelve aquí (y que queden ocultos los
        # que no vengan)
        TrazadoObraDeTercero.objects.filter(
            obra__origen=origen).update(publicado=False)

        trazados = len(jsondata['features'])
        self.stdout.write(
            self.style.SUCCESS(
                'Se obtuvieron {} trazados'.format(trazados)))

        trazados_nuevos = 0
        trazados_repetidos = 0
        errores = []
        total_fotos = 0
        fotos_repetidas = 0
        fotos_nuevas = 0
        ciclos = 0

        for ftrazado in jsondata['features']:
            ciclos += 1
            dato = ftrazado['properties']

            id_obra = dato['id_obra']
            try:
                obra = ObraDeTerceros.objects.get(id_externo=id_obra)
            except ObraDeTerceros.DoesNotExist:
                err = 'NO EXISTE la obra ID:{}'.format(id_obra)
                self.stdout.write(self.style.ERROR(err))
                errores.append(err)
                continue

            id_externo = dato['id_trazado']
            trazado, created_trazado = TrazadoObraDeTercero.objects.get_or_create(
                obra=obra, id_externo=id_externo)
            if created_trazado:
                trazados_nuevos += 1
            else:
                trazados_repetidos += 1
            trazado.estado = dato['estado']

            # fotos de las obras
            self.stdout.write(
                self.style.SUCCESS(
                    'Trazado {}, {} nuevos, {} repetidos. {} fotos, {} nuevas, {} repetidas'.format(
                        ciclos,
                        trazados_nuevos,
                        trazados_repetidos,
                        total_fotos,
                        fotos_nuevas,
                        fotos_repetidas)))
            adjuntos = dato['adjuntos']
            # self.stdout.write(self.style.SUCCESS('Hay {} fotos'.format(len(adjuntos))))
            fotos = 0
            for adjunto in adjuntos:
                foto, created = FotoDeTrazadoDeObraDeTercero.objects.get_or_create(
                    trazado=trazado, id_externo=adjunto['id'])

                foto.url = adjunto['url']
                # el campo paraPublicar tiene un string, se pasa a booleano
                if adjunto['paraPublicar'] == 'SI':
                    foto.publicado = True
                else:
                    foto.publicado = False
                # importo las fotos solo si es la primera vez
                if created:
                    fotos_nuevas += 1
                    fotos += 1
                    # self.stdout.write(self.style.SUCCESS('foto n°: {}'.format(fotos)))
                    try:
                        # self.stdout.write(self.style.SUCCESS('Nueva Foto: {} Trayendo imagen {}'.format(adjunto, foto.url)))
                        url_foto = adjunto['url']
                        contenido = session.get(
                            url_foto).content  # es tipo byte
                    except requests.exceptions.ConnectionError as e:
                        err = 'Connection to {} failed. La imagen no pudo guardarse.'.format(
                            adjunto['url'])
                        self.stdout.write(self.style.ERROR(err))
                        errores.append(err)

                    tf = tempfile.NamedTemporaryFile()
                    tf.write(contenido)

                    img = File(tf)
                    try:
                        # controlamos que el archivo sea una imagen válida
                        img = Image.open(img)
                        if img.size != 0:
                            foto.foto.save(
                                str(foto.url.split('/')[-1]), File(tf))
                    except Exception as e:
                        err = 'ERROR con el formato de la FOTO:{}'.format(
                            foto.url)
                        self.stdout.write(self.style.ERROR(err))
                        errores.append(err)
                        foto.delete()
                        continue

                        foto.save()
                else:
                    # self.stdout.write(self.style.SUCCESS('Omitiendo descargar imagen {}'.format(foto.url)))
                    fotos_repetidas += 1
            total_fotos += fotos

            # datos geoespaciales
            geometry = ftrazado['geometry']
            try:
                geo_str = str(geometry)
                geometry_data = GEOSGeometry(geo_str)
                trazado.trazado = geometry_data
                if trazado.trazado is None:
                    trazado.observaciones_internas = "No está geolocalizado. Consultar con MuniDigital"
            except Exception as e:
                self.stdout.write(
                    self.style.ERROR(
                        'GeoJSON Error: {}'.format(geometry)))
                error = 'Obra {}'.format(id_externo)
                errores.append(error)

            trazado.publicado = True
            if created_trazado:  # si el trazado viene por primera vez la publico, despues puedo revisar
                trazado.publicado_gobierno = True

            trazado.save()
            self.stdout.write(
                self.style.SUCCESS(
                    'Trazado {} de {}'.format(
                        ciclos, trazados)))

        if len(errores) > 0:
            self.stdout.write(self.style.SUCCESS(
                'LOS SIGUIENTES TRAZADOS TUVIERON ALGÚN TIPO DE ERROR:'))
            self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR'))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))
        else:
            self.stdout.write(self.style.SUCCESS('SIN ERRORES'))

        self.stdout.write(self.style.SUCCESS('RESULTADOS:'))
        self.stdout.write(self.style.SUCCESS(
            'TRAZADOS NUEVOS: {}'.format(trazados_nuevos)))
        self.stdout.write(self.style.SUCCESS(
            'TRAZADOS REPETIDOS: {}'.format(trazados_repetidos)))
        self.stdout.write(self.style.SUCCESS(
            'TOTAL DE FOTOS: {}'.format(total_fotos)))

        self.stdout.write(self.style.SUCCESS("FIN"))
