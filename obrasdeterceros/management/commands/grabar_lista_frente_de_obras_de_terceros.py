#!/usr/bin/python
'''
Grabar lista de obras de terceros.
Da timeout si se hace en real time
'''
from django.core.management.base import BaseCommand
from django.conf import settings
import django_excel as excel
from django.db import transaction
import os
from obrasdeterceros.models import ObraDeTerceros, TrazadoObraDeTercero


class Command(BaseCommand):
    help = """Generar un CSV/XLS con todas las obras de terceros"""

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS(
            'Comenzando generación de CSV y XLS'))

        obras = ObraDeTerceros.objects.filter(
            publicado=True, publicado_gobierno=True)

        csv_list = []
        csv_list.append(['ID Obra', 'Obra de tercero',
                         'ID frente', 'Descripción frente de obra'])

        c1 = 0
        c2 = 0
        for obra in obras:
            c1 += 1
            nombre = 'Sin cargar aún' if obra.contratista is None else obra.contratista.nombre
            cuit = '' if obra.contratista is None else obra.contratista.CUIT
            org = '{} {}'.format(nombre, cuit)

            trazados = TrazadoObraDeTercero.objects.filter(
                obra=obra, publicado=True, publicado_gobierno=True)

            for trazado in trazados:
                c2 += 1
                if trazado.descripcion_publica is not None:
                    descripcion = trazado.descripcion_publica
                else:
                    descripcion = 'Trazado {}-{}. Sin descripción aún '.format(
                        trazado.id, trazado.id_externo)

                csv_list.append(
                    [obra.id, obra.nombre, trazado.id, descripcion])

                self.stdout.write(
                    self.style.SUCCESS(
                        '{} {} Obra: {} {} {}'.format(
                            c1, c2, obra.nombre, nombre, cuit)))

        dest_csv = os.path.join(
            settings.MEDIA_ROOT,
            'lista-frentes-obras-de-terceros.csv')
        dest_xls = os.path.join(
            settings.MEDIA_ROOT,
            'lista-frentes-obras-de-terceros.xls')

        excel.pe.save_as(array=csv_list, dest_file_name=dest_csv)
        excel.pe.save_as(array=csv_list, dest_file_name=dest_xls)

        self.stdout.write(
            self.style.SUCCESS(
                'Archivos creados con {} obras y {} trazados'.format(
                    c1, c2)))
