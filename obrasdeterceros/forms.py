from django_select2.forms import ModelSelect2Widget
from .models import ObraDeTerceros, TrazadoObraDeTercero
from django import forms


class ObraDeTercerosWidget(ModelSelect2Widget):
    model = ObraDeTerceros
    search_fields = ['contratista__nombre__icontains',
                     'contratista__CUIT__icontains']

    # def label_from_instance(self, obj):
    #     return '{} : {}'.format(obj.contratista.nombre, obj.contratista.CUIT)

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault(
            'data-placeholder',
            'Ingrese parte del CUIT o nombre')
        self.attrs.setdefault('data-minimum-input-length', 3)
        self.attrs.setdefault('data-width', '45em')

        return super(ObraDeTercerosWidget, self).build_attrs(*args, **kwargs)


class ObraDeTercerosWidgetParaTrazado(ModelSelect2Widget):
    model = ObraDeTerceros
    search_fields = ['nombre__icontains']

    # def label_from_instance(self, obj):
    #     return '{} : {}'.format(obj.contratista.nombre, obj.contratista.CUIT)

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault(
            'data-placeholder',
            'Ingrese nombre o parte del nombre')
        self.attrs.setdefault('data-minimum-input-length', 3)
        self.attrs.setdefault('data-width', '45em')

        return super(
            ObraDeTercerosWidgetParaTrazado,
            self).build_attrs(
            *args,
            **kwargs)


class ObraDeTercerosFormSelect2(forms.ModelForm):
    class Meta:
        model = ObraDeTerceros

        fields = '__all__'
        widgets = {'contratista': ObraDeTercerosWidget}


class TrazadoObraDeTercerosFormSelect2(forms.ModelForm):
    class Meta:
        model = TrazadoObraDeTercero

        fields = '__all__'
        widgets = {'obra': ObraDeTercerosWidgetParaTrazado}
