from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import (ObraDeTercerosSerializer,
                          TrazadoObraDeTerceroSerializer,
                          ContratistaObraDeTercerosSerializer,
                          GroupObraDeTercerosSerializer)
from api.pagination import DefaultPagination
from obrasdeterceros.models import ObraDeTerceros, TrazadoObraDeTercero
from barrios.models import Barrio
from django.db.models import Count
from core.utils import normalizar
from django.contrib.gis.db.models.functions import GeoHash, Centroid
from rest_framework_gis.filters import InBBoxFilter
from organizaciones.models import Organizacion
from obras.api.serializers import BarrioObraSerializer


class BarrioObrasTercerosViewSet(viewsets.ModelViewSet):
    """
    Barrios de obras de terceros.
    """
    serializer_class = BarrioObraSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Barrio.objects.filter(
            obradeterceros__isnull=False).distinct().order_by('nombre')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ObraDeTercerosViewSet(viewsets.ModelViewSet):
    """
    Lista de obras de terceros geolocalizadas con sus datos y trazados
    Puede filtrarse por:
     - tipo_id (identificador del tipo de obra), pueden ser varios separados por comas
     - avance_desde (entero vinculado al campo "porcentaje_completado")
     - avance_hasta (entero vinculado al campo "porcentaje_completado")
     - barrios_ids: lista de barrios separados por comas
     - texto: filtra aquellas obras que contienen el texto indicado en el nombre de la obra o
      en el nombre de la org
     - estado: estado de avance de la obra. 2 posibles opciones(son valores númericos):
        "6: EN EJECUCION"
        "8: FINALIZADA"
        Pueden ser más de un valor.
    - empresa_id: filtra las obras por empresas que las llevan a cabo. Pueden ser varios ids
        separados por comas.
     - in_bbox = se usa para buscar árboles dentro de un Bounding Box. Formato
        de parámetros (min Lon, min Lat, max Lon, max Lat). Ejemplo:
        in_bbox=-64.19215920250672,-31.413600429103756,-64.19254544060477,-31.41472664471516
    - precision = valor de 5 a 10 para clusterizar los puntos. Si se usa este parametro
        entonces el resultado serán puntos con un contador de obras, sin propiedades de
        cada obra. Tamaño del cluster según valor:
        5 4.89km × 4.89km
        6 1.22km × 0.61km
        7 153m × 153m
        8 38.2m × 19.1m
        9 4.77m × 4.77m
        10 1.19m × 0.596m
    """
    serializer_class = ObraDeTercerosSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    bbox_filter_field = 'ubicacion'
    filter_backends = (InBBoxFilter, )

    def get_queryset(self):
        queryset = ObraDeTerceros.objects.filter(
            publicado=True, publicado_gobierno=True).exclude(
            ubicacion__isnull=True)

        tipos_id_str = self.request.query_params.get('tipo_id', None)
        if tipos_id_str is not None:
            tipos_id = tipos_id_str.split(',')
            queryset = queryset.filter(tipo__id__in=tipos_id)

        avance_desde = self.request.query_params.get('avance_desde', None)
        if avance_desde is not None:
            queryset = queryset.filter(
                porcentaje_completado__gte=int(avance_desde))

        avance_hasta = self.request.query_params.get('avance_hasta', None)
        if avance_hasta is not None:
            queryset = queryset.filter(
                porcentaje_completado__lte=int(avance_hasta))

        barrios_str = self.request.query_params.get('barrios_ids', None)
        if barrios_str is not None:
            barrios_ids = barrios_str.split(',')
            queryset = queryset.filter(barrios__in=barrios_ids)

        texto = self.request.query_params.get('texto', None)
        if texto is not None:
            # FIXME: acá se busca el texto que quiere el usuario, por más que
            # en la bd tenga acento lo mismo lo encuentra.
            # Si Django trae algo predefinido, usarlo acá
            copia_queryset = queryset.values(
                'id', 'nombre', 'contratista__nombre', 'descripcion_publica')
            list_ids = []
            texto_norm = normalizar(texto)
            for obra in copia_queryset:
                if texto_norm in normalizar(
                        obra['nombre']) or texto_norm in normalizar(
                        obra['contratista__nombre']) or texto_norm in normalizar(
                        obra['descripcion_publica']):
                    list_ids.append(obra['id'])
            queryset = queryset.filter(id__in=list_ids)

        # Filtro por estado
        estado = self.request.query_params.get('estado', None)
        if estado is not None:
            estados = estado.split(',')
            queryset = queryset.filter(estado__in=estados)

        empresas = self.request.query_params.get('empresa_id', None)
        if empresas is not None:
            empresas_ids = empresas.split(',')
            queryset = queryset.filter(contratista__id__in=empresas_ids)

        precision_str = self.request.query_params.get('precision', None)
        if precision_str:
            # trabajo unicamente con las obras que son puntos
            # queryset = queryset.extra(where=["geometrytype(ubicacion) LIKE 'POINT'"])
            precision = int(precision_str)
            # en este caso el serializador no tiene todos los campos
            self.serializer_class = GroupObraDeTercerosSerializer
            anot = GeoHash(Centroid('ubicacion'), precision=precision)
            queryset = queryset.annotate(
                ubicacion_geohash=anot).exclude(
                ubicacion_geohash__isnull=True).values('ubicacion_geohash').annotate(
                cluster_count=Count('ubicacion_geohash')).exclude(
                cluster_count=0)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class TrazadoObraDeTercerosViewSet(viewsets.ModelViewSet):
    """
    Trazados de obras de terceros
    Puede fitrarse:
     - solo para una obra con el parámetro id_obra
     - por tipo_id (identificador del tipo de obra), pueden ser varios separados por comas
     - obra_avance_desde (entero vinculado al campo "porcentaje_completado")
     - obra_avance_hasta (entero vinculado al campo "porcentaje_completado")
     - barrios_ids: lista de barrios separados por comas
     - texto: filtra aquellos frentes de obras que contienen el texto indicado
        en la descripcion del frente o en el nombre de la obra
     - estado: estado de avance de la obra. 9 posibles opciones(son valores númericos):
        "1: EN PROYECTO"
        "2: LICITADAS"
        "3: ADJUDICADA"
        "4: CONTRATADA"
        "5: REPLANTEADA"
        "6: EN EJECUCION"
        "7: EN EJECUCION POR AMPLIACION"
        "8: FINALIZADA"
        "9: RESCINDIDA"
        Pueden ser más de un valor.
    """

    serializer_class = TrazadoObraDeTerceroSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):

        queryset = TrazadoObraDeTercero.objects.filter(
            obra__publicado=True,
            obra__publicado_gobierno=True,
            publicado=True,
            publicado_gobierno=True).order_by('obra')

        id_obra = self.request.query_params.get('id_obra', None)
        if id_obra is not None:
            queryset = queryset.filter(obra__id=int(id_obra))

        tipos_id_str = self.request.query_params.get('tipo_id', None)
        if tipos_id_str is not None:
            tipos_id = tipos_id_str.split(',')
            queryset = queryset.filter(obra__tipo__id__in=tipos_id)

        obra_avance_desde = self.request.query_params.get(
            'obra_avance_desde', None)
        if obra_avance_desde is not None:
            queryset = queryset.filter(
                obra__porcentaje_completado__gte=int(obra_avance_desde))

        obra_avance_hasta = self.request.query_params.get(
            'obra_avance_hasta', None)
        if obra_avance_hasta is not None:
            queryset = queryset.filter(
                obra__porcentaje_completado__lte=int(obra_avance_hasta))

        barrios_str = self.request.query_params.get('barrios_ids', None)
        if barrios_str is not None:
            barrios_ids = barrios_str.split(',')
            queryset = queryset.filter(obra__barrios__in=barrios_ids)

        texto = self.request.query_params.get('texto', None)
        if texto is not None:
            # FIXME: acá se busca el texto que quiere el usuario, por más que
            # en la bd tenga acento lo mismo lo encuentra.
            # Si Django trae algo predefinido, usarlo acá
            copia_queryset = queryset.values(
                'id', 'obra__nombre', 'descripcion_publica')
            list_ids = []
            for trazado in copia_queryset:
                if normalizar(texto) in (
                    normalizar(
                        trazado['obra__nombre']) or normalizar(
                        trazado['descripcion_publica'])):
                    list_ids.append(trazado['id'])
            queryset = queryset.filter(id__in=list_ids)

        # Filtro por estado
        estado = self.request.query_params.get('estado', None)
        if estado is not None:
            estados = estado.split(',')
            queryset = queryset.filter(obra__estado__in=estados)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ContratistaObrasTercerosViewSet(viewsets.ModelViewSet):
    """
    Empresas que realizan obras en la ciudad por cuenta propia.
    """
    serializer_class = ContratistaObraDeTercerosSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Organizacion.objects.filter(
            obradeterceros__isnull=False).distinct().order_by('nombre')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
