from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from rest_framework_gis.serializers import (GeoFeatureModelSerializer,
                                            GeometrySerializerMethodField)
from versatileimagefield.serializers import VersatileImageFieldSerializer
import geohash
from obrasdeterceros.models import (ObraDeTerceros, TrazadoObraDeTercero,
                                    FotoDeObraDeTercero,
                                    FotoDeTrazadoDeObraDeTercero)
from barrios.models import Barrio
from organizaciones.models import Organizacion
from django.contrib.gis.geos import Point


class ContratistaObraDeTercerosSerializer(CachedSerializerMixin):

    class Meta:
        model = Organizacion
        fields = ('id', 'nombre', 'CUIT')


class BarrioObraDeTercerosSerializer(CachedSerializerMixin):

    class Meta:
        model = Barrio
        fields = ('id', 'nombre')


class FotoDeObraDeTerceroSerializer(CachedSerializerMixin):
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail_125", 'thumbnail__125x125'),
                                          ("thumbnail_500", 'thumbnail__500x500')])

    class Meta:
        model = FotoDeObraDeTercero
        fields = (
            'id',
            'id_externo',
            'publicado',
            'publicado_gobierno',
            'foto')


class FotoDeTrazadoDeObraDeTerceroSerializer(CachedSerializerMixin):
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail_125", 'thumbnail__125x125'),
                                          ("thumbnail_500", 'thumbnail__500x500')])

    class Meta:
        model = FotoDeTrazadoDeObraDeTercero
        fields = (
            'id',
            'id_externo',
            'publicado',
            'publicado_gobierno',
            'foto')


class TrazadoObraDeTerceroSerializer(
        GeoFeatureModelSerializer,
        CachedSerializerMixin):
    adjuntos = FotoDeTrazadoDeObraDeTerceroSerializer(many=True)
    obra = serializers.CharField(source='obra.nombre')
    id_obra = serializers.CharField(source='obra.id')
    tipo = serializers.StringRelatedField(
        source='obra.tipo.nombre', read_only=True)
    tipo_id = serializers.StringRelatedField(
        source='obra.tipo.id', read_only=True)
    descripcion_frente = serializers.CharField(source='descripcion_publica')

    class Meta:
        model = TrazadoObraDeTercero
        geo_field = "trazado"
        fields = (
            'id',
            'descripcion_frente',
            'obra',
            'id_obra',
            'tipo',
            'tipo_id',
            'adjuntos')


class TrazadoObraDeTerceroSinGeoSerializer(CachedSerializerMixin):
    # adjuntos = serializers.SerializerMethodField('get_fotos')
    descripcion_frente = serializers.CharField(source='descripcion_publica')

    # def get_fotos(self, container):
    #     fotos = FotoDeObraDeTercero.objects.filter(trazado=container, publicado=True, publicado_gobierno=True)
    #     serializer = FotoDeObraDeTerceroSerializer(instance=fotos, many=True)

    class Meta:
        model = TrazadoObraDeTercero
        fields = ('id', 'descripcion_frente', 'adjuntos')


class GroupObraDeTercerosSerializer(
        GeoFeatureModelSerializer,
        CachedSerializerMixin):

    cluster_count = serializers.IntegerField()
    punto_agrupador = GeometrySerializerMethodField()
    ubicacion_geohash = serializers.CharField()
    # centroide = serializers.CharField()

    def get_punto_agrupador(self, obj):
        # return obj['ubicacion'].centroid if obj['ubicacion'] is not None
        # else Point(0, 0)
        """Parse a geohashed point to Geometry."""
        y, x = geohash.decode(obj['ubicacion_geohash'])
        return Point(x, y)

    class Meta:
        model = ObraDeTerceros
        geo_field = "punto_agrupador"
        fields = ('ubicacion_geohash', 'cluster_count')


class ObraDeTercerosSerializer(
        GeoFeatureModelSerializer,
        CachedSerializerMixin):
    adjuntos = FotoDeObraDeTerceroSerializer(many=True)
    tipo = serializers.CharField(source='tipo.nombre')
    tipo_id = serializers.CharField(source='tipo.id')
    organizacion = serializers.CharField(source='contratista.nombre')
    CUIT = serializers.CharField(source='contratista.CUIT')
    trazados = TrazadoObraDeTerceroSinGeoSerializer(many=True, read_only=True)
    barrios = BarrioObraDeTercerosSerializer(many=True, read_only=True)
    estado = serializers.SerializerMethodField()
    porcentaje_obras_completadas = serializers.SerializerMethodField()

    def get_estado(self, obj):
        return obj.get_estado_display()

    def get_porcentaje_obras_completadas(self, obj):
        # se obtiene el porcentaje del total de obras y no del queryset
        porcentaje_completado = ObraDeTerceros.objects.porcentaje_total_finalizadas()
        return porcentaje_completado

    class Meta:
        model = ObraDeTerceros
        geo_field = "ubicacion"
        auto_bbox = True
        fields = ('id', 'nombre', 'decreto', 'estado', 'expediente_origen',
                  'descripcion_publica', 'porcentaje_completado', 'tipo',
                  'tipo_id', 'organizacion', 'CUIT', 'fecha_inicio', 'monto',
                  'fecha_finalizacion_estimada', 'trazados', 'barrios',
                  'resumen', 'adjuntos', 'porcentaje_obras_completadas')


# Registro los serializadores en la cache de DRF
cache_registry.register(ObraDeTercerosSerializer)
cache_registry.register(TrazadoObraDeTerceroSerializer)
cache_registry.register(TrazadoObraDeTerceroSinGeoSerializer)
cache_registry.register(FotoDeObraDeTerceroSerializer)
