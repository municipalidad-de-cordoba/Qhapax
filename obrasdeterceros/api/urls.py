from django.conf.urls import url, include
from rest_framework import routers
from .views import (ObraDeTercerosViewSet, TrazadoObraDeTercerosViewSet,
                    BarrioObrasTercerosViewSet, ContratistaObrasTercerosViewSet)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register('obras-de-terceros', ObraDeTercerosViewSet,
                base_name='obras-de-terceros.api.lista')
router.register('frentes-obras-de-terceros', TrazadoObraDeTercerosViewSet,
                base_name='frentes-obras-de-terceros.api.lista')
router.register('barrios', BarrioObrasTercerosViewSet,
                base_name='barrios-obras-de-terceros.api.lista')
router.register('empresas', ContratistaObrasTercerosViewSet,
                base_name='empresas-obras-de-terceros.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
