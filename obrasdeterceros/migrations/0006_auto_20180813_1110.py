# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-08-13 14:10
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('obrasdeterceros', '0005_auto_20180810_1604'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='trazadoobradetercero',
            options={'verbose_name': 'Trazado de Obra de Terceros', 'verbose_name_plural': 'Trazado de Obras de Terceros'},
        ),
    ]
