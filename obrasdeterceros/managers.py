from django.db import models
# from django.db.models import Max, Min


class ObraDeTercerosManager(models.Manager):
    """
    Datos específicosd de obras de terceros
    """

    def porcentaje_total_finalizadas(self):
        """
        Porcentaje de obras finalizadas.
        Retorna un valor tipo float
        """
        total_obras = self.all().count()
        if total_obras == 0:
            total_obras = 1  # evitar el error de division por cero

        obras_finalizadas = self.filter(estado__in=[8, 9]).count()

        return (obras_finalizadas / total_obras) * 100
