from django.db import models
from obras.models import Obra
from obrasdeterceros.managers import ObraDeTercerosManager
from versatileimagefield.fields import VersatileImageField, PPOIField
from django.contrib.gis.db import models as models_geo


class ObraDeTerceros(Obra):
    """
    Hereda de Obra, por ahora alcanza con los atributos definidos en ese modelo
    """
    objects = ObraDeTercerosManager()

    class Meta:
        verbose_name = 'Obra de Tercero'
        verbose_name_plural = 'Obras de Terceros'
        # ordering = ['nombre']


class TrazadoObraDeTercero(models.Model):
    """
    Info geo de cada parte de la obra
    """
    obra = models.ForeignKey(ObraDeTerceros, related_name='trazados')

    id_externo = models.PositiveIntegerField(default=0)
    descripcion_publica = models.TextField(null=True, blank=True)

    # campo generico espacial, puede contener puntos, líneas o polígonos
    trazado = models_geo.GeometryField(null=True)

    observaciones_internas = models.TextField(null=True, blank=True)
    publicado = models.BooleanField(default=True)
    publicado_gobierno = models.BooleanField(
        default=True, help_text='Campo para editar y aprobar localmente')

    def __str__(self):
        return '{} {}'.format(self.obra.nombre, self.descripcion_publica)

    class Meta:
        # ordering = ['obra__nombre', 'descripcion_publica']
        verbose_name = 'Trazado de Obra de Terceros'
        verbose_name_plural = 'Trazado de Obras de Terceros'


class FotoDeObraDeTercero(models.Model):
    """
    Fotos de Obras de Terceros
    """
    obra = models.ForeignKey(ObraDeTerceros, related_name='adjuntos')

    id_externo = models.PositiveIntegerField(default=0)
    url = models.CharField(max_length=320, null=True, blank=True)
    publicado = models.BooleanField(default=True)
    publicado_gobierno = models.BooleanField(
        default=True, help_text='Campo para editar y aprobar localmente')

    foto = VersatileImageField(
        upload_to='imagenes/obrasdeterceros',
        ppoi_field='foto_ppoi',
        null=True,
        blank=True,
        max_length=200
    )
    foto_ppoi = PPOIField('Image PPOI')

    def __str__(self):
        return "Foto {} {}".format(self.id, self.id_externo)

    class Meta:
        verbose_name = 'Foto de Obra de Tercero'
        verbose_name_plural = 'Fotos de Obras de Terceros'


class FotoDeTrazadoDeObraDeTercero(models.Model):
    """
    Fotos de trazados de obras de Terceros
    """
    trazado = models.ForeignKey(TrazadoObraDeTercero, related_name='adjuntos')

    id_externo = models.PositiveIntegerField(default=0)
    url = models.CharField(max_length=320, null=True, blank=True)
    publicado = models.BooleanField(default=True)
    publicado_gobierno = models.BooleanField(
        default=True, help_text='Campo para editar y aprobar localmente')

    foto = VersatileImageField(
        upload_to='imagenes/trazados_obrasdeterceros',
        ppoi_field='foto_ppoi',
        null=True,
        blank=True,
        max_length=200
    )
    foto_ppoi = PPOIField('Image PPOI')

    def __str__(self):
        return "Foto {} {}".format(self.id, self.id_externo)

    class Meta:
        verbose_name = 'Foto de Trazado de Obra de Tercero'
        verbose_name_plural = 'Foto de Trazados de Obras de Terceros'
