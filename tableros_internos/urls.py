from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^tablero-(?P<slug>[\w-]+)/(?P<vista>[\w-]+)$', views.tablero_interno, name='tablero-interno'),
    url(r'^mapa-alumbrado(?:/(?P<dias>[0-9]+))?/$', views.mapa_147_alumbrado, name='mapa-147-alumbrado'),
    url(r'^mapa-residuos(?:/(?P<dias>[0-9]+))?/$', views.mapa_147_residuos, name='mapa-147-residuos'),
    url(r'^mapa-baches(?:/(?P<dias>[0-9]+))?/$', views.mapa_147_baches, name='mapa-147-baches'),
    url(r'^mapa-cloacas(?:/(?P<dias>[0-9]+))?/$', views.mapa_147_cloacas, name='mapa-147-cloacas'),
    ]