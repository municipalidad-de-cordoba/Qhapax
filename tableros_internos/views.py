from django.shortcuts import render
from .models import TableroInterno, PaginaEnTablero, PaginaDeTablero
from django.views.decorators.cache import cache_page
import logging
logger = logging.getLogger(__name__)
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import permission_required
from .settings import *
import requests
import json
import datetime


@cache_page(5 * 60)  # 1 h
# @permission_required('tableros_internos.ver_tableros')
def tablero_interno(request, slug, vista='slides'):
    """ ver un tablero interno (van girando sus páginas con datos) """
    tablero = TableroInterno.objects.get(slug=slug, publicado=True)

    # secciones del sitio web para el menu. Solo las principales (no son subsecciones de otras)
    paginas = PaginaEnTablero.objects.filter(tablero=tablero, pagina__publicado=True).order_by('orden')

    # se puede ver como slides o solo los 4 primeros a pantalla dividida
    if vista == 'slides':
        url = 'tableros_internos/tablero_interno.html'
    elif vista == 'cuatro':
        paginas = paginas[:4]
        url = 'tableros_internos/tablero_interno_x4.html'
    elif vista == 'cordobus':
        paginas = paginas[:4]
        url = 'tableros_internos/tablero_interno_x4.html'

    context = {'paginas': paginas, 'tablero': tablero}
    
    return render(request, url, context)

@cache_page(5 * 60)  # 1 h
def mapa_147_alumbrado(request, dias=None):
    """
    Mapa para ver los reclamos geolocalizados
    """
    url_api = URL_API_147
    token = TOKEN
    passw = CLAVE

    headers = {'Clave': passw,
                'Token': token,
                "Content-Type" : "application/json",
                "Accept" :"application/json"
            }

    if dias is None:
        # idsArea, idsServicio, idsMotivo, keyValuesEstado
        body = """{idsServicio: [13], keyValuesEstado: [1, 2, 4]}"""
    else:
        fecha_ahora = datetime.datetime.now()
        fecha_str = fecha_ahora.strftime('%Y-%m-%dT%H:%M:%SZ')
        fecha_desde = fecha_ahora - datetime.timedelta(days=int(dias))
        fecha_desde_str = fecha_desde.strftime('%Y-%m-%dT%H:%M:%SZ')

        body = '{keyValuesEstado: [1, 2, 4], idsServicio: [13], '+'"fechaDesde": "{}", "fechaHasta": "{}"'.format(fecha_desde_str, fecha_str)+'}'

    basedata = requests.put(url_api, headers=headers, data=body)

    try:
        basedata = basedata.content
        # basedata = basedata.decode("iso-8859-1")
        basedata = basedata.decode("utf-8")

    except Exception as e:
        print(e)

    jsondata = json.loads(basedata)

    url = 'tableros_internos/mapa_147_alumbrado.html'
    context = {'json': jsondata['return'], 'reclamos': len(jsondata['return']['coordinates']),
                'titulo': 'Requerimientos de ALUMBRADO'}

    return render(request, url, context)

@cache_page(5 * 60)  # 1 h
def mapa_147_residuos(request, dias=None):
    """
    Mapa para ver los reclamos geolocalizados
    """
    url_api = URL_API_147
    token = TOKEN
    passw = CLAVE

    headers = {'Clave': passw,
                'Token': token,
                "Content-Type" : "application/json",
                "Accept" :"application/json"
            }

    if dias is None:
        # idsArea, idsServicio, idsMotivo, keyValuesEstado
        # lusa: 1380 1381 1382
        body = """{idsArea: [1380, 1381, 1382], keyValuesEstado: [1, 2, 4]}"""
    else:
        fecha_ahora = datetime.datetime.now()
        fecha_str = fecha_ahora.strftime('%Y-%m-%dT%H:%M:%SZ')
        fecha_desde = fecha_ahora - datetime.timedelta(days=int(dias))
        fecha_desde_str = fecha_desde.strftime('%Y-%m-%dT%H:%M:%SZ')

        body = '{keyValuesEstado: [1, 2, 4], idsArea: [1380, 1381, 1382], '+'"fechaDesde": "{}", "fechaHasta": "{}"'.format(fecha_desde_str, fecha_str)+'}'

    basedata = requests.put(url_api, headers=headers, data=body)

    try:
        basedata = basedata.content
        # basedata = basedata.decode("iso-8859-1")
        basedata = basedata.decode("utf-8")

    except Exception as e:
        print(e)

    jsondata = json.loads(basedata)

    url = 'tableros_internos/mapa_147_residuos.html'
    context = {'json': jsondata['return'], 'reclamos': len(jsondata['return']['coordinates']),
                'titulo': 'Requerimientos de RESIDUOS'}

    return render(request, url, context)

@cache_page(5 * 60)  # 1 h
def mapa_147_baches(request, dias=None):
    """
    Mapa para ver los reclamos geolocalizados
    """
    url_api = URL_API_147
    token = TOKEN
    passw = CLAVE

    headers = {'Clave': passw,
                'Token': token,
                "Content-Type" : "application/json",
                "Accept" :"application/json"
            }

    if dias is None:
        # idsArea, idsServicio, idsMotivo, keyValuesEstado
        body = """{keyValuesEstado: [1, 2, 4], idsMotivo: [188]}"""
    else:
        fecha_ahora = datetime.datetime.now()
        fecha_str = fecha_ahora.strftime('%Y-%m-%dT%H:%M:%SZ')
        fecha_desde = fecha_ahora - datetime.timedelta(days=int(dias))
        fecha_desde_str = fecha_desde.strftime('%Y-%m-%dT%H:%M:%SZ')

        body = '{keyValuesEstado: [1, 2, 4], idsMotivo: [188], '+'"fechaDesde": "{}", "fechaHasta": "{}"'.format(fecha_desde_str, fecha_str)+'}'

    basedata = requests.put(url_api, headers=headers, data=body)

    try:
        basedata = basedata.content
        # basedata = basedata.decode("iso-8859-1")
        basedata = basedata.decode("utf-8")

    except Exception as e:
        print(e)

    jsondata = json.loads(basedata)

    url = 'tableros_internos/mapa_147_baches.html'
    context = {'json': jsondata['return'], 'reclamos': len(jsondata['return']['coordinates']),
                'titulo': 'Requerimientos de BACHES'}

    return render(request, url, context)

@cache_page(5 * 60)  # 1 h
def mapa_147_cloacas(request, dias=None):
    """
    Mapa para ver los reclamos geolocalizados
    """
    url_api = URL_API_147
    token = TOKEN
    passw = CLAVE

    headers = {'Clave': passw,
                'Token': token,
                "Content-Type" : "application/json",
                "Accept" :"application/json"
            }

    if dias is None:
        body = '{keyValuesEstado: [1, 2, 4], idsMotivo: [273, 192]}'
    else:
        fecha_ahora = datetime.datetime.now()
        fecha_str = fecha_ahora.strftime('%Y-%m-%dT%H:%M:%SZ')
        fecha_desde = fecha_ahora - datetime.timedelta(days=int(dias))
        fecha_desde_str = fecha_desde.strftime('%Y-%m-%dT%H:%M:%SZ')

        # idsArea, idsServicio, idsMotivo, keyValuesEstado
        body = '{keyValuesEstado: [1, 2, 4], idsMotivo: [273, 192], '+'"fechaDesde": "{}", "fechaHasta": "{}"'.format(fecha_desde_str, fecha_str)+'}'

    basedata = requests.put(url_api, headers=headers, data=body)
    try:
        basedata = basedata.content
        # basedata = basedata.decode("iso-8859-1")
        basedata = basedata.decode("utf-8")

    except Exception as e:
        print(e)

    jsondata = json.loads(basedata)

    url = 'tableros_internos/mapa_147_cloacas.html'
    context = {'json': jsondata['return'], 'reclamos': len(jsondata['return']['coordinates']),
                'titulo': 'Requerimientos de CLOACAS'}

    return render(request, url, context)
