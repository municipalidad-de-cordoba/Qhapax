from django.db import models
from versatileimagefield.fields import VersatileImageField, PPOIField


class PaginaDeTablero(models.Model):
    ''' cada una de las páginas incluidas en un tablero, solo muestra un iframe embebido '''
    nombre = models.CharField(max_length=90)
    iframe_url = models.URLField(null=True, blank=True)
    publicado = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.nombre


class TableroInterno(models.Model):
    ''' Tablero general, conjunto de tableros específicos '''
    nombre = models.CharField(max_length=90)
    descripcion = models.TextField()
    slug = models.CharField(max_length=90, unique=True)
    publicado = models.BooleanField(default=True)
    paginas = models.ManyToManyField(PaginaDeTablero, through='PaginaEnTablero')

    favicon = VersatileImageField(
        upload_to='tableros_internos/favicons',
        ppoi_field='foto_ppoi',
        null=True,
        blank=True,
        max_length=200,
        help_text='Imagen para usar como favicon para cada tablero en particular. Tamaño mínimo: 16x16 px.'
    )
    foto_ppoi = PPOIField('Image PPOI')

    def __str__(self):
        return self.nombre

    class Meta:
        permissions = (('ver_tableros', 'Puede ver tableros internos de gestión'), )


class PaginaEnTablero(models.Model):
    ''' cada pagina dentro de los tableros donde se usa '''
    tablero = models.ForeignKey(TableroInterno, on_delete=models.CASCADE)
    pagina = models.ForeignKey(PaginaDeTablero, on_delete=models.CASCADE)
    orden = models.PositiveIntegerField(default=1000)

    def __str__(self):
        return '{} {} '.format(self.tablero, self.pagina)
