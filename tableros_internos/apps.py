from django.apps import AppConfig


class TablerosInternosConfig(AppConfig):
    name = 'tableros_internos'
