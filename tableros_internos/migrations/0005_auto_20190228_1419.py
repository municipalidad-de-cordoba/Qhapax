# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2019-02-28 17:19
from __future__ import unicode_literals

from django.db import migrations
import versatileimagefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('tableros_internos', '0004_paginadetablero_publicado'),
    ]

    operations = [
        migrations.AddField(
            model_name='tablerointerno',
            name='favicon',
            field=versatileimagefield.fields.VersatileImageField(blank=True, help_text='Imagen para usar como favicon para cada tablero en particular. Tamaño mínimo: 16x16 px.', max_length=200, null=True, upload_to='tableros_internos/favicons'),
        ),
        migrations.AddField(
            model_name='tablerointerno',
            name='foto_ppoi',
            field=versatileimagefield.fields.PPOIField(default='0.5x0.5', editable=False, max_length=20, verbose_name='Image PPOI'),
        ),
    ]
