from django.contrib import admin
from .models import TableroInterno, PaginaDeTablero, PaginaEnTablero


class PaginaEnTableroInline(admin.StackedInline):
    model = PaginaEnTablero
    extra = 1


@admin.register(TableroInterno)
class TableroInternoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'slug', 'publicado', 'favicon')
    inlines = [PaginaEnTableroInline]


@admin.register(PaginaDeTablero)
class PaginaDeTableroAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'publicado', 'iframe_url')
