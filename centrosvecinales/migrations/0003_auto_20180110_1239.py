# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-10 15:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('centrosvecinales', '0002_auto_20180103_1107'),
    ]

    operations = [
        migrations.AddField(
            model_name='balancecentrovecinal',
            name='publicado',
            field=models.BooleanField(default=False, help_text='Indica si ya fuechequeado por la persona encargada depublicar los centros vecinales'),
        ),
        migrations.AddField(
            model_name='centrovecinal',
            name='observaciones_internas',
            field=models.TextField(blank=True, help_text='Guarda las columnnasdel excel que no fueronutilizadas en la base de datos', null=True),
        ),
        migrations.AddField(
            model_name='centrovecinal',
            name='publicado',
            field=models.BooleanField(default=False, help_text='Indica si ya fuechequeado por la persona encargada depublicar los centros vecinales'),
        ),
    ]
