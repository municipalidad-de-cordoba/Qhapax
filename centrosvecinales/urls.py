from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^lista-centros-vecinales.(?P<filetype>csv|xls)$',
        views.lista_centros_vecinales, name='centros_vecinales.lista'),
]
