from django.contrib.gis.db import models
from barrios.models import Barrio
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from datetime import date


class CentroVecinal(models.Model):
    ''' Forma de organización administrativa para '''
    nombre = models.CharField(max_length=120)
    descripcion = models.TextField(null=True, blank=True)
    barrio_de_referencia = models.ForeignKey(Barrio, null=True, blank=True)
    fecha_de_oficializacion = models.DateField(null=True, blank=True)
    horario_atencion = models.TimeField(null=True, blank=True)
    poligono = models.PolygonField(null=True, blank=True)
    publicado = models.BooleanField(
        default=False, help_text="Indica si ya fue "
        "chequeado por la persona encargada de "
        "publicar los centros vecinales")
    observaciones_internas = models.TextField(null=True, blank=True,
                                              help_text="Columnnas del archivo"
                                              " excel que no fueron "
                                              "utilizadas en la base de datos")

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        """
        Se sobreescribe para que tome como poligono por defecto el poligono del
        barrio de referencia
        """
        if self.barrio_de_referencia is not None:
            barrio = Barrio.objects.get(id=self.barrio_de_referencia.id)
            if self.poligono is None:
                self.poligono = barrio.poligono
        super(CentroVecinal, self).save(*args, **kwargs)

    class Meta:
        ordering = ['nombre']
        verbose_name_plural = "Centros Vecinales"
        verbose_name = "Centro Vecinal"


def validate_anio(value):
    if value < 1900 or value > date.today().year:
        raise ValidationError(
            _('%(value)s Año incorrecto'),
            params={'value': value},
        )


class BalanceCentroVecinal(models.Model):
    ''' cada uno de los balances obligatorios de los centros vecinales
        #TODO agregar cada uno de estos a una categoría especial del portal de datos '''
    centro_vecinal = models.ForeignKey(CentroVecinal)
    anio_balance = models.SmallIntegerField(
        validators=[validate_anio],
        default=date.today().year)
    archivo = models.FileField(upload_to='balances-centros-vecinales/')
    fecha_de_presentacion = models.DateField(null=True, blank=True)
    publicado = models.BooleanField(default=False, help_text="Indica si ya fue"
                                    "chequeado por la persona encargada de"
                                    "publicar los centros vecinales")


# TODO definir elecciones, centros de votacion, autoridades, padrones y
# resultados por mesa
