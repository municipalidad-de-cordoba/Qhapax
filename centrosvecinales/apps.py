from django.apps import AppConfig


class CentrosvecinalesConfig(AppConfig):
    name = 'centrosvecinales'
