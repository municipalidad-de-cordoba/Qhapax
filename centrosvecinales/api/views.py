from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import CentroVecinalSerializer
from api.pagination import DefaultPagination
from centrosvecinales.models import CentroVecinal


class CentroVecinalViewSet(viewsets.ModelViewSet):
    """

    """
    serializer_class = CentroVecinalSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CentroVecinal.objects.all().order_by('nombre')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
