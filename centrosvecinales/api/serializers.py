from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from centrosvecinales.models import CentroVecinal


class CentroVecinalSerializer(
        GeoFeatureModelSerializer,
        CachedSerializerMixin):
    barrio_de_referencia = serializers.CharField(
        source='barrio_de_referencia.nombre')

    class Meta:
        model = CentroVecinal
        fields = ('nombre', 'descripcion', 'barrio_de_referencia',
                  'fecha_de_oficializacion', )
        geo_field = "poligono"


# Registro los serializadores en la cache de DRF
cache_registry.register(CentroVecinalSerializer)
