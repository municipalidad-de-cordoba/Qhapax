from django.conf.urls import url, include
from rest_framework import routers
from .views import CentroVecinalViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register('centros-vecinales', CentroVecinalViewSet,
                base_name='centros-vecinales.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
