from django.contrib import admin
from .models import BalanceCentroVecinal, CentroVecinal
from core.admin import QhapaxOSMGeoAdmin


class BalanceInline(admin.StackedInline):
    model = BalanceCentroVecinal
    extra = 1


class CentroVecinalAdmin(QhapaxOSMGeoAdmin):
    list_display = ['nombre', 'descripcion', 'barrio_de_referencia',
                    'fecha_de_oficializacion', 'horario_atencion', 'publicado']
    search_fields = ['nombre']

    # al agregar un centro vecinal nuevo, el poligono se guardará de forma
    # predeterminada de acuerdo al barrio de referencia
    def add_view(self, *args, **kwargs):
        self.fields = [
            'nombre',
            'descripcion',
            'barrio_de_referencia',
            'fecha_de_oficializacion',
            'horario_atencion',
            'publicado',
            'observaciones_internas']
        return super(CentroVecinalAdmin, self).add_view(*args, **kwargs)

    def change_view(self, *args, **kwargs):
        self.fields = [
            'nombre',
            'descripcion',
            'barrio_de_referencia',
            'fecha_de_oficializacion',
            'horario_atencion',
            'poligono',
            'publicado',
            'observaciones_internas']
        return super(CentroVecinalAdmin, self).change_view(*args, **kwargs)

    inlines = [BalanceInline]


class BalanceCentroVecinalAdmin(admin.ModelAdmin):
    list_display = (
        "centro_vecinal",
        "archivo",
        "anio_balance",
        "fecha_de_presentacion")
    search_fields = ['centro_vecinal', 'archivo', 'anio_balance']


admin.site.register(BalanceCentroVecinal, BalanceCentroVecinalAdmin)
admin.site.register(CentroVecinal, CentroVecinalAdmin)
# admin.site.register(CentroVecinal)
