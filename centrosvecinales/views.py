import django_excel as excel
from .models import CentroVecinal
from django.views.decorators.cache import cache_page


@cache_page(60 * 60)  # 1 hora
def lista_centros_vecinales(request, filetype):
    '''
    lista de centros vecinales en formato csv o excel
    '''
    centros = CentroVecinal.objects.all()

    csv_list = []
    csv_list.append(['Nombre', 'Descripcion', 'Fecha de oficialización',
                     'Barrio de referencia'])

    for centro in centros:

        csv_list.append([centro.nombre,
                         centro.descripcion,
                         centro.fecha_de_oficializacion,
                         centro.barrio_de_referencia.nombre])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
