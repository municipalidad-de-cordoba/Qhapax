#!/usr/bin/python
"""
Importar centros vecinales
"""
from django.core.management.base import BaseCommand
from centrosvecinales.models import CentroVecinal
from django.db import transaction
import sys
import csv
from barrios.models import Barrio
import difflib


class Command(BaseCommand):
    help = """Comando para Importar Centros Vecinales"""

    def add_arguments(self, parser):
        parser.add_argument(
            '--path',
            type=str,
            help='Path del archivo csv local')

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS(
            'Iniciando importación de centros vecinales'))

        path = options['path']

        self.stdout.write(
            self.style.SUCCESS(
                'Buscando datos en {}'.format(path)))

        try:
            csv_archivo = open(path)
        except Exception as e:
            self.stdout.write(self.style.ERROR('CSV Error: {}'.format(e)))
            sys.exit(1)

        dato = csv.reader(csv_archivo)
        datos = next(dato)  # omite la primer linea

        centro_vecinal_nuevo = 0
        centro_vecinal_repetido = 0

        for reg in dato:
            print(reg)

        # necesito el queryset como lista
        barrios_queryset = list(Barrio.objects.values())
        barrios = [barrio['nombre'] for barrio in barrios_queryset]

        for reg in dato:
            nombre = 'Centro Vecinal {}'.format(reg[0])
            self.stdout.write("=============================")
            self.stdout.write("{}".format(nombre))
            self.stdout.write("=============================")

            """ acá se obtiene el barrio que mejor coincida del excel con los de
                la bd. No siempre arroja resultados, y a veces no matchea
                correctamente por lo que se deberá controlar a ojo los resultados
            """
            matches = difflib.get_close_matches(
                reg[0], possibilities=barrios, n=1)
            centro_vecinal, created = CentroVecinal.objects.get_or_create(
                nombre=nombre)
            if created:
                try:
                    barrio = Barrio.objects.get(nombre=matches[0])
                    centro_vecinal.barrio_de_referencia = barrio
                    observaciones_internas = 'Vencimiento de mandato: {} '\
                        'Decreto/Resolución: {} '\
                        'Dirección/Sede: {} '\
                        'CD: {} '\
                        'Estado: {} '\
                        'Presidente: {} '\
                        'Domicilio Presidente: {} ' \
                        'Teléfono Presidente: {} '\
                        'Secretario: {} '\
                        'Domicilio Secretario: {} '\
                        'Teléfono Secretario: {} '\
                        'Tesorero: {} '\
                        'Domicilio Tesorero: {} '\
                        'Teléfono Tesorero: {}'.format(
                            reg[1], reg[2], reg[3], reg[4], reg[5],
                            reg[6], reg[7], reg[8], reg[9], reg[10],
                            reg[11], reg[12], reg[13], reg[14])
                    centro_vecinal.observaciones_internas = observaciones_internas

                except Exception as e:
                    self.stdout.write(
                        self.style.ERROR(
                            'No existe barrio de'
                            'referencia: {}'.format(e)))

                centro_vecinal.save()
                centro_vecinal_nuevo += 1

            else:
                centro_vecinal_repetido += 1

        self.stdout.write(
            "Total de Centros Vecinales Nuevos: {}".format(centro_vecinal_nuevo))
        self.stdout.write(
            "Total de Centros Vecinales Repetidos: {}".format(centro_vecinal_repetido))
        self.stdout.write("FIN")
