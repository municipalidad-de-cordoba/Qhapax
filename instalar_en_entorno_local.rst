Instalación en entorno local
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tienes que tener instalado pip3 en tu distribución linux. Con pip3 instalado:

.. code:: bash 

    $ sudo pip3 install virtualenv
    $ python -m venv <path ambiente py3>
    $ source <path ambiente py3>/bin/activate
    $ python --version
    Python 3.x

Ya está instalado un ambiente para python3. Ahora debes clonar el
software usando git e instalar dependencias:

.. code:: bash 

    $ git clone https://github.com/avdata99/Qhapax
    $ cd Qhapax
    $ pip install -r requirements.txt