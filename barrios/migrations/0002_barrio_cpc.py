# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-10-02 16:18
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('CPC', '0001_initial'),
        ('barrios', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='barrio',
            name='cpc',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='CPC.Cpc'),
        ),
    ]
