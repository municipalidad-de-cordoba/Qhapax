#!/usr/bin/python
from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from django.contrib.gis.geos import Polygon
from fastkml import kml
from barrios import settings
from barrios.models import Barrio


class Command(BaseCommand):
    help = """Comando para importar barrios al sistema desde un .kml"""

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS(
            'Iniciando importación de barrios'))
        url_base = settings.URL_BARRIOS
        # user = settings.USER_BARRIOS
        # passw = settings.PASS_BARRIOS
        # url = '{}?user={}&password={}'.format(url_base, user, passw)
        url = url_base

        try:
            with open(url, 'rt', encoding="utf-8") as myfile:
                doc = myfile.read().encode('utf-8')
            self.stdout.write(
                self.style.SUCCESS(
                    'Buscando datos en {}'.format(url)))
        except Exception as e:
            self.stdout.write(
                self.style.ERROR(
                    'Error al leer de {}'.format(url)))
            sys.exit(1)

        # Create the KML object to store the parsed result
        k = kml.KML()

        # Read in the KML string
        k.from_string(doc)
        features = list(k.features())

        barrios = 0  # barrios descargados a la base de datos
        for feature in features:
            self.stdout.write("=============================")
            self.stdout.write("Doc: {}".format(feature.name))
            self.stdout.write("=============================")

            folders = list(feature.features())
            for folder in folders:
                self.stdout.write("=============================")
                self.stdout.write("Folder: {}".format(folder.name))
                self.stdout.write("=============================")

                places = list(folder.features())
                for place in places:
                    for ed in place.extended_data.elements:
                        for sd in ed.data:
                            if sd['name'] == 'Nombre':
                                barrio, created = Barrio.objects.get_or_create(
                                    nombre=sd['value'])
                                barrios += 1
                                self.stdout.write(
                                    "=============================")
                                self.stdout.write(
                                    "Barrio: {}".format(sd['value']))
                                self.stdout.write(
                                    "=============================")
                            if sd['name'] == 'TipoBarrio':
                                if sd['value'] == '1':
                                    barrio.es_oficial = True
                                elif sd['value'] == '2':
                                    barrio.es_oficial = False
                    barrio.poligono = Polygon(
                        place.geometry.geoms[0].exterior.coords)
                    barrio.save()
        self.stdout.write("Total de Barrios: {}".format(barrios))
