from django.contrib.gis.db import models
from CPC.models import Cpc


class Barrio(models.Model):
    nombre = models.CharField(max_length=120)
    descripcion = models.TextField(null=True, blank=True)
    es_oficial = models.NullBooleanField(null=True, default=None)
    fecha_inicio_tramite_oficilizacion = models.DateField(
        null=True, blank=True)
    fecha_de_oficializacion = models.DateField(null=True, blank=True)
    # podría ser útil un MultiPolygonField pero posiblemente agregue una
    # complejidad a todo para solo una pequeña cantidad de casos
    poligono = models.PolygonField(null=True, blank=True)
    cpc = models.ForeignKey(Cpc, blank=True, null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']
        verbose_name_plural = "Barrios"
        verbose_name = "Barrio"
