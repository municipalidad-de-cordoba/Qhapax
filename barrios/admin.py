from django.contrib import admin
from .models import Barrio
from core.admin import QhapaxOSMGeoAdmin


class BarrioAdmin(QhapaxOSMGeoAdmin):
    list_display = [
        'nombre',
        'descripcion',
        'es_oficial',
        'fecha_inicio_tramite_oficilizacion',
        'fecha_de_oficializacion']
    search_fields = ['nombre']


admin.site.register(Barrio, BarrioAdmin)
