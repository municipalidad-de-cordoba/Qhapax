from barrios.models import Barrio
from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from rest_framework_cache.registry import cache_registry


class BarrioGeoSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):
    class Meta:
        model = Barrio
        geo_field = "poligono"
        fields = [
            'id',
            'nombre',
            'descripcion',
            'es_oficial',
            'fecha_inicio_tramite_oficilizacion',
            'fecha_de_oficializacion',
            'poligono']


class BarrioSinGeoSerializer(CachedSerializerMixin):

    class Meta:
        model = Barrio
        fields = [
            'id',
            'nombre',
            'descripcion',
            'es_oficial',
            'cpc',
            'fecha_inicio_tramite_oficilizacion',
            'fecha_de_oficializacion']


cache_registry.register(BarrioGeoSerializer)
cache_registry.register(BarrioSinGeoSerializer)
