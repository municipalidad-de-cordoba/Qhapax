from django.conf.urls import url, include
from rest_framework import routers
from .views import BarrioGeoViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'barrios', BarrioGeoViewSet, base_name='barrio.api')

urlpatterns = [
    url(r'^', include(router.urls)),
]
