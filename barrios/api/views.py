from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import BarrioGeoSerializer
from api.pagination import DefaultPagination
from barrios.models import Barrio
from django.db.models import Q


class BarrioGeoViewSet(viewsets.ModelViewSet):
    """
    Filtros:
    q = {nombre, descripcion, es_oficial}
    nombre, descripcion: string
    es_oficial: bool.
    Ej: api/?q=Guemes, api/?q=True, api/?q=barrionuevo

    ids = id del barrio. Pueden ser varios ids separados por comas.
    Ej: api/?ids=2, api/?ids=5,47,51, 3

    fecha_inicio_tramite: date
    Ej: api/?fecha_inicio_tramite=1972-06-01

    fecha_oficializacion: date
    Ej: api/?fecha_oficializacion=1972-06-01
    """
    serializer_class = BarrioGeoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):

        queryset = Barrio.objects.all()

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(nombre__icontains=q) |
                Q(descripcion__icontains=q) |
                Q(es_oficial__icontains=q)
            )

        fecha_inicio_tramite = self.request.query_params.get(
            'fecha_inicio_tramite', None)
        if fecha_inicio_tramite is not None:
            queryset = queryset.filter(
                Q(fecha_inicio_tramite_oficilizacion__icontains=fecha_inicio_tramite)
            )

        fecha_oficializacion = self.request.query_params.get(
            'fecha_oficializacion', None)
        if fecha_oficializacion is not None:
            queryset = queryset.filter(
                Q(fecha_de_oficializacion__icontains=fecha_oficializacion)
            )

        ids = self.request.query_params.get('ids', None)
        if ids is not None:
            ids = ids.split(',')
            queryset = queryset.filter(id__in=ids)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
