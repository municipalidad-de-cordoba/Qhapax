# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-26 14:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('obraspublicas', '0011_auto_20171218_1540'),
    ]

    operations = [
        migrations.AddField(
            model_name='fotodetrazadoobrapublica',
            name='publicado_gobierno',
            field=models.BooleanField(default=True, help_text='Campo para editar y aprobar localmente'),
        ),
        migrations.AddField(
            model_name='trazadoobrapublica',
            name='publicado_gobierno',
            field=models.BooleanField(default=True, help_text='Campo para editar y aprobar localmente'),
        ),
    ]
