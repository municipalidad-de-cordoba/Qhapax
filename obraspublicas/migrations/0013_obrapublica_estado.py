# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-05-29 17:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('obraspublicas', '0012_auto_20171226_1112'),
    ]

    operations = [
        migrations.AddField(
            model_name='obrapublica',
            name='estado',
            field=models.PositiveIntegerField(blank=True, choices=[(1, 'Proyectada'), (2, 'Licitada'), (3, 'Adjudicada'), (4, 'Contratada'), (5, 'Replanteada'), (6, 'En ejecución'), (7, 'En ejecución(por ampliación del plazo)'), (8, 'Finalizada'), (9, 'Rescindida')], null=True),
        ),
    ]
