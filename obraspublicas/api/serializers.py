from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from versatileimagefield.serializers import VersatileImageFieldSerializer
from obraspublicas.models import (ObraPublica, TrazadoObraPublica,
                                  TipoObraPublica, BarrioObraPublica,
                                  FotoDeTrazadoObraPublica)


class BarrioObraPublicaSerializer(CachedSerializerMixin):

    class Meta:
        model = BarrioObraPublica
        fields = ('id', 'nombre')


class FotoTrazadoObraPublicaSerializer(CachedSerializerMixin):
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail_125", 'thumbnail__125x125'),
                                          ("thumbnail_500", 'thumbnail__500x500')])

    class Meta:
        model = FotoDeTrazadoObraPublica
        fields = (
            'id',
            'id_externo',
            'publicado',
            'publicado_gobierno',
            'foto')


class TrazadoObraPublicaSerializer(
        GeoFeatureModelSerializer,
        CachedSerializerMixin):
    # adjuntos = FotoTrazadoObraPublicaSerializer(many=True)
    adjuntos = serializers.SerializerMethodField('get_fotos')
    obra = serializers.CharField(source='obra.nombre')
    id_obra = serializers.CharField(source='obra.id')
    tipo = serializers.StringRelatedField(
        source='obra.tipo.nombre', read_only=True)
    tipo_id = serializers.StringRelatedField(
        source='obra.tipo.id', read_only=True)
    descripcion_frente = serializers.CharField(source='descripcion_publica')

    def get_fotos(self, container):
        fotos = FotoDeTrazadoObraPublica.objects.filter(
            trazado=container, publicado=True, publicado_gobierno=True)
        serializer = FotoTrazadoObraPublicaSerializer(
            instance=fotos, many=True)
        return serializer.data

    class Meta:
        model = TrazadoObraPublica
        geo_field = "trazado"
        fields = (
            'id',
            'descripcion_frente',
            'obra',
            'id_obra',
            'tipo',
            'tipo_id',
            'adjuntos')


class TipoObraPublicaSerializer(CachedSerializerMixin):

    class Meta:
        model = TipoObraPublica
        fields = ('id', 'nombre', 'color')


class TrazadoObraPublicaSinGeoSerializer(CachedSerializerMixin):
    adjuntos = FotoTrazadoObraPublicaSerializer(many=True)
    descripcion_frente = serializers.CharField(source='descripcion_publica')

    class Meta:
        model = TrazadoObraPublica
        fields = ('id', 'descripcion_frente', 'adjuntos')


class ObraPublicaSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):
    tipo = serializers.CharField(source='tipo.nombre')
    tipo_id = serializers.CharField(source='tipo.id')
    organizacion = serializers.SerializerMethodField()
    CUIT = serializers.SerializerMethodField()
    trazados = TrazadoObraPublicaSinGeoSerializer(many=True, read_only=True)
    barrios = BarrioObraPublicaSerializer(many=True, read_only=True)
    estado = serializers.SerializerMethodField()

    def get_estado(self, obj):
        return obj.get_estado_display()

    def get_organizacion(self, obj):
        return '' if obj.organizacion is None else obj.organizacion.nombre

    def get_CUIT(self, obj):
        return '' if obj.organizacion is None else obj.organizacion.CUIT

    class Meta:
        model = ObraPublica
        geo_field = "ubicacion"
        fields = ('id', 'nombre', 'decreto', 'estado', 'expediente_origen',
                  'descripcion_publica', 'porcentaje_completado', 'tipo',
                  'tipo_id', 'organizacion', 'CUIT', 'fecha_inicio', 'monto',
                  'fecha_finalizacion_estimada', 'trazados', 'barrios',
                  'resumen')


# Registro los serializadores en la cache de DRF
cache_registry.register(ObraPublicaSerializer)
cache_registry.register(TrazadoObraPublicaSerializer)
cache_registry.register(TipoObraPublicaSerializer)
cache_registry.register(BarrioObraPublicaSerializer)
cache_registry.register(FotoTrazadoObraPublicaSerializer)
