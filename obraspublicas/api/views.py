from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import (ObraPublicaSerializer, TrazadoObraPublicaSerializer,
                          TipoObraPublicaSerializer,
                          BarrioObraPublicaSerializer)
from api.pagination import DefaultPagination
from obraspublicas.models import (ObraPublica, TrazadoObraPublica,
                                  TipoObraPublica, BarrioObraPublica)
from core.utils import normalizar


class ObraPublicaViewSet(viewsets.ModelViewSet):
    """
    Lista de obras publicas geolocalizadas con sus datos y trazados
    Puede filtrarse por:
     - tipo_id (identificador del tipo de obra), pueden ser varios separados por comas
     - avance_desde (entero vinculado al campo "porcentaje_completado")
     - avance_hasta (entero vinculado al campo "porcentaje_completado")
     - barrios_ids: lista de barrios separados por comas
     - texto: filtra aquellas obras que contienen el texto indicado en el nombre de la obra o
      en el nombre de la org
     - estado: estado de avance de la obra. 9 posibles opciones(son valores númericos):
        "1: EN PROYECTO"
        "2: LICITADAS"
        "3: ADJUDICADA"
        "4: CONTRATADA"
        "5: REPLANTEADA"
        "6: EN EJECUCION"
        "7: EN EJECUCION POR AMPLIACION"
        "8: FINALIZADA"
        "9: RESCINDIDA"
        Pueden ser más de un valor.
    """
    serializer_class = ObraPublicaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = ObraPublica.objects.filter(
            publicado=True, publicado_gobierno=True).order_by('nombre')

        tipos_id_str = self.request.query_params.get('tipo_id', None)
        if tipos_id_str is not None:
            tipos_id = tipos_id_str.split(',')
            queryset = queryset.filter(tipo__id__in=tipos_id)

        avance_desde = self.request.query_params.get('avance_desde', None)
        if avance_desde is not None:
            queryset = queryset.filter(
                porcentaje_completado__gte=int(avance_desde))

        avance_hasta = self.request.query_params.get('avance_hasta', None)
        if avance_hasta is not None:
            queryset = queryset.filter(
                porcentaje_completado__lte=int(avance_hasta))

        barrios_str = self.request.query_params.get('barrios_ids', None)
        if barrios_str is not None:
            barrios_ids = barrios_str.split(',')
            queryset = queryset.filter(barrios__in=barrios_ids)

        texto = self.request.query_params.get('texto', None)
        if texto is not None:
            # FIXME: acá se busca el texto que quiere el usuario, por más que
            # en la bd tenga acento lo mismo lo encuentra.
            # Si Django trae algo predefinido, usarlo acá
            copia_queryset = queryset.values(
                'id', 'nombre', 'organizacion__nombre', 'descripcion_publica')
            list_ids = []
            texto_norm = normalizar(texto)
            for obra in copia_queryset:
                if texto_norm in normalizar(
                        obra['nombre']) or texto_norm in normalizar(
                        obra['organizacion__nombre']) or texto_norm in normalizar(
                        obra['descripcion_publica']):
                    list_ids.append(obra['id'])
            queryset = queryset.filter(id__in=list_ids)

        # Filtro por estado
        estado = self.request.query_params.get('estado', None)
        if estado is not None:
            estados = estado.split(',')
            queryset = queryset.filter(estado__in=estados)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class TrazadoObraPublicaViewSet(viewsets.ModelViewSet):
    """
    Trazados de obras publicas
    Puede fitrarse:
     - solo para una obra con el parámetro id_obra
     - por tipo_id (identificador del tipo de obra), pueden ser varios separados por comas
     - obra_avance_desde (entero vinculado al campo "porcentaje_completado")
     - obra_avance_hasta (entero vinculado al campo "porcentaje_completado")
     - barrios_ids: lista de barrios separados por comas
     - texto: filtra aquellos frentes de obras que contienen el texto indicado en la descripcion del frente
      o en el nombre de la obra
     - estado: estado de avance de la obra. 9 posibles opciones(son valores númericos):
        "1: EN PROYECTO"
        "2: LICITADAS"
        "3: ADJUDICADA"
        "4: CONTRATADA"
        "5: REPLANTEADA"
        "6: EN EJECUCION"
        "7: EN EJECUCION POR AMPLIACION"
        "8: FINALIZADA"
        "9: RESCINDIDA"
        Pueden ser más de un valor.
    """

    serializer_class = TrazadoObraPublicaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):

        queryset = TrazadoObraPublica.objects.filter(
            obra__publicado=True,
            obra__publicado_gobierno=True,
            publicado=True,
            publicado_gobierno=True).order_by('obra')

        id_obra = self.request.query_params.get('id_obra', None)
        if id_obra is not None:
            queryset = queryset.filter(obra__id=int(id_obra))

        tipos_id_str = self.request.query_params.get('tipo_id', None)
        if tipos_id_str is not None:
            tipos_id = tipos_id_str.split(',')
            queryset = queryset.filter(obra__tipo__id__in=tipos_id)

        obra_avance_desde = self.request.query_params.get(
            'obra_avance_desde', None)
        if obra_avance_desde is not None:
            queryset = queryset.filter(
                obra__porcentaje_completado__gte=int(obra_avance_desde))

        obra_avance_hasta = self.request.query_params.get(
            'obra_avance_hasta', None)
        if obra_avance_hasta is not None:
            queryset = queryset.filter(
                obra__porcentaje_completado__lte=int(obra_avance_hasta))

        barrios_str = self.request.query_params.get('barrios_ids', None)
        if barrios_str is not None:
            barrios_ids = barrios_str.split(',')
            queryset = queryset.filter(obra__barrios__in=barrios_ids)

        texto = self.request.query_params.get('texto', None)
        if texto is not None:
            # FIXME: acá se busca el texto que quiere el usuario, por más que
            # en la bd tenga acento lo mismo lo encuentra.
            # Si Django trae algo predefinido, usarlo acá
            copia_queryset = queryset.values(
                'id', 'obra__nombre', 'descripcion_publica')
            list_ids = []
            for trazado in copia_queryset:
                if normalizar(texto) in (
                    normalizar(
                        trazado['obra__nombre']) or normalizar(
                        trazado['descripcion_publica'])):
                    list_ids.append(trazado['id'])
            queryset = queryset.filter(id__in=list_ids)

        # Filtro por estado
        estado = self.request.query_params.get('estado', None)
        if estado is not None:
            estados = estado.split(',')
            queryset = queryset.filter(obra__estado__in=estados)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class TipoObraPublicaViewSet(viewsets.ModelViewSet):
    """
    Tipos de obras publicas
    """
    serializer_class = TipoObraPublicaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = TipoObraPublica.objects.all().order_by('nombre')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class BarrioObraPublicaViewSet(viewsets.ModelViewSet):
    """
    Tipos de obras publicas
    """
    serializer_class = BarrioObraPublicaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = BarrioObraPublica.objects.all().order_by('nombre')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
