from rest_framework.views import exception_handler
from obraspublicas.models import TrazadoObraPublica, FotoDeTrazadoObraPublica
from obraspublicas.api.views import ObraPublicaViewSet
from PIL import Image


def custom_exception_handler(exc, context):
    """
    Maneja excepciones generadas al renderizar el api de obras publicas.
    Borra los objetos que causan problemas.
    """
    view = context['view']

    if isinstance(view, ObraPublicaViewSet):
        obras = view.get_queryset()
        for obra in obras:
            trazados = TrazadoObraPublica.objects.filter(obra=obra)
            for trazado in trazados:
                fotos = FotoDeTrazadoObraPublica.objects.filter(
                    trazado=trazado)
                for foto in fotos:
                    try:
                        imagen = Image.open(foto.foto.path)
                        # print(foto.foto.path)
                    except Exception as e:
                        print(e)
                        foto.delete()

    response = exception_handler(exc, context)
    return response
