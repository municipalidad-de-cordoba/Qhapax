from django.conf.urls import url, include
from rest_framework import routers
from .views import (ObraPublicaViewSet, TrazadoObraPublicaViewSet,
                    TipoObraPublicaViewSet, BarrioObraPublicaViewSet)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'obras-publicas',
    ObraPublicaViewSet,
    base_name='obra-publica.api.lista')
router.register(r'frentes-obras-publicas', TrazadoObraPublicaViewSet,
                base_name='frentes-obra-publica.api.lista')
router.register(r'tipos-obras-publicas', TipoObraPublicaViewSet,
                base_name='tipos-obra-publica.api.lista')
router.register(r'barrios-obras-publicas', BarrioObraPublicaViewSet,
                base_name='barrios-obra-publica.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
