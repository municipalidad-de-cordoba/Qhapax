from django.apps import AppConfig


class ObraspublicasConfig(AppConfig):
    name = 'obraspublicas'
