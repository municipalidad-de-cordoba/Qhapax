from django.db import models
from django.contrib.gis.db import models as models_geo
from django.conf import settings
from versatileimagefield.fields import VersatileImageField, PPOIField
from core.models import Moneda
from organizaciones.models import Organizacion


class TipoObraPublica(models.Model):
    ''' tipo de obra públicas (pavimentación, etc)'''
    nombre = models.CharField(max_length=90)
    color = models.CharField(
        max_length=20,
        default='ffffffff',
        help_text=('Se esperan 8 caracteres hexBinary: aabbggrr',
                   ' (alpha + blue + gren + red) de 0 a f'))

    def __str__(self):
        return self.nombre


class BarrioObraPublica(models.Model):
    ''' lista de barrios personalizada de obra publica
        #FIXME conectar a la futura base oficial de barrios '''
    nombre = models.CharField(max_length=90)

    def __str__(self):
        return self.nombre


class OrigenDatoObraPublica(models.Model):
    '''
    Los datos pueden venir desde mas de un origen
    cuando comienzo la importación desde un origen y a los fines de eliminar
    los que no lleguen debo marcar a todos los elementos como temporalmente
    inactivos desde un origen hasta que pase de nuevo la migración
    '''
    nombre = models.CharField(max_length=190)

    def __str__(self):
        return self.nombre


class ObraPublica(models.Model):
    """
    obras públicas
    """
    PROYECTADA = 1
    LICITADA = 2
    ADJUDICADA = 3
    CONTRATADA = 4
    REPLANTEADA = 5
    EN_EJECUCION = 6
    EN_EJECUCION_POR_AMPLIACION = 7
    FINALIZADA = 8
    RESCINDIDA = 9

    tipos_estados = (
        (PROYECTADA, 'Proyectada'),
        (LICITADA, 'Licitada'),
        (ADJUDICADA, 'Adjudicada'),
        (CONTRATADA, 'Contratada'),
        (REPLANTEADA, 'Replanteada'),
        (EN_EJECUCION, 'En ejecución'),
        (EN_EJECUCION_POR_AMPLIACION, 'En ejecución(por ampliación del plazo)'),
        (FINALIZADA, 'Finalizada'),
        (RESCINDIDA, 'Rescindida')
    )

    id_externo = models.PositiveIntegerField(default=0)
    nombre = models.CharField(max_length=190)
    decreto = models.CharField(max_length=90, null=True, blank=True)
    expediente_origen = models.CharField(max_length=90, null=True, blank=True)
    tipo = models.ForeignKey(
        TipoObraPublica,
        null=True,
        on_delete=models.SET_NULL)

    organizacion = models.ForeignKey(Organizacion, null=True, blank=True)
    moneda_monto = models.ForeignKey(
        Moneda,
        null=True,
        on_delete=models.SET_NULL,
        default=settings.MONEDA_PK_PRINCIPAL)
    monto = models.DecimalField(max_digits=19, decimal_places=2, default=0.0)
    fecha_inicio = models.DateTimeField(null=True, blank=True)
    fecha_finalizacion_estimada = models.DateTimeField(null=True, blank=True)

    barrios = models.ManyToManyField(BarrioObraPublica, blank=True)
    color = models.CharField(max_length=20, null=True, blank=True)

    porcentaje_completado = models.DecimalField(
        max_digits=5, decimal_places=2, default=0.0)
    estado = models.PositiveIntegerField(
        choices=tipos_estados, null=True, blank=True)

    # campo generico espacial, puede contener puntos, líneas o polígonos
    ubicacion = models_geo.GeometryField(null=True)

    origen = models.ForeignKey(OrigenDatoObraPublica, null=True, blank=True)
    observaciones_internas = models.TextField(null=True, blank=True)
    publicado = models.BooleanField(
        default=True,
        help_text='Indica si vino OK desde el sistema externo que nos provee los datos')
    publicado_gobierno = models.BooleanField(
        default=False, help_text='Campo para editar y aprobar localmente')
    descripcion_publica = models.TextField(
        null=True,
        blank=True,
        help_text='Descripcion local cargada por el equipo de comunicación')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    @property
    def resumen(self, largo_texto=280):
        """
        Retorna nombre - monto - proveedor
        """
        propuesto = ''
        # si el nombre es mas grande, se trunca para que entre en un tweet
        descripcion_publica = self.nombre if self.descripcion_publica is None else self.descripcion_publica
        if len(descripcion_publica) >= largo_texto:
            propuesto += '{} ...'.format(descripcion_publica[:largo_texto - 4])
        else:
            propuesto += descripcion_publica
            if len(propuesto + ' - $' + str(self.monto)) <= largo_texto:
                # descripcion_publica + monto entran en el tweet
                propuesto += ' - ${}'.format(str(self.monto))
                nombre = '' if self.organizacion is None else self.organizacion.nombre
                if len(propuesto + ' - ' + nombre) <= largo_texto:
                    # descripcion_publica + monto + proveedor entran en el
                    # tweet
                    propuesto += ' - {}'.format(nombre)
        return propuesto

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']


class TrazadoObraPublica(models.Model):
    ''' Info geo de cada parte de la obra '''
    obra = models.ForeignKey(ObraPublica, related_name='trazados')

    id_externo = models.PositiveIntegerField(default=0)
    descripcion_publica = models.TextField(null=True, blank=True)

    # campo generico espacial, puede contener puntos, líneas o polígonos
    trazado = models_geo.GeometryField(null=True)

    observaciones_internas = models.TextField(null=True, blank=True)
    publicado = models.BooleanField(default=True)
    publicado_gobierno = models.BooleanField(
        default=True, help_text='Campo para editar y aprobar localmente')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} {}'.format(self.obra.nombre, self.descripcion_publica)

    class Meta:
        ordering = ['obra__nombre', 'descripcion_publica']


class FotoDeTrazadoObraPublica(models.Model):
    """
    Fotos del trazado de Obras Publicas
    """
    trazado = models.ForeignKey(TrazadoObraPublica, related_name='adjuntos')

    id_externo = models.PositiveIntegerField(default=0)
    url = models.CharField(max_length=320, null=True, blank=True)
    publicado = models.BooleanField(default=True)
    publicado_gobierno = models.BooleanField(
        default=True, help_text='Campo para editar y aprobar localmente')

    foto = VersatileImageField(
        upload_to='imagenes/obraspublicas',
        ppoi_field='foto_ppoi',
        null=True,
        blank=True,
        max_length=200
    )
    foto_ppoi = PPOIField('Image PPOI')

    def __str__(self):
        return "Foto {} {}".format(self.id, self.id_externo)
