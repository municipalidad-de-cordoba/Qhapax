#!/usr/bin/python
"""
De cada obra hay trazadas (información GEO) aquí se importa esta parte
"""
from django.core.management.base import BaseCommand
from obraspublicas import settings as settings_op
# from django.db import transaction
import sys
import requests
import json
from obraspublicas.models import (ObraPublica, OrigenDatoObraPublica,
                                  TrazadoObraPublica, FotoDeTrazadoObraPublica)
from django.contrib.gis.geos import GEOSGeometry
from django.core.files import File
import tempfile
from PIL import Image


class Command(BaseCommand):
    help = """Comando para Importar trazados de obras públicas en ejecución """

    def add_arguments(self, parser):
        parser.add_argument(
            '--fecha',
            type=str,
            help='fecha desde la cual se importarán nuevas obras y cambios a las existentes. Formato dd/mm/aaaa',
            default='01/01/2016')

    # @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS(
            'Iniciando importación obras públicas'))

        fecha = options['fecha']
        url_base = settings_op.URL_API_TRAZADOS_OBRAS_PUBLICAS
        user = settings_op.USER_API_OBRAS_PUBLICAS
        passw = settings_op.PASS_API_OBRAS_PUBLICAS
        # url = '{}?user={}&password={}'.format(url_base, user, passw)
        url = '{}&fecha={}'.format(url_base, fecha)

        """ EJEMPLO DE RESULTADOS

        {"type":"FeatureCollection",
        "features":[
            {"type":"Feature",
            "properties":
                {"descripcion":"Santo Domingo ",
                "estado":"Realizado",
                "id_trazado":1606,
                "id_obra":26
                "adjuntos": [
                        {
                            "id_externo": 12812,
                            "publicado": true,
                            "foto": {
                                "original": "http://localhost:8000/media/imagenes/obraspublicas/imagen2_Nkkbdbe.jpg",
                                "thumbnail_32x32": "http://localhost:8000/media/__sized__/imagenes/obraspublicas/imagen2_Nkkbdbe-thumbnail-32x32-70.jpg",
                                "thumbnail": "http://localhost:8000/media/__sized__/imagenes/obraspublicas/imagen2_Nkkbdbe-thumbnail-125x125-70.jpg"
                            }
                        }
                ]
                },
            "geometry":
                {"type":"LineString",
                "coordinates":[
                    [-64.24571871757507,-31.478671646334753],[-64.24290776252747,-31.481196990913652]
                    ]
                }
            },
        """

        try:
            headers = {'Authorization': settings_op.URL_AUTHORIZATION}
            basedata = requests.get(
                url, headers=headers).content  # es tipo byte
            basedata = basedata.decode("iso-8859-1")
        except Exception as e:
            self.stdout.write(
                self.style.ERROR(
                    'Error al leer el web service {}'.format(url)))
            sys.exit(1)

        try:
            jsondata = json.loads(basedata)
        except Exception as e:
            self.stdout.write(
                self.style.ERROR(
                    'JSON Error: {}'.format(basedata)))
            print(e)
            sys.exit(1)

        # marcar como el origen que tienen
        origen, created = OrigenDatoObraPublica.objects.get_or_create(
            nombre='MuniDigital.com')
        # despublicar todo para ver que vuelve aquí (y que queden ocultos los
        # que no vengan)
        TrazadoObraPublica.objects.filter(
            obra__origen=origen).update(
            publicado=False)
        # FotoDeTrazadoObraPublica.objects.filter(trazado__obra__origen=origen).update(publicado=False)

        trazados = len(jsondata['features'])
        self.stdout.write(
            self.style.SUCCESS(
                'Se obtuvieron {} trazados en {}'.format(
                    trazados, url)))

        # chequear los datos
        errores = []
        trazados_nuevos = 0
        trazados_repetidos = 0

        for jtrazado in jsondata['features']:
            dato = jtrazado['properties']
            id_obra = dato['id_obra']
            try:
                obra = ObraPublica.objects.get(id_externo=id_obra)
            except ObraPublica.DoesNotExist:
                err = 'NO EXISTE la obra ID:{}'.format(id_obra)
                self.stdout.write(self.style.ERROR(err))
                errores.append(err)
                continue

            id_externo = dato['id_trazado']
            trazado, created_trazado = TrazadoObraPublica.objects.get_or_create(
                obra=obra, id_externo=id_externo)
            if created_trazado:
                trazados_nuevos += 1
            else:
                trazados_repetidos += 1

            # fotos de las obras
            adjuntos = dato['adjuntos']
            for adjunto in adjuntos:
                foto, created = FotoDeTrazadoObraPublica.objects.get_or_create(
                    trazado=trazado, id_externo=adjunto['id'])

                foto.url = adjunto['url']
                # el campo paraPublicar tiene un string, se pasa a booleano
                if adjunto['paraPublicar'] == 'SI':
                    foto.publicado = True
                else:
                    foto.publicado = False
                # importo las fotos solo si es la primera vez
                if created:
                    url_foto = adjunto['url']
                    contenido = requests.get(url_foto).content  # es tipo byte

                    tf = tempfile.NamedTemporaryFile()
                    tf.write(contenido)

                    img = File(tf)
                    try:
                        # controlamos que el archivo sea una imagen válida
                        img = Image.open(img)
                        if img.size != 0:
                            foto.foto.save(
                                str(foto.url.split('/')[-1]), File(tf))
                    except Exception as e:
                        err = 'ERROR con el formato de la FOTO:{}'.format(
                            foto.url)
                        self.stdout.write(self.style.ERROR(err))
                        errores.append(err)
                        foto.delete()
                        continue

                        foto.save()
                # else:
                #     self.stdout.write(self.style.SUCCESS('Omitiendo descargar imagen {}'.format(foto.url)))

            # datos geoespaciales
            geometry = jtrazado['geometry']
            geo_str = str(geometry)
            geometry_data = GEOSGeometry(geo_str)
            trazado.trazado = geometry_data
            trazado.descripcion_publica = dato['descripcion']
            trazado.publicado = True
            if created_trazado:  # si la oibra viene por primera vez la publico, despues puedo revisar
                trazado.publicado_gobierno = True
            trazado.save()

            log = 'Trazado id:{}, obra:{}, nuevos {}, repetidos {}'.format(
                id_externo, id_obra, trazados_nuevos, trazados_repetidos)
            self.stdout.write(self.style.SUCCESS(log))

        if len(errores) > 0:
            self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR'))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))
            # sys.exit(1)
        else:
            self.stdout.write(self.style.SUCCESS('SIN ERRORES'))

        self.stdout.write(self.style.SUCCESS('FIN'))
