#!/usr/bin/python
"""
Importar obras públicas de software externo
"""
from django.core.management.base import BaseCommand
from obraspublicas import settings as settings_op
from organizaciones.models import Organizacion
from django.db import transaction
import sys
from datetime import datetime
import requests
import json
from obraspublicas.models import (ObraPublica, BarrioObraPublica,
                                  TipoObraPublica)
import pytz
from django.contrib.gis.geos import GEOSGeometry


class Command(BaseCommand):
    help = """Comando para Importar obras públicas en ejecución """

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS(
            'Iniciando importación obras privadas'))

        url_base = settings_op.URL_API_OBRAS_PUBLICAS
        user = settings_op.USER_API_OBRAS_PUBLICAS
        passw = settings_op.PASS_API_OBRAS_PUBLICAS
        # url = '{}?user={}&password={}'.format(url_base, user, passw)
        url = url_base

        """ EJEMPLO DE RESULTADOS

        {"type":"FeatureCollection",
         "features":[
            {
            "type":"Feature",
            "properties":
                {
                "Evento":"Emergencia - AGUAS C.",
                "Barrio":"Centro","Observaciones":"120 HORAS - A.C. - OT_METRD - 247185",
                "Direccion":"25 de Mayo 180","iconUrl":"https://munidigital.com/app/img/markers/icono-aguas-rojo.png",
                "Estado":"Pendiente"},
            "geometry":
                {
                "type":"Point",
                "coordinates":[-64.1812642285714,-31.4155853591837]
                }
            },

        """
        sys.exit(1)
        self.stdout.write(
            self.style.SUCCESS(
                'Buscando datos en {}'.format(url)))

        basedata = requests.get(url).content  # es tipo byte
        # basedata = basedata.decode("utf-8")
        basedata = basedata.decode("iso-8859-1")

        try:
            jsondata = json.loads(basedata)
        except Exception as e:
            self.stdout.write(
                self.style.ERROR(
                    'JSON Error: {}'.format(basedata)))
            sys.exit(1)

        obras = len(jsondata['features'])
        self.stdout.write(
            self.style.SUCCESS(
                'Se obtuvieron datos de {} obras'.format(obras)))

        # chequear los datos
        errores = []
        obras_nuevas = 0
        obras_repetidas = 0
        organizaciones_nuevas = 0
        organizaciones_repetidas = 0
        tipos_nuevos = 0
        tipos_repetidos = 0
        barrios_nuevos = 0
        barrios_repetidos = 0

        for fobra in jsondata['features']:

            dato = fobra['properties']

            # self.stdout.write(self.style.SUCCESS('Obra: \n********\n{}\n********\n'.format(dato)))

            nombre = dato['nombreObra'].strip()
            if 'id_obra' not in dato.keys():
                self.stdout.write(
                    self.style.ERROR(
                        'La obra "{} no tiene ID_OBRA'.format(nombre)))
                continue

            id_externo = dato['id_obra']
            obra, created = ObraPublica.objects.get_or_create(
                id_externo=id_externo)
            if created:
                obras_nuevas += 1
            else:
                obras_repetidas += 1

            obra.nombre = nombre
            obra.decreto = dato['decreto'].strip()
            obra.expediente_origen = dato['expedienteOriginal'].strip()
            tipo_str = dato['tipoObra'].strip()
            if tipo_str == '':
                tipo_str = 'Desconocido'
            tipo, created = TipoObraPublica.objects.get_or_create(
                nombre=tipo_str)
            if created:
                tipos_nuevos += 1
            else:
                tipos_repetidos += 1

            obra.tipo = tipo

            # datos geoespaciales
            geometry = fobra['geometry']
            geo_str = str(geometry)
            geometry_data = GEOSGeometry(geo_str)
            obra.ubicacion = geometry_data

            organizacion_str = dato['contratista'].strip()
            if organizacion_str == '':
                organizacion_str = 'Desconocido'

            cuit_str = dato['cuitContratista'].strip()
            if cuit_str == '':
                cuit_str = 'Desconocido'

            organizacion, created = Organizacion.objects.get_or_create(
                nombre=organizacion_str, CUIT=cuit_str)
            if created:
                organizaciones_nuevas += 1
            else:
                organizaciones_repetidas += 1

            obra.organizacion = organizacion

            obra.monto = 0.0 if dato['monto'] == '' else dato['monto']
            # 10/03/2017 00:00
            fecha_inicio = datetime.strptime(
                dato['fechaIniciacion'].strip(), "%d/%m/%Y %H:%M")
            fecha_finalizacion_estimada = datetime.strptime(
                dato['fechaTerminacion'].strip(), "%d/%m/%Y %H:%M")

            obra.fecha_inicio = pytz.utc.localize(fecha_inicio)
            obra.fecha_finalizacion_estimada = pytz.utc.localize(
                fecha_finalizacion_estimada)

            barrios_str = dato['barrios']
            barrios = []
            for barrio_str in barrios_str:
                barrio, created = BarrioObraPublica.objects.get_or_create(
                    nombre=barrio_str)
                if created:
                    barrios_nuevos += 1
                else:
                    barrios_repetidos += 1

                barrios.append(barrio)

            obra.barrios = barrios
            obra.color = dato['color'].strip()

            obra.porcentaje_completado = 0.0 if dato['porcentajeCompletado'] == '' else dato['porcentajeCompletado']

            obra.save()

            log = 'Obras: {}-{} Tipos {}-{} Contratistas {}-{} Barrios: {}-{} )'.format(
                obras_nuevas,
                obras_repetidas,
                tipos_nuevos,
                tipos_repetidos,
                organizaciones_nuevas,
                organizaciones_repetidas,
                barrios_nuevos,
                barrios_repetidos)
            self.stdout.write(self.style.SUCCESS(log))

        if len(errores) > 0:
            self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR'))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))
            sys.exit(1)

        self.stdout.write(self.style.SUCCESS('FIN'))
