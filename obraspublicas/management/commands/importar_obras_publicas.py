#!/usr/bin/python
"""
Importar obras públicas de software externo
"""
from django.core.management.base import BaseCommand
from obraspublicas import settings as settings_op
from organizaciones.models import Organizacion
from django.db import transaction
import sys
from datetime import datetime
import requests
import json
from obraspublicas.models import (ObraPublica, BarrioObraPublica,
                                  TipoObraPublica, OrigenDatoObraPublica)
import pytz
from django.contrib.gis.geos import GEOSGeometry
import random
from core.utils import normalizar
from django.template.loader import render_to_string
from core.lib.comm import QhapaxMail


class Command(BaseCommand):
    help = """Comando para Importar obras públicas en ejecución """

    def add_arguments(self, parser):
        parser.add_argument(
            '--fecha',
            type=str,
            help=('fecha desde la cual se importarán nuevas obras y cambios a',
                  'las existentes. Formato dd/mm/aaaa'),
            default='01/01/2016')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS(
            'Iniciando importación obras públicas'))

        fecha = options['fecha']
        url_base = settings_op.URL_API_OBRAS_PUBLICAS
        user = settings_op.USER_API_OBRAS_PUBLICAS
        passw = settings_op.PASS_API_OBRAS_PUBLICAS
        url = '{}&fecha={}'.format(url_base, fecha)

        """ EJEMPLO DE RESULTADOS

        {"type":"FeatureCollection",
        "features":[
            {
            "type":"Feature",
            "properties":
                {
                "decreto":"Dto. Nº NN/NN (Notif. 27/01/2017).",
                "tipoObra":"Pavimentacion",
                "nombreObra":"Santa Juana - Pavimentación Sector Noroeste",
                "monto":75218191.10,
                "fechaIniciacion":"10/03/2017 00:00",
                "contratista":"Empresa S.A.",
                "cuitContratista": "30-00000000-0",
                "color":"#6C6B6B",
                "porcentajeCompletado":24.00,
                "id_obra":26,
                "expedienteOriginal":"047.824-16",
                "barrios":
                    ["Parque Futura","Santa Isabel Sec III","Vicor","Santa Isabel Secc I"],
                "fechaTerminacion":"05/03/2018 00:00"
                },
            "geometry":
                {
                "type":"MultiLineString",
                "coordinates":[
                                [
                                  [-64.23861622810364,-31.477579139176946],
                                  [-64.24153447151183,-31.475026246128753]
                                ],
                                [
                                  [-64.24251294149144,-31.477496788875293],
                                  [-64.24340343488439,-31.476691582107403]
                                ],
                                [
                                  [-64.24135422719701,-31.48092799006047],
                                  [-64.24198722852452,-31.480324107777545],
                                  [-64.24530243886693,-31.4773961384221]
                                ]
                            ]
                }
            },
        """

        self.stdout.write(self.style.SUCCESS(
            'Buscando datos en {}'.format(url)))

        headers = {'Authorization': settings_op.URL_AUTHORIZATION}
        basedata = requests.get(url, headers=headers).content  # es tipo byte
        # basedata = basedata.decode("utf-8")
        basedata = basedata.decode("iso-8859-1")

        try:
            jsondata = json.loads(basedata)
        except Exception as e:
            self.stdout.write(self.style.ERROR(
                'JSON Error: {}'.format(basedata)))
            sys.exit(1)

        # marcar como el origen que tienen
        origen, created = OrigenDatoObraPublica.objects.get_or_create(
            nombre='MuniDigital.com')
        # despublicar todo para ver que vuelve aquí (y que queden ocultos los
        # que no vengan)
        ObraPublica.objects.filter(origen=origen).update(publicado=False)

        obras = len(jsondata['features'])
        self.stdout.write(
            self.style.SUCCESS(
                'Se obtuvieron datos de {} obras'.format(obras)))

        # chequear los datos
        errores = []
        obras_nuevas = 0
        obras_repetidas = 0
        organizaciones_nuevas = 0
        organizaciones_repetidas = 0
        tipos_nuevos = 0
        tipos_repetidos = 0
        barrios_nuevos = 0
        barrios_repetidos = 0

        for fobra in jsondata['features']:
            dato = fobra['properties']
            nombre = dato['nombreObra'].strip()
            if 'id_obra' not in dato.keys():
                self.stdout.write(
                    self.style.ERROR(
                        'La obra "{} no tiene ID_OBRA'.format(nombre)))
                continue

            id_externo = dato['id_obra']
            obra, created_obra = ObraPublica.objects.get_or_create(
                id_externo=id_externo)
            if created_obra:
                obras_nuevas += 1
            else:
                obras_repetidas += 1

            obra.nombre = nombre
            obra.decreto = dato['decreto'].strip()
            obra.expediente_origen = dato['expedienteOriginal'].strip()
            tipo_str = dato['tipoObra'].strip()
            if tipo_str == '':
                tipo_str = 'Desconocido'
            tipo, created = TipoObraPublica.objects.get_or_create(
                nombre=tipo_str)
            if created:
                tipos_nuevos += 1
            else:
                tipos_repetidos += 1
            if tipo.color == 'ffffffff':
                tipo.color = ''.join(
                    random.choice('0123456789abcdef') for n in range(8))
                tipo.save()
            obra.tipo = tipo
            obra.origen = origen
            # datos geoespaciales
            geometry = fobra['geometry']
            geo_str = str(geometry)
            geometry_data = GEOSGeometry(geo_str)
            obra.ubicacion = geometry_data

            organizacion_str = dato['contratista'].strip()
            if organizacion_str == '':
                organizacion_str = 'Desconocido'

            cuit_str = dato['cuitContratista'].strip()
            # limpio el cuit de caracteres extraños
            cuit_str = cuit_str.replace(
                '-', '').replace(' ', '').replace('.', '').replace(',', '')
            if cuit_str == '':
                cuit_str = 'Desconocido'

            try:
                organizacion, created = Organizacion.objects.get_or_create(
                    nombre=organizacion_str, CUIT=cuit_str)
                if created:
                    organizaciones_nuevas += 1
                else:
                    organizaciones_repetidas += 1
            except Organizacion.MultipleObjectsReturned:
                # Fix para cuando la organizacion está duplicada
                organizaciones = Organizacion.objects.filter(
                    nombre=organizacion_str, CUIT=cuit_str)

                max_obras_relacionadas = 0
                organizacion = None
                # por cada organización, obtengo las obras que tiene a su cargo
                for org in organizaciones:
                    cant = len(org.obrapublica_set.all())
                    if cant > max_obras_relacionadas:
                        organizacion = org
                        max_obras_relacionadas = cant
                # elijo la organizacion que tiene mas obras
                organizaciones_repetidas += 1

            obra.organizacion = organizacion

            if 'monto' not in dato.keys() or dato['monto'] == '':
                obra.monto = 0.0
            else:
                obra.monto = dato['monto']
            # 10/03/2017 00:00
            try:
                fecha_inicio = datetime.strptime(
                    dato['fechaIniciacion'].strip(), "%d/%m/%Y %H:%M")
                obra.fecha_inicio = pytz.utc.localize(fecha_inicio)
            except Exception as e:
                error = 'La obra {} con id(externo) {} tuvo error en fecha inicio {}'.format(
                    obra.nombre, obra.id_externo, e)
                errores.append(error)
                self.stdout.write(self.style.ERROR(error))
                obra.fecha_inicio = None

            try:
                fecha_finalizacion_estimada = datetime.strptime(
                    dato['fechaTerminacion'].strip(), "%d/%m/%Y %H:%M")
                obra.fecha_finalizacion_estimada = pytz.utc.localize(
                    fecha_finalizacion_estimada)
            except Exception as e:
                error = 'La obra {} con id(externo) {} tuvo error en fecha fin {}'.format(
                    obra.nombre, obra.id_externo, e)
                errores.append(error)
                self.stdout.write(self.style.ERROR(error))
                obra.fecha_finalizacion_estimada = None

            if fecha_finalizacion_estimada < fecha_inicio:
                error = 'La obra {} con id(externo) {} FINALIZA antes de empezar {}<{}'.format(
                    obra.nombre, obra.id_externo, fecha_finalizacion_estimada, fecha_inicio)
                errores.append(error)
                self.stdout.write(self.style.ERROR(error))

            if fecha_inicio.year < 1980:
                error = 'La obra {} con id(externo) {} tiene INICIO muy viejo {}'.format(
                    obra.nombre, obra.id_externo, fecha_inicio)
                errores.append(error)
                self.stdout.write(self.style.ERROR(error))
                obra.fecha_inicio = None

            if fecha_finalizacion_estimada.year < 1980:
                error = 'La obra {} con id(externo) {} es tiene finalizacion muy vieja {}'.format(
                    obra.nombre, obra.id_externo, fecha_finalizacion_estimada)
                errores.append(error)
                self.stdout.write(self.style.ERROR(error))
                obra.fecha_finalizacion_estimada = None

            barrios_str = dato['barrios']
            barrios = []
            for barrio_str in barrios_str:
                barrio, created = BarrioObraPublica.objects.get_or_create(
                    nombre=barrio_str)
                if created:
                    barrios_nuevos += 1
                else:
                    barrios_repetidos += 1

                barrios.append(barrio)

            obra.barrios = barrios
            obra.color = dato['color'].strip()

            obra.porcentaje_completado = 0.0 if dato[
                'porcentajeCompletado'] == '' else dato['porcentajeCompletado']
            obra.publicado = True
            if created_obra:
                # si la obra viene por primera vez la publico,
                # despues puedo revisar
                obra.publicado_gobierno = True

            estado = normalizar(dato['estadoObra'])
            if estado == normalizar('Proyectada'):
                obra.estado = ObraPublica.PROYECTADA
            elif estado == normalizar('Licitada'):
                obra.estado = ObraPublica.LICITADA
            elif estado == normalizar('Adjudicada'):
                obra.estado = ObraPublica.ADJUDICADA
            elif estado == normalizar('Contratada'):
                obra.estado = ObraPublica.CONTRATADA
            elif estado == normalizar('Replanteada'):
                obra.estado = ObraPublica.REPLANTEADA
            elif estado == normalizar('En ejecucion'):
                obra.estado = ObraPublica.EN_EJECUCION
            elif estado == normalizar('En ejecucion(por ampliacion de plazo)'):
                obra.estado = ObraPublica.EN_EJECUCION_POR_AMPLIACION
            elif estado == normalizar('Finalizada'):
                obra.estado = ObraPublica.FINALIZADA
            elif estado == normalizar('Rescindida'):
                obra.estado = ObraPublica.RESCINDIDA

            if obra.monto is None:
                self.stdout.write(
                    self.style.ERROR(
                        'Monto inválido {} {}'.format(
                            obra.nombre, dato)))
                obra.monto = 0.0  # error desconocido
            obra.save()

            log = 'Obras: {}-{} Tipos {}-{} Contratistas {}-{} Barrios: {}-{} )'.format(
                obras_nuevas,
                obras_repetidas,
                tipos_nuevos,
                tipos_repetidos,
                organizaciones_nuevas,
                organizaciones_repetidas,
                barrios_nuevos,
                barrios_repetidos)
            self.stdout.write(self.style.SUCCESS(log))

        if len(errores) > 0:
            self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR'))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))

            html_mensaje = render_to_string(
                'core/mails/errores_en_obras_publicas.html', {'errores': errores})
            qm = QhapaxMail()
            subject = "Errores en datos de Obras Públicas - Municipalidad de Córdoba"
            qm.send_mail(subject, html_mensaje, to="wilmays10@gmail.com")

        self.stdout.write(self.style.SUCCESS('FIN'))
