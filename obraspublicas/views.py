from django.views.decorators.cache import cache_page
from .models import ObraPublica, TipoObraPublica, TrazadoObraPublica
from django.http import HttpResponse
import django_excel as excel
from django.utils.text import slugify


@cache_page(60 * 60)  # 1 hora
def lista_obras_publicas(request, filetype):
    '''
    lista de obras publicas en la ciudad
    '''
    obras = ObraPublica.objects.filter(publicado=True, publicado_gobierno=True)

    tipos_id_str = request.GET.get('tipo_id', None)
    if tipos_id_str is not None:
        tipos_id = tipos_id_str.split(',')
        obras = obras.filter(tipo__id__in=tipos_id)

    avance_desde = request.GET.get('avance_desde', None)
    if avance_desde is not None:
        obras = obras.filter(porcentaje_completado__gte=int(avance_desde))

    avance_hasta = request.GET.get('avance_hasta', None)
    if avance_hasta is not None:
        obras = obras.filter(porcentaje_completado__lte=int(avance_hasta))

    estado = request.GET.get('estado', None)
    if estado is not None:
        if estado == 'proyecto':
            obras = [obj for obj in obras if obj.estado == 'proyecto']
        elif estado == 'licitada':
            obras = [obj for obj in obras if obj.estado == 'licitada']
        elif estado == 'en_progreso':
            obras = [obj for obj in obras if obj.estado == 'en_progreso']
        elif estado == 'finalizada':
            obras = [obj for obj in obras if obj.estado == 'finalizada']

    barrios_str = request.GET.get('barrios_ids', None)
    if barrios_str is not None:
        barrios_ids = barrios_str.split(',')
        obras = obras.filter(barrios__in=barrios_ids)

    csv_list = []
    csv_list.append(['ID obra',
                     'Obra',
                     'Decreto',
                     'Expediente',
                     '% completado',
                     'tipo',
                     'Organización',
                     'Fecha inicio',
                     'Monto $',
                     'Fecha finalización estimada',
                     'Barrios'])

    for obra in obras:

        nombre = 'Sin cargar aún' if obra.organizacion is None else obra.organizacion.nombre
        cuit = '' if obra.organizacion is None else obra.organizacion.CUIT
        org = '{} {}'.format(nombre, cuit)
        barrios = ''
        for barrio in obra.barrios.all():
            barrios += '{} - '.format(barrio.nombre)

        fecha_inicio = '' if obra.fecha_inicio is None else obra.fecha_inicio.date()
        fecha_fin = '' if obra.fecha_finalizacion_estimada is None else obra.fecha_finalizacion_estimada.date()
        csv_list.append([obra.id,
                         obra.nombre,
                         obra.decreto,
                         obra.expediente_origen,
                         obra.porcentaje_completado,
                         obra.tipo.nombre,
                         org,
                         fecha_inicio,
                         obra.monto,
                         fecha_fin,
                         barrios])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60)  # 1 hora
def lista_frentes_obras_publicas(request, filetype):
    '''
    lista de paradas de transporte
    '''
    obras = ObraPublica.objects.filter(publicado=True, publicado_gobierno=True)

    csv_list = []
    csv_list.append(['ID Obra', 'Obra', 'ID frente',
                     'Descripción frente de obra'])

    for obra in obras:

        nombre = 'Sin cargar aún' if obra.organizacion is None else obra.organizacion.nombre
        cuit = '' if obra.organizacion is None else obra.organizacion.CUIT
        org = '{} {}'.format(nombre, cuit)

        barrios = ''
        for barrio in obra.barrios.all():
            barrios += '{} - '.format(barrio.nombre)

        trazados = TrazadoObraPublica.objects.filter(
            obra=obra, publicado=True, publicado_gobierno=True)

        id_obra = request.GET.get('id_obra', None)
        if id_obra is not None:
            trazados = trazados.filter(obra__id=int(id_obra))

        tipos_id_str = request.GET.get('tipo_id', None)
        if tipos_id_str is not None:
            tipos_id = tipos_id_str.split(',')
            trazados = trazados.filter(obra__tipo__id__in=tipos_id)

        obra_avance_desde = request.GET.get('obra_avance_desde', None)
        if obra_avance_desde is not None:
            trazados = trazados.filter(
                obra__porcentaje_completado__gte=int(obra_avance_desde))

        obra_avance_hasta = request.GET.get('obra_avance_hasta', None)
        if obra_avance_hasta is not None:
            trazados = trazados.filter(
                obra__porcentaje_completado__lte=int(obra_avance_hasta))

        barrios_str = request.GET.get('barrios_ids', None)
        if barrios_str is not None:
            barrios_ids = barrios_str.split(',')
            trazados = trazados.filter(obra__barrios__in=barrios_ids)

        for trazado in trazados:
            if trazado.descripcion_publica is not None:
                descripcion = trazado.descripcion_publica
            else:
                descripcion = 'Trazado {}-{}. Sin descripción aún '.format(
                    trazado.id, trazado.id_externo)

            csv_list.append([obra.id, obra.nombre, trazado.id, descripcion])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60)  # 1 hora
def mapa_obras_publicas(request, filetype):
    ''' mapa de las obras públcias en KML o en (geo)JSON '''

    content_type = "application/json" if filetype == 'json' else 'application/vnd.google-earth.kml+xml'
    # FIXME agregar la salida geoJson por si fuera necesaria

    obras = ObraPublica.objects.filter(publicado=True, publicado_gobierno=True)
    # referencias del formato KML
    # https://developers.google.com/kml/documentation/kmlreference

    kml = ('<?xml version="1.0" encoding="UTF-8"?>'
           '<kml xmlns="http://www.opengis.net/kml/2.2">'
           '<Document>'
           '<name>ObrasPublicasCordoba.kml</name>'
           '{styles}'
           '{doc}'
           '</Document>'
           '</kml>')

    tipos = TipoObraPublica.objects.all()
    styles = []
    for tipo in tipos:
        tipo_slug = slugify(tipo.nombre)
        style = ('<Style id="{tipo_slug}">\n'
                 '<LineStyle>\n'
                 '<color>{color}</color>\n'
                 '<width>4</width>\n'
                 '</LineStyle>\n'
                 '</Style>\n'.format(tipo_slug=tipo_slug, color=tipo.color))

        styles.append(style)

    styles_str = ''.join(styles)

    places = []
    for obra in obras:
        if obra.ubicacion is None:
            continue
        tipo_slug = slugify(obra.tipo.nombre)
        place = (
            '<Placemark>\n'
            '<name><![CDATA[{nombre}]]></name>\n'
            '<styleUrl>#{tipo_slug}</styleUrl>\n'
            '<ExtendedData>'
            '<Data name="Decreto"><value><![CDATA[{decreto}]]></value></Data>'
            '<Data name="Expediente"><value><![CDATA[{expediente_origen}]]></value></Data>'
            '<Data name="Porcentaje de avance"><value><![CDATA[{porcentaje_completado}]]></value></Data>'
            '<Data name="Tipo"><value><![CDATA[{tipo}]]></value></Data>'
            '<Data name="Organización"><value><![CDATA[{organizacion}]]></value></Data>'
            '<Data name="Monto"><value><![CDATA[{monto}]]></value></Data>'
            '<Data name="Fecha de inicio"><value><![CDATA[{fecha_inicio}]]></value></Data>'
            '<Data name="Fecha de finalización estimada"><value><![CDATA[{fecha_finalizacion_estimada}]]></value></Data>'
            '<Data name="Barrios"><value><![CDATA[{barrios}]]></value></Data>'
            '</ExtendedData>'
            '{geom}'
            '</Placemark>\n')

        nombre = 'Sin cargar aún' if obra.organizacion is None else obra.organizacion.nombre
        cuit = '' if obra.organizacion is None else obra.organizacion.CUIT
        org = '{} {}'.format(nombre, cuit)

        monto = '{} {}'.format(obra.moneda_monto, obra.monto)
        barrios = ''
        for barrio in obra.barrios.all():
            barrios += '{} - '.format(barrio.nombre)
        geo = place.format(
            nombre=obra.nombre,
            decreto=obra.decreto,
            expediente_origen=obra.expediente_origen,
            porcentaje_completado=obra.porcentaje_completado,
            geom=obra.ubicacion.kml,
            tipo=obra.tipo.nombre,
            organizacion=org,
            fecha_inicio=obra.fecha_inicio,
            monto=monto,
            fecha_finalizacion_estimada=obra.fecha_finalizacion_estimada,
            barrios=barrios,
            tipo_slug=tipo_slug)

        places.append(geo)

    places_str = ''.join(places)
    data = kml.format(doc=places_str, styles=styles_str)
    return HttpResponse(data, content_type=content_type)


@cache_page(60 * 60)  # 1 hora
def mapa_trazado_obras_publicas(request, filetype):
    ''' mapa de las obras públcias en KML o en (geo)JSON '''

    content_type = "application/json" if filetype == 'json' else 'application/vnd.google-earth.kml+xml'
    # FIXME agregar la salida geoJson por si fuera necesaria

    obras = ObraPublica.objects.filter(publicado=True, publicado_gobierno=True)
    # referencias del formato KML
    # https://developers.google.com/kml/documentation/kmlreference

    kml = ('<?xml version="1.0" encoding="UTF-8"?>'
           '<kml xmlns="http://www.opengis.net/kml/2.2">'
           '<Document>'
           '<name>TrazadoObrasPublicasCordoba.kml</name>'
           '{styles}'
           '{folders}'
           '</Document>'
           '</kml>')

    tipos = TipoObraPublica.objects.all()
    styles = []
    for tipo in tipos:
        tipo_slug = slugify(tipo.nombre)
        style = ('<Style id="{tipo_slug}">\n'
                 '<LineStyle>\n'
                 '<color>{color}</color>\n'
                 '<width>4</width>\n'
                 '</LineStyle>\n'
                 '</Style>\n'.format(tipo_slug=tipo_slug, color=tipo.color))

        styles.append(style)

    styles_str = ''.join(styles)

    folders = []
    for obra in obras:
        trazado_obras = TrazadoObraPublica.objects.filter(
            publicado=True, publicado_gobierno=True, obra=obra)

        obra_slug = slugify(obra.nombre)
        tipo_slug = slugify(obra.tipo.nombre)

        nombre = 'Sin cargar aún' if obra.organizacion is None else obra.organizacion.nombre
        cuit = '' if obra.organizacion is None else obra.organizacion.CUIT
        org = '{} {}'.format(nombre, cuit)

        monto = '{} {}'.format(obra.moneda_monto, obra.monto)
        barrios = ''
        for barrio in obra.barrios.all():
            barrios += '{} - '.format(barrio.nombre)

        folder = (
            '<Folder>\n'
            '<name><![CDATA[{nombre}]]></name>\n'
            '<description><![CDATA[{tipo}: OBRA: {nombre}]]></name>\n'
            '<open>1</open>'
            '<ExtendedData>'
            '<Data name="Decreto"><value><![CDATA[{decreto}]]></value></Data>'
            '<Data name="Expediente"><value><![CDATA[{expediente_origen}]]></value></Data>'
            '<Data name="Porcentaje de avance"><value><![CDATA[{porcentaje_completado}]]></value></Data>'
            '<Data name="Tipo"><value><![CDATA[{tipo}]]></value></Data>'
            '<Data name="Organización"><value><![CDATA[{organizacion}]]></value></Data>'
            '<Data name="Monto"><value><![CDATA[{monto}]]></value></Data>'
            '<Data name="Fecha de inicio"><value><![CDATA[{fecha_inicio}]]></value></Data>'
            '<Data name="Fecha de finalización estimada"><value><![CDATA[{fecha_finalizacion_estimada}]]></value></Data>'
            '<Data name="Barrios"><value><![CDATA[{barrios}]]></value></Data>'
            '</ExtendedData>'
            '{docs}'
            '</Folder>\n')

        places = []
        for trazado in trazado_obras:
            place = (
                '<Placemark>\n'
                '<name><![CDATA[{descripcion_publica}]]></name>\n'
                '<description><![CDATA[{tipo_nombre}: OBRA: {nombre}]]></name>\n'
                '<styleUrl>#{tipo_slug}</styleUrl>\n'
                '{geom}'
                '</Placemark>\n')
            geo = place.format(
                descripcion_publica=trazado.descripcion_publica,
                tipo_nombre=obra.tipo.nombre,
                nombre=obra.nombre,
                geom=trazado.trazado.kml,
                tipo=trazado.obra.tipo.nombre,
                tipo_slug=tipo_slug)

            places.append(geo)

        places_str = ''.join(places)
        folder_final = folder.format(
            nombre=obra.nombre,
            decreto=obra.decreto,
            expediente_origen=obra.expediente_origen,
            porcentaje_completado=obra.porcentaje_completado,
            tipo=obra.tipo.nombre,
            organizacion=org,
            fecha_inicio=obra.fecha_inicio,
            monto=monto,
            fecha_finalizacion_estimada=obra.fecha_finalizacion_estimada,
            barrios=barrios,
            docs=places_str)

        folders.append(folder_final)

    folders_str = ''.join(folders)
    data = kml.format(folders=folders_str, styles=styles_str)
    return HttpResponse(data, content_type=content_type)
