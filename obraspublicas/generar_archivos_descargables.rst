Generación de archivos para descargar
=====================================

En el siguiente link, se puede descargar la información de las obras en formato xls o csv:

- obras públicas en formato xls: https://gobiernoabierto.cordoba.gob.ar/obras-publicas/lista-obras-publicas.xls
- obras públicas en formato csv: https://gobiernoabierto.cordoba.gob.ar/obras-publicas/lista-obras-publicas.csv
- frentes de obras públicas en formato xls: https://gobiernoabierto.cordoba.gob.ar/obras-publicas/lista-frentes-obras-publicas.xls
- frentes de obras públicas en formato csv: https://gobiernoabierto.cordoba.gob.ar/obras-publicas/lista-frentes-obras-publicas.csv

Se puede filtrar la información antes de descargar el archivo por los siguientes parámetros:

- por identificador del tipo de obra: con el parámetro **tipo_id**, pueden ser varios separados por comas.
  Ejemplo: https://gobiernoabierto.cordoba.gob.ar/obras-publicas/lista-obras-publicas.xls?tipo_id=1,2
- por barrio: con el parámetro **barrios_ids**, pueden ser varios separados por comas.
  Ejemplo: https://gobiernoabierto.cordoba.gob.ar/obras-publicas/lista-obras-publicas.csv?barrios_ids=2,3
- por texto: con el parámetro **texto**, se filtra las obras vinculadas a algún texto en particular.
  Ejemplo: https://gobiernoabierto.cordoba.gob.ar/obras-publicas/lista-obras-publicas.csv?texto=plaza españa
