from django.conf.urls import url

from . import views


urlpatterns = [url(r'^obras-publicas.(?P<filetype>kml|json)$',
                   views.mapa_obras_publicas,
                   name='obras-publicas.mapa'),
               url(r'^trazado-obras-publicas.(?P<filetype>kml|json)$',
                   views.mapa_trazado_obras_publicas,
                   name='trazado-obras-publicas.mapa'),
               url(r'^lista-obras-publicas.(?P<filetype>csv|xls)$',
                   views.lista_obras_publicas,
                   name='obras-publicas.lista'),
               url(r'^lista-frentes-obras-publicas.(?P<filetype>csv|xls)$',
                   views.lista_frentes_obras_publicas,
                   name='frentes-obras-publicas.lista'),
               ]
