from django.contrib import admin
from .models import LogUsuario, DetalleLogUsuario
from mensajes_a_usuarios.models import MensajeDirecto, Mensaje
from core.models import get_recorrido_cordobus


class DetalleLogUsuarioInLine(admin.StackedInline):
    model = DetalleLogUsuario
    extra = 0


@admin.register(LogUsuario)
class LogUsuarioAdmin(admin.ModelAdmin):
    def email(self, obj):
        if obj.user is None:
            return 'Desconocido'
        else:
            return obj.user.email

    def version(self, obj):
        for detalle in obj.detalles.all():
            if detalle.codigo in ['Version', 'Versión']:
                return detalle.str_val
        return None

    def opinion(self, obj):
        for detalle in obj.detalles.all():
            if detalle.codigo in ['opinion', 'opinión']:
                return detalle.str_val
        return None

    def valoracion(self, obj):
        for detalle in obj.detalles.all():
            if detalle.codigo in ['valoracion', 'valoración']:
                return detalle.str_val
        return None

    def recorrido(self, obj):
        for detalle in obj.detalles.all():
            if detalle.codigo in ['recorrido_id']:
                recorrido_id = detalle.int_val
                recorrido = get_recorrido_cordobus(pk=recorrido_id)
                if len(recorrido) == 0:
                    return 'Desconocido'
                recorrido = recorrido[0]
                linea = recorrido['properties']['linea']
                return '{} {}'.format(
                    linea['nombre_publico'],
                    recorrido['properties']['descripcion_cuando_llega'])
        return None

    def error(self, obj):
        for detalle in obj.detalles.all():
            if detalle.codigo in ['detalle_error']:
                return detalle.str_val
        return None

    def respondido(self, obj):
        mensaje = MensajeDirecto.objects.filter(object_id=obj.id)
        result = None
        if mensaje.count() > 0:
            mensaje = mensaje[0]
            if mensaje.estado == Mensaje.EST_EN_EDICION:
                result = 'NO'
            else:
                result = 'SI'
        return result

    list_per_page = 10
    list_display = [
        'user',
        'email',
        'sistema',
        'created',
        'codigo_accion',
        'observaciones',
        'version',
        'valoracion',
        'opinion',
        'recorrido',
        'error',
        'respondido']

    inlines = [DetalleLogUsuarioInLine]
    list_filter = ['codigo_accion']
    search_fields = (
        'sistema__titulo',
        'sistema__descripcion',
        'user__username')
