# Log de usuarios

Registros de acciones de usaurios de este y otros sistemas.  
Creado para loguear opiniones de gente en apps móviles del gobierno pero amplio para referir a muchos otros datos que dban ser trackeados.  

Es un módulo que pretende ser muy genérico y apto para todo tipo de registros.  

Prueba
```
curl -X POST \
    -v \
    -d '{"sistema": "1", "codigo_accion": "polonia", "detalles": [{"codigo": "cod1", "int_val": 4}, {"codigo": "cod2", "str_val": "poloniense"}] }' \
    -H "Authorization: Token y6y6y6y6y6y6yy66y" \
    -H "Content-Type: application/json" \
    http://localhost:8000/api/v2/log-usuarios/log/ 
```