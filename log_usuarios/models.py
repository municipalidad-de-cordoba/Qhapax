from django.db import models
from softwaremunicipal.models import SoftwarePublico, UsuarioSoftware
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from mensajes_a_usuarios.models import MensajeDirecto, Mensaje


class LogUsuario(models.Model):
    '''
    Acciones de los usuarios en sistemas varios que loguean acá
    (apps móviles por ejemplo)
    '''

    id_usuario = models.PositiveIntegerField(
        default=0, help_text='Id en algún sistema externo')
    user = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL)

    sistema = models.ForeignKey(SoftwarePublico, null=True, blank=True)

    codigo_accion = models.CharField(
        max_length=40, help_text='Codigo de la accion desarrollada')
    observaciones = models.TextField(null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        """
        Se sobreescribe para crear mensajes cada vez que un usuario de Go
        emite un opinión y se pueda responder por la app.
        """
        # sólo se genera un nuevo mensaje cuando el log es nuevo
        if not self.pk:
            if self.codigo_accion == 'OPINA':
                try:
                    detalle = DetalleLogUsuario.objects.get(
                        log_usuario__id=self.id)
                    mensaje = MensajeDirecto.objects.create(
                        usuario=UsuarioSoftware.objects.get(user=self.user),
                        estado=Mensaje.EST_EN_EDICION,
                        mensaje_html='<br> <br> --------------------------------------------'
                        '----------------------<br>{}'.format(detalle.str_val),
                        content_type=ContentType.objects.get_for_model(LogUsuario),
                        object_id=self.id
                    )
                except Exception as e:
                    # No debería entrar nunca acá, en caso que suceda no se
                    # crea el mensaje
                    pass
        super(LogUsuario, self).save(*args, **kwargs)

    def __str__(self):
        return 'log id {}'.format(self.id)


class DetalleLogUsuario(models.Model):
    ''' valores extras de un log particular '''
    log_usuario = models.ForeignKey(
        LogUsuario,
        on_delete=models.CASCADE,
        related_name='detalles')
    codigo = models.CharField(
        max_length=40,
        help_text='Nombre del valor usado')
    decimal_val = models.DecimalField(
        null=True,
        blank=True,
        decimal_places=8,
        max_digits=20)
    int_val = models.PositiveIntegerField(null=True, blank=True)
    str_val = models.TextField(null=True, blank=True)

    def __str__(self):
        return '{} {} {} {}'.format(
            self.codigo,
            self.decimal_val,
            self.int_val,
            self.str_val)
