from django.conf.urls import url, include

urlpatterns = [
    url(r'^v2/', include('log_usuarios.api.v2.urls')),
]
