from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from log_usuarios.models import LogUsuario, DetalleLogUsuario


class DetalleLogUsuarioSerializer(CachedSerializerMixin):

    class Meta:
        model = DetalleLogUsuario
        fields = ['log_usuario', 'codigo', 'decimal_val', 'int_val', 'str_val']


class LogUsuarioSerializer(CachedSerializerMixin):

    class Meta:
        model = LogUsuario
        fields = ['id_usuario', 'sistema', 'codigo_accion', 'observaciones']


# Registro los serializadores en la cache de DRF
cache_registry.register(DetalleLogUsuarioSerializer)
cache_registry.register(LogUsuarioSerializer)
