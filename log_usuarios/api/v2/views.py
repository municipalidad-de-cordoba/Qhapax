import json
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from api.pagination import DefaultPagination
from log_usuarios.models import LogUsuario, DetalleLogUsuario
from .serializers import LogUsuarioSerializer, DetalleLogUsuarioSerializer
from rest_framework.exceptions import MethodNotAllowed
from softwaremunicipal.models import SoftwarePublico, UsuarioSoftware
from rest_framework.response import Response
import logging
logger = logging.getLogger(__name__)


class LogUsuarioViewSet(viewsets.ModelViewSet):
    """ Log de algun usuario de alguna acción específica """
    serializer_class = LogUsuarioSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = DefaultPagination

    def create(self, request):

        # logger.info('data create LogUsuario {}'.format(request.data))
        data = request.data.copy()

        # data create LogUsuario <QueryDict:
        #   {'codigo_accion': ['INGRESO'], 'sistema': ['2'],
        #   'detalles':
        #       ['[{"codigo":"latitud","decimal_val":123.456}, {"codigo":"longitud","decimal_val":123.456}, {"codigo":"Version","str_val":"VERSION_APP"}]']
        # }>
        detalles = data.pop('detalles')

        # si no existe sista sale con error
        sistema = SoftwarePublico.objects.get(pk=data['sistema'])
        data['id_usuario'] = request.user.id  # lo pisa si viene inventado

        # ---------------------------------------------------------------
        # ---------------------------------------------------------------
        # HAY UN USUARIO LOGUEADO USANDO UN SOFTWARE ESPECIFICO
        # LO MARCO COMO USUARIO OFICIAL DE ESE SOFTWARE PARA ENVIARLE
        # NOTIFICACIONES SI FUERA NECESARIO
        # Quizas esto deba existir un registro mas formal de esta asociacion
        # #FIXME
        uss = UsuarioSoftware.objects.filter(
            user=request.user, software=sistema)
        if uss.count() == 0:
            us = UsuarioSoftware.objects.create(
                user=request.user, software=sistema)
        else:
            us = uss[0]  # QUE SE YO. SE DUPLICAN

        # QUE SE YO. SE DUPLICAN cuando uso esto:
        # us, created = UsuarioSoftware.objects.get_or_create(user=request.user, software=sistema)
        # ASI SE RESUELVE EN EL SHELL
        """
        casos = UsuarioSoftware.objects.values('software', 'user').annotate(uss=Count('software')).filter(uss__gt=1)
        for caso in casos:
            user = User.objects.get(pk=caso["user"])
            usuarios_soft = UsuarioSoftware.objects.filter(user=user).order_by('-modified')
            diff = usuarios_soft[0].modified - usuarios_soft[1].modified
            print('@{} DIFF {} .{} {}'.format(user.username, diff, usuarios_soft[0].modified, usuarios_soft[1].modified))
            usuarios_soft[1].delete()
        """
        # ---------------------------------------------------------------
        # ---------------------------------------------------------------

        us.touch()  # contador de accesos o usos
        # ---------------------------------------------------------------
        # ---------------------------------------------------------------

        serializer = LogUsuarioSerializer(data=data)
        if not serializer.is_valid():
            res = {'ok': False, 'error': 'Error al serializar el log',
                   'data': data, 'errors': serializer.errors.copy()}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        self.perform_create(serializer)
        log = serializer.save()

        log.user = request.user
        log.save()

        headers = self.get_success_headers(serializer.data)

        # detalles viene raro
        # logger.info('detalles create LogUsuario {}'.format(detalles))
        if isinstance(detalles[0], str):
            # viene asi (lista con un string), no preguntes porque
            detalles_obj = json.loads(detalles[0])
        elif isinstance(detalles[0], dict):
            detalles_obj = detalles

        for detalle in detalles_obj:
            # logger.info('DETALLE create LogUsuario {}'.format(detalle))
            detalle['log_usuario'] = log.id  # darle el ID para conectarlo

            serializer2 = DetalleLogUsuarioSerializer(data=detalle)
            if not serializer2.is_valid():
                res = {
                    'ok': False,
                    'error': 'Error al serializar detalles del log',
                    'detalle': detalle,
                    'data': detalle,
                    'errors': serializer2.errors.copy()}
                return Response(res, status=status.HTTP_400_BAD_REQUEST)

            self.perform_create(serializer2)

        return Response({'ok': True, 'error': ''}, headers=headers)

    # no se puede leer la lista, solo escribir

    def list(self, request, *args, **kwargs):
        raise MethodNotAllowed("GET")

    # no se puede leer un registro, solo escribir
    def retrieve(self, request, pk=None):
        raise MethodNotAllowed("GET")
