from django.conf.urls import url, include
from rest_framework import routers
from .views import LogUsuarioViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'log', LogUsuarioViewSet, base_name='log-usuario')

urlpatterns = [
    url(r'^', include(router.urls)),
]
