from django.contrib import admin
from .models import (PlanForestal, ZonaForestal, BarrioPF, EspecieArbol,
                     FotoEspecieArbol, VeredaPF)


class PlanForestalAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    list_display = ('nombre', 'id')


class ZonaForestalAdmin(admin.ModelAdmin):
    list_display = ('plan', 'id_en_plan')


class BarrioPFAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    list_display = ('zona', 'nombre', 'id_en_plan')


class EspecieArbolAdmin(admin.ModelAdmin):
    search_fields = ['nombre', 'nombre_cientifico']
    list_display = (
        'nombre',
        'nombre_cientifico',
        'plan',
        'id_en_plan',
        'mas_info_url')


class FotoEspecieArbolAdmin(admin.ModelAdmin):
    list_display = ('especie_arbol', 'foto')


class VeredaPFAdmin(admin.ModelAdmin):
    search_fields = ['nombre', 'nombre_cientifico']
    list_display = ('nombre', 'ancho_desde', 'ancho_hasta',
                    'distancia_desde', 'distancia_hasta',
                    'plan', 'id_en_plan')


admin.site.register(PlanForestal, PlanForestalAdmin)
admin.site.register(ZonaForestal, ZonaForestalAdmin)
admin.site.register(BarrioPF, BarrioPFAdmin)
admin.site.register(EspecieArbol, EspecieArbolAdmin)
admin.site.register(FotoEspecieArbol, FotoEspecieArbolAdmin)
admin.site.register(VeredaPF, VeredaPFAdmin)
