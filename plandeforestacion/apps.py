from django.apps import AppConfig


class PlandeforestacionConfig(AppConfig):
    name = 'plandeforestacion'
