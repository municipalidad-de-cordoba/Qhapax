from django.db import models
from versatileimagefield.fields import VersatileImageField


class PlanForestal(models.Model):
    ''' Esperamos que este modelo permita contener tambien
    los siguientes planes forestales
    '''
    nombre = models.CharField(max_length=90)  # en general será el año

    def __str__(self):
        return self.nombre


class ZonaForestal(models.Model):
    ''' En realidad el plan forestal esta basado en zonas.
    Estas zonas definen despues los barrios (que es mas comprensible al vecino)
    donde se utilizan las diferentes especies
    '''
    plan = models.ForeignKey(PlanForestal, on_delete=models.CASCADE)
    # id en el sistema del plan de forestacion
    id_en_plan = models.PositiveIntegerField(default=0)

    def __str__(self):
        return 'ZONA {}'.format(self.id_en_plan)


class BarrioPF(models.Model):
    ''' Barrios de la base del plan forestal.
        REQUIERE integración a la base de datos oficial de la muni de barrios
    '''
    zona = models.ForeignKey(ZonaForestal, on_delete=models.CASCADE)
    # id en el sistema del plan de forestacion
    id_en_plan = models.PositiveIntegerField(default=0)
    nombre = models.CharField(max_length=90)

    def __str__(self):
        return 'BARRIO {}'.format(self.nombre)


class EspecieArbol(models.Model):
    ''' Todas las especies de árboles contempladas para un plan forestal específico '''
    plan = models.ForeignKey(PlanForestal, on_delete=models.CASCADE)
    # id en el sistema del plan de forestacion
    id_en_plan = models.PositiveIntegerField(default=0)
    nombre = models.CharField(max_length=90)
    nombre_cientifico = models.CharField(max_length=90)
    mas_info_url = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']


class FotoEspecieArbol(models.Model):
    especie_arbol = models.ForeignKey(EspecieArbol, on_delete=models.CASCADE)
    foto = VersatileImageField(
        upload_to='imagenes/especies-arboles',
        null=True,
        blank=True)


class VeredaPF(models.Model):
    """ en el plan de forestación son importantes las especificaciones técnicas de la vereda """
    plan = models.ForeignKey(PlanForestal, on_delete=models.CASCADE)
    # id en el sistema del plan de forestacion
    id_en_plan = models.PositiveIntegerField(default=0)
    nombre = models.CharField(max_length=90)
    # valores en metros
    ancho_desde = models.DecimalField(
        max_digits=6, decimal_places=2, default=0.0)
    ancho_hasta = models.DecimalField(
        max_digits=6, decimal_places=2, default=0.0)
    distancia_desde = models.DecimalField(
        max_digits=6, decimal_places=2, default=0.0)
    distancia_hasta = models.DecimalField(
        max_digits=6, decimal_places=2, default=0.0)

    def __str__(self):
        return '{} {}-{}'.format(self.nombre,
                                 self.ancho_desde, self.ancho_hasta)
