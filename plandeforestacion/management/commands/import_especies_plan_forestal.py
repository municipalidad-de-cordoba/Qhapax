#!/usr/bin/python
"""
Importar nacimientos del web service del registro civil
"""
from django.core.management.base import BaseCommand
from plandeforestacion.models import PlanForestal, EspecieArbol
from portaldedatos.models import ArchivoCSV
from django.db import transaction
import sys
import csv


class Command(BaseCommand):
    help = """Comando para Importar las especies de árboles a usar en un plan de forestación """

    def add_arguments(self, parser):
        parser.add_argument(
            '--plan_id',
            nargs='?',
            type=int,
            help='Id del plan donde se colovaran los datos')
        parser.add_argument(
            '--csv_id',
            nargs='?',
            type=int,
            help='ID del CSV cargado al sistema con las especies')

    @transaction.atomic
    def handle(self, *args, **options):

        plan_id = options['plan_id']
        if plan_id > 0:
            plan = PlanForestal.objects.get(pk=plan_id)
        else:
            plan = PlanForestal(nombre='Plan Forestal 2016')

        self.stdout.write(
            self.style.ERROR(
                'Se eligió el plan "{}"'.format(
                    plan.nombre)))

        instanceCSV = ArchivoCSV.objects.get(pk=options['csv_id'])

        if instanceCSV is None:
            self.stdout.write(self.style.ERROR('No existe el CSV'))
            sys.exit(1)

        self.stdout.write(
            self.style.SUCCESS(
                'Importando csv (id: {})'.format(
                    instanceCSV.id)))

        # TIENEN QUE TENER ENCABEZADO!
        if not instanceCSV.tiene_fila_encabezado:
            self.stdout.write(self.style.ERROR(
                'El CSV indica que no tiene encabezado. Se procesará igualemente'))

        # asegurarse de que el archivo tenga la misma estructura (encabezados)
        fieldnames = ["Id", "Nombre", "Nombre_cient", "Info"]
        """ ejemplo de datos
        "Id","Nombre","Nombre_cient","Info"
        "1","Limpiatubos","Callistemom sp.","http://es.wikipedia.org/wiki/Callistemon"
        "2","Ligustro Aureo","L. lucidum var. aureovariegatum",""
        """

        with open(instanceCSV.archivo_local.path) as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=instanceCSV.separado_por,
                                    quotechar=instanceCSV.contenedor_de_texto)

            header = reader.__next__()
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR(
                    'BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            nuevos = 0
            repetidos = 0

            for row in reader:

                id_en_plan = row["Id"]
                especie, created = EspecieArbol.objects.get_or_create(
                    plan=plan, id_en_plan=id_en_plan)
                if created:
                    nuevos += 1
                else:
                    repetidos += 1

                especie.nombre = row["Nombre"]
                especie.nombre_cientifico = row["Nombre_cient"]
                mas_info_url = row["Info"]
                if mas_info_url.lower().startswith('http'):
                    especie.mas_info_url = mas_info_url
                especie.save()

        self.stdout.write(
            self.style.SUCCESS(
                'Archivo cargado con éxito, se cargaron {} registros nuevos ({} repetidos)'.format(
                    nuevos,
                    repetidos)))
