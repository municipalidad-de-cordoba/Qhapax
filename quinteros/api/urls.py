from django.conf.urls import url, include
from rest_framework import routers
from .views import QuintaQuinteroViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'info-quinteros', QuintaQuinteroViewSet, base_name='api.info.quinteros')

urlpatterns = [
    url(r'^', include(router.urls)),
]