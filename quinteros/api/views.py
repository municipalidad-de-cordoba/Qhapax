from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import QuintaQuinteroSerializer
from api.pagination import DefaultPagination
from quinteros.models import QuintaQuintero
from django.db.models import Q


class QuintaQuinteroViewSet(viewsets.ModelViewSet):
    """
    Lista de quinteros relacionada con sus quintas.
    Con el filtro 'q' vamos a poder filtrar por nombre, apellido,
    Dni o cuil, nave y codigo renspa del quintero.
    Ej: API/?q=Diaz, API/?q=21547856, etc
    """
    serializer_class = QuintaQuinteroSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = QuintaQuintero.objects.all().order_by('id')

        # filtro por texto
        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(quintero__nombre__icontains=q) |
                Q(quintero__apellido__icontains=q) |
                Q(quintero__unique_id__icontains=q) |
                Q(quintero__nave__nombre__icontains=q) |
                Q(codigo_renspa__icontains=q))

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

