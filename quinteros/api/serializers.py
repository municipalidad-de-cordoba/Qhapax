from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from quinteros.models import QuintaQuintero, Quintero ,Quinta
from rest_framework import serializers
from versatileimagefield.serializers import VersatileImageFieldSerializer



class QuinteroSerializer(CachedSerializerMixin):
    foto =  VersatileImageFieldSerializer([("original", 'url'),
                                            ("thumbnail_32x32",
                                             'thumbnail__32x32'),
                                            ("thumbnail", 'thumbnail__125x125')])
    nave = serializers.CharField(source='nave.nombre')

    class Meta:
        model = Quintero
        fields = ['id', 'nombre', 'apellido', 'unique_id', 'nave', 'foto']


class QuintaSerializer(CachedSerializerMixin):

    class Meta:
        model = Quinta
        fields = ['id', 'nombre', 'domicilio']


class QuintaQuinteroSerializer(CachedSerializerMixin):
    quintero = QuinteroSerializer()
    quinta = QuintaSerializer()
    relacion = serializers.SerializerMethodField()

    def get_relacion(self, obj):
        return obj.get_relacion_display()

    class Meta:
        model = QuintaQuintero
        fields = ['id', 'relacion', 'codigo_renspa', 'quintero', 'quinta']


cache_registry.register(QuintaQuinteroSerializer)
cache_registry.register(QuintaSerializer)
cache_registry.register(QuinteroSerializer)
