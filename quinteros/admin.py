from django.contrib import admin
from core.admin import QhapaxOSMGeoAdmin
from .models import *
from django.db.models import Max, Count
from .forms import AyudanteQuinteroForm, QuintaQuinteroForm, InspeccionForm, SuspensionQuinteroForm, HabilitacionForm



class NaveInline(admin.StackedInline):
    model = Nave
    exclude = ('poligono', )
    extra = 1


class FeriaAdmin(QhapaxOSMGeoAdmin):
    list_display = ('nombre','domicilio', 'ciudad', 'descripcion')
    inlines = [NaveInline]


class NaveAdmin(admin.ModelAdmin):
    list_display = ('nombre', )
    exclude = ('poligono', )


class QuintaAdmin(QhapaxOSMGeoAdmin):
    list_display = ('domicilio', 'ciudad', 'hectareas', 'nombre')
    search_fields = ['domicilio', 'ciudad__nombre', 'hectareas', 'nombre']
    list_filter = ['ciudad__nombre']


class FiltroHabilitacionQuintero(admin.SimpleListFilter):
    title = 'estado habilitación quintero'

    parameter_name = 'quintero_estado_habilitacion'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return Quintero.estados_habilitaciones.items()

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        v = None if self.value() is None else int(self.value())
        if v == Quintero.EST_HABILITADO:
            q = queryset.annotate(vto=Max('habilitacionquintero__fecha_fin')).filter(vto__gt=date.today())
        elif v == Quintero.EST_SIN_HABILITACION:
            q = queryset.annotate(habs=Count('habilitacionquintero')).filter(habs=0)
        elif v == Quintero.EST_HABILITACION_VENCIDA:
            q = queryset.annotate(vto=Max('habilitacionquintero__fecha_fin')).filter(vto__lt=date.today())
        elif v == Quintero.EST_VENCE_EN_7:
            q = queryset.annotate(vto=Max('habilitacionquintero__fecha_fin')).filter(vto__gt=date.today(), vto__lt=date.today() + timedelta(days=7))
        elif v == Quintero.EST_VENCE_EN_15:
            q = queryset.annotate(vto=Max('habilitacionquintero__fecha_fin')).filter(vto__gt=date.today(), vto__lt=date.today() + timedelta(days=15))
        elif v == Quintero.EST_VENCE_EN_30:
            q = queryset.annotate(vto=Max('habilitacionquintero__fecha_fin')).filter(vto__gt=date.today(), vto__lt=date.today() + timedelta(days=30))
        else:
            q = queryset

        return q


class FiltroRenspaQuintero(admin.SimpleListFilter):
    title = 'estado renspa quintero'

    parameter_name = 'quintero_estado_renspa'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return Quintero.estados_habilitaciones.items()

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        v = None if self.value() is None else int(self.value())
        if v == Quintero.EST_HABILITADO:
            q = queryset.annotate(vto=Max('quintaquintero__fecha_fin_renspa')).filter(vto__gt=date.today())
        elif v == Quintero.EST_SIN_HABILITACION:
            q = queryset.annotate(habs=Count('quintaquintero')).filter(habs=0)
        elif v == Quintero.EST_HABILITACION_VENCIDA:
            q = queryset.annotate(vto=Max('quintaquintero__fecha_fin_renspa')).filter(vto__lt=date.today())
        elif v == Quintero.EST_VENCE_EN_7:
            q = queryset.annotate(vto=Max('quintaquintero__fecha_fin_renspa')).filter(vto__gt=date.today(), vto__lt=date.today() + timedelta(days=7))
        elif v == Quintero.EST_VENCE_EN_15:
            q = queryset.annotate(vto=Max('quintaquintero__fecha_fin_renspa')).filter(vto__gt=date.today(), vto__lt=date.today() + timedelta(days=15))
        elif v == Quintero.EST_VENCE_EN_30:
            q = queryset.annotate(vto=Max('quintaquintero__fecha_fin_renspa')).filter(vto__gt=date.today(), vto__lt=date.today() + timedelta(days=30))
        else:
            q = queryset

        return q


class FiltroContratoQuintero(admin.SimpleListFilter):
    title = 'estado contrato quintero'

    parameter_name = 'quintero_estado_contrato'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return Quintero.estados_habilitaciones.items()

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        v = None if self.value() is None else int(self.value())
        if v == Quintero.EST_HABILITADO:
            q = queryset.annotate(vto=Max('quintaquintero__fecha_fin_quinta')).filter(vto__gt=date.today())
        elif v == Quintero.EST_SIN_HABILITACION:
            q = queryset.annotate(habs=Count('quintaquintero')).filter(habs=0)
        elif v == Quintero.EST_HABILITACION_VENCIDA:
            q = queryset.annotate(vto=Max('quintaquintero__fecha_fin_quinta')).filter(vto__lt=date.today())
        elif v == Quintero.EST_VENCE_EN_7:
            q = queryset.annotate(vto=Max('quintaquintero__fecha_fin_quinta')).filter(vto__gt=date.today(), vto__lt=date.today() + timedelta(days=7))
        elif v == Quintero.EST_VENCE_EN_15:
            q = queryset.annotate(vto=Max('quintaquintero__fecha_fin_quinta')).filter(vto__gt=date.today(), vto__lt=date.today() + timedelta(days=15))
        elif v == Quintero.EST_VENCE_EN_30:
            q = queryset.annotate(vto=Max('quintaquintero__fecha_fin_quinta')).filter(vto__gt=date.today(), vto__lt=date.today() + timedelta(days=30))
        else:
            q = queryset

        return q


class QuinteroAdmin(admin.ModelAdmin):

    def habilitado(self, obj):
        return obj.estado_habilitacion()
    habilitado.boolean = True

    def suspendido(self, obj):
        return obj.estado_suspension()
    suspendido.boolean = True

    list_display = ('nombre', 'apellido', 'nave', 'numero_de_carnet', 'numero_de_expediente',
                    'publicado', 'habilitado','suspendido', 'declaracion_jurada',
                    'buenas_practicas', 'detalle')
    list_filter = (FiltroHabilitacionQuintero, FiltroContratoQuintero, FiltroRenspaQuintero, 'nave__nombre')
    exclude = ('slug', 'nombre_publico', 'html', 'user')
    search_fields = ['nombre', 'apellido', 'nave__nombre', 'numero_de_carnet']


class AyudanteQuinteroAdmin(admin.ModelAdmin):
    form = AyudanteQuinteroForm
    list_display = ('nombre', 'apellido', 'quintero_responsable')
    exclude = ('slug', 'nombre_publico', 'html', 'user')
    search_fields = ['nombre', 'apellido', 'quintero_responsable__nombre', 'quintero_responsable__apellido']

    # para que ande el select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


class QuintaQuinteroAdmin(admin.ModelAdmin):
    form = QuintaQuinteroForm
    list_display = ('quinta', 'quintero', 'relacion', 'fecha_inicio_quinta', 'fecha_fin_quinta'
                    ,'codigo_renspa', 'fecha_inicio_renspa', 'fecha_fin_renspa',
                    'fecha_inicio_sanitario', 'fecha_fin_sanitario')
    search_fields = ['quinta__domicilio', 'quintero__nombre', 'quintero__apellido', 
                    'relacion', 'codigo_renspa']

    # para que ande el select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


class InspeccionAdmin(admin.ModelAdmin):
    form = InspeccionForm

    def quintero_responsable_actualmente(self, obj):
        try:
            quintero_responsable = obj.quinta_a_inspeccionar.quintasquinteros.all()[0]
            return '{}'.format(quintero_responsable.quintero)
        except Exception as e:
            res = "Quinta sin Quintero asociado!"
            return '{}'.format(res)

    list_display = ('inspector', 'quinta_a_inspeccionar', 'quintero_responsable_actualmente', 'fecha_inspeccion', 'documentacion_cierre_inspeccion')
    exclude = ('slug', 'nombre_publico', 'user')
    search_fields = ['inspector__nombre']
    # list_filter = ['inspector__nombre', 'quinta_a_inspeccionar']

    # para que ande el select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


class HabilitacionQuinteroAdmin(admin.ModelAdmin):
    form = HabilitacionForm
    list_display = ('quintero', 'fecha_inicio', 'fecha_fin', 'numero_de_resolucion', 'resolucion_comprobante')
    search_fields = ['quintero__nombre', 'quintero__apellido','numero_de_resolucion']

    # para que ande el select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


class MotivoSuspensionAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'descripcion')
    search_fields = ['titulo']


class SuspensionQuinterosAdmin(admin.ModelAdmin):
    form = SuspensionQuinteroForm
    list_display = ('quintero', 'motivo', 'fecha_inicio_suspension',
                    'fecha_fin_suspension', 'observaciones_suspension', 'resolucion_suspension')
    search_fields = ['quintero__nombre', 'quintero__apellido' ,'motivo__titulo']

    # para que ande el select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


admin.site.register(Feria, FeriaAdmin)
admin.site.register(Nave, NaveAdmin)
admin.site.register(Quinta, QuintaAdmin)
admin.site.register(Quintero, QuinteroAdmin)
admin.site.register(AyudanteQuintero, AyudanteQuinteroAdmin)
admin.site.register(QuintaQuintero, QuintaQuinteroAdmin)
admin.site.register(Inspeccion, InspeccionAdmin)
admin.site.register(HabilitacionQuintero, HabilitacionQuinteroAdmin)
admin.site.register(MotivoSuspension, MotivoSuspensionAdmin)
admin.site.register(SuspensionQuinteros, SuspensionQuinterosAdmin)
