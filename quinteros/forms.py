from django_select2.forms import ModelSelect2Widget
from django import forms
from .models import Quintero, Quinta, AyudanteQuintero, QuintaQuintero, Inspeccion, SuspensionQuinteros, HabilitacionQuintero



class QuinteroWidget(ModelSelect2Widget):
    model = Quintero
    search_fields = ['nombre__icontains', 'apellido__icontains']
    max_results = 10

    def label_from_instance(self, obj):
        return '{} {}'.format(obj.nombre, obj.apellido)

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault('data-placeholder', 'Ingrese nombre o apellido')
        self.attrs.setdefault('data-minimum-input-length', 3)
        self.attrs.setdefault('data-width', '25em')

        return super(QuinteroWidget, self).build_attrs(*args, **kwargs)


class QuintaWidget(ModelSelect2Widget):
    model = Quinta
    search_fields = ['domicilio__icontains', 'nombre__icontains']
    max_results = 10

    def label_from_instance(self, obj):
        return '{} - {}'.format(obj.nombre, obj.domicilio)

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault('data-placeholder', 'Ingrese nombre o domicilio')
        self.attrs.setdefault('data-minimum-input-length', 3)
        self.attrs.setdefault('data-width', '25em')

        return super(QuintaWidget, self).build_attrs(*args, **kwargs)


class AyudanteQuinteroForm(forms.ModelForm):

    class Meta:
        model = AyudanteQuintero
        fields = '__all__'
        widgets = {
                    'quintero_responsable': QuinteroWidget
                    }


class QuintaQuinteroForm(forms.ModelForm):

    class Meta:
        model = QuintaQuintero
        fields = '__all__'
        widgets = {
                    'quintero': QuinteroWidget,
                    'quinta': QuintaWidget
                    }


class InspeccionForm(forms.ModelForm):

    class Meta:
        model = Inspeccion
        fields = '__all__'
        widgets = {
                    'quinta_a_inspeccionar': QuintaWidget
                    }


class SuspensionQuinteroForm(forms.ModelForm):

    class Meta:
        model = SuspensionQuinteros
        fields = '__all__'
        widgets = {
                    'quintero': QuinteroWidget
                    }


class HabilitacionForm(forms.ModelForm):

    class Meta:
        model = HabilitacionQuintero
        fields = '__all__'
        widgets = {
                    'quintero': QuinteroWidget
                    }
