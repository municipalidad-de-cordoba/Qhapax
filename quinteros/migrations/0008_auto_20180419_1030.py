# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-04-19 13:30
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('geo', '0005_auto_20180327_1429'),
        ('quinteros', '0007_auto_20180410_1544'),
    ]

    operations = [
        migrations.AddField(
            model_name='ayudantequintero',
            name='ciudad',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='geo.Ciudad'),
        ),
        migrations.AddField(
            model_name='ayudantequintero',
            name='domicilio',
            field=models.CharField(blank=True, max_length=130, null=True),
        ),
        migrations.AddField(
            model_name='ayudantequintero',
            name='telefono_celular',
            field=models.CharField(blank=True, max_length=130, null=True),
        ),
        migrations.AddField(
            model_name='ayudantequintero',
            name='telefono_fijo',
            field=models.CharField(blank=True, max_length=130, null=True),
        ),
        migrations.AddField(
            model_name='inspeccion',
            name='cultivos',
            field=models.CharField(blank=True, help_text='Ej: papas, cebollas, zanahorias', max_length=130, null=True),
        ),
        migrations.AddField(
            model_name='inspeccion',
            name='produccion',
            field=models.CharField(blank=True, help_text='Produccion que se ve a ojo en la inspeccion', max_length=130, null=True),
        ),
        migrations.AddField(
            model_name='inspeccion',
            name='produccion_estimada',
            field=models.CharField(blank=True, help_text='Produccion estimada declarada', max_length=130, null=True),
        ),
        migrations.AddField(
            model_name='quintero',
            name='ciudad',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='geo.Ciudad'),
        ),
        migrations.AddField(
            model_name='quintero',
            name='detalle',
            field=models.TextField(blank=True, help_text='Cuando se da de baja un quintero se debera aclarar el motivo', null=True),
        ),
        migrations.AddField(
            model_name='quintero',
            name='domicilio',
            field=models.CharField(blank=True, max_length=130, null=True),
        ),
        migrations.AddField(
            model_name='quintero',
            name='telefono_celular',
            field=models.CharField(blank=True, max_length=130, null=True),
        ),
        migrations.AddField(
            model_name='quintero',
            name='telefono_fijo',
            field=models.CharField(blank=True, max_length=130, null=True),
        ),
        migrations.AddField(
            model_name='suspensionquinteros',
            name='suspendido',
            field=models.BooleanField(default='True'),
        ),
        migrations.AlterField(
            model_name='habilitacionquintero',
            name='fecha_fin',
            field=models.DateField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='habilitacionquintero',
            name='fecha_inicio',
            field=models.DateField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='quintero',
            name='publicado',
            field=models.BooleanField(default=True, help_text='Destildar si el quintero se da de baja'),
        ),
    ]
