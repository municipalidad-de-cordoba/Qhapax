# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-04-10 18:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quinteros', '0006_auto_20180406_1642'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='habilitacionquintero',
            options={'verbose_name_plural': '9. Habilitaciones'},
        ),
        migrations.AlterModelOptions(
            name='motivosuspension',
            options={'verbose_name_plural': '7. MotivosSuspension'},
        ),
        migrations.AlterModelOptions(
            name='suspensionquinteros',
            options={'verbose_name': '8. Suspension quintero', 'verbose_name_plural': '8. Suspensiones quinteros'},
        ),
        migrations.RemoveField(
            model_name='historicalquintaquintero',
            name='numero_de_expediente',
        ),
        migrations.RemoveField(
            model_name='quintaquintero',
            name='numero_de_expediente',
        ),
        migrations.AlterField(
            model_name='quinta',
            name='domicilio',
            field=models.CharField(default='sin definir', max_length=130),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='quinta',
            name='nombre',
            field=models.CharField(blank=True, help_text='Nombre de la quinta', max_length=130, null=True),
        ),
    ]
