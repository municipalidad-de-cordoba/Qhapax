from django.conf.urls import url, include
from . import views


urlpatterns = [
    url(r'^lista-de-quinteros.(?P<filetype>csv|xls)$', views.lista_quinteros, name='quinteros.index'),
    url(r'^lista-de-ayudantes-quinteros.(?P<filetype>csv|xls)$', views.lista_ayudantes_quinteros, name='ayudantesquinteros.index'),
    url(r'^lista-de-habilitados.(?P<filetype>csv|xls)$', views.lista_habilitaciones, name='habilitadosquinteros.index'),
    url(r'^lista-de-inspecciones.(?P<filetype>csv|xls)$', views.lista_inspecciones, name='inspeccionesquinteros.index'),

    url(r'^quintero/(?P<uuid>[\w-]+)$', views.ficha_quintero, name='quinteros.ficha'),
    url(r'^ayudante/(?P<uuid>[\w-]+)$', views.ficha_ayudante_quintero, name='ayudantesquinteros.ficha'),
]
