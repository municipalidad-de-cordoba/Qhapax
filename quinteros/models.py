from django.contrib.gis.db import models
from core.models import Persona
from credenciales.models import CredencialGeneral
from django.core.urlresolvers import reverse
from simple_history.models import HistoricalRecords
from geo.models import Ciudad
from quinteros import settings
from django.utils.translation import ugettext_lazy as txterr
from django.core.exceptions import ValidationError
from datetime import date, timedelta
from django.utils import timezone
import uuid



class Feria(models.Model):
    """
    Nombre de la feria donde estaran situadas las naves.
    Ej: Mercado de abasto de Cordoba.
    """
    nombre = models.CharField(max_length=130, default="Mercado de abasto de Cordoba",
                                    null=True, blank=True)
    domicilio = models.CharField(max_length=130, null=True, blank=True)
    ciudad = models.ForeignKey(Ciudad, null=True, blank=True, on_delete=models.SET_NULL)
    descripcion = models.TextField(null=True, blank=True)
    poligono = models.PolygonField(null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "0. Ferias"


class Nave(models.Model):
    """ Cada una de las zonas en la que se divide el mercado"""
    feria = models.ForeignKey(Feria, null=True, blank=True, help_text="A que feria pertenece la nave")
    nombre = models.CharField(max_length=70, null=True, blank=True)
    poligono = models.PolygonField(null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "1. Naves"


class Quinta(models.Model):
    """ Cada quinta que luego puede vincularse a un quintero """
    domicilio = models.CharField(max_length=130)
    ciudad = models.ForeignKey(Ciudad, null=True, blank=True, on_delete=models.SET_NULL)
    hectareas = models.PositiveIntegerField(default=0,
                                            help_text='Unidad de medida en hectareas')
    nombre = models.CharField(max_length=130, null=True, blank=True,
                              help_text="Nombre de la quinta")
    poligono = models.PolygonField(null=True, blank=True)

    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.nombre:
            return '{} - {}'.format(self.domicilio, self.nombre)
        else:
            return self.domicilio

    class Meta:
        verbose_name_plural = "2. Quintas"


class Quintero(Persona):
    """ Cada uno de los quinteros """
    domicilio = models.CharField(max_length=130, null=True, blank=True)
    ciudad = models.ForeignKey(Ciudad, null=True, blank=True, on_delete=models.SET_NULL)
    telefono_fijo = models.CharField(max_length=130, null=True, blank=True)
    telefono_celular = models.CharField(max_length=130, null=True, blank=True)
    nave = models.ForeignKey(Nave)
    publicado = models.BooleanField(default=True, help_text="Destildar si el quintero se da de baja")
    numero_de_carnet = models.CharField(max_length=30, null=True, blank=True)
    numero_de_expediente = models.CharField(max_length=30, null=True, blank=True)
    declaracion_jurada = models.FileField(upload_to='habilitaciones-quinteros/declaraciones/',
                                            null=True, blank=True,
                                            help_text='Comprobante en PDF, imagen u otros')
    # Certificado acreditando el curso de buenas practicas
    buenas_practicas = models.FileField(upload_to='habilitaciones-quinteros/buenas-practicas/',
                                            null=True, blank=True,
                                            help_text='Comprobante del certificado en PDF, imagen u otros')
    detalle = models.TextField(null=True, blank=True, help_text="Cuando se da de baja un quintero se debera aclarar el motivo")
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)


    def get_absolute_url(self):
        return reverse('quinteros.ficha', kwargs={'uuid': self.uuid})

    EST_HABILITADO = 10
    EST_SIN_HABILITACION = 20
    EST_HABILITACION_VENCIDA = 30
    EST_VENCE_EN_7 = 40
    EST_VENCE_EN_15 = 50
    EST_VENCE_EN_30 = 60
    estados_habilitaciones = {
                                EST_HABILITADO: 'Habilitado',
                                EST_SIN_HABILITACION: 'Sin ninguna habilitación',
                                EST_HABILITACION_VENCIDA: 'Habilitación vencida',
                                EST_VENCE_EN_7: 'Vence en menos de 7 días',
                                EST_VENCE_EN_15: 'Vence en menos de 15 días',
                                EST_VENCE_EN_30: 'Vence en menos de 30 días'
                            }

    def estado_habilitacion(self):
        last = self.habilitacionquintero_set.all().order_by('-fecha_fin')[:1]

        ret = self.EST_HABILITADO
        if len(last) == 0:  # si no tiene habilitacion
            ret = self.EST_SIN_HABILITACION
        else:
            hab = last[0]
            if hab.fecha_vencimiento_habilitacion is None:
                ret = self.EST_SIN_HABILITACION
            elif hab.fecha_vencimiento_habilitacion < date.today():
                ret = self.EST_HABILITACION_VENCIDA
            elif hab.fecha_vencimiento_habilitacion < date.today() + timedelta(days=7):
                ret = self.EST_VENCE_EN_7
            elif hab.fecha_vencimiento_habilitacion < date.today() + timedelta(days=15):
                ret = self.EST_VENCE_EN_15
            elif hab.fecha_vencimiento_habilitacion < date.today() + timedelta(days=30):
                ret = self.EST_VENCE_EN_30

        return self.estados_habilitaciones[ret]

    def estado_suspension(self):
        suspensiones = self.suspensionquinteros_set.filter(quintero__id=self.id)
        suspension = suspensiones[0] if len(suspensiones) != 0 else None

        # Si estoy suspendido retorno True, sino False.
        return True if (suspension != None and suspension.suspendido == True) else False

    def estado_habilitacion(self):
        habilitaciones = self.habilitacionquintero_set.filter(quintero__id=self.id)
        habilitacion = habilitaciones[0] if len(habilitaciones) != 0 else None

        # Si estoy habilitado retorno True, sino False.
        return True if (habilitacion != None and habilitacion.fecha_fin > date.today() and self.publicado == True) else False

    @property
    def ayudantequintero(self):
        return self.ayudantequintero_set.filter(publicado=True)

    def __str__(self):
        return '{} {}'.format(self.nombre, self.apellido)

    class Meta:
        verbose_name_plural = "3. Quinteros"
        permissions = (
            ('ver_lista_quinteros', 'Solo puede ver a quinteros'),)


class AyudanteQuintero(Persona):
    """ Personas que ayudan a los quinteros"""
    # Un ayudante tiene solo UN quintero asignado y 
    # un quintero puede tener solo UN ayudante.
    domicilio = models.CharField(max_length=130, null=True, blank=True)
    ciudad = models.ForeignKey(Ciudad, null=True, blank=True, on_delete=models.SET_NULL)
    telefono_fijo = models.CharField(max_length=130, null=True, blank=True)
    telefono_celular = models.CharField(max_length=130, null=True, blank=True)
    quintero_responsable = models.OneToOneField(Quintero, blank=True, null=True, help_text="Quintero responsable de este ayudante")
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)


    def get_absolute_url(self):
        return reverse('ayudantesquinteros.ficha', kwargs={'uuid': self.uuid})

    def __str__(self):
        return '{} {}'.format(self.nombre, self.apellido)

    class Meta:
        verbose_name_plural = "4. AyudantesQuinteros"
        permissions = (
            ('ver_lista_quinteros', 'Solo puede ver a quinteros'),)


class QuintaQuintero(models.Model):
    """Relacion entre quinteros y quintas con sus respectivos codigos"""
    ARRENDATARIO = 10
    PROPIETARIO = 20

    relaciones = (
                    (PROPIETARIO, 'Propietario'),
                    (ARRENDATARIO, 'Arrendatario')
                )

    quintero = models.ForeignKey(Quintero)
    quinta = models.ForeignKey(Quinta, related_name='quintasquinteros')
    relacion = models.PositiveIntegerField(choices=relaciones, default=PROPIETARIO)
    # Estos 2 campos de abajo van a indicar, en caso de que la relacion sea arrendatario,
    # desde que fecha se alquila la quinta y hasta cuando.
    fecha_inicio_quinta = models.DateField(null=True, blank=True,
                                            help_text="Fecha inicio en la cual el arrendatario comenzo a alquilar la quinta") 
    fecha_fin_quinta = models.DateField(null=True, blank=True,
                                        help_text="Fecha fin del alquiler de la quinta del arrendatario")
    # RENSPA es un código que vincula quintero y quinta.
    codigo_renspa = models.CharField(max_length=30, null=True, blank=True)
    fecha_inicio_renspa = models.DateField(null=True, blank=True)
    fecha_fin_renspa = models.DateField(null=True, blank=True)
    fecha_inicio_sanitario = models.DateField(null=True, blank=True)
    fecha_fin_sanitario = models.DateField(null=True, blank=True)

    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    history = HistoricalRecords()

    def __str__(self):
        return '{} - {}'.format(self.quintero, self.quinta)

    class Meta:
        verbose_name_plural = "5. QuintasQuintero"


class Inspeccion(models.Model):
    """ Modulo para controlar las quintas """
    inspector = models.ForeignKey(CredencialGeneral)
    quinta_a_inspeccionar = models.ForeignKey(Quinta, related_name='quintas_inspeccionadas')
    fecha_inspeccion = models.DateField(null=True, blank=True)
    cultivos = models.CharField(max_length=130, null=True, blank=True,
                                help_text="Ej: papas, cebollas, zanahorias")
    produccion = models.CharField(max_length=130, null=True, blank=True,
                                    help_text="Produccion que se ve a ojo en la inspeccion")
    produccion_estimada = models.CharField(max_length=130, null=True, blank=True,
                                            help_text="Produccion estimada declarada")
    detalle = models.TextField(null=True, blank=True)
    documentacion_cierre_inspeccion = models.FileField(upload_to='habilitaciones-quinteros/inspecciones/',
                                                null=True, blank=True,
                                                help_text='Comprobante en PDF, imagen u otros')

    def clean(self):
        if settings.ID_INSPECTORES_QUINTAS != self.inspector.tipo_credencial.id:
            raise ValidationError(
                txterr('Verificar que el inspector sea un inspector de quinteros.'),
            )

    def save(self, *args, **kwargs):
        self.clean()
        super(Inspeccion, self).save(*args, **kwargs)

    def __str__(self):
        return '{}'.format(self.quinta_a_inspeccionar)

    class Meta:
        verbose_name_plural = "6. Inspecciones"


class MotivoSuspension(models.Model):
    """Clase para las suspensiones"""
    titulo = models.CharField(max_length=100)
    descripcion = models.TextField(null=True, blank=True,
                                    help_text='Detalle de los casos en los que se aplica esta suspensión')

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name_plural = "7. MotivosSuspension"


class SuspensionQuinteros(models.Model):
    """Suspension a cada quintero (junto con su ayudante)"""
    quintero = models.ForeignKey(Quintero)
    motivo = models.ForeignKey(MotivoSuspension)
    fecha_inicio_suspension = models.DateField()
    fecha_fin_suspension = models.DateField(null=True, blank=True)
    suspendido = models.BooleanField(default="True")
    observaciones_suspension = models.TextField(null=True, blank=True)
    resolucion_suspension = models.FileField(upload_to='habilitaciones-quinteros/suspensiones/', 
                                                null=True, blank=True, 
                                                verbose_name='Comprobante en PDF, imagen u otros')

    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} - {}'.format(self.quintero, self.motivo)

    class Meta:
        verbose_name_plural = "8. Suspensiones quinteros"
        verbose_name = "8. Suspension quintero"


class HabilitacionQuintero(models.Model):
    """ Habilitación de un Quintero"""
    quintero = models.ForeignKey(Quintero)
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()
    numero_de_resolucion = models.CharField(max_length=30, null=True, blank=True)
    resolucion_comprobante = models.FileField(upload_to='habilitaciones-quinteros/resoluciones/',
                                                null=True, blank=True,
                                                help_text='Comprobante en PDF, imagen u otros')

    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def clean(self):
        # Si las fechas estan mal o el quintero esta suspendido entonces error.
        if self.fecha_fin <= self.fecha_inicio:
            raise ValidationError(
                txterr('La fecha de fin es menor o igual a la fecha de inicio.'),
            )
        elif self.quintero.estado_suspension():
            raise ValidationError(
                txterr('Quintero actualmente suspendido!'),
            )

    def save(self, *args, **kwargs):
        self.clean()
        super(HabilitacionQuintero, self).save(*args, **kwargs)

    def __str__(self):
        return '{}: {} - {}'.format(self.quintero, self.fecha_inicio, self.fecha_fin)

    class Meta:
        verbose_name_plural = "9. Habilitaciones"
