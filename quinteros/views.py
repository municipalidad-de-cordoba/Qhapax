from django.http import HttpResponse
from django.shortcuts import render
import django_excel as excel
from django.views.decorators.cache import cache_page
from django.http import Http404
from django.shortcuts import get_object_or_404
from .models import Quintero, HabilitacionQuintero, AyudanteQuintero, Inspeccion
from django.contrib.auth.decorators import login_required, permission_required


@cache_page(60 * 60 * 12)  # 12 h
def ficha_quintero(request, uuid):
    '''
    Mostrar un quintero
    http://localhost:8000/quinteros/quintero/uuid
    '''

    quintero = get_object_or_404(Quintero, uuid=uuid, publicado=True)
    habilitacion = HabilitacionQuintero.objects.filter(quintero=quintero)
    hab = "" if len(habilitacion) == 0 else habilitacion[0]
    # hff = habilitacion fecha fin
    # hnr = habilitacion numero resolucion
    hff = ""
    hnr = ""
    # verificamos que el quintero tenga una habilitacion
    if hab is not "":
        hff = hab.fecha_fin
        # si hab.numero_de_resolucion es None => "", sino hab.numero_de_resolucion
        hnr = hab.numero_de_resolucion or ""
    base_url = request.website
    url = '{}{}'.format(base_url, quintero.get_absolute_url())
    qr_image = quintero.get_image_qr(url=url)
    qr_download = quintero.get_download_link_qr(url=url)
    context = {'quintero': quintero, 'qr_image': qr_image,
                'qr_download': qr_download, 'hff':hff, 'hnr':hnr}
    url = 'quinteros/ficha.html'
    return render(request, url, context)

@cache_page(60 * 60 * 12)  # 12 h
def ficha_ayudante_quintero(request, uuid):
    '''
    Mostrar un ayudante de quintero
    http://localhost:8000/quinteros/ayudante/uuid
    '''

    # No hace falta filtrar si el quintero relacionado con este ayudante
    # esta publicado o no porque ya lo filtro en lista_ayudantes_quinteros
    ayudante = get_object_or_404(AyudanteQuintero, uuid=uuid)
    quintero = ayudante.quintero_responsable
    habilitacion = HabilitacionQuintero.objects.filter(quintero=quintero)
    hab = "" if len(habilitacion) == 0 else habilitacion[0]
    # hff = habilitacion fecha fin
    # hnr = habilitacion numero resolucion
    hff = ""
    hnr = ""
    # verificamos que el quintero tenga una habilitacion
    if hab is not "":
        hff = hab.fecha_fin
        # si hab.numero_de_resolucion es None => "", sino hab.numero_de_resolucion
        hnr = hab.numero_de_resolucion or ""
    base_url = request.website
    url = '{}{}'.format(base_url, ayudante.get_absolute_url())
    qr_image = ayudante.get_image_qr(url=url)
    qr_download = ayudante.get_download_link_qr(url=url)
    context = {'ayudante': ayudante, 'quintero': quintero, 'hff':hff, 'hnr':hnr,
                'qr_image': qr_image, 'qr_download': qr_download}
    url = 'quinteros/fichaayudante.html'
    return render(request, url, context)

@cache_page(60 * 60 * 12)  # 12 h
@permission_required('quinteros.ver_lista_quinteros')
def lista_quinteros(request, filetype):
    '''
    Planilla de quinteros
    http://localhost:8000/quinteros/lista-de-quinteros.xls
    '''

    quinteros = Quintero.objects.filter(publicado=True).order_by('apellido', 'nombre')
    csv_list = []
    csv_list.append(['Numero de carnet', 'Numero de expediente', 'Nave',
                    'Numero resolucion', 'Fecha fin resolucion',
                    'Nombre', 'Apellido', 'DNI', 'Genero', 'Edad',
                    'Domicilio quintero', 'Ciudad', 'Telefono fijo',
                    'Telefono celular', 'url', 'url_foto_thumbnail', 'url_foto_original',
                    'Fecha de creacion', 'Fecha ultima modificacion'])

    for quintero in quinteros:
        dni = '{} {}'.format(quintero.tipo_id, quintero.unique_id)
        url = "{}{}".format(request.website, quintero.get_absolute_url())

        try:
            foto = "{}{}".format(request.website, quintero.foto.url)
            foto_thumb = "{}{}".format(request.website, quintero.foto.thumbnail['600x600'].url)
        except (ValueError, FileNotFoundError):
            foto = ""
            foto_thumb = ""

        habilitacion = HabilitacionQuintero.objects.filter(quintero=quintero)
        hab = "" if len(habilitacion) == 0 else habilitacion[0]
        # hff = habilitacion fecha fin
        # hnr = habilitacion numero resolucion
        hff = ""
        hnr = ""
        # verificamos que el quintero tenga una habilitacion
        if hab is not "":
            hff = hab.fecha_fin
            hnr = hab.numero_de_resolucion

        csv_list.append([
            quintero.numero_de_carnet,
            quintero.numero_de_expediente,
            quintero.nave.nombre,
            hnr,
            hff,
            quintero.nombre,
            quintero.apellido,
            dni,
            quintero.get_genero_display(),
            quintero.edad,
            quintero.domicilio,
            quintero.ciudad,
            quintero.telefono_fijo,
            quintero.telefono_celular,
            url,
            foto_thumb,
            foto,
            # Formateamos las fechas de la forma dia/mes/año horas:minutos:segundos
            '{}/{}/{} {}:{}:{}'.format(quintero.creado.day, quintero.creado.month, quintero.creado.year,
                                quintero.creado.hour, quintero.creado.minute, quintero.creado.second),
            '{}/{}/{} {}:{}:{}'.format(quintero.ultima_modificacion.day, quintero.ultima_modificacion.month, quintero.ultima_modificacion.year,
                                quintero.ultima_modificacion.hour, quintero.ultima_modificacion.minute, quintero.ultima_modificacion.second),
            ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 h
@permission_required('quinteros.ver_lista_quinteros')
def lista_ayudantes_quinteros(request, filetype):
    '''
    Planilla de ayudante quinteros
    http://localhost:8000/quinteros/lista-de-ayudante-quinteros.xls
    '''

    quinteros = Quintero.objects.filter(publicado=True).order_by('apellido', 'nombre')
    csv_list = []
    csv_list.append(['Numero de carnet', 'Numero de expediente', 'Nave',
                    'Numero resolucion', 'Fecha fin resolucion',
                    'Nombre', 'Apellido', 'DNI', 'Genero', 'Edad','Domicilio ayudante', 
                    'Ciudad', 'Telefono fijo','Telefono celular', 'Quintero responsable', 
                    'url', 'url_foto_thumbnail', 'url_foto_original',
                    'Fecha de creacion', 'Fecha ultima modificacion'])

    for quintero in quinteros:
        try:
            ayudante = quintero.ayudantequintero
            dni = '{} {}'.format(ayudante.tipo_id, ayudante.unique_id)
            url = "{}{}".format(request.website, ayudante.get_absolute_url())
            foto = "{}{}".format(request.website, ayudante.foto.url)
            foto_thumb = "{}{}".format(request.website, ayudante.foto.thumbnail['600x600'].url)
        except (ValueError, FileNotFoundError, Exception):
            ayudante = ""
            dni = ""
            url = ""
            foto = ""
            foto_thumb = ""

        habilitacion = HabilitacionQuintero.objects.filter(quintero=quintero)
        hab = "" if len(habilitacion) == 0 else habilitacion[0]
        # hff = habilitacion fecha fin
        # hnr = habilitacion numero resolucion
        hff = ""
        hnr = ""
        # verificamos que el quintero tenga una habilitacion
        if hab is not "":
            hff = hab.fecha_fin
            hnr = hab.numero_de_resolucion

        if ayudante is not "":
            csv_list.append([
                quintero.numero_de_carnet,
                quintero.numero_de_expediente,
                quintero.nave.nombre,
                hnr,
                hff,
                ayudante.nombre,
                ayudante.apellido,
                dni,
                ayudante.get_genero_display(),
                ayudante.edad,
                ayudante.domicilio,
                ayudante.ciudad,
                ayudante.telefono_fijo,
                ayudante.telefono_celular,
                quintero.nombre + " " + quintero.apellido,
                url,
                foto_thumb,
                foto,
                # Formateamos las fechas de la forma dia/mes/año horas:minutos:segundos
                '{}/{}/{} {}:{}:{}'.format(quintero.creado.day, quintero.creado.month, quintero.creado.year,
                                quintero.creado.hour, quintero.creado.minute, quintero.creado.second),
                '{}/{}/{} {}:{}:{}'.format(quintero.ultima_modificacion.day, quintero.ultima_modificacion.month, quintero.ultima_modificacion.year,
                                quintero.ultima_modificacion.hour, quintero.ultima_modificacion.minute, quintero.ultima_modificacion.second),
                ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 h
@permission_required('quinteros.ver_lista_quinteros')
def lista_habilitaciones(request, filetype):
    '''
    Planilla de quinteros
    http://localhost:8000/quinteros/lista-de-habilitados.csv
    '''

    habilitaciones = HabilitacionQuintero.objects.all().order_by('fecha_inicio')
    csv_list = []
    csv_list.append(['Nombre', 'Apellido', 'DNI', 'Genero', 'Edad',
                    'Domicilio quintero', 'Ciudad', 'Telefono fijo',
                    'Telefono celular', 'numero_de_resolucion',
                    'Fecha de creacion', 'Fecha ultima modificacion'])

    for habilitacion in habilitaciones:
        dni = '{} {}'.format(habilitacion.quintero.tipo_id, habilitacion.quintero.unique_id)
        hnr = habilitacion.numero_de_resolucion

        csv_list.append([
            habilitacion.quintero.nombre,
            habilitacion.quintero.apellido,
            dni,
            habilitacion.quintero.get_genero_display(),
            habilitacion.quintero.edad,
            habilitacion.quintero.domicilio,
            habilitacion.quintero.ciudad,
            habilitacion.quintero.telefono_fijo,
            habilitacion.quintero.telefono_celular,
            hnr,
            # Formateamos las fechas de la forma dia/mes/año horas:minutos:segundos
            # Fecha creacion
            '{}/{}/{} {}:{}:{}'.format(habilitacion.quintero.creado.day, habilitacion.quintero.creado.month, habilitacion.quintero.creado.year,
                                habilitacion.quintero.creado.hour, habilitacion.quintero.creado.minute, habilitacion.quintero.creado.second),
            # Fecha ultima modificacion
            '{}/{}/{} {}:{}:{}'.format(habilitacion.quintero.ultima_modificacion.day, habilitacion.quintero.ultima_modificacion.month, habilitacion.quintero.ultima_modificacion.year,
                                habilitacion.quintero.ultima_modificacion.hour, habilitacion.quintero.ultima_modificacion.minute, habilitacion.quintero.ultima_modificacion.second),
            ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 h
@permission_required('quinteros.ver_lista_quinteros')
def lista_inspecciones(request, filetype):
    '''
    Planilla de quinteros
    http://localhost:8000/quinteros/lista-de-inspecciones.csv
    '''

    inspecciones = Inspeccion.objects.all().order_by('fecha_inspeccion')
    csv_list = []
    csv_list.append(['Nombre inspector', 'Apellido inspector', 'Quinta inspeccionada',
                    'Direccion quinta ', 'Fecha inspeccion', 'Cultivos', 'Produccion',
                    'Produccion estimada', 'Detalle'])

    for inspeccion in inspecciones:

        csv_list.append([
            inspeccion.inspector.nombre,
            inspeccion.inspector.apellido,
            inspeccion.quinta_a_inspeccionar.nombre,
            inspeccion.quinta_a_inspeccionar.domicilio,
            '{}'.format(inspeccion.fecha_inspeccion),
            inspeccion.cultivos,
            inspeccion.produccion,
            inspeccion.produccion_estimada,
            inspeccion.detalle,
            ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)