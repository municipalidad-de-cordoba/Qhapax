from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import TipoObraSerializer, BarrioObraSerializer
from api.pagination import DefaultPagination
from obras.models import TipoObra, BarrioObra


class TipoEspaciosVerdesViewSet(viewsets.ModelViewSet):
    """
    Tipos de obras de ambiente(espacios verdes).
    """
    serializer_class = TipoObraSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = TipoObra.objects.exclude(
            espacioverde=None).order_by('nombre')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class TipoObrasTercerosViewSet(viewsets.ModelViewSet):
    """
    Tipos de obras de terceros.
    """
    serializer_class = TipoObraSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # Obtiene todos los tipos correspondientes a obras de terceros
        queryset = TipoObra.objects.exclude(
            obradeterceros=None).order_by('nombre')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class BarrioEspaciosVerdesViewSet(viewsets.ModelViewSet):
    """
    Barrios de obras de ambiente(espacios verdes).
    """
    serializer_class = BarrioObraSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = BarrioObra.objects.exclude(
            espacioverde=None).order_by('nombre')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class BarrioObrasTercerosViewSet(viewsets.ModelViewSet):
    """
    Barrios de obras de terceros.
    """
    serializer_class = BarrioObraSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = BarrioObra.objects.exclude(
            obradeterceros=None).order_by('nombre')

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
