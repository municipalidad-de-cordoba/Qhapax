from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from obras.models import TipoObra, BarrioObra


class BarrioObraSerializer(CachedSerializerMixin):

    class Meta:
        model = BarrioObra
        fields = ('id', 'nombre')


class TipoObraSerializer(CachedSerializerMixin):

    class Meta:
        model = TipoObra
        fields = ('id', 'nombre', 'color')


cache_registry.register(TipoObraSerializer)
cache_registry.register(BarrioObraSerializer)
