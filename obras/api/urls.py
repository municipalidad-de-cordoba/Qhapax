from django.conf.urls import url, include
from rest_framework import routers
from .views import (TipoEspaciosVerdesViewSet, TipoObrasTercerosViewSet,
                    BarrioEspaciosVerdesViewSet, BarrioObrasTercerosViewSet)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    'tipos-espacios-verdes',
    TipoEspaciosVerdesViewSet,
    base_name='tipos-espacios-verdes.api.lista')
router.register(
    'tipos-obras-terceros',
    TipoObrasTercerosViewSet,
    base_name='tipos-obras-terceros.api.lista')
router.register(
    'barrios-espacios-verdes',
    BarrioEspaciosVerdesViewSet,
    base_name='barrios-espacios-verdes.api.lista')
router.register(
    'barrios-obras-terceros',
    BarrioObrasTercerosViewSet,
    base_name='barrios-obras-terceros.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
