from django.db import models
from core.models import Moneda
from barrios.models import Barrio
from organizaciones.models import Organizacion
from django.conf import settings
from django.contrib.gis.db import models as models_geo


class TipoObra(models.Model):
    """
    tipo de obra(pavimentación, etc)
    """
    nombre = models.CharField(max_length=90)
    color = models.CharField(
        max_length=20,
        default='ffffffff',
        help_text=('Se esperan 8 caracteres hexBinary: aabbggrr',
                   '(alpha + blue + gren + red) de 0 a f'))

    def __str__(self):
        return self.nombre


class BarrioObra(models.Model):
    """
    lista de barrios personalizada de obra
    #FIXME conectar a la futura base oficial de barrios
    """
    nombre = models.CharField(max_length=90)

    def __str__(self):
        return self.nombre


class OrigenDatoObra(models.Model):
    """
    los datos pueden venir desde mas de un origen
    cuando comienzo la importación desde un origen y a los fines de eliminar
    los que no lleguen debo marcar a todos los elementos como temporalmente
    inactivos desde un origen hasta que pase de nuevo la migración
    """
    nombre = models.CharField(max_length=190)

    def __str__(self):
        return self.nombre


class Obra(models.Model):
    """
    Modelo para guardar obras de terceros.
    Similar al modelo de obras públicas.
    FIXME: obras públicas debería heredar de este modelo
    """
    EN_EJECUCION = 6
    FINALIZADA = 8

    tipos_estados = (
        (EN_EJECUCION, 'En ejecución'),
        (FINALIZADA, 'Finalizada')
    )

    id_externo = models.PositiveIntegerField(default=0)
    nombre = models.CharField(max_length=190)
    decreto = models.CharField(max_length=90, null=True, blank=True)
    tipo = models.ForeignKey(TipoObra, null=True, on_delete=models.SET_NULL)
    fecha_inicio = models.DateTimeField(null=True, blank=True)
    fecha_finalizacion_estimada = models.DateTimeField(null=True, blank=True)
    color = models.CharField(max_length=20, null=True, blank=True)
    porcentaje_completado = models.DecimalField(
        max_digits=5, decimal_places=2, default=0.0)
    estado = models.PositiveIntegerField(
        choices=tipos_estados, null=True, blank=True)
    expediente_origen = models.CharField(max_length=90, null=True, blank=True)
    barrios = models.ManyToManyField(Barrio, blank=True)
    monto = models.DecimalField(max_digits=19, decimal_places=2, default=0.0)
    contratista = models.ForeignKey(Organizacion, null=True, blank=True)
    moneda_monto = models.ForeignKey(
        Moneda,
        null=True,
        on_delete=models.SET_NULL,
        default=settings.MONEDA_PK_PRINCIPAL)

    # campo generico espacial, puede contener puntos, líneas o polígonos
    ubicacion = models_geo.GeometryField(null=True)

    origen = models.ForeignKey(OrigenDatoObra, null=True, blank=True)
    observaciones_internas = models.TextField(null=True, blank=True)
    publicado = models.BooleanField(
        default=True,
        help_text='Indica si vino OK desde el sistema externo que nos provee los datos')
    publicado_gobierno = models.BooleanField(
        default=True, help_text='Campo para editar y aprobar localmente')
    descripcion_publica = models.TextField(
        null=True,
        blank=True,
        help_text='Descripcion local cargada por el equipo de comunicación')

    def __str__(self):
        return self.nombre

    @property
    def resumen(self, largo_texto=280):
        """
        Retorna nombre - monto - proveedor
        """
        propuesto = ''
        # si el nombre es mas grande, se trunca para que entre en un tweet
        descripcion_publica = self.nombre if self.descripcion_publica is None else self.descripcion_publica
        if len(descripcion_publica) >= largo_texto:
            propuesto += '{} ...'.format(descripcion_publica[:largo_texto - 4])
        else:
            propuesto += descripcion_publica
            if len(propuesto + ' - $' + str(self.monto)) <= largo_texto:
                # descripcion_publica + monto entran en el tweet
                propuesto += ' - ${}'.format(str(self.monto))
                if len(
                    propuesto +
                    ' - ' +
                        self.contratista.nombre) <= largo_texto:
                    # descripcion_publica + monto + proveedor entran en el
                    # tweet
                    propuesto += ' - {}'.format(self.contratista.nombre)
        return propuesto

    class Meta:
        # ordering = ['nombre']
        abstract = True
