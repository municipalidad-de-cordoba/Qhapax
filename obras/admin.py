from django.contrib import admin
from .models import BarrioObra, TipoObra


class BarrioObraAdmin(admin.ModelAdmin):
    list_display = ["nombre"]
    search_fields = ['nombre']


class TipoObraAdmin(admin.ModelAdmin):
    list_display = ["nombre", "color"]
    search_fields = ['nombre']


admin.site.register(BarrioObra, BarrioObraAdmin)
admin.site.register(TipoObra, TipoObraAdmin)
