from django.db import models
from ckeditor.fields import RichTextField


class Tramite(models.Model):
    ''' cada uno de los trámites que documentamos '''
    nombre = models.CharField(max_length=90)
    objectivo_del_tramite = models.TextField(null=True, blank=True, help_text='Que consigue el interesado luego de hacer el trámite')
    quien_puede_iniciar_el_tramite = models.CharField(max_length=90, null=True, blank=True, help_text='Un texto corto la forma "Solo el titula", "Cualquier persona", etc')
    descripcion_interna = models.TextField(null=True, blank=True)
    procedimiento_publico = RichTextField(null=True, blank=True, help_text='Ayuda general de lo que hay que hacer')
    tramites_relacionados = models.ManyToManyField('self', blank=True)
    #FIXME cuando se haga un módulo con el digesto deberá conectarse
    legislacion = models.CharField(null=True, blank=True, max_length=90, help_text='Del la forma "Ordenanza 12.900" o "Decreto 1123/18", a futuro se conectará al digesto')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre


class Requisito(models.Model):
    ''' cada uno de los requisitos para un trámite '''

    nombre = models.CharField(max_length=90)
    descripcion_publica = models.TextField(null=True, blank=True, help_text='Descripcion o explicacion detallada de este requisito')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre


class RequisitoTramite(models.Model):
    ''' los requisitos de cada tramite '''
    tramite = models.ForeignKey(Tramite)
    requisito = models.ForeignKey(Requisito)
    descripcion_publica_de_este_paso = models.TextField(null=True, blank=True, help_text='Descripcion o explicacion detallada de este paso')
    orden = models.PositiveIntegerField(default=100, help_text='Orden en que debe realizarse (si es que hay un orden)')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} {}'.format(self.tramite.nombre, self.requisito.nombre)

    class Meta:
        verbose_name = 'Requisito en tramite'
        verbose_name_plural = 'Requisitos en tramite'
