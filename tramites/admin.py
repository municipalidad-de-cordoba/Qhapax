from simple_history.admin import SimpleHistoryAdmin
from django.contrib import admin
from .models import *


class RequisitoTramiteInline(admin.StackedInline):
    model = RequisitoTramite
    extra = 1


class TramiteAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'legislacion')
    search_fields = ('nombre', 'legislacion', 'procedimiento_publico', 'descripcion_interna')

    inlines = [RequisitoTramiteInline]


class RequisitoAdmin(admin.ModelAdmin):
    list_display = ['nombre']
    search_fields = ['nombre']


admin.site.register(Tramite, TramiteAdmin)
admin.site.register(Requisito, RequisitoAdmin)
# admin.site.register(RequisitoTramite, RequisitoTramiteAdmin)
