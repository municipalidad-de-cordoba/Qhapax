Tramites de Gobierno
====================

Modulo para administrar los trámites que pueden hacerse. Se especifican los requisitos, lugares, costos y todos los detalles para hacer un trámite.
