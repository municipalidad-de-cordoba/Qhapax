from django.db import models
from organizaciones.models import SucursalOrganizacion
from simple_history.models import HistoricalRecords
from datetime import date, timedelta
from versatileimagefield.fields import VersatileImageField


class TipoEntePrivado(models.Model):
    """ Tipos de empresas que se consideran entes privados.
    Hoy son Geriátricos y Jardines Maternales privados """
    nombre = models.CharField(max_length=250)
    history = HistoricalRecords()

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']


class EntePrivado(models.Model):
    """ Tipo de locales comerciales especiales de un previamente inscripto
    Cada municipio puede definirlo distinto, en general son servios de interes
    En nuestro caso son Jardines y Geriátricos, podrían agregarse institutos
    educativos y otros similares. Son locales o sucursales de un comercio
    previamente inscripto
    """
    sucursal = models.ForeignKey(SucursalOrganizacion)
    tipo = models.ForeignKey(TipoEntePrivado, null=True, blank=True)
    descripcion_publica = models.TextField(
        null=True,
        blank=True,
        help_text=('Detalles de los servicios de la institución, sus objetivos',
                   'y otros detalles para publicar.'))
    descripcion_privada = models.TextField(
        null=True,
        blank=True,
        help_text='Detalles o notas privadas sobre el ente (no se publicará en la web)')
    legajo_interno = models.CharField(max_length=90, null=True, blank=True)
    expediente_interno = models.CharField(max_length=90, null=True, blank=True)
    fecha_inscripcion = models.DateField(
        auto_now=False, auto_now_add=False, null=True, blank=True)
    acta_inscripcion = models.FileField(
        upload_to='actas-inscripcion-recursos-tributarios/',
        null=True,
        blank=True,
        verbose_name='Acta de inscripcion de Entes Privados')

    # para los casos que corresponda
    plazas_habilitadas = models.PositiveIntegerField(default=0)
    publicado = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()

    def habilitado(self):
        last = self.habilitacionentesprivados_set.all().order_by(
            '-fecha_vencimiento_habilitacion')[:1]

        if len(last) == 0:  # si no tiene habilitacion
            return False

        return last[0].fecha_vencimiento_habilitacion > date.today()

    EST_SIN_HABILITACION = 10
    EST_HABILITACION_VENCIDA = 20
    EST_VENCE_EN_30 = 30
    EST_VENCE_EN_60 = 40
    EST_HABILITADO = 50
    EST_HABILITADO_MAS_60 = 60  # habilitado y con vencimiento en mas de 60 días
    estados_habilitaciones = {
        EST_SIN_HABILITACION: 'Sin ninguna habilitación',
        EST_HABILITACION_VENCIDA: 'Habilitación vencida',
        EST_VENCE_EN_30: 'Vence en menos de 30 días',
        EST_VENCE_EN_60: 'Vence en menos de 60 días',
        EST_HABILITADO: 'Habilitado',
        EST_HABILITADO_MAS_60: 'Habilitado con vto mas de 60 días'}

    def estado_habilitacion(self):
        last = self.habilitacionentesprivados_set.all().order_by(
            '-fecha_vencimiento_habilitacion')[:1]

        ret = self.EST_HABILITADO
        if len(last) == 0:  # si no tiene habilitacion
            ret = self.EST_SIN_HABILITACION
        else:
            hab = last[0]
            if hab.fecha_vencimiento_habilitacion is None:
                ret = self.EST_SIN_HABILITACION
            elif hab.fecha_vencimiento_habilitacion < date.today():
                ret = self.EST_HABILITACION_VENCIDA
            elif hab.fecha_vencimiento_habilitacion < date.today() + timedelta(days=30):
                ret = self.EST_VENCE_EN_30
            elif hab.fecha_vencimiento_habilitacion < date.today() + timedelta(days=60):
                ret = self.EST_VENCE_EN_60

        return self.estados_habilitaciones[ret]

    def __str__(self):
        return "{} | {} [{}]".format(self.sucursal.nombre,
                                     self.sucursal.organizacion.nombre,
                                     self.sucursal.organizacion.CUIT)

    class Meta:
        ordering = ['sucursal__organizacion__nombre']
        verbose_name_plural = "Entes privados"
        verbose_name = "Ente privado"


class FotoEntePrivado(models.Model):
    """ fotos del lugar """
    ente = models.ForeignKey(EntePrivado, on_delete=models.CASCADE)
    orden = models.PositiveIntegerField(
        default=100,
        help_text='Mientras más bajo la foto es más importante (es una forma de ordenar)')
    foto = VersatileImageField(upload_to='imagenes/entes-privados')
    descripcion_publica = models.TextField(
        null=True, blank=True, help_text='Detalles para publicar sobre la foto')
    descripcion_privada = models.TextField(
        null=True, blank=True, help_text='Detalles o notas privadas de la foto')

    class Meta:
        verbose_name = "Fotos del Ente privado"
        verbose_name_plural = "Fotos de entes privados"
        ordering = ['orden']


class HabilitacionEntesPrivados(models.Model):
    """ Cada proveedor del gobierno """
    ente = models.ForeignKey(EntePrivado)
    resolucion = models.CharField(max_length=90, null=True, blank=True)
    tarjeta_habilitacion = models.CharField(
        max_length=90, null=True, blank=True)
    fecha_inicio_habilitacion = models.DateField(
        auto_now=False, auto_now_add=False, null=True, blank=True)
    fecha_vencimiento_habilitacion = models.DateField(
        auto_now=False, auto_now_add=False, null=True, blank=True)
    acta_habilitacion = models.FileField(
        upload_to='actas-habilitacion-entes-privados/',
        null=True,
        blank=True,
        verbose_name='Acta de habilitación del Ente Privado')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()

    descripcion_publica = models.TextField(
        null=True, blank=True, help_text='Detalles para publicar')
    descripcion_privada = models.TextField(
        null=True, blank=True, help_text='Detalles o notas privadas')

    def __str__(self):
        nombre = self.ente.sucursal.organizacion.nombre if not self.ente.sucursal.nombre else self.ente.sucursal.nombre
        return "{} [{}-{}]".format(nombre,
                                   self.fecha_inicio_habilitacion,
                                   self.fecha_vencimiento_habilitacion)

    class Meta:
        verbose_name_plural = "Habilitaciones de Entes Privados"
        verbose_name = "Habilitaciones Ente Privado"
        ordering = ['-resolucion']


class FotoMomentoHabilitacionEntePrivado(models.Model):
    """ fotos sacadas en el momento de la visita de habilitación """
    habilitacion = models.ForeignKey(
        HabilitacionEntesPrivados,
        on_delete=models.CASCADE)
    orden = models.PositiveIntegerField(
        default=100,
        help_text='Mientras más bajo la foto es más importante (es una forma de ordenar)')
    foto = VersatileImageField(upload_to='imagenes/entes-privados')
    descripcion_publica = models.TextField(
        null=True, blank=True, help_text='Detalles para publicar sobre la foto')
    descripcion_privada = models.TextField(
        null=True, blank=True, help_text='Detalles o notas privadas de la foto')

    class Meta:
        verbose_name = "Fotos al momento de habilitar Ente privado"
        verbose_name_plural = "Fotoss al momento de habilitar entes privados"
        ordering = ['orden']
