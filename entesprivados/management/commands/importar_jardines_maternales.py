#!/usr/bin/python
from django.core.management.base import BaseCommand

from portaldedatos.models import ArchivoCSV
from organizaciones.models import Organizacion, SucursalOrganizacion
from inscripcionempresas.models import InscripcionEmpresa
from entesprivados.models import EntePrivado, HabilitacionEntesPrivados, TipoEntePrivado
from django.db import transaction
import csv
import sys
import datetime
from datetime import date
from dateutil.relativedelta import relativedelta


"""
EJEMPLO DE DATOS a importar

EXPTE,NOMBRE FANTASIA,DOMICILIO,BARRIO,SECC,TITULAR,CUIT,INSCR. REC.TRIB.,INSCR. ENTES PRIV.,TH Nº,FECHA
297907/11,Jardin Maternal Privado 'LOS OSITOS',Gabriel Miro N° 4685,PARQUE LICEO II SECCION,17,"GARCIA, Liliana Gabriela",27-22773663-7,26/04/1999,25/09/2003,EP 1977,22/06/2016
302395/11,Jardin Maternal Privado 'MI CLUB',Corrientes N° 3930,ALTAMIRA,,"GARCIA, Sonia Beatriz",27-17629120-1,15/04/2002,18/04/2002,EP 1824,14/12/2014

"""


class Command(BaseCommand):
    help = """Comando para importar lista de jardines maternales.
                Requiere un CSV cargado en el sistema. Se marcará como _procesado_
                luego de ejecutarse para evitar duplicados
            """

    def add_arguments(self, parser):
        parser.add_argument(
            '--csv_id',
            type=int,
            help='ID del CSV cargado al sistema')
        parser.add_argument('--force', action='store_true', dest='force',
                            default=False, help="forzar a procesar el archivo")

    @transaction.atomic
    def handle(self, *args, **options):
        force = options['force']
        if force:
            self.stdout.write(self.style.WARNING(
                '--- Forzando importación de CSV ---'))

        try:
            if force:
                instanceCSV = ArchivoCSV.objects.get(pk=options['csv_id'])
            else:
                instanceCSV = ArchivoCSV.objects.get(
                    pk=options['csv_id'], procesado=False)
        except ArchivoCSV.DoesNotExist:
            if force:
                self.stdout.write(
                    self.style.ERROR(
                        'El CSV id: %s no existe' %
                        options['csv_id']))
            else:
                self.stdout.write(
                    self.style.ERROR(
                        'El CSV id: %s no existe o ya está procesado' %
                        options['csv_id']))
            sys.exit(1)

        if instanceCSV is None:
            self.stdout.write(self.style.ERROR('No hay CSV'))
            sys.exit(1)

        self.stdout.write(
            self.style.SUCCESS(
                'Importando csv (id: {})'.format(
                    instanceCSV.id)))

        # TIENEN QUE TENER ENCABEZADO!
        if not instanceCSV.tiene_fila_encabezado:
            self.stdout.write(self.style.ERROR(
                'El CSV indica que no tiene encabezado. Se procesará igualemente'))

        # asegurarse de que el archivo tenga la misma estructura (encabezados)
        fieldnames = [
            'EXPTE',
            'NOMBRE FANTASIA',
            'DOMICILIO',
            'BARRIO',
            'SECC',
            'TITULAR',
            'CUIT',
            'INSCR. REC.TRIB.',
            'INSCR. ENTES PRIV.',
            'TH Nº',
            'FECHA']
        count = 0
        with open(instanceCSV.archivo_local.path) as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=instanceCSV.separado_por,
                                    quotechar=instanceCSV.contenedor_de_texto)

            header = reader.__next__()
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR(
                    'BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            for row in reader:
                count += 1
                # chequear los datos
                errores = []
                self.stdout.write(
                    self.style.SUCCESS(
                        'Linea leida: {}'.format(row)))

                institucion = row['NOMBRE FANTASIA'].strip()
                domicilio = row['DOMICILIO'].strip()
                barrio = row['BARRIO'].strip()
                # seccional = row['Secc'].strip()
                responsable = row['TITULAR'].strip()
                cuit = row['CUIT'].strip()
                fecha_inscripcion = None if row['INSCR. REC.TRIB.'].strip(
                ) == '' else datetime.datetime.strptime(row['INSCR. REC.TRIB.'].strip(), "%d/%m/%Y").date()

                fecha_ep = None if row['INSCR. ENTES PRIV.'].strip() == '' else datetime.datetime.strptime(
                    row['INSCR. ENTES PRIV.'].strip(), "%d/%m/%Y").date()

                nro_habilitacion = row['TH Nº'].strip()
                # Si empieza con EP o similares es por 5 años
                # las provisorias son por 180 días.
                delta = relativedelta(months=6)

                if nro_habilitacion[:3] in ['EP ', 'E.P', 'EP.', ' EP', 'E P']:
                    delta = relativedelta(years=5)

                # resolucion = row['Res. Nº'].strip()
                expediente = row['EXPTE'].strip()
                fecha_inicio_habilitacion = None
                try:
                    fecha_inicio = row['FECHA'].strip()
                    fecha_inicio_habilitacion = datetime.datetime.strptime(
                        fecha_inicio, "%d/%m/%Y").date()
                except Exception as e:
                    self.stdout.write(
                        self.style.ERROR(
                            'Fecha de inicio de habilitación inválida "{}"'.format(fecha_inicio)))
                else:
                    fecha_vencimiento_habilitacion = fecha_inicio_habilitacion + delta

                # en base al CUIT, en primer lugar, asegurarse que exista la
                # Empresa (organizacion)
                organizacion, created = Organizacion.objects.get_or_create(
                    CUIT=cuit)
                organizacion.nombre = responsable
                organizacion.save()

                inscripcion, created = InscripcionEmpresa.objects.get_or_create(
                    organizacion=organizacion)
                inscripcion.fecha_inscripcion = fecha_inscripcion
                inscripcion.save()

                sucursal, created = SucursalOrganizacion.objects.get_or_create(
                    organizacion=organizacion, direccion=domicilio)
                sucursal.nombre = institucion
                sucursal.barrio = barrio
                sucursal.save()

                tipo_ente, created = TipoEntePrivado.objects.get_or_create(
                    nombre='Jardín Maternal')

                ente, created = EntePrivado.objects.get_or_create(
                    sucursal=sucursal)
                ente.tipo = tipo_ente
                ente.fecha_inscripcion = fecha_ep
                ente.expediente_interno = expediente
                ente.save()
                if fecha_inicio_habilitacion is not None:
                    habilitacion, created = HabilitacionEntesPrivados.objects.get_or_create(
                        ente=ente)
                    # habilitacion.resolucion = resolucion
                    habilitacion.tarjeta_habilitacion = nro_habilitacion
                    habilitacion.fecha_inicio_habilitacion = fecha_inicio_habilitacion
                    habilitacion.fecha_vencimiento_habilitacion = fecha_vencimiento_habilitacion
                    habilitacion.save()

                if len(errores) > 0:
                    self.stdout.write(
                        self.style.ERROR(
                            'ERRORES AL PROCESAR LA LINEA {}'.format(count)))
                    for e in errores:
                        self.stdout.write(self.style.ERROR(e))
                    sys.exit(1)

        instanceCSV.procesado = True
        instanceCSV.save()

        self.stdout.write(
            self.style.SUCCESS(
                'Archivo cargado con éxito, se cargaron {} registros'.format(count)))
