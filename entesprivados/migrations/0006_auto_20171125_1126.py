# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-25 14:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('entesprivados', '0005_auto_20170726_1652'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalenteprivado',
            name='history_change_reason',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='historicalhabilitacionentesprivados',
            name='history_change_reason',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='historicaltipoenteprivado',
            name='history_change_reason',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='historicalenteprivado',
            name='history_type',
            field=models.CharField(choices=[('+', 'Fecha de creación'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1),
        ),
        migrations.AlterField(
            model_name='historicalhabilitacionentesprivados',
            name='history_type',
            field=models.CharField(choices=[('+', 'Fecha de creación'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1),
        ),
        migrations.AlterField(
            model_name='historicaltipoenteprivado',
            name='history_type',
            field=models.CharField(choices=[('+', 'Fecha de creación'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1),
        ),
    ]
