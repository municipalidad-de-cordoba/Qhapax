# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2019-01-29 17:03
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import versatileimagefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('entesprivados', '0006_auto_20171125_1126'),
    ]

    operations = [
        migrations.CreateModel(
            name='FotoEntePrivado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('orden', models.PositiveIntegerField(default=100, help_text='Mientras más bajo la foto es más importante (es una forma de ordenar)')),
                ('foto', versatileimagefield.fields.VersatileImageField(upload_to='imagenes/entes-privados')),
                ('descripcion_publica', models.TextField(blank=True, help_text='Detalles para publicar sobre la foto', null=True)),
                ('descripcion_privada', models.TextField(blank=True, help_text='Detalles o notas privadas de la foto', null=True)),
            ],
            options={
                'ordering': ['orden'],
                'verbose_name': 'Fotos del Ente privado',
                'verbose_name_plural': 'Fotos de entes privados',
            },
        ),
        migrations.CreateModel(
            name='FotoMomentoHabilitacionEntePrivado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('orden', models.PositiveIntegerField(default=100, help_text='Mientras más bajo la foto es más importante (es una forma de ordenar)')),
                ('foto', versatileimagefield.fields.VersatileImageField(upload_to='imagenes/entes-privados')),
                ('descripcion_publica', models.TextField(blank=True, help_text='Detalles para publicar sobre la foto', null=True)),
                ('descripcion_privada', models.TextField(blank=True, help_text='Detalles o notas privadas de la foto', null=True)),
            ],
            options={
                'ordering': ['orden'],
                'verbose_name': 'Fotos al momento de habilitar Ente privado',
                'verbose_name_plural': 'Fotoss al momento de habilitar entes privados',
            },
        ),
        migrations.AddField(
            model_name='enteprivado',
            name='descripcion_privada',
            field=models.TextField(blank=True, help_text='Detalles o notas privadas sobre el ente (no se publicará en la web)', null=True),
        ),
        migrations.AddField(
            model_name='enteprivado',
            name='descripcion_publica',
            field=models.TextField(blank=True, help_text='Detalles de los servicios de la institución, sus objetivos y otros detalles para publicar.', null=True),
        ),
        migrations.AddField(
            model_name='habilitacionentesprivados',
            name='descripcion_privada',
            field=models.TextField(blank=True, help_text='Detalles o notas privadas', null=True),
        ),
        migrations.AddField(
            model_name='habilitacionentesprivados',
            name='descripcion_publica',
            field=models.TextField(blank=True, help_text='Detalles para publicar', null=True),
        ),
        migrations.AddField(
            model_name='historicalenteprivado',
            name='descripcion_privada',
            field=models.TextField(blank=True, help_text='Detalles o notas privadas sobre el ente (no se publicará en la web)', null=True),
        ),
        migrations.AddField(
            model_name='historicalenteprivado',
            name='descripcion_publica',
            field=models.TextField(blank=True, help_text='Detalles de los servicios de la institución, sus objetivos y otros detalles para publicar.', null=True),
        ),
        migrations.AddField(
            model_name='historicalhabilitacionentesprivados',
            name='descripcion_privada',
            field=models.TextField(blank=True, help_text='Detalles o notas privadas', null=True),
        ),
        migrations.AddField(
            model_name='historicalhabilitacionentesprivados',
            name='descripcion_publica',
            field=models.TextField(blank=True, help_text='Detalles para publicar', null=True),
        ),
        migrations.AddField(
            model_name='fotomomentohabilitacionenteprivado',
            name='habilitacion',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='entesprivados.HabilitacionEntesPrivados'),
        ),
        migrations.AddField(
            model_name='fotoenteprivado',
            name='ente',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='entesprivados.EntePrivado'),
        ),
    ]
