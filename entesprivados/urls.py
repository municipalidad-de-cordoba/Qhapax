from django.conf.urls import url

from . import views

urlpatterns = [url(r'^ente-(?P<tipo_id>[0-9]+).(?P<filetype>csv|xls)$',
                   views.lista_entes, name='entesprivados.lista'), ]
