from django.views.decorators.cache import cache_page
from entesprivados.models import EntePrivado
import django_excel as excel
from entesprivados import settings as settings_ep


@cache_page(60 * 60)  # 1 hora
def lista_entes(request, tipo_id, filetype):
    '''
    lista de entes públicos según tipo
    '''
    ids = [ente.id for ente in EntePrivado.objects.filter(
        publicado=True, tipo=tipo_id) if ente.habilitado()]
    entes = EntePrivado.objects.filter(id__in=ids)
    entes.order_by('sucursal__nombre')

    csv_list = []
    csv_list.append(['ID', 'Nombre', 'Titular', 'CUIT', 'Inscr Ente', 'Estado',
                     'Dirección', 'Plazas habilitadas'])
    for ente in entes:
        sucursal = ente.sucursal
        organizacion = sucursal.organizacion
        direccion = '{} {}'.format(sucursal.direccion, sucursal.barrio)
        csv_list.append([ente.id,
                         sucursal.nombre,
                         organizacion.nombre,
                         organizacion.CUIT,
                         ente.fecha_inscripcion,
                         ente.estado_habilitacion(),
                         direccion,
                         ente.plazas_habilitadas])

    filename = "entes-privados"
    if int(tipo_id) == settings_ep.TIPO_GERIATRICOS_ID:
        filename = 'entes-privados-geriatricos'
    elif int(tipo_id) == settings_ep.TIPO_JARDINES_ID:
        filename = 'entes-privados-jardines-maternales'

    return excel.make_response(
        excel.pe.Sheet(csv_list),
        filetype,
        file_name=filename)
