from django.apps import AppConfig


class EntesprivadosConfig(AppConfig):
    name = 'entesprivados'
