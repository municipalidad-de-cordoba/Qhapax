from django.contrib import admin
from .models import (EntePrivado, FotoEntePrivado,
                     FotoMomentoHabilitacionEntePrivado,
                     HabilitacionEntesPrivados, TipoEntePrivado)
from simple_history.admin import SimpleHistoryAdmin
from django.db.models import Max, Count
from datetime import date, timedelta
from .forms import HabilitacionEntesPrivadosFormSelect2, EntePrivadoFormSelect2


class FotoEntePrivadoInline(admin.StackedInline):
    model = FotoEntePrivado
    extra = 1


class FotoMomentoHabilitacionEntePrivadoInline(admin.StackedInline):
    model = FotoMomentoHabilitacionEntePrivado
    extra = 1


class TipoEntePrivadoAdmin(SimpleHistoryAdmin):
    list_display = ("nombre", )


class HabilitadoListFilter(admin.SimpleListFilter):
    title = 'estado habilitación ente'

    parameter_name = 'ente_estado_habilitacion'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return EntePrivado.estados_habilitaciones.items()

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        v = None if self.value() is None else int(self.value())
        if v == EntePrivado.EST_SIN_HABILITACION:
            q = queryset.annotate(
                habs=Count('habilitacionentesprivados')).filter(
                habs=0)
        elif v == EntePrivado.EST_HABILITACION_VENCIDA:
            q = queryset.annotate(
                vto=Max('habilitacionentesprivados__fecha_vencimiento_habilitacion')).filter(
                vto__lt=date.today())
        elif v == EntePrivado.EST_VENCE_EN_30:
            q = queryset.annotate(
                vto=Max('habilitacionentesprivados__fecha_vencimiento_habilitacion')).filter(
                vto__gt=date.today(),
                vto__lt=date.today() +
                timedelta(
                    days=30))
        elif v == EntePrivado.EST_VENCE_EN_60:
            q = queryset.annotate(
                vto=Max('habilitacionentesprivados__fecha_vencimiento_habilitacion')).filter(
                vto__gt=date.today() +
                timedelta(
                    days=30),
                vto__lt=date.today() +
                timedelta(
                    days=60))
        elif v == EntePrivado.EST_HABILITADO:
            q = queryset.annotate(
                vto=Max('habilitacionentesprivados__fecha_vencimiento_habilitacion')).filter(
                vto__gt=date.today())
        elif v == EntePrivado.EST_HABILITADO_MAS_60:
            q = queryset.annotate(
                vto=Max('habilitacionentesprivados__fecha_vencimiento_habilitacion')).filter(
                vto__gt=date.today() +
                timedelta(
                    days=60))
        else:
            q = queryset

        return q


class EntePrivadoAdmin(SimpleHistoryAdmin):
    form = EntePrivadoFormSelect2

    def ultimo_editor(self, obj):
        return obj.history.all()[0].history_user

    def sucursal_nombre(self, obj):
        return obj.sucursal.nombre
    sucursal_nombre.admin_order_field = 'organizaciones_sucursalorganizacion.nombre'

    def sucursal_direccion(self, obj):
        return obj.sucursal.direccion
    sucursal_direccion.admin_order_field = 'organizaciones_sucursalorganizacion.direccion'

    def organizacion(self, obj):
        return obj.sucursal.organizacion.nombre

    def habilitado(self, obj):
        return obj.habilitado()

    def estado_habilitacion(self, obj):
        return obj.estado_habilitacion()

    list_display = (
        'id',
        'sucursal_nombre',
        'organizacion',
        'estado_habilitacion',
        'publicado',
        'sucursal_direccion',
        'tipo',
        'legajo_interno',
        'fecha_inscripcion',
        'expediente_interno',
        'ultimo_editor')
    search_fields = [
        'sucursal__organizacion__nombre',
        'sucursal__organizacion__CUIT',
        'sucursal__nombre',
        'sucursal__direccion',
        'sucursal__barrio']
    list_filter = ['tipo', 'publicado', HabilitadoListFilter]
    inlines = [FotoEntePrivadoInline]

    # sin esto no funciona el JS de select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


class HabilitacionEntesPrivadosAdmin(SimpleHistoryAdmin):
    form = HabilitacionEntesPrivadosFormSelect2

    def ultimo_editor(self, obj):
        return obj.history.all()[0].history_user

    def sucursal(self, obj):
        return obj.ente.sucursal.nombre

    def organizacion(self, obj):
        return obj.ente.sucursal.organizacion.nombre

    def organizacion_cuit(self, obj):
        return obj.ente.sucursal.organizacion.CUIT

    list_display = (
        'id',
        'sucursal',
        'organizacion',
        'organizacion_cuit',
        'resolucion',
        'tarjeta_habilitacion',
        'fecha_inicio_habilitacion',
        'fecha_vencimiento_habilitacion',
        'ultimo_editor')
    search_fields = ['ente__sucursal__organizacion__nombre',
                     'ente__sucursal__organizacion__CUIT',
                     'ente__sucursal__nombre',
                     'ente__sucursal__direccion']

    inlines = [FotoMomentoHabilitacionEntePrivadoInline]

    # sin esto no funciona el JS de select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


admin.site.register(TipoEntePrivado, TipoEntePrivadoAdmin)
admin.site.register(EntePrivado, EntePrivadoAdmin)
admin.site.register(HabilitacionEntesPrivados, HabilitacionEntesPrivadosAdmin)
