from django import forms
from django_select2.forms import ModelSelect2Widget
from entesprivados.models import EntePrivado, HabilitacionEntesPrivados
from organizaciones.models import SucursalOrganizacion


class SucursalOrganizacionWidget(ModelSelect2Widget):
    model = SucursalOrganizacion
    search_fields = ['nombre__icontains',
                     'organizacion__CUIT__contains',
                     'organizacion__nombre__icontains']
    max_results = 5

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault(
            'data-placeholder',
            'Ingrese parte del CUIT o nombre')
        self.attrs.setdefault('data-minimum-input-length', 3)
        self.attrs.setdefault('data-width', '45em')

        return super(
            SucursalOrganizacionWidget,
            self).build_attrs(
            *
            args,
            **kwargs)


class EntePrivadoFormSelect2(forms.ModelForm):
    class Meta:
        model = EntePrivado

        fields = [
            'sucursal',
            'tipo',
            'descripcion_publica',
            'descripcion_privada',
            'legajo_interno',
            'expediente_interno',
            'fecha_inscripcion',
            'acta_inscripcion',
            'publicado',
            'plazas_habilitadas']
        widgets = {'sucursal': SucursalOrganizacionWidget}


class EntePrivadoWidget(ModelSelect2Widget):
    model = EntePrivado
    search_fields = ['sucursal__nombre__icontains',
                     'sucursal__organizacion__CUIT__contains',
                     'sucursal__organizacion__nombre__icontains']
    max_results = 5

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault(
            'data-placeholder',
            'Ingrese parte del CUIT o nombre')
        self.attrs.setdefault('data-minimum-input-length', 3)
        self.attrs.setdefault('data-width', '45em')

        return super(EntePrivadoWidget, self).build_attrs(*args, **kwargs)


class HabilitacionEntesPrivadosFormSelect2(forms.ModelForm):
    class Meta:
        model = HabilitacionEntesPrivados

        fields = [
            'ente',
            'resolucion',
            'tarjeta_habilitacion',
            'fecha_inicio_habilitacion',
            'fecha_vencimiento_habilitacion',
            'acta_habilitacion']
        widgets = {'ente': EntePrivadoWidget}
