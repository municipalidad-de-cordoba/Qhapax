from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import EntePrivadoSerializer
from api.pagination import DefaultPagination
from django.db.models import Q
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from entesprivados.models import EntePrivado
from entesprivados import settings as settings_ep


@method_decorator(cache_page(60 * 60 * 12), name='dispatch')
class GeriatricosViewSet(viewsets.ModelViewSet):
    """
    Lista de geriátricos habilitados y publicados por tipo.
    Se puede buscar por nombres de la sucursal, organizacion, comicilio y CUIT
    con el param "q"
    Por ejemplo "q=perez"
    """
    serializer_class = EntePrivadoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # entregar los publicados y los habilitados
        ids = [
            ente.id for ente in EntePrivado.objects.filter(
                publicado=True) if ente.habilitado()]
        queryset = EntePrivado.objects.filter(id__in=ids)
        queryset = queryset.filter(tipo__id=settings_ep.TIPO_GERIATRICOS_ID)

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(sucursal__nombre__icontains=q) |
                Q(sucursal__organizacion__nombre__icontains=q) |
                Q(sucursal__organizacion__CUIT__icontains=q) |
                Q(sucursal__direccion__icontains=q)
            )

        queryset.order_by('sucursal__nombre')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


@method_decorator(cache_page(60 * 60 * 12), name='dispatch')
class JardinesViewSet(viewsets.ModelViewSet):
    """
    Lista de jardines maternales habilitados y publicados por tipo.
    Se puede buscar por nombres de la sucursal, organizacion, comicilio y CUIT
    con el param "q"
    Por ejemplo "q=perez"
    """
    serializer_class = EntePrivadoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        # entregar los publicados y los habilitados
        ids = [
            ente.id for ente in EntePrivado.objects.filter(
                publicado=True) if ente.habilitado()]
        queryset = EntePrivado.objects.filter(id__in=ids)
        queryset = queryset.filter(tipo__id=settings_ep.TIPO_JARDINES_ID)

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(sucursal__nombre__icontains=q) |
                Q(sucursal__organizacion__nombre__icontains=q) |
                Q(sucursal__organizacion__CUIT__icontains=q) |
                Q(sucursal__direccion__icontains=q)
            )

        queryset.order_by('sucursal__nombre')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
