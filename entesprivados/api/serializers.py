from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from entesprivados.models import EntePrivado


class EntePrivadoSerializer(CachedSerializerMixin):
    nombre = serializers.CharField(source='sucursal.nombre')
    titular = serializers.CharField(source='sucursal.organizacion.nombre')
    CUIT = serializers.CharField(source='sucursal.organizacion.CUIT')
    estado = serializers.CharField(source='estado_habilitacion')
    direccion = serializers.CharField(source='sucursal.direccion')

    class Meta:
        model = EntePrivado
        fields = [
            'id',
            'nombre',
            'titular',
            'CUIT',
            'direccion',
            'fecha_inscripcion',
            'estado',
            'plazas_habilitadas']


# Registro los serializadores en la cache de DRF
cache_registry.register(EntePrivadoSerializer)
