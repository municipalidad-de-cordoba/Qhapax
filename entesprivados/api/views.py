from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import EntePrivadoSerializer
from api.pagination import DefaultPagination
from entesprivados.models import EntePrivado


class EntePrivadoViewSet(viewsets.ModelViewSet):
    """
    Cada una de las versiones de los datos del portal
    """
    serializer_class = EntePrivadoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = EntePrivado.objects.filter(publicado=True)
        queryset.order_by('sucursal__nombre')
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
