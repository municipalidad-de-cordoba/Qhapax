from django.conf.urls import url, include
from rest_framework import routers
from .views import EntePrivadoViewSet
from .views_tmp import GeriatricosViewSet, JardinesViewSet

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'entes-privados',
    EntePrivadoViewSet,
    base_name='entes-privados.api')
router.register(
    r'geriatricos',
    GeriatricosViewSet,
    base_name='geriatricos.api')
router.register(r'jardines', JardinesViewSet, base_name='jardines.api')

urlpatterns = [
    url(r'^', include(router.urls)),
]
