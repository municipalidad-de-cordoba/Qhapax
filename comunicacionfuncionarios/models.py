from django.db import models
from funcionarios.models import Cargo, CargoCategoria, Funcion
from ckeditor.fields import RichTextField


class TipoActividadFuncionarios(models.Model):
    """ tipos de actividades """
    titulo = models.CharField(max_length=90)

    def __str__(self):
        return self.titulo


class ActividadFuncionarios(models.Model):
    """ de las actividades públicas administradas desde
        eventospublicos.ActividadPublica algunas se marcarán
        para que sea notificada a la app de funcionarios """
    titulo = models.CharField(max_length=90)
    descripcion = RichTextField(null=True, blank=True)
    tipo = models.ForeignKey(
        TipoActividadFuncionarios,
        on_delete=models.SET_NULL,
        null=True,
        blank=True)
    publicado = models.BooleanField(default=True)

    oficina_invita = models.ForeignKey(
        Cargo,
        null=True,
        blank=True,
        related_name='invita_actividades_funcionarios')
    fecha = models.DateTimeField(null=True, blank=True)

    # FIXME cuando tengamos geoDjango poner un PointField
    lugar = models.CharField(
        max_length=130,
        help_text='Dirección o ubicación claramente definida')
    url_mas_info = models.URLField(
        blank=True,
        help_text='Link a más datos a al mapa de ubicación',
        null=True)

    INV_TODOS = 10
    INV_NINGUNO = 20
    INV_ALGUNOS = 30
    publicos_invitados = ((INV_TODOS, 'Todos'),
                          (INV_NINGUNO, 'Ninguno'),
                          (INV_ALGUNOS, 'Solo los marcados'))
    funcionarios_invitados = models.PositiveIntegerField(
        choices=publicos_invitados, default=INV_NINGUNO)

    OBL_TODOS = 10
    OBL_NINGUNO = 20
    OBL_ALGUNOS = 30
    publicos_obligados = ((OBL_TODOS, 'Todos'),
                          (OBL_NINGUNO, 'Ninguno'),
                          (OBL_ALGUNOS, 'Solo los marcados'))
    funcionarios_obligados = models.PositiveIntegerField(
        choices=publicos_obligados, default=OBL_NINGUNO)

    # en caso de que se elija ALGUNOS
    obligatorio_para_cargos = models.ManyToManyField(
        Cargo, blank=True, related_name='obligado_actividades_funcionarios')
    obligatorio_para_categorias = models.ManyToManyField(
        CargoCategoria, blank=True)
    creado = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.titulo


class NotificacionActividadFuncionario(models.Model):
    """ respuestas previas de los funcionarios sobre si asistirán """
    actividad = models.ForeignKey(
        ActividadFuncionarios,
        on_delete=models.CASCADE)
    funcionario = models.ForeignKey(Funcion, on_delete=models.CASCADE)

    NOT_ENVIADA = 10
    NOT_LEIDA = 20
    NOT_RESPONDIDA_NO_ASISTIRA = 30
    NOT_RESPONDIDA_NO_SABE = 40
    NOT_RESPONDIDA_ASISTIRA = 50

    estados_notificacion = (
        (NOT_ENVIADA,
         'Notificación enviada sin respuesta'),
        (NOT_LEIDA,
         'Notificación leída y sin respuesta'),
        (NOT_RESPONDIDA_NO_ASISTIRA,
         'Confirmado que no aistirá'),
        (NOT_RESPONDIDA_NO_SABE,
         'No sabe aún si asistirá'),
        (NOT_RESPONDIDA_ASISTIRA,
         'Confirmado que asistirá'))

    estado_notificacion = models.PositiveIntegerField(
        choices=estados_notificacion, default=NOT_ENVIADA)
    creado = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} {} => {}'.format(
            self.actividad.titulo,
            self.funcionario.funcionario.nombrepublico,
            self.get_estado_notificacion_display())


class AsistenciaActividadFuncionario(models.Model):
    """ asistencia de funcionarios a actividades a las que fueron citados """
    actividad = models.ForeignKey(
        ActividadFuncionarios,
        on_delete=models.CASCADE)
    funcionario = models.ForeignKey(Funcion, on_delete=models.CASCADE)
    asistio = models.NullBooleanField(null=True, default=None)
    creado = models.DateTimeField(auto_now_add=True)
    # cuando tengamos GeoDjango ==> asistio_point = models.PointField()

    def __str__(self):
        return '{} {} ASISTIO:{}'.format(
            self.actividad.titulo,
            self.funcionario.funcionario.nombrepublico,
            self.asistio)


"""
class MemoInterno():
    Mensajes de notificación semi formales internos


class TipoNotificacionFuncionario(models.Model):
    ''' tipos de alertas que un funcionario puede notificar '''
    nombre = models.CharField(max_length=80)
    descripcion = models.TextField(null=True, blank=True)


class NotificacionFuncionario(models.Model):
    ''' Notificación de los funcionarios de
        - asambleas internas
        - viajes a charlas, cursos, seminarios, etc
        - vacaciones
    '''
    tipo = models.ForeignKey(TipoNotificacionFuncionario, on_delete=models.SET_NULL, null=True)

    gravedades = ((URGENTE, 'Urgente'), (IMPORTANTE, 'Importante'), (NORMAL, 'Normal'))
    gravedad = models.PositiveIntegerField(choices=gravedades, default=NORMAL)

"""
