# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-07-17 19:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('comunicacionfuncionarios', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='actividadfuncionarios',
            name='funcionarios_invitados',
            field=models.PositiveIntegerField(choices=[(10, 'Todos'), (20, 'Ninguno'), (30, 'Solo los marcados')], default=20),
        ),
    ]
