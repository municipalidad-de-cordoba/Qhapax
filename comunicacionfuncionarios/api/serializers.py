
from rest_framework.serializers import ModelSerializer
from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry

from ..models import (
    ActividadFuncionarios, TipoActividadFuncionarios,
    NotificacionActividadFuncionario, AsistenciaActividadFuncionario
)


class TipoActividadFuncionariosSerializer(
        CachedSerializerMixin, ModelSerializer):

    class Meta:
        model = TipoActividadFuncionarios
        fields = '__all__'


class ActividadFuncionariosSerializer(CachedSerializerMixin, ModelSerializer):

    class Meta:
        model = ActividadFuncionarios
        fields = '__all__'


class NotificacionActividadFuncionarioSerializer(
        CachedSerializerMixin, ModelSerializer):

    class Meta:
        model = NotificacionActividadFuncionario
        fields = '__all__'


class AsistenciaActividadFuncionarioSerializer(
        CachedSerializerMixin, ModelSerializer):

    class Meta:
        model = AsistenciaActividadFuncionario
        fields = '__all__'


# Registro los serializadores en la cache de DRF
cache_registry.register(TipoActividadFuncionariosSerializer)
cache_registry.register(ActividadFuncionariosSerializer)
cache_registry.register(NotificacionActividadFuncionarioSerializer)
cache_registry.register(AsistenciaActividadFuncionarioSerializer)
