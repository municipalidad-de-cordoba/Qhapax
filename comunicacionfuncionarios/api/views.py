from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissions
from .serializers import (
    ActividadFuncionariosSerializer, TipoActividadFuncionariosSerializer,
    NotificacionActividadFuncionarioSerializer,
    AsistenciaActividadFuncionarioSerializer
)

from ..models import (
    ActividadFuncionarios, TipoActividadFuncionarios,
    NotificacionActividadFuncionario, AsistenciaActividadFuncionario
)


class TipoActividadFuncionariosViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = TipoActividadFuncionariosSerializer
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        queryset = TipoActividadFuncionarios.objects.all()
        return queryset


class ActividadFuncionariosViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = ActividadFuncionariosSerializer
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        queryset = ActividadFuncionarios.objects.filter(publicado=True)
        return queryset


class NotificacionActividadFuncionarioViewSet(viewsets.ModelViewSet):
    serializer_class = NotificacionActividadFuncionarioSerializer
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        queryset = NotificacionActividadFuncionario.objects.filter(
            publicado=True)
        return queryset


class AsistenciaActividadFuncionarioViewSet(viewsets.ModelViewSet):
    serializer_class = AsistenciaActividadFuncionarioSerializer
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        queryset = AsistenciaActividadFuncionario.objects.all()
        return queryset
