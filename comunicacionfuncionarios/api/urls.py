from rest_framework import routers
from .views import (
    ActividadFuncionariosViewSet,
    TipoActividadFuncionariosViewSet,
    AsistenciaActividadFuncionarioViewSet,
    NotificacionActividadFuncionarioViewSet)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'actividades',
    ActividadFuncionariosViewSet,
    base_name='actividades')
router.register(
    r'tipo-actividades',
    TipoActividadFuncionariosViewSet,
    base_name='actividades')
router.register(
    r'notificaciones',
    NotificacionActividadFuncionarioViewSet,
    base_name='actividades')
router.register(
    r'asistencias',
    AsistenciaActividadFuncionarioViewSet,
    base_name='actividades')

urlpatterns = router.urls
