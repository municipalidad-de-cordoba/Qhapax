from django.contrib import admin
from .models import ActividadFuncionarios, TipoActividadFuncionarios


class TipoActividadFuncionariosAdmin(admin.ModelAdmin):
    list_display = ['titulo']
    search_fields = ['titulo']


class ActividadFuncionariosAdmin(admin.ModelAdmin):
    list_display = (
        'titulo',
        'tipo',
        'publicado',
        'funcionarios_obligados',
        'fecha',
        'lugar')
    search_fields = ['titulo', 'descripcion']
    list_filter = ['tipo', 'funcionarios_obligados', 'publicado']


admin.site.register(TipoActividadFuncionarios, TipoActividadFuncionariosAdmin)
admin.site.register(ActividadFuncionarios, ActividadFuncionariosAdmin)
