from django.db import models
from core.models import Comunicacion
from catastro.models import ParcelaCatastral
from django.utils import timezone


class PrestatariaDesagotes(models.Model):
    CUIT = models.CharField(max_length=30, null=True, blank=True)
    nombre = models.CharField(max_length=250)
    direccion = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return '{} CUIT:{}'.format(self.nombre, self.CUIT)

    class Meta:
        ordering = ['nombre']
        verbose_name_plural = "Prestatarias de desagotes"
        verbose_name = "Prestataria de desagotes"


class ComunicacionPrestatariaDesagotes(Comunicacion):
    ''' permite cargar telefonos, emails, web, etc) '''
    objeto = models.ForeignKey(PrestatariaDesagotes, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("tipo", "valor", "objeto"),)


class SolicitudDesagote(models.Model):
    ''' solicitud de desagote '''
    _orig_estado = None

    def __init__(self, *args, **kwargs):
        super(SolicitudDesagote, self).__init__(*args, **kwargs)
        ''' anotar el estado para cuando lo grabe registrar el canbio '''
        self._orig_estado = self.estado

    parcela = models.ForeignKey(
        ParcelaCatastral,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        help_text=('Use el formato Distrito-Zona-Manzana-Parcela. Puede buscarlo',
                   'en https://gobiernoabierto.cordoba.gob.ar/mapas/emap/'))
    direccion = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        help_text=('Opcionalmente (y obligatoriamente en caso de que no',
                   'identifique la parcela), coloque dirección y barrio aquí'))
    dni = models.CharField(max_length=20)
    nombres = models.CharField(max_length=90)
    apellidos = models.CharField(max_length=90)
    # tienen que tener una cámara ya que nosotros desagotamos eso
    cantidad_camaras_existentes = models.PositiveIntegerField(default=1)

    cantidad_viviendas_en_terreno = models.PositiveIntegerField(default=1)
    fecha = models.DateTimeField(auto_now_add=True)
    prestataria_designada = models.ForeignKey(
        PrestatariaDesagotes,
        on_delete=models.SET_NULL,
        null=True,
        blank=True)

    # automatizar
    fecha_designacion_prestataria = models.DateTimeField(null=True, blank=True)
    fecha_desagote_terminado = models.DateTimeField(null=True, blank=True)

    observaciones_trabajo_terminado = models.TextField(null=True, blank=True)

    PENDIENTE = 10  # el niño nació y tenemos que plantar el árbol
    DERIVADO_A_PRESTATARIA = 20  # mandamos un email notificando
    RECHAZADO = 30   # lo llamamos o verificamos la recepción del correo de notificación
    TERMINADO = 40  # los padres vinieron a retirar las plantas al vivero o se las llevamos
    CERTIFICADO = 50

    estados = (
        (PENDIENTE,
         'Pendiente'),
        (DERIVADO_A_PRESTATARIA,
         'Derivado a la prestataria'),
        (RECHAZADO,
         'Trabajo rechazado'),
        (TERMINADO,
         'Trabajo terminado'),
        (CERTIFICADO,
         'Trabajo certificado'))

    estado = models.PositiveIntegerField(choices=estados, default=PENDIENTE)

    PRIORIDAD_NORMAL = 10
    PRIORIDAD_URGENTE = 20
    prioridades = ((PRIORIDAD_NORMAL, 'Normal'),
                   (PRIORIDAD_URGENTE, 'Urgente'))
    prioridad = models.PositiveIntegerField(
        choices=prioridades, default=PRIORIDAD_NORMAL)

    def __str__(self):
        return '{} {}'.format(self.nombres, self.apellidos)

    def save(self, *args, **kwargs):
        now = timezone.now()
        if self._orig_estado != self.estado:
            # cambio!
            if self.estado == self.DERIVADO_A_PRESTATARIA:
                self.fecha_designacion_prestataria = now
            elif self.estado in [self.RECHAZADO, self.TERMINADO]:
                self.fecha_desagote_terminado = now
        super(SolicitudDesagote, self).save(*args, **kwargs)

    class Meta:
        ordering = ['apellidos', 'nombres']
        verbose_name_plural = "Solicitudes de desagotes"
        verbose_name = "Solicitud de desagotes"


class PozoEnTerreno(models.Model):
    ''' datos de cada pozo '''
    solicitud = models.ForeignKey(SolicitudDesagote, on_delete=models.CASCADE)

    POZO_EN_FRENTE = 10
    POZO_EN_PATIO_TRASERO = 20
    POZO_EN_OTRO_LADO = 30
    ubicaciones_pozos = (
        (POZO_EN_FRENTE,
         'Pozo ubicado en el frente o jardín'),
        (POZO_EN_PATIO_TRASERO,
         'Pozo ubicado en el patio trasero'),
        (POZO_EN_OTRO_LADO,
         'Pozo en otra ubicación'))

    ubicacion = models.PositiveIntegerField(
        choices=ubicaciones_pozos, default=POZO_EN_OTRO_LADO)
    esta_saturado = models.NullBooleanField(default=None, null=True)
    esta_entubado = models.NullBooleanField(default=None, null=True)

    def __str__(self):
        return '{} Saturado:{} Entubado:{}'.format(
            self.get_ubicacion_display(), self.esta_saturado, self.esta_entubado)


class ComunicacionSolicitanteDesagote(Comunicacion):
    ''' permite cargar telefonos, emails, web, etc) '''
    objeto = models.ForeignKey(SolicitudDesagote, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("tipo", "valor", "objeto"),)
