# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-06-30 13:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('desagotesavecinos', '0005_solicituddesagote_estado'),
    ]

    operations = [
        migrations.AddField(
            model_name='solicituddesagote',
            name='prioridad',
            field=models.PositiveIntegerField(choices=[(10, 'Normal'), (20, 'Urgente')], default=10),
        ),
        migrations.AlterField(
            model_name='solicituddesagote',
            name='estado',
            field=models.PositiveIntegerField(choices=[(10, 'Pendiente'), (20, 'Derivado a la prestataria'), (30, 'Trabajo rechazado'), (40, 'Trabajo terminado'), (50, 'Trabajo certificado')], default=10),
        ),
    ]
