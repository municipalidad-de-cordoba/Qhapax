from django.contrib import admin
from .models import (ComunicacionPrestatariaDesagotes,
                     ComunicacionSolicitanteDesagote, PrestatariaDesagotes,
                     SolicitudDesagote, PozoEnTerreno)
from .forms import SolicitudDesagoteFormSelect2


class ComunicacionPrestatariaDesagotesInline(admin.StackedInline):
    model = ComunicacionPrestatariaDesagotes
    extra = 0


class PrestatariaDesagotesAdmin(admin.ModelAdmin):
    search_fields = ['CUIT', 'nombre']
    list_display = ('CUIT', 'nombre')
    ordering = ['nombre']
    inlines = [ComunicacionPrestatariaDesagotesInline]


class ComunicacionSolicitanteDesagoteInline(admin.StackedInline):
    model = ComunicacionSolicitanteDesagote
    extra = 0


class PozoEnTerrenoInline(admin.StackedInline):
    model = PozoEnTerreno
    extra = 0


class SolicitudDesagoteAdmin(admin.ModelAdmin):
    form = SolicitudDesagoteFormSelect2
    # agrupar los campos
    fields = ('prioridad', 'parcela', 'direccion',
              ('nombres', 'apellidos'), 'estado',
              ('cantidad_camaras_existentes', 'cantidad_viviendas_en_terreno'),
              'prestataria_designada')

    search_fields = ['nombres', 'apellidos', 'dni']
    list_display = ('apellidos', 'nombres', 'estado', 'parcela', 'direccion',
                    'cantidad_camaras_existentes',
                    'cantidad_viviendas_en_terreno',
                    'fecha', 'prestataria_designada',
                    'fecha_designacion_prestataria',
                    'fecha_desagote_terminado')
    ordering = ['apellidos', 'nombres']
    inlines = [ComunicacionSolicitanteDesagoteInline, PozoEnTerrenoInline]
    list_filter = ['estado', 'prestataria_designada']

    # sin esto no funciona el JS de select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


admin.site.register(PrestatariaDesagotes, PrestatariaDesagotesAdmin)
admin.site.register(SolicitudDesagote, SolicitudDesagoteAdmin)
