from django import forms
from django_select2.forms import ModelSelect2Widget
from desagotesavecinos.models import SolicitudDesagote
from catastro.models import ParcelaCatastral
from django.db.models.functions import Concat
from django.db.models import Value


class ParcelaWidget(ModelSelect2Widget):
    model = ParcelaCatastral
    search_fields = ['codigo__icontains']
    max_results = 10

    def filter_queryset(self, term, queryset=None, **dependent_fields):

        qs = ParcelaCatastral.objects.annotate(search_nomenclatura=Concat(
            'manzana__zona__distrito__codigo',
            Value('-'), 'manzana__zona__codigo',
            Value('-'), 'manzana__codigo',
            Value('-'), 'codigo'))

        qs = qs.filter(search_nomenclatura__icontains=term)

        return qs

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault(
            'data-placeholder',
            'Ingrese la nomenclatura catastral')
        self.attrs.setdefault('data-minimum-input-length', 3)
        self.attrs.setdefault('data-width', '25em')

        return super(ParcelaWidget, self).build_attrs(*args, **kwargs)


class SolicitudDesagoteFormSelect2(forms.ModelForm):
    class Meta:
        model = SolicitudDesagote
        fields = (
            'prioridad',
            'parcela',
            'direccion',
            'nombres',
            'apellidos',
            'estado',
            'cantidad_camaras_existentes',
            'cantidad_viviendas_en_terreno',
            'prestataria_designada')
        widgets = {'parcela': ParcelaWidget}


'''
# ejemplo de data_view valida en el ejemplo de select2
# https://github.com/applegrew/django-select2/blob/master/tests/testapp/views.py#L14
def heavy_parcela(request):
    term = request.GET.get("term", "")
    numbers = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five']
    numbers = filter(lambda num: term.lower() in num.lower(), numbers)
    results = [{'id': index, 'text': value} for (index, value) in enumerate(numbers)]
    return HttpResponse(json.dumps({'err': 'nil', 'results': results}), content_type='application/json')
'''
