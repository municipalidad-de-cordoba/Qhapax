from django.contrib.gis.db import models
import uuid
from django.contrib.auth.models import User
from enum import IntEnum
from django.utils import timezone


class Central(models.Model):
    ''' empresa proveedora o central de servicios de taxis o remises '''
    nombre = models.CharField(max_length=120)

    def __str__(self):
        return self.nombre


class SoftwareFlota(models.Model):
    ''' productos de software que se conectan a nuestra platforma '''
    nombre = models.CharField(max_length=120)
    user = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL)
    fecha_expiracion = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class HabilitacionCentralASoftware(models.Model):
    # los proveedores de software pueden repetirse para más de una central
    # en estos casos cada central le da su token a las empresas de software
    # como permisos para escribir sus viajes. Esto solo aplica a los móviles
    # que pertenecen a centrales

    central = models.ForeignKey(
        Central,
        on_delete=models.CASCADE,
        related_name='tokens_central')
    software = models.ForeignKey(
        SoftwareFlota,
        on_delete=models.CASCADE,
        related_name='tokens_software')
    token = models.UUIDField(
        default=uuid.uuid4,
        editable=False,
        help_text='Token de permiso para escribir a nombre de una central')
    fecha_expiracion = models.DateField(null=True, blank=True)

    def __str__(self):
        return '{} {}'.format(self.central, self.software)


class EstadoServicio(IntEnum):
    DESCONOCIDO = 0
    LIBRE_EN_SERVICIO = 50  # la unidad esta libre, disponible y esperando viaje
    FUERA_DE_SERVICIO = 75
    # En camino. La unidad está yendo a recoger al pasajero.
    VIAJE_RECOGIENDO = 100
    VIAJE_INICIADO = 200  # Iniciado. El móvil ya se encuentra con el pasajero
    VIAJE_TERMINADO = 300  # Terminado. La unidad terminó el viaje y está libre
    # Viaje cancelado (por falla mecánica del auto, anulación o inexistencia
    # del cliente
    VIAJE_CANCELADO = 400

    @classmethod
    def choices(cls):
        return [(tag.name, tag.value) for tag in cls]


class UltimaInfo(models.Model):
    ''' guardar ultima conexion y estado para multiples objetos '''
    ultimo_registro_estado = models.DateTimeField(
        null=True, blank=True, help_text='Ultimo registro recibido estado')
    ultimo_estado = models.IntegerField(
        choices=EstadoServicio.choices(),
        default=EstadoServicio.DESCONOCIDO)
    ultimo_registro_ubicacion = models.DateTimeField(
        null=True, blank=True, help_text='Ultimo registro recibido ubicacion')
    ultima_ubicacion = models.PointField(null=True, blank=True)

    def touch(self, estado=None, ubicacion=None):
        # las ubicaciones y los estados pueden llegar de formas distintas
        if estado and estado > EstadoServicio.DESCONOCIDO:
            self.ultimo_estado = estado
            self.ultimo_registro_estado = timezone.now()
        if ubicacion is not None:
            self.ultima_ubicacion = ubicacion
            self.ultimo_registro_ubicacion = timezone.now()

        self.save()

    class Meta:
        abstract = True


class Auto(UltimaInfo):
    ''' cada uno de los autos brindando servicio de transporte
        Si están '''
    dominio = models.CharField(max_length=20)

    def __str__(self):
        return self.dominio


class Chofer(UltimaInfo):
    ''' empresa proveedora de servicios '''
    dni = models.CharField(max_length=20)

    def __str__(self):
        return self.dni


class Servicio(UltimaInfo):
    ''' chofer + auto + central '''

    central = models.ForeignKey(
        Central,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='servicios_central')
    auto = models.ForeignKey(
        Auto,
        on_delete=models.CASCADE,
        related_name='viajes_auto')
    chofer = models.ForeignKey(
        Chofer,
        on_delete=models.CASCADE,
        related_name='viajes_chofer')
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} {} {}'.format(self.auto, self.chofer, self.central)


class EstadoServicio(UltimaInfo):
    ''' cada vez que un servicio cambia de estado '''

    servicio = models.ForeignKey(
        Servicio,
        on_delete=models.CASCADE,
        related_name='estados')
    estado = models.IntegerField(
        choices=EstadoServicio.choices(),
        default=EstadoServicio.DESCONOCIDO)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} {}'.format(self.servicio, self.estado)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.servicio.auto.touch(estado=self.estado)
        self.servicio.chofer.touch(estado=self.estado)


class Viaje(models.Model):
    ''' viaje formal con origen y destino '''
    servicio = models.ForeignKey(
        Servicio,
        on_delete=models.CASCADE,
        related_name='viajes_servicio')

    origen = models.PointField(null=True, blank=True)
    destino = models.PointField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} {} {}'.format(self.servicio, self.origen, self.destino)

    class Meta:
        # para que los usuarios de cada software de gestión de flota registre
        permissions = (('track_viaje', 'Puede registrar tracking de viaje'), )


class Tracking(models.Model):
    ''' cada uno de los registros por viaje o de geoposición '''
    estado_servicio = models.ForeignKey(
        EstadoServicio,
        on_delete=models.CASCADE,
        help_text='puede o no ser un viaje, es tambien un agrupador')

    ubicacion = models.PointField(null=True, blank=True)
    momento = models.DateTimeField(
        null=True,
        blank=True,
        help_text='timestamp del dato recibido')
    recibido = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} {} {}'.format(self.viaje, self.chofer, self.recibido)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.estado_servicio.servicio.auto.touch(ubicacion=self.ubicacion)
        self.estado_servicio.servicio.chofer.touch(ubicacion=self.ubicacion)
