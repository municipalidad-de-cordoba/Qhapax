from django.apps import AppConfig


class GeotaxisConfig(AppConfig):
    name = 'geotaxis'
