# Seguimiento de taxis y remises

Sistema interno que recibe registros de viajes y geoposición de las unidades de taxis y remises

Modulo inicial para recibir los datos masivamente.  
Los controles se hacen comparando estos datos con otros módulos en este u otros sistemas de validación.  

## Elementos

Existen productos de software usado por centrales de remises o taxis que se repiten. Este módulo los identifica y les da una autorización a cada uno.  
Además los automóviles individuales o las centrales tienen un token de autorización que entregan a los administradores del 
software para que registren en nombre de ellos. 
Todas estas autorizaciones pueden caducar cuando sea necesario.  



