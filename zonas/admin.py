from django.contrib import admin
from .models import Zonas, Categoria, Agrupador
from core.admin import QhapaxOSMGeoAdmin


class ZonasAdmin(QhapaxOSMGeoAdmin):
    list_display = ['id', 'categoria', 'nombre', 'descripcion', 'publicado']
    search_fields = ['nombre', 'descripcion']
    list_filter = ['categoria']


class CategoriaAdmin(admin.ModelAdmin):
    list_display = ['id', 'nombre']
    search_fields = ['nombre']
    list_filter = ['grupo']


class AgrupadorAdmin(admin.ModelAdmin):
    list_display = ['id', 'nombre']
    search_fields = ['nombre']


admin.site.register(Zonas, ZonasAdmin)
admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Agrupador, AgrupadorAdmin)
