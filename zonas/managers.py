from django.db import models


class ZonasManager(models.Manager):

    def coincidentes(self, punto):
        """
        Retorna las zonas a las que pertenece el punto dado
        """
        if punto is not None:
            qs = self.filter(poligono__contains=punto)
        else:
            qs = self.none()
        return qs
