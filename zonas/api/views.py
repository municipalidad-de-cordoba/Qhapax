from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import (ZonasGeoSerializer, CategoriaSerializer,
                          AgrupadorSerializer)
from api.pagination import DefaultPagination
from rest_framework_gis.filters import InBBoxFilter
from zonas.models import Zonas, Categoria, Agrupador


class AgrupadorViewSet(viewsets.ModelViewSet):
    """
    Agrupador de categorías de zonas
    """

    serializer_class = AgrupadorSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Agrupador.objects.all()
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CategoriaViewSet(viewsets.ModelViewSet):
    """
    Categorias de zonas.
    Se puede filtrar por nombre de categoria
    Ej: api/?categoria=wifi

    Se puede filtrar por id agrupador. Pueden ser varios id's
    separados por comas.
    Ej: ?ids_agrupadores=2,5
    """

    serializer_class = CategoriaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Categoria.objects.all().order_by('nombre')

        categorias = self.request.query_params.get('categoria', None)
        if categorias is not None:
            queryset = queryset.filter(nombre__icontains=categorias)

        ids_agrupadores = self.request.query_params.get(
            'ids_agrupadores', None)
        if ids_agrupadores is not None:
            ids_agrupadores = ids_agrupadores.split(',')
            queryset = queryset.filter(grupo__in=ids_agrupadores)
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ZonasViewSet(viewsets.ModelViewSet):
    """
    Zonas geoubicadas.
    Se puede filtrar por IDs de categorias (si es mas de una separadas por
    comas).
    Por ej: api/?categorias=1 o api/?categorias=1,3,5
    """

    serializer_class = ZonasGeoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    bbox_filter_field = 'poligono'
    filter_backends = (InBBoxFilter, )

    def get_queryset(self):
        queryset = Zonas.objects.filter(publicado=True).order_by('nombre')

        categorias = self.request.query_params.get('categorias', None)
        if categorias is not None:
            ids = categorias.split(',')
            queryset = queryset.filter(categoria__id__in=ids)
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
