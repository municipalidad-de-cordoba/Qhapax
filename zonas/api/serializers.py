from rest_framework import serializers
from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from zonas.models import Zonas, Categoria, Agrupador


class AgrupadorSerializer(CachedSerializerMixin):

    class Meta:
        model = Agrupador
        fields = ['id', 'nombre']


class CategoriaSerializer(CachedSerializerMixin):
    grupo = AgrupadorSerializer(read_only=True, many=True)

    class Meta:
        model = Categoria
        fields = ['id', 'nombre', 'grupo']


class ZonasGeoSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):

    class Meta:
        model = Zonas
        geo_field = 'poligono'
        auto_bbox = True
        fields = [
            'categoria',
            'nombre',
            'poligono',
            'descripcion',
            'publicado']


class ZonasSerializer(CachedSerializerMixin):
    """
    Zonas sin geolocalización
    """
    categoria = serializers.SerializerMethodField()
    id_categoria = serializers.IntegerField(source='categoria.id')

    def get_categoria(self, obj):
        return obj.categoria.nombre if obj is not None else 'Sin categoria'

    class Meta:
        model = Zonas
        fields = [
            'categoria',
            'id_categoria',
            'nombre',
            'descripcion',
            'publicado']


cache_registry.register(CategoriaSerializer)
cache_registry.register(ZonasGeoSerializer)
cache_registry.register(AgrupadorSerializer)
