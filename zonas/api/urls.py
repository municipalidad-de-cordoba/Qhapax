from django.conf.urls import url, include
from rest_framework import routers
from .views import ZonasViewSet, CategoriaViewSet, AgrupadorViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'zonas', ZonasViewSet, base_name='zonas.api.lista')
router.register(
    r'categorias',
    CategoriaViewSet,
    base_name='categorias.api.lista')
router.register(r'agrupador-categorias', AgrupadorViewSet,
                base_name='agrupador-categorias.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
