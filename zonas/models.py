from django.contrib.gis.db import models
from .managers import ZonasManager


class Agrupador(models.Model):
    """
    Agrupador de categorías.
    Por ejemplo, Distrito sería el agrupador para categorias
    Distrito Joven y Distrito Abasto.
    """
    nombre = models.CharField(max_length=45)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Agrupadores'


class Categoria(models.Model):
    """
    Categoría para zonas, a su vez pueden agruparse categorías en la clase
    Agrupador.
    """
    nombre = models.CharField(max_length=45, null=True, blank=True)
    grupo = models.ManyToManyField(Agrupador, blank=True)

    def __str__(self):
        return self.nombre


class Zonas(models.Model):
    categoria = models.ForeignKey(
        Categoria,
        on_delete=models.CASCADE,
        null=True,
        blank=True)
    nombre = models.CharField(max_length=90)
    poligono = models.PolygonField(null=True, blank=True)
    descripcion = models.TextField(null=True, blank=True)
    publicado = models.BooleanField(default=True)

    objects = ZonasManager()

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Zonas'
