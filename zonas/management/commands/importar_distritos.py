from django.core.management.base import BaseCommand
from django.db import transaction
import sys
from django.contrib.gis.geos import Polygon
from fastkml import kml
from portaldedatos.models import ArchivoKML
from zonas.models import Zonas, Categoria, Agrupador
from core.utils import normalizar


class Command(BaseCommand):
    help = """Comando para importar distritos de la ciudad al sistema desde un
                .kml"""

    def add_arguments(self, parser):
        parser.add_argument(
            '--kml_id',
            type=int,
            help='ID del KML cargado al sistema')
        parser.add_argument('--force', action='store_true', dest='force',
                            default=False, help="forzar a procesar el archivo")

    @transaction.atomic
    def handle(self, *args, **options):
        force = options['force']
        if force:
            self.stdout.write(self.style.WARNING(
                '--- Forzando importación de KML ---'))

        kml_id = options['kml_id']
        instancesKML = ArchivoKML.objects.filter(pk=kml_id)
        if len(instancesKML) == 0:
            self.stdout.write(
                self.style.ERROR(
                    'El KML id {} no existe'.format(kml_id)))
            sys.exit(1)

        instancesKML = instancesKML[0]
        if instancesKML.procesado and not force:
            self.stdout.write(
                self.style.ERROR(
                    'El KML id {} ya fue  procesado, use --force'.format(
                        kml_id)))
            sys.exit(1)

        self.stdout.write(
            self.style.SUCCESS(
                'Importando kml (id: {})'.format(
                    instancesKML.id)))

        try:
            with open(instancesKML.archivo_local.path,
                      'rt',
                      encoding="utf-8") as myfile:
                doc = myfile.read().encode('utf-8')

        except Exception as e:
            self.stdout.write(
                self.style.ERROR(
                    'Error al leer de {}'.format(
                        instancesKML.archivo_local.path)))
            sys.exit(1)

        # Create the KML object to store the parsed result
        k = kml.KML()

        # Read in the KML string
        k.from_string(doc)
        features = list(k.features())

        for feature in features:
            self.stdout.write("=============================")
            self.stdout.write("Doc: {}".format(feature.name))
            self.stdout.write("=============================")

            agrupador, created = Agrupador.objects.get_or_create(
                nombre=normalizar(feature.name))
            folders = list(feature.features())
            for folder in folders:
                self.stdout.write("=============================")
                self.stdout.write("Folder: {}".format(folder.name))
                self.stdout.write("=============================")

                categoria, created = Categoria.objects.get_or_create(
                    nombre=normalizar(folder.name))
                categoria.grupo.add(agrupador)
                categoria.save()
                places = list(folder.features())
                for place in places:
                    self.stdout.write("Place: {}".format(place.name))
                    zona, created = Zonas.objects.get_or_create(
                        nombre=normalizar(place.name))
                    zona.poligono = Polygon(
                        [xy[0:2] for xy in place.geometry.exterior.coords])
                    zona.categoria = categoria
                    zona.save()

        instancesKML.procesado = True
        instancesKML.save()
        self.stdout.write("FIN")
