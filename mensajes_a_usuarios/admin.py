from django.contrib import admin
from .models import (Mensaje, MensajeAGrupo, MensajeGrupalAUsuario,
                     MensajeDirecto)
from .forms import (MensajeDirectoForm, MensajeAGrupoForm,
                    MensajeGrupalAUsuarioForm, MensajeForm)


@admin.register(Mensaje)
class MensajeAdmin(admin.ModelAdmin):
    form = MensajeForm
    list_display = ['creador', 'software', 'titulo', 'created']

    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


@admin.register(MensajeAGrupo)
class MensajeAGrupoAdmin(admin.ModelAdmin):
    form = MensajeAGrupoForm
    list_display = ['emisor', 'mensaje', 'grupo', 'vencimiento']

    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


@admin.register(MensajeGrupalAUsuario)
class MensajeGrupalAUsuarioAdmin(admin.ModelAdmin):
    def titulo(self, obj):
        return obj.titulo_final()
    form = MensajeGrupalAUsuarioForm
    list_display = ['mensaje', 'usuario', 'estado', 'titulo']

    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


@admin.register(MensajeDirecto)
class MensajeDirectoAdmin(admin.ModelAdmin):
    form = MensajeDirectoForm
    list_display = [
        'id',
        'emisor',
        'usuario',
        'titulo',
        'estado',
        'content_type']

    list_filter = ['estado']

    fieldsets = (
        (None, {
            'fields': (
                'emisor',
                'usuario',
                'titulo',
                'mensaje_html',
                'estado',
            )
        }),
        ('App que usa este mensaje', {
            'classes': ('grp-collapse grp-open',),
            'fields': ('content_type', 'object_id', )
        }),
    )

    autocomplete_lookup_fields = {
        'generic': [['content_type', 'object_id']],
    }

    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )
