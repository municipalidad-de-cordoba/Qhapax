from django.apps import AppConfig


class MensajesAUsuariosConfig(AppConfig):
    name = 'mensajes_a_usuarios'
