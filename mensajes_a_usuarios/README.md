# Mensajes a usuarios

Mensajes de notificacion a usuarios. Puede ser mensajes generales a usaurios de diferentes  aplicaciones, a usuarios únicos o aquellos que cumplean algunas condiciones definidas.  

## Tests

Probar API de mensajes

```
curl -X GET \
    -v \
    -d '{"sistema": 1}' \
    -H "Authorization: Token e7319e5fecb2189c3ce56e92d5b8822777f6b944" \
    -H "Content-Type: application/json" \
    http://localhost:8000/api/v2/mensajes/mensajes/
```

Probar API de reacciones de mensajes

```
curl -X POST \
    -v \
    -d '{"tipo": "directo", "id": 3, "reaccion": 400}' \
    -H "Authorization: Token e7319e5fecb2189c3ce56e92d5b8822777f6b944" \
    -H "Content-Type: application/json" \
    http://localhost:8000/api/v2/mensajes/mensajes/reaccion_mensaje/
```

Crear mensaje directo de prueba

```
curl -X POST \
    -v \
    -d '{"titulo": "Mensaje de prueba", "sistema": 2, "mensaje": "Ola Ke Ase"}' \
    -H "Authorization: Token e7319e5fecb2189c3ce56e92d5b8822777f6b944" \
    -H "Content-Type: application/json" \
    http://localhost:8000/api/v2/mensajes/mensajes/crear_test_mensaje/
```

Crear mensaje grupal de prueba

```
curl -X POST \
    -v \
    -d '{"titulo": "Mensaje GRUPAL de prueba", "sistema": 2, "mensaje": "Hola Grupo de usuarios de X"}' \
    -H "Authorization: Token e7319e5fecb2189c3ce56e92d5b8822777f6b944" \
    -H "Content-Type: application/json" \
    http://localhost:8000/api/v2/mensajes/mensajes/crear_test_mensaje_grupal/
```

