from django.db import models
from softwaremunicipal.models import SoftwarePublico
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField
from django.template import Context, Template
from django.utils import timezone
import datetime
from enum import IntEnum
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class Mensaje(models.Model):
    ''' mensaje general '''
    creador = models.ForeignKey(User, null=True, blank=True)
    software = models.ForeignKey(
        'softwaremunicipal.SoftwarePublico',
        on_delete=models.CASCADE)

    # los mensajes tanto en el titulo como en los textos puede incluir
    # {{ variables }} que se procesarán antes de enviarse.
    # Como variables de contexto podran tener el sistema o el user al que se
    # quieren dirigir.
    titulo = models.CharField(max_length=90)
    mensaje_html = RichTextField(null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True)

    EST_EN_EDICION = 0
    EST_CREADO = 50
    EST_DESCARGADO = 100  # lo pide desde app o sistema
    EST_LEIDO = 200  # lo lee desde la app o sistema
    # interacciones (el mensaje tiene botones de interaccion genéricos)
    EST_MEGUSTA = 300
    EST_NOMEGUSTA = 400
    EST_ELIMINADO = 500

    estados = ((EST_EN_EDICION, 'En edición'),
               (EST_CREADO, 'Creado'),
               (EST_DESCARGADO, 'Ya descargado'),
               (EST_LEIDO, 'Ya leído'),
               (EST_MEGUSTA, 'Gusta'),
               (EST_NOMEGUSTA, 'No gusta'),
               (EST_ELIMINADO, 'Eliminado')
               )

    def __str__(self):
        return self.titulo


def manana():
    # fecha de vencimiento predeterminada para los mensajes a grupos
    return timezone.now() + datetime.timedelta(days=1)


class MensajeAGrupo(models.Model):
    '''
    Mensaje definido para generarse y enviarse a un grupo de usuarios de una
    app o sistema
    '''
    emisor = models.ForeignKey(User, null=True, blank=True)
    mensaje = models.ForeignKey(Mensaje, on_delete=models.CASCADE)
    grupo = models.ForeignKey(
        'softwaremunicipal.GrupoUsuariosSoftware',
        null=True,
        blank=True)
    vencimiento = models.DateTimeField(
        default=manana,
        help_text='Fecha máxima de validez del mensaje')

    def __str__(self):
        return self.mensaje.titulo


class EstadoMensaje(IntEnum):
    ########################################
    # esto se creo una vez y no me deja eliminarlo por migrates viejo, no se usa
    ########################################
    EST_CREADO = 0
    EST_DESCARGADO = 100  # lo pide desde app o sistema
    EST_LEIDO = 200  # lo lee desde la app o sistema
    # interacciones (el mensaje tiene botones de interaccion genéricos)
    EST_MEGUSTA = 300
    EST_NOMEGUSTA = 400
    EST_ELIMINADO = 500


class MensajeGrupalAUsuario(models.Model):
    '''
    Mensaje a usuario especifico de una app o sistema
    (se crean solo cuando el usuario entra a buscar sus mensajes)
    '''
    mensaje = models.ForeignKey(
        MensajeAGrupo,
        on_delete=models.CASCADE,
        related_name='mensajes_propios')
    usuario = models.ForeignKey(
        'softwaremunicipal.UsuarioSoftware',
        on_delete=models.CASCADE)

    estado = models.PositiveIntegerField(
        choices=Mensaje.estados,
        default=Mensaje.EST_CREADO)

    created = models.DateTimeField(auto_now_add=True)

    # procesar por template
    def titulo_final(self, context={}):
        template = Template(self.mensaje.mensaje.titulo)
        context.update({'user': self.usuario.user})
        ctx = Context(context)
        return template.render(ctx)

    def mensaje_html_final(self, context={}):
        template = Template(self.mensaje.mensaje.mensaje_html)
        context.update({'user': self.usuario.user})
        ctx = Context(context)
        return template.render(ctx)

    def __str__(self):
        return self.mensaje.mensaje.titulo


class MensajeDirecto(models.Model):
    ''' mensaje particular y directo a un usuario especifico '''
    emisor = models.ForeignKey(
        User,
        null=True,
        blank=True,
        help_text='Solo para cuando sea personalizado')
    usuario = models.ForeignKey(
        'softwaremunicipal.UsuarioSoftware',
        on_delete=models.CASCADE)

    titulo = models.CharField(max_length=90)
    mensaje_html = RichTextField(null=True, blank=True)

    estado = models.PositiveIntegerField(
        choices=Mensaje.estados,
        default=Mensaje.EST_EN_EDICION,
        help_text='Para mensajes a usuarios de GO, una vez editado marcar como creado')

    # relación opcional a otro modelo cualquiera que es "dueño" (app que usará
    # el mjs directo)
    content_type = models.ForeignKey(ContentType, blank=True,
                                     null=True,
                                     on_delete=models.SET_NULL)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.titulo
