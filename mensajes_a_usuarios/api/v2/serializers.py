from mensajes_a_usuarios.models import Mensaje
from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry


class MensajeSerializer(CachedSerializerMixin):

    class Meta:
        model = Mensaje
        fields = ['titulo']


# Registro los serializadores en la cache de DRF
cache_registry.register(MensajeSerializer)
