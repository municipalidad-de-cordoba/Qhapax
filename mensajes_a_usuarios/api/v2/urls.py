from django.conf.urls import url, include
from rest_framework import routers
from .views import MensajeViewSet

router = routers.DefaultRouter()
router.register(r'mensajes', MensajeViewSet, base_name='mensajes-usuario')

urlpatterns = [
    url(r'^', include(router.urls)),
]
