from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from api.pagination import DefaultPagination
from mensajes_a_usuarios.models import (Mensaje, MensajeDirecto,
                                        MensajeGrupalAUsuario, MensajeAGrupo)
from .serializers import MensajeSerializer
from softwaremunicipal.models import (SoftwarePublico, UsuarioSoftware,
                                      GrupoUsuariosSoftware,
                                      UsuarioEnGrupoSoftware)
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.utils import timezone
from rest_framework.decorators import action
import logging
logger = logging.getLogger(__name__)


class MensajeViewSet(viewsets.ModelViewSet):
    """ Log de algun usuario de alguna acción específica """
    serializer_class = MensajeSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = DefaultPagination

    def list(self, request, *args, **kwargs):
        # crear una lista de mensajes del usuario específico que hace esta
        # llamada desde la app especifica

        user = request.user
        data = request.data

        logger.info('Usuario entra a buscar mensajes {}'.format(user))

        if 'sistema' in data.keys():
            sistema_id = data['sistema']
        else:
            sistema_id = request.query_params.get('sistema', None)

        if sistema_id is None:
            res = {'ok': False, 'error': 'Sistema no informado', 'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        if isinstance(sistema_id, str):
            sistema_id = int(sistema_id)

        try:
            sistema = SoftwarePublico.objects.get(pk=sistema_id)
        except Exception as e:
            res = {
                'ok': False,
                'error': 'Sistema no válido: {}'.format(e),
                'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        # ver si el usuario es valido en la app
        try:
            usuario = UsuarioSoftware.objects.get(software=sistema, user=user)
        except Exception as e:
            logger.info('Falló Sistema {} + user {}'.format(sistema, user))
            res = {
                'ok': False,
                'error': 'Usuario en sistema no válido: {}'.format(e),
                'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        # ###############################################
        # # Toma los mensajes directos y los que fueron destinados a los grupos
        # # a los que ele usuario pertenece
        # ###############################################

        # ###############################################
        # mensajes directos (solo nuevos)
        # ###############################################

        respuesta = {'ok': True, 'total': 0, 'mensajes': []}

        mensajes_directos = MensajeDirecto.objects.filter(
            usuario=usuario, estado=Mensaje.EST_CREADO)
        logger.info(
            'Mensajes directos al usuario {}: {}'.format(
                user.username,
                mensajes_directos.count()))
        for mensaje in mensajes_directos:
            respuesta['total'] += 1
            msj = {
                'titulo': mensaje.titulo,
                'mensaje_html': mensaje.mensaje_html,
                'tipo': 'directo',
                'id': mensaje.id}
            respuesta['mensajes'].append(msj)
            mensaje.estado = Mensaje.EST_DESCARGADO
            mensaje.save()

        # ###############################################
        # mensajes a los grupos a los que el usuario pertenece
        # ###############################################

        # ir a ver que mensajes hay para sus grupos y saber si se bajo a
        # mensaje privado
        usuario_en_grupos = usuario.user.en_grupos_software.all()

        grupos_del_usuario = [g.grupo for g in usuario_en_grupos]

        # se filtran unicamente los mensajes grupales a los cuales el user
        # pertenezca
        mensajes_a_grupos = MensajeAGrupo.objects.filter(
            vencimiento__gt=timezone.now(), grupo__in=grupos_del_usuario)
        logger.info(
            'Mensajes no vencidos: {}'.format(
                mensajes_a_grupos.count()))

        for mensaje_a_grupo in mensajes_a_grupos:
            logger.info('Mensaje no vencido: {}. Usuario: {} Grupo: {}'.format(
                mensaje_a_grupo.mensaje.titulo, usuario, mensaje_a_grupo.grupo))

            # ver si el usuario pertenece al grupo (antes de ver si tiene su
            # mensaje propio creado)
            msj, created = MensajeGrupalAUsuario.objects.get_or_create(
                mensaje=mensaje_a_grupo, usuario=usuario)
            if created:
                msj.estado = Mensaje.EST_CREADO
                msj.save()
            logger.info('Mensaje {} CREADO: {}'.format(msj.mensaje, created))

        mensajes_grupales = MensajeGrupalAUsuario.objects.filter(
            usuario=usuario,
            estado=Mensaje.EST_CREADO,
            mensaje__vencimiento__gt=timezone.now())

        for mensaje in mensajes_grupales:
            respuesta['total'] += 1
            msj = {'titulo': mensaje.titulo_final(),
                   'mensaje_html': mensaje.mensaje_html_final(),
                   'tipo': 'grupal',
                   'id': mensaje.id}
            respuesta['mensajes'].append(msj)
            mensaje.estado = Mensaje.EST_DESCARGADO
            mensaje.save()

        return Response(respuesta, status=status.HTTP_200_OK)

    @action(
        methods=['post'],
        detail=False,
        permission_classes=[IsAuthenticated])
    def reaccion_mensaje(self, request):
        ''' calificación del mensaje que nos devuelve cada usuario '''

        data = request.data
        # preguntarle a google si el token esta OK
        # no se puede confiar en los otros campos, podrían ser fake
        tipo = data.get('tipo', None)
        if tipo is None:
            res = {
                'ok': False,
                'error': 'Tipo de mensaje no informado: use tipo = "directo" o "grupal"',
                'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        pk = data.get('id', None)
        if pk is None:
            res = {
                'ok': False,
                'error': 'ID de mensaje no informado',
                'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        if tipo == 'directo':
            mensajes = MensajeDirecto.objects.filter(pk=pk)
        elif tipo == 'grupal':
            mensajes = MensajeGrupalAUsuario.objects.filter(pk=pk)
        else:
            res = {
                'ok': False,
                'error': 'Tipo de mensaje MAL informado: use solo directo o grupal',
                'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        if mensajes.count() == 0:
            res = {
                'ok': False,
                'error': 'ID de mensaje inexistente: [{}]'.format(pk),
                'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        mensaje = mensajes[0]

        reacciones_validas = [
            Mensaje.EST_LEIDO,
            Mensaje.EST_MEGUSTA,
            Mensaje.EST_NOMEGUSTA,
            Mensaje.EST_ELIMINADO]
        reaccion = data.get('reaccion', None)
        if reaccion is None or reaccion not in reacciones_validas:
            reacciones_dict = dict((x, y) for x, y in Mensaje.estados)
            reacciones_validas_dict = {}
            for reaccion in reacciones_validas:
                reacciones_validas_dict[reaccion] = reacciones_dict[reaccion]

            res = {'ok': False, 'error': 'Reacción no válida. Use algunda de estas: [{}]'.format(
                reacciones_validas_dict), 'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        # revisar que el usuario logueado sea el verdadero dueño del mensaje
        if mensaje.usuario.user != request.user:
            logger.error(
                'INTENTO DE REACCION A UN MENSAJE DE OTRO USUARIO: {}. Para {}, responde {}'.format(
                    mensaje, mensaje.usuario.user, request.user))
            res = {'ok': False,
                   'error': 'El mensaje al que se pretende reaccionar no pertenece al usuario actual',
                   'data': data
                   # 'u1': mensaje.usuario.user.username,
                   # 'u2': request.user.username
                   }
            return Response(res, status=status.HTTP_400_BAD_REQUEST)
        mensaje.estado = reaccion
        mensaje.save()

        res = {'ok': True}
        return Response(res, status=status.HTTP_200_OK)

    @action(
        methods=['post'],
        detail=False,
        permission_classes=[IsAuthenticated])
    def crear_test_mensaje(self, request):
        ''' crear un mensaje de prueba para el usuario logueado '''

        data = request.data
        data.update(request.query_params)
        user = request.user

        sistema_id = data.get('sistema', None)
        if sistema_id is None:
            res = {'ok': False, 'error': 'Sistema no informado', 'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        try:
            sistema = SoftwarePublico.objects.get(pk=sistema_id)
        except Exception as e:
            res = {
                'ok': False,
                'error': 'Sistema no válido: {}'.format(e),
                'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        # ver si el usuario es valido en la app
        try:
            usuario_app = UsuarioSoftware.objects.get(
                software=sistema, user=user)
        except Exception as e:
            logger.info('Falló Sistema {} + user {}'.format(sistema, user))
            res = {
                'ok': False,
                'error': 'Usuario en sistema no válido: {}'.format(e),
                'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        titulo = data.get('titulo', None)
        mensaje = data.get('mensaje', None)
        if titulo is None or mensaje is None:
            res = {
                'ok': False,
                'error': 'No se específico el "titulo" o el "mensaje". Ambos son obligatorios',
                'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        mensaje = MensajeDirecto.objects.create(
            emisor=request.user,
            usuario=usuario_app,
            titulo=titulo,
            mensaje_html=mensaje,
            estado=Mensaje.EST_CREADO)

        res = {'ok': True, 'error': '', 'mensaje_id': mensaje.id}
        return Response(res, status=status.HTTP_200_OK)

    @action(
        methods=['post'],
        detail=False,
        permission_classes=[IsAuthenticated])
    def crear_test_mensaje_grupal(self, request):
        '''
        Crear un mensaje GRUPAL de prueba para el usuario logueado
        (si no hay grupo, lo crea)
        '''

        data = request.data
        data.update(request.query_params)
        user = request.user

        sistema_id = data.get('sistema', None)
        if sistema_id is None:
            res = {'ok': False, 'error': 'Sistema no informado', 'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        try:
            sistema = SoftwarePublico.objects.get(pk=sistema_id)
        except Exception as e:
            res = {
                'ok': False,
                'error': 'Sistema no válido: {}'.format(e),
                'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        # ver si el usuario es valido en la app
        try:
            usuario_app = UsuarioSoftware.objects.get(
                software=sistema, user=user)
        except Exception as e:
            logger.info('Falló Sistema {} + user {}'.format(sistema, user))
            res = {
                'ok': False,
                'error': 'Usuario en sistema no válido: {}'.format(e),
                'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        grupos_sistema = GrupoUsuariosSoftware.objects.filter(software=sistema)
        grupo_elegido = None
        for grupo in grupos_sistema:
            if grupo_elegido is not None:
                break
            for usuario_en_grupo in grupo.usuarios.all():
                if usuario_en_grupo.user == user:
                    grupo_elegido = grupo
                    break

        if grupo is None:
            grupo_elegido = GrupoUsuariosSoftware.objects.create(
                software=sistema, nombre="Grupo de prueba para {}".format(
                    user.username), cod_num_1=0)
            user_group = UsuarioEnGrupoSoftware.objects.create(
                grupo=grupo_elegido, user=user, ponderador=1)

        titulo = data.get('titulo', None)
        mensaje = data.get('mensaje', None)
        if titulo is None or mensaje is None:
            res = {
                'ok': False,
                'error': 'No se específico el "titulo" o el "mensaje". Ambos son obligatorios',
                'data': data}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

        mensaje = Mensaje.objects.create(creador=user, software=sistema,
                                         titulo=titulo, mensaje_html=mensaje)
        mensaje_a_grupo = MensajeAGrupo.objects.create(
            emisor=user, mensaje=mensaje, grupo=grupo_elegido)

        res = {'ok': True, 'error': '', 'mensaje_id': mensaje.id}
        return Response(res, status=status.HTTP_200_OK)
