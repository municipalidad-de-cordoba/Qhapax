from django_select2.forms import ModelSelect2Widget
from django import forms
from .models import (Mensaje, MensajeDirecto, MensajeAGrupo,
                     MensajeGrupalAUsuario)
from django.contrib.auth.models import User
from softwaremunicipal.models import UsuarioSoftware


class UsuarioSoftwareWidget(ModelSelect2Widget):
    model = UsuarioSoftware
    search_fields = ['user__username', 'user__email']
    max_results = 10

    def label_from_instance(self, obj):
        return '{}-{}'.format(obj.user.username, obj.user.email)

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault(
            'data-placeholder',
            'Ingrese nombre de usuario o email')
        self.attrs.setdefault('data-minimum-input-length', 1)
        self.attrs.setdefault('data-width', '25em')

        return super(UsuarioSoftwareWidget, self).build_attrs(*args, **kwargs)


class EmisorWidget(ModelSelect2Widget):
    model = User
    search_fields = ['username', 'email']
    max_results = 10

    def label_from_instance(self, obj):
        return '{}-{}'.format(obj.username, obj.email)

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault(
            'data-placeholder',
            'Ingrese nombre de usuario o email')
        self.attrs.setdefault('data-minimum-input-length', 1)
        self.attrs.setdefault('data-width', '25em')

        return super(EmisorWidget, self).build_attrs(*args, **kwargs)


class MensajeForm(forms.ModelForm):

    class Meta:
        model = Mensaje
        fields = '__all__'
        widgets = {
            'creador': EmisorWidget
        }


class MensajeDirectoForm(forms.ModelForm):

    class Meta:
        model = MensajeDirecto
        fields = '__all__'
        widgets = {
            'emisor': EmisorWidget,
            'usuario': UsuarioSoftwareWidget
        }


class MensajeAGrupoForm(forms.ModelForm):

    class Meta:
        model = MensajeAGrupo
        fields = '__all__'
        widgets = {
            'emisor': EmisorWidget
        }


class MensajeGrupalAUsuarioForm(forms.ModelForm):

    class Meta:
        model = MensajeGrupalAUsuario
        fields = '__all__'
        widgets = {
            'usuario': UsuarioSoftwareWidget
        }
