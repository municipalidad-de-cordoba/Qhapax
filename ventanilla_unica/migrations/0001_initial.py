# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-09-11 17:29
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('ordenanzatributaria', '0007_auto_20180302_1125'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoricalLocalVentanillaUnica',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('cuit', models.CharField(max_length=250)),
                ('nombre_completo', models.CharField(max_length=250)),
                ('nombre_fantasia', models.CharField(blank=True, max_length=250, null=True)),
                ('direccion_completa', models.CharField(max_length=250)),
                ('numero_habilitacion', models.CharField(max_length=250)),
                ('fecha_habilitacion_aprobada', models.DateField(blank=True, null=True)),
                ('fecha_vencimiento_habilitacion', models.DateField(blank=True, null=True)),
                ('fecha_inicio_tramite', models.DateField(blank=True, null=True)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Fecha de creación'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'get_latest_by': 'history_date',
                'verbose_name': 'historical local ventanilla unica',
                'ordering': ('-history_date', '-history_id'),
            },
        ),
        migrations.CreateModel(
            name='LocalVentanillaUnica',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cuit', models.CharField(max_length=250)),
                ('nombre_completo', models.CharField(max_length=250)),
                ('nombre_fantasia', models.CharField(blank=True, max_length=250, null=True)),
                ('direccion_completa', models.CharField(max_length=250)),
                ('numero_habilitacion', models.CharField(max_length=250)),
                ('fecha_habilitacion_aprobada', models.DateField(blank=True, null=True)),
                ('fecha_vencimiento_habilitacion', models.DateField(blank=True, null=True)),
                ('fecha_inicio_tramite', models.DateField(blank=True, null=True)),
                ('codigo_actividad_ota', models.ManyToManyField(to='ordenanzatributaria.ActividadOTA')),
            ],
        ),
    ]
