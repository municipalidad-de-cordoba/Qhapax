from django.apps import AppConfig


class VentanillaUnicaConfig(AppConfig):
    name = 'ventanilla_unica'
