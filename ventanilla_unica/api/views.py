from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import LocalVentanillaUnicaSerializer
from api.pagination import DefaultPagination
from ventanilla_unica.models import LocalVentanillaUnica


class LocalVentanillaUnicaViewSet(viewsets.ModelViewSet):
    """
    Locales habilitados segun ventanilla unica.
    -Con el parametro nombre buscamos el nombre del responsable del local (A
     veces es el de fantasia).
    Ej: API/?nombre=juan
    -Con el parametro nombre_fantasia buscamos por el nombre del local.
    Ej: API/?nombre_fantasia=Grido helados
    -Con el parametro direccion completa buscamos la direccion del local
    Ej: API/?direccion_completa=bonaire 1831, Cordoba, Cordoba
    -Con el parametro codigo_ota buscamos los locales con ese codigo asociado
    Ej: API/?codigo_ota=523110.2
    -Con el parametro codigo_categoria se puede filtrar por el codigo de
     categoria.
    Ej: API/?codigo_categoria=1
    -Con el parámetro rama_id se puede filtrar por ramas a las que pertenecen
     los locales.
    Ej: API/?rama_id=20
    """
    serializer_class = LocalVentanillaUnicaSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = LocalVentanillaUnica.objects.filter(publicado=True)

        nombre = self.request.query_params.get('nombre', None)
        if nombre is not None:
            queryset = queryset.filter(nombre_completo__icontains=nombre)

        nombre_fantasia = self.request.query_params.get(
            'nombre_fantasia', None)
        if nombre_fantasia is not None:
            queryset = queryset.filter(nombre_fantasia=nombre_fantasia)

        direccion_completa = self.request.query_params.get(
            'direccion_completa', None)
        if direccion_completa is not None:
            queryset = queryset.filter(direccion_completa=direccion_completa)

        codigo_ota = self.request.query_params.get('codigo_ota', None)
        if codigo_ota is not None:
            queryset = queryset.filter(codigo_actividad_ota__codigo=codigo_ota)

        codigo_categoria = self.request.query_params.get(
            'codigo_categoria', None)
        if codigo_categoria is not None:
            queryset = queryset.filter(
                codigo_actividad_ota__categoria__id=codigo_categoria)

        rama_id = self.request.query_params.get('rama_id', None)
        if rama_id is not None:
            queryset = queryset.filter(
                codigo_actividad_ota__categoria__rama__id=rama_id
                )

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
