from django.conf.urls import url, include
from rest_framework import routers
from .views import LocalVentanillaUnicaViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'locales-habilitados',
    LocalVentanillaUnicaViewSet,
    base_name='locales-habilitados.api.lista')


urlpatterns = [
    url(r'^', include(router.urls)),
]
