from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from ventanilla_unica.models import LocalVentanillaUnica
from rest_framework_gis.serializers import GeoFeatureModelSerializer


class LocalVentanillaUnicaSerializer(GeoFeatureModelSerializer, serializers.ModelSerializer):
    codigo_actividad_ota = serializers.SerializerMethodField()
    # codigo_categoria = serializers.SerializerMethodField()
    # nombre_categoria = serializers.SerializerMethodField()

    def get_codigo_actividad_ota(self, obj):
        return obj.codigo_actividad_ota.values('codigo')

    # def get_codigo_categoria(self, obj):
    #     return obj.codigo_actividad_ota.categoria.id

    # def get_nombre_categoria(self, obj):
    #     return obj.codigo_actividad_ota.categoria.nombre

    class Meta:
        model = LocalVentanillaUnica
        geo_field = 'ubicacion'
        fields = ('nombre_completo', 'nombre_fantasia', 'direccion_completa',
                  'codigo_actividad_ota', 'latitud', 'longitud')


# Registro los serializadores en la cache de DRF
cache_registry.register(LocalVentanillaUnicaSerializer)
