from django.db import models
from django.contrib.gis.db import models as geo_model
from simple_history.models import HistoricalRecords
from ordenanzatributaria.models import ActividadOTA


class LocalVentanillaUnica(models.Model):
    ''' Local habilitado segun base de ventanilla unica'''

    # Datos del responsable del local
    cuit = models.CharField(max_length=250)
    nombre_completo = models.CharField(max_length=250)
    # Datos del local
    nombre_fantasia = models.CharField(max_length=250, null=True, blank=True)
    direccion_completa = models.CharField(max_length=250)
    numero_habilitacion = models.CharField(max_length=250)
    fecha_habilitacion_aprobada = models.DateField(null=True, blank=True)
    fecha_vencimiento_habilitacion = models.DateField(null=True, blank=True)
    fecha_inicio_tramite = models.DateField(null=True, blank=True)
    # Los locales estan clasificados segun los codigos de actividades de la OTA
    # Un local puede tener varias actividades
    codigo_actividad_ota = models.ManyToManyField(ActividadOTA,
                                                  related_name='locales'
                                                 )

    latitud = models.DecimalField(max_digits=11, decimal_places=8, default=0.0)
    longitud = models.DecimalField(
        max_digits=11, decimal_places=8, default=0.0)
    ubicacion = geo_model.PointField(null=True, blank=True)
    publicado = models.BooleanField(default=True)

    history = HistoricalRecords()

    def __str__(self):
        return self.nombre_completo
