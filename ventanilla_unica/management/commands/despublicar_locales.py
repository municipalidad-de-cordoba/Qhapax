from django.core.management.base import BaseCommand
from django.contrib.gis.geos import Point
from ventanilla_unica.models import LocalVentanillaUnica
from geo.models import Departamento


class Command(BaseCommand):
    help = """
            Comando para despublicar los locales de ventanilla unica
            que están mal georefereciados.
           """

    def handle(self, *args, **options):
        locales = LocalVentanillaUnica.objects.all()
        dpto_capital = Departamento.objects.get(nombre='CAPITAL')
        despublicados = 0

        for local in locales:
            #pasamos las coordenadas a punto geo
            self.stdout.write(self.style.SUCCESS('Georeferenciando local {}'.format(local)))
            point = Point(float(local.longitud), float(local.latitud))
            local.ubicacion = point
            local.save()

            # Si la ubicación está fuera de Córdoba, lo despublicamos
            if not(dpto_capital.poligono.contains(local.ubicacion)):
                local.publicado = False
                local.save()
                despublicados += 1

        self.stdout.write("Total de locales despublicados: {}".format(despublicados))
