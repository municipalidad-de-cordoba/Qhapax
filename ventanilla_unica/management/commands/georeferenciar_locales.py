from geopy.geocoders import Nominatim
from geopy.extra.rate_limiter import RateLimiter
from django.core.management.base import BaseCommand
from ventanilla_unica.models import LocalVentanillaUnica


class Command(BaseCommand):
    help = """Comando para georeferenciar los locales de ventanilla unica"""

    def handle(self, *args, **options):

        # los que no están publicados es porque fueron mal georeferenciados
        locales = LocalVentanillaUnica.objects.filter(publicado=False)
        geolocator = Nominatim()
        res_geocode = RateLimiter(geolocator.geocode, min_delay_seconds=5)
        count = 0
        no_geo = 0

        for local in locales:
            if (local.latitud == 0.0) or (local.longitud == 0.0):
                try:
                    result = res_geocode(local.direccion_completa, "ES", 5)
                    local.latitud = result[1][0]
                    local.longitud = result[1][1]
                    local.save()
                    count += 1
                    print("Grabado:", local.nombre_completo)
                except Exception as e:
                    print("El error fue:", e)
                    print("Local con error:", local.nombre_completo)
                    no_geo += 1
                    pass

        self.stdout.write("Total de locales georeferenciados:", count)
        self.stdout.write("Total de locales NO georeferenciados:", no_geo)
