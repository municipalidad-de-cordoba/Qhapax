import geocoder
from django.core.management.base import BaseCommand
from ventanilla_unica.models import LocalVentanillaUnica
from qhapax.settings import GOOGLE_API_KEY


class Command(BaseCommand):
    help = """Comando para georeferenciar los locales de ventanilla unica con
              el api de Google
           """

    def handle(self, *args, **options):

        # los que no están publicados es porque fueron mal georeferenciados
        locales = LocalVentanillaUnica.objects.filter(publicado=False)
        count = 0
        no_geo = 0
        errores = []

        for local in locales:
            if (local.latitud == 0.0) or (local.longitud == 0.0):
                try:
                    geodir = geocoder.google('{}, Córdoba, Córdoba, Argentina'.format(local.direccion_completa),
                        key=GOOGLE_API_KEY
                        )
                    local.latitud = geodir.latlng[0]
                    local.longitud = geodir.latlng[1]
                    local.publicado = True
                    local.save()
                    count += 1
                    self.stdout.write(self.style.SUCCESS("Grabado: {}".format(local.nombre_completo)))
                except Exception as e:
                    self.stdout.write(self.style.ERROR("El error fue: {}".format(e)))
                    error = "Local con error: {}".format(local.nombre_completo)
                    self.stdout.write(self.style.ERROR(error))
                    errores.append(error)
                    no_geo += 1
                    pass

        if len(errores) > 0:
            for error in errores:
                self.stdout.write(self.style.ERROR(error))
        self.stdout.write("Total de locales georeferenciados: {}".format(count))
        self.stdout.write("Total de locales NO georeferenciados: {}".format(no_geo))
