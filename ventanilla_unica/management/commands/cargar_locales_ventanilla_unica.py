#!/usr/bin/python3

from django.core.management.base import BaseCommand
from django.db import transaction
from ventanilla_unica.models import LocalVentanillaUnica
from ordenanzatributaria.models import ActividadOTA
import sys
import csv
import datetime


# Nota: algunos de estos locales pueden estar en orgizacion ya cargados.
# Vamos a seperar las cosas
class Command(BaseCommand):
    help = """
              Comando para cargar los locales desde un csv de ventanilla
              unica
           """

    def add_arguments(self, parser):
        parser.add_argument(
            '--path',
            type=str,
            help='Path del archivo CSV local',
            default='ventanilla_unica/resources/habilitados_2011-2018.csv')

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('--- Comenzando carga ---'))
        fieldnames = [
            'NroTramite',
            'Cuit',
            'Nombre',
            'NombreFantasia',
            'Calle',
            'Nro',
            'Piso',
            'Dpto',
            'NroHabilitacion',
            'Estado',
            'FechaHabAprobada',
            'añoHabilitacion',
            'FechaInicioTramite',
            'FechaVencimientoHab',
            'Actividades',
            'CodigosRubro']
        count = 0
        codigos_malos = 0
        path = options['path']
        with open(path, encoding="ISO-8859-3") as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=';', quotechar='"')

            header = reader.__next__()
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR(
                    'BAD FIELDNAMES [{}] -> [{}]'.
                    format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            for row in reader:
                count += 1
                print(row)

                cuit = row['Cuit'].replace(' ', '').strip()
                nombre = row['Nombre'].lower().capitalize().strip()
                nombre_fantasia = row['NombreFantasia'].lower(
                ).capitalize().strip()
                calle = row['Calle'].lower().capitalize().strip()
                numero_calle = row['Nro'].strip().strip()
                num_hab = row['NroHabilitacion'].lower().capitalize().strip()
                # Tomamos solo la fecha y no la hora
                fecha_hab = row['FechaHabAprobada'].split()[0]
                fecha_inic = row['FechaInicioTramite'].split()[0]
                try:
                    # Es el unico que puede llegar a venir vacio
                    fecha_venc = row['FechaVencimientoHab'].split()[0]
                    fecha_venc = datetime.datetime.strptime(
                        fecha_venc, '%d/%m/%Y')
                except IndexError:
                    # Si fecha_venc es vacio entonces lo seteamos con string
                    # vacio.
                    if not fecha_venc:
                        fecha_venc = ""

                # Pueden ser varios rubros
                codigos_rubro = row['CodigosRubro'].split(',')

                direccion_completa = calle + ' ' + numero_calle + (', Cordoba,'
                                                                   ' Cordoba')
                fecha_hab = datetime.datetime.strptime(fecha_hab, '%d/%m/%Y')
                fecha_inic = datetime.datetime.strptime(fecha_inic, '%d/%m/%Y')

                # Vamos a tomar los objetos segun los codigos que tengamos
                lista_actividades = []
                for codigo in codigos_rubro:
                    try:
                        actividad = ActividadOTA.objects.filter(codigo=codigo)[
                            0]
                        lista_actividades.append(actividad)
                    except IndexError:
                        # Le agregamos el '.' al codigo entre el ultimo y
                        # anteultimo char
                        # Ej: 12345 -> 1234.5
                        tmp_1 = codigo[-1]
                        codigo_tmp1 = codigo[:-1]
                        codigo_tmp1 = codigo_tmp1 + '.'
                        codigo_tmp1 = codigo_tmp1 + tmp_1
                        try:
                            actividad = ActividadOTA.objects.filter(
                                codigo=codigo_tmp1)[0]
                            lista_actividades.append(actividad)
                        except IndexError:
                            # Le agregamos el '.' al codigo entre el anteultimo
                            # char y su antecesor
                            # Ej: 123456 -> 1234.56
                            tmp_2 = codigo[-2:]
                            codigo_tmp2 = codigo[:-2]
                            codigo_tmp2 = codigo_tmp2 + '.'
                            codigo_tmp2 = codigo_tmp2 + tmp_2
                            try:
                                actividad = ActividadOTA.objects.filter(
                                    codigo=codigo_tmp2)[0]
                                lista_actividades.append(actividad)
                            except IndexError as e:
                                print(e)
                                print("El codigo fue: ", codigo)
                                codigos_malos += 1

                local, created = LocalVentanillaUnica.objects.get_or_create(
                    cuit=cuit,
                    nombre_completo=nombre,
                    nombre_fantasia=nombre_fantasia,
                    direccion_completa=direccion_completa,
                    numero_habilitacion=num_hab,
                    fecha_habilitacion_aprobada=fecha_hab,
                    fecha_vencimiento_habilitacion=fecha_venc,
                    fecha_inicio_tramite=fecha_inic
                )
                local.save()
                local.codigo_actividad_ota.set(lista_actividades)
                local.save()

        self.stdout.write("Total de locales grabados: {}".format(count))
        self.stdout.write("Total de codigos malos: {}".format(codigos_malos))
