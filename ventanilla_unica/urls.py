from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^locales-habilitados.(?P<filetype>csv|xls)$',
        views.lista_locales_habilitados,
        name='ventanilla-unica.locales-habilitados'),
    ]
