from django.contrib import admin
from .models import LocalVentanillaUnica
from core.admin import QhapaxOSMGeoAdmin


class LocalVentanillaUnicaAdmin(QhapaxOSMGeoAdmin):
    list_display = (
        'cuit',
        'nombre_completo',
        'nombre_fantasia',
        'fecha_vencimiento_habilitacion',
        'latitud',
        'longitud',
        'publicado')
    search_fields = ['cuit', 'nombre_completo', 'nombre_fantasia',
                     'direccion_completa']
    list_filter = ['publicado']


admin.site.register(LocalVentanillaUnica, LocalVentanillaUnicaAdmin)
