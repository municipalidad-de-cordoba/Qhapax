from django.views.decorators.cache import cache_page
from .models import LocalVentanillaUnica
import django_excel as excel


@cache_page(60 * 60 * 12)  # 12 h
def lista_locales_habilitados(request, filetype):
    """
    lista de locales habilitados a través de ventanilla única
    """
    locales = LocalVentanillaUnica.objects.all().prefetch_related('codigo_actividad_ota')

    csv_list = []
    csv_list.append(['nombre completo',
                     'nombre fantasía',
                     'cuit',
                     'dirección completa',
                     'numero habilitación',
                     'fecha de habilitación aprobada',
                     'fecha vencimiento de habilitación',
                     'ubicacion',
                     'actividad'])

    for local in locales:

        actividades = ''
        for actividad in local.codigo_actividad_ota.all():
            actividades += '{} - '.format(actividad.nombre)

        csv_list.append([local.nombre_completo,
                         local.nombre_fantasia,
                         local.cuit,
                         local.direccion_completa,
                         local.numero_habilitacion,
                         local.fecha_habilitacion_aprobada.strftime('%d/%m/%Y'),
                         local.fecha_vencimiento_habilitacion.strftime('%d/%m/%Y'),
                         local.ubicacion.ewkt,
                         actividades])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
