from django.contrib import admin
from .models import MotivoAudiencia, SolicitudAudiencia
from django.core.urlresolvers import reverse
from simple_history.admin import SimpleHistoryAdmin
from import_export.admin import ImportExportModelAdmin
from import_export import resources, fields


class SolicitudAudienciaAdminResource(resources.ModelResource):
    '''
    recurso a exportarse cuando se descarga
    '''
    dni = fields.Field()
    tipo_organizacion = fields.Field()
    funcionario = fields.Field()
    cargo = fields.Field()
    ver_funcionario = fields.Field()

    def dehydrate_dni(self, obj):
        tipoid = '' if obj.tipo_id is None else obj.tipo_id.nombre
        return '{} {}'.format(tipoid, obj.unique_id)

    def dehydrate_tipo_organizacion(self, obj):
        return obj.get_tipo_org_general_display()

    def dehydrate_estado(self, obj):
        return obj.get_estado_display()

    def dehydrate_funcionario(self, obj):
        return '{} {}'.format(obj.funcion.funcionario.nombre,
                              obj.funcion.funcionario.apellido)

    def dehydrate_cargo(self, obj):
        return obj.funcion.cargo_gen

    def dehydrate_ver_funcionario(self, obj):
        try:
            return obj.funcion.get_absolute_url()
        except Exception as e:
            return 'Funcionario sin función activa'

    class Meta:
        model = SolicitudAudiencia
        fields = ('id', 'estado', 'tema_general__nombre', 'nombre_persona',
                  'tipo_organizacion', 'nombre_org', 'dni', 'email',
                  'telefono', 'titulo', 'descripcion', 'respuesta',
                  'observaciones_internas', 'motivo__nombre',
                  'ultima_modificacion', 'funcionario', 'cargo',
                  'ver_funcionario',
                  )
        export_order = ('id', 'ultima_modificacion', 'tema_general__nombre',
                        'titulo', 'estado', 'motivo__nombre', 'descripcion',
                        'funcionario', 'cargo', 'ver_funcionario',
                        'nombre_persona', 'dni', 'nombre_org',
                        'tipo_organizacion', 'email', 'telefono', 'respuesta',
                        'observaciones_internas')


class SolicitudAudienciaAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    resource_class = SolicitudAudienciaAdminResource

    def comunicacion_funcionario(self, obj):
        com_url = reverse(
            'portaldedatos.comunicacion_funcionario', kwargs={
                'persona_id': obj.funcion.funcionario.id})
        return '<a target="_blank" href="{}">Ver vías comunicacion funcionario</a>'.format(
            com_url)
    comunicacion_funcionario.allow_tags = True
    comunicacion_funcionario.short_description = 'Ver funcionario'

    def notificar_solicitud(self, obj):
        # texto de la solicitud
        url = reverse('audienciafuncionarios.notificar_funcionario', kwargs={
                      'solicitud_id': obj.id, 'solicitud_uuid': obj.uuid})
        return '<a target="_blank" href="{}">Ver texto notificación funcionario</a>'.format(
            url)
    notificar_solicitud.allow_tags = True
    notificar_solicitud.short_description = 'Notificar'

    list_display = (
        'id',
        'tema_general',
        'titulo',
        'creado',
        'estado',
        'motivo',
        'funcion',
        'notificar_solicitud',
        'comunicacion_funcionario',
        'nombre_persona',
        'observaciones_internas',
        'tipo_org_general',
        'nombre_org')
    search_fields = [
        'tema_general__nombre',
        'titulo',
        'descripcion',
        'observaciones_internas',
        'respuesta',
        'funcion__funcionario__nombre',
        'funcion__funcionario__apellido',
        'funcion__cargo__nombre',
        'nombre_persona',
        'nombre_org']
    list_filter = ['tema_general', 'tipo_org_general', 'estado', 'motivo']
    exclude = ['tipo_org', 'tema']


admin.site.register(MotivoAudiencia)
admin.site.register(SolicitudAudiencia, SolicitudAudienciaAdmin)
