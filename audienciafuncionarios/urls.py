from django.conf.urls import url
from . import views


urlpatterns = [
    # url(r'^$', views.index, name='index'),
    url(
        r'^solicitud/notificar-audiencia-(?P<solicitud_id>[0-9]+)-(?P<solicitud_uuid>[\w-]+)$',
        views.notificar_funcionario,
        name='audienciafuncionarios.notificar_funcionario',
    ),
    url(r'^solicitud/(?P<persona>[\w-]+)/(?P<persona_id>[0-9]+)$',
        views.solicitud, name='audienciafuncionarios.solicitud',),

]
