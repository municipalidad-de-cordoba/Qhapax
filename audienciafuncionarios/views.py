from core.models import ComunicacionPersona
from audienciafuncionarios.models import SolicitudAudiencia
from django.shortcuts import render
from django.conf import settings
from django.views.decorators.cache import cache_page
from django.shortcuts import get_object_or_404
from django.http import Http404
from core.models import Persona
from django.http import JsonResponse
from core.models import TipoOrganizacion
from django.contrib.auth.decorators import login_required
from . import forms
import logging
logger = logging.getLogger(__name__)


@cache_page(60 * 60)  # 1 h
def solicitud(request, persona, persona_id):
    '''
    mostrar el formulario de acceso a la audiencia
    Asegurarse que sea un cargo que permite solicitud
    '''
    persona = get_object_or_404(Persona, pk=persona_id, slug=persona)
    if not persona.en_funciones:
        raise Http404("No hay cargo")

    funcion = persona.en_funciones

    form = forms.SolicitudAudenciaForm

    context = {
        'titulo': 'Formulario de solicitud de audiencia',
        'funcion': funcion,
        'form': form(
            initial={
                'tipo_id': settings.TIPOID_PK_PRINCIPAL,
                'funcion': funcion,
                'tipo_org_general': TipoOrganizacion.PERSONA_FISICA})}

    if request.method == "POST":
        context['form'] = form(request.POST)
        if context['form'].is_valid():
            context['form'].save()
            return JsonResponse({'ok': True})
        else:
            return JsonResponse(
                {'ok': False, 'errors': context['form'].errors.as_json()})

    url = "website/{}/solicitud-audiencia.html".format(
        request.website.template)
    return render(request, url, context)


@login_required()
def notificar_funcionario(request, solicitud_id, solicitud_uuid):
    """ automatizar el texto para notificar al funcionario que le pidieron una audiencia """
    # TODO enviar el texto por email automaticamente a la persona
    solicitud = SolicitudAudiencia.objects.get(
        pk=solicitud_id, uuid=solicitud_uuid)
    funcion = solicitud.funcion
    persona = funcion.funcionario
    comunicaciones_persona = ComunicacionPersona.objects.filter(objeto=persona)
    area = funcion.cargo.area_gobierno.nombre
    comunicaciones_cargo = funcion.cargo.comunicacioncargo_set.filter()

    context = {
        'persona': persona,
        'funcion': funcion,
        'area': area,
        'comunicaciones_cargo': comunicaciones_cargo,
        'solicitud': solicitud,
        'comunicaciones_persona': comunicaciones_persona}
    url = 'audienciafuncionarios/notificar-audiencia.html'.format(
        request.website.template)
    return render(request, url, context)
