# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-02-16 15:03
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('audienciafuncionarios', '0017_remove_solicitudaudiencia_tipo_org'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='motivoaudiencia',
            name='gobierno',
        ),
        migrations.RemoveField(
            model_name='temaaudiencia',
            name='gobierno',
        ),
    ]
