# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-08-07 15:34
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('audienciafuncionarios', '0007_auto_20160807_1231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solicitudaudiencia',
            name='tema',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='gobiernos.TemaGeneral'),
        ),
    ]
