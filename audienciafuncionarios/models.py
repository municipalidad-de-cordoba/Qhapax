"""
Los ciudadanos pueden enviar solicitudes de acceso a la información.
La oficina encargada derivará y comunicará respuestas
"""
from django.db import models
from django.utils.text import slugify
import uuid
from core.models import TipoId, TipoOrganizacion
from funcionarios.models import Funcion
from gobiernos.models import TemaGeneral


# TODO remove, no se usa
class TemaAudiencia(models.Model):
    """
    Temas sobre los cuales alguien quiere una audiencia.
    """
    nombre = models.CharField(max_length=250)
    slug = models.SlugField(max_length=140, null=True, blank=True)

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        super(TemaAudiencia, self).save(*args, **kwargs)

    class Meta:
        unique_together = (("nombre", "slug"),)


class MotivoAudiencia(models.Model):
    """
    Motivo por el cual se solicita la información
    """
    nombre = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, null=True, blank=True)

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        super(MotivoAudiencia, self).save(*args, **kwargs)

    class Meta:
        unique_together = (("nombre", "slug"),)
        ordering = ['nombre']


class SolicitudAudiencia(models.Model):
    """
    Cada una de las solicitudes de audiencia de los ciudadanos y su estado actual
    """
    tema = models.ForeignKey(
        TemaAudiencia,
        on_delete=models.SET_NULL,
        null=True,
        blank=True)  # vincula además la solicitud con el gobierno
    tema_general = models.ForeignKey(
        TemaGeneral,
        on_delete=models.SET_NULL,
        null=True,
        blank=True)  # vincula además la solicitud con el gobierno
    motivo = models.ForeignKey(
        MotivoAudiencia,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name='Motivo de la audiencia',
        help_text="Obligatorio. El motivo de la solicitud ayudará a priorizarla")
    # funcionario y cargo al que se le solicita
    funcion = models.ForeignKey(Funcion, on_delete=models.SET_NULL, null=True)
    # persona que lo solicita
    nombre_persona = models.CharField(
        max_length=180,
        verbose_name='Nombre completo del solicitante',
        help_text="Obligatorio.")
    tipo_org_general = models.PositiveIntegerField(
        choices=TipoOrganizacion.tipos,
        blank=True,
        null=True,
        verbose_name='Tipo de solicitante',
        help_text="Persona física es predeterminado")
    nombre_org = models.CharField(max_length=180, null=True, blank=True,
                                  verbose_name='Nombre de la organización',
                                  help_text="Sólo si corresponde")
    # si pertenece a ONG, empresa, etc
    # tipo y nro de identificacion
    tipo_id = models.ForeignKey(TipoId, on_delete=models.SET_NULL, null=True,
                                verbose_name='Tipo documento')

    unique_id = models.CharField(max_length=90, null=True, blank=True,
                                 verbose_name='Nro de documento')

    email = models.EmailField()  # obligatorio, para validar que es real
    telefono = models.CharField(max_length=90, null=True, blank=True)

    VIA_WEB = 10
    VIA_MESA_DE_ENTRADAS = 20
    canales_de_envio = ((VIA_WEB, 'Vía Web'),
                        (VIA_MESA_DE_ENTRADAS, 'Vía mesa de entradas'))
    canal_de_envio = models.PositiveIntegerField(
        choices=canales_de_envio, default=VIA_WEB, null=True)

    titulo = models.CharField(max_length=130,
                              help_text="Sobre la solicitud")

    slug = models.SlugField(max_length=140, null=True, blank=True)
    descripcion = models.TextField(
        verbose_name='Detalles de la solicitud',
        help_text="No obligatorio. Ayudará a priorizar esta solicitud")

    NUEVO = 10
    VALIDO = 20  # vio el email y le dio al link de control
    EN_ESTUDIO = 30  # puede ser válido
    DESCARTADO = 40  # se descarto la idea (enviar motivos al ciudadano)
    ACEPTADO = 50  # Se va a realizar
    RESPONDIDO = 60  # Se llevo a cabo
    DERIVADO = 70  # se derivo al funcionario, el responderá
    HECHA = 80  # la audiencia se realizó

    estados = ((NUEVO, '10 Nuevo'), (VALIDO, '20 Valido'),
               (EN_ESTUDIO, '30 En estudio'),
               (DESCARTADO, '40 Descartado'), (ACEPTADO, '50 Aceptado'),
               (RESPONDIDO, '60 Respondido al solicitante'),
               (DERIVADO, '70 Derivado al funcionario'),
               (HECHA, '80 Audiencia realizada'))

    estado = models.PositiveIntegerField(choices=estados, default=NUEVO)
    # por ahora la respuesta se garda como texto, luego podría estar vinculada
    # a algún modelo de la clase datos
    respuesta = models.TextField(null=True, blank=True)
    observaciones_internas = models.TextField(null=True, blank=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    def save(self, *args, **kwargs):
        # si es un nuevo objeto DEBO mandarle un email de notificación (DESPUES
        # de grabarlo)
        nuevo = self.pk is None
        self.slug = slugify(self.titulo)
        super(SolicitudAudiencia, self).save(*args, **kwargs)

        if nuevo:
            from django.template.loader import render_to_string
            html_mensaje = render_to_string(
                'core/mails/nueva_solicitud_audiencia.html',
                {
                    'usuario': self.nombre_persona,
                    'titulo': self.titulo,
                    'pk': self.pk,
                    'descripcion': self.descripcion})
            from core.lib.comm import QhapaxMail
            qm = QhapaxMail()
            subject = "Solicitud de audiencia Municipalidad de Córdoba [AUD-{}]".format(
                self.pk)
            qm.send_mail(subject, html_mensaje, to=self.email)

    def __str__(self):
        return self.titulo

    class Meta:
        unique_together = (("tema", "slug"),)
