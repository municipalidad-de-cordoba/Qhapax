from django.forms import ModelForm
from .models import SolicitudAudiencia
from django import forms
from captcha.fields import ReCaptchaField


class SolicitudAudenciaForm(ModelForm):
    captcha = ReCaptchaField(attrs={'lang': 'es'})

    class Meta:
        model = SolicitudAudiencia
        exclude = ['canal_de_envio', 'slug', 'estado', 'respuesta',
                   'creado', 'ultima_modificacion', 'html', 'tema',
                   'tipo_org', 'observaciones_internas']
        widgets = {'funcion': forms.HiddenInput()}
