"""
Si el plan de metas esta en uso por aqui se mostrarán las vistas
correspondientes
"""
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import *
from plandemetas.models import *
from .settings import *
from django.views.decorators.cache import cache_page


@cache_page(60 * 5)  # 5 m
def ver(
        request,
        plan,
        linea=None,
        comp_id=None,
        comp_real_id=None,
        obj_id=None,
        meta_id=None,
        act_id=None):
    '''
    Cargar la HomePage del plan
    '''
    return redirect('https://gobiernoabierto.cordoba.gob.ar/data/datos-abiertos/categoria/plan-de-metas/informe-de-plan-de-metas/49')


    colores = ['#DD9E00', '#6ECDC6', '#C65133', '#80AE52']
    plan = get_object_or_404(PlanDeMetas, slug=plan)
    if not plan.publicado:
        return HttpResponse(
            'El plan de metas no esta publicado en este momento',
            status=404)

    # los lineamientos son las secciones principales
    lineamientos = Lineamiento.objects.filter(plan_de_metas=plan)

    if linea:  # slug del lineamiento
        lineamiento = get_object_or_404(
            Lineamiento, plan_de_metas=plan, slug=linea)
        if not lineamiento.publicado:
            return HttpResponse('No publicado en este momento', status=404)
    else:
        context = {
            'plan': plan,
            'lineamientos': lineamientos,
            'colores': colores}
        url = 'website/{}/plandemetas.html'.format(request.website.template)
        return render(request, url, context)

    # los lineamientos son las secciones principales
    componentes = Componente.objects.filter(
        lineamiento__plan_de_metas=plan,
        lineamiento=lineamiento)

    if comp_id:  # id del componente
        componente = get_object_or_404(Componente,
                                       lineamiento__plan_de_metas=plan,
                                       lineamiento=lineamiento,
                                       pk=comp_id)
        if not componente.publicado:
            return HttpResponse('No publicado en este momento', status=404)
    else:
        context = {
            'plan': plan,
            'lineamientos': lineamientos,
            'lineamiento': lineamiento,
            'componentes': componentes,
            'colores': colores,
            'titulo': 'Plan de metas',
            'subtitulo': plan.titulo}
        url = 'website/{}/plan_lineamiento.html'.format(
            request.website.template)
        return render(request, url, context)

    # los lineamientos son las secciones principales
    objetivos = Objetivo.objects.filter(
        componente__lineamiento__plan_de_metas=plan,
        componente__lineamiento=lineamiento,
        componente=componente).order_by('orden_prioridad')

    if obj_id:  # id del componente
        objetivo = get_object_or_404(
            Objetivo,
            componente__lineamiento__plan_de_metas=plan,
            componente__lineamiento=lineamiento,
            componente=componente,
            pk=obj_id)
        if not objetivo.publicado:
            return HttpResponse('No publicado en este momento', status=404)
    else:
        context = {
            'plan': plan,
            'lineamientos': lineamientos,
            'lineamiento': lineamiento,
            'componentes': componentes,
            'componente': componente,
            'objetivos': objetivos,
            'colores': colores,
            'titulo': 'Plan de metas',
            'subtitulo': plan.titulo}
        url = 'website/{}/plan_componente.html'.format(
            request.website.template)
        return render(request, url, context)

    # las metas del componente
    metas = Meta.objects.filter(
        objetivo__componente__lineamiento__plan_de_metas=plan,
        objetivo__componente__lineamiento=lineamiento,
        objetivo__componente=componente,
        objetivo=objetivo)

    if meta_id:  # id de la meta
        meta = Meta.objects.get(
            objetivo__componente__lineamiento__plan_de_metas=plan,
            objetivo__componente__lineamiento=lineamiento,
            objetivo__componente=componente,
            objetivo=objetivo,
            pk=meta_id)
        if not meta.publicado:
            return HttpResponse('No publicado en este momento', status=404)
        # indicadores de la meta
        indicadores = IndicadorMeta.objects.filter(meta=meta, publicado=True)
        avance = dict(Meta.estados)[meta.estado_de_avance]

    else:
        context = {
            'plan': plan,
            'lineamientos': lineamientos,
            'lineamiento': lineamiento,
            'componentes': componentes,
            'componente': componente,
            'colores': colores,
            'objetivos': objetivos,
            'objetivo': objetivo,
            'metas': metas,
            'titulo': 'Plan de metas',
            'subtitulo': plan.titulo}
        url = 'website/{}/plan_objetivo.html'.format(request.website.template)
        return render(request, url, context)

    context = {
        'plan': plan,
        'lineamientos': lineamientos,
        'lineamiento': lineamiento,
        'componentes': componentes,
        'componente': componente,
        'colores': colores,
        'objetivos': objetivos,
        'objetivo': objetivo,
        'indicadores': indicadores,
        'metas': metas,
        'meta': meta,
        'titulo': 'Plan de metas',
        'subtitulo': plan.titulo,
        'avance': avance}

    url = 'website/{}/plan_meta.html'.format(request.website.template)

    return render(request, url, context)
