# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-05-11 14:47
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0013_auto_20170511_1123'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticiashome',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime(2017, 5, 11, 11, 47, 44, 12418)),
        ),
    ]
