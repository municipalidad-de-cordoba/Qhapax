# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-08-03 18:09
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0015_auto_20170511_1412'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticiashome',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
    ]
