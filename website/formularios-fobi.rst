Formularios rápidos al público
==============================

Para muchos casos es necesario disponer de formularios al público.
Un ejemplo son las actividades públicas que requieren un sistema de
inscripción rápida.

Estramos finalmente usado `django-fobi https://github.com/barseghyanartur/django-fobi>`_ en reemplazo de una app propia.
