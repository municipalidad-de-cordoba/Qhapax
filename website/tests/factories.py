from factory.django import DjangoModelFactory


class WebsiteFactory(DjangoModelFactory):
    class Meta:
        model = 'website.Website'
        django_get_or_create = ('dominio',)
    dominio = 'testserver'

