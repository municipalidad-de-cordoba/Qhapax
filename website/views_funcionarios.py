"""
Vistas al publico de los datos de funcionarios
"""
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.cache import cache_page
from django.core.urlresolvers import reverse
from django.http import Http404
from core.models import Moneda
from django.conf import settings
from .models import *
from core.models import Persona, ComunicacionPersona
from funcionarios.models import WebFuncionarios, Funcion, Sueldo, SueldoReal
from declaracionesjuradas.models import DeclaracionJurada
from plandemetas.models import *
from .settings import *
from django.shortcuts import get_object_or_404
import json


@cache_page(60 * 5)  # 5 m
def home(request, organigrama):
    """
    Cargar la información inicial de los funcionarios
    """

    return redirect('https://www.cordoba.gob.ar/areas-de-gobierno/#areas_gobierno') 

    wf = WebFuncionarios.objects.get(slug=organigrama)
    # oficinas que dependen del cargo a partir del cual se grafica
    dependencias = wf.cargo.dependencias(
        hasta_categoria=wf.hasta_categoria, recursive=False)

    context = {'web_funcionarios': wf, 'dependencias': dependencias}
    # url = 'website/{}/funcionarios.html'.format(request.website.template)
    url = 'website/{}/en_construccion.html'.format(request.website.template)
    return render(request, url, context)

@cache_page(60 * 5)  # 5 m
def home_prueba(request, organigrama):
    """
    Cargar la información inicial de los funcionarios
    """
    wf = WebFuncionarios.objects.get(slug=organigrama)
    # oficinas que dependen del cargo a partir del cual se grafica
    dependencias = wf.cargo.dependencias(
        hasta_categoria=wf.hasta_categoria, recursive=False)

    context = {'web_funcionarios': wf, 'dependencias': dependencias}
    url = 'website/{}/funcionarios.html'.format(request.website.template)
    return render(request, url, context)

@cache_page(60 * 5)  # 5 m
def oficina_view(request, oficina, oficina_id):
    '''
    Cargar los datos y las dependencias de una oficina en particular
    '''
    try:
        funcion = Funcion.objects.get(
            cargo_id=oficina_id,
            cargo__slug=oficina,
            activo=True)
    except Funcion.DoesNotExist:
        # si no hay un funcionario designado tratar de mostrar los que dependen
        # de el (si existieran)
        cargo = get_object_or_404(Cargo, pk=oficina_id)
        dependencias = cargo.dependencias(recursive=False)
        if dependencias is not None:
            return redirect(
                oficina_arbol,
                oficina=oficina,
                oficina_id=oficina_id)
        else:
            raise Http404("Oficina/Cargo inactivo/inexistente")

    return redirect('https://www.cordoba.gob.ar/areas-de-gobierno/#areas_gobierno') 
    # sueldo por su categoria
    sueldos = Sueldo.objects.filter(
        cargo_categoria=funcion.cargo.categoria,
        activo=True).order_by('-fecha_inicio')
    sueldo = None if len(sueldos) < 1 else sueldos[0]
    persona = funcion.funcionario

    url_persona = request.build_absolute_uri(persona.get_absolute_url())
    qr_image = persona.make_qr(url_persona)

    # agregar declaración Jurada si la hubiera
    ddjj = DeclaracionJurada.objects.filter(persona=persona, publicada=True)

    sueldos = Sueldo.objects.filter(
        cargo_categoria=funcion.cargo.categoria,
        activo=True).order_by('-fecha_inicio')
    sueldo_categoria = None if len(sueldos) < 1 else sueldos[0]
    sueldos_reales = SueldoReal.objects.filter(
        funcion=funcion).order_by('-mes')

    comunicaciones_cargo = funcion.cargo.comunicacioncargo_set.filter(
        privado=False)
    comunicaciones_persona = ComunicacionPersona.objects.filter(
        privado=False, objeto=persona)

    # oficinas que dependen del cargo a partir de esta
    dependencias = funcion.cargo.dependencias(recursive=False)

    # los decretos deberían ser en PDF pero a veces suben jpg. Avisarle al html
    if funcion.decreto_pdf is None or funcion.decreto_pdf == "":
        extension_decreto = ''
    else:
        extension_decreto = funcion.decreto_pdf.url.split(".")[-1].lower()

    mi_moneda = Moneda.objects.get(pk=settings.MONEDA_PK_PRINCIPAL)
    context = {
        'cargo': funcion.cargo,
        'dependencias': dependencias,
        'persona': persona,
        'funcion': funcion,
        'sueldo': sueldo,
        'ddjj': ddjj,
        'qr_image': qr_image,
        'sueldo_categoria': sueldo_categoria,
        'sueldos_reales': sueldos_reales,
        'comunicaciones_persona': comunicaciones_persona,
        'comunicaciones_cargo': comunicaciones_cargo,
        'mi_moneda': mi_moneda,
        'extension_decreto': extension_decreto}
    url = 'website/{}/reparticion.html'.format(request.website.template)
    # url = 'website/{}/en_construccion.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 5)  # 5 m
def oficina_arbol(request, oficina, oficina_id):
    '''
    Arbol de funcionarios de una secretaría
    '''
    cargo = Cargo.objects.get(
        slug=oficina,
        # para que no busquen auto x id y para resolver oficinas que tienen el
        # mismo nombre
        pk=oficina_id)
    # oficinas que dependen del cargo a partir de esta
    dependencias = cargo.dependencias(recursive=False)

    # Si la oficina elegida no tiene dependencias que vaya directo a su ficha,
    # no sierve un mapa vacio
    if dependencias is None:
        return redirect(oficina_view, oficina=oficina, oficina_id=oficina_id)

    context = {'cargo': cargo, 'dependencias': dependencias}
    # url = 'website/{}/reparticion_arbol.html'.format(request.website.template)
    url = 'website/{}/en_construccion.html'.format(request.website.template)
    return render(request, url, context)


def persona(request, persona, persona_id):
    '''
        función pivot, dirige persona a la oficina que dirige
    '''

    try:
        obj = Funcion.objects.get(
            funcionario_id=persona_id,
            funcionario__slug=persona,
            activo=True)
    except Funcion.DoesNotExist:
        raise Http404("Funcionario inexistente")
    except Funcion.MultipleObjectsReturned:
        raise Http404("Funcionario no actualizado")

    return redirect(obj)


@cache_page(60 * 5)  # 5 m
def qr(request, persona_id, persona):
    '''
    JSON: devuelve un base 64 de la imagen qr de la persona
    '''

    context = {"status": 404}
    try:
        persona = Persona.objects.get(pk=persona_id, slug=persona)

        host = "http://"
        if request.is_secure():
            host = "https://"
        host += request.get_host()

        context['url'] = "%s%s" % (host,
                                   reverse(
                                       'website.funcionarios.persona',
                                       kwargs={
                                           'persona': persona.slug,
                                           'persona_id': persona.id}))

        context['image'] = persona.make_qr(context['url'])
        context['status'] = 200

    except Persona.DoesNotExist:
        pass

    return HttpResponse(json.dumps(context), content_type="application/json")
