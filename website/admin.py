from django.contrib import admin
from .models import *


class WebsiteAdmin(admin.ModelAdmin):
    list_display = ('dominio', 'template', 'titulo')


class CategoriaNoticiaAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'id')


class NoticiasHomeAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'website', 'fecha', 'imagen', 'mas_info_url')


class SeccionAdmin(admin.ModelAdmin):
    list_display = (
        'titulo_corto',
        'orden_prioridad',
        'publicada',
        'activada',
        'subseccion_de',
        'website',
        'url_especial')
    prepopulated_fields = {"titulo_slug_web": ("titulo_texto",)}
    list_filter = ['subseccion_de', 'publicada']


admin.site.register(Website, WebsiteAdmin)
admin.site.register(CategoriaNoticia, CategoriaNoticiaAdmin)
admin.site.register(NoticiasHome, NoticiasHomeAdmin)
admin.site.register(Seccion, SeccionAdmin)
admin.site.register(CategoriaSeccion)
