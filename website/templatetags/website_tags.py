from django import template
register = template.Library()


@register.simple_tag(takes_context=True)
def add_ints(context, orig, add):
    context[orig] = int(context[orig]) + int(add)
    return context[orig]


@register.filter
def get_ext(value):
    if not value or len(value.split('.')) == 1:
        return ''

    ret = value.split('.')[-1]
    return ret


@register.filter
def mod(value, modvalue):
    if not value:
        return ''

    ret = value % modvalue
    return ret
