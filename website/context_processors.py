from django.core.urlresolvers import reverse
from funcionarios.models import WebFuncionarios
from gobiernos.models import Preferencias
from plandemetas.models import PlanDeMetas
from funcionarios.models import WebFuncionarios
from gobiernos.models import Preferencias
from portaldedatos.models import Portal, CategoriaPortalDatos, DatoPortal
from website.models import Seccion


# @cache_page(60 * 60 * 12)  # 12 horas
def website(request):
    """
    contexto habitual de los sitios webs
    """
    website = request.website
    extends_from = 'website/{}/base.html'.format(website.template)
    base_template = 'website/{}'.format(website.template)

    # ademas de las secciones principales revisar las aplicaciones principales
    # enviar todas en un formato unificado para la home
    secciones = []

    # funcionarios
    wfs = WebFuncionarios.objects.filter(publicado=True)
    for wf in wfs:
        secciones.append({'fa_icon': 'fa-group',
                          'url': reverse('website.funcionarios.home',
                                         kwargs={'organigrama': wf.slug}),
                          'titulo': wf.titulo,
                          'descripcion': '',
                          'activada': True,
                          'imagen': '/media/{}'.format(wf.imagen)})

    # planes de metas
    plan_de_metas = PlanDeMetas.objects.filter(publicado=True)
    for plan in plan_de_metas:
        secciones.append({'fa_icon': 'fa-gears',
                          'url': reverse('website.plandemetas',
                                         kwargs={'plan': plan.slug}),
                          'titulo': plan.titulo,
                          'descripcion': '',
                          'activada': True,
                          'imagen': '/media/{}'.format(plan.imagen)})

    # portales de datos
    portales_de_datos = Portal.objects.filter(publicado=True)
    for pdd in portales_de_datos:
        secciones.append({'fa_icon': 'fa-gears',
                          'url': reverse('website.portaldatos.home',
                                         kwargs={'portal': pdd.slug}),
                          'titulo': pdd.titulo,
                          'descripcion': '',
                          'activada': True,
                          'imagen': '/media/{}'.format(pdd.imagen)})

    secciones_principales = Seccion.objects.filter(
        website=request.website, subseccion_de__isnull=True, publicada=True)
    for s in secciones_principales:
        secciones.append({'fa_icon': s.fa_icon,
                          'url': s.get_url(),
                          'titulo': s.titulo_corto,
                          'activada': s.activada,
                          'descripcion': s.descripcion_breve,
                          'imagen': '/media/{}'.format(s.imagen)
                          })

    preferencias = Preferencias.objects.as_dict()
    datos_importantes_portal = DatoPortal.objects.filter(
        estado=DatoPortal.PUBLICADO).order_by('-importancia')[:10]

    context = {'website': website,
               'titulo': website.titulo,
               'subtitulo': website.subtitulo,
               'extends_from': extends_from,
               'base_template': base_template,
               'secciones_principales': secciones,
               'aplicaciones': {},
               'preferencias': preferencias,
               'datos_importantes': datos_importantes_portal}

    # hay apps internas que pueden hacer las veces de secciones principales.
    # si hay, agregarlas de alguna forma como principales.
    # plan de metas
    plan_de_metas = PlanDeMetas.objects.filter(publicado=True)
    if plan_de_metas:
        context['aplicaciones']['plandemetas'] = plan_de_metas

    # funcionarios
    funcionarios = WebFuncionarios.objects.filter(publicado=True)
    if funcionarios:
        context['aplicaciones']['funcionarios'] = funcionarios

    # portal de datos
    portaldedatos = Portal.objects.filter(publicado=True)
    if portaldedatos:
        context['aplicaciones']['portaldedatos'] = portaldedatos

    return context
