'''
Vistas al publico del portal de datos generales
'''
from django.shortcuts import render, redirect
from django.views.decorators.cache import cache_page
from portaldedatos.models import *
from .models import *
from .settings import *
from web_analytics.models import ResultadosQuery, QueryGoogleAnalytics


@cache_page(60 * 5)  # 5 m
def home(request, portal):
    '''
    Cargar la información inicial de los funcionarios
    '''
    portalobj = Portal.objects.get(slug=portal, publicado=True)
    categorias_principales = CategoriaPortalDatos.objects.filter(
        depende_de=None).order_by('-importancia')
    total_recursos = Recurso.objects.filter(
        version_dato__dato__estado=DatoPortal.PUBLICADO).count()

    categoriaNoticiaDatos = CategoriaNoticia.objects.all().filter(
        nombre__icontains='datos')
    noticiasDatos = NoticiasHome.objects.all().filter(
        categoria__in=categoriaNoticiaDatos).order_by('-fecha')[:5]

    context = {
        'portal': portalobj,
        'categorias': categorias_principales,
        'total_recursos': total_recursos,
        'series_datos': DatoPortal.objects.filter(
            estado=DatoPortal.PUBLICADO).count(),
        'cantidad_categorias_con_datos': len(
            CategoriaPortalDatos.objects.tienen_datos()),
        'datos_mas_visitados': DatoPortal.objects.mas_visitados(
            cantidad=4),
        'datos_ultimos': DatoPortal.objects.ultimos_subidos(
            cantidad=4),
        'recursos_ultimos': Recurso.objects.ultimos_subidos(
            cantidad=4),
        'noticiasDatos': noticiasDatos,
    }
    url = 'website/{}/datos-portal.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 5)  # 5 m
def categoria(request, portal, categoria):
    '''
    Cargar la información inicial de los funcionarios
    '''
    categoriaobj = CategoriaPortalDatos.objects.get(slug=categoria,
                                                    portal__slug=portal)
    categorias_dependientes = CategoriaPortalDatos.objects.filter(
        depende_de=categoriaobj).order_by('-importancia')
    datos = DatoPortal.objects.filter(
        categoria=categoriaobj,
        estado=DatoPortal.PUBLICADO)
    deps = categoriaobj.dependencias(recursive=True)
    cantidad_recursos = len(categoriaobj.recursos())
    if deps is not None:
        for c in deps:
            if c.recursos():
                cantidad_recursos += len(c.recursos())

    context = {'categoria': categoriaobj, 'datos': datos,
               'sub_categorias': categorias_dependientes,
               'cantidad_recursos': cantidad_recursos}
    url = 'website/{}/datos-categoria.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 5)  # 5 m
def dato(request, portal, categoria, dato, dato_id):
    """
    Cargar la información inicial de los funcionarios
    """

    # necesitamos que si cambia de categoria o de nombre el dato la URL vieja
    # no de error 404. Es por esto que redirigimos si es necesario a la nueva
    # URL correcta
    portalobj = Portal.objects.get(slug=portal, publicado=True)
    datoobj = DatoPortal.objects.get(pk=dato_id,
                                     estado=DatoPortal.PUBLICADO,
                                     categoria__portal__publicado=True)

    bad_url = False
    if datoobj.slug != dato:
        bad_url = True

    if datoobj.categoria.slug != categoria:
        bad_url = True

    if datoobj.categoria.portal.slug != portal:
        bad_url = True

    if bad_url:
        return redirect(datoobj)  # tomara el get_absolute_url del objeto

    versiones = VersionDato.objects.filter(dato=datoobj).order_by('-fecha')
    context = {'portal': portalobj, 'dato': datoobj, 'versiones': versiones}
    url = 'website/{}/datos-dato.html'.format(request.website.template)

    return render(request, url, context)
