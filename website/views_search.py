from datetime import date
from haystack.generic_views import SearchView


class QhapaxSearchView(SearchView):
    """ vista de busquedas personalizadas """

    def get(self, request, *args, **kwargs):
        self.template_name = 'website/{}/search.html'.format(
            request.website.template)
        return super().get(request, *args, **kwargs)
