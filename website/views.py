from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from .models import Seccion, NoticiasHome
from portaldedatos.models import (Recurso, DatoPortal, CategoriaPortalDatos,
                                  Portal)
from softwaremunicipal.models import AppMovil, RepositorioSoftware
from propuestaciudadana.models import Propuesta
from .settings import *
from django.views.decorators.cache import cache_page
import logging
logger = logging.getLogger(__name__)


@cache_page(60 * 5)  # 5 min
def index(request):
    '''
    Cargar la HomePage
    '''
    noticiasHome = NoticiasHome.objects.all().order_by('-fecha')[:5]
    total_recursos = Recurso.objects.filter(
        version_dato__dato__estado=DatoPortal.PUBLICADO).count()
    apps_totales = AppMovil.objects.filter(
        publicado=True, activado=True).count()
    ideas_totales = Propuesta.objects.count()
    portales_de_datos = Portal.objects.filter(publicado=True)
    context = {
        'noticiasHome': noticiasHome,
        'total_recursos': total_recursos,
        'apps_totales': apps_totales,
        'ideas_totales': ideas_totales,
        'datos_mas_visitados': DatoPortal.objects.mas_visitados(
            cantidad=4),
        'datos_ultimos': DatoPortal.objects.ultimos_subidos(
            cantidad=4),
        'series_datos': DatoPortal.objects.filter(
            estado=DatoPortal.PUBLICADO).count(),
        'cantidad_categorias_con_datos': len(
            CategoriaPortalDatos.objects.tienen_datos()),
        'portales': portales_de_datos}

    url = 'website/{}/index.html'.format(request.website.template)

    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def seccion(request, seccion):
    """
    Cargar una seccion
    """
    seccion = Seccion.objects.get(
        titulo_slug_web=seccion,
        website=request.website,
        publicada=True)

    # ver si requiere redirección a URL especial
    if seccion.url_especial is not None and seccion.url_especial != '':
        return redirect(reverse(seccion.url_especial))

    # secciones del sitio web para el menu. Solo las principales (no son
    # subsecciones de otras)
    secciones = Seccion.objects.filter(
        subseccion_de=seccion,
        publicada=True).order_by('orden_prioridad')
    context = {'seccion': seccion, 'secciones': secciones}
    url = 'website/{}/seccion.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def nuestra_ciudad(request):
    ''' Cargar seccion especial "Nuestra ciudad" '''
    # FIXME esto no va aquí. Las páginas personalizadas debería o bien ser
    # secciones o ser cvonfigurables fuera del código
    context = {}
    url = 'website/{}/nuestra-ciudad.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def institucional(request):
    ''' Cargar seccion especial "Institucional" '''
    # FIXME esto no va aquí. Las páginas personalizadas debería o bien ser
    # secciones o ser cvonfigurables fuera del código
    context = {}
    url = 'website/{}/institucional.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def software_libre(request):
    ''' Cargar seccion especial "Software Libre" '''
    # FIXME esto no va aquí. Las páginas personalizadas debería o bien ser
    # secciones o ser cvonfigurables fuera del código
    repositorios = RepositorioSoftware.objects.filter(es_software_libre=True)
    repos = repositorios.values('software__titulo', 'url', 'tipo')
    tipos = dict(RepositorioSoftware.tipos)
    cantidad_apps_movil = AppMovil.objects.filter(
        activado=True, publicado=True).count()
    context = {'repos': repos, 'tipos': tipos,
               'cantidad_apps_movil': cantidad_apps_movil}
    url = 'website/{}/software-libre.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def avance_de_obras(request):
    ''' Cargar seccion especial "Avance de obras" '''
    context = {}
    url = 'website/{}/avance-de-obras.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def estado_de_obra(request):
    ''' Cargar seccion especial "Estado de obra" '''
    context = {}
    url = 'website/{}/estado-de-obra.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def habilitaciones_especial(request):
    ''' Cargar seccion especial "Habilitaciones" '''
    context = {}
    url = 'website/{}/habilitaciones.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def accesibilidad(request):
    ''' Cargar seccion especial "Accesibilidad" '''
    context = {}
    url = 'website/{}/accesibilidad.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def boletines(request):
    ''' Cargar seccion especial "Boletines" '''
    # FIXME esto no va aquí. Las páginas personalizadas debería o bien ser
    # secciones o ser cvonfigurables fuera del código
    context = {}
    url = 'website/{}/boletines.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def arbolado(request):
    ''' Cargar seccion especial "Arbolado" '''
    # FIXME esto no va aquí. Las páginas personalizadas debería o bien ser
    # secciones o ser cvonfigurables fuera del código
    context = {}
    url = 'website/{}/arbolado.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def obras_terceros(request):
    ''' Cargar seccion especial "Obras de Terceros" '''
    # FIXME esto no va aquí. Las páginas personalizadas debería o bien ser
    # secciones o ser cvonfigurables fuera del código
    context = {}
    url = 'website/{}/obras-terceros.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def estado_de_obra_terceros(request):
    ''' Cargar seccion especial "Estado de obra de Terceros" '''
    context = {}
    url = 'website/{}/estado-de-obra-terceros.html'.format(
        request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def go_index(request):
    ''' Cargar seccion especial "app GO" '''
    context = {}
    url = 'website/{}/GO.html'.format(request.website.template)
    return render(request, url, context)

@cache_page(60 * 60)  # 1 h
def radar_ciudadano(request):
    ''' Cargar seccion de Radar Ciudadano '''
    context = {}
    url = 'website/{}/radar-ciudadano.html'.format(request.website.template)
    return render(request, url, context)

@cache_page(60 * 60)  # 1 h
def locales_habilitados(request):
    ''' Cargar seccion de locales habilitados '''
    context = {}
    url = 'website/{}/locales-habilitados.html'.format(request.website.template)
    return render(request, url, context)

@cache_page(60 * 60)  # 1 h
def apis(request):
    ''' Cargar seccion de apis '''
    context = {}
    url = 'website/{}/apis.html'.format(request.website.template)
    return render(request, url, context)

@cache_page(60 * 60)  # 1 h
def codigos(request):
    ''' Cargar seccion de codigos '''
    context = {}
    url = 'website/{}/codigos.html'.format(request.website.template)
    return render(request, url, context)