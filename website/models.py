from django.core.urlresolvers import reverse
from django.db import models
from ckeditor.fields import RichTextField
from django.utils.text import slugify
from versatileimagefield.fields import VersatileImageField
from django.utils import timezone


class Website(models.Model):
    """
    Sitio web de transparencia o datos del gobierno.
    Este debe incuir secciones con textos editables, un blog, la lista de
    datos, servicios, apps y otras herramientas que el municipio ofrece en
    materia de transparencia.
    """
    protocolo = models.CharField(max_length=6, default='http')
    dominio = models.CharField(max_length=150)  # sin protocolo ni slashes
    template = models.CharField(max_length=45, default='html5up-alpha')
    titulo = models.CharField(max_length=45, default='Portal de Gobierno')
    subtitulo = models.CharField(
        max_length=95,
        default='Municipalidad de Córdoba')
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}://{}'.format(self.protocolo, self.dominio)


class CategoriaSeccion(models.Model):
    """
    Por si es necesario agrupar secciones
    """
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre


class CategoriaNoticia(models.Model):
    """
    Agrupador de las noticias
    """
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre


class NoticiasHome(models.Model):
    ''' Noticias para la home de un website '''
    website = models.ForeignKey(Website, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=90)
    categoria = models.ForeignKey(CategoriaNoticia, null=True, blank=True)
    html = RichTextField(null=True, blank=True)
    imagen = VersatileImageField(upload_to='imagenes/noticias')
    mas_info_url = models.URLField(null=True, blank=True)
    fecha = models.DateTimeField(default=timezone.now)


class Seccion(models.Model):
    '''
    Cada una de las secciones del sitio web
    '''
    website = models.ForeignKey(Website, on_delete=models.CASCADE)
    subseccion_de = models.ForeignKey(
        'self', on_delete=models.CASCADE, null=True, blank=True)
    titulo_corto = models.CharField(max_length=45)
    # para usar en la URL, asegurar único dentro de un website
    titulo_slug_web = models.SlugField(max_length=150)
    titulo_texto = models.CharField(max_length=145)
    subtitulo_texto = models.CharField(max_length=145, null=True, blank=True)
    descripcion_breve = models.CharField(max_length=195, null=True, blank=True)
    publicada = models.BooleanField(default=False)
    # puede estar publicada en la home pero queremos que aparezca inactiva y
    # sin link
    activada = models.BooleanField(default=True)
    html = RichTextField()
    # font-awesome icon. Example fa-gear
    # https://fortawesome.github.io/Font-Awesome/icons/
    fa_icon = models.CharField(max_length=90, null=True, blank=True)
    imagen = models.ImageField(
        upload_to='imagenes/secciones',
        null=True,
        blank=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    orden_prioridad = models.PositiveIntegerField(default=10)
    # algunas secciones queremos que apunten a partes especiales del sistema
    # este campo DEBE ser el name de una URL que se pueda obtener vía reverse
    url_especial = models.CharField(max_length=195, null=True, blank=True)
    categoria = models.ForeignKey(
        CategoriaSeccion,
        on_delete=models.SET_NULL,
        null=True,
        blank=True)

    def __str__(self):
        return self.titulo_corto

    def save(self, *args, **kwargs):
        self.titulo_slug_web = slugify(self.titulo_corto)
        super(Seccion, self).save(*args, **kwargs)

    def get_url(self):
        '''
        algunas secciones son comunes y otras apuntan a urls especiales
        '''
        if not self.url_especial or self.url_especial == '':
            return reverse(
                'website.seccion', kwargs={
                    'seccion': self.titulo_slug_web})
        else:
            return reverse(self.url_especial)

    class Meta:
        unique_together = (("website", "titulo_slug_web"),)
        ordering = ['orden_prioridad']
