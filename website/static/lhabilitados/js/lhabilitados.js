var _fcSearchTimer = null, _faSearchTimer = null;
var _categoryList = [];
var _actividadesList = [];
var _subCatList = [];

var _markerList = [];
var _markerCluster = null;
var _curMap = null;

var _tooltip = null;

var _mouseX;
var _mouseY;

$(document).ready(function() {
    initMap();
    
    getRamasFromAPI();

    registerPageEvents();

    tooltipHandler_byTitle();
});

function displayBizInfo(selectedMarker) {
    let bizData = selectedMarker.customData;
    let htmlCode = "";
    let bizDetailElement = $("#bizDetails");
    let catName = "";

    bizDetailElement.hide();

    htmlCode += "<div class='colInfo'>";
    htmlCode += "<div class='boxInfo'><p><span class='bd_title'>Nombre Completo: </span><span>" + bizData.nombre_completo + "</span></p></div>";
    htmlCode += "<div class='boxInfo'><p><span class='bd_title'>Nombre Fantasía: </span><span>" + bizData.nombre_fantasia + "</span></p></div>";
    htmlCode += "<div class='boxInfo'><p><span class='bd_title'>Dirección: </span><span>" + bizData.direccion_completa + "</span></p></div>";

    // Recorremos todas las categorías del comercio.
    htmlCode += "<div class='boxInfo'><p><span class='bd_title'>Categorías: </span>";
    for (let i = 0; i < bizData.codigo_actividad_ota.length; i++) {
        let curCat = bizData.codigo_actividad_ota[i];

        if (curCat) {
            let catIndex = getCategoryIndex_ByID(curCat.codigo);
            if (catIndex != -1) {
                catName += "<span class='bd_cat'>" + _subCatList[catIndex].nombre + "</span>"
            }
        }
    }
    
    htmlCode += catName + "</p></div>";

    htmlCode += "</div>";

    bizDetailElement.html(htmlCode).fadeIn(150);
}

function searchRamasByName(actName) {
    clearTimeout(_fcSearchTimer);
    _fcSearchTimer = setTimeout(() => {
        $("#filtroCategoria > span").each(function(index, element) {
            let curCatName = $(this).text().toLowerCase();
            if (curCatName.indexOf(actName.toLowerCase()) >= 0) {
                $(element).show();
            }
            else $(element).hide();
        })
    }, 150);
}

function searchCategoryByName(catName) {
    clearTimeout(_faSearchTimer);
    _faSearchTimer = setTimeout(() => {
        $("#sf_categoria > span").each(function(index, element) {
            let curActName = $(this).text().toLowerCase();
            let curEl = $(element);
            if (curActName.indexOf(catName.toLowerCase()) >= 0) curEl.show();
            else curEl.hide();
        })
    }, 150);
}

/**
 * Registra los eventos que se desean capturar a lo largo de la página.
 */
function registerPageEvents() {
    let filtroRamas = $("#filtroCategoria");
    let filtroCategoria = $("#sf_categoria");

    $(".fc_inputCont").click(function() {
        toggleCatResDialog($(this));
    });

    // Click sobre cada una de las ramas de negocios.
    filtroRamas.on("click", ".filtroItem", function() {
        selectRubroLocales($(this), filtroRamas);
    });

    // Click sobre cada una de las sub-categorias de negocios.
    filtroCategoria.on("click", ".filtroItem", function() {
        selectCategoriaLocales($(this), filtroCategoria);
    });

    $("#btn_srch").click(function() {
        searchLocales($(this));
    });

    $("#tb_fc_srch").keyup(function() {
        searchRamasByName($(this).val());
    });

    $("#tb_fcs_srch").keyup(function() {
        searchCategoryByName($(this).val());
    })

    $(document).mousemove(function (e) {
        _mouseX = e.pageX;
        _mouseY = e.pageY;
    });

    /*$("#filtroMainCont").on("click", "#biz_backButton", function() {
        //$("#bizDetails").hide();
        //$("#filters_wrapper").fadeIn(150);
        animateSelectedMarker(null);
    });*/

    documentClickHandler();
}

function searchLocales(curButton) {
    let srchTextbox = $("#tb_fcs_srch");

    let codeID = srchTextbox.attr("data-catid");
    let rubID = srchTextbox.attr("data-rubid");

    if (curButton.hasClass("disabled")) return;

    $("#bizDetails").hide();
    getBusinessFromApi_ByRamaID(codeID, null, null, rubID);
}

function selectCategoriaLocales(curElement, filtroElement) {
    let codeID = curElement.attr("data-code");
    let rubID = curElement.attr("data-rubid");
    let filterText = curElement.text();
    
    filtroElement.hide();
    $("#tb_fcs_srch").val(filterText).attr({"data-catID": codeID, "data-rubid": rubID}); //.attr("data-catID", codeID);
    $("#btn_srch").removeClass("disabled");
}

function selectRubroLocales(curElement, filtroRamas) {
    let codeID = curElement.attr("data-code");
    let filterText = curElement.text();

    showCategoriesFilter(codeID);

    filtroRamas.hide();
    $("#tb_fc_srch").val(filterText).attr("data-rubID", codeID);
    $("#tb_fcs_srch").val("");
    $("#srch_res").empty();
    $("#btn_srch").addClass("disabled");
}

function documentClickHandler() {
    $(document).mouseup(function (e) {
        var container = new Array();
        container.push("#filtroCategoria");
        container.push("#sf_categoria");
        
        $.each(container, function(key, value) {
            if (!$(value).is(e.target) && $(value).has(e.target).length === 0) {
                $(value).hide();
            }
        });
    });
}

function toggleCatResDialog(curEl) {
    let curType = curEl.attr("data-type").toLowerCase();
    let element = null;

    if (curType == "cat") element = $("#filtroCategoria");
    else if (curType == "subcat") element = $("#sf_categoria");

    if (element.is(":visible")) element.fadeOut(150);
    else element.fadeIn(150);
}

/**
 * Añade todos los locales al mapa.
 * @param localesList Lista de locales obtenidos desde el API
 */
function addLocalesToMap(localesList, srchResEl) {
    let markerBounds = new google.maps.LatLngBounds();

    // Recorremos todos los locales
    for (let i = 0; i < localesList.length; i++) {
        let local = localesList[i];
        let localGeo = local.geometry.coordinates;
        let localProperties = local.properties
        let pos = {lng: parseFloat(localGeo[0]), lat: parseFloat(localGeo[1])};

        let curMarker = new google.maps.Marker({
            position: pos,
            customData: localProperties
        });
        
        curMarker.addListener("click", function() {
            showBizDetails(curMarker);
        });

        curMarker.addListener("mouseover", function(el) {
            displayMapTooltip(el, this.customData);
        });

        curMarker.addListener("mouseout", function() {
            hideMapTooltip();
        });

        markerBounds.extend(pos);

        _markerList.push(curMarker);
    }

    _markerCluster = new MarkerClusterer(_curMap, _markerList, 
        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

    if (_markerList.length > 0) _curMap.fitBounds(markerBounds);

    if (localesList.length > 0) {
        srchResEl.html("<span>Total de locales encontrados: " + localesList.length + "</span>");
    }
    else {
        srchResEl.html("<span>No se han encontrado locales con los filtros seleccionados.");
        //<span>Total de locales encontrados: <span id="sr_totalCount">0</span> </span>
    }

    removeLoadingImage(srchResEl);
}

/**
 * Obtiene todos los comercios que pertenecen a una rama en particular.
 * @param ramaID ID de la rama para la cual se desean obtener comercios.
 */
function getBusinessFromApi_ByRamaID(catID, totalList, urlParam, ramaID) {
    let curURL = (urlParam == null) 
        ? "https://gobiernoabierto.cordoba.gob.ar/api/v2/ventanilla-unica/locales-habilitados/?codigo_categoria="+catID
        : urlParam;

    let srchResEl = $("#srch_res");
    if (!totalList) totalList = [];

    if (catID == -1 && urlParam == null) {
        curURL = "https://gobiernoabierto.cordoba.gob.ar/api/v2/ventanilla-unica/locales-habilitados/?rama_id="+ramaID;
    }

    showLoadingImage(srchResEl);

    $.getJSON(curURL, function(response) {
        // Limpiamos los marcadores del mapa.
        clearMarkers();
        // Recorremos todos los resultados
        for (let i = 0; i < response.results.features.length; i++) {
            if (response.results.features[i]) {
                totalList.push(response.results.features[i]);
            }
        }

        if (response.next) {
            getBusinessFromApi_ByRamaID(catID, totalList, response.next, ramaID);
        }
        else {
            addLocalesToMap(totalList, srchResEl);
        }
    });
}

function displayMapTooltip(selectedEl, data) {
    let posX = selectedEl.xa.clientX + 30;
    let posY = selectedEl.xa.clientY + 45;
    let htmlCode = "";
    let bizName = data.nombre_fantasia.length <= 0 ? data.nombre_completo : data.nombre_fantasia;

    htmlCode += "<h2>" + bizName + "</h2>";
    htmlCode += "<p>" + data.direccion_completa + "</p>";

    _tooltip.html(htmlCode);
    _tooltip.css({top: posY, left: posX});
    _tooltip.fadeIn(150);
}

/**
 * Muestra u oculta los tooltip obteniendo el valor del mismo del atributo title de un elemento.
 */
function tooltipHandler_byTitle() {
    $("body").on("mouseenter", ".ttip", function (e) {
        var elParent = $(this).closest("div");

        var curPos = elParent.offset().top - $(window).scrollTop();

        // Obtenemos el valor del tooltip
        var title = $(this).attr('title');

        $(this).data('ttipText', title).removeAttr('title');
        $('<div class="ttooltip"></div>').text(title).appendTo('body').fadeIn(150).css('visibility: hidden');

        var docHeight = $('.ttooltip').outerHeight();
        var windowHeight = $(window).height();
        var yVal = 0;
        var xVal = e.pageX + 5;

        var maxHeight = parseInt(windowHeight) - parseInt(docHeight) - 25;
        var totalPos = parseInt(curPos) + parseInt(docHeight);

        if (totalPos < maxHeight) yVal = parseInt(_mouseY) + 25;
        else yVal = parseInt(_mouseY) - parseInt(docHeight) - 10;

        var tgtElement = $(".ttooltip");
        tgtElement.css({ top: yVal, left: xVal, visibility: 'normal' });

        //if (changeArrow === true) tgtElement.removeClass("tooltipUp").addClass("tooltipDown");  
    }).on("mouseleave", ".ttip", function () {
        $(this).attr('title', $(this).data('ttipText'));
        var tgt = $(".ttooltip");

        if (tgt.length == 0) tgt = $(".ttooltip");

        tgt.remove();
    }).on("mousemove", ".ttip", function (e) {
        var elParent = $(this).closest("div");
        var target = $('.ttooltip');

        if (target.length === 0) target = $('.ttooltip');

        var docHeight = target.outerHeight();
        var curPos = elParent.offset().top - $(window).scrollTop();

        var windowHeight = $(window).height();
        var yVal = 0;
        var xVal = e.pageX + 5;

        var maxHeight = parseInt(windowHeight) - parseInt(docHeight) - 25;
        var totalPos = parseInt(curPos) + parseInt(docHeight);

        if (totalPos < maxHeight) yVal = parseInt(_mouseY) + 25;
        else yVal = parseInt(_mouseY) - parseInt(docHeight) - 10;

        target.css({ top: yVal, left: xVal });
    });
}

function hideMapTooltip() {
    _tooltip.hide();
}

function showBizDetails(marker) {
    animateSelectedMarker(marker);

    displayBizInfo(marker);
}

function animateSelectedMarker(marker) {
    // Desactivamos la animacion de todos los demás marcadores.
    for (let i = 0; i < _markerList.length; i++) {
        if (_markerList[i]) {
            _markerList[i].setAnimation(null);
        }
    }

    if (marker) {
        // animamos el marcador seleccionado.
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}

function clearMarkers() {
    if (_markerList) {
        for (let i = 0; i < _markerList.length; i++) {
            _markerList[i].setMap(null);
        }
        _markerList = [];
    }

    if (_markerCluster) {
        _markerCluster.clearMarkers(null);
        _markerCluster = null;
    }
}

function getRamasFromAPI() {
    let url = "https://gobiernoabierto.cordoba.gob.ar/api/v2/ordenanzatributaria/ordenanzatributaria-ramas/?excluir=si";

    showLoadingImage("#hab_filters");

    $.getJSON(url, function(response) {
        if (response) {
            let filtroCat = $("#filtroCategoria");
            for (let i = 0; i < response.results.length; i++) {
                let curRama = response.results[i];
                _categoryList.push(curRama);
                
                filtroCat.append("<span class='filtroItem' data-code='" + curRama.id + "'>" + curRama.nombre + "</span>");
            }

            fillActividades();
            fillSubCategories();
        }

        removeLoadingImage("#hab_filters");

        $("#tb_fc_srch").focus();
        $("#fc_cont").show();
    });
}

function getCategoriesFromAPI(totalData, urlParam) {
    let curURL = urlParam == null ? "https://gobiernoabierto.cordoba.gob.ar/api/v2/ordenanzatributaria/ordenanzatributaria/" : urlParam;
    let habFilterElement = $("#hab_filters");

    showLoadingImage(habFilterElement);

    $.getJSON(curURL, function(response) {
        // Si existen mas páginas con resultados, las solicitamos al servidor, pasandole la url correspondiente.
        if (response.next != null) {
            /*if (!categoryExists(totalData, response.results.codigo)) */
            totalData.push(response.results);

            getCategoriesFromAPI(totalData, response.next);
        }
        else {
            let filtroCategoria = $("#filtroCategoria");
            totalData.push(response.results);
            _categoryList = filterCategoryList(totalData);
            fillSubCategories();
            for (let j = 0; j < _categoryList.length; j++) {
                let curData = _categoryList[j];
                if (curData) {
                    filtroCategoria.append("<span class='filtroItem' data-code='" + curData.id + "'>"+ curData.nombre + "</span>");
                }
            }

            removeLoadingImage(habFilterElement);

            $("#tb_fc_srch").focus();
            $("#fc_cont").show();
        }
    });
}

function showSubRamasFilter(codeID) {
    let subCatEl = $("#sf_categoria");

    // Limpiamos el contenedor
    subCatEl.html("");

    showLoadingImage(subCatEl);

    /*for (let i = 0; i < _actividadesList.length; i++) {
        let curAct = _actividadesList[i];

        if (curAct) {
            subCatEl.append("<span class='filtroItem' data-code='" + curAct.id + "'>"+ curAct.nombre + "</span>");
        }
    }*/

    let curURL = "https://gobiernoabierto.cordoba.gob.ar/api/v2/ordenanzatributaria/ordenanzatributaria/?id_rama=" + codeID + "page_size=150";
    let habFilterElement = $("#hab_filters");

    $.getJSON(curURL, function(response) {
        console.log(response);
/*         for (let j = 0; j < _categoryList.length; j++) {
            let curData = _categoryList[j];
            if (curData) {
                subCatEl.append("<span class='filtroItem' data-code='" + curData.id + "'>"+ curData.nombre + "</span>");
            }
        } */

        removeLoadingImage(subCatEl);
        $("#fcs_cont").show();
        $("#fc_cont").show();
    });
}

function showCategoriesFilter(codeID) {
    let subCatEl = $("#sf_categoria");

    // Limpiamos el contenedor
    subCatEl.html("");

    showLoadingImage(subCatEl);

    subCatEl.append("<span class='filtroItem' data-code='-1' data-rubid='" + codeID + "'>Todas las categorías</span>");

    let curURL = "https://gobiernoabierto.cordoba.gob.ar/api/v2/ordenanzatributaria/ordenanzatributaria/?id_rama=" + codeID + "&page_size=150";

    $.getJSON(curURL, function(response) {
        console.log(response);
        for (let j = 0; j < response.results.length; j++) {
            let curData = response.results[j];
            if (curData) {
                subCatEl.append("<span class='filtroItem' data-code='" + curData.id + "'>"+ curData.nombre + "</span>");
            }
        }

        removeLoadingImage(subCatEl);
        $("#fcs_cont").show();
        $("#fc_cont").show();
    });
}

function filterCategoryList(categoryList) {
    let tmpArray = [];
    
    for (let i = 0; i < categoryList.length; i++) {
        let curItem = categoryList[i];

        for (let j = 0; j < curItem.length; j++) {
            let curCat = curItem[j];
            if (!categoryExists(tmpArray, curCat.id)) tmpArray.push(curCat);
        }
    }

    return tmpArray;
}

function fillActividades() {
    _actividadesList = [];

    for (let i = 0; i < _categoryList.length; i++) {
        if (_categoryList[i]) {
            for (let j = 0; j < _categoryList[i].categoria.length; j++) {
                _actividadesList.push(_categoryList[i].categoria[j]);
            }
        }
    }
}

function fillSubCategories() {
    for (let i = 0; i < _actividadesList.length; i++) {
        if (_actividadesList[i]) {
            for (let j = 0; j < _actividadesList[i].actividades.length; j++) {
                _subCatList.push(_actividadesList[i].actividades[j]);
            }
        }
    }
}

function categoryExists(catList, categoryCode) {
    for (let i = 0; i < catList.length; i++) {
        if (catList[i] && catList[i].id == categoryCode) return true;
    }
}

function getCategoryIndex_ByID(codeID) {
    for (let i = 0; i < _subCatList.length; i++) {
        let curCat = _subCatList[i];
        if (curCat && curCat.codigo == codeID) return i;
    }

    return -1;
}

/**
 * Inicializa la carga y renderizado del mapa.
 */
function initMap() {
    _curMap = new google.maps.Map($("#mapa").get(0), {
        center: new google.maps.LatLng(-31.420116, -64.1910517),
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [
            {
                featureType: "poi",
                elementType: "labels",
                stylers: [
                    { visibility: "off" }
                ]
            }
        ]
    });

    _tooltip = $(".lhabTootlip");
}

function showLoadingImage(elementName) {
    let spinner = null;

    // Si como parametro se recibio una cadena entonces debemos buscar el elemento en el DOM.
    if (typeof(elementName) == "string") spinner = $(elementName).find(".loadingSpinner");
    else spinner = elementName.find(".loadingSpinner");
    
    if (spinner.length === 0) {
        // Insertamos el spinner antes del resto del contenido.
        $(elementName).prepend("<div class='loadingSpinner'></div>");
        // Ocultamos los demás elementos de manera que solo quede visible el spinner.
        $(elementName).children().not(".loadingSpinner").hide();
    }
}

function removeLoadingImage(elementName) {
    (typeof(elementName) == "string") ? $(elementName).find(".loadingSpinner").remove()
                                      : elementName.find(".loadingSpinner").remove();
}