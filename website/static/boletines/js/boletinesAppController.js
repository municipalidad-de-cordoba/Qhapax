app.controller('boletinesController', ['$scope', '$http', '$filter', function($scope, $http, $filter) {
  $scope.formData = {};
  $scope.storage = window.localStorage;

  var lastChecked = $scope.storage.getItem('lastChecked');
  var today = new Date();
  $scope.results = JSON.parse($scope.storage.getItem('data'));
  $scope.years = JSON.parse($scope.storage.getItem('years'));
  $scope.monthsByYear = JSON.parse($scope.storage.getItem('months'));


  $scope.selectedYearSelect = "2018";
  $scope.boletinCode = "";
  $scope.colors = {"2018": "verde", "2017":"rosa", "2016":"verdeOsc", "antes":"celeste", "Todos":"azul"};
  $scope.selectedYear = "2018";
  $scope.months = {"01": "Enero", "02":"Febrero", "03": "Marzo", "04":"Abril", "05":"Mayo", "06":"Junio", "07":"Julio", "08":"Agosto", "09":"Septiembre", "10":"Octubre", "11":"Noviembre", "12":"Diciembre"};
  $scope.textoYear = {"2018": "2018", "2017":"2017", "2016":"2016", "antes":"anterior a 2016", "Todos": ""};
  $scope.selectedMonths = [];


  if(lastChecked<today || $scope.results == null ){
    $http({
    method: "GET",
    crossDomain: true,
    url: url_API + "boletin-municipal/boletines-municipales/?page_size=150"
    }).then(function successCallback(response) {
      $scope.storage.setItem('lastChecked', today);
      $scope.results = response.data.results;
      $scope.storage.setItem('data', JSON.stringify(response.data.results));
      $scope.years = ["Todos"];
      $scope.monthsByYear = {};
      $scope.identity = angular.identity;
      for(var i = 0; i<$scope.results.length;i++){
        var year = $scope.results[i].fecha.split('-')[0];
        var month = $scope.results[i].fecha.split('-')[1];
        if($scope.years.indexOf(year) === -1){
          $scope.years.push(year);
          $scope.monthsByYear[year] = [month];
          // $scope.monthsByYear.push({key: year, value:[month]});
        }else{
          $scope.monthsByYear[year].indexOf(month) === -1 ?  $scope.monthsByYear[year].push(month) : null ;

        }

      }
      console.log($scope.monthsByYear);
      $scope.years = $filter('orderBy')($scope.years, '', false)
      $scope.storage.setItem('years', JSON.stringify($scope.years));
      $scope.storage.setItem('months', JSON.stringify($scope.monthsByYear));

      $scope.showMonths = $scope.monthsByYear[$scope.selectedYearSelect];
      $scope.selectedColor = $scope.colors[$scope.selectedYear];
      $scope.selectedtextoYear = $scope.textoYear[$scope.selectedYear];
      $scope.boletines = $filter('filter')($scope.results, {fecha: $scope.selectedYear });
    }, function errorCallback(response) {

    });
  }else{
    $scope.showMonths = $scope.monthsByYear[$scope.selectedYearSelect];
    $scope.selectedColor = $scope.colors[$scope.selectedYear];
    $scope.selectedtextoYear = $scope.textoYear[$scope.selectedYear];
    $scope.boletines = $filter('filter')($scope.results, {fecha: $scope.selectedYear });
  }


  $scope.selectChange = function(x) {
    $scope.showMonths = $scope.monthsByYear[x];
  }

  $scope.filterYear = function(year) {
    $scope.clearForm();
    $scope.selectedYear = year;
    $scope.selectedColor = $scope.colors[$scope.selectedYear];
    $scope.selectedtextoYear = $scope.textoYear[$scope.selectedYear];
    if(year != "Todos"){
      $scope.boletines = $filter('filter')($scope.results, {fecha: $scope.selectedYear });
    }else{
        $scope.boletines = $scope.results;
    }
    if(year!= "antes"){
      $scope.selectedYearSelect = year;
    }
    $scope.showMonths = $scope.monthsByYear[year];
  };

  $scope.toggleCheck = function (month) {
      if ($scope.selectedMonths.indexOf(month) === -1) {
          $scope.selectedMonths.push(month);
      } else {
          $scope.selectedMonths.splice($scope.selectedMonths.indexOf(month), 1);
      }
  };

  $scope.clearForm = function(){
    $scope.formData = {};
    $scope.boletinCode = "";
    $scope.selectedYearSelect = "2018";
    $scope.showMonths = $scope.monthsByYear[$scope.selectedYearSelect];
    $('.mutliSelect input[type="checkbox"]').prop("checked", false);
    $('.multiSel').hide();
    $('.multiSel').html("");
    $(".hida").show();
    $scope.selectedMonths = [];
    $scope.myForm.$setPristine();
    $scope.search();
  }
  var URLactual = window.location;

  $scope.search = function(){

    $scope.selectedYear = $scope.selectedYearSelect;
    $scope.selectedColor = $scope.colors[$scope.selectedYearSelect];
    $scope.selectedtextoYear = $scope.textoYear[$scope.selectedYearSelect];
    $scope.showMonths = $scope.monthsByYear[$scope.selectedYearSelect];

    //Agregado para controlar las urls
    var url = URLactual.toString();
    if(url.substr(-6,1) == '#'){
      location.href=url.slice(-6, 6)+'#'+$scope.selectedYear;
    }
    if(url.substr(-5,1) == '#'){
      location.href=url.slice(-5,5)+'#'+$scope.selectedYear;
    }
    else {
      location.href=window.location+'#'+$scope.selectedYear;
    }
    //Agregado para controlar las urls


    if($scope.selectedYearSelect == "Todos"){
        $scope.boletines = $scope.results;
    }else{
      $scope.boletines = [];
      if($scope.selectedMonths.length > 0){
        for(var i = 0; i< $scope.selectedMonths.length; i++){
          $scope.boletines = $scope.boletines.concat($filter('filter')($scope.results, {fecha: $scope.selectedYear+"-"+$scope.selectedMonths[i] }));
        }
      }else{
        $scope.boletines = $filter('filter')($scope.results, {fecha: $scope.selectedYear});
      }

    }
    if( $scope.boletinCode != ""){
      $scope.boletines = $filter('filter')($scope.boletines, function (i) {
          return (i.codigo === $scope.boletinCode);
      });
    }

  }

}]);



app.filter('moment', [
  function () {
    return function (date, method) {
      moment.locale('es-do');
      var momented = moment(date);
      return momented[method].apply(momented, Array.prototype.slice.call(arguments, 2));
    };
  }
]);
