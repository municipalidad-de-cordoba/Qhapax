var app = angular.module("boletinesMunicipales", []);
var url_API = "https://gobiernoabierto.cordoba.gob.ar/api/v2/";

app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
});
