import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { HttpClientModule } from '@angular/common/http';  // replaces previous Http service
import {ArboladoService} from './arbolado.service'

import { Globals } from './globals'
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import {MapContentComponent} from './map-content/map-content.component';

registerLocaleData(localeEs, 'es');

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAYw1rqa9mL1-__v8h2CVgsRJmpmP2mP1s' // AIzaSyAYw1rqa9mL1-__v8h2CVgsRJmpmP2mP1s
    }),
    HttpClientModule
  ],
  providers: [ArboladoService, Globals, { provide: LOCALE_ID, useValue: 'es' }],
  declarations: [ AppComponent, MapContentComponent ],
  bootstrap: [ AppComponent ]
})
export class AppModule {}
