import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {MapBounds} from './arbol'
import { Globals } from './globals';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ArboladoService {

    constructor(private http:HttpClient) {}
    strQueries:string = "";
    globales : Globals = new Globals();
    // Uses http.get() to load data from a single API endpoint
    getArboles(bounds:MapBounds, especie?: string[], barrio?: string[], cazuela?:string[], estado_fito?:string[], inclinacion?:string[], descalzado?:string[], fustePartido?:string[], tipoOrden?:string[], precision?:number, next?:string) {
        this.strQueries = "";
        if(next!=null){
          return this.http.get(next);
        }else{
          if(especie && especie.length > 0){
            this.strQueries += "&especie_ids="+especie.toString();
          }
          if(cazuela && cazuela.length > 0){
            this.strQueries += "&cazuela="+cazuela.toString();
          }
          if(barrio && barrio.length > 0){
            this.strQueries += "&barrio_nombre="+barrio.toString();
          }
          if(estado_fito && estado_fito.length > 0){
            this.strQueries += "&fitosanitario="+estado_fito.toString();
          }
          if(inclinacion && inclinacion.length > 0){
            this.strQueries += "&inclinacion="+inclinacion.toString();
          }
          if(descalzado && descalzado.length > 0){
            this.strQueries += "&descalzado="+descalzado.toString();
          }
          if(fustePartido && fustePartido.length > 0){
            this.strQueries += "&fuste_partido="+fustePartido.toString();
          }
          if(tipoOrden && tipoOrden.length > 0){
            this.strQueries += "&tipo_orden="+tipoOrden.toString();
          }
          if(precision<=8){
            this.strQueries += "&precision="+Math.round(precision).toString();
          }
          this.strQueries += "&in_bbox="+bounds.bottom+","+bounds.left+","+bounds.top+","+bounds.right;

          // console.log('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/arboles/?page_size='+this.globales.pageSize+this.strQueries);
          return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/arboles/?page_size='+this.globales.pageSize+this.strQueries);
        }

    }
    getCantArboles(especie?: string[], barrio?: string[], cazuela?:string[], estado_fito?:string[], inclinacion?:string[], descalzado?:string[], fustePartido?:string[], tipoOrden?:string[]) {
        this.strQueries = "";

        if(especie && especie.length > 0){
          this.strQueries += "&especie_ids="+especie.toString();
        }
        if(cazuela && cazuela.length > 0){
          this.strQueries += "&cazuela="+cazuela.toString();
        }
        if(barrio && barrio.length > 0){
          this.strQueries += "&barrio_nombre="+barrio.toString();
        }
        if(estado_fito && estado_fito.length > 0){
          this.strQueries += "&fitosanitario="+estado_fito.toString();
        }
        if(inclinacion && inclinacion.length > 0){
          this.strQueries += "&inclinacion="+inclinacion.toString();
        }
        if(descalzado && descalzado.length > 0){
          this.strQueries += "&descalzado="+descalzado.toString();
        }
        if(fustePartido && fustePartido.length > 0){
          this.strQueries += "&fuste_partido="+fustePartido.toString();
        }
        if(tipoOrden && tipoOrden.length > 0){
          this.strQueries += "&tipo_orden="+tipoOrden.toString();
        }
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/contador-arboles/?page_size='+this.globales.pageSize+this.strQueries);

    }
    getSpecies(){
      return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/especies/?page_size=100');
    }
    getBarrios(){
      return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/barrios/barrios/?page_size=470');
    }
    getTotalArboles(){
      return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/contador-arboles');
    }
    getTotalAusentes(){
      return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/contador-arboles/?especie_ids=2');
    }
    getTotalPlantados(){
      return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/arboles/?tipo_orden=200');
    }
}
