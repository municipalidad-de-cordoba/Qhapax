import { Component, Input} from '@angular/core';
import { MouseEvent } from '@agm/core';
import {Http} from '@angular/http';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {enableProdMode} from '@angular/core';
enableProdMode();
import {ArboladoService} from './arbolado.service'
import {Especie, Foto, FiltroStandard, Properties, Tree, MapBounds, Cluster} from './arbol'
import { Globals } from './globals';

import { GoogleMapsAPIWrapper } from '@agm/core';

declare const google:any; 
import '../assets/markerclusterer.js';
declare const MarkerClusterer;

import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  globales : Globals = new Globals();
  totalPaginas : number = 0;
  nroPagActual : number = 0;
  title: string = 'Obras de Terceros';
  zoom: number = 13;
  lat: number = -31.414321;
  lng: number = -64.183307;
  markerCluster:any;
  map: any;
  mapIsInitialize : boolean = false;


  @Input() locations: {};
  public gMaps: GoogleMapsAPIWrapper;

  public loadAPIWrapper(map) {
    this.map = map;
    this.markerCluster = new MarkerClusterer(this.map, null, {imagePath: '/static/arbolado/img/map_img/m'});
  }

  constructor(private _arboladoService: ArboladoService) { }

  cantArboles: number = 0;
  cantTotalArboles: number = 0;
  currentPrecision : number = 6;
  trees : Tree[];

  shownTrees : Tree[];
  arboles: any;
  especies: any;
  auxData : any;
  barrios: any;
  cantTotAux : any;
  species : Especie[] = [];
  plantedSpecies : Especie[] = [];
  cazuelasFilter : FiltroStandard[] = [new FiltroStandard(1, "Faltante", false), new FiltroStandard(2,"Adecuada", false), new FiltroStandard(3,"Inadecuada", false), new FiltroStandard(4,"Franja Verde", false)];
  inclinacionFilter : FiltroStandard[] = [new FiltroStandard(10, "Si", false), new FiltroStandard(20, "No", false),new FiltroStandard(30, "Leve", false), new FiltroStandard(40,"Severa", false)];
  estadoFitosanitarioFilter : FiltroStandard[] = [new FiltroStandard(1, "Bueno", false), new FiltroStandard(2, "Malo", false), new FiltroStandard(3, "Regular", false), new FiltroStandard(4, "Muerto", false)];
  descalzadoFilter : FiltroStandard[] = [new FiltroStandard("si","si",false), new FiltroStandard("no", "no", false)];
  fustePartidoFilter : FiltroStandard[] = [new FiltroStandard("si","si",false), new FiltroStandard("no", "no", false)];
  tipoOrdenFilter : FiltroStandard[] = [new FiltroStandard(100, "Relevamiento", false), new FiltroStandard(200, "Plantación", false)];
  barrioFilter : FiltroStandard[] = [];

  selectedSpecies : string[] = [];
  selectedBarrio : string[] = [];
  selectedCazuelas: string[] = [];
  selectedInclinacion: string[] = [];
  selectedEstadoFitosanitario: string[] = [];
  selectedFustePartido : string[] = [];
  selectedTipoOrden : string[] = [];
  selectedDescalzado : string[] = [];

  currentSpecies : string[] = [];
  currentBarrio : string[] = [];
  currentCazuelas: string[] = [];
  currentInclinacion: string[] = [];
  currentEstadoFitosanitario: string[] = [];
  currentFustePartido : string[] = [];
  currentTipoOrden : string[] = [];
  currentDescalzado : string[] = [];

  selectedTree : Tree;
  currentBounds:MapBounds;

  clusters: Cluster[] = [];
  clustersres:any;
  emptyTrees : number = 0;
  totalTrees : number = 0;
  plantedTrees : number = 0;
  plantedTresSpecies : number = 0;

  private ngUnsubscribe: Subject<any>[] = [];
  currentCall : number = 0;

  ngOnInit() {
    console.log("HOLA");
    this.ngUnsubscribe.push(new Subject());
    this.getStatistics();
    this.getArbolesFromEmpty();
    $(document).on('click', ".dropdownSelect dt a", function(e) {
      e.preventDefault();
      $(e.target).closest(".dropdownSelect").find("dd").slideToggle('fast');
      $(e.target).closest(".dropdownSelect").find("dd ul").slideToggle('fast');
      $('.dropdownSelect').each(function(){
        if($(this).html() != $(e.target).closest(".dropdownSelect").html()){
          $(this).addClass('collapsiblockCollapsed');
          $(this).find("dd").hide();
          $(this).find("dd ul").hide();
        }
      });
      $(e.target).closest(".dropdownSelect").find(".arrow").toggleClass("up down");
    });

    $(document).on('click', ".dropdownSelect dd ul li a", function(e) {
      e.preventDefault();
      $(e.target).closest(".dropdownSelect").find("dd").hide();
      $(e.target).closest(".dropdownSelect").find("dd ul").hide();
    });

    function getSelectedValue(id) {
      return $("#" + id).find("dt a span.value").html();
    }

    $(document).bind('click', function(e) {
      var $clicked = $(e.target);
      if (!$clicked.parents().hasClass("dropdownSelect")){
        $(e.target).closest(".dropdownSelect").find("dd").hide();
        $(e.target).closest(".dropdownSelect").find("dd ul").hide();
      }
    });

    $(document).on('click', '.mutliSelect input[type="checkbox"]', function(e) {
      var $container = $(e.target).closest(".dropdownSelect");
      var inputName = $(e.target).attr("name");
    });

    $(document).on('click', '#referenciasLink', function(e) {
      var $container = $(e.target);
      $("#referencias").slideToggle();
      $container.find(".arrow").toggleClass("up down")
    });

  }
  getStatistics(){
    this._arboladoService.getTotalPlantados().subscribe(
      data => {this.auxData = data},
      err => console.error(err),
      () => {
        this.auxData.results.features.forEach(e => {
            var auxEspecie = e.properties.especie.id;
            if(this.plantedSpecies.indexOf(auxEspecie) ==-1){
                this.plantedSpecies.push(auxEspecie);
            }

        });
        this.plantedTrees = this.auxData.count;
        this.plantedTresSpecies = this.plantedSpecies.length;
      }
    );
    this._arboladoService.getTotalArboles().subscribe(
      data => { this.auxData = data},
      err => console.error(err),
      () => {
        this.totalTrees = this.auxData.count;
      }
    );
    this._arboladoService.getTotalAusentes().subscribe(
      data => { this.auxData = data},
      err => console.error(err),
      () => {
        this.emptyTrees = this.auxData.count;
      }
    );


  }

  selectedArrayFromIDNames(array, selectedArray){
    var selectedNames = [];
    array.forEach(e => {
      if(selectedArray.indexOf(e.id.toString()) != -1){
        selectedNames.push(e);
      }
    });
    return selectedNames;
  }
  selectedArrayFromTextNames(array, selectedArray){
    var selectedNames = [];
    array.forEach(e => {
      if(selectedArray.indexOf(e.name) != -1){
        selectedNames.push(e);
      }
    });
    return selectedNames;
  }

  selectAllEspecies(){
    var checked = true;
    if(this.selectedSpecies.length == this.species.length){
      checked = false;
    }
    this.selectedSpecies = [];
    this.species.forEach(e => {
      e.checked = checked;
      if(checked){
        this.selectedSpecies.push(e.id.toString());
      }
    });
  }

  selectAllBarrios(){
    var checked = true;
    if(this.selectedBarrio.length == this.barrioFilter.length){
      checked = false;
    }
    this.selectedBarrio = [];

    this.barrioFilter.forEach(e => {
      e.checked = checked;
      if(checked){
        this.selectedBarrio.push(e.id.toString());
      }
    });
  }

  checkSpecie(value: string){
    if(this.selectedSpecies.some(x=>x==value.toString())){
      this.checkinModel(this.species, value.toString(), false);
      this.selectedSpecies = this.selectedSpecies.filter(item => item !== value.toString());
    }else{
      this.selectedSpecies.push(value.toString());
      this.checkinModel(this.species, value.toString(), true);
    }
  }

  checkinModel(array, value:string, checked:boolean){
    array.forEach(e => {
      if(e.id == value){
        e.checked = checked;
        return false;
      }
    });
  }

  checkBarrio(value: string){
    if(this.selectedBarrio.some(x=>x==value.toString())){
      this.checkinModel(this.barrioFilter, value.toString(), false);
      this.selectedBarrio = this.selectedBarrio.filter(item => item !== value.toString());
    }else{
      this.selectedBarrio.push(value.toString());
      this.checkinModel(this.barrioFilter, value.toString(), true);
    }
  }
  checkCazuelas(value:string){
    if(this.selectedCazuelas.some(x=>x==value.toString())){
      this.checkinModel(this.cazuelasFilter, value.toString(), false);
      this.selectedCazuelas = this.selectedCazuelas.filter(item => item !== value.toString());
    }else{
      this.selectedCazuelas.push(value.toString());
      this.checkinModel(this.cazuelasFilter, value.toString(), true);
    }
  }
  checkEstadoFito(value:string){
    if(this.selectedEstadoFitosanitario.some(x=>x==value.toString())){
      this.selectedEstadoFitosanitario = this.selectedEstadoFitosanitario.filter(item => item !== value.toString());
      this.checkinModel(this.estadoFitosanitarioFilter, value.toString(), false);

    }else{
      this.selectedEstadoFitosanitario.push(value.toString());
      this.checkinModel(this.estadoFitosanitarioFilter, value.toString(), true);
    }

  }
  checkInclinacion(value:string){
    if(this.selectedInclinacion.some(x=>x==value.toString())){
      this.selectedInclinacion = this.selectedInclinacion.filter(item => item !== value.toString());
      this.checkinModel(this.inclinacionFilter, value.toString(), false);

    }else{
      this.selectedInclinacion.push(value.toString());
      this.checkinModel(this.inclinacionFilter, value.toString(), true);
    }

  }
  checkDescalzado(value:string){
    if(this.selectedDescalzado.some(x=>x==value.toString())){
      this.selectedDescalzado = this.selectedDescalzado.filter(item => item !== value.toString());
      this.checkinModel(this.descalzadoFilter, value.toString(), false);

    }else{
      this.selectedDescalzado.push(value.toString());
      this.checkinModel(this.descalzadoFilter, value.toString(), true);
    }
  }
  checkFuste(value:string){
    if(this.selectedFustePartido.some(x=>x==value.toString())){
      this.selectedFustePartido = this.selectedFustePartido.filter(item => item !== value.toString());
      this.checkinModel(this.fustePartidoFilter, value.toString(), false);
    }else{
      this.selectedFustePartido.push(value.toString());
      this.checkinModel(this.fustePartidoFilter, value.toString(), true);
    }
  }
  checkTipoOrden(value:string){
    if(this.selectedTipoOrden.some(x=>x==value.toString())){
      this.selectedTipoOrden = this.selectedTipoOrden.filter(item => item !== value.toString());
      this.checkinModel(this.tipoOrdenFilter, value.toString(), false);
    }else{
      this.selectedTipoOrden.push(value.toString());
      this.checkinModel(this.tipoOrdenFilter, value.toString(), true);
    }
  }

  today:string = new Date().toLocaleDateString();

  getArbolesFromEmpty(){
    this.trees = [];

    if (localStorage.speciesData == null || localStorage.speciesData == "null" || localStorage.speciesData == "undefined" || localStorage.userDate != this.today ) {
      this._arboladoService.getSpecies().subscribe(
        data => { this.especies = data},
        err => console.error(err),
        () => {
          // console.log('done loading species', this.especies)
          this.especies.results.forEach(e => {
              var auxEspecie = new Especie(e.id, e.nombre,(this.selectedSpecies.indexOf(e.id.toString())!=-1), null);
              this.species.push(auxEspecie);
          });
          localStorage.speciesData = JSON.stringify(this.species);
        }
      );
      this.getBarrios();
    } else {
        this.species = JSON.parse(localStorage.speciesData);
        this.selectedSpecies.forEach(e =>{
          this.checkinModel(this.species, e, true);
        });
        this.getBarrios();
    }

  }
  isSelectedSpecies(id){
    if(this.selectedSpecies.indexOf(id.toString())!= -1){
      return true;
    }
    return false;
  }
  getBarrios(){
    if (localStorage.barriosData == null || localStorage.barriosData == "null" || localStorage.barriosData == "undefined" || localStorage.userDate != this.today ) {
      this._arboladoService.getBarrios().subscribe(
        data => { this.barrios = data},
        err => console.error(err),
        () => {
          this.barrios.results.features.forEach(e => {
              var auxBarrio = new FiltroStandard(e.id, e.properties.nombre,(this.selectedBarrio.indexOf(e.properties.nombre)!=-1));
              this.barrioFilter.push(auxBarrio);
          });
          localStorage.barriosData = JSON.stringify(this.barrioFilter);
        }
      );
      localStorage.userDate = this.today;
    } else {
      this.barrioFilter = JSON.parse(localStorage.barriosData);
      this.selectedBarrio.forEach(e =>{
        this.checkinModel(this.barrioFilter, e, true);
      });
    }

  }

  exists : boolean = false;
  speciesExists(id){
    this.exists = false;
    this.species.forEach(e => {
      if(e.id == id){
        this.exists = true;
      }
    });
    return this.exists;
  }
  filterTrees(){
    this.ngUnsubscribe[this.currentCall].next();
    this.ngUnsubscribe[this.currentCall].complete();
    this.currentCall++;
    // console.log("change" this.currentCall);
    this.ngUnsubscribe.push(new Subject());

    var speciesToSend = (this.selectedSpecies.length == this.species.length? [] : this.selectedSpecies);
    var barriosToSend = (this.selectedBarrio.length == this.barrioFilter.length? [] : this.selectedBarrio);
    this.currentBarrio = barriosToSend;
    this.currentSpecies = speciesToSend;
    this.currentTipoOrden = this.selectedTipoOrden;
    this.currentCazuelas = this.selectedCazuelas;
    this.currentEstadoFitosanitario = this.selectedEstadoFitosanitario;
    this.currentInclinacion = this.selectedInclinacion;
    this.currentDescalzado = this.selectedDescalzado;
    this.currentFustePartido = this.selectedFustePartido;
    this.currentTipoOrden = this.selectedTipoOrden;
    this.getTrees();
  }

  getTrees() {
    this.getArboles(this.currentCall, this.currentSpecies , this.currentBarrio, this.currentCazuelas, this.currentEstadoFitosanitario, this.currentInclinacion, this.currentDescalzado, this.currentFustePartido, this.currentTipoOrden, this.currentPrecision);
  }

  circleClick(event){
    this.selectedTree = this.getTreeInfo(event);

    setTimeout(function(){
      $('html, body').animate({
         scrollTop: $(".treeFile").offset().top - 165
      }, 1000);
    }, 250);
  }

  auxTree:Tree;
  getTreeInfo(id){
    this.auxTree = null;
    this.trees.forEach(e => {
      if(e.id == id){
        this.auxTree = e;
        return false;
      }
    });

    return this.auxTree;
  }
  // speciesColors : string[] = ["#96BF6E", null, "#BB4E98", "#F1F5CD", "#DEA9D0", "#556C2F", "#6E8F46", "#BCBCEE", "#EBD366", "#E07B45", null, "#ADC257", "#D1C6CB", "#849442"];
   treeTypeColors: { [id: string] : string; } = {"normal": "#30b030", "plantacion":"#3635c4", "falta":"#c43635"};
   treeTypeStrokeColors: { [id: string] : string; } = {"normal": "#258825", "plantacion":"#252588", "falta":"#9c2b2a"};
  getArboles(callnum:number,especie?:string[], barrio?:string[], cazuela?:string[], estado_fito?:string[], inclinacion?:string[], descalzado?:string[], fustePartido?:string[], tipoOrden?:string[], precision?:number){
    this.trees = [];

    this.nroPagActual=0;
    this.clusters = [];
    this.getArbolesWFilter(callnum, especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden, precision);
  }

  getArbolesWFilter(callnum:number, especie?:string[], barrio?:string[], cazuela?:string[], estado_fito?:string[], inclinacion?:string[], descalzado?:string[], fustePartido?:string[], tipoOrden?:string[], precision?:number, next?:string){

    this._arboladoService.getCantArboles(especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden).subscribe(
      data => {this.cantTotAux = data},
      err => console.error(err),
      () => {
        this.cantTotalArboles = this.cantTotAux.count;
      }
    );
    this.cantArboles=0;

    this.markerCluster.RemoveClusters();


    if(precision<=8){
      this.totalPaginas=0;
      this.markerCluster.clearMarkers();
      this._arboladoService.getArboles(this.currentBounds,especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden, precision, next).pipe(takeUntil(this.ngUnsubscribe[callnum])).subscribe(
        data => {this.clustersres = data;},
        err => console.error(err),
        () => {
          // console.log('done loading trees', this.clustersres);
          this.clustersres.results.features.forEach(e => {
              this.cantArboles+=e.properties.cluster_count;
              this.clusters.push(new Cluster(e.geometry.coordinates[1], e.geometry.coordinates[0], e.properties.cluster_count, (Math.pow(2, (21 - this.zoom)) * 1128.497220 * 0.0005)));
          });
          if(this.clustersres.next!=null){
            this.getArbolesWFilter(callnum, especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden, precision, this.clustersres.next);
          }else{
            this.clusters.forEach(e => {
              console.log(e)
              this.markerCluster.AddCluster(e.lat, e.lng, e.count);
            });
          }
        }
      );
    }else{

      this._arboladoService.getArboles(this.currentBounds,especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden, precision, next).pipe(takeUntil(this.ngUnsubscribe[callnum])).subscribe(
        data => {this.arboles = data},
        err => console.error(err),
        () => {
          // console.log('done loading trees', this.arboles);
          this.cantArboles = this.arboles.count;
          var redondeado = Math.round(this.cantArboles/this.globales.pageSize);
          this.totalPaginas = (this.cantArboles/this.globales.pageSize>redondeado?redondeado+1 :redondeado);
          this.cantArboles+=this.arboles.count;
          this.arboles.results.features.forEach(e => {

              var auxType = (e.properties.especie.nombre.toLowerCase() == "ausente"? "falta" : (e.properties.tipos_de_orden.toLowerCase() == "plantación"? "plantacion" : "normal"));
              var auxEspecie = new Especie(e.properties.especie.id, e.properties.especie.nombre, (this.selectedSpecies.indexOf(e.id.toString())!=-1), this.treeTypeColors[auxType], this.treeTypeStrokeColors[auxType]);
              var auxFoto = new Foto(e.properties.foto.original, e.properties.foto.thumbnail_500);
              var auxProperties = new Properties(e.properties.barrio, e.properties.cazuela, auxEspecie, auxFoto, e.properties.problema_descalzado, e.properties.problema_fuste_partido, e.properties.tipos_de_orden ,e.properties.estado_fitosanitario, e.properties.inclinacion)
              this.trees.push(new Tree(e.id, e.geometry.coordinates[1], e.geometry.coordinates[0], auxProperties, (Math.pow(2, (21 - this.zoom)) * 1128.497220 * 0.00035)));
          });
          this.nroPagActual ++;
          if(this.arboles.next!=null){
            this.getArbolesWFilter(callnum,especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden, precision, this.arboles.next);
          }else{
              this.nroPagActual=0;
              this.totalPaginas=0;
          }
          this.shownTrees = this.trees;
        }
      );
    }
  }

  resetSelectedTree(){
    this.selectedTree = null;
    $('html, body').animate({
       scrollTop: $(".mapCont").offset().top - 165
    }, 1000);
  }
  zoomChange(zoom){
    if(this.zoom>zoom){
      this.currentPrecision-=.4;
    }else if(this.zoom<zoom){
      this.currentPrecision+=.4;
    }
    switch(zoom){
      case 13:
        this.currentPrecision = 6;
        break;
      case 14:
        this.currentPrecision = 6;
        break;
      case 15:
        this.currentPrecision = 6;
        break;
      case 16:
        this.currentPrecision = 7;
        break;
      case 17:
        this.currentPrecision = 8;
        break;
    }

    this.trees.forEach(e => {
      if(zoom > 1 && zoom<19){
        e.radius = Math.pow(2, (21 - zoom)) * 1128.497220 * 0.00035;
      }
    });

    this.clusters.forEach(e => {
      if(zoom > 1 && zoom<19){
        e.radius = Math.pow(2, (21 - zoom)) * 1128.497220 * 0.0008;
      }
    });
    this.zoom = zoom;
  }
  boundsChange(bounds) {
     this.currentBounds = new MapBounds(bounds.j.j, bounds.j.l, bounds.l.j, bounds.l.l);
  }
  //setBoundsData(bounds){
  setBoundsData() {

    this.ngUnsubscribe[this.currentCall].next();
    this.ngUnsubscribe[this.currentCall].complete();
    this.currentCall++;
    // console.log("change" this.currentCall);
    this.ngUnsubscribe.push(new Subject());
    this.getTrees();
  }

  // getShownTrees(isNext){
  //   this.shownTrees = this.trees;
  //   // this.shownTrees = this.trees.filter( t => {
  //   //   return (t.lat >= this.currentBounds.left && t.lat <= this.currentBounds.right) && (t.lng >= this.currentBounds.top && t.lng <= this.currentBounds.bottom);
  //   // });
  //   if(isNext==null){
  //       this.nroPagActual=0;
  //       this.totalPaginas=0;
  //   }
  // }



}
