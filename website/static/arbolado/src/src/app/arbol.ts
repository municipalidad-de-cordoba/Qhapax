

export class Especie {
  id: string;
  name: string;
  color: string;
  strokeColor: string;
  checked: boolean;
  constructor(id: string, nombre: string, checked:boolean, color?:string, strokeColor?:string) {
    this.id = id;
    this.name = nombre;
    if (!color) {
        this.color = "#006400";
    }else{
      this.color = color;
    }
    if (!strokeColor) {
        this.strokeColor = "#006400";
    }else{
      this.strokeColor = color;
    }
    this.checked = checked;
  }
}


export class Foto{
  original: string;
  thumbnail: string;
  constructor(original: string, thumbnail:string){
    this.original = original;
    this.thumbnail = thumbnail;
  }
}

export class FiltroStandard{
  id: string;
  name:string;
  checked: boolean;
  constructor(id: any, nombre: string, checked: boolean) {
    this.id = id.toString();
    this.name = nombre;
    this.checked = checked;
  }
}

export class Properties{
  barrio: string;
  cazuela: string;
  especie: Especie;
  estadoFitosanitario: string;
  foto: Foto;
  inclinacion: string;
  descalzado: string;
  fustePartido: string;
  tipoOrden: string;

  constructor(barrio: string, cazuela:string, especie:Especie, foto:Foto,  descalzado:boolean, fustePartido:boolean, tipoOrden:string, estadoFitosanitario?:string, inclinacion?: string) {
    this.barrio = barrio;
    this.cazuela = cazuela;
    this.especie = especie;
    this.foto =foto;
    this.tipoOrden = tipoOrden;
    this.descalzado = (descalzado == true ? "si" : "no");
    this.fustePartido = (fustePartido == true ? "si" : "no");
    if(estadoFitosanitario){
      this.estadoFitosanitario = estadoFitosanitario;
    }
    if(inclinacion){
      this.inclinacion = inclinacion;
    }

  }
}
export class Cluster{
  lat: string;
	lng: string;
	count: number;
  radius : number;
  constructor(lat: string, lng:string, count:number, radius?: number){
    this.lat = lat;
    this.lng = lng;
    this.count = count;
    if (!radius) {
        this.radius = 10;
    }
    this.radius = radius;
  }
}

export class Tree {
  id: string;
  lat: string;
	lng: string;
	radius: number;
  color: string;
  properties: Properties;
  constructor(id: string, lat:string, lng:string, properties: Properties, radius?:number) {
    if (!radius) {
        this.radius = 10;
    }
    this.id = id;
    this.lat = lat;
    this.lng  = lng;
    this.radius = radius;
    this.properties = properties;
  }

}

export class MapBounds{
  top: string;
  bottom: string;
  left: string;
  right: string;
  constructor(top: string, bottom:string, left:string,  right:string) {
    this.top = top;
    this.bottom = bottom;
    this.left = left;
    this.right = right;
  }
}
