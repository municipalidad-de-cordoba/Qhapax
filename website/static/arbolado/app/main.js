(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* @import url(https://fonts.googleapis.com/css?family=Lato); */\n\n\nagm-map{\n  height: 500px;\n}\n\n\n.filterCol{\n  position: relative;\n  height: auto;\n  font-family: 'Lato', sans-serif;\n  font-size: 1.15em;\n}\n\n\n.no-padding{\n  padding: 0;\n}\n\n\n.appCont{\n  border-bottom: 1px solid #7D35C4;\n}\n\n\n.filterCol{\n  background-color: #FFF;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  overflow:scroll;\n  border-left: 1px solid #7D35C4;\n}\n\n\n.indicadoresContCol{\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n}\n\n\n.filterContainer{\n  background-color: #FFF;\n  padding: 10px;\n  position: relative;\n}\n\n\n.titFooterBox{\n  margin: 0;\n  color: #FF00FF;\n  background-color: #C8C5EF;\n  border-bottom: 1px solid #AAA;\n  padding: 10px;\n\n}\n\n\n.titleFooterFilters{\n  color: #7D35C4;\n  font-style: italic;\n  margin: 0;\n}\n\n\n.titFilter{\n  margin: 0;\n  color: #8984AE;\n  font-family: 'Lato', sans-serif;\n}\n\n\n.dropdownSelect {\n  border-radius: 0px;\n  border: none;\n  height: 40px;\n  margin-bottom: 0px;\n  position: relative;\n}\n\n\n.dropdownSelect.first,\n.dropdownSelect.first dt{\n  height: 30px;\n}\n\n\n.dropdownSelect.first dt a{\n  padding-top: 0px;\n  padding-bottom: 0px;\n  height: 30px;\n  color: #7D35C4;\n  border-bottom: 1px solid #AAA;\n  padding-right: 40px;\n}\n\n\n.dropdownSelect dd,\n.dropdownSelect dt {\n  margin: 0px;\n  padding: 0px;\n  height: 40px;\n}\n\n\n.dropdownSelect ul {\n  margin: -1px 0 0 0;\n}\n\n\n.dropdownSelect dd {\n  position: relative;\n}\n\n\n.dropdownSelect a,\n.dropdownSelect a:visited {\n  /* color: #fff; */\n  /* text-decoration: none;\n  outline: none;\n  font-size: 12px; */\n  display: inline-block;\n  font-weight: 400;\n}\n\n\n.dropdownSelect dt a {\n  width: 100%;\n  height: 40px;\n  border-radius: 0px;\n  border: none;\n  padding: 10px 10px 10px 0px;\n  color: #8984AE;\n  font-family: 'Lato', sans-serif;\n  background-color: #FFFFFF;\n  padding-right: 30px;\n  border-bottom: 1px solid #B6B6B7;\n}\n\n\n.dropdownSelect dt a span,\n.multiSel span {\n  cursor: pointer;\n  display: inline-block;\n  padding: 0 3px 2px 0;\n}\n\n\n.hida{\n  width: 100%;\n  text-overflow: ellipsis;\n  overflow: hidden;\n  white-space: nowrap;\n  position: relative;\n  z-index: 1;\n}\n\n\n.dropdownSelect dd{\n  display: none;\n  z-index: 2;\n}\n\n\n.dropdownSelect dd ul {\n  background-color: #FFFFFF;\n  border: 0;\n  /* color: #000000; */\n  color: #8984AE;\n  left: 0px;\n  padding: 10px 15px 10px 15px;\n  position: absolute;\n  top: 2px;\n  width: 100%;\n  list-style: none;\n  height: 300px;\n  overflow: scroll;\n  display: none;\n  box-shadow: 0px 0px 17px -1px rgba(0,0,0,0.32);\n}\n\n\n.dropdownSelect.smallDrop dd ul {\n  height: 125px;\n  overflow: scroll;\n}\n\n\n.dropdownSelect.smallerDrop dd ul {\n  height: 100px;\n  overflow: scroll;\n}\n\n\n.dropdownSelect.smallestDrop dd ul {\n  height: 75px;\n  overflow: scroll;\n}\n\n\n.dropdownSelect span.value {\n  display: none;\n}\n\n\n.dropdownSelect dd ul li a {\n  padding: 5px;\n  display: block;\n}\n\n\n.dropdownSelect dd ul li a:hover {\n  background-color: #fff;\n}\n\n\ninput[type=checkbox]{\n  margin-right: 5px;\n}\n\n\n.mapCont{\n  position: relative;\n}\n\n\n.btnFiltrar{\n  width: 100%;\n  height: 40px;\n  background-color: #7D35C4;\n  color: #FFF;\n  margin-top: 30px;\n  margin-bottom: 15px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n\n.btnFiltrar.btnVolver{\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n\n\n.btnFiltrar:hover{\n  background-color: #632a9c;\n  cursor: pointer;\n  color: #FFFFFF;\n}\n\n\n/* Ficha Arbol */\n\n\n.colFile{\n  margin-top: 10px;\n}\n\n\n.treeFile{\n  position: relative;\n  padding: 50px 10px;\n  background-color: #FFFFFF;\n  background-image: url(\"/static/arbolado/img/arboles.jpg\");\n  background-position: left center;\n  background-size: cover;\n}\n\n\n.closeSelectedTree{\n  position: absolute;\n  left: 50%;\n  top:0;\n}\n\n\n.closeSelectedTree .cont{\n  position: relative;\n  left: -50%;\n  background-color: #7D35C4;\n  color: #FFF;\n  width: 50px;\n  height: 50px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  text-align: center;\n}\n\n\n.closeSelectedTree .cont:hover{\n  cursor: pointer;\n  background-color: #632a9c;\n}\n\n\n.treePic{\n  width: 100%;\n  max-width: 450px;\n  max-height: 450px;\n  margin: 0 auto;\n}\n\n\n.treePic img{\n  width: 100%;\n  max-width: 450px;\n  max-height: 450px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n\n.treeInfo{\n  /* height: 100%; */\n  height: 450px;\n  padding: 30px;\n  width: 100%;\n  background-color: #FFF;\n  margin: 0 auto;\n  position: relative;\n  font-size: 1.25em;\n}\n\n\n.treeInfoText{\n  margin-top: -5px;\n  color: #7D35C4;\n  margin-bottom: 3px;\n}\n\n\n.treeImg{\n  position: absolute;\n  top: 10px;\n  right: 10px;\n}\n\n\n.treeImg img{\n  height: 50px;\n}\n\n\n.treeInfo .treeImg{\n  top: 30px;\n  right: 30px;\n}\n\n\n.treeInfo .treeImg img{\n  height: 40px;\n}\n\n\n.treeHeader{\n  height: 50px;\n}\n\n\n.treeSpeciesCont{\n  border-bottom: 1px solid #AAA;\n}\n\n\n.treeDataCont{\n  padding-top: 20px;\n  padding-bottom: 20px;\n  height: 340px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n\n.treeData{\n  margin: 0;\n}\n\n\n.arrow {\n  border: solid #7D35C4;\n  border-width: 0 2px 2px 0;\n  display: inline-block;\n  padding: 3px;\n  position: absolute;\n  top: 18px;\n  right: 5px;\n}\n\n\n.right {\n    transform: rotate(-45deg);\n    -webkit-transform: rotate(-45deg);\n}\n\n\n.left {\n    transform: rotate(135deg);\n    -webkit-transform: rotate(135deg);\n}\n\n\n.up {\n    transform: rotate(-135deg);\n    -webkit-transform: rotate(-135deg);\n}\n\n\n.down {\n    transform: rotate(45deg);\n    -webkit-transform: rotate(45deg);\n  }\n\n\n.capitalize{\n  text-transform: capitalize;\n}\n\n\n.cantArboles{\n  color: #8984AE;\n  font-style: italic;\n  margin: 0;\n}\n\n\n.hl{\n  color: #7D35C4;\n  margin: 0;\n}\n\n\n.hl.mostrar{\n  font-weight: 600;\n}\n\n\n.hl.total{\n  font-style: italic;\n}\n\n\n.selectAll:hover{\n  cursor: pointer;\n  text-decoration: underline;\n}\n\n\n.cargando{\n  position: absolute;\n  width: auto;\n  bottom: 5px;\n  left: 5px;\n  background-color: #7D35C4;\n  color: #FFFFFF;\n  padding:5px 10px;\n  margin: 0;\n}\n\n\n#referenciasLink:hover{\n  cursor: pointer;\n}\n\n\n.referencias .arrow {\n    border: solid #7D35C4;\n    border-width: 0 2px 2px 0;\n    display: inline-block;\n    padding: 3px;\n    position: absolute;\n    top: 10px;\n    right: 3px;\n}\n\n\n.referencias{\n  position: absolute;\n  width: 320px;\n  bottom: 24px;\n  right: 50px;\n  background-color: #FFFFFF;\n  color: #7D35C4;\n  padding: 5px 20px 5px 10px;\n  margin: 0;\n}\n\n\n.centered{\n  text-align: center;\n}\n\n\n.imgTitle {\n  display: inline-block;\n  margin-top: 10px;\n  margin-bottom: 10px;\n}\n\n\n.imgTitle img{\n  height: 70px;\n}\n\n\n.appTitle{\n  display: inline-block;\n  color: #7D35C4;\n  text-transform: uppercase;\n  font-size: 2.25em;\n  vertical-align: -10px;\n  margin-left: 10px;\n}\n\n\n.filterBoxTitleCont{\n  display: flex;\n  flex-direction: column;\n  justify-content: flex-end;\n  border-bottom: 5px solid #7D35C4;\n}\n\n\n.filterBoxTitle{\n  margin-bottom: 0px;\n  font-size: 1.25em;\n  text-transform: uppercase;\n  text-align: center;\n  font-style: italic;\n  color: #7D35C4;\n  font-weight: 500;\n}\n\n\n.btnDescargar{\n  width: 100%;\n  display: block;\n  padding: 10px;\n  text-align: center;\n  border: 1px solid #C8C5EF;\n  color: #7D35C4;\n  font-size: 1.25em;\n  text-transform: uppercase;\n  align-self: flex-start;\n  margin-top: 40px;\n}\n\n\n.btnDescargar:hover{\n  background-color: #C8C5EF;\n}\n\n\n.btnDescargar .la-stack{\n  float: right;\n  margin-top: -6px;\n}\n\n\n.indicadorTitle{\n  font-size: 1.25em;\n  color: #7D35C4;\n  font-size: 1.25em;\n  text-transform: uppercase;\n  margin-top: 10px;\n}\n\n\n.indicadoresCont{\n  padding: 15px;\n  margin-top: 30px;\n}\n\n\n.indicadoresCont li{\n  color: #7D35C4;\n}\n\n\n.indicadorRow{\n  display: list-item;\n  color:#7D35C4;\n}\n\n\n.indicadorRow .indicador{\n  color: #8984AE;\n  text-align: left;\n  display: inline-block;\n  width: 100px;\n}\n\n\n.indicadorRow .valor{\n  color: #00A665;\n  font-size: 1.25em;\n  font-weight: 600;\n  text-align: left;\n  margin-top: -4px;\n  display: inline-block;\n    margin-left: 40px;\n}\n\n\n.refColores{\n  text-align: center;\n}\n\n\n.colorRef{\n  color: #8984AE;\n  font-style: italic;\n  font-size: 0.75em;\n}\n\n\n.refIndicador{\n  border: 1px solid #000;\n  display: inline-flex;\n  flex-direction: column;\n  justify-content: center;\n  width: 21px;\n  height: 21px;\n  margin-right: 5px;\n}\n\n\n.refIndicador.azul{\n  border-color: #3635c4;\n}\n\n\n.refIndicador.rojo{\n  border-color: #c43635;\n}\n\n\n.refIndicador.verde{\n  border-color: #30b030;\n}\n\n\n.textRefIndicador{\n  color: #4B4B4C;\n  font-style: italic;\n  font-size: 0.9em;\n}\n\n\n.refIndicador .circuloIndicador{\n  height: 15px;\n  width: 15px;\n  background-color: #bbb;\n  border-radius: 50%;\n  display: inline-block;\n  margin-left: 2px;\n}\n\n\n.refIndicador.azul .circuloIndicador{\n  background-color: #3635c4;\n}\n\n\n.refIndicador.rojo .circuloIndicador{\n  background-color: #c43635;\n}\n\n\n.refIndicador.verde .circuloIndicador{\n  background-color: #30b030;\n}\n\n\n@media(min-width:576px){\n}\n\n\n@media(min-width:768px){}\n\n\n@media(min-width:992px){\n\n  agm-map,\n  .filterCol{\n    position: relative;\n    min-height: 520px;\n    font-family: 'Lato', sans-serif;\n    font-size: 1.15em;\n  }\n  .treeFile{\n    padding: 50px;\n  }\n}\n\n\n@media(min-width:1200px){\n  .btnDescargar{\n    margin-top: 0px;\n  }\n}\n"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n<script src=\"https://code.jquery.com/jquery-3.2.1.min.js\"></script>\n<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>\n<link rel=\"stylesheet\" href=\"https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css\"> \n\n  <div class=\"row appCont\">\n    <div class=\"col-xs-12 col-md-12 col-lg-9 centered colHeader\">\n      <div class=\"imgTitle pulse infinite animated\"><img src=\"/static/arbolado/img/treeFilled.png\"></div>\n      <h1 class=\"appTitle\">Nuestros Árboles</h1>\n    </div>\n    <div class=\"col-xs-12 col-lg-3 visible-lg-* filterBoxTitleCont colHeader\">\n      <h2 class=\"filterBoxTitle\">Buscá nuestros árboles</h2>\n    </div>\n    <div class=\"col-xs-12 col-md-12 col-lg-6 no-padding mapCont\">\n      <agm-map [latitude]=\"lat\" [longitude]=\"lng\" [zoom]=\"zoom\" (zoomChange)=\"zoomChange($event)\" (boundsChange)=\"boundsChange($event)\" (idle)=\"setBoundsData($event)\">\n        <span *ngIf=\"currentPrecision>8\">\n          <agm-circle *ngFor=\"let t of shownTrees; let i = index\"\n          [latitude]=\"t.lat\"\n          [longitude]=\"t.lng\"\n          [radius]=\"t.radius\"\n          [fillOpacity]=\"0.75\"\n          [fillColor]=\"t.properties.especie.color\"\n          [strokeColor] = \"t.properties.especie.strokeColor\"\n          [strokeOpacity] = \"1\"\n          [strokePosition]=\"0\"\n          [strokeWeight]=\"1\"\n          (circleClick)=\"circleClick(t.id)\">\n          </agm-circle>\n        </span>\n      <core-map-content (onMapLoad)='loadAPIWrapper($event)'></core-map-content>\n      </agm-map>\n      <div class=\"referencias\">\n        <div class=\"row\">\n          <div class=\"col-xs-12 col-lg-12\">\n            <div id=\"referenciasLink\">REFERENCIAS DEL COLOR DEL MAPA <i class=\"arrow up\"></i></div>\n          </div>\n          <div class=\"col-xs-12 col-lg-12 refColores\" id=\"referencias\" style=\"display: none;\">\n            <div class=\"row\">\n              <div class=\"col-xs-4 col-lg-4\">\n                <div class=\"refIndicador azul\"><div class=\"circuloIndicador\"></div></div><span class=\"colorRef\">azúl</span>\n                <div class=\"textRefIndicador\">árboles plantados</div>\n              </div>\n              <div class=\"col-xs-4 col-lg-4\">\n                <div class=\"refIndicador rojo\"><div class=\"circuloIndicador\"></div></div><span class=\"colorRef\">rojo</span>\n                <div class=\"textRefIndicador\">árboles faltantes</div>\n              </div>\n              <div class=\"col-xs-4 col-lg-4\">\n                <div class=\"refIndicador verde\"><div class=\"circuloIndicador\"></div></div><span class=\"colorRef\">verde</span>\n                <div class=\"textRefIndicador\">árboles censados</div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div *ngIf=\"totalPaginas>0\" class=\"cargando\">Cargando página {{nroPagActual}} de {{totalPaginas| number:'1.0-0'}}</div>\n    </div>\n    <div class=\"col-xs-12 col-md-6 col-lg-3 indicadoresContCol\">\n      <a href=\"https://gobiernoabierto.cordoba.gob.ar/data/datos-abiertos/categoria/ambiente/arbolado-municipal/2770\" class=\"btnDescargar\" target=\"_blank\">Descargar\n        <span class=\"la-stack\" title=\"Icono de descarga\">\n          <span class=\"la la-circle-thin la-stack-2x\"></span>\n          <span class=\"la la-download la-stack-1x\"></span>\n        </span>\n      </a>\n      <div class=\"indicadoresCont\">\n        <h2 class=\"indicadorTitle\">Indicadores de Plantación</h2>\n          <div class=\"indicadorRow\">\n            <div class=\"indicador\">Árboles</div>\n            <div class=\"valor\">{{plantedTrees | number:'1.0-0':'es'}}</div>\n          </div>\n          <div class=\"indicadorRow\">\n            <div class=\"indicador\">Especies</div>\n            <div class=\"valor\">{{plantedTresSpecies | number:'1.0-0':'es'}}</div>\n          </div>\n        <h2 class=\"indicadorTitle\">Indicadores del Censo</h2>\n          <div class=\"indicadorRow\">\n            <div class=\"indicador\">Árboles</div>\n            <div class=\"valor\">{{totalTrees | number:'1.0-0':'es'}}</div>\n          </div>\n          <div class=\"indicadorRow\">\n            <div class=\"indicador\">Ausentes</div>\n            <div class=\"valor\">{{emptyTrees | number:'1.0-0':'es'}}</div>\n          </div>\n          <div class=\"indicadorRow\">\n            <div class=\"indicador\">Total</div>\n            <div class=\"valor\">{{totalTrees + emptyTrees | number:'1.0-0':'es'}}</div>\n          </div>\n          <div class=\"indicadorRow\">\n            <div class=\"indicador\">Especies</div>\n            <div class=\"valor\">{{species.length | number:'1.0-0':'es'}}</div>\n          </div>\n      </div>\n    </div>\n    <div class=\"col-xs-12 col-md-6 hidden-lg filterBoxTitleCont colHeader\">\n      <h2 class=\"filterBoxTitle\">Buscá nuestros árboles</h2>\n    </div>\n    <div class=\"col-xs-12 col-md-6 col-lg-3 no-padding filterCol\">\n      <div class=\"filterContainer\">\n\n        <div class=\"treeImg\"><img src=\"/static/arbolado/img/treeFilled.png\"></div>\n\n        <!-- Dropdrown Barrio -->\n        <dl class=\"dropdownSelect\">\n          <dt>\n          <a href=\"#\">\n            <span class=\"hida\">Barrio<span *ngIf=\"selectedBarrio.length>0\">:<span *ngIf=\"selectedBarrio.length < barrioFilter.length\"><span *ngFor=\"let s of selectedArrayFromTextNames(barrioFilter, selectedBarrio)\"> {{s.name}},</span></span><span *ngIf=\"selectedBarrio.length == barrioFilter.length\">Todos</span></span></span><i class=\"arrow down\"></i>\n          </a>\n          </dt>\n\n          <dd>\n              <div class=\"mutliSelect\">\n                  <ul>\n                      <li>\n                        <input name=\"selectAllB\" type=\"checkbox\" (change)=\"selectAllBarrios()\"/>Seleccionar Todos\n                      </li>\n                      <li *ngFor=\"let b of barrioFilter; let i = index\">\n                          <input name=\"selectedB[]\" value=\"{{b.name}}\" type=\"checkbox\" (change)=\"checkBarrio(b.name)\" [(ngModel)]=\"b.checked\"/>{{b.name}}\n                      </li>\n                  </ul>\n              </div>\n          </dd>\n        </dl>\n        <!-- Dropdrown Cazuela -->\n        <dl class=\"dropdownSelect smallDrop\">\n          <dt>\n          <a href=\"#\">\n            <span class=\"hida\">Cazuela<span *ngIf=\"selectedCazuelas.length>0\">:</span> <span *ngFor=\"let s of selectedArrayFromTextNames(cazuelasFilter, selectedCazuelas)\"> {{s.name}},</span></span><i class=\"arrow down\"></i>\n          </a>\n          </dt>\n\n          <dd>\n              <div class=\"mutliSelect\">\n                  <ul>\n                      <li *ngFor=\"let c of cazuelasFilter; let i = index\">\n                          <input name=\"selectedC[]\" value=\"{{c.name}}\" type=\"checkbox\" (change)=\"checkCazuelas(c.name)\" [(ngModel)]=\"c.checked\"/>{{c.name}}\n                      </li>\n                  </ul>\n              </div>\n          </dd>\n        </dl>\n\n        <!-- Dropdown Descalzado -->\n        <dl class=\"dropdownSelect smallestDrop\">\n          <dt>\n          <a href=\"#\">\n            <span class=\"hida capitalize\">Descalzado<span *ngIf=\"selectedDescalzado.length>0\">:</span> <span *ngFor=\"let des of selectedArrayFromTextNames(descalzadoFilter, selectedDescalzado)\"> {{des.name}},</span></span><i class=\"arrow down\"></i>\n          </a>\n          </dt>\n\n          <dd>\n              <div class=\"mutliSelect capitalize\">\n                  <ul>\n                      <li *ngFor=\"let des of descalzadoFilter; let i = index\">\n                          <input name=\"selectedDescalzado[]\" value=\"{{des.name}}\" type=\"checkbox\" (change)=\"checkDescalzado(des.name)\" [(ngModel)]=\"des.checked\"/>{{des.name}}\n                      </li>\n                  </ul>\n              </div>\n          </dd>\n        </dl>\n\n        <!-- Dropdrown Tipo de Arbol -->\n        <dl class=\"dropdownSelect\">\n          <dt>\n            <a href=\"#\">\n              <span class=\"hida\">Especie<span *ngIf=\"selectedSpecies.length >0\">:</span><span *ngIf=\"selectedSpecies.length < species.length\"><span *ngFor=\"let s of selectedArrayFromIDNames(species, selectedSpecies)\" class=\"selectedCont\"> {{s.name}},</span></span><span *ngIf=\"selectedSpecies.length == species.length\">Todas</span></span><i class=\"arrow down\"></i>\n            </a>\n          </dt>\n          <dd>\n              <div class=\"mutliSelect\">\n                  <ul>\n                      <li>\n                          <input name=\"selectAllS\" type=\"checkbox\" (change)=\"selectAllEspecies()\"/>Seleccionar Todas\n                      </li>\n                      <li *ngFor=\"let s of species; let i = index\">\n                          <input name=\"selectedS[]\" value=\"{{s.id}}\" type=\"checkbox\" (change)=\"checkSpecie(s.id)\" [(ngModel)]=\"s.checked\"/><span class=\"sName\">{{s.name}}</span>\n                      </li>\n                  </ul>\n              </div>\n          </dd>\n        </dl>\n\n\n\n        <!-- Dropdrown Estado Fitosanitario -->\n        <dl class=\"dropdownSelect smallDrop\">\n          <dt>\n          <a href=\"#\">\n            <span class=\"hida\">Estado Fitosanitario<span *ngIf=\"selectedEstadoFitosanitario.length>0\">:</span> <span *ngFor=\"let s of selectedArrayFromIDNames(estadoFitosanitarioFilter, selectedEstadoFitosanitario)\"> {{s.name}},</span></span><i class=\"arrow down\"></i>\n          </a>\n          </dt>\n\n          <dd>\n              <div class=\"mutliSelect\">\n                  <ul>\n                      <li *ngFor=\"let e of estadoFitosanitarioFilter; let i = index\">\n                          <input name=\"selectedEst[]\" value=\"{{e.id}}\" type=\"checkbox\" (change)=\"checkEstadoFito(e.id)\" [(ngModel)]=\"e.checked\"/><span class=\"sName\">{{e.name}}</span>\n                      </li>\n                  </ul>\n              </div>\n          </dd>\n        </dl>\n\n        <!-- Dropdrown Inclinacion -->\n        <dl class=\"dropdownSelect smallDrop\">\n          <dt>\n          <a href=\"#\">\n            <span class=\"hida\">Inclinación <span *ngIf=\"selectedInclinacion.length>0\">:</span> <span *ngFor=\"let s of selectedArrayFromIDNames(inclinacionFilter, selectedInclinacion)\"> {{s.name}},</span></span><i class=\"arrow down\"></i>\n          </a>\n          </dt>\n\n          <dd>\n              <div class=\"mutliSelect\">\n                  <ul>\n                      <li *ngFor=\"let inc of inclinacionFilter; let i = index\">\n                          <input name=\"selectedInc[]\" value=\"{{inc.id}}\" type=\"checkbox\" (change)=\"checkInclinacion(inc.id)\" [(ngModel)]=\"inc.checked\"/><span class=\"sName\">{{inc.name}}</span>\n                      </li>\n                  </ul>\n              </div>\n          </dd>\n        </dl>\n\n        <!-- Dropdown Fuste Partido -->\n        <dl class=\"dropdownSelect smallestDrop\">\n          <dt>\n          <a href=\"#\">\n            <span class=\"hida capitalize\">Fuste Partido<span *ngIf=\"selectedFustePartido.length>0\">:</span> <span *ngFor=\"let fus of selectedArrayFromTextNames(fustePartidoFilter, selectedFustePartido)\"> {{fus.name}},</span></span><i class=\"arrow down\"></i>\n          </a>\n          </dt>\n\n          <dd>\n              <div class=\"mutliSelect capitalize\">\n                  <ul>\n                      <li *ngFor=\"let fus of fustePartidoFilter; let i = index\">\n                          <input name=\"selectedFuste[]\" value=\"{{fus.name}}\" type=\"checkbox\" (change)=\"checkFuste(fus.name)\" [(ngModel)]=\"fus.checked\"/>{{fus.name}}\n                      </li>\n                  </ul>\n              </div>\n          </dd>\n        </dl>\n\n        <!-- Dropdown Tipo de Orden -->\n        <dl class=\"dropdownSelect smallestDrop\">\n          <dt>\n          <a href=\"#\">\n            <span class=\"hida\">Tipo de Orden<span *ngIf=\"selectedTipoOrden.length>0\">:</span> <span *ngFor=\"let s of selectedArrayFromIDNames(tipoOrdenFilter, selectedTipoOrden)\"> {{s.name}},</span></span><i class=\"arrow down\"></i>\n          </a>\n          </dt>\n\n          <dd>\n              <div class=\"mutliSelect\">\n                  <ul>\n                      <li *ngFor=\"let tipo of tipoOrdenFilter; let i = index\">\n                          <input name=\"selectedTipoO[]\" value=\"{{tipo.id}}\" type=\"checkbox\" (change)=\"checkTipoOrden(tipo.id)\" [(ngModel)]=\"tipo.checked\"/>{{tipo.name}}\n                      </li>\n                  </ul>\n              </div>\n          </dd>\n        </dl>\n\n        <div class=\"btn btnFiltrar\" (click)=\"filterTrees()\"><div>Filtrar <i class=\"fa fa-filter\" aria-hidden=\"true\"></i></div></div>\n        <p class=\"titleFooterFilters\">Arbolado</p>\n      </div>\n      <div class=\"titFooterBox\">\n        <div class=\"row\">\n          <div class=\"col-xs-6 col-lg-6\">\n            <p class=\"cantArboles\">Se muestran</p>\n          </div>\n          <div class=\"col-xs-6 col-lg-6\">\n            <p class=\"hl mostrar\">{{cantArboles}} árboles</p>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col-xs-6 col-lg-6\">\n            <p class=\"cantArboles\">Total</p>\n          </div>\n          <div class=\"col-xs-6 col-lg-6\">\n            <p class=\"hl total\">{{cantTotalArboles}} árboles</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"row treeFile\" *ngIf=\"selectedTree\">\n    <div class=\"closeSelectedTree\" (click)=\"resetSelectedTree()\"><div class=\"cont\"><i class=\"fa fa-chevron-up\" aria-hidden=\"true\"></i></div></div>\n    <div class=\"col-xs-12 col-lg-5 colFile\">\n      <div class=\"treePic\">\n        <a href=\"{{selectedTree.properties.foto.thumbnail}}\" target=\"_blank\" rel=\"noopener\"><img src=\"{{selectedTree.properties.foto.thumbnail}}\"></a>\n      </div>\n    </div>\n    <div class=\"col-xs-12 col-lg-7 colFile\">\n      <div class=\"treeInfo\">\n        <div class=\"treeHeader\">\n          <div class=\"treeImg\"><img src=\"/static/arbolado/img/treeFile.png\"></div>\n          <div class=\"treeSpeciesCont\">\n            <p class=\"titFilter\">Especie</p>\n            <p class=\"treeInfoText\">{{selectedTree.properties.especie.name}}</p>\n          </div>\n        </div>\n        <div class=\"treeDataCont\">\n          <p class=\"treeData\"><span class=\"titFilter\">Barrio: </span><span class=\"treeInfoText\">{{selectedTree.properties.barrio}}</span></p>\n          <p class=\"treeData\"><span class=\"titFilter\">Cazuela: </span><span class=\"treeInfoText\">{{selectedTree.properties.cazuela}}</span></p>\n          <p class=\"treeData\"><span class=\"titFilter\">Estado Fitosanitario: </span><span class=\"treeInfoText\">{{selectedTree.properties.estadoFitosanitario}}</span></p>\n          <p class=\"treeData\"><span class=\"titFilter\">Inclinación: </span><span class=\"treeInfoText\">{{selectedTree.properties.inclinacion}}</span></p>\n          <p class=\"treeData\"><span class=\"titFilter\">Descalzado: </span><span class=\"treeInfoText capitalize\">{{selectedTree.properties.descalzado}}</span></p>\n          <p class=\"treeData\"><span class=\"titFilter\">Fuste Partido: </span><span class=\"treeInfoText capitalize\">{{selectedTree.properties.fustePartido}}</span></p>\n          <p class=\"treeData\"><span class=\"titFilter\">Tipo de Orden: </span><span class=\"treeInfoText\">{{selectedTree.properties.tipoOrden}}</span></p>\n        </div>\n      </div>\n    </div>\n  </div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _arbolado_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./arbolado.service */ "./src/app/arbolado.service.ts");
/* harmony import */ var _arbol__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./arbol */ "./src/app/arbol.ts");
/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./globals */ "./src/app/globals.ts");
/* harmony import */ var _assets_markerclusterer_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../assets/markerclusterer.js */ "./src/assets/markerclusterer.js");
/* harmony import */ var _assets_markerclusterer_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_assets_markerclusterer_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_7__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();





var AppComponent = /** @class */ (function () {
    function AppComponent(_arboladoService) {
        this._arboladoService = _arboladoService;
        this.globales = new _globals__WEBPACK_IMPORTED_MODULE_5__["Globals"]();
        this.totalPaginas = 0;
        this.nroPagActual = 0;
        this.title = 'Obras de Terceros';
        this.zoom = 13;
        this.lat = -31.414321;
        this.lng = -64.183307;
        this.mapIsInitialize = false;
        this.cantArboles = 0;
        this.cantTotalArboles = 0;
        this.currentPrecision = 6;
        this.species = [];
        this.plantedSpecies = [];
        this.cazuelasFilter = [new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](1, "Faltante", false), new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](2, "Adecuada", false), new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](3, "Inadecuada", false), new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](4, "Franja Verde", false)];
        this.inclinacionFilter = [new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](10, "Si", false), new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](20, "No", false), new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](30, "Leve", false), new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](40, "Severa", false)];
        this.estadoFitosanitarioFilter = [new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](1, "Bueno", false), new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](2, "Malo", false), new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](3, "Regular", false), new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](4, "Muerto", false)];
        this.descalzadoFilter = [new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"]("si", "si", false), new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"]("no", "no", false)];
        this.fustePartidoFilter = [new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"]("si", "si", false), new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"]("no", "no", false)];
        this.tipoOrdenFilter = [new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](100, "Relevamiento", false), new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](200, "Plantación", false)];
        this.barrioFilter = [];
        this.selectedSpecies = [];
        this.selectedBarrio = [];
        this.selectedCazuelas = [];
        this.selectedInclinacion = [];
        this.selectedEstadoFitosanitario = [];
        this.selectedFustePartido = [];
        this.selectedTipoOrden = [];
        this.selectedDescalzado = [];
        this.currentSpecies = [];
        this.currentBarrio = [];
        this.currentCazuelas = [];
        this.currentInclinacion = [];
        this.currentEstadoFitosanitario = [];
        this.currentFustePartido = [];
        this.currentTipoOrden = [];
        this.currentDescalzado = [];
        this.clusters = [];
        this.emptyTrees = 0;
        this.totalTrees = 0;
        this.plantedTrees = 0;
        this.plantedTresSpecies = 0;
        this.ngUnsubscribe = [];
        this.currentCall = 0;
        this.today = new Date().toLocaleDateString();
        this.exists = false;
        // speciesColors : string[] = ["#96BF6E", null, "#BB4E98", "#F1F5CD", "#DEA9D0", "#556C2F", "#6E8F46", "#BCBCEE", "#EBD366", "#E07B45", null, "#ADC257", "#D1C6CB", "#849442"];
        this.treeTypeColors = { "normal": "#30b030", "plantacion": "#3635c4", "falta": "#c43635" };
        this.treeTypeStrokeColors = { "normal": "#258825", "plantacion": "#252588", "falta": "#9c2b2a" };
    }
    AppComponent.prototype.loadAPIWrapper = function (map) {
        this.map = map;
        this.markerCluster = new MarkerClusterer(this.map, null, { imagePath: '/static/arbolado/img/map_img/m' });
    };
    AppComponent.prototype.ngOnInit = function () {
        this.ngUnsubscribe.push(new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]());
        this.getStatistics();
        this.getArbolesFromEmpty();
        jquery__WEBPACK_IMPORTED_MODULE_7__(document).on('click', ".dropdownSelect dt a", function (e) {
            e.preventDefault();
            jquery__WEBPACK_IMPORTED_MODULE_7__(e.target).closest(".dropdownSelect").find("dd").slideToggle('fast');
            jquery__WEBPACK_IMPORTED_MODULE_7__(e.target).closest(".dropdownSelect").find("dd ul").slideToggle('fast');
            jquery__WEBPACK_IMPORTED_MODULE_7__('.dropdownSelect').each(function () {
                if (jquery__WEBPACK_IMPORTED_MODULE_7__(this).html() != jquery__WEBPACK_IMPORTED_MODULE_7__(e.target).closest(".dropdownSelect").html()) {
                    jquery__WEBPACK_IMPORTED_MODULE_7__(this).addClass('collapsiblockCollapsed');
                    jquery__WEBPACK_IMPORTED_MODULE_7__(this).find("dd").hide();
                    jquery__WEBPACK_IMPORTED_MODULE_7__(this).find("dd ul").hide();
                }
            });
            jquery__WEBPACK_IMPORTED_MODULE_7__(e.target).closest(".dropdownSelect").find(".arrow").toggleClass("up down");
        });
        jquery__WEBPACK_IMPORTED_MODULE_7__(document).on('click', ".dropdownSelect dd ul li a", function (e) {
            e.preventDefault();
            jquery__WEBPACK_IMPORTED_MODULE_7__(e.target).closest(".dropdownSelect").find("dd").hide();
            jquery__WEBPACK_IMPORTED_MODULE_7__(e.target).closest(".dropdownSelect").find("dd ul").hide();
        });
        function getSelectedValue(id) {
            return jquery__WEBPACK_IMPORTED_MODULE_7__("#" + id).find("dt a span.value").html();
        }
        jquery__WEBPACK_IMPORTED_MODULE_7__(document).bind('click', function (e) {
            var $clicked = jquery__WEBPACK_IMPORTED_MODULE_7__(e.target);
            if (!$clicked.parents().hasClass("dropdownSelect")) {
                jquery__WEBPACK_IMPORTED_MODULE_7__(e.target).closest(".dropdownSelect").find("dd").hide();
                jquery__WEBPACK_IMPORTED_MODULE_7__(e.target).closest(".dropdownSelect").find("dd ul").hide();
            }
        });
        jquery__WEBPACK_IMPORTED_MODULE_7__(document).on('click', '.mutliSelect input[type="checkbox"]', function (e) {
            var $container = jquery__WEBPACK_IMPORTED_MODULE_7__(e.target).closest(".dropdownSelect");
            var inputName = jquery__WEBPACK_IMPORTED_MODULE_7__(e.target).attr("name");
        });
        jquery__WEBPACK_IMPORTED_MODULE_7__(document).on('click', '#referenciasLink', function (e) {
            var $container = jquery__WEBPACK_IMPORTED_MODULE_7__(e.target);
            jquery__WEBPACK_IMPORTED_MODULE_7__("#referencias").slideToggle();
            $container.find(".arrow").toggleClass("up down");
        });
    };
    AppComponent.prototype.getStatistics = function () {
        var _this = this;
        this._arboladoService.getTotalPlantados().subscribe(function (data) { _this.auxData = data; }, function (err) { return console.error(err); }, function () {
            _this.auxData.results.features.forEach(function (e) {
                var auxEspecie = e.properties.especie.id;
                if (_this.plantedSpecies.indexOf(auxEspecie) == -1) {
                    _this.plantedSpecies.push(auxEspecie);
                }
            });
            _this.plantedTrees = _this.auxData.count;
            _this.plantedTresSpecies = _this.plantedSpecies.length;
        });
        this._arboladoService.getTotalArboles().subscribe(function (data) { _this.auxData = data; }, function (err) { return console.error(err); }, function () {
            _this.totalTrees = _this.auxData.count;
        });
        this._arboladoService.getTotalAusentes().subscribe(function (data) { _this.auxData = data; }, function (err) { return console.error(err); }, function () {
            _this.emptyTrees = _this.auxData.count;
        });
    };
    AppComponent.prototype.selectedArrayFromIDNames = function (array, selectedArray) {
        var selectedNames = [];
        array.forEach(function (e) {
            if (selectedArray.indexOf(e.id.toString()) != -1) {
                selectedNames.push(e);
            }
        });
        return selectedNames;
    };
    AppComponent.prototype.selectedArrayFromTextNames = function (array, selectedArray) {
        var selectedNames = [];
        array.forEach(function (e) {
            if (selectedArray.indexOf(e.name) != -1) {
                selectedNames.push(e);
            }
        });
        return selectedNames;
    };
    AppComponent.prototype.selectAllEspecies = function () {
        var _this = this;
        var checked = true;
        if (this.selectedSpecies.length == this.species.length) {
            checked = false;
        }
        this.selectedSpecies = [];
        this.species.forEach(function (e) {
            e.checked = checked;
            if (checked) {
                _this.selectedSpecies.push(e.id.toString());
            }
        });
    };
    AppComponent.prototype.selectAllBarrios = function () {
        var _this = this;
        var checked = true;
        if (this.selectedBarrio.length == this.barrioFilter.length) {
            checked = false;
        }
        this.selectedBarrio = [];
        this.barrioFilter.forEach(function (e) {
            e.checked = checked;
            if (checked) {
                _this.selectedBarrio.push(e.id.toString());
            }
        });
    };
    AppComponent.prototype.checkSpecie = function (value) {
        if (this.selectedSpecies.some(function (x) { return x == value.toString(); })) {
            this.checkinModel(this.species, value.toString(), false);
            this.selectedSpecies = this.selectedSpecies.filter(function (item) { return item !== value.toString(); });
        }
        else {
            this.selectedSpecies.push(value.toString());
            this.checkinModel(this.species, value.toString(), true);
        }
    };
    AppComponent.prototype.checkinModel = function (array, value, checked) {
        array.forEach(function (e) {
            if (e.id == value) {
                e.checked = checked;
                return false;
            }
        });
    };
    AppComponent.prototype.checkBarrio = function (value) {
        if (this.selectedBarrio.some(function (x) { return x == value.toString(); })) {
            this.checkinModel(this.barrioFilter, value.toString(), false);
            this.selectedBarrio = this.selectedBarrio.filter(function (item) { return item !== value.toString(); });
        }
        else {
            this.selectedBarrio.push(value.toString());
            this.checkinModel(this.barrioFilter, value.toString(), true);
        }
    };
    AppComponent.prototype.checkCazuelas = function (value) {
        if (this.selectedCazuelas.some(function (x) { return x == value.toString(); })) {
            this.checkinModel(this.cazuelasFilter, value.toString(), false);
            this.selectedCazuelas = this.selectedCazuelas.filter(function (item) { return item !== value.toString(); });
        }
        else {
            this.selectedCazuelas.push(value.toString());
            this.checkinModel(this.cazuelasFilter, value.toString(), true);
        }
    };
    AppComponent.prototype.checkEstadoFito = function (value) {
        if (this.selectedEstadoFitosanitario.some(function (x) { return x == value.toString(); })) {
            this.selectedEstadoFitosanitario = this.selectedEstadoFitosanitario.filter(function (item) { return item !== value.toString(); });
            this.checkinModel(this.estadoFitosanitarioFilter, value.toString(), false);
        }
        else {
            this.selectedEstadoFitosanitario.push(value.toString());
            this.checkinModel(this.estadoFitosanitarioFilter, value.toString(), true);
        }
    };
    AppComponent.prototype.checkInclinacion = function (value) {
        if (this.selectedInclinacion.some(function (x) { return x == value.toString(); })) {
            this.selectedInclinacion = this.selectedInclinacion.filter(function (item) { return item !== value.toString(); });
            this.checkinModel(this.inclinacionFilter, value.toString(), false);
        }
        else {
            this.selectedInclinacion.push(value.toString());
            this.checkinModel(this.inclinacionFilter, value.toString(), true);
        }
    };
    AppComponent.prototype.checkDescalzado = function (value) {
        if (this.selectedDescalzado.some(function (x) { return x == value.toString(); })) {
            this.selectedDescalzado = this.selectedDescalzado.filter(function (item) { return item !== value.toString(); });
            this.checkinModel(this.descalzadoFilter, value.toString(), false);
        }
        else {
            this.selectedDescalzado.push(value.toString());
            this.checkinModel(this.descalzadoFilter, value.toString(), true);
        }
    };
    AppComponent.prototype.checkFuste = function (value) {
        if (this.selectedFustePartido.some(function (x) { return x == value.toString(); })) {
            this.selectedFustePartido = this.selectedFustePartido.filter(function (item) { return item !== value.toString(); });
            this.checkinModel(this.fustePartidoFilter, value.toString(), false);
        }
        else {
            this.selectedFustePartido.push(value.toString());
            this.checkinModel(this.fustePartidoFilter, value.toString(), true);
        }
    };
    AppComponent.prototype.checkTipoOrden = function (value) {
        if (this.selectedTipoOrden.some(function (x) { return x == value.toString(); })) {
            this.selectedTipoOrden = this.selectedTipoOrden.filter(function (item) { return item !== value.toString(); });
            this.checkinModel(this.tipoOrdenFilter, value.toString(), false);
        }
        else {
            this.selectedTipoOrden.push(value.toString());
            this.checkinModel(this.tipoOrdenFilter, value.toString(), true);
        }
    };
    AppComponent.prototype.getArbolesFromEmpty = function () {
        var _this = this;
        this.trees = [];
        if (localStorage.speciesData == null || localStorage.speciesData == "null" || localStorage.speciesData == "undefined" || localStorage.userDate != this.today) {
            this._arboladoService.getSpecies().subscribe(function (data) { _this.especies = data; }, function (err) { return console.error(err); }, function () {
                // console.log('done loading species', this.especies)
                _this.especies.results.forEach(function (e) {
                    var auxEspecie = new _arbol__WEBPACK_IMPORTED_MODULE_4__["Especie"](e.id, e.nombre, (_this.selectedSpecies.indexOf(e.id.toString()) != -1), null);
                    _this.species.push(auxEspecie);
                });
                localStorage.speciesData = JSON.stringify(_this.species);
            });
            this.getBarrios();
        }
        else {
            this.species = JSON.parse(localStorage.speciesData);
            this.selectedSpecies.forEach(function (e) {
                _this.checkinModel(_this.species, e, true);
            });
            this.getBarrios();
        }
    };
    AppComponent.prototype.isSelectedSpecies = function (id) {
        if (this.selectedSpecies.indexOf(id.toString()) != -1) {
            return true;
        }
        return false;
    };
    AppComponent.prototype.getBarrios = function () {
        var _this = this;
        if (localStorage.barriosData == null || localStorage.barriosData == "null" || localStorage.barriosData == "undefined" || localStorage.userDate != this.today) {
            this._arboladoService.getBarrios().subscribe(function (data) { _this.barrios = data; }, function (err) { return console.error(err); }, function () {
                _this.barrios.results.features.forEach(function (e) {
                    var auxBarrio = new _arbol__WEBPACK_IMPORTED_MODULE_4__["FiltroStandard"](e.id, e.properties.nombre, (_this.selectedBarrio.indexOf(e.properties.nombre) != -1));
                    _this.barrioFilter.push(auxBarrio);
                });
                localStorage.barriosData = JSON.stringify(_this.barrioFilter);
            });
            localStorage.userDate = this.today;
        }
        else {
            this.barrioFilter = JSON.parse(localStorage.barriosData);
            this.selectedBarrio.forEach(function (e) {
                _this.checkinModel(_this.barrioFilter, e, true);
            });
        }
    };
    AppComponent.prototype.speciesExists = function (id) {
        var _this = this;
        this.exists = false;
        this.species.forEach(function (e) {
            if (e.id == id) {
                _this.exists = true;
            }
        });
        return this.exists;
    };
    AppComponent.prototype.filterTrees = function () {
        this.ngUnsubscribe[this.currentCall].next();
        this.ngUnsubscribe[this.currentCall].complete();
        this.currentCall++;
        // console.log("change" this.currentCall);
        this.ngUnsubscribe.push(new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]());
        var speciesToSend = (this.selectedSpecies.length == this.species.length ? [] : this.selectedSpecies);
        var barriosToSend = (this.selectedBarrio.length == this.barrioFilter.length ? [] : this.selectedBarrio);
        this.currentBarrio = barriosToSend;
        this.currentSpecies = speciesToSend;
        this.currentTipoOrden = this.selectedTipoOrden;
        this.currentCazuelas = this.selectedCazuelas;
        this.currentEstadoFitosanitario = this.selectedEstadoFitosanitario;
        this.currentInclinacion = this.selectedInclinacion;
        this.currentDescalzado = this.selectedDescalzado;
        this.currentFustePartido = this.selectedFustePartido;
        this.currentTipoOrden = this.selectedTipoOrden;
        this.getTrees();
    };
    AppComponent.prototype.getTrees = function () {
        this.getArboles(this.currentCall, this.currentSpecies, this.currentBarrio, this.currentCazuelas, this.currentEstadoFitosanitario, this.currentInclinacion, this.currentDescalzado, this.currentFustePartido, this.currentTipoOrden, this.currentPrecision);
    };
    AppComponent.prototype.circleClick = function (event) {
        this.selectedTree = this.getTreeInfo(event);
        setTimeout(function () {
            jquery__WEBPACK_IMPORTED_MODULE_7__('html, body').animate({
                scrollTop: jquery__WEBPACK_IMPORTED_MODULE_7__(".treeFile").offset().top - 165
            }, 1000);
        }, 250);
    };
    AppComponent.prototype.getTreeInfo = function (id) {
        var _this = this;
        this.auxTree = null;
        this.trees.forEach(function (e) {
            if (e.id == id) {
                _this.auxTree = e;
                return false;
            }
        });
        return this.auxTree;
    };
    AppComponent.prototype.getArboles = function (callnum, especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden, precision) {
        this.trees = [];
        this.nroPagActual = 0;
        this.clusters = [];
        this.getArbolesWFilter(callnum, especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden, precision);
    };
    AppComponent.prototype.getArbolesWFilter = function (callnum, especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden, precision, next) {
        var _this = this;
        this._arboladoService.getCantArboles(especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden).subscribe(function (data) { _this.cantTotAux = data; }, function (err) { return console.error(err); }, function () {
            _this.cantTotalArboles = _this.cantTotAux.count;
        });
        this.cantArboles = 0;
        this.markerCluster.RemoveClusters();
        if (precision <= 8) {
            this.totalPaginas = 0;
            this.markerCluster.clearMarkers();
            this._arboladoService.getArboles(this.currentBounds, especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden, precision, next).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.ngUnsubscribe[callnum])).subscribe(function (data) { _this.clustersres = data; }, function (err) { return console.error(err); }, function () {
                // console.log('done loading trees', this.clustersres);
                _this.clustersres.results.features.forEach(function (e) {
                    _this.cantArboles += e.properties.cluster_count;
                    _this.clusters.push(new _arbol__WEBPACK_IMPORTED_MODULE_4__["Cluster"](e.geometry.coordinates[1], e.geometry.coordinates[0], e.properties.cluster_count, (Math.pow(2, (21 - _this.zoom)) * 1128.497220 * 0.0005)));
                });
                if (_this.clustersres.next != null) {
                    _this.getArbolesWFilter(callnum, especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden, precision, _this.clustersres.next);
                }
                else {
                    _this.clusters.forEach(function (e) {
                        console.log(e);
                        _this.markerCluster.AddCluster(e.lat, e.lng, e.count);
                    });
                }
            });
        }
        else {
            this._arboladoService.getArboles(this.currentBounds, especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden, precision, next).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.ngUnsubscribe[callnum])).subscribe(function (data) { _this.arboles = data; }, function (err) { return console.error(err); }, function () {
                // console.log('done loading trees', this.arboles);
                _this.cantArboles = _this.arboles.count;
                var redondeado = Math.round(_this.cantArboles / _this.globales.pageSize);
                _this.totalPaginas = (_this.cantArboles / _this.globales.pageSize > redondeado ? redondeado + 1 : redondeado);
                _this.cantArboles += _this.arboles.count;
                _this.arboles.results.features.forEach(function (e) {
                    var auxType = (e.properties.especie.nombre.toLowerCase() == "ausente" ? "falta" : (e.properties.tipos_de_orden.toLowerCase() == "plantación" ? "plantacion" : "normal"));
                    var auxEspecie = new _arbol__WEBPACK_IMPORTED_MODULE_4__["Especie"](e.properties.especie.id, e.properties.especie.nombre, (_this.selectedSpecies.indexOf(e.id.toString()) != -1), _this.treeTypeColors[auxType], _this.treeTypeStrokeColors[auxType]);
                    var auxFoto = new _arbol__WEBPACK_IMPORTED_MODULE_4__["Foto"](e.properties.foto.original, e.properties.foto.thumbnail_500);
                    var auxProperties = new _arbol__WEBPACK_IMPORTED_MODULE_4__["Properties"](e.properties.barrio, e.properties.cazuela, auxEspecie, auxFoto, e.properties.problema_descalzado, e.properties.problema_fuste_partido, e.properties.tipos_de_orden, e.properties.estado_fitosanitario, e.properties.inclinacion);
                    _this.trees.push(new _arbol__WEBPACK_IMPORTED_MODULE_4__["Tree"](e.id, e.geometry.coordinates[1], e.geometry.coordinates[0], auxProperties, (Math.pow(2, (21 - _this.zoom)) * 1128.497220 * 0.00035)));
                });
                _this.nroPagActual++;
                if (_this.arboles.next != null) {
                    _this.getArbolesWFilter(callnum, especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden, precision, _this.arboles.next);
                }
                else {
                    _this.nroPagActual = 0;
                    _this.totalPaginas = 0;
                }
                _this.shownTrees = _this.trees;
            });
        }
    };
    AppComponent.prototype.resetSelectedTree = function () {
        this.selectedTree = null;
        jquery__WEBPACK_IMPORTED_MODULE_7__('html, body').animate({
            scrollTop: jquery__WEBPACK_IMPORTED_MODULE_7__(".mapCont").offset().top - 165
        }, 1000);
    };
    AppComponent.prototype.zoomChange = function (zoom) {
        if (this.zoom > zoom) {
            this.currentPrecision -= .4;
        }
        else if (this.zoom < zoom) {
            this.currentPrecision += .4;
        }
        switch (zoom) {
            case 13:
                this.currentPrecision = 6;
                break;
            case 14:
                this.currentPrecision = 6;
                break;
            case 15:
                this.currentPrecision = 6;
                break;
            case 16:
                this.currentPrecision = 7;
                break;
            case 17:
                this.currentPrecision = 8;
                break;
        }
        this.trees.forEach(function (e) {
            if (zoom > 1 && zoom < 19) {
                e.radius = Math.pow(2, (21 - zoom)) * 1128.497220 * 0.00035;
            }
        });
        this.clusters.forEach(function (e) {
            if (zoom > 1 && zoom < 19) {
                e.radius = Math.pow(2, (21 - zoom)) * 1128.497220 * 0.0008;
            }
        });
        this.zoom = zoom;
    };
    AppComponent.prototype.boundsChange = function (bounds) {
        //this.currentBounds = new _arbol__WEBPACK_IMPORTED_MODULE_4__["MapBounds"](bounds.j.j, bounds.j.l, bounds.l.j, bounds.l.l);
        this.currentBounds = new _arbol__WEBPACK_IMPORTED_MODULE_4__["MapBounds"](bounds.ga.j, bounds.ga.l, bounds.na.j, bounds.na.l);
    };
    AppComponent.prototype.setBoundsData = function (bounds) {
        this.ngUnsubscribe[this.currentCall].next();
        this.ngUnsubscribe[this.currentCall].complete();
        this.currentCall++;
        // console.log("change" this.currentCall);
        this.ngUnsubscribe.push(new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]());
        this.getTrees();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AppComponent.prototype, "locations", void 0);
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_arbolado_service__WEBPACK_IMPORTED_MODULE_3__["ArboladoService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _arbolado_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./arbolado.service */ "./src/app/arbolado.service.ts");
/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./globals */ "./src/app/globals.ts");
/* harmony import */ var _angular_common_locales_es__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/locales/es */ "./node_modules/@angular/common/locales/es.js");
/* harmony import */ var _angular_common_locales_es__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_es__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _map_content_map_content_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./map-content/map-content.component */ "./src/app/map-content/map-content.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






 // replaces previous Http service





Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["registerLocaleData"])(_angular_common_locales_es__WEBPACK_IMPORTED_MODULE_9___default.a, 'es');
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_5__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyAYw1rqa9mL1-__v8h2CVgsRJmpmP2mP1s' // AIzaSyAYw1rqa9mL1-__v8h2CVgsRJmpmP2mP1s
                }),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"]
            ],
            providers: [_arbolado_service__WEBPACK_IMPORTED_MODULE_7__["ArboladoService"], _globals__WEBPACK_IMPORTED_MODULE_8__["Globals"], { provide: _angular_core__WEBPACK_IMPORTED_MODULE_1__["LOCALE_ID"], useValue: 'es' }],
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"], _map_content_map_content_component__WEBPACK_IMPORTED_MODULE_10__["MapContentComponent"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/arbol.ts":
/*!**************************!*\
  !*** ./src/app/arbol.ts ***!
  \**************************/
/*! exports provided: Especie, Foto, FiltroStandard, Properties, Cluster, Tree, MapBounds */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Especie", function() { return Especie; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Foto", function() { return Foto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltroStandard", function() { return FiltroStandard; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Properties", function() { return Properties; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Cluster", function() { return Cluster; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tree", function() { return Tree; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapBounds", function() { return MapBounds; });
var Especie = /** @class */ (function () {
    function Especie(id, nombre, checked, color, strokeColor) {
        this.id = id;
        this.name = nombre;
        if (!color) {
            this.color = "#006400";
        }
        else {
            this.color = color;
        }
        if (!strokeColor) {
            this.strokeColor = "#006400";
        }
        else {
            this.strokeColor = color;
        }
        this.checked = checked;
    }
    return Especie;
}());

var Foto = /** @class */ (function () {
    function Foto(original, thumbnail) {
        this.original = original;
        this.thumbnail = thumbnail;
    }
    return Foto;
}());

var FiltroStandard = /** @class */ (function () {
    function FiltroStandard(id, nombre, checked) {
        this.id = id.toString();
        this.name = nombre;
        this.checked = checked;
    }
    return FiltroStandard;
}());

var Properties = /** @class */ (function () {
    function Properties(barrio, cazuela, especie, foto, descalzado, fustePartido, tipoOrden, estadoFitosanitario, inclinacion) {
        this.barrio = barrio;
        this.cazuela = cazuela;
        this.especie = especie;
        this.foto = foto;
        this.tipoOrden = tipoOrden;
        this.descalzado = (descalzado == true ? "si" : "no");
        this.fustePartido = (fustePartido == true ? "si" : "no");
        if (estadoFitosanitario) {
            this.estadoFitosanitario = estadoFitosanitario;
        }
        if (inclinacion) {
            this.inclinacion = inclinacion;
        }
    }
    return Properties;
}());

var Cluster = /** @class */ (function () {
    function Cluster(lat, lng, count, radius) {
        this.lat = lat;
        this.lng = lng;
        this.count = count;
        if (!radius) {
            this.radius = 10;
        }
        this.radius = radius;
    }
    return Cluster;
}());

var Tree = /** @class */ (function () {
    function Tree(id, lat, lng, properties, radius) {
        if (!radius) {
            this.radius = 10;
        }
        this.id = id;
        this.lat = lat;
        this.lng = lng;
        this.radius = radius;
        this.properties = properties;
    }
    return Tree;
}());

var MapBounds = /** @class */ (function () {
    function MapBounds(top, bottom, left, right) {
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
    }
    return MapBounds;
}());



/***/ }),

/***/ "./src/app/arbolado.service.ts":
/*!*************************************!*\
  !*** ./src/app/arbolado.service.ts ***!
  \*************************************/
/*! exports provided: ArboladoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArboladoService", function() { return ArboladoService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./globals */ "./src/app/globals.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
var ArboladoService = /** @class */ (function () {
    function ArboladoService(http) {
        this.http = http;
        this.strQueries = "";
        this.globales = new _globals__WEBPACK_IMPORTED_MODULE_2__["Globals"]();
    }
    // Uses http.get() to load data from a single API endpoint
    ArboladoService.prototype.getArboles = function (bounds, especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden, precision, next) {
        this.strQueries = "";
        if (next != null) {
            return this.http.get(next);
        }
        else {
            if (especie && especie.length > 0) {
                this.strQueries += "&especie_ids=" + especie.toString();
            }
            if (cazuela && cazuela.length > 0) {
                this.strQueries += "&cazuela=" + cazuela.toString();
            }
            if (barrio && barrio.length > 0) {
                this.strQueries += "&barrio_nombre=" + barrio.toString();
            }
            if (estado_fito && estado_fito.length > 0) {
                this.strQueries += "&fitosanitario=" + estado_fito.toString();
            }
            if (inclinacion && inclinacion.length > 0) {
                this.strQueries += "&inclinacion=" + inclinacion.toString();
            }
            if (descalzado && descalzado.length > 0) {
                this.strQueries += "&descalzado=" + descalzado.toString();
            }
            if (fustePartido && fustePartido.length > 0) {
                this.strQueries += "&fuste_partido=" + fustePartido.toString();
            }
            if (tipoOrden && tipoOrden.length > 0) {
                this.strQueries += "&tipo_orden=" + tipoOrden.toString();
            }
            if (precision <= 8) {
                this.strQueries += "&precision=" + Math.round(precision).toString();
            }
            this.strQueries += "&in_bbox=" + bounds.bottom + "," + bounds.left + "," + bounds.top + "," + bounds.right;
            // console.log('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/arboles/?page_size='+this.globales.pageSize+this.strQueries);
            return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/arboles/?page_size=' + this.globales.pageSize + this.strQueries);
        }
    };
    ArboladoService.prototype.getCantArboles = function (especie, barrio, cazuela, estado_fito, inclinacion, descalzado, fustePartido, tipoOrden) {
        this.strQueries = "";
        if (especie && especie.length > 0) {
            this.strQueries += "&especie_ids=" + especie.toString();
        }
        if (cazuela && cazuela.length > 0) {
            this.strQueries += "&cazuela=" + cazuela.toString();
        }
        if (barrio && barrio.length > 0) {
            this.strQueries += "&barrio_nombre=" + barrio.toString();
        }
        if (estado_fito && estado_fito.length > 0) {
            this.strQueries += "&fitosanitario=" + estado_fito.toString();
        }
        if (inclinacion && inclinacion.length > 0) {
            this.strQueries += "&inclinacion=" + inclinacion.toString();
        }
        if (descalzado && descalzado.length > 0) {
            this.strQueries += "&descalzado=" + descalzado.toString();
        }
        if (fustePartido && fustePartido.length > 0) {
            this.strQueries += "&fuste_partido=" + fustePartido.toString();
        }
        if (tipoOrden && tipoOrden.length > 0) {
            this.strQueries += "&tipo_orden=" + tipoOrden.toString();
        }
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/contador-arboles/?page_size=' + this.globales.pageSize + this.strQueries);
    };
    ArboladoService.prototype.getSpecies = function () {
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/especies/?page_size=100');
    };
    ArboladoService.prototype.getBarrios = function () {
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/barrios/barrios/?page_size=470');
    };
    ArboladoService.prototype.getTotalArboles = function () {
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/contador-arboles');
    };
    ArboladoService.prototype.getTotalAusentes = function () {
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/contador-arboles/?especie_ids=2');
    };
    ArboladoService.prototype.getTotalPlantados = function () {
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/arboles/?tipo_orden=200');
    };
    ArboladoService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ArboladoService);
    return ArboladoService;
}());



/***/ }),

/***/ "./src/app/globals.ts":
/*!****************************!*\
  !*** ./src/app/globals.ts ***!
  \****************************/
/*! exports provided: Globals */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Globals", function() { return Globals; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// globals.ts

var Globals = /** @class */ (function () {
    function Globals() {
        this.pageSize = 100;
    }
    Globals = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], Globals);
    return Globals;
}());



/***/ }),

/***/ "./src/app/map-content/map-content.component.ts":
/*!******************************************************!*\
  !*** ./src/app/map-content/map-content.component.ts ***!
  \******************************************************/
/*! exports provided: MapContentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapContentComponent", function() { return MapContentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MapContentComponent = /** @class */ (function () {
    function MapContentComponent(gMaps) {
        this.gMaps = gMaps;
        this.onMapLoad = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    MapContentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.gMaps.getNativeMap().then(function (map) {
            _this.onMapLoad.emit(map);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], MapContentComponent.prototype, "onMapLoad", void 0);
    MapContentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'core-map-content',
            template: '',
        }),
        __metadata("design:paramtypes", [_agm_core__WEBPACK_IMPORTED_MODULE_1__["GoogleMapsAPIWrapper"]])
    ], MapContentComponent);
    return MapContentComponent;
}());



/***/ }),

/***/ "./src/assets/markerclusterer.js":
/*!***************************************!*\
  !*** ./src/assets/markerclusterer.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// ==ClosureCompiler==
// @compilation_level ADVANCED_OPTIMIZATIONS
// @externs_url https://raw.githubusercontent.com/google/closure-compiler/master/contrib/externs/maps/google_maps_api_v3.js
// ==/ClosureCompiler==

/**
 * @name MarkerClusterer for Google Maps v3
 * @version version 1.0
 * @author Luke Mahe
 * @fileoverview
 * The library creates and manages per-zoom-level clusters for large amounts of
 * markers.
 * <br/>
 * This is a v3 implementation of the
 * <a href="http://gmaps-utility-library-dev.googlecode.com/svn/tags/markerclusterer/"
 * >v2 MarkerClusterer</a>.
 */

/**
 * @license
 * Copyright 2010 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * A Marker Clusterer that clusters markers.
 *
 * @param {google.maps.Map} map The Google map to attach to.
 * @param {Array.<google.maps.Marker>=} opt_markers Optional markers to add to
 *   the cluster.
 * @param {Object=} opt_options support the following options:
 *     'gridSize': (number) The grid size of a cluster in pixels.
 *     'maxZoom': (number) The maximum zoom level that a marker can be part of a
 *                cluster.
 *     'zoomOnClick': (boolean) Whether the default behaviour of clicking on a
 *                    cluster is to zoom into it.
 *     'averageCenter': (boolean) Whether the center of each cluster should be
 *                      the average of all markers in the cluster.
 *     'minimumClusterSize': (number) The minimum number of markers to be in a
 *                           cluster before the markers are hidden and a count
 *                           is shown.
 *     'styles': (object) An object that has style properties:
 *       'url': (string) The image url.
 *       'height': (number) The image height.
 *       'width': (number) The image width.
 *       'anchor': (Array) The anchor position of the label text.
 *       'textColor': (string) The text color.
 *       'textSize': (number) The text size.
 *       'backgroundPosition': (string) The position of the backgound x, y.
 *       'iconAnchor': (Array) The anchor position of the icon x, y.
 * @constructor
 * @extends google.maps.OverlayView
 */
function MarkerClusterer(map, opt_markers, opt_options) {
  // MarkerClusterer implements google.maps.OverlayView interface. We use the
  // extend function to extend MarkerClusterer with google.maps.OverlayView
  // because it might not always be available when the code is defined so we
  // look for it at the last possible moment. If it doesn't exist now then
  // there is no point going ahead :)
  this.extend(MarkerClusterer, google.maps.OverlayView);
  this.map_ = map;

  /**
   * @type {Array.<google.maps.Marker>}
   * @private
   */
  this.markers_ = [];

  /**
   *  @type {Array.<Cluster>}
   */
  this.clusters_ = [];

  this.sizes = [53, 56, 66, 78, 90];

  /**
   * @private
   */
  this.styles_ = [];

  /**
   * @type {boolean}
   * @private
   */
  this.ready_ = false;

  var options = opt_options || {};

  /**
   * @type {number}
   * @private
   */
  this.gridSize_ = options['gridSize'] || 60;

  /**
   * @private
   */
  this.minClusterSize_ = options['minimumClusterSize'] || 2;


  /**
   * @type {?number}
   * @private
   */
  this.maxZoom_ = options['maxZoom'] || null;

  this.styles_ = options['styles'] || [];

  /**
   * @type {string}
   * @private
   */
  this.imagePath_ = options['imagePath'] ||
      this.MARKER_CLUSTER_IMAGE_PATH_;

  /**
   * @type {string}
   * @private
   */
  this.imageExtension_ = options['imageExtension'] ||
      this.MARKER_CLUSTER_IMAGE_EXTENSION_;

  /**
   * @type {boolean}
   * @private
   */
  this.zoomOnClick_ = true;

  if (options['zoomOnClick'] != undefined) {
    this.zoomOnClick_ = options['zoomOnClick'];
  }

  /**
   * @type {boolean}
   * @private
   */
  this.averageCenter_ = false;

  if (options['averageCenter'] != undefined) {
    this.averageCenter_ = options['averageCenter'];
  }

  this.setupStyles_();

  this.setMap(map);

  /**
   * @type {number}
   * @private
   */
  this.prevZoom_ = this.map_.getZoom();

  // Add the map event listeners
  var that = this;
  google.maps.event.addListener(this.map_, 'zoom_changed', function() {
    var zoom = that.map_.getZoom();

    if (that.prevZoom_ != zoom) {
      that.prevZoom_ = zoom;
      that.resetViewport();
    }
  });

  google.maps.event.addListener(this.map_, 'idle', function() {
    that.redraw();
  });

  // Finally, add the markers
  if (opt_markers && opt_markers.length) {
    this.addMarkers(opt_markers, false);
  }
}


/**
 * The marker cluster image path.
 *
 * @type {string}
 * @private
 */
MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_PATH_ = '../images/m';


/**
 * The marker cluster image path.
 *
 * @type {string}
 * @private
 */
MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_EXTENSION_ = 'png';


/**
 * Extends a objects prototype by anothers.
 *
 * @param {Object} obj1 The object to be extended.
 * @param {Object} obj2 The object to extend with.
 * @return {Object} The new extended object.
 * @ignore
 */
MarkerClusterer.prototype.extend = function(obj1, obj2) {
  return (function(object) {
    for (var property in object.prototype) {
      this.prototype[property] = object.prototype[property];
    }
    return this;
  }).apply(obj1, [obj2]);
};

MarkerClusterer.prototype.AddCluster = function(clat, clng, csize)
{
  if (typeof this.aAddClusterIcons == "undefined"){
      this.aAddClusterIcons = [];
  }

  var clusterlocation = new google.maps.LatLng(clat, clng)
  var CI = new ClusterIcon(new Cluster(this), this.getStyles, this.getGridSize());
  var index = 0;
  var dv = csize;
  while (dv !== 0) {
    dv = parseInt(dv / 10, 10);
    index++;
  }
  var style = this.styles_[index-1];
  CI.setCenter(clusterlocation);
  $.extend(CI, {sums_ : {text : csize, index: index}, url_ : style['url'], width_ : style['width'], height_ : style['height']});
  CI.setMap(this.map_);
  CI.show();
  CI.triggerClusterClick = function(){
    this.map_.setCenter(clusterlocation);
   this.map_.setZoom(this.map_.getZoom()+1);
 }
 this.aAddClusterIcons.push(CI);
}


MarkerClusterer.prototype.RemoveClusters = function()
    {
        if (typeof this.aAddClusterIcons == "undefined"){
            this.aAddClusterIcons = [];
        }

        $.each(this.aAddClusterIcons, function(iIndex, oObj){
            oObj.onRemove();
        });
        this.aAddClusterIcons = [];
    }

/**
 * Implementaion of the interface method.
 * @ignore
 */
MarkerClusterer.prototype.onAdd = function() {
  this.setReady_(true);
};

/**
 * Implementaion of the interface method.
 * @ignore
 */
MarkerClusterer.prototype.draw = function() {};

/**
 * Sets up the styles object.
 *
 * @private
 */
MarkerClusterer.prototype.setupStyles_ = function() {
  if (this.styles_.length) {
    return;
  }

  for (var i = 0, size; size = this.sizes[i]; i++) {
    this.styles_.push({
      url: this.imagePath_ + (i + 1) + '.' + this.imageExtension_,
      height: size,
      width: size
    });
  }
};

/**
 *  Fit the map to the bounds of the markers in the clusterer.
 */
MarkerClusterer.prototype.fitMapToMarkers = function() {
  var markers = this.getMarkers();
  var bounds = new google.maps.LatLngBounds();
  for (var i = 0, marker; marker = markers[i]; i++) {
    bounds.extend(marker.getPosition());
  }

  this.map_.fitBounds(bounds);
};


/**
 *  Sets the styles.
 *
 *  @param {Object} styles The style to set.
 */
MarkerClusterer.prototype.setStyles = function(styles) {
  this.styles_ = styles;
};


/**
 *  Gets the styles.
 *
 *  @return {Object} The styles object.
 */
MarkerClusterer.prototype.getStyles = function() {
  return this.styles_;
};


/**
 * Whether zoom on click is set.
 *
 * @return {boolean} True if zoomOnClick_ is set.
 */
MarkerClusterer.prototype.isZoomOnClick = function() {
  return this.zoomOnClick_;
};

/**
 * Whether average center is set.
 *
 * @return {boolean} True if averageCenter_ is set.
 */
MarkerClusterer.prototype.isAverageCenter = function() {
  return this.averageCenter_;
};


/**
 *  Returns the array of markers in the clusterer.
 *
 *  @return {Array.<google.maps.Marker>} The markers.
 */
MarkerClusterer.prototype.getMarkers = function() {
  return this.markers_;
};


/**
 *  Returns the number of markers in the clusterer
 *
 *  @return {Number} The number of markers.
 */
MarkerClusterer.prototype.getTotalMarkers = function() {
  return this.markers_.length;
};


/**
 *  Sets the max zoom for the clusterer.
 *
 *  @param {number} maxZoom The max zoom level.
 */
MarkerClusterer.prototype.setMaxZoom = function(maxZoom) {
  this.maxZoom_ = maxZoom;
};


/**
 *  Gets the max zoom for the clusterer.
 *
 *  @return {number} The max zoom level.
 */
MarkerClusterer.prototype.getMaxZoom = function() {
  return this.maxZoom_;
};


/**
 *  The function for calculating the cluster icon image.
 *
 *  @param {Array.<google.maps.Marker>} markers The markers in the clusterer.
 *  @param {number} numStyles The number of styles available.
 *  @return {Object} A object properties: 'text' (string) and 'index' (number).
 *  @private
 */
MarkerClusterer.prototype.calculator_ = function(markers, numStyles) {
  var index = 0;
  var count = markers.length;
  var dv = count;
  while (dv !== 0) {
    dv = parseInt(dv / 10, 10);
    index++;
  }

  index = Math.min(index, numStyles);
  return {
    text: count,
    index: index
  };
};


/**
 * Set the calculator function.
 *
 * @param {function(Array, number)} calculator The function to set as the
 *     calculator. The function should return a object properties:
 *     'text' (string) and 'index' (number).
 *
 */
MarkerClusterer.prototype.setCalculator = function(calculator) {
  this.calculator_ = calculator;
};


/**
 * Get the calculator function.
 *
 * @return {function(Array, number)} the calculator function.
 */
MarkerClusterer.prototype.getCalculator = function() {
  return this.calculator_;
};


/**
 * Add an array of markers to the clusterer.
 *
 * @param {Array.<google.maps.Marker>} markers The markers to add.
 * @param {boolean=} opt_nodraw Whether to redraw the clusters.
 */
MarkerClusterer.prototype.addMarkers = function(markers, opt_nodraw) {
  for (var i = 0, marker; marker = markers[i]; i++) {
    this.pushMarkerTo_(marker);
  }
  if (!opt_nodraw) {
    this.redraw();
  }
};


/**
 * Pushes a marker to the clusterer.
 *
 * @param {google.maps.Marker} marker The marker to add.
 * @private
 */
MarkerClusterer.prototype.pushMarkerTo_ = function(marker) {
  marker.isAdded = false;
  if (marker['draggable']) {
    // If the marker is draggable add a listener so we update the clusters on
    // the drag end.
    var that = this;
    google.maps.event.addListener(marker, 'dragend', function() {
      marker.isAdded = false;
      that.repaint();
    });
  }
  this.markers_.push(marker);
};


/**
 * Adds a marker to the clusterer and redraws if needed.
 *
 * @param {google.maps.Marker} marker The marker to add.
 * @param {boolean=} opt_nodraw Whether to redraw the clusters.
 */
MarkerClusterer.prototype.addMarker = function(marker, opt_nodraw) {
  this.pushMarkerTo_(marker);
  if (!opt_nodraw) {
    this.redraw();
  }
};


/**
 * Removes a marker and returns true if removed, false if not
 *
 * @param {google.maps.Marker} marker The marker to remove
 * @return {boolean} Whether the marker was removed or not
 * @private
 */
MarkerClusterer.prototype.removeMarker_ = function(marker) {
  var index = -1;
  if (this.markers_.indexOf) {
    index = this.markers_.indexOf(marker);
  } else {
    for (var i = 0, m; m = this.markers_[i]; i++) {
      if (m == marker) {
        index = i;
        break;
      }
    }
  }

  if (index == -1) {
    // Marker is not in our list of markers.
    return false;
  }

  marker.setMap(null);

  this.markers_.splice(index, 1);

  return true;
};


/**
 * Remove a marker from the cluster.
 *
 * @param {google.maps.Marker} marker The marker to remove.
 * @param {boolean=} opt_nodraw Optional boolean to force no redraw.
 * @return {boolean} True if the marker was removed.
 */
MarkerClusterer.prototype.removeMarker = function(marker, opt_nodraw) {
  var removed = this.removeMarker_(marker);

  if (!opt_nodraw && removed) {
    this.resetViewport();
    this.redraw();
    return true;
  } else {
   return false;
  }
};


/**
 * Removes an array of markers from the cluster.
 *
 * @param {Array.<google.maps.Marker>} markers The markers to remove.
 * @param {boolean=} opt_nodraw Optional boolean to force no redraw.
 */
MarkerClusterer.prototype.removeMarkers = function(markers, opt_nodraw) {
  var removed = false;

  for (var i = 0, marker; marker = markers[i]; i++) {
    var r = this.removeMarker_(marker);
    removed = removed || r;
  }

  if (!opt_nodraw && removed) {
    this.resetViewport();
    this.redraw();
    return true;
  }
};


/**
 * Sets the clusterer's ready state.
 *
 * @param {boolean} ready The state.
 * @private
 */
MarkerClusterer.prototype.setReady_ = function(ready) {
  if (!this.ready_) {
    this.ready_ = ready;
    this.createClusters_();
  }
};


/**
 * Returns the number of clusters in the clusterer.
 *
 * @return {number} The number of clusters.
 */
MarkerClusterer.prototype.getTotalClusters = function() {
  return this.clusters_.length;
};


/**
 * Returns the google map that the clusterer is associated with.
 *
 * @return {google.maps.Map} The map.
 */
MarkerClusterer.prototype.getMap = function() {
  return this.map_;
};


/**
 * Sets the google map that the clusterer is associated with.
 *
 * @param {google.maps.Map} map The map.
 */
MarkerClusterer.prototype.setMap = function(map) {
  this.map_ = map;
};


/**
 * Returns the size of the grid.
 *
 * @return {number} The grid size.
 */
MarkerClusterer.prototype.getGridSize = function() {
  return this.gridSize_;
};


/**
 * Sets the size of the grid.
 *
 * @param {number} size The grid size.
 */
MarkerClusterer.prototype.setGridSize = function(size) {
  this.gridSize_ = size;
};


/**
 * Returns the min cluster size.
 *
 * @return {number} The grid size.
 */
MarkerClusterer.prototype.getMinClusterSize = function() {
  return this.minClusterSize_;
};

/**
 * Sets the min cluster size.
 *
 * @param {number} size The grid size.
 */
MarkerClusterer.prototype.setMinClusterSize = function(size) {
  this.minClusterSize_ = size;
};


/**
 * Extends a bounds object by the grid size.
 *
 * @param {google.maps.LatLngBounds} bounds The bounds to extend.
 * @return {google.maps.LatLngBounds} The extended bounds.
 */
MarkerClusterer.prototype.getExtendedBounds = function(bounds) {
  var projection = this.getProjection();

  // Turn the bounds into latlng.
  var tr = new google.maps.LatLng(bounds.getNorthEast().lat(),
      bounds.getNorthEast().lng());
  var bl = new google.maps.LatLng(bounds.getSouthWest().lat(),
      bounds.getSouthWest().lng());

  // Convert the points to pixels and the extend out by the grid size.
  var trPix = projection.fromLatLngToDivPixel(tr);
  trPix.x += this.gridSize_;
  trPix.y -= this.gridSize_;

  var blPix = projection.fromLatLngToDivPixel(bl);
  blPix.x -= this.gridSize_;
  blPix.y += this.gridSize_;

  // Convert the pixel points back to LatLng
  var ne = projection.fromDivPixelToLatLng(trPix);
  var sw = projection.fromDivPixelToLatLng(blPix);

  // Extend the bounds to contain the new bounds.
  bounds.extend(ne);
  bounds.extend(sw);

  return bounds;
};


/**
 * Determins if a marker is contained in a bounds.
 *
 * @param {google.maps.Marker} marker The marker to check.
 * @param {google.maps.LatLngBounds} bounds The bounds to check against.
 * @return {boolean} True if the marker is in the bounds.
 * @private
 */
MarkerClusterer.prototype.isMarkerInBounds_ = function(marker, bounds) {
  return bounds.contains(marker.getPosition());
};


/**
 * Clears all clusters and markers from the clusterer.
 */
MarkerClusterer.prototype.clearMarkers = function() {
  this.resetViewport(true);

  // Set the markers a empty array.
  this.markers_ = [];
};


/**
 * Clears all existing clusters and recreates them.
 * @param {boolean} opt_hide To also hide the marker.
 */
MarkerClusterer.prototype.resetViewport = function(opt_hide) {
  // Remove all the clusters
  for (var i = 0, cluster; cluster = this.clusters_[i]; i++) {
    cluster.remove();
  }

  // Reset the markers to not be added and to be invisible.
  for (var i = 0, marker; marker = this.markers_[i]; i++) {
    marker.isAdded = false;
    if (opt_hide) {
      marker.setMap(null);
    }
  }

  this.clusters_ = [];
};

/**
 *
 */
MarkerClusterer.prototype.repaint = function() {
  var oldClusters = this.clusters_.slice();
  this.clusters_.length = 0;
  this.resetViewport();
  this.redraw();

  // Remove the old clusters.
  // Do it in a timeout so the other clusters have been drawn first.
  window.setTimeout(function() {
    for (var i = 0, cluster; cluster = oldClusters[i]; i++) {
      cluster.remove();
    }
  }, 0);
};


/**
 * Redraws the clusters.
 */
MarkerClusterer.prototype.redraw = function() {
  this.createClusters_();
};


/**
 * Calculates the distance between two latlng locations in km.
 * @see http://www.movable-type.co.uk/scripts/latlong.html
 *
 * @param {google.maps.LatLng} p1 The first lat lng point.
 * @param {google.maps.LatLng} p2 The second lat lng point.
 * @return {number} The distance between the two points in km.
 * @private
*/
MarkerClusterer.prototype.distanceBetweenPoints_ = function(p1, p2) {
  if (!p1 || !p2) {
    return 0;
  }

  var R = 6371; // Radius of the Earth in km
  var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
  var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d;
};


/**
 * Add a marker to a cluster, or creates a new cluster.
 *
 * @param {google.maps.Marker} marker The marker to add.
 * @private
 */
MarkerClusterer.prototype.addToClosestCluster_ = function(marker) {
  var distance = 40000; // Some large number
  var clusterToAddTo = null;
  var pos = marker.getPosition();
  for (var i = 0, cluster; cluster = this.clusters_[i]; i++) {
    var center = cluster.getCenter();
    if (center) {
      var d = this.distanceBetweenPoints_(center, marker.getPosition());
      if (d < distance) {
        distance = d;
        clusterToAddTo = cluster;
      }
    }
  }

  if (clusterToAddTo && clusterToAddTo.isMarkerInClusterBounds(marker)) {
    clusterToAddTo.addMarker(marker);
  } else {
    var cluster = new Cluster(this);
    cluster.addMarker(marker);
    this.clusters_.push(cluster);
  }
};


/**
 * Creates the clusters.
 *
 * @private
 */
MarkerClusterer.prototype.createClusters_ = function() {
  if (!this.ready_) {
    return;
  }

  // Get our current map view bounds.
  // Create a new bounds object so we don't affect the map.
  var mapBounds = new google.maps.LatLngBounds(this.map_.getBounds().getSouthWest(),
      this.map_.getBounds().getNorthEast());
  var bounds = this.getExtendedBounds(mapBounds);

  for (var i = 0, marker; marker = this.markers_[i]; i++) {
    if (!marker.isAdded && this.isMarkerInBounds_(marker, bounds)) {
      this.addToClosestCluster_(marker);
    }
  }
};


/**
 * A cluster that contains markers.
 *
 * @param {MarkerClusterer} markerClusterer The markerclusterer that this
 *     cluster is associated with.
 * @constructor
 * @ignore
 */
function Cluster(markerClusterer) {
  this.markerClusterer_ = markerClusterer;
  this.map_ = markerClusterer.getMap();
  this.gridSize_ = markerClusterer.getGridSize();
  this.minClusterSize_ = markerClusterer.getMinClusterSize();
  this.averageCenter_ = markerClusterer.isAverageCenter();
  this.center_ = null;
  this.markers_ = [];
  this.bounds_ = null;
  this.clusterIcon_ = new ClusterIcon(this, markerClusterer.getStyles(),
      markerClusterer.getGridSize());
}

/**
 * Determins if a marker is already added to the cluster.
 *
 * @param {google.maps.Marker} marker The marker to check.
 * @return {boolean} True if the marker is already added.
 */
Cluster.prototype.isMarkerAlreadyAdded = function(marker) {
  if (this.markers_.indexOf) {
    return this.markers_.indexOf(marker) != -1;
  } else {
    for (var i = 0, m; m = this.markers_[i]; i++) {
      if (m == marker) {
        return true;
      }
    }
  }
  return false;
};


/**
 * Add a marker the cluster.
 *
 * @param {google.maps.Marker} marker The marker to add.
 * @return {boolean} True if the marker was added.
 */
Cluster.prototype.addMarker = function(marker) {
  if (this.isMarkerAlreadyAdded(marker)) {
    return false;
  }

  if (!this.center_) {
    this.center_ = marker.getPosition();
    this.calculateBounds_();
  } else {
    if (this.averageCenter_) {
      var l = this.markers_.length + 1;
      var lat = (this.center_.lat() * (l-1) + marker.getPosition().lat()) / l;
      var lng = (this.center_.lng() * (l-1) + marker.getPosition().lng()) / l;
      this.center_ = new google.maps.LatLng(lat, lng);
      this.calculateBounds_();
    }
  }

  marker.isAdded = true;
  this.markers_.push(marker);

  var len = this.markers_.length;
  if (len < this.minClusterSize_ && marker.getMap() != this.map_) {
    // Min cluster size not reached so show the marker.
    marker.setMap(this.map_);
  }

  if (len == this.minClusterSize_) {
    // Hide the markers that were showing.
    for (var i = 0; i < len; i++) {
      this.markers_[i].setMap(null);
    }
  }

  if (len >= this.minClusterSize_) {
    marker.setMap(null);
  }

  this.updateIcon();
  return true;
};


/**
 * Returns the marker clusterer that the cluster is associated with.
 *
 * @return {MarkerClusterer} The associated marker clusterer.
 */
Cluster.prototype.getMarkerClusterer = function() {
  return this.markerClusterer_;
};


/**
 * Returns the bounds of the cluster.
 *
 * @return {google.maps.LatLngBounds} the cluster bounds.
 */
Cluster.prototype.getBounds = function() {
  var bounds = new google.maps.LatLngBounds(this.center_, this.center_);
  var markers = this.getMarkers();
  for (var i = 0, marker; marker = markers[i]; i++) {
    bounds.extend(marker.getPosition());
  }
  return bounds;
};


/**
 * Removes the cluster
 */
Cluster.prototype.remove = function() {
  this.clusterIcon_.remove();
  this.markers_.length = 0;
  delete this.markers_;
};


/**
 * Returns the center of the cluster.
 *
 * @return {number} The cluster center.
 */
Cluster.prototype.getSize = function() {
  return this.markers_.length;
};


/**
 * Returns the center of the cluster.
 *
 * @return {Array.<google.maps.Marker>} The cluster center.
 */
Cluster.prototype.getMarkers = function() {
  return this.markers_;
};


/**
 * Returns the center of the cluster.
 *
 * @return {google.maps.LatLng} The cluster center.
 */
Cluster.prototype.getCenter = function() {
  return this.center_;
};


/**
 * Calculated the extended bounds of the cluster with the grid.
 *
 * @private
 */
Cluster.prototype.calculateBounds_ = function() {
  var bounds = new google.maps.LatLngBounds(this.center_, this.center_);
  this.bounds_ = this.markerClusterer_.getExtendedBounds(bounds);
};


/**
 * Determines if a marker lies in the clusters bounds.
 *
 * @param {google.maps.Marker} marker The marker to check.
 * @return {boolean} True if the marker lies in the bounds.
 */
Cluster.prototype.isMarkerInClusterBounds = function(marker) {
  return this.bounds_.contains(marker.getPosition());
};


/**
 * Returns the map that the cluster is associated with.
 *
 * @return {google.maps.Map} The map.
 */
Cluster.prototype.getMap = function() {
  return this.map_;
};


/**
 * Updates the cluster icon
 */
Cluster.prototype.updateIcon = function() {
  var zoom = this.map_.getZoom();
  var mz = this.markerClusterer_.getMaxZoom();

  if (mz && zoom > mz) {
    // The zoom is greater than our max zoom so show all the markers in cluster.
    for (var i = 0, marker; marker = this.markers_[i]; i++) {
      marker.setMap(this.map_);
    }
    return;
  }

  if (this.markers_.length < this.minClusterSize_) {
    // Min cluster size not yet reached.
    this.clusterIcon_.hide();
    return;
  }

  var numStyles = this.markerClusterer_.getStyles().length;
  var sums = this.markerClusterer_.getCalculator()(this.markers_, numStyles);
  this.clusterIcon_.setCenter(this.center_);
  this.clusterIcon_.setSums(sums);
  this.clusterIcon_.show();
};


/**
 * A cluster icon
 *
 * @param {Cluster} cluster The cluster to be associated with.
 * @param {Object} styles An object that has style properties:
 *     'url': (string) The image url.
 *     'height': (number) The image height.
 *     'width': (number) The image width.
 *     'anchor': (Array) The anchor position of the label text.
 *     'textColor': (string) The text color.
 *     'textSize': (number) The text size.
 *     'backgroundPosition: (string) The background postition x, y.
 * @param {number=} opt_padding Optional padding to apply to the cluster icon.
 * @constructor
 * @extends google.maps.OverlayView
 * @ignore
 */
function ClusterIcon(cluster, styles, opt_padding) {
  cluster.getMarkerClusterer().extend(ClusterIcon, google.maps.OverlayView);

  this.styles_ = styles;
  this.padding_ = opt_padding || 0;
  this.cluster_ = cluster;
  this.center_ = null;
  this.map_ = cluster.getMap();
  this.div_ = null;
  this.sums_ = null;
  this.visible_ = false;

  this.setMap(this.map_);
}


/**
 * Triggers the clusterclick event and zoom's if the option is set.
 *
 * @param {google.maps.MouseEvent} event The event to propagate
 */
ClusterIcon.prototype.triggerClusterClick = function(event) {
  var markerClusterer = this.cluster_.getMarkerClusterer();

  // Trigger the clusterclick event.
  google.maps.event.trigger(markerClusterer, 'clusterclick', this.cluster_, event);

  if (markerClusterer.isZoomOnClick()) {
    // Zoom into the cluster.
    this.map_.fitBounds(this.cluster_.getBounds());
  }
};


/**
 * Adding the cluster icon to the dom.
 * @ignore
 */
ClusterIcon.prototype.onAdd = function() {
  this.div_ = document.createElement('DIV');
  if (this.visible_) {
    var pos = this.getPosFromLatLng_(this.center_);
    this.div_.style.cssText = this.createCss(pos);
    this.div_.innerHTML = this.sums_.text;
  }

  var panes = this.getPanes();
  panes.overlayMouseTarget.appendChild(this.div_);

  var that = this;
  var isDragging = false;
  google.maps.event.addDomListener(this.div_, 'click', function(event) {
    // Only perform click when not preceded by a drag
    if (!isDragging) {
      that.triggerClusterClick(event);
    }
  });
  google.maps.event.addDomListener(this.div_, 'mousedown', function() {
    isDragging = false;
  });
  google.maps.event.addDomListener(this.div_, 'mousemove', function() {
    isDragging = true;
  });
};


/**
 * Returns the position to place the div dending on the latlng.
 *
 * @param {google.maps.LatLng} latlng The position in latlng.
 * @return {google.maps.Point} The position in pixels.
 * @private
 */
ClusterIcon.prototype.getPosFromLatLng_ = function(latlng) {
  var pos = this.getProjection().fromLatLngToDivPixel(latlng);

  if (typeof this.iconAnchor_ === 'object' && this.iconAnchor_.length === 2) {
    pos.x -= this.iconAnchor_[0];
    pos.y -= this.iconAnchor_[1];
  } else {
    pos.x -= parseInt(this.width_ / 2, 10);
    pos.y -= parseInt(this.height_ / 2, 10);
  }
  return pos;
};


/**
 * Draw the icon.
 * @ignore
 */
ClusterIcon.prototype.draw = function() {
  if (this.visible_) {
    var pos = this.getPosFromLatLng_(this.center_);
    this.div_.style.top = pos.y + 'px';
    this.div_.style.left = pos.x + 'px';
  }
};


/**
 * Hide the icon.
 */
ClusterIcon.prototype.hide = function() {
  if (this.div_) {
    this.div_.style.display = 'none';
  }
  this.visible_ = false;
};


/**
 * Position and show the icon.
 */
ClusterIcon.prototype.show = function() {
  if (this.div_) {
    var pos = this.getPosFromLatLng_(this.center_);
    this.div_.style.cssText = this.createCss(pos);
    this.div_.style.display = '';
  }
  this.visible_ = true;
};


/**
 * Remove the icon from the map
 */
ClusterIcon.prototype.remove = function() {
  this.setMap(null);
};


/**
 * Implementation of the onRemove interface.
 * @ignore
 */
ClusterIcon.prototype.onRemove = function() {
  if (this.div_ && this.div_.parentNode) {
    this.hide();
    this.div_.parentNode.removeChild(this.div_);
    this.div_ = null;
  }
};


/**
 * Set the sums of the icon.
 *
 * @param {Object} sums The sums containing:
 *   'text': (string) The text to display in the icon.
 *   'index': (number) The style index of the icon.
 */
ClusterIcon.prototype.setSums = function(sums) {
  this.sums_ = sums;
  this.text_ = sums.text;
  this.index_ = sums.index;
  if (this.div_) {
    this.div_.innerHTML = sums.text;
  }

  this.useStyle();
};


/**
 * Sets the icon to the the styles.
 */
ClusterIcon.prototype.useStyle = function() {
  var index = Math.max(0, this.sums_.index - 1);
  index = Math.min(this.styles_.length - 1, index);
  var style = this.styles_[index];
  this.url_ = style['url'];
  this.height_ = style['height'];
  this.width_ = style['width'];
  this.textColor_ = style['textColor'];
  this.anchor_ = style['anchor'];
  this.textSize_ = style['textSize'];
  this.backgroundPosition_ = style['backgroundPosition'];
  this.iconAnchor_ = style['iconAnchor'];
};


/**
 * Sets the center of the icon.
 *
 * @param {google.maps.LatLng} center The latlng to set as the center.
 */
ClusterIcon.prototype.setCenter = function(center) {
  this.center_ = center;
};


/**
 * Create the css text based on the position of the icon.
 *
 * @param {google.maps.Point} pos The position.
 * @return {string} The css style text.
 */
ClusterIcon.prototype.createCss = function(pos) {
  var style = [];
  style.push('background-image:url(' + this.url_ + ');');
  var backgroundPosition = this.backgroundPosition_ ? this.backgroundPosition_ : '0 0';
  style.push('background-position:' + backgroundPosition + ';');

  if (typeof this.anchor_ === 'object') {
    if (typeof this.anchor_[0] === 'number' && this.anchor_[0] > 0 &&
        this.anchor_[0] < this.height_) {
      style.push('height:' + (this.height_ - this.anchor_[0]) +
          'px; padding-top:' + this.anchor_[0] + 'px;');
    } else if (typeof this.anchor_[0] === 'number' && this.anchor_[0] < 0 &&
        -this.anchor_[0] < this.height_) {
      style.push('height:' + this.height_ + 'px; line-height:' + (this.height_ + this.anchor_[0]) +
          'px;');
    } else {
      style.push('height:' + this.height_ + 'px; line-height:' + this.height_ +
          'px;');
    }
    if (typeof this.anchor_[1] === 'number' && this.anchor_[1] > 0 &&
        this.anchor_[1] < this.width_) {
      style.push('width:' + (this.width_ - this.anchor_[1]) +
          'px; padding-left:' + this.anchor_[1] + 'px;');
    } else {
      style.push('width:' + this.width_ + 'px; text-align:center;');
    }
  } else {
    style.push('height:' + this.height_ + 'px; line-height:' +
        this.height_ + 'px; width:' + this.width_ + 'px; text-align:center;');
  }

  var txtColor = this.textColor_ ? this.textColor_ : 'black';
  var txtSize = this.textSize_ ? this.textSize_ : 11;

  style.push('cursor:pointer; top:' + pos.y + 'px; left:' +
      pos.x + 'px; color:' + txtColor + '; position:absolute; font-size:' +
      txtSize + 'px; font-family:Arial,sans-serif; font-weight:bold');
  return style.join('');
};


// Export Symbols for Closure
// If you are not going to compile with closure then you can remove the
// code below.
window['MarkerClusterer'] = MarkerClusterer;
MarkerClusterer.prototype['addMarker'] = MarkerClusterer.prototype.addMarker;
MarkerClusterer.prototype['addMarkers'] = MarkerClusterer.prototype.addMarkers;
MarkerClusterer.prototype['clearMarkers'] =
    MarkerClusterer.prototype.clearMarkers;
MarkerClusterer.prototype['fitMapToMarkers'] =
    MarkerClusterer.prototype.fitMapToMarkers;
MarkerClusterer.prototype['getCalculator'] =
    MarkerClusterer.prototype.getCalculator;
MarkerClusterer.prototype['getGridSize'] =
    MarkerClusterer.prototype.getGridSize;
MarkerClusterer.prototype['getExtendedBounds'] =
    MarkerClusterer.prototype.getExtendedBounds;
MarkerClusterer.prototype['getMap'] = MarkerClusterer.prototype.getMap;
MarkerClusterer.prototype['getMarkers'] = MarkerClusterer.prototype.getMarkers;
MarkerClusterer.prototype['getMaxZoom'] = MarkerClusterer.prototype.getMaxZoom;
MarkerClusterer.prototype['getStyles'] = MarkerClusterer.prototype.getStyles;
MarkerClusterer.prototype['getTotalClusters'] =
    MarkerClusterer.prototype.getTotalClusters;
MarkerClusterer.prototype['getTotalMarkers'] =
    MarkerClusterer.prototype.getTotalMarkers;
MarkerClusterer.prototype['redraw'] = MarkerClusterer.prototype.redraw;
MarkerClusterer.prototype['removeMarker'] =
    MarkerClusterer.prototype.removeMarker;
MarkerClusterer.prototype['removeMarkers'] =
    MarkerClusterer.prototype.removeMarkers;
MarkerClusterer.prototype['resetViewport'] =
    MarkerClusterer.prototype.resetViewport;
MarkerClusterer.prototype['repaint'] =
    MarkerClusterer.prototype.repaint;
MarkerClusterer.prototype['setCalculator'] =
    MarkerClusterer.prototype.setCalculator;
MarkerClusterer.prototype['setGridSize'] =
    MarkerClusterer.prototype.setGridSize;
MarkerClusterer.prototype['setMaxZoom'] =
    MarkerClusterer.prototype.setMaxZoom;
MarkerClusterer.prototype['onAdd'] = MarkerClusterer.prototype.onAdd;
MarkerClusterer.prototype['draw'] = MarkerClusterer.prototype.draw;

Cluster.prototype['getCenter'] = Cluster.prototype.getCenter;
Cluster.prototype['getSize'] = Cluster.prototype.getSize;
Cluster.prototype['getMarkers'] = Cluster.prototype.getMarkers;

ClusterIcon.prototype['onAdd'] = ClusterIcon.prototype.onAdd;
ClusterIcon.prototype['draw'] = ClusterIcon.prototype.draw;
ClusterIcon.prototype['onRemove'] = ClusterIcon.prototype.onRemove;


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/gframirez/Documents/Municipalidad/arbolado/arboladoApp/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map