$(document).ready(function(){
  function animations(max){
    var timeOut2 = setTimeout(function() {

      var counter = { var: 0 };

    // Add Scene to ScrollMagic Controller
      var t = new ScrollMagic.Controller,
          e = $(".count");
      e.each(function() {
          var h = function() {
              TweenMax.to(counter, 2, {
                  var: parseInt(e.attr("data-max"), 10),
                  onUpdate: function() {
                    var nwc = numberoConPuntos(counter.var);
                    e.html(nwc);
                },
                  ease: Power0.easeNone
              })
          }

          ;
          new ScrollMagic.Scene({
              triggerElement: e,
              offset: $('body').offset().top,
              reverse: false
          }).on("enter", h).addTo(t)
      })

    }, 500);
  }
  $.ajax({
		url: "https://gobiernoabierto.cordoba.gob.ar/api/v2/arboles/arboles/",
		type: "get",
		dataType: "json",
		error: function(hr){
      console.log(error);
		},
		success: function(response){
      console.log(response.count);
      $("#countArboles").attr("data-max", response.count);
       animations(response.count);
		}
	});

  // animations();

});
