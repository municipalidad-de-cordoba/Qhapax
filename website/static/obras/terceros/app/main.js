(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/api.service.ts":
/*!********************************!*\
  !*** ./src/app/api.service.ts ***!
  \********************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./globals */ "./src/app/globals.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
var ApiService = /** @class */ (function () {
    function ApiService(http) {
        this.http = http;
        this.strQueries = "";
        this.globales = new _globals__WEBPACK_IMPORTED_MODULE_2__["Globals"]();
    }
    ApiService.prototype.getTipos = function () {
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras/tipos-obras-terceros/?page_size=' + this.globales.pageSize);
    };
    ApiService.prototype.getBarrios = function () {
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-de-terceros/barrios/?page_size=' + this.globales.pageSize);
    };
    ApiService.prototype.getEmpresas = function () {
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-de-terceros/empresas/?page_size=' + this.globales.pageSize);
    };
    ApiService.prototype.getTotalObras = function (tipo, barrio, estado, empresa, query) {
        this.strQueries = "";
        if (tipo) {
            this.strQueries += "&tipo_id=" + tipo.toString();
        }
        if (estado) {
            this.strQueries += "&estado=" + estado.toString();
        }
        if (barrio) {
            this.strQueries += "&barrios_ids=" + barrio.toString();
        }
        if (empresa) {
            this.strQueries += "&empresas_ids=" + empresa.toString();
        }
        if (query) {
            this.strQueries += "&texto=" + query;
        }
        //console.log('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-de-terceros/obras-de-terceros/?page_size=10'+this.strQueries)
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-de-terceros/obras-de-terceros/?page_size=10' + this.strQueries);
    };
    ApiService.prototype.getObrasWPage = function (page, tipo, barrio, estado, empresa, query) {
        this.strQueries = "";
        if (page > 1) {
            this.strQueries += "&page=" + page;
        }
        if (tipo) {
            this.strQueries += "&tipo_id=" + tipo.toString();
        }
        if (estado) {
            this.strQueries += "&estado=" + estado.toString();
        }
        if (query) {
            this.strQueries += "&texto=" + query;
        }
        if (barrio) {
            this.strQueries += "&barrios_ids=" + barrio.toString();
        }
        if (empresa) {
            this.strQueries += "&empresas_ids=" + empresa.toString();
        }
        //console.log('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-de-terceros/obras-de-terceros/?page_size='+this.globales.listPageSize+this.strQueries);
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-de-terceros/obras-de-terceros/?page_size=' + this.globales.listPageSize + this.strQueries);
    };
    ApiService.prototype.getObras = function (bounds, precision, tipo, barrio, estado, empresa, query) {
        this.strQueries = "";
        if (tipo) {
            this.strQueries += "&tipo_id=" + tipo.toString();
        }
        if (estado) {
            this.strQueries += "&estado=" + estado.toString();
        }
        if (query) {
            this.strQueries += "&texto=" + query;
        }
        if (precision <= 8) {
            this.strQueries += "&precision=" + Math.round(precision).toString();
        }
        if (barrio) {
            this.strQueries += "&barrios_ids=" + barrio.toString();
        }
        if (empresa) {
            this.strQueries += "&empresas_ids=" + empresa.toString();
        }
        this.strQueries += "&in_bbox=" + bounds.bottom + "," + bounds.left + "," + bounds.top + "," + bounds.right;
        //console.log(this.strQueries); //es la consulta sobre esto, me devuelve como 1100 elementos ahora te muestro y paso el link.
        //console.log('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-de-terceros/obras-de-terceros/?page_size='+this.globales.pageSize+this.strQueries);
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-de-terceros/obras-de-terceros/?page_size=' + this.globales.pageSize + this.strQueries);
    };
    ApiService.prototype.getFrentes = function (tipo, barrio, estado, empresa, query) {
        this.strQueries = "";
        if (tipo) {
            this.strQueries += "&tipo_id=" + tipo.toString();
        }
        if (estado) {
            this.strQueries += "&estado=" + estado.toString();
        }
        if (query) {
            this.strQueries += "&texto=" + query;
        }
        if (barrio) {
            this.strQueries += "&barrios_ids=" + barrio.toString();
        }
        if (empresa) {
            this.strQueries += "&empresas_ids=" + empresa.toString();
        }
        //console.log('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-de-terceros/frentes-obras-de-terceros/?page_size='+this.globales.pageSize+this.strQueries);
        return this.http.get('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-de-terceros/frentes-obras-de-terceros/?page_size=' + this.globales.pageSize + this.strQueries);
    };
    ApiService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* @import url(https://fonts.googleapis.com/css?family=Lato); */\n\n\nagm-map,\n.filterCol{\n  position: relative;\n  height: calc(100vh - 165px);\n  font-family: 'Lato', sans-serif;\n  font-size: 1.15em;\n}\n\n\n.no-padding{\n  padding: 0;\n}\n\n\nody{\n  background-color: #F9F9F9;\n}\n\n\ntextarea,\ninput {\n  -webkit-appearance: none;\n  -moz-appearance: none;\n  appearance: none;\n  background-clip: padding-box;\n}\n\n\na, ins, del, button {\n  color: inherit;\n  text-decoration: none;\n}\n\n\nul, ol,\nmenu {\n  list-style: none;\n  margin-bottom: 0px;\n}\n\n\n.pagination{\n  margin: 0px;\n}\n\n\n#main-new{\n  padding-top: 180px;\n}\n\n\n#main-new .nav {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  white-space: nowrap;\n  margin: 0 1rem;\n  padding: 3rem 0.5rem 0rem;\n}\n\n\n.nav__controls {\n  display: flex;\n  flex-wrap: wrap;\n}\n\n\n.nav__controls > .nav__label {\n  flex: auto;\n  margin-bottom:10px;\n}\n\n\n.nav__label {\n  position: relative;\n  margin-left: 1rem;\n  text-transform: capitalize;\n  z-index: 1;\n  cursor: pointer;\n  text-align: center;\n}\n\n\n.selects menu{\n  padding-left: 0px;\n\n}\n\n\n.selects .nav__controls{\n  padding: 0px;\n}\n\n\n.selects .nav__controls .nav__label{\n  margin-left: 0px;\n}\n\n\n.nav.selects{\n  border-bottom: 0px!important;\n}\n\n\n.tituloSelects{\n  color: #E90088;\n  font-weight: 700;\n  font-size: 1.5em;\n  text-align: center;\n}\n\n\n.nav__paginacion {\n  display: flex;\n}\n\n\n.nav__paginacion > span {\n  flex: auto;\n  align-items: center;\n}\n\n\n.nav__paginacion .nav__label {\n  padding: 5px;\n  text-align: center;\n  margin-left: 5px;\n  min-width: 25px;\n}\n\n\n.paginacion__botones {\n  display: flex;\n  flex-wrap: wrap;\n  justify-content: center;\n}\n\n\n.nav__label span {\n  line-height: 38px;\n  font-family: Lato;\n  font-size: 14px;\n}\n\n\n.nav__label {\n  font-family: Lato;\n  font-size: 14px !important;\n}\n\n\n.nav__label--active {\n  background-color: #00774e;\n  color: #FFF;\n  border-radius: 2px;\n  pointer-events: none;\n}\n\n\n.nav__label--active:hover {\n  cursor: default;\n}\n\n\n.nav__label--siguiente {\n  margin-left: 1rem;\n}\n\n\n.nav.paginacion{\n  text-align: center;\n  margin-bottom: 15px;\n}\n\n\n.clickable:hover{\n  cursor: pointer;\n}\n\n\n/*cards*/\n\n\n.cards {\n  display: flex;\n  flex-flow: column wrap;\n  width: 100%;\n  overflow-y: auto;\n  overflow-x: hidden;\n  max-height: 2500px;\n}\n\n\n.card {\n  background: #fefff9;\n  color: #363636;\n  margin: 1%;\n  text-decoration: none;\n  box-shadow: rgba(0, 0, 0, 0.19) 0 0 8px 0;\n  border-radius: 4px;\n  width: 23%;\n  flex: 1 0 auto;\n  -webkit-backface-visibility: hidden;\n  backface-visibility: hidden;\n  -webkit-transform-origin: 10% 50%;\n  transform-origin: 10% 50%;\n  z-index: 1;\n}\n\n\n.card a {\n  display: block;\n}\n\n\n.card .card-summary {\n  padding: 0 4%;\n  color: unset;\n}\n\n\n.card .card-summary strong {\n  vertical-align: top;\n  display: inline-block;\n  text-align: right;\n  width: 44%;\n}\n\n\n.card .card-summary span {\n  vertical-align: top;\n  display: inline-block;\n  width: 54%;\n}\n\n\n.card .card-summary .barrios {\n  margin: 5px;\n  line-height: 1.3em;\n  margin-bottom: 10px;\n}\n\n\n.card .card-summary .barrios span {\n  font-size: 14px;\n}\n\n\n.card .card-summary .barrios span:after {\n  content: \", \";\n}\n\n\n.card .card-summary .barrios span:last-child:after {\n  content: \".\";\n}\n\n\n.card .card-summary .barrios strong,\n.card .card-summary .barrios span {\n  width: unset;\n  text-align: left;\n  font-size: 1.1em;\n}\n\n\n.card .card-summary .barrios strong{\n  color: #00a665 !important;\n  font-size: 1.1em;\n}\n\n\n.card .card-summary .barrios span {\n  color: #777777;\n  font-weight: 200;\n\n}\n\n\n.card .vc_progress_bar .vc_single_bar,\n.card .progress {\n  background: #54635d;\n  border-radius: 0;\n  margin-bottom: 0px;\n  height: 30px;\n}\n\n\n.card .fecha {\n  background-color: #ebebeb;\n  color: #6d6d6d;\n  line-height: 1.33em;\n  font-size: 1em;\n}\n\n\n.card .fecha span {\n  width: 50%;\n  display: inline-block;\n  text-align: center;\n  padding: 3% 0;\n  position: relative;\n}\n\n\n.card .fecha span:nth-child(2) {\n  background-color: rgba(0, 166, 101, 0.85);\n  color: #FFFFFF;\n}\n\n\n.card .fecha span:nth-child(2):before {\n  content: '';\n  display: block;\n  position: absolute;\n  left: 0;\n  width: 0;\n  height: 0;\n  border-style: solid;\n  top: 0;\n  border-color: transparent transparent transparent #ebebeb;\n  border-width: 26px 0 26px 7px;\n}\n\n\n.card .icono {\n  position: absolute;\n  right: 3px;\n  width: 15%;\n  top: 4px;\n}\n\n\n.card .card-header {\n  position: relative;\n  height: 175px;\n  overflow: hidden;\n  background-repeat: no-repeat;\n  background-size: cover;\n  background-position: center;\n  background-color: rgba(255, 255, 255, 0.15);\n  background-blend-mode: overlay;\n  border-radius: 4px 4px 0 0;\n  text-align: right;\n}\n\n\n.card .tipo {\n  color: #fff;\n  background-color: #00a665;\n  display: inline-block;\n  font-weight: bold;\n  max-width: 102px;\n  text-align: center;\n  padding: 0 5px;\n  margin-top: 15px;\n  margin-right: 15px;\n}\n\n\n.card .tipo--licitada {\n  margin: 0 0px 0;\n  top: 15px;\n  left: 8px;\n  position: absolute;\n  max-width: unset;\n  background-color: #faa732;\n}\n\n\n.card .card-header:hover, .card .card-header:focus {\n  background-color: rgba(255, 255, 255, 0);\n}\n\n\n.card .card-title {\n  background: rgba(0, 166, 101, 0.85);\n  padding: 3.5% 0 2.5% 0;\n}\n\n\n.card .card-title h3 {\n  font-size: 1.2em;\n  color: white;\n  line-height: 1.2;\n  padding: 0 3.5%;\n  margin: 0;\n  letter-spacing: 1px;\n}\n\n\n.card:hover, .card:focus {\n  background: white;\n  box-shadow: rgba(0, 0, 0, 0.45) 0px 0px 20px 0px;\n}\n\n\n.card:hover .card-title, .card:focus .card-title {\n  background: rgba(0, 217, 132, 0.85);\n}\n\n\nimg {\n  max-width: 100%;\n}\n\n\n.card abbr {\n  text-transform: none;\n  border-bottom: none;\n  letter-spacing: unset;\n}\n\n\n.card .vc_progress_bar.wpb_content_element {\n  margin-bottom: 0;\n}\n\n\n.resultados {\n  margin-top: 10px;\n}\n\n\n.sin-resultados {\n  height: 200px;\n}\n\n\n.sin-resultados,\n.paginacion-obras {\n  padding: 10px;\n  text-align: center;\n  font-size: 1.5em;\n}\n\n\n.paginacion-obras{\n  color: #E90088;\n  font-style: italic;\n}\n\n\n.obra-move {\n  transition: all 600ms ease-in-out 50ms;\n}\n\n\n.obra-enter-active {\n  transition: all 300ms ease-out;\n}\n\n\n.obra-leave-active {\n  transition: all 200ms ease-in;\n  z-index: 0;\n}\n\n\n.obra-enter, .obra-leave-to {\n  opacity: 0;\n}\n\n\n.obra-enter {\n  -webkit-transform: scale(0.9);\n  transform: scale(0.9);\n}\n\n\n@media all and (max-width: 940px) {\n.card {\n  width: 31%;\n}\n}\n\n\n@media all and (max-width: 768px) {\n  .tituloObras {\n    margin-top: 50px;\n  }\n  .tituloSelects {\n    margin: 10px 0;\n  }\n  .cardMedicion .medicion,\n  .cardMedicion .descripcionMed {\n    text-align: center;\n  }\n  .cardMedicion .medicion {\n    padding-top: 5px;\n  }\n}\n\n\n@media all and (max-width: 770px) {\n.card {\n  width: 48%;\n}\n.nav__controls {\n  flex-direction: column;\n  min-height: 50px;\n  margin: 0;\n}\n.nav__label,.nav__label--siguiente {\n  margin: 0;\n}\n}\n\n\n@media all and (max-width: 520px) {\n.nav__paginacion {\n  flex-direction: column;\n}\n}\n\n\n@media all and (max-width: 480px) {\n.card {\n  width: 97%;\n}\n.cards {\n  max-height: unset !important;\n}\n}\n\n\n#mapa {\nheight: 500px;\nwidth: 100%;\nmargin: 0px;\npadding: 0px;\n}\n\n\n.vc_label_units{\nvertical-align: -5px;\nfloat: left;\nmargin-left: 10px;\nmargin-top: 5px;\n}\n\n\n.tituloObras{\nfont-size: 2.75em;\nfont-weight: 500;\nline-height: .3em;\ntext-align: center;\nmargin-bottom: 20px;\ncolor: #E90088;\ntext-transform: uppercase;\n}\n\n\n.tituloObras img{\nmargin-right: 10px;\n}\n\n\n.btnDescargar{\n  width: 100%;\n  display: block;\n  padding: 10px;\n  text-align: center;\n  border: 1px solid #C8C5EF;\n  color: #7D35C4;\n  font-size: 1.25em;\n  text-transform: uppercase;\n  align-self: flex-start;\n  margin-top: 40px;\n}\n\n\n.btnDescargar:hover{\n  background-color: #C8C5EF;\n}\n\n\n.btnDescargar .la-stack{\n  float: right;\n  margin-top: -6px;\n}\n\n\ninput{\n  border: 0px;\n  -webkit-appearance: none;\n  -moz-appearance: none;\n  appearance: none;\n  padding: 12px 14px 12px 20px;\n  font-size: 16px;\n  line-height: 1.3em;\n  color: #E90088;\n  font-style: italic;\n  font-weight: 700;\n  font-size: 1.25em;\n  border-radius: 0!important;\n  min-width: 240px;\n  max-width: 100%;\n  width: 100%;\n  display: block;\n  background-color: #F9F9F9;\n  padding-right: 41px;\n  padding-top: 10px;\n  padding-bottom: 10px;\n}\n\n\ninput::-webkit-input-placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */\n    color: #E90088;\n    opacity: 1; /* Firefox */\n}\n\n\ninput:-ms-input-placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */\n    color: #E90088;\n    opacity: 1; /* Firefox */\n}\n\n\ninput::-ms-input-placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */\n    color: #E90088;\n    opacity: 1; /* Firefox */\n}\n\n\ninput::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */\n    color: #E90088;\n    opacity: 1; /* Firefox */\n}\n\n\ninput:-ms-input-placeholder { /* Internet Explorer 10-11 */\n    color: #E90088;\n}\n\n\ninput::-ms-input-placeholder { /* Microsoft Edge */\n    color: #E90088;\n}\n\n\nselect{\n  border: 0px;\n  /* background: url('/static/obras/images/selectbox-arrow.png') right center no-repeat; */\n  -webkit-appearance: none;\n  -moz-appearance: none;\n  appearance: none;\n  padding: 12px 14px 12px 20px;\n  font-size: 16px;\n  line-height: 1.3em;\n  color: #E90088;\n  font-style: italic;\n  font-weight: 700;\n  font-size: 1.25em;\n  border-radius: 0!important;\n  min-width: 240px;\n  max-width: 100%;\n  width: 100%;\n  display: block;\n  background-color: #F9F9F9;\n  padding-right: 41px;\n  padding-top: 10px;\n  padding-bottom: 10px;\n}\n\n\n.nav__select:after{\n  width: 0;\n  height: 0;\n  border-left: 6px solid transparent;\n  border-right: 6px solid transparent;\n  border-top: 6px solid #E90088;\n  position: absolute;\n  top: 45%;\n  right: 10px;\n  content: \" \";\n  z-index: 98;\n }\n\n\n.selects .nav__label {\nmargin-top: 10px;\nbackground-image: #E90088; /* Old browsers */ /* FF3.6-15 */ /* Chrome10-25,Safari5.1-6 */\nbackground-image: linear-gradient(to right, #E90088 0%,#E8FF00 130%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */\npadding: 1px;\n}\n\n\n.masBuscado.sinBorde{\n  background-color: #FFF;\n  border-top: 5px solid #00a665;\n}\n\n\n.night .masBuscado.sinBorde{\n  background-color: #000;\n  border-top: 5px solid #026089;\n}\n\n\n.high_contrast .masBuscado.sinBorde{\n  background-color: #000;\n  border-top: 5px solid #FFEB02;\n}\n\n\n.btn-iniciar{\ncolor: #FFFFFF;\nfont-weight: 900;\ntext-transform: uppercase;\nfont-size: 1.5em;\nwidth: 100%;\ndisplay: inline-block;\npadding: 7.5px;\ntransition: all .2s ease-in-out;\n-webkit-transition:all .2s ease-in-out;\n}\n\n\n.btn-iniciar:hover,\n.btn-iniciar:focus{\ncolor: #FFFFFF;\nfont-size: 1.55em;\nbackground-color: rgba(0,0,0,0.25) !important;\ntransition: all .2s ease-in-out;\n-webkit-transition:all .2s ease-in-out;\n}\n\n\n.cardMedicion{\ncolor: #E90088;\nmargin: 20px auto;\ndisplay: inline-block;\n}\n\n\n.cardMedicion .imgMedicion{\ntext-align: center;\n}\n\n\n.cardMedicion .descripcionMed{\nfont-style: italic;\nfont-size: 1.2em;\n}\n\n\n.cardMedicion .medicion{\nfont-size: 2em;\nfont-weight: 900;\nmargin: 0px;\n}\n\n\n.colMapa,\n.col-header{\ndisplay: flex;\nflex-direction: column;\njustify-content: center;\nborder-bottom: none;\n}\n\n\n.colMapa{\n  min-height:500px;\n}\n\n\n.cSeparador{\nborder-right: 1px solid #e88044;\n\n}\n\n\n.tituloObra{\n  margin-top: 40px;\n  color: #E90088;\n  font-weight: 600;\n  margin-bottom: 30px;\n  text-transform: lowercase;\n  font-size: 1.75em;\n}\n\n\n.tituloObra:first-letter {\n  text-transform: uppercase;\n}\n\n\n.container.inFContainer{\n  padding-top: 80px;\n}\n\n\n.container.inFContainer.tpl2017{\n  padding-top: 200px;\n}\n\n\n.boxInfo{\n  background-color: #F1F1F1;\n  padding: 5px 20px;\n  margin-bottom: 10px;\n  display: inline-block;\n  width: 100%;\n}\n\n\n.colExp{\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n\n.tipoObra{\n  padding: 2.5px 7.5px;\n  background-color: #FFF;\n  text-align: center;\n  font-weight: 500;\n  font-style: italic;\n}\n\n\n.boxInfo.indent{\n  padding: 5px 100px;\n}\n\n\n.boxInfo p{\n  margin: 0;\n  font-size: 1.2em;\n  color: #000;\n  font-weight: 600;\n}\n\n\n.imgBack{\n   min-height:100px;\n   width:100%;\n   background-size: cover;\n   background-position: center center;\n}\n\n\n.verticalDivider{\n  position: absolute;\n    left: -13px;\n    font-size: 4em;\n    top: -14px;\n}\n"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\">\n<script src=\"https://code.jquery.com/jquery-3.2.1.min.js\"></script>\n<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\"></script>\n<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.0.13/css/all.css\" integrity=\"sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp\" crossorigin=\"anonymous\">\n<link href=\"https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900\" rel=\"stylesheet\"> -->\n\n\n\n\n    <div class=\"row\">\n        <div class=\"col-md-9 nopadding col-header cSeparador\">\n            <h1 class=\"tituloObras\"><img src=\"/static/obras/images/terceros/icon.png\" class=\"pulse infinite animated\" height=\"75\" alt=\"logo de obras de terceros\"/>Obras de Terceros</h1>\n        </div>\n        <div class=\"col-md-3 col-header\">\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-xs-12 col-lg-6 no-padding colMapa filterCol mapCont\">\n            <agm-map [latitude]=\"lat\" [longitude]=\"lng\" [zoom]=\"zoom\" (zoomChange)=\"zoomChange($event)\" (boundsChange)=\"boundsChange($event)\" (idle)=\"setBoundsData($event)\">\n                <core-map-content (onMapLoad)='loadAPIWrapper($event)'></core-map-content>\n            </agm-map>\n        </div>\n        <div class=\"col-xs-12 col-lg-6\">\n            <div class=\"row\">\n                <div class=\"col-xs-12 col-lg-6 colMapa filterCol cSeparador\">\n                  <a href=\"/data/datos-abiertos/categoria/datos-geograficos-y-mapas/intervenciones-de-terceros-en-la-via-publica/204\" class=\"btnDescargar\" target=\"_blank\">Descargar\n                    <span class=\"la-stack\" title=\"Icono de descarga\">\n                      <span class=\"la la-circle-thin la-stack-2x\"></span>\n                      <span class=\"la la-download la-stack-1x\"></span>\n                    </span>\n                  </a>\n                    <div class=\"row\">\n                        <div class=\"cardMedicion col-xs-12 col-sm-4 col-md-12\">\n                            <h2 class=\"medicion\" style=\"text-align: center;\">TOTAL al <span id=\"fechaActual\">{{today|date : \"dd/MM/yy\"}}</span></h2>\n                        </div>\n                        <div class=\"cardMedicion col-xs-12 col-sm-4 col-md-12\">\n                            <div class=\"row\">\n                                <div class=\"col-md-4\">\n                                    <div class=\"imgMedicion\">\n                                        <img src=\"/static/obras/images/terceros/obras.png\" height=\"60\" class=\"pulse infinite animated\" alt=\"icono representando obras\"/>\n                                    </div>\n                                </div>\n                                <div class=\"col-md-8\">\n                                    <div class=\"row\">\n                                        <div class=\"col-md-6\">\n                                            <h2 class=\"medicion\">\n                                                <span id=\"obrasNumero\"><span *ngIf=\"totalObras>-1\">{{totalObras}}</span><span *ngIf=\"totalObras==-1\">0</span></span>\n                                            </h2>\n                                            <p class=\"descripcionMed\">\n                                                Obras\n                                            </p>\n                                        </div>\n                                        <div class=\"col-md-6\" style=\"position: relative;\">\n                                            <div class=\"verticalDivider\">/</div>\n                                            <h2 class=\"medicion\">\n                                                <span id=\"frenteObrasNumero\">{{totalFrentes}}</span>\n                                            </h2>\n                                            <p class=\"descripcionMed\">\n                                                Frentes de Obras\n                                            </p>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"cardMedicion col-xs-12 col-sm-4 col-md-12\">\n                            <div class=\"row\">\n                                <div class=\"col-md-4\">\n                                    <div class=\"imgMedicion\">\n                                        <img src=\"/static/obras/images/terceros/finalizadas.png\" height=\"70\" style=\"margin-right: -20px;\" class=\"pulse infinite animated\" alt=\"icono representando obras finalizadas\" />\n                                    </div>\n                                </div>\n                                <div class=\"col-md-8\" style=\"margin-top:10px;\">\n                                    <h2 class=\"medicion\">\n                                        <span id=\"finalizadasNumero\">{{obrasFinalizadas | number : '1.2-2'}}</span>%\n                                    </h2>\n                                    <p class=\"descripcionMed\">\n                                        Finalizadas\n                                    </p>\n                                </div>\n                            </div>\n                         </div>\n                    </div>\n                </div>\n                <div class=\"col-xs-12 col-lg-6 colMapa\">\n                    <p class=\"tituloSelects\">Buscá obras en tu barrio</p>\n                    <nav class=\"nav selects\">\n                        <menu>\n                            <ul class=\"nav__controls\">\n                            <li class=\"nav__label\">\n                                <label for=\"busquedaTexto\" class=\"sr-only\">Buscar por texto</label>\n                                <input [(ngModel)]=\"searchQ\" type=\"text\" name=\"texto\" id=\"busquedaTexto\" placeholder=\"Buscá una obra\">\n                            </li>\n                            <li class=\"nav__label nav__select\">\n                                <label for=\"tipoObra\" class=\"sr-only\">Tipo de Obra</label>\n                                <select [(ngModel)]=\"selectedTipo\" name=\"tipoObra\" id=\"tipoObra\">\n                                    <option value=\"-1\" selected=\"selected\">Tipo de obra</option>\n                                    <option *ngFor=\"let t of tipos\" [ngValue]=\"t.id\">{{t.nombre}}</option>\n                                </select>\n                            </li>\n                            <li class=\"nav__label nav__select\">\n                                <label for=\"progreso\" class=\"sr-only\">Estado de Obra</label>\n                                <select [(ngModel)]=\"selectedEstado\" name=\"progreso\" id=\"progreso\">\n                                <option value=\"-1\">Estado de Obra</option>\n                                <option value=\"1\">En proyecto</option>\n                                <option value=\"2,3,4,5\">Licitadas</option>\n                                <option value=\"6,7\">En progreso</option>\n                                <option value=\"8,9\">Finalizadas</option>\n                                </select>\n                            </li>\n                            <li class=\"nav__label nav__select\">\n                                <label for=\"barrio\" class=\"sr-only\">Barrio</label>\n                                <select [(ngModel)]=\"selectedBarrio\" name=\"barrio\" id=\"barrio\">\n                                    <option value=\"-1\" selected=\"selected\">Barrio</option>\n                                    <option *ngFor=\"let b of barrios\" [ngValue]=\"b.id\">{{b.nombre}}</option>\n                                </select>\n                            </li>\n                            <li class=\"nav__label nav__select\">\n                                <label for=\"empresa\" class=\"sr-only\">Empresa</label>\n                                <select [(ngModel)]=\"selectedEmpresa\" name=\"empresa\" id=\"empresa\">\n                                    <option value=\"-1\" selected=\"selected\">Empresa</option>\n                                    <option *ngFor=\"let e of empresas\" [ngValue]=\"e.id\">{{e.nombre}}</option>\n                                </select>\n                            </li>\n                            <li class=\"nav__label\">\n                                <div type=\"button\" class=\"btn-iniciar\" (click)=\"getAllFilteredData()\"> Buscar </div>\n                            </li>\n                            </ul>\n                        </menu>\n                    </nav>\n                </div>\n            </div>\n        </div>\n        <div class=\"col-xs-12 col-lg-10 col-lg-offset-1\">\n            <div class=\"resultados\">\n               <div class=\"paginacion-obras\"><span *ngIf=\"totalObras>0\">Mostrando {{ (currentPage-1) * globales.listPageSize + 1}} a <span *ngIf=\"currentPage * globales.listPageSize<totalObras\">{{ currentPage * globales.listPageSize }}</span><span *ngIf=\"currentPage *globales.listPageSize>=totalObras\">{{ totalObras }}</span> de {{totalObras}} obras</span><span *ngIf=\"totalObras==-1\">Cargando..</span><span *ngIf=\"totalObras==0\">No hay resultados.</span></div>\n            </div>\n            <div class=\"resultados\">\n                <div class=\"cards\">\n                        <a href=\"estado-de-obra-terceros#obra-{{obra.id}}\" class=\"card\" *ngFor=\"let obra of listaObras\">\n                            <div class=\"card-header\" style=\"background-image: url(&quot;https://www.cordoba.gob.ar/wp-content/uploads/2017/12/en-construccion-default.png&quot;);\">\n                                <span class=\"tipo tipo--licitada\">{{obra.estado}}</span>\n                                <span class=\"tipo\">{{obra.tipo}}</span>\n                            </div>\n                            <div class=\"card-title\">\n                                <h3>{{obra.nombre}}</h3>\n                            </div>\n                            <div class=\"progress\">\n                                <div role=\"progressbar\"  class=\"progress-bar progress-bar-info\" [attr.aria-valuenow]=\"obra.porcentaje_completado\" [style.width]=\"obra.porcentaje_completado+'%'\" aria-valuemin=\"0\" aria-valuemax=\"100\">\n                                    <span class=\"vc_label_units\">{{obra.porcentaje_completado}}%</span>\n                                </div>\n                            </div>\n                            <div class=\"fecha\">\n                                <span>Inicio<br>{{obra.fechaInicio|date : \"dd/MM/yy\"}}</span>\n                                <span>Fin Estimado<br>{{obra.fechaFinalizacion|date : \"dd/MM/yy\"}}</span>\n                            </div>\n                            <div class=\"card-summary\">\n                                <!-- <p class=\"barrios\">\n                                    <strong>Barrios:</strong>\n                                    <br><span>Alberdi</span><span>Centro</span><span>Nueva Córdoba</span><span>Quinta de Santa Ana</span><span>Paso De Los Andes</span><span>Güemes</span><span>Observatorio</span><span>Villa Maurizzi</span>\n                                </p> -->\n                            </div>\n                        </a>\n                </div>\n            </div>\n            <nav class=\"nav paginacion\">\n                <menu>\n                    <ul class=\"pagination\">\n                         <li *ngIf=\"currentPage>1\"><a class=\"clickable\" (click)=\"setPreviousPage()\"> anterior</a></li>\n                         <li *ngFor=\"let page of pager\" [class.active]=\"currentPage==page\" class=\"item\"><a class=\"clickable\" (click)=\"setCurrentPage(page)\">{{page}}</a></li>\n                         <li *ngIf=\"currentPage<(totalObras/globales.listPageSize)\"><a class=\"clickable\" (click)=\"setNextPage()\">siguiente</a></li>\n                    </ul>\n                </menu>\n            </nav>\n        </div>\n  </div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./api.service */ "./src/app/api.service.ts");
/* harmony import */ var _obra__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./obra */ "./src/app/obra.ts");
/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./globals */ "./src/app/globals.ts");
/* harmony import */ var _assets_markerclusterer_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../assets/markerclusterer.js */ "./src/assets/markerclusterer.js");
/* harmony import */ var _assets_markerclusterer_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_assets_markerclusterer_js__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppComponent = /** @class */ (function () {
    function AppComponent(_apiService) {
        this._apiService = _apiService;
        this.obrasFinalizadas = 0;
        this.currentPrecision = 5;
        this.title = 'My first AGM project';
        this.zoom = 11;
        this.lat = -31.420186;
        this.lng = -64.188861;
        this.today = new Date();
        this.mapIsInitialize = false;
        this.clusters = [];
        this.pager = [];
        this.globales = new _globals__WEBPACK_IMPORTED_MODULE_3__["Globals"]();
        this.totalFrentes = 0;
        this.totalObras = -1;
        this.obras = [];
        this.listaObras = [];
        this.tipos = [];
        this.barrios = [];
        this.empresas = [];
        this.currentPage = 1;
        this.pages = 0;
        this.selectedTipo = "-1";
        this.selectedEstado = "-1";
        this.selectedBarrio = "-1";
        this.selectedEmpresa = "-1";
    }
    AppComponent.prototype.loadAPIWrapper = function (map) {
        this.map = map;
        this.markerCluster = new MarkerClusterer(this.map, null, { imagePath: '/static/obras/images/terceros/map_img/m' });
    };
    AppComponent.prototype.ngOnInit = function () {
        this.getInitialData();
    };
    AppComponent.prototype.onKeyPress = function (value) {
        this.searchQ = value;
    };
    AppComponent.prototype.getInitialData = function () {
        var _this = this;
        this._apiService.getTipos().subscribe(function (data) { _this.apiResults = data; }, function (err) { return console.error(err); }, function () {
            _this.apiResults.results.forEach(function (e) {
                _this.tipos.push(new _obra__WEBPACK_IMPORTED_MODULE_2__["Tipo"](e.id, e.nombre));
            });
        });
        this._apiService.getBarrios().subscribe(function (data) { _this.apiResults = data; }, function (err) { return console.error(err); }, function () {
            _this.apiResults.results.forEach(function (e) {
                _this.barrios.push(new _obra__WEBPACK_IMPORTED_MODULE_2__["Tipo"](e.id, e.nombre));
            });
        });
        this._apiService.getEmpresas().subscribe(function (data) { _this.apiResults = data; }, function (err) { return console.error(err); }, function () {
            _this.apiResults.results.forEach(function (e) {
                _this.empresas.push(new _obra__WEBPACK_IMPORTED_MODULE_2__["Tipo"](e.id, e.nombre));
            });
        });
        this.filterObras();
    };
    AppComponent.prototype.getAllFilteredData = function () {
        this.filterObras();
        this.filterObrasMap();
    };
    AppComponent.prototype.filterObras = function () {
        this.getList(this.selectedTipo, this.selectedEstado, this.selectedBarrio, this.selectedEmpresa, this.searchQ);
    };
    AppComponent.prototype.filterObrasMap = function () {
        this.getData(this.selectedTipo, this.selectedEstado, this.selectedBarrio, this.selectedEmpresa, this.searchQ);
    };
    AppComponent.prototype.getList = function (tipo, estado, barrio, empresa, query) {
        var _this = this;
        var apiTipo = (tipo != "-1" ? tipo : null);
        var apiEstado = (estado != "-1" ? estado : null);
        var apiBarrio = (barrio != "-1" ? barrio : null);
        var apiEmpresa = (empresa != "-1" ? empresa : null);
        this.pager = [];
        this.listaObras = [];
        this.pages = 0;
        this._apiService.getObrasWPage(this.currentPage, apiTipo, apiBarrio, apiEstado, apiEmpresa, query).subscribe(function (data) { _this.apiResults = data; }, function (err) { return console.error(err); }, function () {
            _this.pages = Math.round(_this.apiResults.count / _this.globales.listPageSize);
            _this.apiResults.results.features.forEach(function (e) {
                _this.listaObras.push(new _obra__WEBPACK_IMPORTED_MODULE_2__["Obra"](e.id, e.properties.nombre, e.properties.porcentaje_completado, e.properties.estado, e.properties.organizacion, e.properties.monto, e.properties.fecha_inicio, e.properties.fecha_finalizacion_estimada, e.properties.tipo, null));
                //console.log(this.listaObras);//listado de las obras
            });
            if (_this.currentPage <= 5) {
                for (var i = 1; i <= (_this.pages >= 10 ? 10 : _this.pages); i++) {
                    _this.pager.push(i);
                }
            }
            else {
                if (_this.currentPage >= _this.pages - 5) {
                    for (var i = _this.currentPage - 5; i < _this.pages; i++) {
                        _this.pager.push(i);
                    }
                }
                else {
                    for (var i = _this.currentPage - 5; i < _this.currentPage + 5; i++) {
                        _this.pager.push(i);
                    }
                }
            }
            //console.log("pager", this.pager);
        });
    };
    AppComponent.prototype.getData = function (tipo, estado, barrio, empresa, query) {
        var _this = this;
        this.obras = [];
        var apiTipo = (tipo != "-1" ? tipo : null);
        var apiEstado = (estado != "-1" ? estado : null);
        var apiBarrio = (barrio != "-1" ? barrio : null);
        var apiEmpresa = (empresa != "-1" ? empresa : null);
        this._apiService.getTotalObras(apiTipo, apiBarrio, apiEstado, apiEmpresa, query).subscribe(function (data) { _this.apiResults = data; }, function (err) { return console.error(err); }, function () {
            _this.totalObras = _this.apiResults.count;
            if (_this.totalObras > 0) {
                _this.obrasFinalizadas = _this.apiResults.results.features[0].properties.porcentaje_obras_completadas;
            }
            else {
                _this.obrasFinalizadas = 0;
            }
            //console.log(this.totalObras);//total de las obras
        });
        this._apiService.getFrentes(apiTipo, apiBarrio, apiEstado, apiEmpresa, query).subscribe(function (data) { _this.apiResults = data; }, function (err) { return console.error(err); }, function () {
            _this.totalFrentes = _this.apiResults.count;
            if (_this.totalFrentes > 0) {
                _this.apiResults.results.features.forEach(function (e) {
                    var auxGeometry = new _obra__WEBPACK_IMPORTED_MODULE_2__["Geometry"](e.geometry.type, e.geometry.coordinates);
                    _this.obras.push(new _obra__WEBPACK_IMPORTED_MODULE_2__["Obra"](e.id, e.properties.nombre, e.properties.porcentaje_completado, e.properties.estado, e.properties.organizacion, e.properties.monto, e.properties.fecha_inicio, e.properties.fecha_finalizacion_estimada, e.properties.tipo, auxGeometry));
                });
            }
            else {
                _this.totalFrentes = 0;
                _this.obras = [];
            }
        });
        this.markerCluster.RemoveClusters();
        this.markerCluster.clearMarkers();
        if (this.currentPrecision < 8) {
            //console.log('Entro al current Precision')
            this.markerCluster.clearMarkers();
            this._apiService.getObras(this.currentBounds, this.currentPrecision, apiTipo, apiBarrio, apiEstado, apiEmpresa, query).subscribe(function (data) { _this.apiResults = data; }, function (err) { return console.error(err); }, function () {
                if (_this.apiResults.count > 0) {
                    _this.clusters.length = 0;
                    _this.apiResults.results.features.forEach(function (e) {
                        //ACA  COLOCA LAS X de MARKERS
                        _this.clusters.push(new _obra__WEBPACK_IMPORTED_MODULE_2__["Cluster"](e.geometry.coordinates[1], e.geometry.coordinates[0], e.properties.cluster_count, (Math.pow(2, (21 - _this.zoom)) * 1128.497220 * 0.0005)));
                    });
                    _this.clusters.forEach(function (e) {
                        _this.markerCluster.AddCluster(e.lat, e.lng, e.count);
                    });
                    //console.log(this.apiResults.count)
                }
                else {
                }
            });
        }
        else {
            this._apiService.getObras(this.currentBounds, 8.2, apiTipo, apiBarrio, apiEstado, apiEmpresa, query).subscribe(function (data) { _this.apiResults = data; }, function (err) { return console.error(err); }, function () {
                _this.apiResults.results.features.forEach(function (e) {
									_this.markerCluster.AddCluster(e.geometry.coordinates[1], e.geometry.coordinates[0], "");
                    var auxGeometry = new _obra__WEBPACK_IMPORTED_MODULE_2__["Geometry"](e.geometry.type, e.geometry.coordinates);
                    _this.obras.push(new _obra__WEBPACK_IMPORTED_MODULE_2__["Obra"](e.id, e.properties.nombre, e.properties.porcentaje_completado, e.properties.estado, e.properties.organizacion, e.properties.monto, e.properties.fecha_inicio, e.properties.fecha_finalizacion_estimada, e.properties.tipo, auxGeometry));
                });
            });
        }
    };
    AppComponent.prototype.setCurrentPage = function (page) {
        this.currentPage = page;
        this.filterObras();
    };
    AppComponent.prototype.setNextPage = function () {
        this.setCurrentPage(++this.currentPage);
    };
    AppComponent.prototype.setPreviousPage = function () {
        this.setCurrentPage(--this.currentPage);
    };
    AppComponent.prototype.boundsChange = function (bounds) {
        this.currentBounds = new _obra__WEBPACK_IMPORTED_MODULE_2__["MapBounds"](bounds.b.b, bounds.b.f, bounds.f.b, bounds.f.f);
    };
    AppComponent.prototype.setBoundsData = function (bounds) {
        // this.ngUnsubscribe[this.currentCall].next();
        // this.ngUnsubscribe[this.currentCall].complete();
        // this.currentCall++;
        // this.ngUnsubscribe.push(new Subject());
        this.filterObrasMap();
    };
    AppComponent.prototype.zoomChange = function (zoom) {
        console.log('zoomChange', zoom);
        if (this.zoom > zoom) {
            this.currentPrecision -= .4;
        }
        else if (this.zoom < zoom) {
            this.currentPrecision += .4;
        }
        switch (zoom) {
            case 0:
                this.currentPrecision = 3;
                break;
            case 1:
                this.currentPrecision = 3;
                break;
            case 2:
                this.currentPrecision = 3;
                break;
            case 3:
                this.currentPrecision = 3;
                break;
            case 4:
                this.currentPrecision = 3;
                break;
            case 5:
                this.currentPrecision = 3;
                break;
            case 6:
                this.currentPrecision = 3;
                break;
            case 7:
                this.currentPrecision = 3;
                break;
            case 8:
                this.currentPrecision = 4;
                break;
            case 9:
                this.currentPrecision = 4;
                break;
            case 10:
                this.currentPrecision = 5;
                break;
            case 11:
                this.currentPrecision = 5;
                break;
            case 12:
                this.currentPrecision = 5;
                break;
            case 13:
                this.currentPrecision = 6;
                break;
            case 14:
                this.currentPrecision = 6;
                break;
            case 15:
                this.currentPrecision = 6;
                break;
            case 16:
                this.currentPrecision = 7;
                break;
            case 17:
                this.currentPrecision = 8;
                break;
            case 18:
                //this.currentPrecision = 8;
                break;
            case 19:
                //this.currentPrecision = 8;
                break;
            case 20:
                //this.currentPrecision = 8;
                break;
            case 21:
                //this.currentPrecision = 8;
                break;
            case 22:
                //this.currentPrecision = 8;
                break;
            case 23:
                //this.currentPrecision = 8;
                break;
        }
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./api.service */ "./src/app/api.service.ts");
/* harmony import */ var _angular_common_locales_es__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/locales/es */ "./node_modules/@angular/common/locales/es.js");
/* harmony import */ var _angular_common_locales_es__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_es__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _map_content_map_content_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./map-content/map-content.component */ "./src/app/map-content/map-content.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






 // replaces previous Http service




Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["registerLocaleData"])(_angular_common_locales_es__WEBPACK_IMPORTED_MODULE_8___default.a, 'es');
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_5__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyAYw1rqa9mL1-__v8h2CVgsRJmpmP2mP1s'
                }),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"]
            ],
            providers: [_api_service__WEBPACK_IMPORTED_MODULE_7__["ApiService"]],
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"], _map_content_map_content_component__WEBPACK_IMPORTED_MODULE_9__["MapContentComponent"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/globals.ts":
/*!****************************!*\
  !*** ./src/app/globals.ts ***!
  \****************************/
/*! exports provided: Globals */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Globals", function() { return Globals; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// globals.ts

var Globals = /** @class */ (function () {
    function Globals() {
        this.pageSize = 500;
        this.listPageSize = 20;
    }
    Globals = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], Globals);
    return Globals;
}());



/***/ }),

/***/ "./src/app/map-content/map-content.component.ts":
/*!******************************************************!*\
  !*** ./src/app/map-content/map-content.component.ts ***!
  \******************************************************/
/*! exports provided: MapContentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapContentComponent", function() { return MapContentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MapContentComponent = /** @class */ (function () {
    function MapContentComponent(gMaps) {
        this.gMaps = gMaps;
        this.onMapLoad = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    MapContentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.gMaps.getNativeMap().then(function (map) {
            _this.onMapLoad.emit(map);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], MapContentComponent.prototype, "onMapLoad", void 0);
    MapContentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'core-map-content',
            template: '',
        }),
        __metadata("design:paramtypes", [_agm_core__WEBPACK_IMPORTED_MODULE_1__["GoogleMapsAPIWrapper"]])
    ], MapContentComponent);
    return MapContentComponent;
}());



/***/ }),

/***/ "./src/app/obra.ts":
/*!*************************!*\
  !*** ./src/app/obra.ts ***!
  \*************************/
/*! exports provided: SelectOption, Tipo, Barrio, Empresa, Geometry, Obra, Cluster, MapBounds */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectOption", function() { return SelectOption; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tipo", function() { return Tipo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Barrio", function() { return Barrio; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Empresa", function() { return Empresa; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Geometry", function() { return Geometry; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Obra", function() { return Obra; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Cluster", function() { return Cluster; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapBounds", function() { return MapBounds; });
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var SelectOption = /** @class */ (function () {
    function SelectOption(id, nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    return SelectOption;
}());

var Tipo = /** @class */ (function (_super) {
    __extends(Tipo, _super);
    function Tipo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Tipo;
}(SelectOption));

var Barrio = /** @class */ (function (_super) {
    __extends(Barrio, _super);
    function Barrio() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Barrio;
}(SelectOption));

var Empresa = /** @class */ (function (_super) {
    __extends(Empresa, _super);
    function Empresa() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Empresa;
}(SelectOption));

var Geometry = /** @class */ (function () {
    function Geometry(type, coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }
    return Geometry;
}());

var Obra = /** @class */ (function () {
    function Obra(id, nombre, porcentaje, estado, organizacion, monto, fechaInicio, fechaFinalizacion, tipo, geometry) {
        this.id = id;
        this.nombre = nombre;
        this.porcentaje_completado = porcentaje;
        this.organizacion = organizacion;
        this.monto = monto;
        this.tipo = tipo;
        this.geometry = geometry;
        this.estado = estado;
        this.fechaInicio = fechaInicio;
        this.fechaFinalizacion = fechaFinalizacion;
    }
    return Obra;
}());

var Cluster = /** @class */ (function () {
    function Cluster(lat, lng, count, radius) {
        this.lat = lat;
        this.lng = lng;
        this.count = count;
        if (!radius) {
            this.radius = 10;
        }
        this.radius = radius;
    }
    return Cluster;
}());

var MapBounds = /** @class */ (function () {
    function MapBounds(top, bottom, left, right) {
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
    }
    return MapBounds;
}());



/***/ }),

/***/ "./src/assets/markerclusterer.js":
/*!***************************************!*\
  !*** ./src/assets/markerclusterer.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// ==ClosureCompiler==
// @compilation_level ADVANCED_OPTIMIZATIONS
// @externs_url https://raw.githubusercontent.com/google/closure-compiler/master/contrib/externs/maps/google_maps_api_v3.js
// ==/ClosureCompiler==

/**
 * @name MarkerClusterer for Google Maps v3
 * @version version 1.0
 * @author Luke Mahe
 * @fileoverview
 * The library creates and manages per-zoom-level clusters for large amounts of
 * markers.
 * <br/>
 * This is a v3 implementation of the
 * <a href="http://gmaps-utility-library-dev.googlecode.com/svn/tags/markerclusterer/"
 * >v2 MarkerClusterer</a>.
 */

/**
 * @license
 * Copyright 2010 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * A Marker Clusterer that clusters markers.
 *
 * @param {google.maps.Map} map The Google map to attach to.
 * @param {Array.<google.maps.Marker>=} opt_markers Optional markers to add to
 *   the cluster.
 * @param {Object=} opt_options support the following options:
 *     'gridSize': (number) The grid size of a cluster in pixels.
 *     'maxZoom': (number) The maximum zoom level that a marker can be part of a
 *                cluster.
 *     'zoomOnClick': (boolean) Whether the default behaviour of clicking on a
 *                    cluster is to zoom into it.
 *     'averageCenter': (boolean) Whether the center of each cluster should be
 *                      the average of all markers in the cluster.
 *     'minimumClusterSize': (number) The minimum number of markers to be in a
 *                           cluster before the markers are hidden and a count
 *                           is shown.
 *     'styles': (object) An object that has style properties:
 *       'url': (string) The image url.
 *       'height': (number) The image height.
 *       'width': (number) The image width.
 *       'anchor': (Array) The anchor position of the label text.
 *       'textColor': (string) The text color.
 *       'textSize': (number) The text size.
 *       'backgroundPosition': (string) The position of the backgound x, y.
 *       'iconAnchor': (Array) The anchor position of the icon x, y.
 * @constructor
 * @extends google.maps.OverlayView
 */
function MarkerClusterer(map, opt_markers, opt_options) {
  // MarkerClusterer implements google.maps.OverlayView interface. We use the
  // extend function to extend MarkerClusterer with google.maps.OverlayView
  // because it might not always be available when the code is defined so we
  // look for it at the last possible moment. If it doesn't exist now then
  // there is no point going ahead :)
  this.extend(MarkerClusterer, google.maps.OverlayView);
  this.map_ = map;

  /**
   * @type {Array.<google.maps.Marker>}
   * @private
   */
  this.markers_ = [];

  /**
   *  @type {Array.<Cluster>}
   */
  this.clusters_ = [];

  this.sizes = [53, 56, 66, 78, 90];

  /**
   * @private
   */
  this.styles_ = [];

  /**
   * @type {boolean}
   * @private
   */
  this.ready_ = false;

  var options = opt_options || {};

  /**
   * @type {number}
   * @private
   */
  this.gridSize_ = options['gridSize'] || 60;

  /**
   * @private
   */
  this.minClusterSize_ = options['minimumClusterSize'] || 2;


  /**
   * @type {?number}
   * @private
   */
  this.maxZoom_ = options['maxZoom'] || null;

  this.styles_ = options['styles'] || [];

  /**
   * @type {string}
   * @private
   */
  this.imagePath_ = options['imagePath'] ||
      this.MARKER_CLUSTER_IMAGE_PATH_;

  /**
   * @type {string}
   * @private
   */
  this.imageExtension_ = options['imageExtension'] ||
      this.MARKER_CLUSTER_IMAGE_EXTENSION_;

  /**
   * @type {boolean}
   * @private
   */
  this.zoomOnClick_ = true;

  if (options['zoomOnClick'] != undefined) {
    this.zoomOnClick_ = options['zoomOnClick'];
  }

  /**
   * @type {boolean}
   * @private
   */
  this.averageCenter_ = false;

  if (options['averageCenter'] != undefined) {
    this.averageCenter_ = options['averageCenter'];
  }

  this.setupStyles_();

  this.setMap(map);

  /**
   * @type {number}
   * @private
   */
  this.prevZoom_ = this.map_.getZoom();

  // Add the map event listeners
  var that = this;
  google.maps.event.addListener(this.map_, 'zoom_changed', function() {
    var zoom = that.map_.getZoom();

    if (that.prevZoom_ != zoom) {
      that.prevZoom_ = zoom;
      that.resetViewport();
    }
  });

  google.maps.event.addListener(this.map_, 'idle', function() {
    that.redraw();
  });

  // Finally, add the markers
  if (opt_markers && opt_markers.length) {
    this.addMarkers(opt_markers, false);
  }
}


/**
 * The marker cluster image path.
 *
 * @type {string}
 * @private
 */
MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_PATH_ = '../images/m';


/**
 * The marker cluster image path.
 *
 * @type {string}
 * @private
 */
MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_EXTENSION_ = 'png';


/**
 * Extends a objects prototype by anothers.
 *
 * @param {Object} obj1 The object to be extended.
 * @param {Object} obj2 The object to extend with.
 * @return {Object} The new extended object.
 * @ignore
 */
MarkerClusterer.prototype.extend = function(obj1, obj2) {
  return (function(object) {
    for (var property in object.prototype) {
      this.prototype[property] = object.prototype[property];
    }
    return this;
  }).apply(obj1, [obj2]);
};

MarkerClusterer.prototype.AddCluster = function(clat, clng, csize)
{
  if (typeof this.aAddClusterIcons == "undefined"){
      this.aAddClusterIcons = [];
  }

  var clusterlocation = new google.maps.LatLng(clat, clng)
  var CI = new ClusterIcon(new Cluster(this), this.getStyles, this.getGridSize());
  var index = 0;
  var dv = csize;
  while (dv !== 0) {
    dv = parseInt(dv / 10, 10);
    index++;
  }
  var style = this.styles_[index-1];
  CI.setCenter(clusterlocation);
  $.extend(CI, {sums_ : {text : csize, index: index}, url_ : style['url'], width_ : style['width'], height_ : style['height']});
  CI.setMap(this.map_);
  CI.show();
  CI.triggerClusterClick = function(){
    this.map_.setCenter(clusterlocation);
   this.map_.setZoom(this.map_.getZoom()+1);
 }
 this.aAddClusterIcons.push(CI);
}


MarkerClusterer.prototype.RemoveClusters = function()
    {
        if (typeof this.aAddClusterIcons == "undefined"){
            this.aAddClusterIcons = [];
        }

        $.each(this.aAddClusterIcons, function(iIndex, oObj){
            oObj.onRemove();
        });
        this.aAddClusterIcons = [];
    }

/**
 * Implementaion of the interface method.
 * @ignore
 */
MarkerClusterer.prototype.onAdd = function() {
  this.setReady_(true);
};

/**
 * Implementaion of the interface method.
 * @ignore
 */
MarkerClusterer.prototype.draw = function() {};

/**
 * Sets up the styles object.
 *
 * @private
 */
MarkerClusterer.prototype.setupStyles_ = function() {
  if (this.styles_.length) {
    return;
  }

  for (var i = 0, size; size = this.sizes[i]; i++) {
    this.styles_.push({
      url: this.imagePath_ + (i + 1) + '.' + this.imageExtension_,
      height: size,
      width: size
    });
  }
};

/**
 *  Fit the map to the bounds of the markers in the clusterer.
 */
MarkerClusterer.prototype.fitMapToMarkers = function() {
  var markers = this.getMarkers();
  var bounds = new google.maps.LatLngBounds();
  for (var i = 0, marker; marker = markers[i]; i++) {
    bounds.extend(marker.getPosition());
  }

  this.map_.fitBounds(bounds);
};


/**
 *  Sets the styles.
 *
 *  @param {Object} styles The style to set.
 */
MarkerClusterer.prototype.setStyles = function(styles) {
  this.styles_ = styles;
};


/**
 *  Gets the styles.
 *
 *  @return {Object} The styles object.
 */
MarkerClusterer.prototype.getStyles = function() {
  return this.styles_;
};


/**
 * Whether zoom on click is set.
 *
 * @return {boolean} True if zoomOnClick_ is set.
 */
MarkerClusterer.prototype.isZoomOnClick = function() {
  return this.zoomOnClick_;
};

/**
 * Whether average center is set.
 *
 * @return {boolean} True if averageCenter_ is set.
 */
MarkerClusterer.prototype.isAverageCenter = function() {
  return this.averageCenter_;
};


/**
 *  Returns the array of markers in the clusterer.
 *
 *  @return {Array.<google.maps.Marker>} The markers.
 */
MarkerClusterer.prototype.getMarkers = function() {
  return this.markers_;
};


/**
 *  Returns the number of markers in the clusterer
 *
 *  @return {Number} The number of markers.
 */
MarkerClusterer.prototype.getTotalMarkers = function() {
  return this.markers_.length;
};


/**
 *  Sets the max zoom for the clusterer.
 *
 *  @param {number} maxZoom The max zoom level.
 */
MarkerClusterer.prototype.setMaxZoom = function(maxZoom) {
  this.maxZoom_ = maxZoom;
};


/**
 *  Gets the max zoom for the clusterer.
 *
 *  @return {number} The max zoom level.
 */
MarkerClusterer.prototype.getMaxZoom = function() {
  return this.maxZoom_;
};


/**
 *  The function for calculating the cluster icon image.
 *
 *  @param {Array.<google.maps.Marker>} markers The markers in the clusterer.
 *  @param {number} numStyles The number of styles available.
 *  @return {Object} A object properties: 'text' (string) and 'index' (number).
 *  @private
 */
MarkerClusterer.prototype.calculator_ = function(markers, numStyles) {
  var index = 0;
  var count = markers.length;
  var dv = count;
  while (dv !== 0) {
    dv = parseInt(dv / 10, 10);
    index++;
  }

  index = Math.min(index, numStyles);
  return {
    text: count,
    index: index
  };
};


/**
 * Set the calculator function.
 *
 * @param {function(Array, number)} calculator The function to set as the
 *     calculator. The function should return a object properties:
 *     'text' (string) and 'index' (number).
 *
 */
MarkerClusterer.prototype.setCalculator = function(calculator) {
  this.calculator_ = calculator;
};


/**
 * Get the calculator function.
 *
 * @return {function(Array, number)} the calculator function.
 */
MarkerClusterer.prototype.getCalculator = function() {
  return this.calculator_;
};


/**
 * Add an array of markers to the clusterer.
 *
 * @param {Array.<google.maps.Marker>} markers The markers to add.
 * @param {boolean=} opt_nodraw Whether to redraw the clusters.
 */
MarkerClusterer.prototype.addMarkers = function(markers, opt_nodraw) {
  for (var i = 0, marker; marker = markers[i]; i++) {
    this.pushMarkerTo_(marker);
  }
  if (!opt_nodraw) {
    this.redraw();
  }
};


/**
 * Pushes a marker to the clusterer.
 *
 * @param {google.maps.Marker} marker The marker to add.
 * @private
 */
MarkerClusterer.prototype.pushMarkerTo_ = function(marker) {
  marker.isAdded = false;
  if (marker['draggable']) {
    // If the marker is draggable add a listener so we update the clusters on
    // the drag end.
    var that = this;
    google.maps.event.addListener(marker, 'dragend', function() {
      marker.isAdded = false;
      that.repaint();
    });
  }
  this.markers_.push(marker);
};


/**
 * Adds a marker to the clusterer and redraws if needed.
 *
 * @param {google.maps.Marker} marker The marker to add.
 * @param {boolean=} opt_nodraw Whether to redraw the clusters.
 */
MarkerClusterer.prototype.addMarker = function(marker, opt_nodraw) {
  this.pushMarkerTo_(marker);
  if (!opt_nodraw) {
    this.redraw();
  }
};


/**
 * Removes a marker and returns true if removed, false if not
 *
 * @param {google.maps.Marker} marker The marker to remove
 * @return {boolean} Whether the marker was removed or not
 * @private
 */
MarkerClusterer.prototype.removeMarker_ = function(marker) {
  var index = -1;
  if (this.markers_.indexOf) {
    index = this.markers_.indexOf(marker);
  } else {
    for (var i = 0, m; m = this.markers_[i]; i++) {
      if (m == marker) {
        index = i;
        break;
      }
    }
  }

  if (index == -1) {
    // Marker is not in our list of markers.
    return false;
  }

  marker.setMap(null);

  this.markers_.splice(index, 1);

  return true;
};


/**
 * Remove a marker from the cluster.
 *
 * @param {google.maps.Marker} marker The marker to remove.
 * @param {boolean=} opt_nodraw Optional boolean to force no redraw.
 * @return {boolean} True if the marker was removed.
 */
MarkerClusterer.prototype.removeMarker = function(marker, opt_nodraw) {
  var removed = this.removeMarker_(marker);

  if (!opt_nodraw && removed) {
    this.resetViewport();
    this.redraw();
    return true;
  } else {
   return false;
  }
};


/**
 * Removes an array of markers from the cluster.
 *
 * @param {Array.<google.maps.Marker>} markers The markers to remove.
 * @param {boolean=} opt_nodraw Optional boolean to force no redraw.
 */
MarkerClusterer.prototype.removeMarkers = function(markers, opt_nodraw) {
  var removed = false;

  for (var i = 0, marker; marker = markers[i]; i++) {
    var r = this.removeMarker_(marker);
    removed = removed || r;
  }

  if (!opt_nodraw && removed) {
    this.resetViewport();
    this.redraw();
    return true;
  }
};


/**
 * Sets the clusterer's ready state.
 *
 * @param {boolean} ready The state.
 * @private
 */
MarkerClusterer.prototype.setReady_ = function(ready) {
  if (!this.ready_) {
    this.ready_ = ready;
    this.createClusters_();
  }
};


/**
 * Returns the number of clusters in the clusterer.
 *
 * @return {number} The number of clusters.
 */
MarkerClusterer.prototype.getTotalClusters = function() {
  return this.clusters_.length;
};


/**
 * Returns the google map that the clusterer is associated with.
 *
 * @return {google.maps.Map} The map.
 */
MarkerClusterer.prototype.getMap = function() {
  return this.map_;
};


/**
 * Sets the google map that the clusterer is associated with.
 *
 * @param {google.maps.Map} map The map.
 */
MarkerClusterer.prototype.setMap = function(map) {
  this.map_ = map;
};


/**
 * Returns the size of the grid.
 *
 * @return {number} The grid size.
 */
MarkerClusterer.prototype.getGridSize = function() {
  return this.gridSize_;
};


/**
 * Sets the size of the grid.
 *
 * @param {number} size The grid size.
 */
MarkerClusterer.prototype.setGridSize = function(size) {
  this.gridSize_ = size;
};


/**
 * Returns the min cluster size.
 *
 * @return {number} The grid size.
 */
MarkerClusterer.prototype.getMinClusterSize = function() {
  return this.minClusterSize_;
};

/**
 * Sets the min cluster size.
 *
 * @param {number} size The grid size.
 */
MarkerClusterer.prototype.setMinClusterSize = function(size) {
  this.minClusterSize_ = size;
};


/**
 * Extends a bounds object by the grid size.
 *
 * @param {google.maps.LatLngBounds} bounds The bounds to extend.
 * @return {google.maps.LatLngBounds} The extended bounds.
 */
MarkerClusterer.prototype.getExtendedBounds = function(bounds) {
  var projection = this.getProjection();

  // Turn the bounds into latlng.
  var tr = new google.maps.LatLng(bounds.getNorthEast().lat(),
      bounds.getNorthEast().lng());
  var bl = new google.maps.LatLng(bounds.getSouthWest().lat(),
      bounds.getSouthWest().lng());

  // Convert the points to pixels and the extend out by the grid size.
  var trPix = projection.fromLatLngToDivPixel(tr);
  trPix.x += this.gridSize_;
  trPix.y -= this.gridSize_;

  var blPix = projection.fromLatLngToDivPixel(bl);
  blPix.x -= this.gridSize_;
  blPix.y += this.gridSize_;

  // Convert the pixel points back to LatLng
  var ne = projection.fromDivPixelToLatLng(trPix);
  var sw = projection.fromDivPixelToLatLng(blPix);

  // Extend the bounds to contain the new bounds.
  bounds.extend(ne);
  bounds.extend(sw);

  return bounds;
};


/**
 * Determins if a marker is contained in a bounds.
 *
 * @param {google.maps.Marker} marker The marker to check.
 * @param {google.maps.LatLngBounds} bounds The bounds to check against.
 * @return {boolean} True if the marker is in the bounds.
 * @private
 */
MarkerClusterer.prototype.isMarkerInBounds_ = function(marker, bounds) {
  return bounds.contains(marker.getPosition());
};


/**
 * Clears all clusters and markers from the clusterer.
 */
MarkerClusterer.prototype.clearMarkers = function() {
  this.resetViewport(true);

  // Set the markers a empty array.
  this.markers_ = [];
};


/**
 * Clears all existing clusters and recreates them.
 * @param {boolean} opt_hide To also hide the marker.
 */
MarkerClusterer.prototype.resetViewport = function(opt_hide) {
  // Remove all the clusters
  for (var i = 0, cluster; cluster = this.clusters_[i]; i++) {
    cluster.remove();
  }

  // Reset the markers to not be added and to be invisible.
  for (var i = 0, marker; marker = this.markers_[i]; i++) {
    marker.isAdded = false;
    if (opt_hide) {
      marker.setMap(null);
    }
  }

  this.clusters_ = [];
};

/**
 *
 */
MarkerClusterer.prototype.repaint = function() {
  var oldClusters = this.clusters_.slice();
  this.clusters_.length = 0;
  this.resetViewport();
  this.redraw();

  // Remove the old clusters.
  // Do it in a timeout so the other clusters have been drawn first.
  window.setTimeout(function() {
    for (var i = 0, cluster; cluster = oldClusters[i]; i++) {
      cluster.remove();
    }
  }, 0);
};


/**
 * Redraws the clusters.
 */
MarkerClusterer.prototype.redraw = function() {
  this.createClusters_();
};


/**
 * Calculates the distance between two latlng locations in km.
 * @see http://www.movable-type.co.uk/scripts/latlong.html
 *
 * @param {google.maps.LatLng} p1 The first lat lng point.
 * @param {google.maps.LatLng} p2 The second lat lng point.
 * @return {number} The distance between the two points in km.
 * @private
*/
MarkerClusterer.prototype.distanceBetweenPoints_ = function(p1, p2) {
  if (!p1 || !p2) {
    return 0;
  }

  var R = 6371; // Radius of the Earth in km
  var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
  var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d;
};


/**
 * Add a marker to a cluster, or creates a new cluster.
 *
 * @param {google.maps.Marker} marker The marker to add.
 * @private
 */
MarkerClusterer.prototype.addToClosestCluster_ = function(marker) {
  var distance = 40000; // Some large number
  var clusterToAddTo = null;
  var pos = marker.getPosition();
  for (var i = 0, cluster; cluster = this.clusters_[i]; i++) {
    var center = cluster.getCenter();
    if (center) {
      var d = this.distanceBetweenPoints_(center, marker.getPosition());
      if (d < distance) {
        distance = d;
        clusterToAddTo = cluster;
      }
    }
  }

  if (clusterToAddTo && clusterToAddTo.isMarkerInClusterBounds(marker)) {
    clusterToAddTo.addMarker(marker);
  } else {
    var cluster = new Cluster(this);
    cluster.addMarker(marker);
    this.clusters_.push(cluster);
  }
};


/**
 * Creates the clusters.
 *
 * @private
 */
MarkerClusterer.prototype.createClusters_ = function() {
  if (!this.ready_) {
    return;
  }

  // Get our current map view bounds.
  // Create a new bounds object so we don't affect the map.
  var mapBounds = new google.maps.LatLngBounds(this.map_.getBounds().getSouthWest(),
      this.map_.getBounds().getNorthEast());
  var bounds = this.getExtendedBounds(mapBounds);

  for (var i = 0, marker; marker = this.markers_[i]; i++) {
    if (!marker.isAdded && this.isMarkerInBounds_(marker, bounds)) {
      this.addToClosestCluster_(marker);
    }
  }
};


/**
 * A cluster that contains markers.
 *
 * @param {MarkerClusterer} markerClusterer The markerclusterer that this
 *     cluster is associated with.
 * @constructor
 * @ignore
 */
function Cluster(markerClusterer) {
  this.markerClusterer_ = markerClusterer;
  this.map_ = markerClusterer.getMap();
  this.gridSize_ = markerClusterer.getGridSize();
  this.minClusterSize_ = markerClusterer.getMinClusterSize();
  this.averageCenter_ = markerClusterer.isAverageCenter();
  this.center_ = null;
  this.markers_ = [];
  this.bounds_ = null;
  this.clusterIcon_ = new ClusterIcon(this, markerClusterer.getStyles(),
      markerClusterer.getGridSize());
}

/**
 * Determins if a marker is already added to the cluster.
 *
 * @param {google.maps.Marker} marker The marker to check.
 * @return {boolean} True if the marker is already added.
 */
Cluster.prototype.isMarkerAlreadyAdded = function(marker) {
  if (this.markers_.indexOf) {
    return this.markers_.indexOf(marker) != -1;
  } else {
    for (var i = 0, m; m = this.markers_[i]; i++) {
      if (m == marker) {
        return true;
      }
    }
  }
  return false;
};


/**
 * Add a marker the cluster.
 *
 * @param {google.maps.Marker} marker The marker to add.
 * @return {boolean} True if the marker was added.
 */
Cluster.prototype.addMarker = function(marker) {
  if (this.isMarkerAlreadyAdded(marker)) {
    return false;
  }

  if (!this.center_) {
    this.center_ = marker.getPosition();
    this.calculateBounds_();
  } else {
    if (this.averageCenter_) {
      var l = this.markers_.length + 1;
      var lat = (this.center_.lat() * (l-1) + marker.getPosition().lat()) / l;
      var lng = (this.center_.lng() * (l-1) + marker.getPosition().lng()) / l;
      this.center_ = new google.maps.LatLng(lat, lng);
      this.calculateBounds_();
    }
  }

  marker.isAdded = true;
  this.markers_.push(marker);

  var len = this.markers_.length;
  if (len < this.minClusterSize_ && marker.getMap() != this.map_) {
    // Min cluster size not reached so show the marker.
    marker.setMap(this.map_);
  }

  if (len == this.minClusterSize_) {
    // Hide the markers that were showing.
    for (var i = 0; i < len; i++) {
      this.markers_[i].setMap(null);
    }
  }

  if (len >= this.minClusterSize_) {
    marker.setMap(null);
  }

  this.updateIcon();
  return true;
};


/**
 * Returns the marker clusterer that the cluster is associated with.
 *
 * @return {MarkerClusterer} The associated marker clusterer.
 */
Cluster.prototype.getMarkerClusterer = function() {
  return this.markerClusterer_;
};


/**
 * Returns the bounds of the cluster.
 *
 * @return {google.maps.LatLngBounds} the cluster bounds.
 */
Cluster.prototype.getBounds = function() {
  var bounds = new google.maps.LatLngBounds(this.center_, this.center_);
  var markers = this.getMarkers();
  for (var i = 0, marker; marker = markers[i]; i++) {
    bounds.extend(marker.getPosition());
  }
  return bounds;
};


/**
 * Removes the cluster
 */
Cluster.prototype.remove = function() {
  this.clusterIcon_.remove();
  this.markers_.length = 0;
  delete this.markers_;
};


/**
 * Returns the center of the cluster.
 *
 * @return {number} The cluster center.
 */
Cluster.prototype.getSize = function() {
  return this.markers_.length;
};


/**
 * Returns the center of the cluster.
 *
 * @return {Array.<google.maps.Marker>} The cluster center.
 */
Cluster.prototype.getMarkers = function() {
  return this.markers_;
};


/**
 * Returns the center of the cluster.
 *
 * @return {google.maps.LatLng} The cluster center.
 */
Cluster.prototype.getCenter = function() {
  return this.center_;
};


/**
 * Calculated the extended bounds of the cluster with the grid.
 *
 * @private
 */
Cluster.prototype.calculateBounds_ = function() {
  var bounds = new google.maps.LatLngBounds(this.center_, this.center_);
  this.bounds_ = this.markerClusterer_.getExtendedBounds(bounds);
};


/**
 * Determines if a marker lies in the clusters bounds.
 *
 * @param {google.maps.Marker} marker The marker to check.
 * @return {boolean} True if the marker lies in the bounds.
 */
Cluster.prototype.isMarkerInClusterBounds = function(marker) {
  return this.bounds_.contains(marker.getPosition());
};


/**
 * Returns the map that the cluster is associated with.
 *
 * @return {google.maps.Map} The map.
 */
Cluster.prototype.getMap = function() {
  return this.map_;
};


/**
 * Updates the cluster icon
 */
Cluster.prototype.updateIcon = function() {
  var zoom = this.map_.getZoom();
  var mz = this.markerClusterer_.getMaxZoom();

  if (mz && zoom > mz) {
    // The zoom is greater than our max zoom so show all the markers in cluster.
    for (var i = 0, marker; marker = this.markers_[i]; i++) {
      marker.setMap(this.map_);
    }
    return;
  }

  if (this.markers_.length < this.minClusterSize_) {
    // Min cluster size not yet reached.
    this.clusterIcon_.hide();
    return;
  }

  var numStyles = this.markerClusterer_.getStyles().length;
  var sums = this.markerClusterer_.getCalculator()(this.markers_, numStyles);
  this.clusterIcon_.setCenter(this.center_);
  this.clusterIcon_.setSums(sums);
  this.clusterIcon_.show();
};


/**
 * A cluster icon
 *
 * @param {Cluster} cluster The cluster to be associated with.
 * @param {Object} styles An object that has style properties:
 *     'url': (string) The image url.
 *     'height': (number) The image height.
 *     'width': (number) The image width.
 *     'anchor': (Array) The anchor position of the label text.
 *     'textColor': (string) The text color.
 *     'textSize': (number) The text size.
 *     'backgroundPosition: (string) The background postition x, y.
 * @param {number=} opt_padding Optional padding to apply to the cluster icon.
 * @constructor
 * @extends google.maps.OverlayView
 * @ignore
 */
function ClusterIcon(cluster, styles, opt_padding) {
  cluster.getMarkerClusterer().extend(ClusterIcon, google.maps.OverlayView);

  this.styles_ = styles;
  this.padding_ = opt_padding || 0;
  this.cluster_ = cluster;
  this.center_ = null;
  this.map_ = cluster.getMap();
  this.div_ = null;
  this.sums_ = null;
  this.visible_ = false;

  this.setMap(this.map_);
}


/**
 * Triggers the clusterclick event and zoom's if the option is set.
 *
 * @param {google.maps.MouseEvent} event The event to propagate
 */
ClusterIcon.prototype.triggerClusterClick = function(event) {
  var markerClusterer = this.cluster_.getMarkerClusterer();

  // Trigger the clusterclick event.
  google.maps.event.trigger(markerClusterer, 'clusterclick', this.cluster_, event);

  if (markerClusterer.isZoomOnClick()) {
    // Zoom into the cluster.
    this.map_.fitBounds(this.cluster_.getBounds());
  }
};


/**
 * Adding the cluster icon to the dom.
 * @ignore
 */
ClusterIcon.prototype.onAdd = function() {
  this.div_ = document.createElement('DIV');
  if (this.visible_) {
    var pos = this.getPosFromLatLng_(this.center_);
    this.div_.style.cssText = this.createCss(pos);
    this.div_.innerHTML = this.sums_.text;
  }

  var panes = this.getPanes();
  panes.overlayMouseTarget.appendChild(this.div_);

  var that = this;
  var isDragging = false;
  google.maps.event.addDomListener(this.div_, 'click', function(event) {
    // Only perform click when not preceded by a drag
    if (!isDragging) {
      that.triggerClusterClick(event);
    }
  });
  google.maps.event.addDomListener(this.div_, 'mousedown', function() {
    isDragging = false;
  });
  google.maps.event.addDomListener(this.div_, 'mousemove', function() {
    isDragging = true;
  });
};


/**
 * Returns the position to place the div dending on the latlng.
 *
 * @param {google.maps.LatLng} latlng The position in latlng.
 * @return {google.maps.Point} The position in pixels.
 * @private
 */
ClusterIcon.prototype.getPosFromLatLng_ = function(latlng) {
  var pos = this.getProjection().fromLatLngToDivPixel(latlng);

  if (typeof this.iconAnchor_ === 'object' && this.iconAnchor_.length === 2) {
    pos.x -= this.iconAnchor_[0];
    pos.y -= this.iconAnchor_[1];
  } else {
    pos.x -= parseInt(this.width_ / 2, 10);
    pos.y -= parseInt(this.height_ / 2, 10);
  }
  return pos;
};


/**
 * Draw the icon.
 * @ignore
 */
ClusterIcon.prototype.draw = function() {
  if (this.visible_) {
    var pos = this.getPosFromLatLng_(this.center_);
    this.div_.style.top = pos.y + 'px';
    this.div_.style.left = pos.x + 'px';
  }
};


/**
 * Hide the icon.
 */
ClusterIcon.prototype.hide = function() {
  if (this.div_) {
    this.div_.style.display = 'none';
  }
  this.visible_ = false;
};


/**
 * Position and show the icon.
 */
ClusterIcon.prototype.show = function() {
  if (this.div_) {
    var pos = this.getPosFromLatLng_(this.center_);
    this.div_.style.cssText = this.createCss(pos);
    this.div_.style.display = '';
  }
  this.visible_ = true;
};


/**
 * Remove the icon from the map
 */
ClusterIcon.prototype.remove = function() {
  this.setMap(null);
};


/**
 * Implementation of the onRemove interface.
 * @ignore
 */
ClusterIcon.prototype.onRemove = function() {
  if (this.div_ && this.div_.parentNode) {
    this.hide();
    this.div_.parentNode.removeChild(this.div_);
    this.div_ = null;
  }
};


/**
 * Set the sums of the icon.
 *
 * @param {Object} sums The sums containing:
 *   'text': (string) The text to display in the icon.
 *   'index': (number) The style index of the icon.
 */
ClusterIcon.prototype.setSums = function(sums) {
  this.sums_ = sums;
  this.text_ = sums.text;
  this.index_ = sums.index;
  if (this.div_) {
    this.div_.innerHTML = sums.text;
  }

  this.useStyle();
};


/**
 * Sets the icon to the the styles.
 */
ClusterIcon.prototype.useStyle = function() {
  var index = Math.max(0, this.sums_.index - 1);
  index = Math.min(this.styles_.length - 1, index);
  var style = this.styles_[index];
  this.url_ = style['url'];
  this.height_ = style['height'];
  this.width_ = style['width'];
  this.textColor_ = style['textColor'];
  this.anchor_ = style['anchor'];
  this.textSize_ = style['textSize'];
  this.backgroundPosition_ = style['backgroundPosition'];
  this.iconAnchor_ = style['iconAnchor'];
};


/**
 * Sets the center of the icon.
 *
 * @param {google.maps.LatLng} center The latlng to set as the center.
 */
ClusterIcon.prototype.setCenter = function(center) {
  this.center_ = center;
};


/**
 * Create the css text based on the position of the icon.
 *
 * @param {google.maps.Point} pos The position.
 * @return {string} The css style text.
 */
ClusterIcon.prototype.createCss = function(pos) {
  var style = [];
  style.push('background-image:url(' + this.url_ + ');');
  var backgroundPosition = this.backgroundPosition_ ? this.backgroundPosition_ : '0 0';
  style.push('background-position:' + backgroundPosition + ';');

  if (typeof this.anchor_ === 'object') {
    if (typeof this.anchor_[0] === 'number' && this.anchor_[0] > 0 &&
        this.anchor_[0] < this.height_) {
      style.push('height:' + (this.height_ - this.anchor_[0]) +
          'px; padding-top:' + this.anchor_[0] + 'px;');
    } else if (typeof this.anchor_[0] === 'number' && this.anchor_[0] < 0 &&
        -this.anchor_[0] < this.height_) {
      style.push('height:' + this.height_ + 'px; line-height:' + (this.height_ + this.anchor_[0]) +
          'px;');
    } else {
      style.push('height:' + this.height_ + 'px; line-height:' + this.height_ +
          'px;');
    }
    if (typeof this.anchor_[1] === 'number' && this.anchor_[1] > 0 &&
        this.anchor_[1] < this.width_) {
      style.push('width:' + (this.width_ - this.anchor_[1]) +
          'px; padding-left:' + this.anchor_[1] + 'px;');
    } else {
      style.push('width:' + this.width_ + 'px; text-align:center;');
    }
  } else {
    style.push('height:' + this.height_ + 'px; line-height:' +
        this.height_ + 'px; width:' + this.width_ + 'px; text-align:center;');
  }

  var txtColor = this.textColor_ ? this.textColor_ : 'black';
  var txtSize = this.textSize_ ? this.textSize_ : 11;

  style.push('cursor:pointer; top:' + pos.y + 'px; left:' +
      pos.x + 'px; color:' + txtColor + '; position:absolute; font-size:' +
      txtSize + 'px; font-family:Arial,sans-serif; font-weight:bold');
  return style.join('');
};


// Export Symbols for Closure
// If you are not going to compile with closure then you can remove the
// code below.
window['MarkerClusterer'] = MarkerClusterer;
MarkerClusterer.prototype['addMarker'] = MarkerClusterer.prototype.addMarker;
MarkerClusterer.prototype['addMarkers'] = MarkerClusterer.prototype.addMarkers;
MarkerClusterer.prototype['clearMarkers'] =
    MarkerClusterer.prototype.clearMarkers;
MarkerClusterer.prototype['fitMapToMarkers'] =
    MarkerClusterer.prototype.fitMapToMarkers;
MarkerClusterer.prototype['getCalculator'] =
    MarkerClusterer.prototype.getCalculator;
MarkerClusterer.prototype['getGridSize'] =
    MarkerClusterer.prototype.getGridSize;
MarkerClusterer.prototype['getExtendedBounds'] =
    MarkerClusterer.prototype.getExtendedBounds;
MarkerClusterer.prototype['getMap'] = MarkerClusterer.prototype.getMap;
MarkerClusterer.prototype['getMarkers'] = MarkerClusterer.prototype.getMarkers;
MarkerClusterer.prototype['getMaxZoom'] = MarkerClusterer.prototype.getMaxZoom;
MarkerClusterer.prototype['getStyles'] = MarkerClusterer.prototype.getStyles;
MarkerClusterer.prototype['getTotalClusters'] =
    MarkerClusterer.prototype.getTotalClusters;
MarkerClusterer.prototype['getTotalMarkers'] =
    MarkerClusterer.prototype.getTotalMarkers;
MarkerClusterer.prototype['redraw'] = MarkerClusterer.prototype.redraw;
MarkerClusterer.prototype['removeMarker'] =
    MarkerClusterer.prototype.removeMarker;
MarkerClusterer.prototype['removeMarkers'] =
    MarkerClusterer.prototype.removeMarkers;
MarkerClusterer.prototype['resetViewport'] =
    MarkerClusterer.prototype.resetViewport;
MarkerClusterer.prototype['repaint'] =
    MarkerClusterer.prototype.repaint;
MarkerClusterer.prototype['setCalculator'] =
    MarkerClusterer.prototype.setCalculator;
MarkerClusterer.prototype['setGridSize'] =
    MarkerClusterer.prototype.setGridSize;
MarkerClusterer.prototype['setMaxZoom'] =
    MarkerClusterer.prototype.setMaxZoom;
MarkerClusterer.prototype['onAdd'] = MarkerClusterer.prototype.onAdd;
MarkerClusterer.prototype['draw'] = MarkerClusterer.prototype.draw;

Cluster.prototype['getCenter'] = Cluster.prototype.getCenter;
Cluster.prototype['getSize'] = Cluster.prototype.getSize;
Cluster.prototype['getMarkers'] = Cluster.prototype.getMarkers;

ClusterIcon.prototype['onAdd'] = ClusterIcon.prototype.onAdd;
ClusterIcon.prototype['draw'] = ClusterIcon.prototype.draw;
ClusterIcon.prototype['onRemove'] = ClusterIcon.prototype.onRemove;


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/maxi/Escritorio/Muni/obras de terceros/obras-terceros/arboladoApp/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map
