$( document ).ready(function() {

  var scrollHash = function(div, offset){
    $('html, body').animate({
        scrollTop: $(div).offset().top - offset
    }, 1000);
  }

  var getDato = function (rel, div, callback) {
      $.ajax({
          url: rel+"?TPL=template2017",
          cache: false,
          success: function (data) {
              $(div).html(data);
          },
          complete: function () {
              $('.loading-indicator').hide();
              $(div).removeClass('empty');
              scrollHash("#infoDatos", 175);
              if(callback!=null){
                // openMeta($(callback));
              }
          }
      });
  };

  var url = document.location.toString();

  function addCommas(nStr)
  {
      nStr += '';
      x = nStr.split('.');
      x1 = x[0];
      x2 = x.length > 1 ? ',' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
          x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      return x1 + x2;
  }

  $.ajax({
      url: "https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-publicas/tipos-obras-publicas/",
      cache: false,
      success: function (data) {
        console.log(data);
        $.each(data.results, function(i, tipo) {
            tipos[tipo.nombre] = "#"+tipo.color.substr(2,7)
        })
      }
    });

  if (url.match('#')) {
    var string = url.split('#')[1];
    var type = string.split('-')[0];
    if (type == "obra") {
      var id_obra = string.split('-')[1];
      $.ajax({
          url: "https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-publicas/obras-publicas/"+id_obra,
          cache: false,
          success: function (results) {
              var propiedades = results.properties;
              $('#tituloObra').html(propiedades.nombre);
              $('#expedienteNro').html(propiedades.expediente_origen);
              $('#decretoNro').html(propiedades.decreto);
              $('#tipoObra').html(propiedades.tipo);
              $.ajax({
                  url: "https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-publicas/tipos-obras-publicas/"+propiedades.tipo_id,
                  cache: false,
                  success: function (data2) {
                    $('#tipoObra').css('background-color', "#"+data2.color.substr(2,7));
                  }
              });

              map.data.addGeoJson(results);
              var coords = results.geometry.coordinates[0][0];

              map.setCenter({
            		lat : coords[1],
            		lng : coords[0]
            	});
              var formattedDate = new Date(propiedades.fecha_inicio);
              var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
  "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre"];
              var d = formattedDate.getDate();
              var m =  formattedDate.getMonth();
              var y = formattedDate.getFullYear();

              $("#fechaInicio").html(d + " de " + monthNames[m] + " de " + y);
              $('#montoPresupuesto').html(addCommas(propiedades.monto));
              $('#contratista').html(propiedades.organizacion);
              // var obras = propieda.features.map(dato => Object.assign({id: dato.id}, dato.properties));
              $.each(propiedades.barrios, function(i, item) {
                  $('#barrios').append(item.nombre+', ');
              });
              var index = 0;
              $.each(propiedades.trazados, function(i, item) {
                  $.each(item.adjuntos, function(j, adj) {

                    var appnd_txt = '<li data-target="#carousel-images" data-slide-to="'+index+'" href="javascript:void(0);"';
                    var item_txt = '<div class="item';
                    if(j==0){
                       appnd_txt+='class="active"></li>';
                       item_txt+= ' active">';
                    }else{
                      appnd_txt+='></li>';
                      item_txt+= '">';
                    }
                    item_txt += '<div class="imgBack" style="background-image:url('+adj.foto.thumbnail_500+');"></div>'

                    if(!jQuery.isEmptyObject(adj.foto)){
                      $('#indicators').append(appnd_txt);
                      $('#items').append(item_txt);
                      index +=1;
                    }


                  })
              })
              if(index<2){
                $('#controls').hide();
              }
              $('.carousel-inner').css('max-height', $('.colInfo').height()-10);
              $('.imgBack').css('height', $('.colInfo').height()-10);
          }
      });
    }
  }
});
