var tipos = {};

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function insertParam(key, value) {
        key = escape(key); value = escape(value);
        var newurlQuery = '?' + key + '=' + value;
        if (history.pushState) {
             var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + newurlQuery;
             window.history.pushState({path:newurl},'',newurl);
         }
    }

Vue.component('muni-icono', {
    template: `<img :alt="alt" :src="'https://www.cordoba.gob.ar/wp-content/uploads/2017/11/'+id+'.png'" class="icono" />`,
    props: ['id', 'alt']
});

Vue.component('progress-bar', {
    template: `<div class="progress"><div class="progress-bar progress-bar-info" role="progressbar" :aria-valuenow="valor" aria-valuemin="0" aria-valuemax="100" :style="'width: '+valor+'%;'"><span class="vc_label_units">{{valor}}%</span></div></div>`,
    props: ['valor', 'color']
});

Vue.component('muni-nav-cards', {
  template: `<nav class="nav selects">
  <menu>
    <ul class="nav__controls">
      <li class="nav__label">
        <label for="busquedaTexto" class="sr-only">Buscar por texto</label>
        <input v-model="texto" type="text" name="texto" id="busquedaTexto" :placeholder="textoBusqueda">
      </li>
      <li class="nav__label nav__select">
        <label for="tipoObra" class="sr-only">Tipo de Obra</label>
        <select v-model="tipo" name="tipoObra" id="tipoObra">
          <option :value="null">Tipo de obra</option>
          <option v-for="tipo in tipos" key="tipo.id" :selected="tipo.id == this.tipoActivo ? 'selected' : ''" :value="tipo.id">{{tipo.nombre}}</option>
        </select>
      </li>
      <li class="nav__label nav__select">
        <label for="progreso" class="sr-only">Estado de Obra</label>
        <select v-model="progreso" name="progreso" id="progreso">
          <option :value="null">Estado de Obra</option>
          <option value="proyecto">En proyecto</option>
          <option value="licitadas">Licitadas</option>
          <option value="en_progreso">En progreso</option>
          <option value="finalizadas">Finalizadas</option>
        </select>
      </li>
      <li class="nav__label nav__select">
        <label for="barrio" class="sr-only">Barrio</label>
        <select v-model="barrio" name="barrio" id="barrio">
          <option :value="null">Barrio</option>
          <option v-for="barrio in barrios" key="barrio.id" :value="barrio.id">{{barrio.nombre}}</option>
        </select>
      </li>
      <li class="nav__label">
        <div type="button" class="btn-iniciar" @click="filterObras"> Buscar </div>
      </li>
    </ul>
    </menu></nav>`,
    props: ['tipos', 'barrios', 'paginas', 'actual'],
    data () {
      return {
        texto: null,
        tipo: null,
        progreso: null,
        barrio: null,
        textoBusqueda: getParameterByName('texto')!=null? getParameterByName('texto') : 'Ingresá tu búsqueda'
      }
    },
    mounted: function(){
      var urlHash = document.location.toString();
      var tS = null;
      var pS = null;
      var bS = null;
      if (urlHash.match('#')) {
        var string = urlHash.split('#')[1];
        var filters = string.split('-');

        for(filter in filters){
          var type = filters[filter].split('_')[0];
          var value = filters[filter].split('_')[1];
          if(type == "t"){
            this.tipo = value ? value : null;
          }else if(type == "p"){
            this.progreso = value ? value : null;
          }else if(type == "b"){
            this.barrio = value ? value : null;
          }
        }
      }
    },
    methods: {
      claseActiva(pagina) {
        return this.actual == pagina ? 'nav__label nav__label--active' : 'nav__label';
      },
      filterObras(){

        this.$emit('cambioTexto', this.texto);
        this.$emit('cambioTipo', this.tipo);
        this.$emit('cambioProgreso', this.progreso);
        this.$emit('cambioBarrio', this.barrio);
        this.$emit('buscarFiltros');

        var stringHash = "";
        if(this.texto){
          insertParam('texto',this.texto);
        }
        if(getParameterByName('texto')!=null && !this.texto){
          if (history.pushState) {
               var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname;
               window.history.pushState({path:newurl},'',newurl);
           }
        }
        if(this.tipo){
          stringHash += "t_"+this.tipo+"-";
        }
        if(this.progreso){
          stringHash += "p_"+this.progreso+"-";
        }
        if(this.barrio){
          stringHash += "b_"+this.barrio;
        }
        window.location.hash = stringHash;
      }
    }
});
Vue.component('muni-nav-pag', {
  template: `<nav class="nav">
  <menu>
    <ul class="pagination" v-show="paginas > 1">
        <li ><a href="#" @click="$emit('volverPagina')"> anterior</a></li>
        <li :class="claseActiva(i)" v-for="i in paginas">
          <a href="#" @click="$emit('iraPagina', i)">{{i}}</a>
        </li>
        <li class=""><a href="#" @click="$emit('pasarPagina')">siguiente</a></li>
      </li>
    </ul>
    </menu>
    </nav>`,
    props: ['tipos', 'barrios', 'paginas', 'actual'],
    data () {
      return {
        texto: null,
        tipo: null,
        progreso: null,
        barrio: null
      }
    },
    methods: {
      claseActiva(pagina) {
        return this.actual == pagina ? 'active' : 'item';
      }
    },
    watch: {
      texto: function (textoSeleccionado) {
        this.$emit('cambioTexto', textoSeleccionado);
      },
      tipo: function (tipoSeleccionado) {
        this.$emit('cambioTipo', tipoSeleccionado);
      },
      progreso: function (progresoSeleccionado) {
        this.$emit('cambioProgreso', progresoSeleccionado);
      },
      barrio: function (barrioSeleccionado) {
        this.$emit('cambioBarrio', barrioSeleccionado);
      }
    }
});
Vue.component('muni-card', {
  template: `<a :href='"estado-de-obra-terceros#obra-"+obra.id'>
    <div class="card-header" :style="getImagen()">
        <span v-if="obra.porcentaje_completado == '0.00'" class="tipo tipo--licitada">{{clasificacion}}</span>
        <span class="tipo">{{obra.tipo}}</span>
    </div>
    <div class="card-title"><h3>{{obra.nombre}}</h3></div>
    <progress-bar :color="'sky'" :valor="obra.porcentaje_completado.split('.')[0]"></progress-bar>
    <div class="fecha" v-if="mostrarFecha" v-html="fecha()"></div>
    <div class="card-summary">
        <p class="barrios" v-if="obra.barrios.length > 0"><strong>Barrios:</strong><br><span v-for="barrio in obra.barrios">{{barrio.nombre}}</span></p>
    </div>
  </a>`,
  data: function() {
    return {
      clasificacion: null,
      mostrarFecha: true
    }
  },
  mounted: function() {
    if (this.obra.porcentaje_completado == '0.00') {
      this.clasificacion = this.obra.organizacion.toLowerCase() === 'sin adjudicar' ? 'Proyecto' : 'Licitada';
    } else {
      this.clasificacion = 'Otro';
    }
    this.mostrarFecha = this.clasificacion != 'Proyecto' && this.obra.porcentaje_completado != '100.00';
  },
  methods: {
    fecha() {
        const inicio = this.formatearFecha(this.obra.fecha_inicio);
        const fin = this.formatearFecha(this.obra.fecha_finalizacion_estimada);
        return `<span>Inicio<br/>${inicio}</span><span>Fin Estimado<br/>${fin}</span>`;
    },
    getImagen() {
      let imagen = 'https://www.cordoba.gob.ar/wp-content/uploads/2017/12/en-construccion-default.png';
      if(this.obra.trazados!=null){
        for(let i = 0; i < this.obra.trazados.length; i++) {
          const trazado = this.obra.trazados[i];
          if(trazado.adjuntos!=null){
            for(let j = 0; j < trazado.adjuntos.length; j++) {
              const adjunto = trazado.adjuntos[j];
              if (adjunto.publicado != false && adjunto.foto && adjunto.foto.thumbnail_500 !== undefined) {
                imagen = adjunto.foto.thumbnail_500;
                break;
              }
            }
          }   
        }
      } 
      return { backgroundImage: 'url(' + imagen +')' };
    },
    formatearTitulo() {
      if (this.obra.decreto == 00) {
        return "<strong>Decreto:</strong> <span>Sin información</span>";
      }
      return "<strong>Decreto:</strong> <span>" + this.obra.decreto + "</span><br/><strong><abbr title='Número de expediente'>N° Exp.</abbr>:</strong> " + this.obra.expediente_origen;
    },
    formatearFecha(fecha) {
        const date = new Date(fecha);
        return date.toLocaleDateString();
    },
    nombreToUrl(nombre) {
      return nombre.toLowerCase().replace(/[^a-z0-9]/g, '-').replace(/\-+/g, '-');
    }
  },
  props: ['obra']
})

Vue.component('muni-cards', {
  template: `<section>
  <div class="container-fluid" style="margin-top:10px ">
    <div class="row">
      <div class="col-md-9 nopadding colMapa col-header cSeparador">
      <h1 class="tituloObras"><img src="/static/obras/images/terceros/icon.png" class="pulse infinite animated" height="75" alt="logo de obras de terceros"/>Obras de Terceros</h1>
      </div>
      <div class="col-md-3 col-header">
        
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 nopadding colMapa">
          <div id="mapa"></div>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-6 colMapa cSeparador">
            <div class="row">
              <div class="cardMedicion col-xs-12 col-sm-4 col-md-12">
                <h2 class="medicion" style="text-align: center;">TOTAL al <span id="fechaActual"></span></h2>
              </div>
              <div class="cardMedicion col-xs-12 col-sm-4 col-md-12">
                <div class="row">
                  <div class="col-md-4">
                    <div class="imgMedicion">
                      <img src="/static/obras/images/terceros/inversion.png" height="60" class="pulse infinite animated" alt="icono representando inversiones"/>
                    </div>
                  </div>
                  <div class="col-md-8">
                    <h2 class="medicion">$
                      <span id="financNumero">0</span>
                    </h2>
                    <p class="descripcionMed">
                      Inversión
                    </p>
                  </div>
                </div>
              </div>

              <div class="cardMedicion col-xs-12 col-sm-4 col-md-12">
                <div class="row">
                  <div class="col-md-4">
                    <div class="imgMedicion">
                      <img src="/static/obras/images/terceros/obras.png" height="60" class="pulse infinite animated" alt="icono representando obras"/>
                    </div>
                  </div>
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-6">
                        <h2 class="medicion">
                        <span id="obrasNumero">0</span>
                        </h2>
                        <p class="descripcionMed">
                          Obras
                        </p>
                      </div>
                      <div class="col-md-6" style="position: relative;">
                        <div class="verticalDivider">/</div>
                        <h2 class="medicion">
                        <span id="frenteObrasNumero">0</span>
                        </h2>
                        <p class="descripcionMed">
                          Frentes de Obras
                        </p>
                      </div>
                    </div>

                  </div>
                </div>
              </div>

              <div class="cardMedicion col-xs-12 col-sm-4 col-md-12">
                <div class="row">
                  <div class="col-md-4">
                    <div class="imgMedicion">
                      <img src="/static/obras/images/terceros/finalizadas.png" height="70" style="margin-right: -20px;" class="pulse infinite animated" alt="icono representando obras finalizadas" />
                    </div>
                  </div>
                  <div class="col-md-8" style="margin-top:10px;">
                    <h2 class="medicion">
                      <span id="finalizadasNumero">0</span>
                    %</h2>
                    <p class="descripcionMed">
                      Finalizadas
                    </p>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <div class="col-md-6 colMapa">
          <p class="tituloSelects">Buscá obras en la ciudad</p>
            <muni-nav-cards :actual="paginaActual" :paginas="ultimaPagina" @cambioTexto="cambioTexto" @cambioTipo="cambioTipo" @cambioBarrio="cambioBarrio" @cambioProgreso="cambioProgreso" :barrios="$root.barrios" :tipos="$root.tiposObras" @iraPagina="iraPagina" @volverPagina="volverPagina" @pasarPagina="pasarPagina"  @buscarFiltros="buscarFiltros"></muni-nav-cards>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="resultados" v-if="obras.length > 0">
      <div class="paginacion-obras" v-if="mostrandoA > 1">Mostrando {{ indiceDesde+1 }} a {{ mostrandoA }} de {{ todasObras.length }} obras</div>
    </div>
    <div class="resultados" v-if="obras.length > 0">
      <transition-group class="cards" :style="{maxHeight:altoMaximo}" name="obra" tag="div">
        <muni-card ref="cardgroup" :obra="obra" class="card" v-for="obra in obras" :key="(Math.random()*obras.length)"></muni-card>
      </transition-group>
    </div>
    <div class="sin-resultados" v-if="obras.length <= 0 && !sinResultados"><span>Cargando...</span></div>

    <muni-nav-pag :actual="paginaActual" :paginas="ultimaPagina" @cambioTexto="cambioTexto" @cambioTipo="cambioTipo" @cambioBarrio="cambioBarrio" @cambioProgreso="cambioProgreso" :barrios="$root.barrios" :tipos="$root.tiposObras" @iraPagina="iraPagina" @volverPagina="volverPagina" @pasarPagina="pasarPagina" @buscarFiltros="buscarFiltros"></muni-nav-pag>
    <div class="sin-resultados" v-if="sinResultados">
      <span>No se encontraron resultados.</span>
    </div>

  </div>
  </section>`,
  mounted: function() {
    var urlHash = document.location.toString();
    var tS = null;
    var pS = null;
    var bS = null;
    var text = getParameterByName("texto");;


    if (urlHash.match('#')) {
      var string = urlHash.split('#')[1];
      var filters = string.split('-');

      for(filter in filters){
        var type = filters[filter].split('_')[0];
        var value = filters[filter].split('_')[1];
        if(type == "t"){
          tS = value;
          tipoHash = value;
        }else if(type == "p"){
          pS = value;
          progHash = value;
        }else if(type == "b"){
          bS = value;
          barrioHash = value;
        }
      }
    }
    this.fetchObras({textoSeleccionado:text, tipoSeleccionado:tS, progresoSeleccionado: pS, barrioSeleccionado: bS});
  },
  data: function() {
    return {
      obras: [],
      todasObras: [],
      paginaActual: 1,
      indiceDesde: 1,
      indiceHasta: 12,
      ultimaPagina: 1,
      altoMaximo: '280vh',
      sinResultados: false,
      textoSeleccionado: null,
      tipoSeleccionado: null,
      progresoSeleccionado: null,
      barrioSeleccionado: null,
      estadisticas: {
        inversion: 0
      }
    }
  },
  computed: {
    mostrandoA () {
      return this.indiceHasta >= this.todasObras.length ? this.indiceDesde + this.obras.length : this.indiceHasta;
    }
  },
  watch: {
    obras: function() {
      let division = 3.7;
      if (screen.availWidth <= 940) {
        division = 3.0;
      } else if (screen.availWidth <= 770) {
        division = 2.5;
      }
      const alto = ((this.mostrandoA - this.indiceDesde) * 460 / division);
      this.altoMaximo = alto > 460 ? alto+'px' : '460px';
    },
    todasObras: function() {
      if (this.estadisticas.inversion == 0) {
        this.estadisticas.inversion = 0;
        let cantidadTerminadas = 0;
        this.todasObras.forEach(obra => {
          this.estadisticas.inversion += parseFloat(obra.monto);
          if (obra.porcentaje_completado == 100)
            cantidadTerminadas++;
        })
        let division = 1;
        if (this.todasObras.length != 0) {
          division = this.todasObras.length;
        }
        const porcentajeTerminadas = cantidadTerminadas*100/division;
      }
    }
  },
  methods: {
    fetchObras: function(opciones = {textoSeleccionado:null, progresoSeleccionado:null, tipoSeleccionado:null, barrioSeleccionado:null}) {
      const filtroTexto = opciones.textoSeleccionado ? '&texto='+opciones.textoSeleccionado : '';
      const filtroTipo = opciones.tipoSeleccionado ? '&tipo_id='+opciones.tipoSeleccionado : '';
      const filtroBarrio = opciones.barrioSeleccionado ? '&barrios_ids='+opciones.barrioSeleccionado : '';

      const opcionesProgreso = {
        proyecto: '&avance_hasta=0',
        licitadas: '&avance_hasta=0',
        en_progreso: '&avance_desde=1&avance_hasta=99',
        finalizadas: '&avance_desde=100'
      };
      const filtroProgreso = opciones.progresoSeleccionado ? opcionesProgreso[opciones.progresoSeleccionado] : '';
      const opcionesProgresoFrente = {
        proyecto: '&obra_avance_hasta=0',
        licitadas: '&obra_avance_hasta=0',
        en_progreso: '&obra_avance_desde=1&obra_avance_hasta=99',
        finalizadas: '&obra_avance_desde=100'
      };
      const filtroProgresoFrente = opciones.progresoSeleccionado ? opcionesProgresoFrente[opciones.progresoSeleccionado] : '';
      this.todasObras = [];
      this.obras = [];
      this.indiceDesde = 0;
      this.indiceHasta = 0;
      this.paginaActual = 1;
      this.sinResultados = false;
      const vm = this;

      const url = 'https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-de-terceros/obras-de-terceros/?page_size=500'+filtroProgreso+filtroTipo+filtroBarrio+filtroTexto;
      fetch(url)
      .then(data => data.json())
      .then(function(jsonData) {
        map.data.forEach(feature => {
          map.data.remove(feature);
        });
        let resultados = jsonData.results;
        if (opciones.progresoSeleccionado === 'proyecto') {
          resultados.features = resultados.features.filter(obra => obra.properties.organizacion.toLowerCase() === 'sin adjudicar');
        } else if (opciones.progresoSeleccionado === 'licitadas') {
          resultados.features = resultados.features.filter(obra => obra.properties.organizacion.toLowerCase() !== 'sin adjudicar');
        }
        map.data.addGeoJson(resultados)
        obras = resultados.features.map(dato => Object.assign({id: dato.id}, dato.properties));
        vm.todasObras = obras;

        vm.sinResultados = obras.length > 0 ? false : true;

        vm.ultimaPagina = Math.ceil(obras.length/12.0);
        vm.indiceDesde = 0;
        vm.indiceHasta = 12;
        vm.obras = obras.slice(0, 12);

        let inversion = 0;
        todasObras = obras;
        let cantidadTerminadas = 0;
        this.todasObras.forEach(obra => {
          inversion += parseFloat(obra.monto);
          if (obra.porcentaje_completado == 100)
            cantidadTerminadas++;
        })
        let totalObras = 0;
        if (this.todasObras.length != 0) {
          totalObras = jsonData.count;
        }
        var porcentajeTerminadas = 0;
        if(totalObras != 0){
          porcentajeTerminadas = Math.round(cantidadTerminadas*100/totalObras);
        }
        var d = new Date();

        var month = d.getMonth()+1;
        var day = d.getDate();

        var output = ((''+day).length<2 ? '0' : '') + day + '/' +
            ((''+month).length<2 ? '0' : '') + month + '/' +
            d.getFullYear();


        $('#fechaActual').html(output);
        let totalFrentesObras = 0;
        // console.log(filtroProgreso+filtroTipo+filtroBarrio+filtroTexto);
        fetch('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-de-terceros/frentes-obras-de-terceros/?page_size=250'+filtroProgresoFrente+filtroTipo+filtroBarrio+filtroTexto)
          .then(data => data.json())
          .then(function(dataJson) {
            totalFrentesObras = dataJson.count;
            $('#frenteObrasNumero').animateNumber(
              {
                number: totalFrentesObras,
                easing: 'easeInQuad',
                numberStep: dot_separator_number_step
              }, 2000
            );
          });
        var dot_separator_number_step = $.animateNumber.numberStepFactories.separator('.')
        $('#obrasNumero').animateNumber(
          {
            number: totalObras,
            easing: 'easeInQuad',
            numberStep: dot_separator_number_step
          }, 2000
        );


        $('#financNumero').animateNumber(
          {
            number: inversion,
            easing: 'easeInQuad',
            numberStep: dot_separator_number_step
          }, 2000
        );
        $('#finalizadasNumero').animateNumber(
          {
            number: porcentajeTerminadas,
            easing: 'easeInQuad',
            numberStep: dot_separator_number_step
          }, 2000
        );


      })
      .catch(error => {
        console.log(error);
      });
    },
    cambioTexto: function(textoSeleccionado) {
      this.textoSeleccionado = textoSeleccionado;
      // this.fetchObras({textoSeleccionado, tipoSeleccionado: this.tipoSeleccionado, progresoSeleccionado: this.progresoSeleccionado, barrioSeleccionado:this.barrioSeleccionado});
    },
    cambioTipo: function(tipoSeleccionado) {
      this.tipoSeleccionado = tipoSeleccionado;
      // this.fetchObras({tipoSeleccionado, progresoSeleccionado: this.progresoSeleccionado, barrioSeleccionado:this.barrioSeleccionado});
    },
    cambioProgreso: function(progresoSeleccionado) {
      this.progresoSeleccionado = progresoSeleccionado;
      // this.fetchObras({progresoSeleccionado, tipoSeleccionado:this.tipoSeleccionado, barrioSeleccionado:this.barrioSeleccionado});
    },
    cambioBarrio: function(barrioSeleccionado) {
      this.barrioSeleccionado = barrioSeleccionado;
      // this.fetchObras({barrioSeleccionado, tipoSeleccionado:this.tipoSeleccionado, progresoSeleccionado:this.progresoSeleccionado});
    },
    buscarFiltros:function() {
       this.fetchObras({textoSeleccionado: this.textoSeleccionado, barrioSeleccionado: this.barrioSeleccionado , tipoSeleccionado:this.tipoSeleccionado, progresoSeleccionado:this.progresoSeleccionado});
    },
    pasarPagina: function() {
      if (this.paginaActual < this.ultimaPagina) {
        this.indiceDesde = 0+(this.paginaActual*12);
        this.indiceHasta = this.indiceDesde+12;
        this.paginaActual++;
        this.obras = this.todasObras.slice(this.indiceDesde, this.indiceHasta);
      }
    },
    iraPagina: function (nroPagina) {
      this.paginaActual = nroPagina;
      this.indiceDesde = 0+(this.paginaActual*12)-12;
      this.indiceHasta = this.indiceDesde+12;
      this.obras = this.todasObras.slice(this.indiceDesde, this.indiceHasta);
    },
    volverPagina: function() {
      if (this.paginaActual - 1 >= 1) {
        this.paginaActual--;
        this.indiceDesde = (this.paginaActual*12)-12;
        this.indiceHasta = this.indiceDesde+12;
        this.obras = this.todasObras.slice(this.indiceDesde, this.indiceHasta);
      }
    }
  }
})


const app = new Vue({
  el: '#obras',
  data: {
    tiposObras: [],
    barrios: [],
    tiposConMasCero: []
  },
  created () {
    const vm = this;
    fetch('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras/tipos-obras-terceros/')
      .then(data => data.json())
      .then(function(dataJson) {
        dataJson.results.map(function(value, key) {
          fetch('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras-de-terceros/obras-de-terceros/?page_size=250&tipo_id='+value.id)
          .then(data => data.json())
          .then(function(jsonData) {
            let resultados = jsonData.results;
            let obrasAux = resultados.features.map(dato => Object.assign({id: dato.id}, dato.properties));
            let inversion = 0;
            obrasAux.forEach(obra => {
              inversion += parseFloat(obra.monto);
            })

            if(jsonData.count>0 && inversion>0){
              vm.tiposConMasCero.push(value);
            }
            console.log(jsonData);
            // if(jsonData.next!=null){
            //   alert("A");
            // }
          });
        });
        vm.tiposObras = vm.tiposConMasCero;
        dataJson.results.map(tipo=>{
          tipos[tipo.nombre] = "#"+tipo.color.substr(2,7)
        });
      });
    fetch('https://gobiernoabierto.cordoba.gob.ar/api/v2/obras/barrios-obras-terceros/?page_size=180')
      .then(data => data.json())
      .then(function(dataJson) {
        vm.barrios = dataJson.results;
      });
  }
});
