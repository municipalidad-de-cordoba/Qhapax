jQuery(window).load(function () {
	$('#title-div').connections({ to: '#texto-div' });
	$('#texto-div').connections({ to: '#foto-div' });
	$('#foto-div').connections({ to: '#qr-div' });
	
	$( window ).resize(function() {
		$('#title-div').connections('update');
		$('#texto-div').connections('update');
		$('#foto-div').connections('update');

	})
});
