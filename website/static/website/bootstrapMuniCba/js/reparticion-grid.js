
jQuery(window).load(function () {
	jQuery.expr[':'].regex = function(elem, index, match) {
	var matchParams_b =match[3].split(',');
	matchParams_b[1] = matchParams_b[1].replace('@', ',');
    var matchParams = matchParams_b,
        validLabels = /^(data|css):/,
        attr = {
            method: matchParams[0].match(validLabels) ? 
                        matchParams[0].split(':')[0] : 'attr',
            property: matchParams.shift().replace(validLabels,'')
        },
        regexFlags = 'ig',
        regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
    return regex.test(jQuery(elem)[attr.method](attr.property));
}	

	
	var waitForFinalEvent = (function () {
	  var timers = {};
	  return function (callback, ms, uniqueId) {
	    if (!uniqueId) {
	      uniqueId = "Don't call this twice without a uniqueId";
	    }
	    if (timers[uniqueId]) {
	      clearTimeout (timers[uniqueId]);
	    }
	    timers[uniqueId] = setTimeout(callback, ms);
	  };
	})();

	var draw = function(expression){
		$(expression).each(function(){
			var name = $(this).attr("class");
			name = name.split(' ');
			name = name[1].replace(/\-/g, '.');
			
			var regex_ex = 'div:regex(class,('+name+'.\\d{1@}$))';
			$(this).connections({ to: regex_ex });
			if($(regex_ex)){
				draw(regex_ex);	
			}
		});
	}

	var update_draw = function(expression){
		$(expression).each(function(){
			var name = $(this).attr("class");
			name = name.split(' ');
			name = name[1].replace(/\-/g, '.');
			
			var regex_ex = 'div:regex(class,('+name+'.\\d{1@}$))';
			$(this).connections('update');
			if($(regex_ex)){
				update_draw(regex_ex);	
			}
		});
	}
	var ft = true;
	
	var imgMargin = 0; //Dado por el CSS
	
	var reArrange = function() {
		(function($, viewport){
			if( viewport.is('>=sm') ) {
				$(".parent_head").each(function(){
					if( viewport.is('>=md') ){
					if (ft==true){
						imgMargin = parseFloat($(this).parent().css('marginTop'));						
					}
				   	var $children = $(this).children(":first");
					var $cc = $children.children(":first");
				    $children.css({paddingTop: ($(".height_parent_head").height() / 2) - $cc.height()});
			
					if ($cc.height() >= $(".height_parent_head").height()/2){
						if( viewport.is('sm') ) {
							$(this).parent().children(":first").css({height: $cc.height()*2});
						}else {
							$(this).parent().children(":first").css({height: $cc.height()*2});
						}
						$(this).parent().children(":first").children(":first").css({marginTop: $cc.height()-$(".height_parent_head").height() / 2});
					}else{
					//VER
					}
					}
					
				});	
				$(".parent").each(function(){
// 					if( viewport.is('>=md') ){
						if (ft==true){
							imgMargin = parseFloat($(this).parent().css('marginTop'));	
						}
					   	var $children = $(this).children(":first");
						var $cc = $children.children(":first");
						var $img = $(this).parent().children(".height_parent");
						var img_height = $(this).parent().children(".height_parent").height();
						if (img_height==1){
							img_height=$(this).parent().parent().children(".odd-column").children(".height_parent").height();
						}
	 				    $children.css({paddingTop: (img_height / 2 ) - $cc.height()});
						
						if($cc.height() >= $img.height()/2 ){
							$img.children(".nombre").children(".circle-profile").css({marginTop: $cc.height() - (img_height / 2)});
						}else{
							//VER
						}

// 					}
										
				});
			}
			if( viewport.is('>=md') ){
			if (ft==true){
				draw('div:regex(class,(circle.profile.\\d{1@}$))');
				$('.circle-profile-int').connections({ to: '#leftCorner1' });
				$('#leftCorner2').connections({ to: '#leftCorner1' });
				$('#leftCorner2').connections({ to: 'div:regex(class,(circle.profile.\\d{1@}$))' });
				ft=false;
			}else{
				$('.circle-profile-int').connections('update');
				$('#leftCorner2').connections('update');
				$('#leftCorner2').connections('update');
				update_draw('div:regex(class,(circle.profile.\\d{1@}$))');
	 		}
 		}
		})(jQuery, ResponsiveBootstrapToolkit);	

	}

	var checkArrange = function() {
		(function($, viewport){
			$(".parent").each(function(){
				var $children = $(this).children(":first");
				var $img = $(this).parent().children(".height_parent");
				var $rightElement = $img.children(":first").children(":first");
				var $leftElement = $(this).parent().parent().children(".odd-column").children(".height_parent").children(":first").children(":first");
				$img.children(":first").children(":first").css({marginTop: imgMargin});
				if( viewport.is('<=sm') ) {
					$img.children(".nombre").children(".circle-profile").css({marginTop: '3%'});
					$children.css({paddingTop: 0});
					$leftElement.parent().parent().parent().css({marginTop: '3%'});
					$rightElement.parent().parent().parent().css({marginTop: '3%'});
				}else{
					$img.children(".nombre").children(".circle-profile").css({marginTop: '3%'});
					$children.css({paddingTop: 0});
					$leftElement.parent().parent().parent().css({marginTop: '3%'});
					$rightElement.parent().parent().parent().css({marginTop: '3%'});
				}
				});	
			
		})(jQuery, ResponsiveBootstrapToolkit);	
	}
	reArrange();
		
	
	$( window ).resize(function() {
		waitForFinalEvent(function(){
			checkArrange();
 			reArrange();
    	}, 500, "Final");

	})
	


});
