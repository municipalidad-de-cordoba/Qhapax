function animations(){
  var timeOut2 = setTimeout(function() {
  // Add Scene to ScrollMagic Controller
    var t = new ScrollMagic.Controller,
        e = $(".count");
    e.each(function() {
        var e = $(this),
            i = parseInt(e.attr("data-min"), 10),
            n = parseInt(e.attr("data-max"), 10),
            r = i,
            s = n,
            o = {
                  val: r
                },
            a = !1,
            l = 1.0;
        l = .07 * l,
        l = (n * l).toFixed(2);
        var c = !0;
        c && (l = 1.5);
        var h = function() {
            a = !a,
            a ? (r = i,
            s = n) : (r = n,
            s = i),
            TweenMax.to(o, 2, {
                val: s,
                onUpdate: u,
                ease: Power0.easeNone
            })
        }
          , u = function() {
            e.html(o.val.toFixed())
        }
        ;
        new ScrollMagic.Scene({
            triggerElement: e,
            offset: $('#participacionCiudadana').offset().top-500,
            reverse: false
        }).on("enter", h).addTo(t)
    })
  }, 500);

  var timeOut1 = setTimeout(function() {

    var counter = { var: 0 };

  // Add Scene to ScrollMagic Controller
    var t = new ScrollMagic.Controller,
        e = $(".percentage");
    e.each(function() {
        var h = function() {
            TweenMax.to(counter, 2, {
                var:  e.attr("data-max"),
                onUpdate: function() {
                  var nwc = counter.var.numeroConComa(1);
                  e.html(nwc);
              },
                ease: Power0.easeNone
            })
        }

        ;
        new ScrollMagic.Scene({
            triggerElement: e,
            offset: $('body').offset().top,
            reverse: false
        }).on("enter", h).addTo(t)
    })

  }, 500);
}

animations();


function truncate(input) { 
    if (input.length > 150) { 
        return input.substring(0, 150) + '...'; 
    } 
    return input; 
}; 


function createItem(iniciativa) {
  const template = `<div class="card-iniciativa col-sm-4 col-md-4">
        <div class="card-container">
            <div class="row card-iniciativa-image">
                <div class="col-sm-12 text-center">
                    <a target="_blank" href="${  iniciativa.url }">
                        <div class="image-container">
                            <div>
                                <img class="img-responsive center-block" src="${'/media/' + iniciativa.imagen}" alt="${iniciativa.titulo}"/>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row card-iniciativa-titulo">
                <div class="col-sm-12 text-center">
                    <a target="_blank" href="${  iniciativa.url }">
                       ${  iniciativa.titulo }
                    </a>
                </div>
            </div>
            <div class="row card-iniciativa-description">
                <div class="col-sm-12 text-center">
                    <p class="text-center"> ${  truncate(iniciativa.descripcion) } </p>
                </div>
            </div>
            <div class="row card-iniciativa-footer">
                <div class="col-sm-6 text-left"> ${  moment(iniciativa.fecha).format('DD/MM/YYYY')} </div>
                <div class="col-sm-6 text-right">
                    <a class="btn btn-iniciativa" target="_blank" href="${  iniciativa.url }">LEER MÁS +</a>
                </div>
            </div>
        </div>
    </div> 
  `;

  let item = document.createElement('div');
  item.innerHTML = template.trim();

  return item.firstChild;
}
