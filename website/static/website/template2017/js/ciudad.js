function animations(){
  var timeOut2 = setTimeout(function() {

    var counter = { var: 0 };

  // Add Scene to ScrollMagic Controller
    var t = new ScrollMagic.Controller,
        e = $(".count");
    e.each(function() {
        var h = function() {
            TweenMax.to(counter, 2, {
                var: parseInt(e.attr("data-max"), 10),
                onUpdate: function() {
                  var nwc = numberoConPuntos(counter.var);
                  e.html(nwc);
              },
                ease: Power0.easeNone
            })
        }

        ;
        new ScrollMagic.Scene({
            triggerElement: e,
            offset: $('body').offset().top,
            reverse: false
        }).on("enter", h).addTo(t)
    })

  }, 500);
}

animations();

$(function() {
    $('.cardLineamiento').matchHeight();
});

$(document).ready(function(){
  $('.owl-carousel').on('initialized.owl.carousel changed.owl.carousel', function(e) {
    if (!e.namespace) return
    var carousel = e.relatedTarget;
    $('#infoPaginacionGuia').text('Página ' + (carousel.relative(carousel.current())+1) + ' de ' + carousel.items().length)

  })
  $('.owl-carousel').owlCarousel({
  loop:false,
  animateOut: 'rollOut',
  animateIn: 'rollInt',
  dots:false,
  margin:0,
  slideBy: 1,
  nav:true,
  navText: ['<span class="fa fa-chevron-left" aria-hidden="true"></span>','<span class="fa fa-chevron-right" aria-hidden="true"></span>'],
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
    }
  })

});
