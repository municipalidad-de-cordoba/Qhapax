$(document).ready(function(){

  // $('.owl-carousel').on('initialized.owl.carousel changed.owl.carousel', function(e) {
  //     if (!e.namespace) return
  //     var carousel = e.relatedTarget
  //     $('#infoPaginacionMetas').text('Página ' + (carousel.relative(carousel.current())+1) + ' de ' + ((carousel.items().length)))
  //   })

  $('.owl-carousel').owlCarousel({
  loop:false,
  dots:false,
  margin:0,
  nav:true,
  navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>','<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
    }
  })
  var scrollHash = function(div, offset){
    $('html, body').animate({
        scrollTop: $(div).offset().top - offset
    }, 1000);
  }

  // scrollHash('.datasets.title', 235);

  // var getDato = function (rel, div, callback) {
  //     $.ajax({
  //         url: rel+"?TPL=template2017",
  //         cache: false,
  //         success: function (data) {
  //             $(div).html(data);
  //         },
  //         complete: function () {
  //             $('.loading-indicator').hide();
  //             $(div).removeClass('empty');
  //             scrollHash("#infoDatos", 175);
  //             if(callback!=null){
  //               // openMeta($(callback));
  //             }
  //         }
  //     });
  // };


  // var url = document.location.toString();
  //
  // if (url.match('#')) {
  //   var string = url.split('#')[1];
  //   var type = string.split('_')[0];
  //
  //   if (type == "categoria") {
  //     var categoria = string.split('_')[1];
  //     $('#'+categoria).tab('show');
  //     $('.nav-tabs a[href="#'+categoria+'"]').tab('show') // Select tab by name
  //     scrollHash("#infoCategoria", 250);
  //   }else if (type == "subcategoria") {
  //     var categoria = string.split('_')[1];
  //     var subcategoria = string.split('_')[2];
  //     var id = string.split('_')[3];
  //     $('.nav-tabs a[href="#'+categoria+'"]').tab('show') // Select tab by name
  //     $('.nav-tabs a[href="#subcategoria_'+categoria+'_'+subcategoria+'"]').tab('show')
  //     scrollHash("#tabsSubCat", 250);
  //   }else if (type == "dato") {
  //     var categoria = string.split('_')[1];
  //     var subcategoria = string.split('_')[2];
  //     var dato = string.split('_')[3];
  //     var id = string.split('_')[4];
  //     $('.nav-tabs a[href="#'+categoria+'"]').tab('show') // Select tab by name
  //     $('.nav-tabs a[href="#subcategoria_'+categoria+"_"+subcategoria+'"]').tab('show');
  //     getDato($('#'+categoria+'_'+subcategoria+'_'+dato+'_'+id).data('link'), "#infoDatos", null)
  //   }else if (type == "dato-d") {
  //     var categoria = string.split('_')[1];
  //     var dato = string.split('_')[2];
  //     var id = string.split('_')[3];
  //     $('.nav-tabs a[href="#'+categoria+'"]').tab('show') // Select tab by name
  //     $('#'+categoria+"_"+subcategoria+"_"+id).dropdown('toggle');
  //     getDato($('#'+categoria+'_'+dato+'_'+id).data('link'), "#infoDatos", null)
  //   }
  // }
  function setHash(hash_text) {
    history.pushState({}, '', hash_text);
  }
  // $('.linkDato').on('click', function(e) {
  //   e.preventDefault();
  //   console.log($(this).data('link'));
  //   $('#tabsSubCat .tab-pane').removeClass('active');
  //   getDato($(this).data('link'), "#infoDatos", null)
  // });

  // $('.linkSubCat').on('click', function(e) {
  //   e.preventDefault();
  //   console.log($(this).data('link'));
  //   getDato($(this).data('link'), "#infoDatos", null)
  // });

  $('.datosAbiertos .datasets .boton').matchHeight();
  $('.subSubCategoria').matchHeight();
  $('.colSubCat').matchHeight();
  $('.datoFinal').matchHeight();

  // $('.nav-tabs a').click(function (e) {
  //   $(this).tab('show');
  //   setHash("#categoria_"+$(this).attr('href').replace(/^.*?(#|$)/,''));
  //   $("meta[property='og:url']").attr("content", location.toString());
  //   e.preventDefault();
  // });

  // $('.subSubCategoriaCont.subcat a').click(function (e) {
  //   setHash("#subcategoria_"+this.id);
  //   $("#infoDatos").empty();
  //   scrollHash("#tabsSubCat", 230);
  //   $("meta[property='og:url']").attr("content", location.toString());
  //   e.preventDefault();
  // });
  // $('.subSubCategoriaCont.dato a.linkSubCat').click(function (e) {
  //   setHash("#dato_"+this.id);
  //   $("meta[property='og:url']").attr("content", location.toString());
  //   e.preventDefault();
  // });
  // $('.subSubCategoriaCont.dato a.linkDato').click(function (e) {
  //   setHash("#dato-d_"+this.id);
  //   $("meta[property='og:url']").attr("content", location.toString());
  //   e.preventDefault();
  // });
  // $('.datoCnt a.linkSubCat').click(function (e) {
  //   setHash("#dato_"+this.id);
  //   $("meta[property='og:url']").attr("content", location.toString());
  //   e.preventDefault();
  // });
  // $('.datoCnt a.linkDato').click(function (e) {
  //   setHash("#dato_"+this.id);
  //   $("meta[property='og:url']").attr("content", location.toString());
  //   e.preventDefault();
  // });
});
