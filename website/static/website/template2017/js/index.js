function animations(){
    var t = new ScrollMagic.Controller,
        e = $(".count");
    e.each(function() {
        var e = $(this),
            i = parseInt(e.attr("data-min"), 10),
            n = parseInt(e.attr("data-max"), 10),
            r = i,
            s = n,
            o = {
                  val: r
                },
            a = !1,
            l = 1.5;
        l = .07 * l,
        l = (n * l).toFixed(2);
        var c = !0;
        c && (l = 1.5);
        var h = function() {
            a = !a,
            a ? (r = i,
            s = n) : (r = n,
            s = i),
            TweenMax.to(o, 1, {
                val: s,
                delay: delay_count,
                onUpdate: u,
                ease: Power0.easeNone,
                onComplete: function(){
                  var options = {
                    hash_bookmark: false,
                    initial_zoom: 0,
                    language: 'es'
                  }
                }
            })
        }
          , u = function() {
            e.html(o.val.toFixed())
        }
        ;
        new ScrollMagic.Scene({
            triggerElement: e,
            offset: $('.statisticsHomeHolder').offset().top-1200,
            reverse: false
        }).on("enter", h).addTo(t)
    });
}


$( document ).ready(function() {
  var mobile = false;
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $('.card-container').addClass('manual-flip');
    mobile = true;
  }
  allAnimations();

  $('.carousel').carousel();
  $(".carousel").swipe({

    swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

      if (direction == 'left') $(this).carousel('next');
      if (direction == 'right') $(this).carousel('prev');

    },
    allowPageScroll:"vertical"

  });

  // $('.licencias-btn').on('click',function(e){
  //   e.preventDefault();
  //   $('#licenses').slideDown( 1500, function() {
  //     $('html, body').animate({
  //         scrollTop: $("#licenses").offset().top-100
  //     }, 1500);
  //   });
  // });
  // $('.licenses-close-btn').on('click',function(){
  //   $('html, body').animate({
  //       scrollTop: $("footer").offset().top+125
  //   }, 1500);
  //   $('#licenses').slideUp(1500);
  // });
  //

});
