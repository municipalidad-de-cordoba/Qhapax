function numberoConPuntos(n) {
    var parts=n.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

Number.prototype.numeroConComa = function(decimals, dec_point, thousands_sep) {
  dec_point = typeof dec_point !== 'undefined' ? dec_point : ',';
  var parts = this.toFixed(decimals).split('.');
  return parts.join(dec_point);
}
