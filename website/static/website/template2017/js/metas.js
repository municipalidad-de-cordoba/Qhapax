function animations(){
  var timeOut2 = setTimeout(function() {
  // Add Scene to ScrollMagic Controller
    var t = new ScrollMagic.Controller,
        e = $(".count");
    e.each(function() {
        var e = $(this),
            i = parseInt(e.attr("data-min"), 10),
            n = parseInt(e.attr("data-max"), 10),
            r = i,
            s = n,
            o = {
                  val: r
                },
            a = !1,
            l = 1.5;
        l = .07 * l,
        l = (n * l).toFixed(2);
        var c = !0;
        c && (l = 1.5);
        var h = function() {
            a = !a,
            a ? (r = i,
            s = n) : (r = n,
            s = i),
            TweenMax.to(o, 1, {
                val: s,
                onUpdate: u,
                ease: Power0.easeNone
            })
        }
          , u = function() {
            e.html(o.val.toFixed())
        }
        ;
        new ScrollMagic.Scene({
            triggerElement: e,
            offset: $('#contadorMetas').offset().top-500,
            reverse: false
        }).on("enter", h).addTo(t)
    })
    // $('body').css('overflow', 'inherit');
    // $splash.fadeOut();
  }, 500);
}

animations();




var manoAnimation = function() {
  var setDashoffset = function(el) {
    var l = el.getTotalLength();
    el.setAttribute('stroke-dasharray', l);
    return [l,0];
  }
  var tallo = anime({
    targets: '.tallo path',
    strokeDashoffset: {
      value: setDashoffset,
      duration: 700,
      easing: 'easeOutQuad',
    },
    stroke: {
      value: function(el, i) {
        return '#000';
      },
      easing: 'linear',
      duration: 1,
    },
    delay: function(el, i) {
      return 1000 + (i * 60)
    },
    duration: 100,
  });
  var leftLeaf = anime({
    targets: '.leftLeaf path',
    strokeDashoffset: {
      value: setDashoffset,
      duration: 700,
      easing: 'easeOutQuad'
    },
    stroke: {
      value: function(el, i) {
        return '#000';
      },
      easing: 'linear',
      duration: 1,
    },
    delay: function(el, i) {
      return tallo.duration + (i * 60) - 500
    },
    duration: 100,
  });
  var rightLeaf = anime({
    targets: '.rightLeaf path',
    strokeDashoffset: {
      value: setDashoffset,
      duration: 700,
      easing: 'easeOutQuad'
    },
    stroke: {
      value: function(el, i) {
        return '#000';
      },
      easing: 'linear',
      duration: 1,
    },
    delay: function(el, i) {
      return tallo.duration + (i * 60) - 400
    },
    duration: 100,
  });

}

var checkAnimation = function() {
  var setDashoffset = function(el) {
    var l = el.getTotalLength();
    el.setAttribute('stroke-dasharray', l);
    return [l,0];
  }
  var circle = anime({
    targets: '.circlePath path',
    strokeDashoffset: {
      value: setDashoffset,
      duration: 700,
      easing: 'easeOutQuad'
    },
    stroke: {
      value: function(el, i) {
        return '#FFF';
      },
      easing: 'linear',
      duration: 1,
    },
    delay: function(el, i) {
      return  500 + (i * 60)
    },
    duration: 100,
  });
  var check = anime({
    targets: '.checkPath path',
    strokeDashoffset: {
      value: setDashoffset,
      duration: 700,
      easing: 'easeOutQuad'
    },
    stroke: {
      value: function(el, i) {
        return '#FFFFFF';
      },
      easing: 'linear',
      duration: 1,
    },
    delay: function(el, i) {
      return circle.duration + (i * 60)
    },
    duration: 100,
  });

}
function animationEstados(){
  var timeOut2 = setTimeout(function() {
  // Add Scene to ScrollMagic Controller
    var t = new ScrollMagic.Controller,
        e = $(".metaInfo");
    e.each(function() {
        var e = $(this);
        var h = function() {
            estadosAnimation();
        }
          , u = function() {
            e.html(o.val.toFixed())
        }
        ;
        new ScrollMagic.Scene({
            triggerElement: e,
            offset: $('.metaInfo').offset().top - 300,
            reverse: false
        }).on("enter", h).addTo(t)
    })
    // $('body').css('overflow', 'inherit');
    // $splash.fadeOut();
  }, 1000);
}

function estadosAnimation(){
  manoAnimation();
  checkAnimation();
}

animationEstados();
