function animations(){
  var timeOut2 = setTimeout(function() {
    var t = new ScrollMagic.Controller,
        e = $(".count");
    e.each(function() {
        var e = $(this),
            i = parseInt(e.attr("data-min"), 10),
            n = parseInt(e.attr("data-max"), 10),
            r = i,
            s = n,
            o = {
                  val: r
                },
            a = !1,
            l = 1.5;
        l = .07 * l,
        l = (n * l).toFixed(2);
        var c = !0;
        c && (l = 1.5);
        var h = function() {
            a = !a,
            a ? (r = i,
            s = n) : (r = n,
            s = i),
            TweenMax.to(o, 1, {
                val: s,
                onUpdate: u,
                ease: Power0.easeNone
            })
        }
          , u = function() {
            e.html(o.val.toFixed())
        }
        ;
        new ScrollMagic.Scene({
            triggerElement: e,
            offset: $('#contadorDatos').offset().top-1200,
            reverse: false
        }).on("enter", h).addTo(t)
    })
  }, 500);
}

function animations2(){
  var timeOut2 = setTimeout(function() {
    var t = new ScrollMagic.Controller,
        e = $(".countDatos");
    e.each(function() {
        var e = $(this),
            i = parseInt(e.attr("data-min"), 10),
            n = parseInt(e.attr("data-max"), 10),
            r = i,
            s = n,
            o = {
                  val: r
                },
            a = !1,
            l = 1.5;
        l = .07 * l,
        l = (n * l).toFixed(2);
        var c = !0;
        c && (l = 1.5);
        var h = function() {
            a = !a,
            a ? (r = i,
            s = n) : (r = n,
            s = i),
            TweenMax.to(o, 1, {
                val: s,
                onUpdate: u,
                ease: Power0.easeNone
            })
        }
          , u = function() {
            e.html(o.val.toFixed())
        }
        ;
        new ScrollMagic.Scene({
            triggerElement: e,
            offset: $('#contadorDatosconDatos').offset().top-1200,
            reverse: false
        }).on("enter", h).addTo(t)
    })
  }, 500);
}

$( document ).ready(function() {
    animations();
    animations2();
});
