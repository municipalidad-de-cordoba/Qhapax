var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
var ff = navigator.userAgent.indexOf('Firefox') > 0;
var tap = ('ontouchstart' in window || navigator.msMaxTouchPoints) ? 'touchstart' : 'mousedown';
if (iOS) document.body.classList.add('iOS');
var delay_count = 0;

var allAnimations = function() {
  activateAllHomeEffects();
	$('.splash').css('display', 'none');
  // if(Cookies.get('openedBefore') == "true"){
	// 	activateAllHomeEffects();
	// 	$('.splash').css('display', 'none');
  // }else{
	// 	$('.splash').css('opacity', '1');
	// 	logoAnimation.init();
  //   Cookies.set('openedBefore', 'true');
  // }
}

var logoAnimation = (function() {

  var logoEl = document.querySelector('.logo-animation');
  var pathEls = document.querySelectorAll('.logo-animation path:not(.icon-curve)');
  var innerWidth = window.innerWidth;
  var maxWidth = 1200;
  var logoScale = innerWidth <= maxWidth ? innerWidth / maxWidth : 1;
  var logoTimeline = anime.timeline({ autoplay: false });

  // logoEl.style.transform = 'translateY(50px) scale('+logoScale+')';

  for (var i = 0; i < pathEls.length; i++) {
    var el = pathEls[i];
    el.setAttribute('stroke-dashoffset', anime.setDashoffset(el));
  }

  logoTimeline
    .add({
      targets: '#logoMuni .border path',
      strokeDashoffset: {
        value: [anime.setDashoffset, 0],
        duration: 1000,
        easing: 'easeInOutQuint'
      }
    })
    .add({
      targets: '#logoMuni .inside path, #logoMuni .inside rect',
      strokeDashoffset: {
        value: [anime.setDashoffset, 0],
        duration: 500,
        easing: 'linear'
      },
      offset: '-=250'
    })
    .add({
      targets: '#logoMuni',
      translateX: -175,
      scale: 0.5,
      translateY: -35,
      duration: 500,
      easing: 'easeInOutCirc'
    })
    .add({
      targets: ['#textoLogoMuni .st0Texto'],
      translateY: [30, 0],
      opacity: { value: [0, 1], duration: 100, easing: 'linear' },
      delay: function(el, i, t) { return  ( i * 15); },
      offset: '-=150'
    })
    .add({
      targets: '#logoMuni .inside path, #logoMuni .inside rect, #logoMuni .border path',
      stroke: {
        value:  '#91D23B',
        duration: 500,
        // delay: 200,
        easing: 'easeInQuad'
      },
      offset: '-=150'
    })
    .add({
      targets: '#textoLogoMuni',
      opacity: {
        value:  0,
        duration: 500,
        easing: 'easeInQuad'
      },
      offset: '-=500'
    })
    .add({
      targets: '#logoMuni',
      translateX: -175,
      scale: 0.5,
      translateY: -35,
      stroke: {
        value: [function(el) { return anime.getValue(el.parentNode, 'stroke') }, '#91D23B' ],
        duration: 500,
        delay: 500,
        easing: 'linear'
      },
      backgroundColor: '#91D23B',
      scaleX: 0.1835,
      scaleY: 0.154,
      borderRadius: ['100%'],
      offset: '-=500',

    })
    .add({
      targets: '#logoMuni',
      translateX: -175,
      scale: 0.5,
      translateY: -35,
      scaleX: 0.1835,
      scaleY: 0.154,
      translateX: {
        value: 71.5,
        duration: 100,
        easing: 'linear'
      },
      translateY: {
        value: -135,
        duration: 100,
        easing: 'easeInQuad'
      },
      borderRadius: ['100%'],
      offset:'-=500'
    })
    .add({
      targets: '#lineSquare .first',
      strokeDashoffset: {
        value: [anime.setDashoffset, 0],
        duration: 100,
        easing: 'linear'
      },
      offset: '-=500',
    })
    .add({
      targets: '#roundSquare .second',
      strokeDashoffset: {
        value: [anime.setDashoffset, 0],
        duration: 500,
        delay: 200,
        easing: 'linear',
      },
      stroke: {
        value: ['#91D23B', function(el) { return anime.getValue(el.parentNode, 'stroke') } ],
        duration: 1000,
        delay: 300,
        easing: 'easeInQuad'
      },
      offset: "-=600"
    })
    .add({
      targets: '#logoMuni',
      opacity: 0,
      duration: 10
    })
    .add({
      targets: '#lineSquare',
      rotate: -70,
      duration: 1000,
    })
    .add({
      targets: '#lineSquare .first',
      stroke: {
        value: [function(el) { return anime.getValue(el.parentNode, 'stroke') }, '#FFF' ],
        duration: 500,
        easing: 'easeInQuad'
      },
      offset: '-=1000',
    })

    .add({
      targets: '.logoSquare',
      translateX: -135,
      duration: 1000,
      offset: '-=500'
    })
    .add({
      targets: ['#textoLogo path', '#textoLogo polygon'],
      translateY: [15, 0],
      opacity: { value: [0, 1], duration: 100, easing: 'linear' },
      delay: function(el, i, t) { return  ( i * 20 ); },
      offset: '-=1000'
    })
    .add({
      targets: '#textoLogo',
      opacity: {
        value:  0,
        duration: 500,
        delay: 200,
        easing: 'easeInQuad'
      },
      offset: '-=500'
    })
    .add({
      targets: '.logoSquare',
      translateX: 0,
      scale: 1.6,
      duration: 1000
    })
    .add({
      targets: '.splash',
      opacity: [1, 0],
      duration: 500,
      easing: 'linear',
			complete: function() {
				activateAllHomeEffects();
				$('.splash').css('display', 'none');
			}
    })

  function init() {
    logoTimeline.play();
  }

  return {
    init: init
  }

})();
function activateAllHomeEffects(){
	// activateParallax();
	animations();
	$('.carousel').carousel();
	$(function() {
			$('.card-sImportante').matchHeight();
	});


}

function activateParallax() {

	var parallaxBox = document.getElementById ( 'box' );
	var c1left = document.getElementById ( 'l1' ).offsetLeft,
	c1top = document.getElementById ( 'l1' ).offsetTop,
	c2left = document.getElementById ( 'l2' ).offsetLeft,
	c2top = document.getElementById ( 'l2' ).offsetTop,
	c3left = document.getElementById ( 'l3' ).offsetLeft,
	c3top = document.getElementById ( 'l3' ).offsetTop,
	c4left = document.getElementById ( 'l4' ).offsetLeft,
	c4top = document.getElementById ( 'l4' ).offsetTop,
	// c5left = document.getElementById ( 'l5' ).offsetLeft,
	// c5top = document.getElementById ( 'l5' ).offsetTop,
	c6left = document.getElementById ( 'l6' ).offsetLeft,
	c6top = document.getElementById ( 'l6' ).offsetTop,
	c7left = document.getElementById ( 'l7' ).offsetLeft,
	c7top = document.getElementById ( 'l7' ).offsetTop;

	parallaxBox.onmousemove = function ( event ) {
		event = event || window.event;
		var x = event.clientX - parallaxBox.offsetLeft,
		y = event.clientY - parallaxBox.offsetTop;

		mouseParallax ( 'l1', c1left, c1top, x, y, 5 );
		mouseParallax ( 'l2', c2left, c2top, x, y, -30 );
		mouseParallax ( 'l3', c3left, c3top, x, y, 30 );
		mouseParallax ( 'l4', c4left, c4top, x, y, 65 );
		// mouseParallax ( 'l5', c5left, c5top, x, y, 0 );
		mouseParallax ( 'l6', c6left, c6top, x, y, 0 );
		mouseParallax ( 'l7', c7left, c7top, x, y, 10 );

	}

}

function mouseParallax ( id, left, top, mouseX, mouseY, speed ) {
	var obj = document.getElementById ( id );
	var parentObj = obj.parentNode,
	containerWidth = parseInt( parentObj.offsetWidth ),
	containerHeight = parseInt( parentObj.offsetHeight );
	obj.style.left = left - ( ( ( mouseX - ( parseInt( obj.offsetWidth ) / 2 + left ) ) / containerWidth ) * speed ) + 'px';
	obj.style.top = top - ( ( ( mouseY - ( parseInt( obj.offsetHeight ) / 2 + top ) ) / containerHeight ) * speed ) + 'px';
}
