$(document).ready(function(){
 $('#form-solicitud').ajaxForm({
        beforeSubmit:function(e){
            $('#submitbtn').hide();
            $('#loadingpost').show();
        },
        success : function (response) {
            $('#submitbtn').show();
            $('#loadingpost').hide();
            if(response['ok']){
                 $('#successEnviar').show();
            }else{
                $('#errorEnviar').show();
                var errors = jQuery.parseJSON(response.errors);

                $("[id^='id_'][id*='error']").html(" ");
                $(".form-group").each(function(){
                    $(this).removeClass("has-error");
                });
                $(errors).each(function(i,val){
                    $.each(val,function(k,v){
                        $('#id_'+k+'-error').html(v[0]["message"]);
                        $('#id_'+k+'-error').parent().addClass('has-error');
                });
        if(errors!=null){
          var targetOffset = $(".has-error:first").offset().top - 120;
          $('html, body').animate({
              scrollTop: targetOffset
          }, 2000)
        }
                });
            }
        }
    });
});
