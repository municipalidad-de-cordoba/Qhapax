	$(document).ready(function(){
		$('#form-solicitud').find('select').addClass("form-control");
		$('#form-solicitud').find('select').addClass("dropdown");
		$('#form-solicitud').find('select').wrap('<div class="wrap"></div>')
		$('#form-solicitud').find('input').each(function(){
			if ($(this).is(':radio')) {
		        //code here
		    }else{
			    $(this).addClass("form-control");
		    }
		});
		$('#form-solicitud').find('textarea').addClass("form-control");
		$("form :input").each(function(index, elem) {
    var eId = $(elem).attr("id");
    var label = null;
    if (eId && (label = $(elem).parents("form").find("label[for="+eId+"]")).length == 1) {
        $(elem).attr("placeholder", $(label).html());
    }
		$('label[for="id_captcha"]').attr('for','g-recaptcha-response');
 	});
});
