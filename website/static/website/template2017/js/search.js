$( document ).ready(function() {

  var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
  if (isMobile) {
    /* your code here */
    $('.fa-desktop').addClass("fa-mobile").removeClass("fa-desktop");
  }
  var accMode = Cookies.get('accesibilityMode');

  $( ".modoAccesible" ).each(function( index ) {
    var modeAux = $(this).data("mode");
    if(accMode == modeAux){
      $(this).addClass('active');
    }
  });
  setAccesibilityMode(accMode);

  if(Cookies.get('accesibilityMode') == "bigger"){
    ('.modoTextoAccesible').addClass('active');
    $('html').addClass('a2x');
  }

  //Navigation Menu Slider
  $('#nav-expander').on('click',function(e){
    e.preventDefault();
    $('body').toggleClass('nav-expanded');
    $(this).toggleClass('is-closed is-open');
  });
  $('#nav-close').on('click',function(e){
    e.preventDefault();
    $('body').removeClass('nav-expanded');
    $('#nav-expander').toggleClass('is-closed is-open');
  });

  $('.a-buscar').on('click',function(e){
    e.preventDefault();
    $('.retro-nav-buscar').slideToggle();
    if($('#nav-expander').hasClass('is-open')){
      $('body').removeClass('nav-expanded');
      $('#nav-expander').toggleClass('is-closed is-open');
    }
  });
  $('.dropdown-submenu a.dd').on("click", function(e){
   $(this).next('ul').toggle();
   e.stopPropagation();
   e.preventDefault();
 });

  $('input').on('keyup', function() {
         if (this.value.length > 1) {
              // do search for this.value here
              $("#btn-buscar-cerrar").addClass("hide");
              $("#btn-buscar").removeClass("hide");
         }else{
           $("#btn-buscar").addClass("hide");
           $("#btn-buscar-cerrar").removeClass("hide");
         }
    });
    $('#btn-buscar-cerrar').on('click',function(e){
      e.preventDefault();
      $('.retro-nav-buscar').slideUp();
    });

  $('.modoAccesible').on('click',function(e){
    e.preventDefault();
    $('.modoAccesible').removeClass('active');
    $(this).addClass('active');
    var mode = $(this).data("mode");
    setAccesibilityMode(mode);
    Cookies.set('accesibilityMode', mode);

  });

  $('.modoTextoAccesible').on('click',function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    var mode = 'bigger';
    if($('html').hasClass('a2x')){
      mode = 'smaller';
    }
    Cookies.set('fontMode', mode);
    $('html').toggleClass('a2x');
    $.fn.matchHeight._update();
  });

  function setAccesibilityMode(mode){
    if(mode == 'main'){
      $('html').removeClass('night');
      $('html').removeClass('high_contrast');
    }
    if(mode == 'night'){
      $('html').removeClass('high_contrast');
      $('html').addClass('night');
    }
    if(mode == 'high_contrast'){
      $('html').removeClass('night');
      $('html').addClass('high_contrast');
    }
  }

});
