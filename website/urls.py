from django.conf.urls import url
from . import views
from . import views_plandemetas
from . import views_funcionarios
from . import views_portaldatos
from plandemetas.views import (plan_de_metas_completo_PDF,
                               test_plan_de_metas_PDF,
                               plan_de_metas_temporal_PDF)


urlpatterns = [
    url(r'^$', views.index, name='website.index'),
    url(r'^seccion/(?P<seccion>[\w-]+)$',
        views.seccion, name='website.seccion'),

    # algunas apps generan automaticamente vistas publicas

    # ------------ PLAN DE METAS
    url(r'^plan-de-metas/PDF/plan-(?P<plan_de_metas_id>[0-9]+).pdf$',
        plan_de_metas_completo_PDF, name='website.plandemetas.pdf-completo'),
    url(r'^test-plan-de-metas/PDF/plan-(?P<plan_de_metas_id>[0-9]+).pdf$',
        test_plan_de_metas_PDF, name='website.plandemetas.pdf-test'),
    url(r'^plan-de-metas-temporal/PDF/plan-(?P<plan_de_metas_id>[0-9]+).pdf$',
        plan_de_metas_temporal_PDF, name='website.plandemetas.pdf-temporal'),

    url(r'^plan-de-metas/(?P<plan>[\w-]+)$',
        views_plandemetas.ver,
        name='website.plandemetas'),
    url(r'^plan-de-metas/(?P<plan>[\w-]+)/(?P<linea>[\w-]+)$',
        views_plandemetas.ver, name='website.plandemetas.lineamiento'),
    url(r'^plan-de-metas/(?P<plan>[\w-]+)/'
        r'(?P<linea>[\w-]+)/(?P<comp_id>[0-9]+)$',
        views_plandemetas.ver,
        name='website.plandemetas.componente'),
    url(r'^plan-de-metas/(?P<plan>[\w-]+)/(?P<linea>[\w-]+)/'
        r'(?P<comp_id>[0-9]+)\#objetivos-(?P<comp_real_id>[0-9]+)-'
        r'(?P<obj_id>[0-9]+)$',
        views_plandemetas.ver,
        name='website.plandemetas.objetivo'),
    url(r'^plan-de-metas/(?P<plan>[\w-]+)/(?P<linea>[\w-]+)/'
        r'(?P<comp_id>[0-9]+)\#metas-(?P<comp_real_id>[0-9]+)-'
        r'(?P<obj_id>[0-9]+)-(?P<meta_id>[0-9]+)$',
        views_plandemetas.ver,
        name='website.plandemetas.meta'),

    # ------------ FUNCIONARIOS
    url(r'^funcionarios/(?P<organigrama>[\w-]+)$',
        views_funcionarios.home,
        name='website.funcionarios.home'),
    url(r'^funcionarios/nuevo/(?P<organigrama>[\w-]+)$',
        views_funcionarios.home_prueba,
        name='website.funcionarios.home.prueba'),
    url(
        r'^funcionarios/oficina/mapa-(?P<oficina>[\w-]+)/'
        r'(?P<oficina_id>[0-9]+)$',
        views_funcionarios.oficina_arbol,
        name='website.funcionarios.oficina_arbol'),
    url(
        r'^funcionarios/oficina/(?P<oficina>[\w-]+)/(?P<oficina_id>[0-9]+)$',
        views_funcionarios.oficina_view,
        name='website.funcionarios.oficina'),
    url(
        r'^funcionarios/persona/(?P<persona>[\w-]+)/(?P<persona_id>[0-9]+)$',
        views_funcionarios.persona,
        name='website.funcionarios.persona'),
    url(
        r'^funcionarios/persona/(?P<persona>[\w-]+)/'
        r'(?P<persona_id>[0-9]+)/qr/$',
        views_funcionarios.qr,
        name='website.funcionarios.persona.qr'),

    # ------------ PORTAL DE DATOS
    url(r'^data/(?P<portal>[\w-]+)$',
        views_portaldatos.home,
        name='website.portaldatos.home'),
    url(r'^data/(?P<portal>[\w-]+)/categoria/(?P<categoria>[\w-]+)$',
        views_portaldatos.categoria, name='website.portaldatos.categoria'),
    url(
        r'^data/(?P<portal>[\w-]+)/categoria/(?P<categoria>[\w-]+)/'
        r'(?P<dato>[\w-]+)/(?P<dato_id>[0-9]+)$',
        views_portaldatos.dato,
        name='website.portaldatos.dato'),
    # TODO ficha de fuente de datos (lista con sus recursos)
    # TODO lista de datos buscados (lista con sus recursos).
    # Poner un buscador de datos

    # algunas secciones especiales
    url(r'^nuestra-ciudad$', views.nuestra_ciudad,
        name='website.nuestra-ciudad'),
    url(r'^software-libre$', views.software_libre,
        name='website.software-libre'),
    url(r'^institucional$', views.institucional, name='website.institucional'),
    url(r'^avance-de-obras$',
        views.avance_de_obras,
        name='website.avance-de-obras'),
    url(r'^estado-de-obra$', views.estado_de_obra,
        name='website.estado-de-obra'),
    url(r'^habilitaciones$',
        views.habilitaciones_especial,
        name='website.habilitaciones'),
    url(r'^accesibilidad$', views.accesibilidad, name='website.accesibilidad'),
    url(r'^boletines$', views.boletines, name='website.boletines'),
    url(r'^arbolado$', views.arbolado, name='website.arbolado'),
    url(r'^obras-de-terceros$', views.obras_terceros, name='website.obras-terceros'),
    url(r'^estado-de-obra-terceros$', views.estado_de_obra_terceros, name='website.estado-de-obra-terceros'),
    url(r'^locales-habilitados$', views.locales_habilitados, name='website.locales.habilitados'),
    url(r'^apis$', views.apis, name='website.apis'),
    url(r'^codigos$', views.codigos, name='website.codigos')
]
