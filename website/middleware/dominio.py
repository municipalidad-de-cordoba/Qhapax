# -*- coding: utf-8 -*-
import os
import shutil
from django.shortcuts import redirect
from django.http import HttpResponse
from django.conf import settings
from website.models import Website
from django.http import Http404
from django.core.cache import cache


class MediaFileNotFoundMiddleware(object):
    MASTER = os.path.join(settings.MEDIA_ROOT, 'master.jpg')

    def process_exception(self, request, exception):
        if settings.DEBUG and isinstance(exception, FileNotFoundError):
            print('creating ', exception.filename)
            os.makedirs(os.path.dirname(exception.filename), exist_ok=True)
            shutil.copyfile(MediaFileNotFoundMiddleware.MASTER, exception.filename)
            return redirect(request.build_absolute_uri())


class DominioMiddleware(object):
    """
    Obtener el website del municipio correspondiente segun el dominio.

    Se permiten pruebas locales en el puerto 8000 que no requieren dominio real de un gobierno
    Ver además si el gobierno esta publicado.
    """
    def process_request(self, request):
        request.website, created = Website.objects.get_or_create(protocolo=settings.PROTOCOLO_PRODUCCION,
                                                                    dominio=settings.DOMINIO_PRODUCCION)
        if created:  # si es nuevo le pongo el template más útil para el caso
            request.website.template = 'bootstrapMuniCba'
            
        if 'TPL' in request.GET.keys():
            # si entro a un template de pruebas quiero que lo conserve un rato
            testing_template = request.GET['TPL']
            request.session['testing_template'] = testing_template
            request.website.template = request.session['testing_template']

        # si estoy probando un template lo retomo
        # if request.session.get('testing_template', None) in ['template2017', 'bootstrapMuniCba']:
        #    request.website.template = request.session['testing_template']
