from django.forms import ModelForm, TextInput, Select

from ckeditor.fields import RichTextField

from portaldedatos.models import DatoPortal
from core.models import Persona


class NuevoArchivo(ModelForm):
    class Meta:
        model = DatoPortal
        fields = ['categoria',  # solo para el superadmin ya que identifica gobierno
                  'estado', 'periodicidad', 'titulo',
                  'descripcion', 'html', 'datos_relacionados', 'tags']

        widgets = {
            'titulo': TextInput(
                attrs={
                    'placeholder': 'titulo del dato',
                    'class': 'form-control'}),
            'descripcion': TextInput(
                attrs={
                    'placeholder': 'Descripcion',
                    'class': 'form-control'}),
            'html': RichTextField(),
            'categoria': Select(
                attrs={
                    'class': 'form-control'}),
            'estado': Select(
                attrs={
                    'class': 'form-control'}),
            'periodicidad': Select(
                attrs={
                    'class': 'form-control'}),
            'tags': Select(
                attrs={
                    'class': 'form-control',
                    'multiple': 'multiple'}),
            'datos_relacionados': Select(
                attrs={
                    'class': 'form-control'})}


class PersonaForm(ModelForm):
    class Meta:
        model = Persona
        fields = [
            'user',
            'nombre',
            'apellido',
            'nombre_publico',
            'tipo_id',
            'unique_id',
            'fecha_nacimiento',
            'genero',
            'html']
