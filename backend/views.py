# from calendar import monthrange
import pytz
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.base import TemplateView
from django.db.models import F, Q, Count
from django.db.models.functions import TruncHour, TruncDay
from django.utils.decorators import method_decorator
from django.utils.text import slugify
from django.utils import timezone
import django_excel as excel
from braces.views import StaffuserRequiredMixin
from pyquery import PyQuery
from django.views.decorators.cache import cache_page
from audienciafuncionarios.models import SolicitudAudiencia
from accesoinfopublica.models import SolicitudAccesoInfo
# Los IDs de recorridos están en cordobus, no más en Qhapax
from core.models import Persona, get_recorrido_cordobus
from declaracionesjuradas.models import DeclaracionJurada, Deuda, Bien
from funcionarios.models import Funcion, Cargo
from propuestaciudadana.models import Propuesta
from redbusdata.models import (TarjetaRedBus, NombreTarjetaRedBus,
                               SaldoTarjetaRedBus, ErrorLecturaSaldoRedBus)
from portaldedatos.models import Portal, DatoPortal, VersionDato, Recurso
from .forms.forms import PersonaForm
from wkhtmltopdf.views import PDFTemplateResponse
import datetime
from django.conf import settings
from log_usuarios.models import LogUsuario, DetalleLogUsuario
from softwaremunicipal.models import SoftwarePublico, UsuarioSoftware
# import collections
# from transportepublico.models import (RecorridoLineaTransportePublico,
#                                         LineaTransportePublico)
from obraspublicas.models import ObraPublica, TrazadoObraPublica
from obrasdeterceros.models import ObraDeTerceros, TrazadoObraDeTercero
from arbolado.models import Arbol, EspecieArbol
from plandemetas.models import PlanDeMetas, Meta
from mensajes_a_usuarios.models import (MensajeDirecto, MensajeGrupalAUsuario,
                                        Mensaje)
import time
import requests
import json
import logging
logger = logging.getLogger(__name__)


# ABM de Persona
class PersonaDetailView(
        PermissionRequiredMixin,
        StaffuserRequiredMixin,
        DetailView):
    model = Persona
    template_name = 'backend/persona_detail.html'
    permission_required = "funcionarios.can_view"


class PersonaDeleteView(
        PermissionRequiredMixin,
        StaffuserRequiredMixin,
        DeleteView):
    model = Persona
    template_name = 'backend/persona_confirm_delete.html'
    success_url = reverse_lazy('backend-personas-list')
    permission_required = "funcionarios.delete_funcion"


class PersonaUpdateView(
        PermissionRequiredMixin,
        StaffuserRequiredMixin,
        UpdateView):
    model = Persona
    template_name = 'backend/persona_form.html'
    form_class = PersonaForm
    success_url = reverse_lazy('backend-personas-list')
    permission_required = "funcionarios.change_funcion"


class PersonaCreateView(
        PermissionRequiredMixin,
        StaffuserRequiredMixin,
        CreateView):
    model = Persona
    template_name = 'backend/persona_form.html'
    form_class = PersonaForm
    success_url = reverse_lazy('backend-personas-list')
    permission_required = "funcionarios.add_funcion"


class PersonaListView(
        PermissionRequiredMixin,
        StaffuserRequiredMixin,
        ListView):
    model = Persona
    template_name = 'backend/persona_list.html'
    paginate_by = 25
    queryset = Persona.objects.all().select_related()
    permission_required = "funcionarios.can_view"

    def get_context_data(self, **kwargs):
        context = super(PersonaListView, self).get_context_data(**kwargs)
        context['cantidad'] = self.queryset.count()
        return context


class HomeView(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/home.html'


# Tableros
class FuncionarioSinHTMLListView(StaffuserRequiredMixin, ListView):
    model = Funcion
    template_name = 'backend/tableros/funcionarios/tablero_funcionarios_sin_html_list.html'
    paginate_by = 20

    def get_queryset(self):
        return Funcion.objects.filter(Q(funcionario__html__isnull=True) | Q(
            funcionario__html=''), Q(activo=True)).order_by('cargo__categoria__orden')

    def get_context_data(self, **kwargs):
        context = super(
            FuncionarioSinHTMLListView,
            self).get_context_data(
            **kwargs)
        # Totales
        context['objects_count'] = Funcion.objects.filter(
            Q(funcionario__html__isnull=True) | Q(funcionario__html=''),
            Q(activo=True)).count()
        context['total'] = Funcion.objects.filter(
            activo=True).count()

        return context


class FuncionarioSinDecretoListView(StaffuserRequiredMixin, ListView):
    model = Funcion
    template_name = 'backend/tableros/funcionarios/tablero_funcionarios_sin_decreto_list.html'
    paginate_by = 20

    def get_queryset(self):
        return Funcion.objects.filter(
            decreto_pdf='',
            activo=True).order_by('cargo__categoria__orden')

    def get_context_data(self, **kwargs):
        context = super(
            FuncionarioSinDecretoListView,
            self).get_context_data(
            **kwargs)
        # Totales
        context['objects_count'] = Funcion.objects.filter(
            Q(decreto_pdf='') | Q(decreto_pdf__isnull=True),
            Q(activo=True)).count()
        context['total'] = Funcion.objects.filter(activo=True).count()

        return context


class FuncionarioSinDDJJListView(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros/funcionarios/tablero_funcionarios_sin_ddjj_list.html'

    def get_bad_ddjj(self, anio=None):
        """ obtener declaraciones juradas que faltan """
        lista = []
        por_secretaria = {}  # resumen por secretaría
        if not anio:
            now = timezone.now()
            anio = now.year

        for funcionario in Funcion.objects.filter(
                activo=True).order_by('cargo__categoria__orden'):
            secretaria = funcionario.cargo.area_gobierno.oficina
            if secretaria not in por_secretaria.keys():
                por_secretaria[secretaria] = {'ok': 0, 'falta': 0}

            estados = {
                'persona_id': funcionario.funcionario.id,
                'funcion_id': funcionario.id,
                'nombre': funcionario,
                'errores': [],
            }

            declaracion_jurada = DeclaracionJurada.objects.filter(
                persona=funcionario.funcionario, anio=anio).first()
            if not declaracion_jurada:
                estados['errores'].append('Falta declaracion jurada')
                por_secretaria[secretaria]['falta'] += 1
            else:
                # ver si a pesar de estar cargada le faltan cargar Bienes o
                # Deudas
                declarado = Bien.objects.filter(declaracion_jurada=declaracion_jurada).count(
                ) + Deuda.objects.filter(declaracion_jurada=declaracion_jurada).count()
                if not declarado:
                    estados['errores'].append('Faltan cargar bienes o deudas')
                    por_secretaria[secretaria]['falta'] += 1
                # Si no tiene PDF adjunto
                if not declaracion_jurada.archivo:
                    estados['errores'].append('Falta cargar PDF')
                    por_secretaria[secretaria]['falta'] += 1

            if len(estados['errores']) > 0:
                lista.append(estados)
            else:
                por_secretaria[secretaria]['ok'] += 1

        for secretaria in por_secretaria.keys():
            total = por_secretaria[secretaria]['falta'] + \
                por_secretaria[secretaria]['ok']
            divisor_no_cero = max(1, total)
            por_secretaria[secretaria]['porc_malos'] = round(
                100 * por_secretaria[secretaria]['falta'] / divisor_no_cero, 2)

        malos = len(lista)
        total = Funcion.objects.filter(activo=True).count()
        return malos, total, lista, por_secretaria

    def get_context_data(self, anio=None, **kwargs):
        context = super().get_context_data(**kwargs)

        malos, total, lista_malos, por_secretaria = self.get_bad_ddjj(
            anio=anio)
        # Obtengo funcionarios sin DDJJ o sin bienes y deudas
        context['object_list'] = lista_malos

        # Totales
        context['objects_count'] = malos
        context['total'] = total

        # agrupados por secretaría
        context['por_secretaria'] = por_secretaria

        return context


class FuncionarioSinFotoListView(StaffuserRequiredMixin, ListView):
    model = Funcion
    template_name = 'backend/tableros/funcionarios/tablero_funcionarios_sin_foto_list.html'
    paginate_by = 20

    def get_queryset(self):
        return Funcion.objects.filter(
            funcionario__foto='',
            activo=True).order_by('cargo__categoria__orden')

    def get_context_data(self, **kwargs):
        context = super(
            FuncionarioSinFotoListView,
            self).get_context_data(
            **kwargs)
        # Totales
        context['objects_count'] = self.object_list.count()
        context['total'] = Funcion.objects.filter(activo=True).count()

        return context


class CargosSinFuncionarioListView(StaffuserRequiredMixin, ListView):
    model = Funcion
    template_name = 'backend/tableros/funcionarios/tablero_cargos_sin_funcionario_list.html'
    paginate_by = 20

    def get_faltantes(self):
        cargos = Cargo.objects.filter(activado=True)
        faltantes = []
        for cargo in cargos:
            if cargo.en_funcion_activa is None:
                faltantes.append(cargo)
        return faltantes

    def get_queryset(self):
        """ son los cargos _activados_ que no tienen a nadie en_funcion_activa """
        return self.get_faltantes()

    def get_context_data(self, **kwargs):
        context = super(
            CargosSinFuncionarioListView,
            self).get_context_data(
            **kwargs)
        # Totales
        context['objects_count'] = len(self.object_list)
        context['total'] = Cargo.objects.filter(activado=True).count()
        return context


class CargosSinHTMLListView(StaffuserRequiredMixin, ListView):
    model = Cargo
    template_name = 'backend/tableros/funcionarios/tablero_cargos_sin_html_list.html'
    paginate_by = 20

    def get_queryset(self):
        queryset = Cargo.objects.filter(Q(activado=True), Q(
            html__isnull=True) | Q(html='')).order_by('categoria__orden')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(CargosSinHTMLListView, self).get_context_data(**kwargs)
        # Totales
        context['objects_count'] = self.object_list.count()
        context['total'] = Cargo.objects.filter(activado=True).count()
        return context


class SolicitudAudienciaListView(StaffuserRequiredMixin, ListView):
    model = SolicitudAudiencia
    template_name = 'backend/tableros/audiencias/tablero_solicitud_audiencia_list.html'
    paginate_by = 20

    def get_queryset(self):
        return SolicitudAudiencia.objects.filter(
            funcion__activo=True,
            estado__in=[
                SolicitudAudiencia.NUEVO,
                SolicitudAudiencia.VALIDO,
                SolicitudAudiencia.EN_ESTUDIO,
                SolicitudAudiencia.ACEPTADO]).order_by(
            'estado',
            'ultima_modificacion')

    def get_context_data(self, **kwargs):
        context = super(
            SolicitudAudienciaListView,
            self).get_context_data(
            **kwargs)
        # Totales
        context['objects_count'] = self.object_list.count()
        context['total'] = SolicitudAudiencia.objects.filter(
            funcion__activo=True).count()
        return context


class PropuestaCiudadanaListView(StaffuserRequiredMixin, ListView):
    model = Propuesta
    template_name = 'backend/tableros/ideas/tablero_propuesta_ciudadana_list.html'
    paginate_by = 20

    def get_queryset(self):
        return Propuesta.objects.filter(
            estado__in=[
                Propuesta.NUEVO,
                Propuesta.VALIDO,
                Propuesta.EN_ESTUDIO]).order_by(
            'estado',
            'ultima_modificacion')

    def get_context_data(self, **kwargs):
        context = super(
            PropuestaCiudadanaListView,
            self).get_context_data(
            **kwargs)
        # Totales
        context['objects_count'] = self.object_list.count()
        context['total'] = Propuesta.objects.count()
        return context


class AccesoInformacionListView(StaffuserRequiredMixin, ListView):
    model = SolicitudAccesoInfo
    template_name = 'backend/tableros/acceso_info_publica/tablero_acceso_informacion_list.html'
    paginate_by = 20

    def get_queryset(self):
        return SolicitudAccesoInfo.objects.filter(
            estado__in=[
                SolicitudAccesoInfo.NUEVO,
                SolicitudAccesoInfo.VALIDO,
                SolicitudAccesoInfo.EN_ESTUDIO]).order_by(
            'estado',
            'ultima_modificacion')

    def get_context_data(self, **kwargs):
        context = super(
            AccesoInformacionListView,
            self).get_context_data(
            **kwargs)
        # Totales
        context['objects_count'] = self.object_list.count()
        context['total'] = SolicitudAccesoInfo.objects.count()
        return context


# Listado de Tableros
@method_decorator(cache_page(60 * 60 * 1), name='dispatch')
class TablerosView(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros.html'

    def get_context_data(self, **kwargs):
        context = super(TablerosView, self).get_context_data(**kwargs)
        context['funcionarios_sin_html'] = Funcion.objects.filter(
            Q(funcionario__html__isnull=True) | Q(funcionario__html=''),
            Q(activo=True)).count()
        context['cargos_sin_html'] = Cargo.objects.filter(
            Q(activado=True), Q(html__isnull=True) | Q(html='')).count()

        context['funcionario_sin_foto'] = Funcion.objects.filter(
            funcionario__foto='', activo=True).count()
        context['funcionario_sin_decreto'] = Funcion.objects.filter(
            decreto_pdf='', activo=True).count()
        context['cargos_sin_funcionario'] = len(
            CargosSinFuncionarioListView().get_faltantes())
        context['solicitudes_audiencia'] = SolicitudAudiencia.objects.filter(
            funcion__activo=True,
            estado__in=[
                SolicitudAudiencia.NUEVO,
                SolicitudAudiencia.VALIDO,
                SolicitudAudiencia.EN_ESTUDIO,
                SolicitudAudiencia.ACEPTADO]).count()
        context['propuestas_ciudadanas'] = Propuesta.objects.filter(
            estado__in=[Propuesta.NUEVO, Propuesta.VALIDO, Propuesta.EN_ESTUDIO]
        ).count()
        context['acceso_informacion'] = SolicitudAccesoInfo.objects.filter(
            estado__in=[
                SolicitudAccesoInfo.NUEVO,
                SolicitudAccesoInfo.VALIDO,
                SolicitudAccesoInfo.EN_ESTUDIO]).count()

        malos, total, lista_malos, por_secretaria = FuncionarioSinDDJJListView().get_bad_ddjj()
        context['funcionario_sin_ddjj'] = malos
        context['por_secretaria'] = por_secretaria
        return context


@method_decorator(cache_page(60 * 60 * 1), name='dispatch')
class TablerosRedBus(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros/redbus/index.html'

    def get_context_data(self, **kwargs):
        context = super(TablerosRedBus, self).get_context_data(**kwargs)
        # cantidad de usuarios que usaron la app
        context['usuarios_app'] = NombreTarjetaRedBus.objects.values(
            'uid').distinct().count()
        context['tarjetas_redbus'] = TarjetaRedBus.objects.count()
        context['consultas_saldo_totales'] = SaldoTarjetaRedBus.objects.count()
        context['errores_lectura_saldos'] = ErrorLecturaSaldoRedBus.objects.count()

        return context


class AccesoInfoPDFView(StaffuserRequiredMixin, TemplateView):
    template = 'backend/solicitud-info-print.html'

    def get(self, request, pk):
        logger.info(
            'Acceso info PDF creating {} ({})'.format(
                pk, datetime.datetime.now()))
        solicitud = SolicitudAccesoInfo.objects.get(pk=pk)
        context = {'solicitud': solicitud}
        try:
            response = PDFTemplateResponse(request=request,
                                           template=self.template,
                                           # fuerza descarga OK
                                           # filename="pedido-acceso-info-{}.pdf".format(pk),
                                           context=context,
                                           show_content_in_browser=False,
                                           cmd_options={'margin-top': 5})
        except Exception as e:
            logger.error(
                "Error creando PDF de pedido de acceso a la info".format(e))

        logger.info(
            'Acceso info PDF created: status {} ({})'.format(
                response.status_code,
                datetime.datetime.now()))
        return response


@method_decorator(cache_page(60 * 60 * 1), name='dispatch')
class TablerosPortalDeDatos(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros/portaldedatos/index.html'

    def get_context_data(self, **kwargs):
        context = super(TablerosPortalDeDatos, self).get_context_data(**kwargs)
        # Portal, CategoriaPortalDatos, DatoPortal, VersionDato, Recurso
        context['portales_totales'] = Portal.objects.all().count()
        context['datos_totales'] = DatoPortal.objects.all().count()
        context['desactualizados_totales'] = len(DatoPortal.desactualizados())
        context['versiones_totales'] = VersionDato.objects.all().count()
        context['recursos_totales'] = Recurso.objects.all().count()

        return context


@method_decorator(cache_page(60), name='dispatch')
class TablerosTransporteUnidadesActivas(StaffuserRequiredMixin, TemplateView):

    # TODO agregar a la clase PermissionRequiredMixin y el algun
    # permission_required (ejemplo "funcionarios.can_view") creado para los
    # casos de los tableros

    def get_template_names(self):

        params = self.request.GET
        mobile = params.get('mobile', None)

        if mobile == '1':
            tn = ['backend/tableros/transporte/unidades-activas-mobile.html']
        else:
            tn = ['backend/tableros/transporte/unidades-activas.html']

        return tn

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        req = requests.get(
            'https://cordobus.apps.cordoba.gob.ar/data/datos-de-internos/')
        basedata = req.content.decode('utf-8')
        jdata = json.loads(basedata)[0]

        reg = jdata['registrados']
        no_reg = jdata['no_registrados']

        context['colectivos_activos_registrados'] = reg
        context['colectivos_activos_no_registrados'] = no_reg

        epl = jdata['en_punta_de_lineas']
        context['colectivos_en_punta_de_linea'] = epl
        colectivos_totales = reg + epl + no_reg
        # Restamos el 10% de flota porque son los que quedan para reserva
        context['colectivos_totales'] = int(
            colectivos_totales - (colectivos_totales * 0.10))

        adaptados_en_servicio = jdata['adaptados_en_servicio']
        adaptados_activos = jdata['adaptados_activos']
        context['adaptados_en_servicio'] = adaptados_en_servicio
        context['adaptados_activos'] = adaptados_activos

        context['adaptados_en_servicio'] = jdata['adaptados_en_servicio']
        context['adaptados_activos'] = jdata['adaptados_activos']

        context['porc_adaptados_en_servicio'] = adaptados_en_servicio / \
            colectivos_totales
        context['porc_adaptados_activos'] = adaptados_activos / \
            colectivos_totales

        tot_on = reg + no_reg
        if tot_on == 0:
            tot_on = 1
        pnl = no_reg / (tot_on + epl)
        context['porc_no_logueado'] = pnl
        ppl = epl / (tot_on + epl)
        context['porc_pta_de_linea'] = ppl
        pa = reg / (tot_on + epl)
        context['porc_activo'] = pa

        return context


@method_decorator(cache_page(60), name='dispatch')
class TableroGoMobile(StaffuserRequiredMixin, TemplateView):

    template_name = 'backend/tableros/transporte/go-mobile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        app_go = SoftwarePublico.objects.get(pk=settings.APP_GO_PK)
        qsu = UsuarioSoftware.objects.filter(software=app_go)
        context['usuarios_go_totales'] = qsu.count()
        ult_24_hs = timezone.now() - datetime.timedelta(days=1)
        context['usuaron_go_ult_24_hs'] = qsu.filter(
            modified__gt=ult_24_hs).count()
        ult_semana = timezone.now() - datetime.timedelta(days=7)
        context['usuaron_go_ult_semana'] = qsu.filter(
            modified__gt=ult_semana).count()

        if context['usuarios_go_totales'] == 0:
            context['usuarios_go_totales'] = 1
        context['porc_ult_24_hs'] = round(
            100 *
            context['usuaron_go_ult_24_hs'] /
            context['usuarios_go_totales'],
            2)
        context['porc_ult_semana'] = round(
            100 *
            context['usuaron_go_ult_semana'] /
            context['usuarios_go_totales'],
            2)

        one_week_ago = timezone.now() - datetime.timedelta(days=7)
        qsl = LogUsuario.objects.filter(
            sistema=app_go, created__gt=one_week_ago)
        opiniones = qsl.filter(codigo_accion='OPINA')
        valoraciones = DetalleLogUsuario.objects.filter(
            log_usuario__in=opiniones, codigo='valoracion').values(
            'log_usuario__created', 'str_val')
        valoraciones_por_periodo = valoraciones.annotate(
            valoracion=F('str_val'))
        valoraciones_por_periodo = valoraciones_por_periodo.values(
            'valoracion')
        valoraciones_por_periodo = valoraciones_por_periodo.annotate(
            cantidad=Count('id'))
        valoraciones_por_periodo = valoraciones_por_periodo.values(
            'valoracion', 'cantidad')
        valoraciones_por_periodo = valoraciones_por_periodo.order_by(
            '-cantidad')
        context['valoraciones'] = valoraciones_por_periodo

        return context


@method_decorator(cache_page(60 * 60 * 1), name='dispatch')
class TablerosTransporte(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros/transporte/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        req = requests.get(
            'https://cordobus.apps.cordoba.gob.ar/data/datos-de-internos/')
        basedata = req.content.decode('utf-8')
        jdata = json.loads(basedata)[0]

        colectivos_activos_registrados = jdata['registrados']
        colectivos_en_punta_de_linea = jdata['en_punta_de_lineas']

        context['colectivos_activos_registrados'] = colectivos_activos_registrados
        context['colectivos_en_punta_de_linea'] = colectivos_en_punta_de_linea
        app_go = SoftwarePublico.objects.get(pk=settings.APP_GO_PK)

        dias_atras = int(self.request.GET.get('dias-atras', 7))
        context['dias_atras'] = dias_atras
        one_week_ago = timezone.now() - datetime.timedelta(days=dias_atras)
        periodo = self.request.GET.get('periodo', 'hora')
        context['periodo'] = periodo

        qsu = UsuarioSoftware.objects.filter(software=app_go)
        context['usuarios_go_totales'] = qsu.count()
        ult_24_hs = timezone.now() - datetime.timedelta(days=1)
        context['usuaron_go_ult_24_hs'] = qsu.filter(
            modified__gt=ult_24_hs).count()
        ult_semana = timezone.now() - datetime.timedelta(days=7)
        context['usuaron_go_ult_semana'] = qsu.filter(
            modified__gt=ult_semana).count()

        usuarios_este_periodo = qsu.filter(created__gt=one_week_ago)
        if periodo == 'hora':
            periodo_trunch = TruncHour('created')
        elif periodo == 'dia':
            periodo_trunch = TruncDay('created')

        nuevos_por_periodo = usuarios_este_periodo.annotate(
            periodo=periodo_trunch).values('periodo')
        nuevos_por_periodo = nuevos_por_periodo.annotate(
            usuarios=Count('id')).values(
            'periodo', 'usuarios')
        nuevos_por_periodo = nuevos_por_periodo.order_by('periodo')

        lista_nuevos = []
        lista_nuevos.append(
            [{'type': 'date', 'label': 'periodo'}, 'nuevos usuarios'])
        for per in nuevos_por_periodo:
            ms = int(time.mktime(per['periodo'].timetuple())) * 1000
            h = 'Date({})'.format(ms)
            lista_nuevos.append([h, per['usuarios']])

        context['lista_nuevos'] = lista_nuevos

        qsl = LogUsuario.objects.filter(
            sistema=app_go, created__gt=one_week_ago)
        acciones = qsl.values('codigo_accion').annotate(
            total_acciones=Count('codigo_accion'))
        context['acciones_usuarios_este_periodo'] = acciones

        if periodo == 'hora':
            periodo_trunch = TruncHour('created')
        elif periodo == 'dia':
            periodo_trunch = TruncDay('created')
        acciones_por_periodo = qsl.annotate(
            periodo=periodo_trunch).values('periodo')
        acciones_por_periodo = acciones_por_periodo.annotate(
            usos=Count('id')).values('periodo', 'usos')
        acciones_por_periodo = acciones_por_periodo.order_by('periodo')

        lista_acciones_por_periodo = []
        lista_acciones_por_periodo.append(
            [{'type': 'date', 'label': 'periodo'}, 'acciones'])

        for p in acciones_por_periodo:
            ms = int(time.mktime(p['periodo'].timetuple())) * 1000
            h = 'Date({})'.format(ms)
            lista_acciones_por_periodo.append([h, p['usos']])

        context['lista_acciones_por_periodo'] = lista_acciones_por_periodo

        opiniones = qsl.filter(codigo_accion='OPINA')
        valoraciones = DetalleLogUsuario.objects.filter(
            log_usuario__in=opiniones, codigo='valoracion').values(
            'log_usuario__created', 'str_val')

        if periodo == 'hora':
            periodo_trunch = TruncHour('log_usuario__created')
        elif periodo == 'dia':
            periodo_trunch = TruncDay('log_usuario__created')

        valoraciones_por_periodo = valoraciones.annotate(
            dia=periodo_trunch, valoracion=F('str_val'))
        valoraciones_por_periodo = valoraciones_por_periodo.values(
            'dia', 'valoracion')
        valoraciones_por_periodo = valoraciones_por_periodo.annotate(
            cantidad=Count('id'))
        valoraciones_por_periodo = valoraciones_por_periodo.values(
            'dia', 'valoracion', 'cantidad')
        valoraciones_por_periodo = valoraciones_por_periodo.order_by('dia')

        lista_valoraciones_por_periodo = []
        # ver las acciones que hay
        valoraciones = []
        for caso in valoraciones_por_periodo:
            if caso['valoracion'] not in valoraciones:
                valoraciones.append(caso['valoracion'])

        dias = {}
        for caso in valoraciones_por_periodo:
            ms = int(time.mktime(caso['dia'].timetuple())) * 1000
            d = 'Date({})'.format(ms)
            if d not in dias.keys():
                dias[d] = {}
                for v in valoraciones:
                    dias[d][v] = 0
            dias[d][caso['valoracion']] += caso['cantidad']

        lista_valoraciones_por_periodo.append(
            [{'type': 'date', 'label': 'periodo'}] + valoraciones)
        for key, value in dias.items():
            linea = [key]
            for v in valoraciones:
                linea.append(value[v])
            lista_valoraciones_por_periodo.append(linea)

        context['lista_valoraciones_por_periodo'] = lista_valoraciones_por_periodo

        return context


@method_decorator(cache_page(60), name='dispatch')
class TablerosTransporteMensajesGo(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros/transporte/mensajes-go.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # app_go = SoftwarePublico.objects.get(pk=settings.APP_GO_PK)
        minutos = int(self.request.GET.get('minutos', 1440))
        context['minutos'] = minutos
        ult_minutos = timezone.now() - datetime.timedelta(minutes=minutos)

        mensajes_directos = MensajeDirecto.objects.filter(
            created__gt=ult_minutos)  # .order_by('-id')
        mensajes_grupales = MensajeGrupalAUsuario.objects.filter(
            created__gt=ult_minutos)  # .order_by('-id')

        context['mensajes_directos'] = mensajes_directos
        context['mensajes_grupales'] = mensajes_grupales

        context['total_mensajes_directos'] = mensajes_directos.count()
        context['total_mensajes_grupales'] = mensajes_grupales.count()

        mensajes_directos_agrupados = mensajes_directos.values('estado').annotate(
            opiniones=Count('estado')).values('estado', 'opiniones')
        mensajes_grupales_agrupados = mensajes_grupales.values('estado').annotate(
            opiniones=Count('estado')).values('estado', 'opiniones')

        mensajes_directos_agrupados_to_google_chart = []
        if len(mensajes_directos_agrupados) == 0:  # muchas veces no hay
            mensajes_directos_agrupados_to_google_chart.append(
                ['Sin opiniones', 1, '1'])
        else:
            for msj in mensajes_directos_agrupados:
                estado = dict(Mensaje.estados)[msj['estado']]
                opiniones = msj['opiniones']
                mensajes_directos_agrupados_to_google_chart.append(
                    [estado, opiniones, str(opiniones)])

        mensajes_grupales_agrupados_to_google_chart = []
        for msj in mensajes_grupales_agrupados:
            estado = dict(Mensaje.estados)[msj['estado']]
            opiniones = msj['opiniones']
            mensajes_grupales_agrupados_to_google_chart.append(
                [estado, opiniones, str(opiniones)])

        context['mensajes_directos_agrupados'] = mensajes_directos_agrupados_to_google_chart
        context['mensajes_grupales_agrupados'] = mensajes_grupales_agrupados_to_google_chart

        return context


@method_decorator(cache_page(60), name='dispatch')
class TablerosTransporteEstadisticas(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros/transporte/estadisticas.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        app_go = SoftwarePublico.objects.get(pk=settings.APP_GO_PK)

        # Cantidad de errores en general
        # el codigo = error + int_val = 0 es un _no-error_

        minutos = int(self.request.GET.get('minutos', 15))
        context['minutos'] = minutos
        # -------------------------------------------------------
        ult_minutos = timezone.now() - datetime.timedelta(minutes=minutos)
        # BASE DE TODAS LAS CONSULTAS
        logs = DetalleLogUsuario.objects.filter(
            log_usuario__sistema=app_go,
            log_usuario__created__gt=ult_minutos,
            log_usuario__codigo_accion='PIDEBONDI')
        # -------------------------------------------------------
        # -------------------------------------------------------

        # Cantidad de errores por id
        errores = logs.filter(codigo='error')
        dets = errores.values('int_val').annotate(
            cantidad_errores=Count('int_val')).order_by('-cantidad_errores')
        lista_error = []
        lista_error.append(['tipo', 'cantidad errores'])
        codigos = {0: 'OK',
                   1: 'Recorrido no existe',
                   2: 'NINGUN interno activo',
                   3: 'Ya paso el bondi',
                   500: 'Error 500',
                   502: 'Error 502',
                   404: 'Poste (o recorrido) inexistente'
                   }

        for e in dets:
            if e['int_val'] not in codigos.keys():
                codigos[e['int_val']] = 'ERROR {}'.format(e['int_val'])
            codigo = codigos[e['int_val']]
            lista_error.append([codigo, e['cantidad_errores']])
        context['errores_tipo'] = lista_error

        # ver lineas que dan OK y las que dan mal
        # es incómodo (mejorar) llegar a saber el recorrido de cada línea
        lineas = {}
        usuarios = []  # contar solo uno por cada persona y linea. La app hace multiples llamadas

        for det in errores:
            iv = det.int_val

            log = det.log_usuario
            idsrecorrido = log.detalles.filter(codigo='recorrido_id')
            recorrido_id = 0 if idsrecorrido.count(
            ) == 0 else idsrecorrido[0].int_val
            recorridos = get_recorrido_cordobus(pk=recorrido_id)
            # recorridos = RecorridoLineaTransportePublico.objects.filter(pk=recorrido_id)
            recorrido = None if len(recorridos) == 0 else recorridos[0]
            linea = None if recorrido is None else recorrido['properties']['linea']
            linea_nombre = 'S/D' if linea is None else linea['nombre_publico']

            linea_id = 'LINEAUNK' if linea is None else linea['id']
            user = None if log.user is None else log.user
            user_id = 'USERUNK' if user is None else user.id
            unique_user = '{}-{}'.format(linea_id, user_id)
            if unique_user not in usuarios:
                usuarios.append(unique_user)
            else:
                continue  # no duplicar registros de la misma persona

            if linea_nombre not in lineas.keys():
                lineas[linea_nombre] = {}
                for codigo, value in codigos.items():
                    lineas[linea_nombre][value] = 0

            det = codigos[iv]
            lineas[linea_nombre][det] += 1

            # distancia_recibida_km
            # demora_recibida_minutos

        # mandar el resumen
        campos = ['Linea']
        for codigo, value in codigos.items():
            campos.append(value)
        resultados_lineas = [campos]

        # ordenar para el gráfico
        # sorted(statuses,key=lambda x:lineas[x]['OK'])
        for linea, valor in lineas.items():
            valores = [linea]
            for codigo, value in codigos.items():
                valores.append(valor[value])
                # linea, valor['OK'], valor['MAL']
            resultados_lineas.append(valores)

        context['resultados_lineas'] = resultados_lineas

        """ TODO hacer que se agrupen en rangos de minutos y kilometros
        # Ranking minutos
        dets = logs.filter(codigo='demora_recibida_minutos').values('decimal_val').annotate(ranking_minutos=Count('decimal_val'))
        lista_error = []
        lista_error.append(['ranking_minutos', 'minuto'])
        for m in dets:
            mdec = 0 if m['decimal_val'] is None else m['decimal_val']
            lista_error.append([m['ranking_minutos'], float(mdec)])
        context['ranking_minutos'] = lista_error

        # Ranking km
        dets = logs.filter(codigo='distancia_recibida_km').values('decimal_val').annotate(ranking_kilometros=Count('decimal_val'))
        lista_error = []
        lista_error.append(['ranking_kilometros', 'kilometros'])
        for k in dets:
            km = 0 if k['decimal_val'] is None else k['decimal_val']
            lista_error.append([k['ranking_kilometros'], float(km)])
        context['ranking_km'] = lista_error

        """

        """
        # Ocurrencias de lineas y recorridos de bondis
        pbondi_log = DetalleLogUsuario.objects.filter(log_usuario__sistema=app_go,
                                                        log_usuario__codigo_accion='PIDEBONDI',
                                                        log_usuario__created__gt=ult_semana,
                                                        codigo='recorrido_id')


        recorridos = RecorridoLineaTransportePublico.objects.all()

        tmp_nombre = []
        tmp_descripcion = []
        for pb in pbondi_log:
            ress = recorridos.filter(id=pb.int_val)
            if ress.count() > 0:
                res = ress[0]
                nombre = res.linea.nombre_publico
                des = res.descripcion_corta
                tmp_nombre.append(nombre)
                tmp_descripcion.append(des)
            else:
                # quizas no existe más
                logger.info('Ya no existe el recorrido ID {}'.format(pb.id))

        ocurrencias_lineas = collections.Counter(tmp_nombre)
        ocurrencias_recorridos = collections.Counter(tmp_descripcion)

        l_tmp_1 = [['lineas', 'ocurrencias']] + list(map(list, ocurrencias_lineas.items()))
        l_tmp_2 = [['recorridos', 'ocurrencias']] + list(map(list, ocurrencias_recorridos.items()))
        context['ocurrencias_lineas_bondis'] = l_tmp_1
        context['ocurrencias_recorridos_bondis'] = l_tmp_2
        """

        return context


class TableExcelMixin:
    """
    Este mixin, incorporado a un ListView, permite exportar como CSV o XMLS
    el mismo contenido que la vista habitualmente muestra en una tabla html,
    simplemente pasando `?export_as=<formato>` a la url asociada.

    Se realiza renderizando la pagina y parseando el contenido de las celdas
    via PyQuery. Esta estrategia permite garantizar que el contenido del
    archivo exportado tendrá exactamente el mismo contenido que la version html
    manteniendo simplemente un solo template.

    Por omision busca la primera etiqueta ``table`` que encuentre para extraer
    el contenido, y el primer ``h1`` para asignar un nombre al archivo generado.
    Estos *selectores* se pueden reemplazar definiendo los atributos de clase
    ``table_selector`` y ``title_selector``.

    Para incluir el *dropdown* (botones de exportación)  incluir este
    fragmento en el template::

        {% include "backend/tableros/portaldedatos/export_table_buttons.html" %}

    """
    table_selector = 'table:first'
    title_selector = 'h1:first'

    @property
    def export_format(self):
        filetype = self.request.GET.get('export_as')
        if filetype in ('csv', 'xls'):
            return filetype

    def get_paginate_by(self, queryset):
        """
        Paginate by specified value in querystring, or use default class property value.
        """
        return None if self.export_format else self.paginate_by

    def render_to_response(self, context, **response_kwargs):
        response = super().render_to_response(context, **response_kwargs)
        if self.export_format:
            pq = PyQuery(response.render().content)
            table = pq(self.table_selector)
            csv_list = []
            for row in pq('tr', table):
                csv_list.append([pq(cell).text()
                                 for cell in pq('th, td', row)])

            table_title = pq(self.title_selector).text()
            filename = '{}.{}'.format(slugify(table_title), self.export_format)
            return excel.make_response(
                excel.pe.Sheet(csv_list),
                self.export_format,
                file_name=filename)

        return response


class DesactualizadosListView(
        StaffuserRequiredMixin,
        TableExcelMixin,
        ListView):
    model = DatoPortal
    template_name = 'backend/tableros/portaldedatos/tablero-lista-desactualizados.html'

    def get_context_data(self, **kwargs):
        context = super(
            DesactualizadosListView,
            self).get_context_data(
            **kwargs)
        context['desactualizados'] = sorted(
            DatoPortal.desactualizados(),
            key=lambda d: d.retraso_nueva_version,
            reverse=True)
        return context


class VersionesDeDatosListView(
        StaffuserRequiredMixin,
        TableExcelMixin,
        ListView):
    model = Propuesta
    template_name = 'backend/tableros/portaldedatos/tablero-lista-versiones-de-datos.html'
    paginate_by = 400

    def get_queryset(self):
        return VersionDato.objects.filter(
            dato__estado=DatoPortal.PUBLICADO).order_by('fecha')

    def get_context_data(self, **kwargs):
        context = super(
            VersionesDeDatosListView,
            self).get_context_data(
            **kwargs)
        context['objects_count'] = self.object_list.count()
        return context


@method_decorator(cache_page(60 * 60 * 1), name='dispatch')
class TablerosObrasPublicas(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros/obras_publicas/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        obras_publicas = ObraPublica.objects.filter(
            publicado=True, publicado_gobierno=True)
        context['total_obras'] = obras_publicas.count()
        context['proyectadas'] = obras_publicas.filter(estado=1).count()
        context['licitadas'] = obras_publicas.filter(estado=2).count()
        context['adjudicadas'] = obras_publicas.filter(estado=3).count()
        context['contratadas'] = obras_publicas.filter(estado=4).count()
        context['replanteadas'] = obras_publicas.filter(estado=5).count()
        context['en_ejecucion'] = obras_publicas.filter(estado=6).count()
        context['en_ejecucion_por_ampliacion'] = obras_publicas.filter(
            estado=7).count()
        context['finalizadas'] = obras_publicas.filter(estado=8).count()
        context['rescindidas'] = obras_publicas.filter(estado=9).count()
        context['frentes_activos'] = TrazadoObraPublica.objects.filter(
            obra__publicado=True,
            obra__publicado_gobierno=True,
            publicado=True,
            publicado_gobierno=True).count()

        return context


@method_decorator(cache_page(60), name='dispatch')
class TablerosObrasPublicasMobile(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros/obras_publicas/tableros_obras_publicas_mobile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        obras_publicas = ObraPublica.objects.filter(
            publicado=True, publicado_gobierno=True)
        context['total_obras'] = obras_publicas.count()
        context['proyectadas'] = obras_publicas.filter(estado=1).count()
        context['licitadas'] = obras_publicas.filter(estado=2).count()
        context['adjudicadas'] = obras_publicas.filter(estado=3).count()
        context['contratadas'] = obras_publicas.filter(estado=4).count()
        context['replanteadas'] = obras_publicas.filter(estado=5).count()
        context['en_ejecucion'] = obras_publicas.filter(estado=6).count()
        context['en_ejecucion_por_ampliacion'] = obras_publicas.filter(
            estado=7).count()
        context['finalizadas'] = obras_publicas.filter(estado=8).count()
        context['rescindidas'] = obras_publicas.filter(estado=9).count()
        context['frentes_activos'] = TrazadoObraPublica.objects.filter(
            obra__publicado=True,
            obra__publicado_gobierno=True,
            publicado=True,
            publicado_gobierno=True).count()

        return context


@method_decorator(cache_page(60 * 60 * 1), name='dispatch')
class TablerosObrasDeTerceros(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros/obras_de_terceros/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        obras_de_terceros = ObraDeTerceros.objects.filter(
            publicado=True, publicado_gobierno=True)
        context['total_obras'] = obras_de_terceros.count()
        context['proyectadas'] = obras_de_terceros.filter(estado=1).count()
        context['licitadas'] = obras_de_terceros.filter(estado=2).count()
        context['adjudicadas'] = obras_de_terceros.filter(estado=3).count()
        context['contratadas'] = obras_de_terceros.filter(estado=4).count()
        context['replanteadas'] = obras_de_terceros.filter(estado=5).count()
        context['en_ejecucion'] = obras_de_terceros.filter(estado=6).count()
        context['en_ejecucion_por_ampliacion'] = obras_de_terceros.filter(
            estado=7).count()
        context['finalizadas'] = obras_de_terceros.filter(estado=8).count()
        context['rescindidas'] = obras_de_terceros.filter(estado=9).count()
        context['frentes_activos'] = TrazadoObraDeTercero.objects.filter(
            obra__publicado=True,
            obra__publicado_gobierno=True,
            publicado=True,
            publicado_gobierno=True).count()

        return context


@method_decorator(cache_page(60), name='dispatch')
class TablerosObrasDeTercerosMobile(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros/obras_de_terceros/tableros_obras_de_terceros_mobile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        obras_de_terceros = ObraDeTerceros.objects.filter(
            publicado=True, publicado_gobierno=True)
        context['total_obras'] = obras_de_terceros.count()
        context['proyectadas'] = obras_de_terceros.filter(estado=1).count()
        context['licitadas'] = obras_de_terceros.filter(estado=2).count()
        context['adjudicadas'] = obras_de_terceros.filter(estado=3).count()
        context['contratadas'] = obras_de_terceros.filter(estado=4).count()
        context['replanteadas'] = obras_de_terceros.filter(estado=5).count()
        context['en_ejecucion'] = obras_de_terceros.filter(estado=6).count()
        context['en_ejecucion_por_ampliacion'] = obras_de_terceros.filter(
            estado=7).count()
        context['finalizadas'] = obras_de_terceros.filter(estado=8).count()
        context['rescindidas'] = obras_de_terceros.filter(estado=9).count()
        context['frentes_activos'] = TrazadoObraDeTercero.objects.filter(
            obra__publicado=True,
            obra__publicado_gobierno=True,
            publicado=True,
            publicado_gobierno=True).count()

        return context


@method_decorator(cache_page(60 * 60 * 1), name='dispatch')
class TablerosArbolado(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros/arbolado/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        arboles = Arbol.objects.all()
        especies = EspecieArbol.objects.all()
        context['especies_mas_registradas'] = especies.exclude(
            nombre='Ausente') .exclude(
            nombre='Otra especie') .annotate(
            cantidad_arboles=Count('arboles')) .values(
                'nombre',
                'cantidad_arboles') .order_by('-cantidad_arboles')[
                    :5]
        context['total_arboles'] = arboles.count()
        context['problema_desrame'] = arboles.filter(
            problema_desrame=True).count()
        context['desequilibrio_copa'] = arboles.filter(
            desequilibrio_copa=True).count()

        return context


@method_decorator(cache_page(60), name='dispatch')
class TablerosArboladoMobile(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros/arbolado/tableros_arbolado_mobile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        arboles = Arbol.objects.all()
        especies = EspecieArbol.objects.all()
        context['especies_mas_registradas'] = especies.exclude(
            nombre='Ausente') .exclude(
            nombre='Otra especie') .annotate(
            cantidad_arboles=Count('arboles')) .values(
                'nombre',
                'cantidad_arboles') .order_by('-cantidad_arboles')[
                    :5]
        context['total_arboles'] = arboles.count()
        context['problema_desrame'] = arboles.filter(
            problema_desrame=True).count()
        context['desequilibrio_copa'] = arboles.filter(
            desequilibrio_copa=True).count()

        return context


@method_decorator(cache_page(600), name='dispatch')
class TablerosPlanDeMetas(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/tableros/plan_de_metas/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        plan_de_metas_id = self.kwargs.get('plan-de-metas-id', 1)
        plan_de_metas = PlanDeMetas.objects.get(pk=plan_de_metas_id)
        metas = Meta.objects.filter(
            objetivo__componente__lineamiento__plan_de_metas=plan_de_metas,
            publicado=True)

        context['total_metas'] = metas.count()

        # total por estados
        total_por_estados_de_avance = {}
        por_estados = metas.values('estado_de_avance').annotate(
            total=Count('estado_de_avance')).order_by('estado_de_avance')
        for estado in por_estados:
            estado_str = dict(Meta.estados)[estado['estado_de_avance']]
            total_por_estados_de_avance[estado_str] = estado['total']
        context['total_por_estados_de_avance'] = total_por_estados_de_avance

        # total por estados en proceso de carga y validacion antes de la presentacion
        # como hay algunas que el new_estado es _no iniciado_ y en realidad
        # quedará el anterior
        total_por_estados_de_avance_new = {}
        # de paso procesar tambien x secretaría
        total_por_secretaria_y_estados_de_avance = {}
        # de paso procesar tambien x secretaría en estado nuevo
        total_por_secretaria_y_estados_de_avance_new = {}
        for meta in metas:
            new_estado = max(meta.estado_de_avance, meta.new_estado_de_avance)
            estado_new_str = dict(Meta.estados)[new_estado]
            estado_str = dict(Meta.estados)[meta.estado_de_avance]
            if estado_new_str not in total_por_estados_de_avance_new.keys():
                total_por_estados_de_avance_new[estado_new_str] = 0
            total_por_estados_de_avance_new[estado_new_str] += 1

            # por secretarías en estado nuevo y viejo
            secretaria = meta.oficina_responsable.area_gobierno.oficina
            if secretaria not in total_por_secretaria_y_estados_de_avance.keys():
                total_por_secretaria_y_estados_de_avance[secretaria] = {}
                total_por_secretaria_y_estados_de_avance_new[secretaria] = {}

            if estado_str not in total_por_secretaria_y_estados_de_avance[secretaria].keys(
            ):
                total_por_secretaria_y_estados_de_avance[secretaria][estado_str] = 0
            total_por_secretaria_y_estados_de_avance[secretaria][estado_str] += 1

            if estado_new_str not in total_por_secretaria_y_estados_de_avance_new[secretaria].keys(
            ):
                total_por_secretaria_y_estados_de_avance_new[secretaria][estado_new_str] = 0
            total_por_secretaria_y_estados_de_avance_new[secretaria][estado_new_str] += 1

        context['total_por_estados_de_avance_new'] = total_por_estados_de_avance_new
        context['total_por_secretaria_y_estados_de_avance'] = total_por_secretaria_y_estados_de_avance
        context['total_por_secretaria_y_estados_de_avance_new'] = total_por_secretaria_y_estados_de_avance_new

        return context


@cache_page(60)
def TableroGoMobilePorDia(request, desde=None, hasta=None):

    if (desde is None) and (hasta is None):
        # Si no esta definido lo seteamos siempre al dia de ayer
        desde = pytz.timezone("America/Argentina/Cordoba").localize(
            datetime.datetime.now()) - datetime.timedelta(days=1)
        hasta = pytz.timezone("America/Argentina/Cordoba").localize(
            datetime.datetime.now()) - datetime.timedelta(days=1)
    else:
        desde = datetime.datetime.strptime(desde, '%d-%m-%Y')
        hasta = datetime.datetime.strptime(hasta, '%d-%m-%Y')

    # Vamos a tomar los registrados desde principio de año
    principio_anio = datetime.datetime.strptime('01-01-2019', '%d-%m-%Y')

    # Obtenemos los dias entre 'desde' y 'hasta'
    d = desde
    delta = datetime.timedelta(days=1)
    lista_de_dias = []
    while d <= hasta:
        lista_de_dias.append(d)
        d += delta

    # Convertimos la lista a lista de tuplas para despues usarla con range
    lista_de_tuplas = list(zip(lista_de_dias, lista_de_dias[1:]))

    # Inicializamos listas con datos
    lista_data = []

    app_go = SoftwarePublico.objects.get(pk=settings.APP_GO_PK)
    qsu = UsuarioSoftware.objects.filter(software=app_go)
    # Por cada tupla de la lista anterior, obtenemos los activos, registrados y no activos
    # Trabajamos sobro los registrados en el 2019!
    registrados = qsu.filter(created__gt=principio_anio).count()

    for tupla in lista_de_tuplas:
        data = []
        fecha = tupla[0]
        fecha_str = str(fecha.strftime('%d-%m-%Y'))
        data.append(fecha_str)
        activos = qsu.filter(modified__range=(tupla[0], tupla[1])).count()
        data.append(activos)
        # no_activos = registrados - activos
        # data.append(no_activos)
        lista_data.append(data)

    context = {
        'lista_data': lista_data,
        'registrados': registrados
    }

    url = 'backend/tableros/transporte/go-mobile-diario.html'

    return render(request, url, context)


@cache_page(60)
def TableroGoMobileRegistradosPorDia(request, desde=None, hasta=None):

    # Seteamos a una semana si no se define. Asi lo tiene el 1
    if (desde is None) and (hasta is None):
        # Si no esta definido lo seteamos siempre al dia de ayer
        desde = pytz.timezone("America/Argentina/Cordoba").localize(
            datetime.datetime.now()) - datetime.timedelta(days=7)
        hasta = pytz.timezone("America/Argentina/Cordoba").localize(
            datetime.datetime.now()) - datetime.timedelta(days=1)
    else:
        desde = datetime.datetime.strptime(desde, '%d-%m-%Y')
        hasta = datetime.datetime.strptime(hasta, '%d-%m-%Y')

    # Vamos a tomar los registrados desde principio de año
    # principio_anio = datetime.datetime.strptime('01-01-2019', '%d-%m-%Y')

    # Obtenemos los dias entre 'desde' y 'hasta'
    d = desde
    delta = datetime.timedelta(days=1)
    lista_de_dias = []
    while d <= hasta:
        lista_de_dias.append(d)
        d += delta

    # Convertimos la lista a lista de tuplas para despues usarla con range
    lista_de_tuplas = list(zip(lista_de_dias, lista_de_dias[1:]))

    # Inicializamos listas con datos
    lista_data = []

    app_go = SoftwarePublico.objects.get(pk=settings.APP_GO_PK)
    qsu = UsuarioSoftware.objects.filter(software=app_go)
    # Por cada tupla de la lista anterior, obtenemos los activos, registrados y no activos
    # Trabajamos sobro los registrados en el 2019!
    # registrados = qsu.filter(created__gt=principio_anio).count()

    for tupla in lista_de_tuplas:
        data = []
        fecha = tupla[0]
        fecha_str = str(fecha.strftime('%d-%m-%Y'))
        data.append(fecha_str)
        registrados = qsu.filter(created__range=(tupla[0], tupla[1])).count()
        data.append(registrados)
        # no_activos = registrados - activos
        # data.append(no_activos)
        lista_data.append(data)

    context = {
        'lista_data': lista_data
        # 'registrados': registrados
    }

    url = 'backend/tableros/transporte/go-mobile-registrados-diario.html'

    return render(request, url, context)


@method_decorator(cache_page(60), name='dispatch')
class TablerosTransporteUnidadesActivasDescripcion(TemplateView):
    # Misma clase que arriba pero sin el StaffuserRequiredMixin

    template_name = 'backend/tableros/transporte/unidades-activas-mobile-descripcion.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        req = requests.get(
            'https://cordobus.apps.cordoba.gob.ar/data/datos-de-internos/')
        basedata = req.content.decode('utf-8')
        jdata = json.loads(basedata)[0]

        reg = jdata['registrados']
        no_reg = jdata['no_registrados']

        context['colectivos_activos_registrados'] = reg
        context['colectivos_activos_no_registrados'] = no_reg

        epl = jdata['en_punta_de_lineas']
        context['colectivos_en_punta_de_linea'] = epl
        colectivos_totales = reg + epl + no_reg
        # Restamos el 10% de flota porque son los que quedan para reserva
        context['colectivos_totales'] = int(
            colectivos_totales - (colectivos_totales * 0.10))

        adaptados_en_servicio = jdata['adaptados_en_servicio']
        adaptados_activos = jdata['adaptados_activos']
        context['adaptados_en_servicio'] = adaptados_en_servicio
        context['adaptados_activos'] = adaptados_activos

        context['adaptados_en_servicio'] = jdata['adaptados_en_servicio']
        context['adaptados_activos'] = jdata['adaptados_activos']

        context['porc_adaptados_en_servicio'] = adaptados_en_servicio / \
            colectivos_totales
        context['porc_adaptados_activos'] = adaptados_activos / \
            colectivos_totales

        tot_on = reg + no_reg
        if tot_on == 0:
            tot_on = 1
        pnl = no_reg / (tot_on + epl)
        context['porc_no_logueado'] = pnl
        ppl = epl / (tot_on + epl)
        context['porc_pta_de_linea'] = ppl
        pa = reg / (tot_on + epl)
        context['porc_activo'] = pa

        return context


# @cache_page(60)
# def TableroGoMobileRegistradosMensualAcumulado(request, desde=None,
# hasta=None):

#     # Seteamos a una semana si no se define. Asi lo tiene el 1
#     if (desde is None) and (hasta is None):
#         # Si no esta definido lo seteamos siempre al dia de ayer
#         desde = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=7)
#         hasta = pytz.timezone("America/Argentina/Cordoba").localize(datetime.datetime.now()) - datetime.timedelta(days=1)
#     else:
#         desde = datetime.datetime.strptime(desde, '%d-%m-%Y')
#         hasta = datetime.datetime.strptime(hasta, '%d-%m-%Y')

#     # Vamos a tomar los registrados desde principio de año
#     principio_anio = datetime.datetime.strptime('01-01-2019', '%d-%m-%Y')

#     # Obtenemos los dias del mes 'desde' y del mes 'hasta'
#     # Comenzamos con 'desde'
#     anio_desde = desde.year
#     mes_desde = desde.month
#     # Obtemos una tupla de la forma (3, 31). Donde el primer digito no nos importa.
#     tupla_anio_dias_desde = monthrange(anio_desde, mes_desde)
#     # Armamos el range desde el primer dia del mes hasta el que nos devolvio la funcion de arriba
#     range_desde = (1, tupla_anio_dias_desde[1])
#     # Seguimos con 'hasta'
#     anio_hasta = hasta.year
#     mes_hasta = hasta.month
#     tupla_anio_dias_hasta = monthrange(anio_hasta, mes_hasta)
#     # Armamos el range desde el primer dia del mes hasta el que nos devolvio la funcion de arriba
#     range_hasta = (1, tupla_anio_dias_hasta[1])

#     dic_de_dias = {}
#     while mes_desde <= mes_hasta:
#         tupla_dias = monthrange(anio_desde, mes_desde)
#         range_dias = (1, tupla_dias[1])
#         dic_tmp = {'mes_{}'.format(mes_desde): {
#                         'range': range_dias}
#                         }
#         dic_de_dias.update(dic_tmp)
#         # Aumentamos el mes para ir sacando los datos necesarios
#         mes_desde += 1

#     # Convertimos la lista a lista de tuplas para despues usarla con range
#     lista_de_tuplas = list(zip(lista_de_dias, lista_de_dias[1:]))

#     # Inicializamos listas con datos
#     lista_data = []

#     app_go = SoftwarePublico.objects.get(pk=settings.APP_GO_PK)
#     qsu = UsuarioSoftware.objects.filter(software=app_go)
#     # Por cada tupla de la lista anterior, obtenemos los activos, registrados y no activos
#     # Trabajamos sobro los registrados en el 2019!
#     # registrados = qsu.filter(created__gt=principio_anio).count()

#     # ARMAR BIEN LO DE ABAJO

#     for tupla in lista_de_tuplas:
#         data = []
#         fecha = tupla[0]
#         fecha_str = str(fecha.strftime('%d-%m-%Y'))
#         data.append(fecha_str)
#         registrados = qsu.filter(created__range=('{}-{}-{}'.format(desde.year, desde.month, tupla[0]), '{}-{}-{}'.format(desde.year, desde.month, tupla[1])))
#         data.append(registrados)
#         # no_activos = registrados - activos
#         # data.append(no_activos)
#         lista_data.append(data)


#     context = {
#                 'lista_data': lista_data
#                 #'registrados': registrados
#               }


#     url = 'backend/tableros/transporte/go-mobile-registrados-diario.html'

#     return render(request, url, context)
