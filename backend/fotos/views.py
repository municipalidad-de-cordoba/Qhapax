from django.core.urlresolvers import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView

from braces.views import StaffuserRequiredMixin
from fotos.models import AlbumPrensa, ImagenPrensa

from .forms import AlbumPrensaForm, ImagenPrensaForm


class FotoListView(StaffuserRequiredMixin, ListView):
    model = ImagenPrensa
    paginate_by = '25'
    template_name = 'backend/fotos/fotos_list.html'

    def get_queryset(self):
        """Define el queryset """
        queryset = super(FotoListView, self).get_queryset()
        return queryset.all()


class FotoCreateView(StaffuserRequiredMixin, CreateView):
    model = AlbumPrensa
    template_name = 'backend/fotos/fotos_create.html'
    success_url = reverse_lazy('backend.fotos.index')
    form_class = AlbumPrensaForm

    def form_valid(self, form):
        files = self.request.FILES.getlist('imagen')

        data = super(FotoCreateView, self).form_valid(form)
        for f in files:
            imagen = ImagenPrensa(
                titulo=form.cleaned_data['titulo'],
                descripcion=form.cleaned_data['descripcion'],
                imagen=f,
                album=form.instance
            )
            imagen.save()
            imagen.tags = form.cleaned_data['tags']
            imagen.personas = form.cleaned_data['personas']
            imagen.oficinas = form.cleaned_data['oficinas']

        return data

    def get_context_data(self, **kwargs):
        context = super(FotoCreateView, self).get_context_data(**kwargs)
        context['imagenform'] = ImagenPrensaForm()
        return context
