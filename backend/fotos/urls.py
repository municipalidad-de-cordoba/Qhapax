from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.FotoListView.as_view(), name='backend.fotos.index'),
    url(r'^subir/$', views.FotoCreateView.as_view(), name='backend.fotos.crear'),

]
