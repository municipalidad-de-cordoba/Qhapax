from django.forms import ModelForm, TextInput, FileInput

from fotos.models import ImagenPrensa, AlbumPrensa


class AlbumPrensaForm(ModelForm):
    class Meta:
        model = AlbumPrensa
        fields = ["titulo", "descripcion", "tags", "personas", "oficinas"]
        widgets = {
            'titulo': TextInput(
                attrs={
                    'placeholder': 'título de la/s foto/s',
                    'class': 'form-control'}),
            'descripcion': TextInput(
                attrs={
                    'placeholder': 'Descripcion',
                    'class': 'form-control'}),
        }


class ImagenPrensaForm(ModelForm):
    class Meta:
        model = ImagenPrensa
        fields = [
            "titulo",
            "descripcion",
            "tags",
            "personas",
            "oficinas",
            "imagen"]

        widgets = {
            'titulo': TextInput(
                attrs={
                    'placeholder': 'título de la/s foto/s',
                    'class': 'form-control'}),
            'descripcion': TextInput(
                attrs={
                    'placeholder': 'Descripcion',
                    'class': 'form-control'}),
            'imagen': FileInput(
                attrs={
                    'multiple': True})}
