from django.conf.urls import url
from . import views


urlpatterns = [url(r'^$',
                   views.AccesoInfoPublicaListView.as_view(),
                   name='backend.accesoinfopublica.index'),
               url(r'^edit/(?P<pk>[-_\d]+)/$',
                   views.AccesoInfoPublicaListView.as_view(),
                   name='backend.accesoinfopublica.edit'),
               ]
