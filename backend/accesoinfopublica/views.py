# -*- coding: utf-8 -*-
from django.views.generic.base import TemplateView
from braces.views import StaffuserRequiredMixin


class AccesoInfoPublicaListView(StaffuserRequiredMixin, TemplateView):
    template_name = 'backend/accesoinfopublica/list.html'
