import os

from django.core.management.base import BaseCommand
from django.conf import settings

from funcionarios.models import Funcion


class Command(BaseCommand):
    help = "Pongo en cero el campo de los usuarios que tengan cargada foto pero que el archivo no exista"

    def handle(self, *args, **options):
        for funcion in Funcion.objects.filter(activo=True):
            if funcion.funcionario.foto:
                if not os.path.isfile(
                        str(settings.MEDIA_ROOT) + '/' + str(funcion.funcionario.foto)):
                    persona = funcion.funcionario
                    persona.foto = ''
                    persona.save()
