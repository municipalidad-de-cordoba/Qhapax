FROM python:3.5
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code

ADD requirements.txt /code/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt


# falla el update https://unix.stackexchange.com/questions/508724/failed-to-fetch-jessie-backports-repository
RUN echo "deb [check-valid-until=no] http://cdn-fastly.deb.debian.org/debian jessie main" > /etc/apt/sources.list.d/jessie.list
RUN echo "deb [check-valid-until=no] http://archive.debian.org/debian jessie-backports main" > /etc/apt/sources.list.d/jessie-backports.list
RUN sed -i '/deb http:\/\/deb.debian.org\/debian jessie-updates main/d' /etc/apt/sources.list
RUN apt-get -o Acquire::Check-Valid-Until=false update
# RUN apt-get update 

RUN apt-get install binutils libproj-dev gdal-bin libgeoip1 --assume-yes
ADD . /code/

# Creo carpeta tmp y accedo a la misma.
RUN mkdir tmp
RUN cd tmp/

# Descargo la verion patched del wkhtmltopdf (Lo necesitamos para generar pdf)
RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz

# Descomprimo el archivo y genero links para su correcto funcionamiento
RUN tar xvf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
RUN mv wkhtmltox/bin/wkhtmlto* /usr/bin/
RUN ln -nfs /usr/bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf

# Una vez instalado todo, salgo de la carpeta tmp y la elimino.
RUN cd ..
RUN rm -R tmp/

# create unprivileged user
RUN adduser --disabled-password --gecos '' user_no_root
