# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-02-28 14:23
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('organizaciones', '0009_auto_20171214_1652'),
        ('core', '0022_auto_20180226_1032'),
    ]

    operations = [
        migrations.CreateModel(
            name='EntidadRegistranteFirma',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(help_text='Nombre de Fantasía', max_length=90)),
                ('url', models.URLField(blank=True, null=True)),
                ('organizacion', models.ForeignKey(blank=True, help_text='Por si es una organizacion que deba estar en nuestra DB', null=True, on_delete=django.db.models.deletion.CASCADE, to='organizaciones.Organizacion')),
            ],
        ),
        migrations.CreateModel(
            name='FirmaDigital',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('clave_publica', models.TextField(blank=True, help_text='Clave pública de la firma', null=True)),
                ('estado', models.PositiveIntegerField(choices=[(10, 'Desconocido'), (20, 'Firma digital de prueba'), (30, 'Firma Digital Vigente')], default=10)),
                ('mas_info_url', models.URLField(blank=True, help_text='Si existe, link a ver la certificación de la firma online', null=True)),
                ('entidad_registrante', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='firma_digital.EntidadRegistranteFirma')),
                ('organizacion', models.ForeignKey(blank=True, help_text='Si la forma es de una organización, va aquí', null=True, on_delete=django.db.models.deletion.CASCADE, to='organizaciones.Organizacion')),
                ('persona', models.ForeignKey(blank=True, help_text='Si la forma corresponde a una persona, va aquí', null=True, on_delete=django.db.models.deletion.CASCADE, to='core.Persona')),
            ],
        ),
    ]
