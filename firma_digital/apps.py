from django.apps import AppConfig


class FirmaDigitalConfig(AppConfig):
    name = 'firma_digital'
