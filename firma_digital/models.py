from django.db import models
from organizaciones.models import Organizacion
from core.models import Persona


class EntidadRegistranteFirma(models.Model):
    ''' entidad que genera y certifica la firma '''
    organizacion = models.ForeignKey(
        Organizacion,
        null=True,
        blank=True,
        help_text='Por si es una organizacion que deba estar en nuestra DB')
    nombre = models.CharField(max_length=90, help_text='Nombre de Fantasía')
    url = models.URLField(null=True, blank=True)

    @property
    def nombre_entidad(self):
        nombre2 = '' if self.organizacion is None else ' ({})'.format(
            self.organizacion.nombre)
        return '{}{}'.format(self.nombre, nombre2)

    def __str__(self):
        return self.nombre_entidad


class FirmaDigital(models.Model):
    ''' Cada una de las firmas digitales en uso o registradas '''
    entidad_registrante = models.ForeignKey(EntidadRegistranteFirma)

    # la firma puede ser de una persona O una organización
    persona = models.ForeignKey(
        Persona,
        null=True,
        blank=True,
        help_text='Si la forma corresponde a una persona, va aquí')
    organizacion = models.ForeignKey(
        Organizacion,
        null=True,
        blank=True,
        help_text='Si la forma es de una organización, va aquí')

    clave_publica = models.TextField(
        null=True,
        blank=True,
        help_text='Clave pública de la firma')

    FIRMA_ESTADO_DESCONOCIDO = 10
    FIRMA_ESTADO_PRUEBA = 20  # es una firma de prueba
    FIRMA_ESTADO_LEGAL = 30  # es una forma para uso legal
    estados = ((FIRMA_ESTADO_DESCONOCIDO, 'Desconocido'),
               (FIRMA_ESTADO_PRUEBA, 'Firma digital de prueba'),
               (FIRMA_ESTADO_LEGAL, 'Firma Digital Vigente'))
    estado = models.PositiveIntegerField(
        choices=estados, default=FIRMA_ESTADO_DESCONOCIDO)

    mas_info_url = models.URLField(
        null=True,
        blank=True,
        help_text='Si existe, link a ver la certificación de la firma online')

    @property
    def nombre_firmante(self):
        if self.persona is not None:
            return self.persona.nombrepublico
        elif self.organizacion is not None:
            return self.organizacion.nombre
        else:
            return 'No definido'

    def __str__(self):
        return '{} ({})'.format(
            self.nombre_firmante,
            self.entidad_registrante.nombre_entidad)


class SistemaFirma(models.Model):
    ''' Algoritmo y curvas usadas '''
    nombre = models.CharField(max_length=90, help_text='Nombre del sistema')
    descripcion = models.TextField(null=True, blank=True)
    url_de_referencia = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.nombre
