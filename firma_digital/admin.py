from django.contrib import admin
from .models import *


@admin.register(EntidadRegistranteFirma)
class EntidadRegistranteFirmaAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'organizacion', 'url']
    search_fields = ['nombre']
    # list_filter = []


@admin.register(FirmaDigital)
class FirmaDigitalAdmin(admin.ModelAdmin):
    list_display = ['entidad_registrante', 'persona', 'organizacion', 'estado']
    search_fields = ['entidad_registrante', 'persona', 'organizacion']
    list_filter = ['entidad_registrante']


@admin.register(SistemaFirma)
class SistemaFirmaAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'descripcion', 'url_de_referencia']
    search_fields = ['nombre', 'descripcion']