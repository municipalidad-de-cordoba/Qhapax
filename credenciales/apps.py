from django.apps import AppConfig


class CredencialesConfig(AppConfig):
    name = 'credenciales'
