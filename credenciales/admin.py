from django.contrib import admin
from .models import CredencialGeneral, HabilitacionCredencial, TipoCredencial


class HabilitacionCredencialAdmin(admin.ModelAdmin):
    list_display = [
        'credencial',
        'fecha_inicio_habilitacion',
        'fecha_fin_habilitacion']


class HabilitacionCredencialInline(admin.StackedInline):
    model = HabilitacionCredencial
    extra = 1


class TipoCredencialAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    list_display = ('id', 'nombre')


class CredencialGeneralAdmin(admin.ModelAdmin):
    def foto_thumb(self, instance):
        if instance.foto:
            return '<img src="%s" style="width: 80px;"/>' % (instance.foto.url)
        return ' <img src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACXBIWXMAAAAnAAAAJwEqCZFPAAAA B3RJTUUH4AcJFDE2B3C7PwAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUH AAAACklEQVQI12P4DwABAQEAG7buVgAAAABJRU5ErkJggg==" alt="blanco " style="width: 80px; height: 120px;" />'
    foto_thumb.short_description = 'foto thumb'
    foto_thumb.allow_tags = True

    search_fields = ['nombre', 'apellido', 'unique_id', 'codigo_interno']
    list_display = [
        'nombre',
        'apellido',
        'publicado',
        'tipo_credencial',
        'foto_thumb',
        'codigo_interno',
        'unique_id',
        'id']
    list_filter = ['tipo_credencial', 'cpc_cobertura', 'publicado']

    inlines = [HabilitacionCredencialInline]


admin.site.register(TipoCredencial, TipoCredencialAdmin)
admin.site.register(CredencialGeneral, CredencialGeneralAdmin)
admin.site.register(HabilitacionCredencial, HabilitacionCredencialAdmin)
