from django.db import models
from core.models import Persona
from django.core.urlresolvers import reverse
from CPC.models import Cpc


class TipoCredencial(models.Model):
    ''' tipos de credencial para areas o temas específicos '''
    nombre = models.CharField(max_length=90)
    descripcion_publica = models.TextField(
        null=True,
        blank=True,
        help_text='Detalles de lo que representa la acreditacion')

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Tipo de Credencial'
        verbose_name_plural = 'Tipos de Credencial'


class CredencialGeneral(Persona):
    ''' Adeministrador de personas que están acreditadas para alguna función
        Incluye la posibilidad de descargar listas, API para buscador en la web
        y QR para las credenciales que apunta a una ficha web aquí provistas '''

    tipo_credencial = models.ForeignKey(TipoCredencial)
    publicado = models.BooleanField(default=False)
    codigo_interno = models.CharField(
        max_length=30,
        null=True,
        blank=True,
        help_text='Si existirea, un código de identificación interno')
    cpc_cobertura = models.ForeignKey(
        Cpc,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text='Vacio para cobertura de todo el municipio')

    def ultima_habilitacion(self):
        ultima = self.habilitacioncredencial_set.all().order_by(
            '-fecha_fin_habilitacion')[:1]

        if len(ultima) == 0:  # si no tiene habilitacion
            return None
        else:
            return ultima[0]

    def get_absolute_url(self):
        return reverse('credenciales.ficha', kwargs={'pk': self.pk})

    class Meta:
        verbose_name = 'Credencial general'
        verbose_name_plural = 'Credenciales generales'


class HabilitacionCredencial(models.Model):
    ''' habilitación de la credencial '''
    credencial = models.ForeignKey(CredencialGeneral)
    fecha_inicio_habilitacion = models.DateField(null=True, blank=True)
    fecha_fin_habilitacion = models.DateField(null=True, blank=True)
    documento_habilitacion = models.FileField(
        upload_to='habilitacion-credencial/',
        null=True,
        blank=True,
        verbose_name='Documento de resolución, ordenanza o similar que habilita al acreditado')
    detalles_acreditacion_publica = models.TextField(
        null=True,
        blank=True,
        help_text='Observaciones públicas de esta habilitación si hubiere')
    observaciones_internas = models.TextField(
        null=True,
        blank=True,
        help_text='Detalles u observaciones internas de esta habilitación del acreditado')
