from django.conf.urls import url, include
from rest_framework import routers
from .views import CredencialesViewSet, TipoCredencialViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'tipo-credenciales',
    TipoCredencialViewSet,
    base_name='api.credenciales.tipos')
router.register(
    r'credenciales',
    CredencialesViewSet,
    base_name='api.credenciales.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
