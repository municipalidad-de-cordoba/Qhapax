from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from credenciales.models import TipoCredencial, CredencialGeneral
from versatileimagefield.serializers import VersatileImageFieldSerializer


class TipoCredencialSerializer(CachedSerializerMixin):
    nombre = serializers.CharField()

    class Meta:
        model = TipoCredencial
        fields = ['id', 'nombre']


class CredencialGeneralSerializer(CachedSerializerMixin):
    tipo_credencial = TipoCredencialSerializer(read_only=True)
    dni = serializers.SerializerMethodField()
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail_125", 'thumbnail__125x125'),
                                          ("thumbnail_500", 'thumbnail__500x500')])

    def get_dni(self, obj):
        nombre = "No" if obj.tipo_id is None else obj.tipo_id.nombre
        unique_id = "No" if obj.unique_id is None else obj.unique_id
        return '{} {}'.format(nombre, unique_id)

    class Meta:
        model = CredencialGeneral
        fields = [
            'nombre',
            'apellido',
            'dni',
            'unique_id',
            'foto',
            'tipo_credencial',
            'codigo_interno']


cache_registry.register(TipoCredencialSerializer)
cache_registry.register(CredencialGeneralSerializer)
