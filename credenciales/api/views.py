from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import CredencialGeneralSerializer, TipoCredencialSerializer
from api.pagination import DefaultPagination
from django.db.models import Q
from credenciales.models import CredencialGeneral, TipoCredencial


class TipoCredencialViewSet(viewsets.ModelViewSet):
    """
    Lista de tipos de credenciales.
    """
    serializer_class = TipoCredencialSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = TipoCredencial.objects.all()
        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class CredencialesViewSet(viewsets.ModelViewSet):
    """
    Lista de credenciales.
    Se puede buscar por tipo con el parametro:
     - tipo: (str) Por ejemplo: "?tipo=periodista"
     - tipo_id: (int) por ejemplo ?tipo_id=5
    Se puede filtrar con el parametro "q" por nombre, apellido, dni o codigo interno
    Ej: API/?q=31125478, API/?q=Rodriguez, etc
    """
    serializer_class = CredencialGeneralSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = CredencialGeneral.objects.filter(publicado=True)

        tipo = self.request.query_params.get('tipo', None)
        if tipo is not None:
            queryset = queryset.filter(
                Q(tipo_credencial__nombre__icontains=tipo))

        tipo_id = self.request.query_params.get('tipo_id', None)
        if tipo_id is not None:
            queryset = queryset.filter(Q(tipo_credencial__id=int(tipo_id)))

        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(
                Q(codigo_interno__icontains=q) |
                Q(nombre__icontains=q) |
                Q(apellido__icontains=q) |
                Q(unique_id__icontains=q)
            )

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
