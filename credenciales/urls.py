from django.conf.urls import url

from . import views

urlpatterns = [url(r'^lista-de-acreditados-(?P<tipo>[0-9]+).(?P<filetype>csv|xls)$',
                   views.planilla_credenciales,
                   name='credenciales.lista'),
               url(r'^credencial-(?P<pk>[0-9]+).html$',
                   views.ficha_acreditado_credenciales,
                   name='credenciales.ficha'),
               url(r'^lista-completa-de-acreditados.(?P<filetype>csv|xls)$',
                   views.planilla_credenciales_total,
                   name='credenciales.lista.completa'),
               ]
