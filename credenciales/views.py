from credenciales.models import CredencialGeneral, TipoCredencial
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.views.decorators.cache import cache_page
import django_excel as excel


@cache_page(60 * 60 * 12)  # 12 h
def ficha_acreditado_credenciales(request, pk):

    credencial = get_object_or_404(CredencialGeneral, pk=pk)
    habilitacion = credencial.ultima_habilitacion()
    base_url = request.website
    url = '{}{}'.format(base_url, credencial.get_absolute_url())
    qr_image = credencial.get_image_qr(url=url)
    qr_download = credencial.get_download_link_qr(url=url)
    context = {'credencial': credencial, 'qr_image': qr_image,
               'qr_download': qr_download, 'habilitacion': habilitacion}
    url = 'credenciales/ficha.html'
    return render(request, url, context)


@cache_page(60 * 60 * 12)  # 12 h
def planilla_credenciales(request, tipo, filetype):
    '''
    Planilla de acreditados (para cualer tipo actual o a crearse a futuro)
    http://localhost:8000/credenciales/lista-de-acreditados-N.xls (N=Tipo)
    '''
    tipo_credencial = get_object_or_404(TipoCredencial, pk=tipo)
    credenciales = CredencialGeneral.objects.filter(
        publicado=True, tipo_credencial=tipo_credencial)
    csv_list = []
    csv_list.append(['Codigo Interno',
                     'Apellido',
                     'Nombre',
                     'DNI',
                     'Genero',
                     'Edad',
                     'url',
                     'foto',
                     'acreditación',
                     'tipo',
                     'inicio habilitacion',
                     'fin habilitacion'])

    for credencial in credenciales:
        dni = '{} {}'.format(credencial.tipo_id, credencial.unique_id)
        url = "{}{}".format(request.website, credencial.get_absolute_url())
        try:
            foto = "{}{}".format(
                request.website, credencial.foto.thumbnail['800x800'].url)
        except (ValueError, FileNotFoundError):
            foto = ""

        habilitacion = credencial.ultima_habilitacion()
        hab_detalles = 'Sin habilitacion' if habilitacion is None else habilitacion.detalles_acreditacion_publica
        hab_inicio = 'Sin habilitacion' if habilitacion is None else habilitacion.fecha_inicio_habilitacion
        hab_fin = 'Sin habilitacion' if habilitacion is None else habilitacion.fecha_fin_habilitacion

        csv_list.append([
            credencial.codigo_interno,
            credencial.apellido,
            credencial.nombre,
            dni,
            credencial.get_genero_display(),
            credencial.edad,
            url,
            foto,
            hab_detalles,
            credencial.tipo_credencial.nombre,
            hab_inicio,
            hab_fin
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)


@cache_page(60 * 60 * 12)  # 12 h
def planilla_credenciales_total(request, filetype):
    '''
    Planilla de todos los acreditados
    http://localhost:8000/credenciales/lista-de-acreditados.xls
    '''

    credenciales = CredencialGeneral.objects.filter(publicado=True)
    csv_list = []
    csv_list.append(['Codigo Interno',
                     'Apellido',
                     'Nombre',
                     'DNI',
                     'Genero',
                     'Edad',
                     'url',
                     'foto',
                     'acreditación',
                     'tipo',
                     'inicio habilitacion',
                     'fin habilitacion'])

    for credencial in credenciales:
        dni = '{} {}'.format(credencial.tipo_id, credencial.unique_id)
        url = "{}{}".format(request.website, credencial.get_absolute_url())
        try:
            foto = "{}{}".format(
                request.website, credencial.foto.thumbnail['800x800'].url)
        except (ValueError, FileNotFoundError):
            foto = ""

        habilitacion = credencial.ultima_habilitacion()
        hab_detalles = 'Sin habilitacion' if habilitacion is None else habilitacion.detalles_acreditacion_publica
        hab_inicio = 'Sin habilitacion' if habilitacion is None else habilitacion.fecha_inicio_habilitacion
        hab_fin = 'Sin habilitacion' if habilitacion is None else habilitacion.fecha_fin_habilitacion

        csv_list.append([
            credencial.codigo_interno,
            credencial.apellido,
            credencial.nombre,
            dni,
            credencial.get_genero_display(),
            credencial.edad,
            url,
            foto,
            hab_detalles,
            credencial.tipo_credencial.nombre,
            hab_inicio,
            hab_fin
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
