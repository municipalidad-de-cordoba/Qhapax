# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-09-19 16:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('CPC', '0001_initial'),
        ('alumbrado', '0002_auto_20190918_1507'),
    ]

    operations = [
        migrations.AddField(
            model_name='incidente',
            name='cpc',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='CPC.Cpc'),
        ),
    ]
