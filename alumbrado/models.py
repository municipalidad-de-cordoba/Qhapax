from django.db import models
from django.contrib.gis.db import models as models_geo
from barrios.models import Barrio
from CPC.models import Cpc
from organizaciones.models import Organizacion
from versatileimagefield.fields import VersatileImageField, PPOIField


class TipoIncidente(models.Model):
    """
    Tipo de trabajo realizado
    """
    nombre = models.CharField(max_length=90)

    def __str__(self):
        return self.nombre


class Incidente(models.Model):
    """
    Reporte de trabajo hecho en luminarias de la ciudad.
    """
    numero = models.PositiveIntegerField(null=True, blank=True)
    tipo = models.ForeignKey(
        TipoIncidente,
        null=True,
        on_delete=models.SET_NULL)
    # campo generico espacial, puede contener puntos, líneas o polígonos
    ubicacion = models_geo.PointField(null=True)
    barrio = models.ForeignKey(Barrio, blank=True, null=True)
    cpc = models.ForeignKey(Cpc, blank=True, null=True)
    observaciones = models.TextField(null=True, blank=True)
    fecha_alta = models.DateTimeField(null=True, blank=True)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    direccion = models.CharField(max_length=150, null=True, blank=True)
    organizacion = models.ForeignKey(Organizacion, null=True, blank=True)
    area_servicio = models.CharField(max_length=150, null=True, blank=True)

    def __str__(self):
        return 'Id: {} - Nro incidente: {}'.format(self.id, self.numero)


class FotoDeAlumbrado(models.Model):
    """
    Fotos de trabajos de luminarias
    """
    incidente = models.ForeignKey(Incidente, related_name='adjuntos')

    id_externo = models.PositiveIntegerField(default=0)
    nombre = models.CharField(max_length=90, null=True, blank=True)
    url = models.CharField(max_length=320, null=True, blank=True)

    foto = VersatileImageField(
        upload_to='imagenes/alumbrado',
        ppoi_field='foto_ppoi',
        null=True,
        blank=True,
        max_length=200
    )
    foto_ppoi = PPOIField('Image PPOI')

    def __str__(self):
        return "Foto {} {}".format(self.id, self.id_externo)

    class Meta:
        verbose_name = 'Foto de Alumbrado'
        verbose_name_plural = 'Fotos de Alumbrado'
