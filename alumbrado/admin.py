from django.contrib import admin
from .models import FotoDeAlumbrado, Incidente, TipoIncidente
from core.admin import QhapaxOSMGeoAdmin


class FotoDeAlumbradoInLine(admin.StackedInline):
    model = FotoDeAlumbrado
    extra = 1


class IncidenteAdmin(QhapaxOSMGeoAdmin):
    list_display = [
        'id',
        'numero',
        'tipo',
        'direccion',
        'cpc',
        'observaciones']
    search_fields = ['numero', 'id']
    list_filter = ['tipo', 'organizacion', 'barrio', 'cpc']
    inlines = [FotoDeAlumbradoInLine]

    def get_queryset(self, request):
        queryset = Incidente.objects.all().order_by('-id')
        return queryset


class FotoDeAlumbradoAdmin(QhapaxOSMGeoAdmin):

    def file_size(self, obj):
        if obj.foto and hasattr(obj.foto, 'url'):
            sz = obj.foto.file.size / (1024 * 1024)
            return '{:.2f} MB'.format(sz)
        else:
            return "No hay foto"
    # file_size.admin_order_field = 'foto__file__size'

    def foto_thumb(self, obj):
        if obj.foto:
            try:
                th80 = obj.foto.thumbnail['80x80'].url
            except Exception as e:
                pass
            else:
                return '<img src="{}" style="width: 80px;"/>'.format(
                    obj.foto.thumbnail['80x80'].url)

        return ' <img src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACXBIWXMAAAAnAAAAJwEqCZFPAAAA B3RJTUUH4AcJFDE2B3C7PwAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUH AAAACklEQVQI12P4DwABAQEAG7buVgAAAABJRU5ErkJggg==" alt="blanco " style="width: 80px; height: 120px;" />'

    foto_thumb.short_description = 'thumb'
    foto_thumb.allow_tags = True

    list_display = ['id', 'incidente', 'id_externo', 'nombre', 'foto_thumb',
                    'url', 'file_size']
    search_fields = ['id', 'incidente', 'nombre', 'url']
    list_filter = ['incidente__tipo', ]


admin.site.register(Incidente, IncidenteAdmin)
admin.site.register(FotoDeAlumbrado, FotoDeAlumbradoAdmin)
admin.site.register(TipoIncidente)
