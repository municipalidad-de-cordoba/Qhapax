from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from versatileimagefield.serializers import VersatileImageFieldSerializer
from alumbrado.models import Incidente, FotoDeAlumbrado


class FotoDeAlumbradoSerializer(CachedSerializerMixin):
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail_125", 'thumbnail__125x125'),
                                          ("thumbnail_500", 'thumbnail__500x500')])

    class Meta:
        model = FotoDeAlumbrado
        fields = ('id', 'foto', 'url')


class IncidenteEnAlumbradoSerializer(
        GeoFeatureModelSerializer,
        CachedSerializerMixin):

    fecha = serializers.CharField(source='fecha_actualizacion')
    adjuntos = FotoDeAlumbradoSerializer(many=True, read_only=True)
    barrio = serializers.CharField(source='barrio.nombre')
    id_barrio = serializers.CharField(source='barrio.id')
    tipo = serializers.CharField(source='tipo.nombre')

    class Meta:
        model = Incidente
        geo_field = 'ubicacion'
        fields = ('id', 'numero', 'tipo', 'barrio', 'id_barrio',
                  'observaciones', 'fecha', 'direccion', 'adjuntos')


# Registro los serializadores en la cache de DRF
cache_registry.register(IncidenteEnAlumbradoSerializer)
cache_registry.register(FotoDeAlumbradoSerializer)
