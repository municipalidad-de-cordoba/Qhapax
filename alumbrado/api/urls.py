from django.conf.urls import url, include
from rest_framework import routers
from .views import (BarriosAlumbradoViewSet,
                    BarriosGeoAlumbradoViewSet,
                    IncidenteEnAlumbradoViewSet)


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    'incidentes',
    IncidenteEnAlumbradoViewSet,
    base_name='incidentes.api.lista')
router.register(
    'barrios-geo-incidentes',
    BarriosGeoAlumbradoViewSet,
    base_name='barrios-geo-incidentes.api.lista')
router.register(
    'barrios-incidentes',
    BarriosAlumbradoViewSet,
    base_name='barrios-incidentes.api.lista')


urlpatterns = [
    url(r'^', include(router.urls)),
]
