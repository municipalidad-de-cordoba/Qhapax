from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import IncidenteEnAlumbradoSerializer
from api.pagination import DefaultPagination
from alumbrado.models import Incidente
from barrios.models import Barrio
from barrios.api.serializers import BarrioGeoSerializer, BarrioSinGeoSerializer
from rest_framework_gis.filters import InBBoxFilter
from datetime import timedelta, datetime
from django.http import Http404


class IncidenteEnAlumbradoViewSet(viewsets.ModelViewSet):
    """
    Lista de árboles geolocalizadas con sus datos
    Puede filtrarse por:
     - id_barrio: id del barrio, pueden ser varios separados por coma.
        Api de barrios: https://gobiernoabierto.cordoba.gob.ar/api/v2/alumbrado/barrios-incidentes/
        Ejemplo: id_barrio=10,11
    - id_cpc : id del cpc, pueden ser varios separados por coma.
        Api de cpc: https://gobiernoabierto.cordoba.gob.ar/api/v2/cpc/cpc-geo/
        Ejemplo: id_cpc=2,5

      Por fechas:
     - dias(representado por un número entero): retorna los trazados que tienen
       fotos registradas en los últimos x días.
     - fecha_exacta(formato dd-mm-AAAA): retorna los trazados que tienen fotos
       registradas en la fecha ingresada.
     - desde(formato dd-mm-AAAA): retorna los trazados que tienen fotos
       registradas desde la fecha ingresada hasta la actualidad.
     - hasta(formato dd-mm-AAAA): retorna los trazados que tienen fotos
       registradas hasta la fecha ingresada.
     - Los filtros desde y hasta se pueden combinar para formar un rango de
       fechas en el cual filtrar los trazados que contengan fotos en dicho
       rango.
    """
    serializer_class = IncidenteEnAlumbradoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    bbox_filter_field = 'ubicacion'
    filter_backends = (InBBoxFilter, )
    # bbox_filter_include_overlapping = True # Optional

    def get_queryset(self):
        queryset = Incidente.objects.filter(barrio__isnull=False)

        id_barrio = self.request.query_params.get('id_barrio', None)
        if id_barrio is not None:
            ids_barrio = id_barrio.split(',')
            queryset = queryset.filter(barrio__id__in=ids_barrio)

        id_cpc = self.request.query_params.get('id_cpc', None)
        if id_cpc is not None:
            ids_cpc = id_cpc.split(',')
            queryset = queryset.filter(cpc__id__in=ids_cpc)

        desde = self.request.query_params.get('desde', None)
        hasta = self.request.query_params.get('hasta', None)
        if desde is not None or hasta is not None:
            # filtros desde y hasta especificados
            if desde is not None and hasta is not None:
                desde = datetime.strptime(desde, "%d-%m-%Y").date()
                hasta = datetime.strptime(hasta, "%d-%m-%Y").date()

                if desde > hasta:
                    raise Http404("Fecha mal especificada.")
                else:
                    queryset = queryset.filter(
                        fecha_actualizacion__range=[desde, hasta])

            # el filtro desde está especificado
            elif desde is not None:
                desde = datetime.strptime(desde, "%d-%m-%Y").date()
                queryset = queryset.filter(fecha_actualizacion__gte=desde)

            # el filtro hasta está especificado
            elif hasta is not None:
                hasta = datetime.strptime(hasta, "%d-%m-%Y").date()
                queryset = queryset.filter(fecha_actualizacion__lte=hasta)

        fecha_exacta = self.request.query_params.get('fecha_exacta', None)
        if fecha_exacta is not None:
            fecha_exacta = datetime.strptime(fecha_exacta, "%d-%m-%Y").date()
            queryset = queryset.filter(
                fecha_actualizacion__contains=fecha_exacta)

        dias = self.request.query_params.get('dias', None)
        if dias is not None:
            fecha_ahora = datetime.now()
            fecha_desde = fecha_ahora - timedelta(days=int(dias))
            queryset = queryset.filter(fecha_actualizacion__gte=fecha_desde)

        # tipo_orden = self.request.query_params.get('tipo_orden', None)
        # if tipo_orden is not None:
        #     queryset = queryset.filter(tipo_orden_nombre=tipo_orden)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class BarriosGeoAlumbradoViewSet(viewsets.ModelViewSet):
    """
    Lista de barrios que tienen trabajos de alumbrado
    """
    serializer_class = BarrioGeoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    # pagination_class = DefaultPagination
    # bbox_filter_field = 'ubicacion'
    # filter_backends = (InBBoxFilter, )
    # bbox_filter_include_overlapping = True # Optional

    def get_queryset(self):
        # queryset = Incidente.objects.exclude(ubicacion__isnull=True)
        queryset = Barrio.objects.filter(incidente__isnull=False).distinct()

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class BarriosAlumbradoViewSet(viewsets.ModelViewSet):
    """
    Lista de barrios que tienen trabajos de alumbrado
    """
    serializer_class = BarrioSinGeoSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    # pagination_class = DefaultPagination
    # bbox_filter_field = 'ubicacion'
    # filter_backends = (InBBoxFilter, )
    # bbox_filter_include_overlapping = True # Optional

    def get_queryset(self):
        # queryset = Incidente.objects.exclude(ubicacion__isnull=True)
        queryset = Barrio.objects.filter(incidente__isnull=False).distinct()

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
