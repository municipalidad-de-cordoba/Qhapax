#!/usr/bin/python
"""
Importar nuevas obras de terceros(espacios verdes)
"""
from django.core.management.base import BaseCommand
from django.db import transaction
from datetime import datetime
import json
import sys
from alumbrado.settings import URL_API_ALUMBRADO, TOKEN_ACCESO
from alumbrado.models import FotoDeAlumbrado, Incidente, TipoIncidente
from barrios.models import Barrio
from CPC.models import Cpc
from django.contrib.gis.geos import Point
import requests
from django.core.files import File
import tempfile
from PIL import Image
from organizaciones.models import Organizacion


class Command(BaseCommand):
    help = """Comando para Importar obras de alumbrado público"""

    def add_arguments(self, parser):
        parser.add_argument(
            '--fecha',
            type=str,
            help=('fecha desde la cual se importarán nuevas obras y cambios a '
                  'las existentes. Formato dd/mm/aaaa'),
            default='01/01/2016')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando importación de datos '
                                             'de obras de alumbrado'))

        url_base = URL_API_ALUMBRADO
        fecha = options['fecha']
        url = '{}?fecha={}'.format(url_base, fecha)

        self.stdout.write(self.style.SUCCESS(
            'Buscando datos en {}'.format(url)))

        session = requests.Session()
        headers = {'Authorization': TOKEN_ACCESO}
        basedata = session.get(url, headers=headers).content  # es tipo byte
        basedata = basedata.decode("utf-8")
        # basedata = basedata.decode("iso-8859-1")

        # barrios = Barrio.objects.values_list('nombre', flat=True)
        poligonos_barrios = Barrio.objects.values_list('poligono', flat=True)

        # cpcs = Cpc.objects.values_list('nombre', flat=True)
        poligonos_cpcs = Cpc.objects.values_list('zona', flat=True)

        try:
            jsondata = json.loads(basedata)
        except Exception as e:
            self.stdout.write(self.style.ERROR('JSON Error: {}'.format(url)))
            self.stdout.write(self.style.ERROR(e))
            sys.exit(1)

        obras = jsondata['result']
        self.stdout.write(self.style.SUCCESS(
            'Se obtuvieron datos de {} obras'.format(len(obras))))

        organizaciones_nuevas = 0
        organizaciones_repetidas = 0
        incidentes_nuevos = 0
        incidentes_repetidos = 0

        for obra in obras:
            # self.stdout.write(self.style.SUCCESS('OBRA: {}'.format(obra)))
            """
            {
                'tipoIncidente': 'Luminaria no funciona CONECTAR',
                'contratista': 'Conectar S.R.L.',
                'nroIncidente': 465506,
                'longitud': '-64.1802958',
                'fechaActualizacion': '27/05/2019 15:02',
                'fechaAlta': '27/05/2019 15:02', // DD/MM/AAAA HH:mm
                'direccion': 'Del Bailecito 733',
                'latitud': '-31.3093787',
                'areaServicio': '5963 - Punto de Luz',
                'barrio': 'Villa Alicia Risler',
                'observaciones': ''
                "adjuntos": [
                    {
                        "id": 888,
                        "nombreAdjunto": "string",
                        "url": "string"
                        // ...
                    }
            }
            """

            longitud = float(obra['longitud'])
            latitud = float(obra['latitud'])
            point = Point(longitud, latitud)

            # barrio
            # Hay barrios que vienen como NULL!
            # if obra['barrio'] is not None:
            #     match = difflib.get_close_matches(obra['barrio'].upper(), possibilities=barrios, n=1)
            #     if match == []:
            #         barrio = None
            #         for poligono in poligonos_barrios:
            #             if poligono is not None and poligono.contains(point):
            #                 barrio = Barrio.objects.get(poligono=poligono)
            #                 break
            #     else:
            #         barrio = Barrio.objects.get(nombre=match[0])
            # else:
            barrio = None
            for poligono in poligonos_barrios:
                if poligono is not None and poligono.contains(point):
                    barrio = Barrio.objects.get(poligono=poligono)
                    break

            # detectamos cpc al cual pertenece
            cpc = None
            for poligono in poligonos_cpcs:
                if poligono is not None and poligono.contains(point):
                    cpc = Cpc.objects.get(zona=poligono)
                    break

            incidente, inc_created = Incidente.objects.get_or_create(
                numero=obra['nroIncidente'])

            if inc_created:
                incidentes_nuevos += 1
            else:
                incidentes_repetidos += 1

            incidente.barrio = barrio
            incidente.cpc = cpc

            # tipo de trabajo hecho
            tipo, t_created = TipoIncidente.objects.get_or_create(
                nombre=obra['tipoIncidente'])
            incidente.tipo = tipo

            incidente.ubicacion = point

            # fotos de los trabajos
            adjuntos = obra['adjuntos']
            cantidad_de_fotos = len(adjuntos) if adjuntos is not None else 0
            self.stdout.write(
                self.style.SUCCESS(
                    'Fotos: {}'.format(cantidad_de_fotos)))

            if cantidad_de_fotos > 0:
                for adjunto in adjuntos:
                    foto, created = FotoDeAlumbrado.objects.get_or_create(
                        incidente=incidente, id_externo=adjunto['id'])

                    foto.url = adjunto['url']
                    foto.nombre = adjunto['nombreAdjunto']

                    # importo las fotos solo si es la primera vez
                    if created:
                        self.stdout.write(
                            self.style.SUCCESS(
                                'Nueva Foto: {} Trayendo imagen {}'.format(
                                    adjunto, foto.url)))
                        url_foto = adjunto['url']
                        contenido = session.get(
                            url_foto).content  # es tipo byte

                        tf = tempfile.NamedTemporaryFile()
                        tf.write(contenido)

                        img = File(tf)
                        try:
                            # controlamos que el archivo sea una imagen válida
                            img = Image.open(img)
                            if img.size != 0:
                                foto.foto.save(
                                    str(foto.url.split('/')[-1]), File(tf))
                        except Exception as e:
                            err = 'ERROR con el formato de la FOTO:{}'.format(
                                foto.url)
                            self.stdout.write(self.style.ERROR(err))
                            foto.delete()
                            continue

                            foto.save()
                    else:
                        self.stdout.write(
                            self.style.SUCCESS(
                                'Omitiendo descargar imagen {}'.format(
                                    foto.url)))

            # Datos Contratista
            organizacion_str = obra['contratista'].strip()
            if organizacion_str == '':
                organizacion_str = 'Desconocido'

            # current_tz = timezone.get_current_timezone()
            incidente.fecha_actualizacion = datetime.strptime(
                obra['fechaActualizacion'].strip(), "%d/%m/%Y %H:%M")
            # incidente.fecha = current_tz.localize(incidente.fecha_actualizacion)

            incidente.fecha_alta = datetime.strptime(
                obra['fechaAlta'].strip(), "%d/%m/%Y %H:%M")

            organizacion = None
            try:
                organizacion, created = Organizacion.objects.get_or_create(
                    nombre=organizacion_str)
                if created:
                    organizaciones_nuevas += 1
                else:
                    organizaciones_repetidas += 1
            except Organizacion.MultipleObjectsReturned:
                # Fix para cuando la organizacion está duplicada
                organizaciones = Organizacion.objects.filter(
                    nombre=organizacion_str)

                max_obras_relacionadas = 0
                # por cada organización, obtengo las obras que tiene a su cargo
                for org in organizaciones:
                    cant = len(org.obrapublica_set.all())
                    if cant > max_obras_relacionadas:
                        organizacion = org
                        max_obras_relacionadas = cant
                # elijo la organizacion que tiene mas obras
                organizaciones_repetidas += 1

            incidente.organizacion = organizacion

            incidente.area_servicio = obra['areaServicio']

            incidente.direccion = obra['direccion']

            incidente.observaciones = obra['observaciones']

            incidente.save()

        self.stdout.write(self.style.SUCCESS('FIN.'))
        self.stdout.write(
            self.style.SUCCESS(
                'Incidentes nuevos: {}.'.format(incidentes_nuevos)))
        self.stdout.write(
            self.style.SUCCESS(
                'Incidentes repetidos: {}.'.format(incidentes_repetidos)))
        self.stdout.write(
            self.style.SUCCESS(
                'Organizaciones nuevas: {}.'.format(organizaciones_nuevas)))
        self.stdout.write(
            self.style.SUCCESS(
                'Organizaciones repetidas: {}.'.format(organizaciones_repetidas)))
