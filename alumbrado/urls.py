from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^datos-descargables.(?P<filetype>csv|xls)$',
        views.DatosAlubradoDescargable, name='alumbrado.datos.descargables'),
]
