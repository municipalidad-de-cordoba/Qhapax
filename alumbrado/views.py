from alumbrado.models import Incidente
from django.views.decorators.cache import cache_page
import django_excel as excel


@cache_page(60 * 60)  # 1 hora
def DatosAlubradoDescargable(request, filetype):
    """
    Datos descargables de trabajos realizados en alumbrado público
    """

    datos = Incidente.objects.all()

    csv_list = []
    csv_list.append(['ID', 'TIPO', 'BARRIO', 'FECHA', 'DIRECCION',
                     'EMPRESA', 'AREA SERVICIO', 'UBICACION'])

    for dato in datos:
        barrio = dato.barrio.nombre if dato.barrio is not None else ''
        csv_list.append([
            dato.id,
            dato.tipo.nombre or '',
            barrio,
            dato.fecha_actualizacion.strftime('%d/%m/%Y'),
            dato.direccion or '',
            dato.organizacion.nombre,
            dato.area_servicio,
            dato.ubicacion.ewkt
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
