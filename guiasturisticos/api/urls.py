from django.conf.urls import url, include
from rest_framework import routers
from .views import GuiasTuristicosViewSet, IdiomasViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(
    r'guias-turisticos',
    GuiasTuristicosViewSet,
    base_name='guias-turisticos.api.lista')
router.register(
    r'idiomas',
    IdiomasViewSet,
    base_name='guias-turisticos.idioma.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
