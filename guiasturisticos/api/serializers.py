from versatileimagefield.serializers import VersatileImageFieldSerializer
from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from guiasturisticos.models import GuiaTuristico, NivelIdiomaGuia
from core.models import Idioma


class IdiomaSerializer(CachedSerializerMixin):

    class Meta:
        model = Idioma
        fields = ['id', 'nombre']


class NivelIdiomaGuiaSerializer(CachedSerializerMixin):
    nivel = serializers.CharField(source='nivel.nombre')
    idioma = serializers.CharField(source='idioma.nombre')

    class Meta:
        model = NivelIdiomaGuia
        fields = ['idioma', 'nivel']


class GuiaTuristicoSerializer(CachedSerializerMixin):
    vias_comunicacion = serializers.StringRelatedField(
        many=True, source='comunicacionpersona_set')
    idiomas = NivelIdiomaGuiaSerializer(many=True)
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail", 'thumbnail__125x125')])

    class Meta:
        model = GuiaTuristico
        fields = [
            'id',
            'nombre',
            'apellido',
            'idiomas',
            'vias_comunicacion',
            'foto']


# Registro los serializadores en la cache de DRF
cache_registry.register(IdiomaSerializer)
cache_registry.register(NivelIdiomaGuiaSerializer)
cache_registry.register(GuiaTuristicoSerializer)
