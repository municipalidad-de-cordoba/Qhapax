from django.contrib import admin
from core.models import ComunicacionPersona
from .models import (GuiaTuristico, NivelIdioma, TituloGuia,
                     HabilitacionGuiaTuristico, NivelIdiomaGuia)
from .forms import TituloGuiaForm, GuiaTuristicoForm


class ComunicacionGuiaAdmin(admin.StackedInline):
    model = ComunicacionPersona
    extra = 1


class TituloGuiaInline(admin.StackedInline):
    model = TituloGuia
    form = TituloGuiaForm
    extra = 1


class HabilitacionGuiaTuristicoInLine(admin.StackedInline):
    model = HabilitacionGuiaTuristico
    extra = 1


class GuiaTuristicoAdmin(admin.ModelAdmin):
    form = GuiaTuristicoForm

    def foto_thumb(self, instance):
        if instance.foto:
            return '<img src="%s" style="width: 80px;"/>' % (instance.foto.url)
        return ' <img src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACXBIWXMAAAAnAAAAJwEqCZFPAAAA B3RJTUUH4AcJFDE2B3C7PwAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUH AAAACklEQVQI12P4DwABAQEAG7buVgAAAABJRU5ErkJggg==" alt="blanco " style="width: 80px; height: 120px;" />'
    foto_thumb.short_description = 'foto thumb'
    foto_thumb.allow_tags = True

    def idiomas_hablados(self, instance):
        ret = ''
        for idioma in instance.idiomas.all():
            ret = '{} {}-{}'.format(ret,
                                    idioma.idioma.nombre,
                                    idioma.nivel.nombre)
        return ret.strip()

    list_display = (
        'nombre',
        'apellido',
        'idiomas_hablados',
        'fecha_nacimiento',
        'foto_thumb')
    search_fields = ['nombre', 'apellido', 'unique_id']
    # list_filter = []
    inlines = [
        ComunicacionGuiaAdmin,
        TituloGuiaInline,
        HabilitacionGuiaTuristicoInLine]


class NivelIdiomaAdmin(admin.ModelAdmin):
    list_display = ('nombre', )
    search_fields = ['nombre']
    list_filter = []


class NivelIdiomaGuiaAdmin(admin.ModelAdmin):
    list_display = ('nivel', 'idioma')
    list_filter = ['idioma']


class TituloGuiaAdmin(admin.ModelAdmin):
    form = TituloGuiaForm
    list_display = (
        'guia',
        'titulo',
        'organizacion_cecrtificante',
        'fecha',
        'observaciones_internas')
    search_fields = ['guia__nombre', 'guia__apellido', 'guia__unique_id']
    list_filter = ['titulo']

    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


class HabilitacionGuiaTuristicoAdmin(admin.ModelAdmin):
    list_display = ('guia', 'fecha_inicio', 'fecha_fin')
    search_fields = ['guia__nombre', 'guia__apellido', 'guia__unique_id']
    list_filter = ['fecha_inicio', 'fecha_fin']


admin.site.register(GuiaTuristico, GuiaTuristicoAdmin)
admin.site.register(NivelIdioma, NivelIdiomaAdmin)
admin.site.register(NivelIdiomaGuia, NivelIdiomaGuiaAdmin)
admin.site.register(TituloGuia, TituloGuiaAdmin)
admin.site.register(HabilitacionGuiaTuristico, HabilitacionGuiaTuristicoAdmin)
