from django import forms
from organizaciones.forms import OrganizacionWidget
from .models import TituloGuia, GuiaTuristico
from core.models import ComunicacionPersona


class TituloGuiaForm(forms.ModelForm):
    class Meta:
        model = TituloGuia
        fields = ['guia', 'titulo', 'organizacion_cecrtificante',
                  'fecha', 'titulo_archivo', 'observaciones_internas']
        widgets = {'organizacion_cecrtificante': OrganizacionWidget}


class GuiaTuristicoForm(forms.ModelForm):
    class Meta:
        model = GuiaTuristico
        fields = '__all__'
        widgets = {'idiomas': forms.CheckboxSelectMultiple}


class ComunicacioGuiaTuristicoForm(forms.ModelForm):
    """
    Formulario para crear comunicacion de guias turisticos
    """
    guias = forms.ModelMultipleChoiceField(
        queryset=GuiaTuristico.objects.all())

    class Meta:
        model = ComunicacionPersona
        fields = ("guias", 'tipo', 'valor', 'descripcion', 'privado')
