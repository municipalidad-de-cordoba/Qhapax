from django.shortcuts import render
from guiasturisticos.models import GuiaTuristico
import django_excel as excel
from django.views.decorators.cache import cache_page
from django.shortcuts import get_object_or_404
from core.models import ComunicacionPersona


@cache_page(60 * 60 * 12)  # 12 h
def ficha_guia_turistico(request, nro_registro):
    '''
    Ficha de un guía turístico
    http://localhost:8000/guias-turisticos/guia-1.html
    '''
    guia = get_object_or_404(GuiaTuristico, numero_de_registro=nro_registro)
    comunicacion = ComunicacionPersona.objects.filter(
        objeto=guia, privado=False)
    base_url = request.website
    url = '{}{}'.format(base_url, guia.get_absolute_url())
    qr_image = guia.get_image_qr(url=url)
    qr_download = guia.get_download_link_qr(url=url)
    context = {'guia': guia, 'qr_image': qr_image, 'qr_download': qr_download,
               'comunicaciones': comunicacion}
    url = 'guiasturisticos/ficha.html'
    return render(request, url, context)


@cache_page(60 * 60 * 12)  # 12 h
def planilla_guias_turisticos(request, filetype):
    '''
    Planilla de guías turísticos
    http://localhost:8000/guias-turisticos/lista-de-guias.xls
    '''

    guias = GuiaTuristico.objects.filter(
        publicado=True).order_by(
        'apellido', 'nombre')
    csv_list = []
    csv_list.append(['registro',
                     'Apellido',
                     'Nombre',
                     'DNI',
                     'Genero',
                     'Edad',
                     'contacto',
                     'url',
                     'url_foto_thumbnail',
                     'url_foto_original',
                     'Idiomas'])

    for guia in guias:
        dni = '{} {}'.format(guia.tipo_id, guia.unique_id)
        url = "{}{}".format(request.website, guia.get_absolute_url())

        comunicaciones = ComunicacionPersona.objects.filter(
            objeto=guia, privado=False)
        contacto = ''
        for comunicacion in comunicaciones:
            contacto += '{}: {} '.format(comunicacion.tipo, comunicacion.valor)

        idiomas = ''
        for idioma in guia.idiomas.all():
            idiomas = '{} {} ({}) '.format(
                idiomas, idioma.idioma.nombre, idioma.nivel.nombre)

        try:
            foto = "{}{}".format(request.website, guia.foto.url)
            foto_thumb = "{}{}".format(
                request.website, guia.foto.thumbnail['600x600'].url)
        except (ValueError, FileNotFoundError):
            foto = ""
            foto_thumb = ""

        csv_list.append([
            guia.numero_de_registro,
            guia.apellido,
            guia.nombre,
            dni,
            guia.get_genero_display(),
            guia.edad,
            contacto,
            url,
            foto_thumb,
            foto,
            idiomas
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
