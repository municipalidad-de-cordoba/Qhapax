# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-09-15 15:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('guiasturisticos', '0002_auto_20170908_1922'),
    ]

    operations = [
        migrations.AddField(
            model_name='guiaturistico',
            name='numero_de_registro',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
