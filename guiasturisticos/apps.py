from django.apps import AppConfig


class GuiasturisticosConfig(AppConfig):
    name = 'guiasturisticos'
