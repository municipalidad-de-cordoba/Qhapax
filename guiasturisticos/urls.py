from django.conf.urls import url

from . import views

urlpatterns = [url(r'^lista-de-guias.(?P<filetype>csv|xls)$',
                   views.planilla_guias_turisticos,
                   name='guiasturisticos.lista'),
               url(r'^guia-(?P<nro_registro>[0-9]+).html$',
                   views.ficha_guia_turistico,
                   name='guiasturisticos.ficha'),
               ]
