from django.db import models
from core.models import Persona, TituloEducativo, Idioma
from organizaciones.models import Organizacion
from django.core.urlresolvers import reverse


class NivelIdioma(models.Model):
    """ Niveles en que medimos el manejo de un idioma """
    nombre = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre


class NivelIdiomaGuia(models.Model):
    nivel = models.ForeignKey(NivelIdioma)
    idioma = models.ForeignKey(Idioma)

    def __str__(self):
        return '{} ({})'.format(self.idioma.nombre, self.nivel.nombre)


class GuiaTuristico(Persona):
    """
    Guia turistico no hereda de 'Core.Persona' porque aquel esta pensado para
    funcionarios #TODO
    """
    numero_de_registro = models.PositiveIntegerField(default=0)
    idiomas = models.ManyToManyField(NivelIdiomaGuia, blank=True)
    cv_archivo = models.FileField(
        upload_to='CVs/',
        null=True,
        blank=True,
        verbose_name='Curriculum en PDF, DOC u otros')
    publicado = models.BooleanField(default=False)

    def get_absolute_url(self):
        return reverse(
            'guiasturisticos.ficha', kwargs={
                'nro_registro': self.numero_de_registro})


class TituloGuia(models.Model):
    guia = models.ForeignKey(GuiaTuristico)
    titulo = models.ForeignKey(TituloEducativo)
    organizacion_cecrtificante = models.ForeignKey(Organizacion)
    fecha = models.DateField(null=True, blank=True)
    titulo_archivo = models.FileField(
        upload_to='titulos-educativos/',
        null=True,
        blank=True,
        verbose_name='Título en PDF o JPG')
    observaciones_internas = models.TextField(
        verbose_name='Observaciones internas', null=True, blank=True)


class HabilitacionGuiaTuristico(models.Model):
    """ Habiulitación de ejercicio de un Guía Turístico """
    guia = models.ForeignKey(GuiaTuristico)
    fecha_inicio = models.DateField(null=True, blank=True)
    fecha_fin = models.DateField(null=True, blank=True)
    comprobante = models.FileField(
        upload_to='habilitacion-guias-turisticos/',
        null=True,
        blank=True,
        verbose_name='Comprobante en PDF, imagen u otros')
