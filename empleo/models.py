from django.db import models
from core.models import Persona, TituloEducativo
from django.utils.translation import ugettext_lazy as txterr
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator
from simple_history.models import HistoricalRecords
from django.contrib.auth.models import User


class InteresLaboral(models.Model):
    ''' Preferencias en lo que le gustaria trabajar el Candidato '''
    nombre = models.CharField(max_length=250)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Intereses laborales"
        ordering = ['id']


class CursoProfesional(models.Model):
    ''' Cursos profesionales que le gustaria hacer al Candidato '''
    rubro = models.CharField(max_length=130)
    descripcion = models.CharField(max_length=130)

    def __str__(self):
        return '{}-{}'.format(self.rubro, self.descripcion)

    class Meta:
        verbose_name_plural = "Cursos profesionales"
        ordering = ['id']


class Candidato(Persona):
    """ Candidato entrevistado o a entrevistar"""

    PJMYMT = 10
    PROGRESAR = 20
    SCYE_331 = 25
    SCYE_332 = 30
    SCYE_OTROS = 35
    PROMOVER = 40
    SXD = 50
    EGRESADO_FP = 60
    DESOCUPADO = 70

    programas = (
                (PJMYMT, 'PJMyMT'),
                (PROGRESAR, 'Progresar'),
                (SCYE_331, 'SCyE 331'),
                (SCYE_332, 'SCyE 332'),
                (SCYE_OTROS, 'SCyE (Demas resoluciones)'),
                (PROMOVER, 'Promover'),
                (SXD, 'SxD (Seguro por desempleo)'),
                (EGRESADO_FP, 'Egresado de FP'),
                (DESOCUPADO, 'Desocupado (HL Comun)')
    )

    mail = models.EmailField(max_length=254, null=True)
    # Limitamos la cantidad de digitos de las caracteristicas para Argentina
    caracteristica_celular = models.PositiveIntegerField(
        null=True, validators=[MaxValueValidator(99999)])
    celular = models.PositiveIntegerField(null=True)
    # Limitamos la cantidad de digitos de las caracteristicas para Argentina
    caracteristica_fijo = models.PositiveIntegerField(
        null=True, blank=True, validators=[MaxValueValidator(99999)])
    telefono_fijo = models.PositiveIntegerField(null=True, blank=True)
    barrio = models.CharField(max_length=130, null=True)
    estudios = models.ForeignKey(TituloEducativo, null=True)
    especificar_estudios = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        help_text="Ej: Abogacia hasta segundo año; cursando tecnicatura de mecanica; etc")
    programa = models.PositiveIntegerField(choices=programas, default=PJMYMT)
    curso_introduccion_al_trabajo = models.BooleanField(
        default=False, help_text="Tilde: Con CIT. Sin tilde: Sin CIT.")
    experiencia_laboral = models.TextField(null=True)
    primera_preferencia_laboral = models.ForeignKey(
        InteresLaboral,
        related_name='Primeras_preferencias',
        help_text="¿En que tipo de rubro o actividad le gustaria trabajar?")
    segunda_preferencia_laboral = models.ForeignKey(
        InteresLaboral,
        related_name='Segundas_preferencias',
        help_text="¿En que tipo de rubro o actividad le gustaria trabajar?")
    tercera_preferencia_laboral = models.ForeignKey(
        InteresLaboral,
        related_name='Terceras_preferencias',
        help_text="¿En que tipo de rubro o actividad le gustaria trabajar?")
    primera_preferencia_curso = models.ForeignKey(
        CursoProfesional,
        related_name='Primeras_preferencias_curso',
        help_text="¿Que tipos de cursos de Formación Profesional le gustaría hacer?")
    segunda_preferencia_curso = models.ForeignKey(
        CursoProfesional,
        related_name='Segundas_preferencias_curso',
        help_text="¿Que tipos de cursos de Formación Profesional le gustaría hacer?")
    tercera_preferencia_curso = models.ForeignKey(
        CursoProfesional,
        related_name='Terceras_preferencias_curso',
        help_text="¿Que tipos de cursos de Formación Profesional le gustaría hacer?")
    habilidades = models.TextField(
        null=True, help_text="Lo que sabe hacer o se destaca")
    curso_de_capacitacion_realizados = models.TextField(
        null=True, help_text="Cursos realizados")
    observaciones = models.TextField(
        null=True,
        blank=True,
        help_text="Observaciones personales del entrevistado")
    curriculum = models.FileField(
        upload_to='empleo/cv/',
        null=True,
        blank=True,
        help_text='Curriculum escaneado del candidato')

    # Para detectar quien carga a cada candidato
    selector = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL)
    history = HistoricalRecords()

    def __str__(self):
        return '{} {}'.format(self.nombre, self.apellido)

    def cel_completo(self):
        """ Caracteristica del cel + Numero de cel """
        return '{}-{}'.format(self.caracteristica_celular, self.celular)

    def clean(self):
        if self.tipo_id is None:
            raise ValidationError(
                txterr('Debe seleccionar un tipo de documento.'),
            )
        elif self.unique_id is None:
            raise ValidationError(
                txterr('Debe completar el campo documento.'),
            )
        elif self.fecha_nacimiento is None:
            raise ValidationError(
                txterr('Debe completar la fecha de nacimiento.'),
            )
        elif self.genero is None:
            raise ValidationError(
                txterr('Debe completar el campo genero.'),
            )

    def save(self, *args, **kwargs):
        self.clean()
        super(Candidato, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Candidatos"
