#!/usr/bin/python

from django.core.management.base import BaseCommand
from django.db import transaction
from empleo.models import CursoProfesional
import sys
import csv


class Command(BaseCommand):
    help = """Comando para cargar los rubros laborales """

    def add_arguments(self, parser):
        parser.add_argument(
            '--path',
            type=str,
            help='Path del archivo CSV local',
            default='empleo/resources/rubros.csv')

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('--- Comenzando carga ---'))
        fieldnames = ['tipologia', 'rubro', 'subtipologia', 'descripcion']
        count = 0
        path = options['path']
        with open(path, encoding="UTF-8") as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=',', quotechar='"')

            header = reader.__next__()
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR(
                    'BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            for row in reader:
                count += 1
                print(row)

                rubro = row['rubro'].strip()
                descripcion = row['descripcion'].strip()

                cursoprofesional = CursoProfesional.objects.create(
                    rubro=rubro,
                    descripcion=descripcion)
                cursoprofesional.save()

        self.stdout.write(
            "Total de rubros laborales grabados: {}".format(count))
