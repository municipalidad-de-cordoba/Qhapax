#!/usr/bin/python

from django.core.management.base import BaseCommand
from django.db import transaction
from empleo.models import InteresLaboral
import sys
import csv


class Command(BaseCommand):
    help = """Comando para cargar los intereses laborales """

    def add_arguments(self, parser):
        parser.add_argument(
            '--path',
            type=str,
            help='Path del archivo CSV local',
            default='empleo/resources/intereseslaborales.csv')

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('--- Comenzando carga ---'))
        fieldnames = ['INTERESES']
        count = 0
        path = options['path']
        with open(path, encoding="UTF-8") as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=',', quotechar='"')

            header = reader.__next__()
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR(
                    'BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            for row in reader:
                count += 1
                print(row)

                interes = row['INTERESES'].strip()

                intereseslaborales = InteresLaboral.objects.create(
                    nombre=interes)
                intereseslaborales.save()

        self.stdout.write(
            "Total de intereses laborales grabados: {}".format(count))
