from django.contrib import admin
from .models import Candidato, CursoProfesional, InteresLaboral
from .forms import CandidatoForm


class CandidatoAdmin(admin.ModelAdmin):
    form = CandidatoForm
    list_display = ('candidato', 'unique_id', 'edad', 'barrio', 'estudios',
                    'mail', 'cel_completo', 'selector')
    exclude = ('slug', 'nombre_publico', 'html', 'user', 'selector')
    fields = (
        'nombre',
        'apellido',
        'tipo_id',
        'unique_id',
        'foto',
        'fecha_nacimiento',
        'genero',
        'mail',
        ('caracteristica_celular',
         'celular'),
        ('caracteristica_fijo',
         'telefono_fijo'),
        'barrio',
        'estudios',
        'especificar_estudios',
        'programa',
        'curso_introduccion_al_trabajo',
        'experiencia_laboral',
        ('primera_preferencia_laboral',
         'segunda_preferencia_laboral',
         'tercera_preferencia_laboral'),
        ('primera_preferencia_curso',
         'segunda_preferencia_curso',
         'tercera_preferencia_curso'),
        'habilidades',
        'curso_de_capacitacion_realizados',
        'observaciones',
        'curriculum')
    search_fields = (
        'nombre',
        'apellido',
        'unique_id',
        'mail',
        'caracteristica_celular',
        'celular',
        'caracteristica_fijo',
        'telefono_fijo',
        'barrio',
        'estudios__titulo',
        'selector__username')

    # Para despues mostrar que entrevistador hizo la entrevista
    def save_model(self, request, obj, form, change):
        if not obj.pk:
            # Only set selector during the first save.
            obj.selector = request.user
        super().save_model(request, obj, form, change)

    # para que ande el select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


class CursoProfesionalAdmin(admin.ModelAdmin):
    list_display = ('id', 'rubro', 'descripcion')
    search_fields = ('id', 'rubro', 'descripcion')


class InteresLaboralAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')
    search_fields = ('id', 'nombre')


admin.site.register(Candidato, CandidatoAdmin)
admin.site.register(CursoProfesional, CursoProfesionalAdmin)
admin.site.register(InteresLaboral, InteresLaboralAdmin)
