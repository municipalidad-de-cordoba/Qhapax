from django_select2.forms import ModelSelect2Widget
from django import forms
from .models import Candidato, CursoProfesional, InteresLaboral


class InteresLaboralWidget(ModelSelect2Widget):
    model = InteresLaboral
    search_fields = ['nombre__icontains']
    max_results = 10

    def label_from_instance(self, obj):
        return '{}'.format(obj.nombre)

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault(
            'data-placeholder',
            'Ingrese preferencia o parte de ella')
        self.attrs.setdefault('data-minimum-input-length', 1)
        self.attrs.setdefault('data-width', '25em')

        return super(InteresLaboralWidget, self).build_attrs(*args, **kwargs)


class CursoProfesionalWidget(ModelSelect2Widget):
    model = CursoProfesional
    search_fields = ['rubro__icontains', 'descripcion__icontains']
    max_results = 10

    def label_from_instance(self, obj):
        return '{}-{}'.format(obj.rubro, obj.descripcion)

    def build_attrs(self, *args, **kwargs):
        """Add select2 data attributes."""
        self.attrs.setdefault(
            'data-placeholder',
            'Ingrese curso o parte de ella')
        self.attrs.setdefault('data-minimum-input-length', 1)
        self.attrs.setdefault('data-width', '25em')

        return super(CursoProfesionalWidget, self).build_attrs(*args, **kwargs)


class CandidatoForm(forms.ModelForm):

    class Meta:
        model = Candidato
        fields = '__all__'
        widgets = {
            'primera_preferencia_laboral': InteresLaboralWidget,
            'segunda_preferencia_laboral': InteresLaboralWidget,
            'tercera_preferencia_laboral': InteresLaboralWidget,
            'primera_preferencia_curso': CursoProfesionalWidget,
            'segunda_preferencia_curso': CursoProfesionalWidget,
            'tercera_preferencia_curso': CursoProfesionalWidget
        }
