from django.conf.urls import url
from . import views


urlpatterns = [url(r'^lista-de-arboles.(?P<filetype>csv|xls)$',
                   views.lista_arboles, name='arbolado.lista'), ]
