from django.contrib.gis.db import models
from barrios.models import Barrio
from versatileimagefield.fields import VersatileImageField, PPOIField


class EspecieArbol(models.Model):
    nombre = models.CharField(max_length=120)

    def __str__(self):
        return self.nombre


class OrigenDatoArbolado(models.Model):
    ''' los datos pueden venir desde mas de un origen
        cuando comienzo la importación desde un origen y a los fines de eliminar los que no lleguen
        debo marcar a todos los elementos como temporalmente inactivos desde un origen hasta que
        pase de nuevo la migración
        '''
    nombre = models.CharField(max_length=190)

    def __str__(self):
        return self.nombre


class Arbol(models.Model):
    ''' cada arbol plantado o relevado de un censo local '''
    id_externo = models.PositiveIntegerField(default=0)
    barrio = models.ForeignKey(Barrio, null=True, blank=True,
                               on_delete=models.SET_NULL)
    cazuela = models.CharField(max_length=100, null=True, blank=True)
    especie = models.ForeignKey(
        EspecieArbol,
        null=True,
        on_delete=models.CASCADE,
        related_name='arboles')

    FITO_BUENO = 1
    FITO_MALO = 2
    FITO_REGULAR = 3
    FITO_MUERTO = 4
    estados_fitosanitarios = (
        (FITO_BUENO, 'Bueno'),
        (FITO_MALO, 'Malo'),
        (FITO_REGULAR, 'Regular'),
        (FITO_MUERTO, 'Muerto')
    )
    estado_fitosanitario = models.PositiveSmallIntegerField(
        choices=estados_fitosanitarios, null=True, blank=True)

    fecha = models.DateTimeField(
        null=True,
        blank=True,
        help_text='Fecha del relevamiento')

    INCL_SI = 10
    INCL_NO = 20
    INCL_LEVE = 30
    INCL_SEVERA = 40
    tipos_de_inclinacion = (
        (INCL_SI, 'Si'),
        (INCL_NO, 'No'),
        (INCL_LEVE, 'Leve'),
        (INCL_SEVERA, 'Severa')
    )
    inclinacion = models.PositiveSmallIntegerField(
        choices=tipos_de_inclinacion, null=True, blank=True)

    ubicacion = models.PointField(null=True, blank=True)
    lugar_descripcion = models.CharField(
        max_length=300,
        null=True,
        blank=True,
        help_text='Avenida o calle donde se hizo el relevamiento')
    problema_descalzado = models.NullBooleanField(default=False)
    problema_fuste_partido = models.NullBooleanField(
        default=False, help_text='Tronco del árbol')
    problema_desrame = models.NullBooleanField(
        default=False, help_text='Si necesita poda')
    medida_fuste = models.CharField(max_length=50, blank=True, null=True)
    ahuecamiento = models.CharField(max_length=50, blank=True, null=True)
    dist_forestal_adecuada = models.CharField(
        verbose_name='Distribución forestal adecuada',
        null=True,
        blank=True,
        max_length=50,
        help_text='Referido a la copa del árbol(uniforme, mal distribuida, etc).')
    desequilibrio_copa = models.NullBooleanField(
        default=False, verbose_name='Problema de desequilibrio de copa')
    fase_vital = models.CharField(max_length=50, null=True, blank=True)
    altura = models.CharField(max_length=50, null=True, blank=True)
    manejo_inadecuado = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        help_text='Si la poda fue buena o mala.')
    problema_grieta = models.NullBooleanField(default=False)
    interferencia = models.NullBooleanField(
        default=False, help_text='Si interfiere cables, casas, postes, etc.')
    problema_obj_extranio = models.NullBooleanField(
        default=False,
        verbose_name='Problema con objetos extraños',
        help_text='Similar al campo interferencia')
    levantamiento_vereda = models.CharField(
        max_length=50, null=True, blank=True)
    intervenciones = models.CharField(max_length=50, null=True, blank=True)
    origen = models.ForeignKey(OrigenDatoArbolado, null=True, blank=True)

    ORDEN_CENSO = 100
    ORDEN_PLANTACION = 200
    tipos_de_orden = (
        (ORDEN_CENSO, 'Censo'),
        (ORDEN_PLANTACION, 'Plantación')
    )
    tipo_orden_nombre = models.PositiveSmallIntegerField(
        choices=tipos_de_orden,
        null=True,
        blank=True,
        help_text='Indica si fue un relevamiento o una plantación nueva')
    foto = VersatileImageField(
        upload_to='imagenes/arbolado',
        ppoi_field='foto_ppoi',
        null=True,
        blank=True,
        max_length=200,
        help_text='Foto del relevamiento'
    )
    foto_ppoi = PPOIField('Image PPOI')

    def __str__(self):
        especie = 'Especie sin definir' if self.especie is None else self.especie.nombre
        return 'Árbol {} {}'.format(self.id, especie)

    class Meta:
        verbose_name = 'Árbol'
        verbose_name_plural = 'Árboles'
