# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-11-16 17:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('arbolado', '0002_auto_20180604_1441'),
    ]

    operations = [
        migrations.AddField(
            model_name='arbol',
            name='ahuecamiento',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='arbol',
            name='altura',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='arbol',
            name='desequilibrio_copa',
            field=models.BooleanField(default=False, verbose_name='Problema de desequilibrio de copa'),
        ),
        migrations.AddField(
            model_name='arbol',
            name='dist_forestal_adecuada',
            field=models.CharField(blank=True, help_text='Referido a la copa del árbol(uniforme, mal distribuida, etc).', max_length=50, null=True, verbose_name='Distribución forestal adecuada'),
        ),
        migrations.AddField(
            model_name='arbol',
            name='fase_vital',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='arbol',
            name='interferencia',
            field=models.BooleanField(default=False, help_text='Si interfiere cables, casas, postes, etc.'),
        ),
        migrations.AddField(
            model_name='arbol',
            name='intervenciones',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='arbol',
            name='levantamiento_vereda',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='arbol',
            name='manejo_inadecuado',
            field=models.CharField(blank=True, help_text='Si la poda fue buena o mala.', max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='arbol',
            name='medida_fuste',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='arbol',
            name='problema_desrame',
            field=models.BooleanField(default=False, help_text='Si necesita poda'),
        ),
        migrations.AddField(
            model_name='arbol',
            name='problema_grieta',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='arbol',
            name='problema_obj_extranio',
            field=models.BooleanField(default=False, help_text='Similar al campo interferencia', verbose_name='Problema con objetos extraños'),
        ),
    ]
