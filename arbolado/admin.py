from django.contrib import admin
from .models import Arbol, EspecieArbol
from core.admin import QhapaxOSMGeoAdmin


class EspecieArbolAdmin(admin.ModelAdmin):
    list_display = ["id", "nombre"]
    search_fields = ['nombre']


class ArbolAdmin(QhapaxOSMGeoAdmin):
    list_display = [
        'id',
        'especie',
        'barrio',
        'fecha',
        'lugar_descripcion',
        'problema_descalzado',
        'problema_desrame',
        'desequilibrio_copa',
        'problema_grieta',
        'interferencia',
        'tipo_orden_nombre']
    search_fields = [
        'id',
        'id_externo',
        'especie__nombre',
        'lugar_descripcion',
        'barrio__nombre']
    list_filter = [
        'barrio',
        'especie',
        'inclinacion',
        'estado_fitosanitario',
        'problema_descalzado',
        'tipo_orden_nombre',
        'problema_desrame',
        'desequilibrio_copa',
        'problema_grieta',
        'interferencia',
        'problema_obj_extranio']
    date_hierarchy = 'fecha'


admin.site.register(Arbol, ArbolAdmin)
admin.site.register(EspecieArbol, EspecieArbolAdmin)
