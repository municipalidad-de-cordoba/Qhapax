import django_excel as excel
from django.views.decorators.cache import cache_page
from .models import Arbol
# from qhapax import settings  # para dar la url correcta de la foto


@cache_page(60 * 60 * 12)  # 12 h
def lista_arboles(request, filetype):
    '''
    Lista de arboles en csv o xls
    http://localhost:8000/arbolado/lista-de-arboles.csv
    '''

    arboles = Arbol.objects.all()
    csv_list = []
    csv_list.append(['Id',
                     'Barrio',
                     'Cazuela',
                     'Especie',
                     'Estado fitosanitario',
                     'Inclinacion',
                     'Lugar descripcion',
                     'Problema descalzado',
                     'Problema fuste partido',
                     'Tipo orden nombre'])  # , 'Foto'

    for arbol in arboles:
        # Transformamos los booleanos a strings
        problema_descalzado = 'No'
        problema_fuste_partido = 'No'
        if arbol.problema_descalzado:
            problema_descalzado = 'Si'
        if arbol.problema_fuste_partido:
            problema_fuste_partido = 'Si'

        barrio = 'Sin definir' if arbol.barrio is None else arbol.barrio.nombre
        especie = 'Sin definir' if arbol.especie is None else arbol.especie.nombre

        csv_list.append([
            arbol.id,
            barrio,
            arbol.cazuela,
            especie,
            arbol.get_estado_fitosanitario_display(),
            arbol.get_inclinacion_display(),
            arbol.lugar_descripcion,
            problema_descalzado,
            problema_fuste_partido,
            arbol.get_tipo_orden_nombre_display(),
            # hay algunas fotos de veredas en vez de arboles (?)
            # settings.PROTOCOLO_PRODUCCION + '://' + settings.DOMINIO_PRODUCCION + arbol.foto.url
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
