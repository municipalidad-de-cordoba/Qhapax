from django.conf.urls import url, include
from rest_framework import routers
from .views import ArbolViewSet, ArbolContadorViewSet, EspecieArbolViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register('arboles', ArbolViewSet, base_name='arbol.api.lista')
router.register(
    'contador-arboles',
    ArbolContadorViewSet,
    base_name='contador-arbol.api.lista')
router.register(
    'especies',
    EspecieArbolViewSet,
    base_name='especies.api.lista')

urlpatterns = [
    url(r'^', include(router.urls)),
]
