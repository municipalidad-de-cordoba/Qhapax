from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from .serializers import (ArbolSerializer, ArbolContadorSerializer,
                          EspecieArbolSerializer, GroupArbolSerializer)
from api.pagination import DefaultPagination
from arbolado.models import Arbol, EspecieArbol
from rest_framework_gis.filters import InBBoxFilter
from django.db.models import Count
from django.contrib.gis.db.models.functions import GeoHash


class ArbolViewSet(viewsets.ModelViewSet):
    """
    Lista de árboles geolocalizadas con sus datos
    Puede filtrarse por:
     - especie_ids (identificador de la especie), pueden ser varios separados por comas
     - especie_nombre (nombre de la especie)
     - barrio_nombre: nombre o parte del nombre, pueden ser varios separados por coma.
        Ejemplo: barrio_nombre=CENTRO,NUEVA CORDOBA
     - cazuela: estado de la cazuela. Los estados posibles son:
        Faltante
        Adecuada
        Inadecuada
        Franja Verde
        Pueden ser varios estados separados por coma. Ejemplo: cazuela=Faltante,Inadecuada
        (Notar las mayúsculas en los nombres de los estados)
     - fitosanitario: estado fitosanitario del árbol, puede ser alguna de las siguientes opciones
        bueno = 1
        malo = 2
        regular = 3
        muerto = 4
        Escribir el número. Si se escribe más de un número, funciona como un and.
        Ejemplo: fitosanitario=2,3 filtra los árboles con estado malo Y regular.
     - inclinacion = inclinación del árbol, puede ser alguna de las siguientes opciones:
        si = 10
        no = 20
        leve = 30
        severa = 40
        Escribir el número. Si se escribe más de un número funciona como un and.
        Ejemplo: inclinacion=10,40 filtra los árboles que si tienen inclinación y es severa.
    - fuste_partido: Se refiere al tronco del árbol. Posibles valores: si o no.
    - descalzado: Problemas de la base del árbol. Posibles valores: si o no.
    - tipo_orden: puede ser alguna de las siguientes opciones:
        censo = 100
        plantacion = 200
        Escribir el número.
    - in_bbox = se usa para buscar árboles dentro de un Bounding Box. Formato
        de parámetros (min Lon, min Lat, max Lon, max Lat). Ejemplo:
        in_bbox=-64.19215920250672,-31.413600429103756,-64.19254544060477,-31.41472664471516
    - precision = valor de 5 a 10 para clusterizar los puntos. Si se usa este parametro
        entonces el resultado serán puntos con un contador de árboles, sin propiedades de
        cada árbol. Tamaño del cluster según valor:
        5 4.89km × 4.89km
        6 1.22km × 0.61km
        7 153m × 153m
        8 38.2m × 19.1m
        9 4.77m × 4.77m
        10 1.19m × 0.596m
    """
    serializer_class = ArbolSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination
    bbox_filter_field = 'ubicacion'
    filter_backends = (InBBoxFilter, )
    # bbox_filter_include_overlapping = True # Optional

    def get_queryset(self):
        queryset = Arbol.objects.exclude(ubicacion__isnull=True)

        especie_ids_str = self.request.query_params.get('especie_ids', None)
        if especie_ids_str is not None:
            especie_ids = especie_ids_str.split(',')
            queryset = queryset.filter(especie__id__in=especie_ids)

        barrio_nombre = self.request.query_params.get('barrio_nombre', None)
        if barrio_nombre is not None:
            barrio_nombre = barrio_nombre.split(',')
            queryset = queryset.filter(barrio__nombre__in=barrio_nombre)

        cazuela = self.request.query_params.get('cazuela', None)
        if cazuela is not None:
            cazuela = cazuela.split(',')
            queryset = queryset.filter(cazuela__in=cazuela)

        fitosanitario = self.request.query_params.get('fitosanitario', None)
        if fitosanitario is not None:
            fitosanitario = fitosanitario.split(',')
            queryset = queryset.filter(estado_fitosanitario__in=fitosanitario)

        inclinacion = self.request.query_params.get('inclinacion', None)
        if inclinacion is not None:
            inclinacion = inclinacion.split(',')
            queryset = queryset.filter(inclinacion__in=inclinacion)

        fuste_partido = self.request.query_params.get('fuste_partido', None)
        if fuste_partido is not None:
            if fuste_partido == 'si':
                queryset = queryset.filter(problema_fuste_partido=True)
            elif fuste_partido == 'no':
                queryset = queryset.filter(problema_fuste_partido=False)

        descalzado = self.request.query_params.get('descalzado', None)
        if descalzado is not None:
            if descalzado == 'si':
                queryset = queryset.filter(problema_descalzado=True)
            elif descalzado == 'no':
                queryset = queryset.filter(problema_descalzado=False)

        tipo_orden = self.request.query_params.get('tipo_orden', None)
        if tipo_orden is not None:
            queryset = queryset.filter(tipo_orden_nombre=tipo_orden)

        precision_str = self.request.query_params.get('precision', None)
        if precision_str:
            precision = int(precision_str)
            # en este caso el serializador no tiene todos los campos
            self.serializer_class = GroupArbolSerializer
            anot = GeoHash('ubicacion', precision=precision)
            queryset = queryset.annotate(
                ubicacion_geohash=anot).values('ubicacion_geohash').annotate(
                cluster_count=Count('ubicacion_geohash'))

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ArbolContadorViewSet(viewsets.ModelViewSet):
    """
    Lista de árboles por id y nombre.
    Puede filtrarse por:
     - especie_ids (identificador de la especie), pueden ser varios separados por comas
     - especie_nombre (nombre de la especie)
     - barrio_nombre: nombre o parte del nombre, pueden ser varios separados por coma.
        Ejemplo: barrio_nombre=CENTRO,NUEVA CORDOBA
     - cazuela: estado de la cazuela. Los estados posibles son:
        Faltante
        Adecuada
        Inadecuada
        Franja Verde
        Pueden ser varios estados separados por coma. Ejemplo: cazuela=Faltante,Inadecuada
        (Notar las mayúsculas en los nombres de los estados)
     - fitosanitario: estado fitosanitario del árbol, puede ser alguna de las siguientes opciones
        bueno = 1
        malo = 2
        regular = 3
        muerto = 4
        Escribir el número. Si se escribe más de un número, funciona como un and.
        Ejemplo: fitosanitario=2,3 filtra los árboles con estado malo Y regular.
     - inclinacion = inclinación del árbol, puede ser alguna de las siguientes opciones:
        si = 10
        no = 20
        leve = 30
        severa = 40
        Escribir el número. Si se escribe más de un número funciona como un and.
        Ejemplo: inclinacion=10,40 filtra los árboles que si tienen inclinación y es severa.
    - fuste_partido: Se refiere al tronco del árbol. Posibles valores: si o no.
    - descalzado: Problemas de la base del árbol. Posibles valores: si o no.
    - tipo_orden: puede ser alguna de las siguientes opciones:
        censo = 100
        plantacion = 200
        Escribir el número.
    """
    serializer_class = ArbolContadorSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = Arbol.objects.all()

        especie_ids_str = self.request.query_params.get('especie_ids', None)
        if especie_ids_str is not None:
            especie_ids = especie_ids_str.split(',')
            queryset = queryset.filter(especie__id__in=especie_ids)
        else:
            especies_ausentes_id = 2  # FIXME sacar del importador esta especie y poner NONE
            queryset = queryset.exclude(especie__id=especies_ausentes_id)

        barrio_nombre = self.request.query_params.get('barrio_nombre', None)
        if barrio_nombre is not None:
            barrio_nombre = barrio_nombre.split(',')
            queryset = queryset.filter(barrio__nombre__in=barrio_nombre)

        cazuela = self.request.query_params.get('cazuela', None)
        if cazuela is not None:
            cazuela = cazuela.split(',')
            queryset = queryset.filter(cazuela__in=cazuela)

        fitosanitario = self.request.query_params.get('fitosanitario', None)
        if fitosanitario is not None:
            fitosanitario = fitosanitario.split(',')
            queryset = queryset.filter(estado_fitosanitario__in=fitosanitario)

        inclinacion = self.request.query_params.get('inclinacion', None)
        if inclinacion is not None:
            inclinacion = inclinacion.split(',')
            queryset = queryset.filter(inclinacion__in=inclinacion)

        fuste_partido = self.request.query_params.get('fuste_partido', None)
        if fuste_partido is not None:
            if fuste_partido == 'si':
                queryset = queryset.filter(problema_fuste_partido=True)
            elif fuste_partido == 'no':
                queryset = queryset.filter(problema_fuste_partido=False)

        descalzado = self.request.query_params.get('descalzado', None)
        if descalzado is not None:
            if descalzado == 'si':
                queryset = queryset.filter(problema_descalzado=True)
            elif descalzado == 'no':
                queryset = queryset.filter(problema_descalzado=False)

        tipo_orden = self.request.query_params.get('tipo_orden', None)
        if tipo_orden is not None:
            queryset = queryset.filter(tipo_orden_nombre=tipo_orden)

        return queryset

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class EspecieArbolViewSet(viewsets.ModelViewSet):
    """
    Especies de árboles registrados ordenados alfabeticamente.
    Se puede filtrar por:
    - ids = id de las especies, pueden ser varios ids separados por comas
    - nombre = nombre de la especie o parte del nombre.
    """
    serializer_class = EspecieArbolSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    pagination_class = DefaultPagination

    def get_queryset(self):
        queryset = EspecieArbol.objects.all()

        ids = self.request.query_params.get('ids', None)
        if ids is not None:
            ids = ids.split(',')
            queryset = queryset.filter(id__in=ids)

        nombre = self.request.query_params.get('nombre', None)
        if nombre is not None:
            queryset = queryset.filter(nombre__icontains=nombre)

        return queryset.order_by('nombre')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
