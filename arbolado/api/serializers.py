from rest_framework_cache.serializers import CachedSerializerMixin
from rest_framework_cache.registry import cache_registry
from rest_framework import serializers
from rest_framework_gis.serializers import (GeoFeatureModelSerializer,
                                            GeometrySerializerMethodField)
from versatileimagefield.serializers import VersatileImageFieldSerializer
from arbolado.models import Arbol, EspecieArbol
import geohash
from django.contrib.gis.geos import Point


class EspecieArbolSerializer(CachedSerializerMixin):

    class Meta:
        model = EspecieArbol
        fields = ('id', 'nombre')


class GroupArbolSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):

    cluster_count = serializers.IntegerField()
    ubicacion_geohash = serializers.CharField()
    punto_agrupador = GeometrySerializerMethodField()

    def get_punto_agrupador(self, obj):
        """Parse a geohashed point to Geometry."""
        y, x = geohash.decode(obj['ubicacion_geohash'])
        return Point(x, y)

    class Meta:
        model = Arbol
        geo_field = "punto_agrupador"
        # auto_bbox = True
        fields = ('ubicacion_geohash', 'cluster_count')


class ArbolSerializer(GeoFeatureModelSerializer, CachedSerializerMixin):

    especie = EspecieArbolSerializer(read_only=True)
    barrio = serializers.SerializerMethodField()
    estado_fitosanitario = serializers.SerializerMethodField()
    inclinacion = serializers.SerializerMethodField()
    tipos_de_orden = serializers.SerializerMethodField()
    foto = VersatileImageFieldSerializer([("original", 'url'),
                                          ("thumbnail_32x32", 'thumbnail__32x32'),
                                          ("thumbnail_125", 'thumbnail__125x125'),
                                          ("thumbnail_500", 'thumbnail__500x500')])

    def get_barrio(self, obj):
        """
        Todos los árboles se corresponden a un barrio pero como el modelo
        acepta este campo como NULL se controla que no falle
        """
        return obj.barrio.nombre if obj.barrio is not None else 'Desconocido'

    def get_estado_fitosanitario(self, obj):
        return obj.get_estado_fitosanitario_display()

    def get_inclinacion(self, obj):
        return obj.get_inclinacion_display()

    def get_tipos_de_orden(self, obj):
        return obj.get_tipo_orden_nombre_display()

    class Meta:
        model = Arbol
        geo_field = "ubicacion"
        auto_bbox = True
        fields = ('id', 'especie', 'barrio', 'cazuela', 'estado_fitosanitario',
                  'inclinacion', 'lugar_descripcion', 'problema_descalzado',
                  'problema_fuste_partido', 'tipos_de_orden', 'foto')


class ArbolContadorSerializer(CachedSerializerMixin):
    """
    Versión simplificada de ArbolSerializer.
    Sirve para que sea un contador no afectado por los bbox
    """
    especie = EspecieArbolSerializer(read_only=True)

    class Meta:
        model = Arbol
        fields = ('id', 'especie')


# Registro los serializadores en la cache de DRF
cache_registry.register(EspecieArbolSerializer)
cache_registry.register(ArbolSerializer)
