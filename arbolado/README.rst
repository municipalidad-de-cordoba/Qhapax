Árboles y su estado
===================

Aplicación para tomar los datos de un censo de árboles de la ciudad. El proveedor nos entrega un API 
de la que extraemos los datos.  

Los campos especificados que recibimos son:

 - "barrioNombre": "string": Nombre del Barrio donde se hizo el relevamiento.-
 - "cazuela": "string": Estado de la Cazuela donde esta o no el Arbol relevado: Ejemplo:
    + * Cazuela faltante: En el lugar relevado tendría que haber una cazuela para alojar al arbol
    + * Cazuela adecuada: Cazuela OK
    + * Franja Verde: En el lugar relevado es un cantero o similar. Tendría que haber una cazuela para alojar al arbol 
    + * Cazuela inadecuada: En el lugar relevado tendría que haber una cazuela para alojar al árbol   
 - "especie": "string": Especie a la que hace referencia el Arbol, en caso de ser Null, es un faltante de Arbol
 - "estadoFitosanitario": "string": Estado fitosanitario del Arbol. Bueno, Malo, Regular
 - "fecha": "2018-05-15T00:00:00Z": Fecha de relevamiento
 - "id": 0: Id Arbol
 - "inclinacion": "string": Inclinacion del Arbol [SI/NO] 
 - "latitud": 0: Ubicación de Arbol
 - "longitud": 0: Ubicación de Arbol
 - "lugarDescripcion": "string": Avenida o calle donde se hizo el relevamiento
 - "problemaDescalzado": true: Dato relacionado al estado del Árbol [SI/NO]
 - "problemaFustePartido": true: Dato relacionado al estado del Arbol [SI/NO]
 - "tipoOrdenNombre": "string": [Censo/Plantacion] Si fue un relevamiento o una Plantación nueva
 - "imagenTmpUrl": "string": URL donde descargar la foto del relevamiento


Tamaños de Geohash segun largo de caracteres
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

length	Cell width	Cell height
 1	≤ 5,000km	×	5,000km
 2	≤ 1,250km	×	625km
 3	≤ 156km	×	156km
 4	≤ 39.1km	×	19.5km
 5	≤ 4.89km	×	4.89km
 6	≤ 1.22km	×	0.61km
 7	≤ 153m	×	153m
 8	≤ 38.2m	×	19.1m
 9	≤ 4.77m	×	4.77m
 10	≤ 1.19m	×	0.596m
 11	≤ 149mm	×	149mm
 12	≤ 37.2mm	×	18.6mm
