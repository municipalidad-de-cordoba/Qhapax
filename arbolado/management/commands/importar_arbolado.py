#!/usr/bin/python
"""
Importa datos de aŕboles de software externo
"""
from django.core.management.base import BaseCommand
# from django.db import transaction
import sys
from datetime import datetime
import requests
import json
import pytz
from arbolado.settings import (URL_API_ARBOLADO, URL_TOKEN_ARBOLADO,
                               USER_API_ARBOLADO, PASS_API_ARBOLADO)
from arbolado.models import Arbol, EspecieArbol, OrigenDatoArbolado
from barrios.models import Barrio
import difflib
from django.contrib.gis.geos import Point
from django.core.files import File
import tempfile
from PIL import Image
from core.utils import normalizar


class Command(BaseCommand):
    help = """Comando para Importar arbolado de la ciudad de Córdoba"""

    def add_arguments(self, parser):
        parser.add_argument(
            '--fecha',
            type=str,
            help='fecha desde la cual se importarán datos. Formato aaaa-mm-dd',
            default='2018-01-01')
        parser.add_argument('--start_page',
                            type=int,
                            help='pagina de inicio',
                            default='0')

    # @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS(
            'Iniciando importación de arbolado'))

        fecha = options['fecha']
        start_page = options['start_page']
        url_base = URL_API_ARBOLADO
        url_token = URL_TOKEN_ARBOLADO
        user = USER_API_ARBOLADO
        passw = PASS_API_ARBOLADO
        # url = '{}?fechaDesde={}T00:00:00Z'.format(url_base, fecha)
        payload = {"username": user, "password": passw}
        first_page = '/api/v1/objeto-relevados?page={}&size=100'.format(
            start_page)

        # SOLICITUD DE TOKEN DE AUTENTICACION
        r = requests.post(url_token, json=payload)
        # self.stdout.write(self.style.SUCCESS('Req Token json {}'.format(r.text)))
        token_json = r.json()
        # self.stdout.write(self.style.SUCCESS('Token json {}'.format(token_json)))
        token = token_json['id_token']

        barrios = Barrio.objects.values_list('nombre', flat=True)
        poligonos_barrios = Barrio.objects.values_list('poligono', flat=True)

        session = requests.Session()
        token = 'Bearer {}'.format(token)
        headers = {'Authorization': token, 'Connection': 'close'}
        params = {'fechaDesde': '{}T00:00:00Z'.format(fecha)}

        # primera petición
        self.stdout.write(
            self.style.SUCCESS(
                'Buscando datos en {}'.format(
                    url_base +
                    first_page)))
        basedata = session.get(
            url_base + first_page,
            headers=headers,
            params=params)
        last_page = basedata.links['last']['url']
        next_page = basedata.links['next']['url']

        # chequear los datos
        errores = []
        arbol_nuevo = 0
        arbol_repetido = 0
        foto_arbol_nueva = 0
        foto_arbol_omitida = 0
        especies_nuevas = 0
        especies_repetidas = 0

        pagina = 0
        # marcar como el origen que tienen
        origen, created = OrigenDatoArbolado.objects.get_or_create(
            nombre='MuniDigital.com')

        while True:
            try:
                basedata = basedata.content
                # basedata = basedata.decode("iso-8859-1")
                basedata = basedata.decode("utf-8")

            except Exception as e:
                print(e)
                sys.exit(1)

            # EJEMPLO DE RESULTADOS
            # [
                # {
                #     'problemaDesrames': True,
                #     'fuste': 'Entre 0,10m. y 0,40m.',
                #     'ahuecamiento': 'No',
                #     'distForestalAdecuada': True,
                #     'problemaDesequilibrioCopa': False,
                #     'fecha': '2018-01-15T14:18:59Z',
                #     'faseVital': 'Maduro',
                #     'altura': 'Entre 7m. y 10m.',
                #     'inclinacion': 'No',
                #     'imagenTmpUrl': 'https://arbolapp-usercontent.s3-sa-east-1.amazonaws.com/adjuntos/1516025956_JPEG_20180115_111245_1132615882.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20181115T164033Z&X-Amz-SignedHeaders=host&X-Amz-Expires=3660&X-Amz-Credential=AKIAJXMNJIVAT4PMGCFQ%2F20181115%2Fsa-east-1%2Fs3%2Faws4_request&X-Amz-Signature=9e4dd976d2968f13b32c1357fa8c3c52d59e011d270a68395562a5611cbeceac',
                #     'lugarDescripcion': 'Sgo. del Estero 100-200',
                #     'manejoInadecuado': 'Leve',
                #     'estadoFitosanitario': 'Regular',
                #     'problemaGrieta': False,
                #     'interf': False,
                #     'id': 22,
                #     'cazuela': 'Inadecuada',
                #     'problemaFustePartido': False,
                #     'problemaObjetoExtranio': False,
                #     'longitud': -64.1767062,
                #     'levantamientoVereda': 'Leve',
                #     'barrioNombre': 'Centro',
                #     'intervenciones': 'Mantenimiento',
                #     'problemaDescalzado': False,
                #     'tipoOrdenNombre': 'Censo',
                #     'especie': 'Fresno Americano',
                #     'latitud': -31.4159134
                # }
            # ]

            try:
                jsondata = json.loads(basedata)
            except Exception as e:
                self.stdout.write(self.style.ERROR(
                    'JSON Error: {}'.format(basedata)))
                sys.exit(1)

            pagina += 1
            arboles = len(jsondata)
            self.stdout.write(
                self.style.SUCCESS(
                    'Se obtuvieron datos de {} arboles'.format(arboles)))

            for arbolado in jsondata:

                arbol, created_arbol = Arbol.objects.get_or_create(
                    id_externo=arbolado['id'])
                if created_arbol:
                    arbol_nuevo += 1
                else:
                    arbol_repetido += 1
                if 'id' not in arbolado.keys():
                    error = 'El árbol {} no tiene ID'.format(
                        arbolado['especie'])
                    errores.append(error)
                    continue

                # ubicacion
                latitud = arbolado['latitud']
                longitud = arbolado['longitud']
                point = Point(longitud, latitud)
                arbol.ubicacion = point

                """ acá se obtiene el barrio que mejor coincida con los de
                    nuestra bd. No siempre arroja resultados, y a veces no
                    matchea correctamente por lo que si no encontramos el barrio
                    en nuestra bd,lo buscamos a través de la ubicación del árbol
                """

                # barrio
                # Hay barrios que vienen como NULL!
                if arbolado['barrioNombre'] is not None:
                    match = difflib.get_close_matches(
                        arbolado['barrioNombre'].upper(), possibilities=barrios, n=1)
                    if match == []:
                        barrio = None
                        for poligono in poligonos_barrios:
                            if poligono is not None and poligono.contains(
                                    point):
                                barrio = Barrio.objects.get(poligono=poligono)
                                break
                    else:
                        barrio = Barrio.objects.get(nombre=match[0])
                else:
                    barrio = None
                    for poligono in poligonos_barrios:
                        if poligono is not None and poligono.contains(point):
                            barrio = Barrio.objects.get(poligono=poligono)
                            break
                arbol.barrio = barrio

                # cazuela
                arbol.cazuela = arbolado['cazuela']

                # especie
                especie = arbolado['especie'].strip(
                ) if arbolado['especie'] is not None else 'Ausente'
                especie, created_especie = EspecieArbol.objects.get_or_create(
                    nombre=especie)
                if created_especie:
                    especies_nuevas += 1
                else:
                    especies_repetidas += 1
                arbol.especie = especie

                # si la especie es ausente, no tiene estado_fitosanitario
                if especie != 'Ausente':
                    try:
                        # estado_fitosanitario
                        estado_fitosanitario = arbolado['estadoFitosanitario']
                        if normalizar(estado_fitosanitario) == 'bueno':
                            arbol.estado_fitosanitario = Arbol.FITO_BUENO
                        elif normalizar(estado_fitosanitario) == 'malo':
                            arbol.estado_fitosanitario = Arbol.FITO_MALO
                        elif normalizar(estado_fitosanitario) == 'regular':
                            arbol.estado_fitosanitario = Arbol.FITO_REGULAR
                        elif normalizar(estado_fitosanitario) == 'muerto':
                            arbol.estado_fitosanitario = Arbol.FITO_MUERTO
                        else:
                            arbol.estado_fitosanitario = None

                        # inclinacion
                        inclinacion = arbolado['inclinacion']
                        if normalizar(inclinacion) == 'si':
                            arbol.inclinacion = Arbol.INCL_SI
                        elif normalizar(inclinacion) == 'no':
                            arbol.inclinacion = Arbol.INCL_NO
                        elif normalizar(inclinacion) == 'leve':
                            arbol.inclinacion = Arbol.INCL_LEVE
                        elif normalizar(inclinacion) == 'severa':
                            arbol.inclinacion = Arbol.INCL_SEVERA

                        # problema fuste partido
                        arbol.problema_fuste_partido = arbolado['problemaFustePartido']

                        # problema_descalzado
                        arbol.problema_descalzado = arbolado['problemaDescalzado']

                        # problema desrame
                        arbol.problema_desrame = arbolado['problemaDesrames']

                        # medida del fuste
                        arbol.medida_fuste = arbolado['fuste']

                        # ahuecamiento
                        arbol.ahuecamiento = arbolado['ahuecamiento']

                        # dist_forestal_adecuada
                        arbol.dist_forestal_adecuada = arbolado['distForestalAdecuada']

                        # desequilibrio_copa
                        arbol.desequilibrio_copa = arbolado['problemaDesequilibrioCopa']

                        # fase vital
                        arbol.fase_vital = arbolado['faseVital']

                        # altura
                        arbol.altura = arbolado['altura']

                        # manejo inadecuado
                        arbol.manejo_inadecuado = arbolado['manejoInadecuado']

                        # problema_grieta
                        arbol.problema_grieta = arbolado['problemaGrieta']

                        # interferencia
                        arbol.interferencia = arbolado['interf']

                        # problema de obj extraño
                        arbol.problema_obj_extranio = arbolado['problemaObjetoExtranio']

                        # levantamiento_vereda
                        arbol.levantamiento_vereda = arbolado['levantamientoVereda']

                        # intervenciones
                        arbol.intervenciones = arbolado['intervenciones']

                    except Exception as e:
                        err = 'ERROR en árbol id externo:{} - {}'.format(
                            arbol.id_externo, e)
                        # self.stdout.write(self.style.ERROR(err))
                        errores.append(err)

                        # continuamos con el siguiente árbol
                        continue

                # fecha
                # viene como aaaa-mm-ddThh:mm:20Z
                fecha = datetime.strptime(
                    arbolado['fecha'].strip(), "%Y-%m-%dT%H:%M:%SZ")
                arbol.fecha = pytz.utc.localize(fecha)

                # lugar_descripcion
                arbol.lugar_descripcion = arbolado['lugarDescripcion']

                # tipo_orden_nombre
                tipo_orden = arbolado['tipoOrdenNombre']
                if normalizar(tipo_orden) == 'censo':
                    arbol.tipo_orden_nombre = Arbol.ORDEN_CENSO
                else:
                    arbol.tipo_orden_nombre = Arbol.ORDEN_PLANTACION

                # imagen
                if created_arbol:
                    foto_arbol_nueva += 1
                    url_foto = arbolado['imagenTmpUrl']
                    # self.stdout.write(self.style.SUCCESS('Trayendo imagen {}'.format(url_foto)))
                    contenido = requests.get(url_foto).content  # es tipo byte

                    tf = tempfile.NamedTemporaryFile()
                    tf.write(contenido)

                    img = File(tf)
                    try:
                        # controlamos que el archivo sea una imagen válida
                        img = Image.open(img)
                        if img.size != 0:
                            arbol.foto.save(
                                str(url_foto.split('/')[-1][-15:]), File(tf))
                    except Exception as e:
                        err = 'ERROR en la FOTO:{}, {}'.format(url_foto, e)
                        # self.stdout.write(self.style.ERROR(err))
                        errores.append(err)
                        continue
                else:
                    # FIXME: ¿Qué pasa si actualizan la foto?
                    # self.stdout.write(self.style.SUCCESS('Omitiendo descargar imagen {}'.format(arbolado['imagenTmpUrl'])))
                    foto_arbol_omitida += 1

                arbol.origen = origen
                arbol.save()

            self.stdout.write(
                self.style.SUCCESS(
                    'PAGINA: {}. Árboles {} nuevos, {} repetidos. Especies {} nuevas {} repetidas. Fotos {} nuevas, {} omitidas'.format(
                        pagina,
                        arbol_nuevo,
                        arbol_repetido,
                        especies_nuevas,
                        especies_repetidas,
                        foto_arbol_nueva,
                        foto_arbol_omitida)))
            if next_page == last_page:
                break
            else:
                # self.stdout.write(self.style.SUCCESS('Buscando datos en {}'.format(url_base+next_page)))
                basedata = session.get(
                    url_base + next_page,
                    headers=headers,
                    params=params)
                next_page = basedata.links['next']['url']

        if len(errores) > 0:
            self.stdout.write(self.style.ERROR('ERRORES AL PROCESAR'))
            for e in errores:
                self.stdout.write(self.style.ERROR(e))

        self.stdout.write(self.style.SUCCESS('RESUMEN:'))
        self.stdout.write(
            self.style.SUCCESS(
                'Total de árboles nuevos: {}'.format(arbol_nuevo)))
        self.stdout.write(
            self.style.SUCCESS(
                'Total de árboles actualizados: {}'.format(arbol_repetido)))
        self.stdout.write(
            self.style.SUCCESS(
                'Total de especies nuevas: {}'.format(especies_nuevas)))
        self.stdout.write(
            self.style.SUCCESS(
                'Total de especies repetidas: {}'.format(especies_repetidas)))
        self.stdout.write(self.style.SUCCESS('FIN'))
