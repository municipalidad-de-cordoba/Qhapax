#!/usr/bin/python
'''
Grabar lista de arboles a carpeta media para despues servirlo al portal.
Por ahora se hace esto hasta que se pueda construir online en real time.
'''
from django.core.management.base import BaseCommand
from django.conf import settings
import django_excel as excel
from django.db import transaction
import os
from arbolado.models import Arbol


class Command(BaseCommand):
    help = """Generar un CSV/XLS con todos los arboles de la ciudad"""

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS(
            'Comenzando generación de CSV y XLS'))

        self.stdout.write(self.style.SUCCESS('Buscando arboles'))
        arboles = Arbol.objects.all()
        self.stdout.write(
            self.style.SUCCESS(
                '{} arboles encontrados'.format(
                    len(arboles))))

        csv_list = []

        csv_list.append(['Id',
                         'Barrio',
                         'Cazuela',
                         'Especie',
                         'Estado fitosanitario',
                         'Inclinacion',
                         'Lugar descripcion',
                         'Problema descalzado',
                         'Problema fuste partido',
                         'Tipo orden nombre'])  # , Foto

        c = 0
        for arbol in arboles:
            c += 1
            # Transformamos los booleanos a strings
            problema_descalzado = 'No'
            problema_fuste_partido = 'No'
            if arbol.problema_descalzado:
                problema_descalzado = 'Si'
            if arbol.problema_fuste_partido:
                problema_fuste_partido = 'Si'

            barrio = 'Sin definir' if arbol.barrio is None else arbol.barrio.nombre
            especie = 'Sin definir' if arbol.especie is None else arbol.especie.nombre

            csv_list.append([
                arbol.id,
                barrio,
                arbol.cazuela,
                especie,
                arbol.get_estado_fitosanitario_display(),
                arbol.get_inclinacion_display(),
                arbol.lugar_descripcion,
                problema_descalzado,
                problema_fuste_partido,
                arbol.get_tipo_orden_nombre_display(),
                # hay algunas fotos de veredas en vez de arboles (?)
                # settings.PROTOCOLO_PRODUCCION + '://' + settings.DOMINIO_PRODUCCION + arbol.foto.url
            ])

        # FIXME #THINKME pensar una forma centralizada de guardar datos que no
        # se generan en timpo real

        dest_csv = os.path.join(settings.MEDIA_ROOT, 'lista-de-arboles.csv')
        dest_xls = os.path.join(settings.MEDIA_ROOT, 'lista-de-arboles.xls')

        excel.pe.save_as(array=csv_list, dest_file_name=dest_csv)
        excel.pe.save_as(array=csv_list, dest_file_name=dest_xls)

        self.stdout.write(
            self.style.SUCCESS(
                'Archivos creados con {} arboles cada uno'.format(c)))
