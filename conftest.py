import pytest
from website.tests.factories import WebsiteFactory

@pytest.fixture
def website(db):
    return WebsiteFactory()
