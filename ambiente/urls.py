from django.conf.urls import url
from ambiente.views import lista_ambiente


urlpatterns = [url(r'^lista-de-ambiente.(?P<filetype>csv|xls)$',
                   lista_ambiente, name='ambiente.lista'), ]
