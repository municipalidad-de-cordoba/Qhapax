from django.db import models


class Ambiente(models.Model):

    BUENO = 10
    MODERADO = 20
    INSALUBRE = 30
    PELIGROSO = 40

    estados = (
        (BUENO, 'Bueno'),
        (MODERADO, 'Moderado'),
        (INSALUBRE, 'Insalubre para personas sensibles'),
        (PELIGROSO, 'Peligroso')
    )

    fecha_de_datos = models.DateField(null=True, blank=True)
    hora_datos = models.TimeField(null=True, blank=True)
    NO = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ppm")
    NO2 = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ppm")
    NOx = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ppm")
    CO = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ppm")
    O3 = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ppm")
    Material_particulado_total = models.CharField(
        max_length=10, null=True, blank=True,
        help_text="Unidad de medida en mg/m3")
    Temp_Amb = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en °C")
    Sensacion_Termica = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en °C")
    Presion_ambiental = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en hPa")
    Humedad_Relativa_Ambiente = models.CharField(
        max_length=10, null=True, blank=True,
        help_text="Unidad de medida en %")
    Direccion_del_viento = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en °")
    Velocidad_del_viento = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en m/s")
    Radiacion_solar = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en W/m2")
    Radiacion_UV = models.CharField(max_length=10, null=True, blank=True)
    Lluvia_diaria = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en mm")
    Lluvia_mensual = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en mm")
    Ozono_8hr = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ppm cada 8 hr")
    Ozono_1hr = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ppm cada 1 hr")
    PM_10 = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ug/m^3 cada 24 hr")
    CO_8hr = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ppm cada 8 hr")
    NO2_1hr = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ppb cada 1 hr")
    ICA_Ozono_8hr = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ppm cada 8 hr")
    ICA_Ozono_1hr = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ppm cada 1 hr")
    ICA_PM_10 = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ug/m^3 cada 24 hr")
    ICA_CO = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ppm cada 8 hr")
    ICA_NO2 = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        help_text="Unidad de medida en ppb cada 1 hr")
    ICA = models.CharField(max_length=10, null=True, blank=True)
    Estado = models.PositiveIntegerField(choices=estados, default=BUENO)
    Contaminante_maximo = models.CharField(
        max_length=50, null=True, blank=True)

    creado = models.DateTimeField(
        auto_now=False,
        auto_now_add=True,
        null=True,
        blank=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} {}'.format(self.fecha_de_datos, self.hora_datos)
