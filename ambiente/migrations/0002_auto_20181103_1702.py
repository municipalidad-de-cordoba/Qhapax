# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-11-03 20:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ambiente', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ambiente',
            old_name='ICA_Ozono',
            new_name='ICA_Ozono_1hr',
        ),
        migrations.RenameField(
            model_name='ambiente',
            old_name='Ozono',
            new_name='Ozono_1hr',
        ),
        migrations.AddField(
            model_name='ambiente',
            name='CO_8hr',
            field=models.CharField(blank=True, help_text='Unidad de medida en ppm cada 8 hr', max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='ambiente',
            name='ICA_Ozono_8hr',
            field=models.CharField(blank=True, help_text='Unidad de medida en ppm cada 8 hr', max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='ambiente',
            name='NO2_1hr',
            field=models.CharField(blank=True, help_text='Unidad de medida en ppb cada 1 hr', max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='ambiente',
            name='Ozono_8hr',
            field=models.CharField(blank=True, help_text='Unidad de medida en ppm cada 8 hr', max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='ambiente',
            name='CO',
            field=models.CharField(blank=True, help_text='Unidad de medida en ppm', max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='ambiente',
            name='NO2',
            field=models.CharField(blank=True, help_text='Unidad de medida en ppm', max_length=10, null=True),
        ),
    ]
