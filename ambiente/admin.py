from django.contrib import admin
from .models import Ambiente


class AmbienteAdmin(admin.ModelAdmin):
    list_display = (
        "fecha_de_datos",
        "hora_datos",
        "NO",
        "NO2",
        "NOx",
        "CO",
        "O3",
        "Material_particulado_total",
        "Temp_Amb",
        "Sensacion_Termica",
        "Presion_ambiental",
        "Humedad_Relativa_Ambiente",
        "Direccion_del_viento",
        "Velocidad_del_viento",
        "Radiacion_solar",
        "Radiacion_UV",
        "Lluvia_diaria",
        "Lluvia_mensual",
        "Ozono_8hr",
        "Ozono_1hr",
        "PM_10",
        "CO_8hr",
        "NO2_1hr",
        "ICA_Ozono_8hr",
        "ICA_Ozono_1hr",
        "ICA_PM_10",
        "ICA_CO",
        "ICA_NO2",
        "ICA",
        "Estado",
        "Contaminante_maximo")
    list_filter = ['fecha_de_datos', 'hora_datos', 'Estado']


admin.site.register(Ambiente, AmbienteAdmin)
