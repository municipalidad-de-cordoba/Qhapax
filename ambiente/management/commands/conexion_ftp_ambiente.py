from django.core.management.base import BaseCommand
from ambiente.local_settings import HOST, USER, PASS, FILE, DIRECTORIO
from django.db import transaction
import sys
from ftplib import FTP
import subprocess
import os


class Command(BaseCommand):
    help = """
           Comando para conectarse al FTP de la UTN y bajar csv de ambiente
           """

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Conectandose al server...'))

        try:
            ftp = FTP(HOST)
            ftp.login(USER, PASS)
            os.chdir(DIRECTORIO)
            ftp.encoding = 'ISO-8859-3'
            ftp.retrbinary("RETR " + FILE, open(FILE, 'wb').write)
            ftp.quit()
        except Exception as e:
            self.stdout.write(
                self.style.ERROR(
                    'Error conectandose al server: {}'.format(e)))
            sys.exit(1)

        diferencias = subprocess.call(
            ['diff', 'DatosMunicipalidadICA-local.csv',
             'DatosMunicipalidadICA.csv'])
        if diferencias == 0:
            self.stdout.write(self.style.SUCCESS(
                'No hay diferencias entre nuestro archivo local y el del FTP'))
        else:
            self.stdout.write(
                self.style.SUCCESS(
                    'Nuevos datos. Ejecutar ./manage importador_csv_ambiente.py --path="%s"' %
                    DIRECTORIO))
