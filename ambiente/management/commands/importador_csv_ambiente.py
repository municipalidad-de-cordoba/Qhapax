#!/usr/bin/python

from django.core.management.base import BaseCommand
from django.db import transaction
from ambiente.models import Ambiente
import sys
import csv


class Command(BaseCommand):
    help = """Comando para importar csv de ambiente"""

    def add_arguments(self, parser):
        parser.add_argument(
            '--path',
            type=str,
            help='Path del archivo CSV local',
            default='ambiente/resources/ambiente.csv')

    @transaction.atomic
    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS(
            '--- Comenzando carga, por favor espere ---'))
        fieldnames = [
            'Anio',
            'Mes',
            'Dia',
            'HoraDatos',
            'NO',
            'NO2',
            'Nox',
            'CO',
            'O3',
            'Material particulado total',
            'Temp. Amb.',
            'Sensacion Termica',
            'Presion ambiental',
            'Humedad Relativa Ambiente',
            'Direccion del viento',
            'Velocidad del viento',
            'Radiacion solar',
            'Radiacion UV',
            'Lluvia diaria',
            'Lluvia mensual',
            'Ozono 8hr',
            'Ozono 1hr',
            'PM 10',
            'CO 8hr',
            'NO2 1hr',
            'ICA Ozono 8hr',
            'ICA Ozono 1hr',
            'ICA PM 10',
            'ICA CO',
            'ICA NO2',
            'ICA',
            'Estado',
            'Contaminante maximo']
        count = 0
        path = options['path']
        with open(path, encoding="ISO-8859-3") as csvfile:
            reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                    delimiter=',', quotechar='"')

            header = reader.__next__()
            if sorted(fieldnames) != sorted(list(header.values())):
                self.stdout.write(self.style.ERROR(
                    'BAD FIELDNAMES [{}] -> [{}]'.format(sorted(list(header.values())), fieldnames)))
                sys.exit(1)

            for row in reader:

                anio = row['Anio'].strip()
                mes = row['Mes'].strip()
                dia = row['Dia'].strip()
                fecha_completa = anio + '-' + mes + '-' + dia

                hora_datos = row['HoraDatos'].strip()
                if hora_datos == '0':
                    hora_datos = "00:00"
                elif hora_datos == '1':
                    hora_datos = "01:00"
                elif hora_datos == '2':
                    hora_datos = "02:00"
                elif hora_datos == '3':
                    hora_datos = "03:00"
                elif hora_datos == '4':
                    hora_datos = "04:00"
                elif hora_datos == '5':
                    hora_datos = "05:00"
                elif hora_datos == '6':
                    hora_datos = "06:00"
                elif hora_datos == '7':
                    hora_datos = "07:00"
                elif hora_datos == '8':
                    hora_datos = "08:00"
                elif hora_datos == '9':
                    hora_datos = "09:00"
                elif hora_datos == '10':
                    hora_datos = "10:00"
                elif hora_datos == '11':
                    hora_datos = "11:00"
                elif hora_datos == '12':
                    hora_datos = "12:00"
                elif hora_datos == '13':
                    hora_datos = "13:00"
                elif hora_datos == '14':
                    hora_datos = "14:00"
                elif hora_datos == '15':
                    hora_datos = "15:00"
                elif hora_datos == '16':
                    hora_datos = "16:00"
                elif hora_datos == '17':
                    hora_datos = "17:00"
                elif hora_datos == '18':
                    hora_datos = "18:00"
                elif hora_datos == '19':
                    hora_datos = "19:00"
                elif hora_datos == '20':
                    hora_datos = "20:00"
                elif hora_datos == '21':
                    hora_datos = "21:00"
                elif hora_datos == '22':
                    hora_datos = "22:00"
                elif hora_datos == '23':
                    hora_datos = "23:00"
                elif hora_datos == '24':
                    hora_datos = "24:00"

                no = row['NO'].strip()
                no2 = row['NO2'].strip()
                nox = row['Nox'].strip()
                co = row['CO'].strip()
                o3 = row['O3'].strip()
                material = row['Material particulado total'].strip()
                temp = row['Temp. Amb.'].strip()
                sensacion = row['Sensacion Termica'].strip()
                presion = row['Presion ambiental'].strip()
                humedad = row['Humedad Relativa Ambiente'].strip()
                direccion_viento = row['Direccion del viento'].strip()
                velocidad_viento = row['Velocidad del viento'].strip()
                radiacion = row['Radiacion solar'].strip()
                radiacion_uv = row['Radiacion UV'].strip()
                lluvia_diaria = row['Lluvia diaria'].strip()
                lluvia_mensual = row['Lluvia mensual'].strip()
                ozono_8hr = row['Ozono 8hr'].strip()
                ozono_1hr = row['Ozono 1hr'].strip()
                pm_10 = row['PM 10'].strip()
                co_8hr = row['CO 8hr'].strip()
                no2_1hr = row['NO2 1hr'].strip()
                ica_ozono_8hr = row['ICA Ozono 8hr'].strip()
                ica_ozono_1hr = row['ICA Ozono 1hr'].strip()
                ica_pm_10 = row['ICA PM 10'].strip()
                ica_co = row['ICA CO'].strip()
                ica_no2 = row['ICA NO2'].strip()
                ica = row['ICA'].strip()
                estado = row['Estado'].strip()
                if estado == 'Bueno':
                    estado = Ambiente.BUENO
                elif estado == 'Moderado':
                    estado = Ambiente.MODERADO
                elif estado == 'Insalubre para personas sensibles':
                    estado = Ambiente.INSALUBRE
                elif estado == 'Peligroso':
                    estado = Ambiente.PELIGROSO
                contaminante_maximo = row['Contaminante maximo'].strip()

                ambiente, created = Ambiente.objects.get_or_create(
                    fecha_de_datos=fecha_completa, hora_datos=hora_datos,
                    NO=no, NO2=no2, NOx=nox, CO=co, O3=o3, Material_particulado_total=material,
                    Temp_Amb=temp, Sensacion_Termica=sensacion, Presion_ambiental=presion,
                    Humedad_Relativa_Ambiente=humedad, Direccion_del_viento=direccion_viento,
                    Velocidad_del_viento=velocidad_viento, Radiacion_solar=radiacion,
                    Radiacion_UV=radiacion_uv, Lluvia_diaria=lluvia_diaria, Lluvia_mensual=lluvia_mensual,
                    Ozono_8hr=ozono_8hr, Ozono_1hr=ozono_1hr, PM_10=pm_10, CO_8hr=co_8hr,
                    NO2_1hr=no2_1hr, ICA_Ozono_8hr=ica_ozono_8hr, ICA_Ozono_1hr=ica_ozono_1hr,
                    ICA_PM_10=ica_pm_10, ICA_CO=ica_co, ICA_NO2=ica_no2, ICA=ica, Estado=estado,
                    Contaminante_maximo=contaminante_maximo)

                if created:
                    count += 1

        self.stdout.write("Total de datos grabados: {}".format(count))
