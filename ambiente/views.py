from django.contrib.auth.decorators import login_required
from .models import Ambiente
from django.views.decorators.cache import cache_page
import django_excel as excel


@cache_page(60 * 60 * 12)  # 12 h
@login_required
def lista_ambiente(request, filetype):
    '''
    Planilla de ambiente
    http://localhost:8000/ambiente/lista-de-ambiente.xls
    '''

    ambiente = Ambiente.objects.all()
    csv_list = []

    csv_list.append(['Fecha de datos',
                     'Hora datos',
                     'NO',
                     'NO2',
                     'NOx',
                     'CO',
                     'O3',
                     'Material particulado total',
                     'Temp Amb',
                     'Sensacion termica',
                     'Presion ambiental',
                     'Humedad relativa ambiente',
                     'Direccion del viento',
                     'Velocidad del viento',
                     'Radiacion solar',
                     'Radiacion UV',
                     'Lluvia diaria',
                     'Lluvia mensual',
                     'Ozono 8hr',
                     'Ozono 1hr',
                     'PM 10',
                     'CO 8hr',
                     'NO2 1hr',
                     'ICA Ozono 8hr',
                     'ICA Ozono 1hr',
                     'ICA PM 10',
                     'ICA CO',
                     'ICA NO2',
                     'ICA',
                     'Estado',
                     'Contaminante maximo',
                     'Fecha de creacion',
                     'Fecha ultima modificacion'])

    for a in ambiente:
        csv_list.append([
            a.fecha_de_datos,
            a.hora_datos,
            a.NO,
            a.NO2,
            a.NOx,
            a.CO,
            a.O3,
            a.Material_particulado_total,
            a.Temp_Amb,
            a.Sensacion_Termica,
            a.Presion_ambiental,
            a.Humedad_Relativa_Ambiente,
            a.Direccion_del_viento,
            a.Velocidad_del_viento,
            a.Radiacion_solar,
            a.Radiacion_UV,
            a.Lluvia_diaria,
            a.Lluvia_mensual,
            a.Ozono_8hr,
            a.Ozono_1hr,
            a.PM_10,
            a.CO_8hr,
            a.NO2_1hr,
            a.ICA_Ozono_8hr,
            a.ICA_Ozono_1hr,
            a.ICA_PM_10,
            a.ICA_CO,
            a.ICA_NO2,
            a.ICA,
            a.get_Estado_display(),
            a.Contaminante_maximo,
            # Formateamos las fechas de la forma dia/mes/año
            # horas:minutos:segundos
            '{}/{}/{} {}:{}:{}'.format(a.creado.day, a.creado.month,
                                       a.creado.year, a.creado.hour,
                                       a.creado.minute, a.creado.second),
            '{}/{}/{} {}:{}:{}'.format(a.ultima_modificacion.day,
                                       a.ultima_modificacion.month,
                                       a.ultima_modificacion.year,
                                       a.ultima_modificacion.hour,
                                       a.ultima_modificacion.minute,
                                       a.ultima_modificacion.second)
        ])

    return excel.make_response(excel.pe.Sheet(csv_list), filetype)
