# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2017-03-21 19:02
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('softwaremunicipal', '0004_auto_20161102_1609'),
        ('redbusdata', '0006_auto_20170321_1210'),
    ]

    operations = [
        migrations.CreateModel(
            name='AppRedBus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('version_minima', models.CharField(max_length=250)),
                ('txt_sino_hay_version_minima', models.TextField(default='Tu versión de esta aplicación ha quedado obsleta. Actualízala para poder continuar')),
                ('version_recomendada', models.CharField(max_length=250)),
                ('txt_sino_hay_version_recomendada', models.TextField(default='Te recomendamos actualizar esta aplicación, esta por quedar obsoleta')),
                ('app', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='softwaremunicipal.AppMovil')),
            ],
        ),
    ]
