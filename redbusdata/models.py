"""
Datos de las tarjetas de RedBus que los usuarios de nuestra app.
Guarda datos de las tarjetas y los saldos. App a fines de conocer más
sobre el uso de estas tarjetas por parte de los vecinos.
"""
from django.db import models
# reuso los datos de las aplicaciones de la muni para las apps que usan estos datos
from softwaremunicipal.models import AppMovil
from ckeditor.fields import RichTextField


class TarjetaRedBus(models.Model):
    '''
    Cada una de las tarjetas RedBus identificadas por su código en la pate posterior de la misma
    '''
    codigo = models.CharField(max_length=250, help_text="Código identificador impreso en la parte posterior de la tarjeta")
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return "RedBus {}".format(self.codigo)

    class Meta:
        verbose_name_plural = "Tarjetas RedBus"
        ordering = ['-id']


class NombreTarjetaRedBus(models.Model):
    """
    Cada usuario del sistema nombra a las tarjetas que usa
    Una tarjeta puede ser revisada por otros usuarios
    """
    uid = models.CharField(max_length=250,
        help_text="Identificador único del usuario (en nuestro caso, UID de la app Cuanto Tengo)")
    tarjeta = models.ForeignKey(TarjetaRedBus)
    nombre = models.CharField(max_length=250, help_text="Nombre que el usuario le dio para recordar")
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return "RedBus Nombre {} ({})".format(self.nombre, self.tarjeta.codigo)

    class Meta:
        unique_together = (("uid", "tarjeta"),)
        verbose_name_plural = "Nombres de Tarjetas RedBus"
        ordering = ['-id']


class SaldoTarjetaRedBus(models.Model):
    '''
    Cada una de las consultas de saldo de los usuarios del sistema
    '''
    # desde donde se pidio (para despues poder mostrarle historial)
    nombre_tarjeta = models.ForeignKey(NombreTarjetaRedBus)
    momento_consulta = models.DateTimeField(auto_now=False, auto_now_add=True, help_text="Momento en que se consulto el servidor de redbus")
    momento_dato = models.DateTimeField(auto_now=False, auto_now_add=False, help_text="Fecha y hora del dato indicado por redbus")
    saldo = models.FloatField()
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return "RedBus {} {} {}".format(self.nombre_tarjeta.tarjeta.codigo, self.momento_dato, self.saldo)

    class Meta:
        verbose_name_plural = "Saldos de Tarjetas RedBus"
        ordering = ['-id']


class ErrorLecturaSaldoRedBus(models.Model):
    """ Registro de errores al tratar de acceder al saldo de una tarjeta RedBus """
    nombre_tarjeta = models.ForeignKey(NombreTarjetaRedBus)
    momento_consulta = models.DateTimeField(auto_now=False, auto_now_add=True, help_text="Momento en que se consulto el servidor de redbus")
    error_redbus_code = models.PositiveIntegerField(default=0, null=True)
    error_code = models.PositiveIntegerField(default=0, null=True)
    error_details = models.TextField(blank=True, null=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return "RedBus ERROR {} {} {}".format(self.nombre_tarjeta.tarjeta.codigo, self.error_code, self.error_details)

    class Meta:
        verbose_name_plural = "Errores de lecturas de saldos Tarjetas RedBus"
        ordering = ['-id']


class AppRedBus(models.Model):
    """
    aplicaciones conectadas a estos datos (derivado de las apps municipales en general)
    aqui se informa cuales son las versiones activas y recomendadas.
        Ahora muchas de estas propiedades ya son parte de la clase AppMovil, aqui quedaron duplicadas
        Podrían migrarse #FIXME
    """
    app = models.ForeignKey(AppMovil)
    # datos de la app que puedan modificarse desde esta base de datos y entregados como webservice para evitar recompilaciones solo por texto
    terminos_y_condiciones = RichTextField(null=True, blank=True, help_text="Texto para la sección de la APP Terminos y Condiciones")
    politica_de_privacidad = RichTextField(null=True, blank=True, help_text="Texto para la sección de la APP Política de privacidad")
    about_app = RichTextField(null=True, blank=True, help_text="Texto para la sección de la APP Acerca de")
    about_data = RichTextField(null=True, blank=True, help_text="Texto para la sección con datos del saldo. Info del proveedor")

    # datos para anuncios en la APP
    # si el cliente es menor que lo indicado acá la app no debería funcionar enviando un mensaje
    version_minima = models.CharField(max_length=250)
    txt_sino_hay_version_minima = models.TextField(default="Tu versión de esta aplicación ha quedado obsleta. Actualízala para poder continuar")
    # si el cliente no tiene la última versión y quiero avisarle que debe actualizar
    version_recomendada = models.CharField(max_length=250)
    txt_sino_hay_version_recomendada = models.TextField(default="Te recomendamos actualizar esta aplicación, esta por quedar obsoleta")
