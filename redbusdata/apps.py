from django.apps import AppConfig


class RedbusdataConfig(AppConfig):
    name = 'redbusdata'
