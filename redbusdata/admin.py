from django.contrib import admin
from .models import *


class TarjetaRedBusAdmin(admin.ModelAdmin):
    list_display = ('codigo', 'creado')
    search_fields = ['codigo']


class NombreTarjetaRedBusAdmin(admin.ModelAdmin):

    def codigo_tarjeta(self, obj):
        return obj.tarjeta.codigo

    list_display = ('uid', 'codigo_tarjeta', 'nombre', 'creado')
    search_fields = ['uid', 'tarjeta__codigo', 'nombre']


class SaldoTarjetaRedBusAdmin(admin.ModelAdmin):

    def nombre_tarjeta(self, obj):
        return obj.nombre_tarjeta.nombre

    def codigo_tarjeta(self, obj):
        return obj.nombre_tarjeta.tarjeta.codigo

    list_display = ('saldo', 'codigo_tarjeta', 'nombre_tarjeta', 'momento_consulta', 'momento_dato', 'creado')


class ErrorLecturaSaldoRedBusAdmin(admin.ModelAdmin):

    def nombre_tarjeta(self, obj):
        return obj.nombre_tarjeta.nombre

    def codigo_tarjeta(self, obj):
        return obj.nombre_tarjeta.tarjeta.codigo

    list_display = ('error_redbus_code', 'error_code', 'error_details', 'nombre_tarjeta', 'momento_consulta', 'creado')


class AppRedBusAdmin(admin.ModelAdmin):

    def app_titulo(self, obj):
        return obj.app.titulo

    list_display = ('app_titulo', 'version_minima', 'txt_sino_hay_version_minima', 'version_recomendada', 'txt_sino_hay_version_recomendada')


admin.site.register(TarjetaRedBus, TarjetaRedBusAdmin)
admin.site.register(NombreTarjetaRedBus, NombreTarjetaRedBusAdmin)
admin.site.register(SaldoTarjetaRedBus, SaldoTarjetaRedBusAdmin)
admin.site.register(ErrorLecturaSaldoRedBus, ErrorLecturaSaldoRedBusAdmin)
admin.site.register(AppRedBus, AppRedBusAdmin)
