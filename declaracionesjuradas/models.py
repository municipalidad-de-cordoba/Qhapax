from django.db import models
from django.conf import settings
from decimal import Decimal
from core.models import Persona, Moneda


class DeclaracionJurada(models.Model):
    """
    Una declaración jurada presentada por una persona en un año específico
    """
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    anio = models.PositiveIntegerField(default=2017)
    # PDF o imagen escaneada de la declaración
    archivo = models.FileField(
        upload_to='declaraciones-juradas/',
        null=True,
        blank=True)
    publicada = models.BooleanField(default=True)
    observaciones = models.TextField(null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    # para saber si ya fue exportado y cuando
    id_portal_de_datos = models.PositiveIntegerField(
        default=0, help_text='ID de esta DDJJ en el portal de datos abiertos')
    ultima_exportacion_portal_de_datos = models.DateTimeField(
        null=True, blank=True, help_text='Ultima vez que se exporto exitosamente al portal')

    def get_url_pdf(self):
        if self.archivo:
            return '{}://{}{}'.format(settings.PROTOCOLO_PRODUCCION,
                                      settings.DOMINIO_PRODUCCION, self.archivo.url)

    def __str__(self):
        return '{} {}'.format(self.anio, self.persona)

    def bienes(self):
        return self.bien_set.all()

    def deudas(self):
        return self.deuda_set.all()

    @property
    def total_bienes(self):
        """
        Monto total expresado en la moneda definida en
        settings.MONEDA_PK_PRINCIPAL
        """
        mi_moneda = Moneda.objects.get(pk=settings.MONEDA_PK_PRINCIPAL)
        bienes = Decimal(0.0)
        for bien in self.bienes():
            if bien.moneda == mi_moneda:
                bienes += bien.valor
            else:
                # pasar a dolares y luego a la moneda de referencia local
                # (mi_moneda)
                moneda = bien.moneda
                valor_en_mi_moneda = bien.valor * \
                    moneda.cantidad_por_dolar * mi_moneda.cantidad_por_dolar
                bienes += valor_en_mi_moneda
        return bienes

    @property
    def total_deudas(self):
        """
        Monto total expresado en la moneda definida en
        settings.MONEDA_PK_PRINCIPAL
        """
        mi_moneda = Moneda.objects.get(pk=settings.MONEDA_PK_PRINCIPAL)

        deudas = Decimal(0.0)
        for deuda in self.deudas():
            if deuda.moneda == mi_moneda:
                deudas += deuda.valor
            else:
                # pasar a dolares y luego a la moneda de referencia local
                # (mi_moneda)
                moneda = deuda.moneda
                deuda_en_mi_moneda = deuda.valor * \
                    moneda.cantidad_por_dolar * mi_moneda.cantidad_por_dolar
                deudas += deuda_en_mi_moneda

        return deudas

    @property
    def total(self):
        """
        Monto total expresado en la moneda definida en
        settings.MONEDA_PK_PRINCIPAL
        """
        return self.total_bienes - self.total_deudas

    class Meta:
        ordering = ['-anio']
        unique_together = (("persona", "anio"),)
        verbose_name = "Declaración Jurada"
        verbose_name_plural = "Declaraciones Juradas"


class Deuda(models.Model):
    """
    Deudas del funcionario en el pais o en el exterior
    En general los valores deben ser expresados en pesos argentinos
    """
    declaracion_jurada = models.ForeignKey(
        DeclaracionJurada, on_delete=models.CASCADE, null=True)
    en_el_pais = models.NullBooleanField(default=True, null=True)
    con_entidad_financiera = models.NullBooleanField(
        default=True, null=True)  # False = con otras personas o entidades
    # saber si es un bien de la persona o de su cónyuge
    es_de_conyuge = models.BooleanField(default=False)
    moneda = models.ForeignKey(Moneda, on_delete=models.CASCADE)
    valor = models.DecimalField(max_digits=14, decimal_places=2)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'Deuda {} {}'.format(self.moneda.simbolo, self.valor)


class TipoBien(models.Model):
    """
    Cada uno de los tipos de bienes que se pueden declara
    Inmuebles, automoviles, semovientes, participación en sociedades, etc
    """
    nombre = models.CharField(max_length=75)

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']


class Bien(models.Model):
    """
    Bienes del funcionario
    """
    declaracion_jurada = models.ForeignKey(
        DeclaracionJurada, on_delete=models.CASCADE, null=True)
    tipo = models.ForeignKey(TipoBien, on_delete=models.CASCADE)
    en_el_pais = models.NullBooleanField(default=True, null=True)
    # saber si es un bien de la persona o de su cónyuge
    es_de_conyuge = models.BooleanField(default=False)
    moneda = models.ForeignKey(Moneda, on_delete=models.CASCADE)
    valor = models.DecimalField(max_digits=14, decimal_places=2)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'Bien {} {} {}'.format(
            self.tipo, self.moneda.simbolo, self.valor)
