#!/usr/bin/python
from django.core.management.base import BaseCommand
from django.db import transaction
import datetime
from django.urls import reverse
from declaracionesjuradas.models import DeclaracionJurada
from portaldedatos.models import (Portal, DatoPortal, VersionDato,
                                  CategoriaPortalDatos, LicenciaDato,
                                  TipoDato, ArchivoPDF, ArchivoXLS, ArchivoCSV)


class Command(BaseCommand):
    help = """Comando para exportar (sincronizar todas) DDJJ al portal como
              datos abiertos con versiones y recursos """

    def add_arguments(self, parser):
        parser.add_argument(
            '--categoria_id',
            nargs='?',
            default=0,
            type=int,
            help='Id de la categoria donde cargar todo, predeterminado crea una.')
        parser.add_argument(
            '--licencia_id',
            nargs='?',
            default=0,
            type=int,
            help='Id de la categoria donde cargar todo, predeterminado crea una.')
        parser.add_argument(
            '--anio',
            nargs='?',
            default=0,
            type=int,
            help='Año de las DDJJ a exportar')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS(
            'Iniciando sincronizacion de DDJJ al portal de datos'))
        # definir en que categoría y portal van a publicarse
        categoria_id = options['categoria_id']
        if categoria_id == 0:
            # tomar el primer portal
            portal = Portal.objects.get(pk=1)
            categoria, created = CategoriaPortalDatos.objects.get_or_create(
                portal=portal, nombre='Declaraciones Juradas de Funcionarios')
        else:
            categoria = CategoriaPortalDatos.objects.get(pk=categoria_id)

        # ver que licencia tendran los datos
        licencia_id = options['licencia_id']
        licencia = LicenciaDato.objects.get(pk=licencia_id)
        anio = options['anio']

        # ver el tipo de los datos a referencias
        tipo_archivo, created = TipoDato.objects.get_or_create(
            tipo=TipoDato.ARCHIVO, subtipo="Web")

        # lista de DDJJ a exportar
        if anio == 0:
            ddjjs = DeclaracionJurada.objects.all()
        else:
            ddjjs = DeclaracionJurada.objects.filter(anio=anio)

        datos_nuevos = 0
        datos_existentes = 0

        versiones_nuevas = 0
        versiones_existentes = 0

        for dj in ddjjs:
            # el ID externo del dato es el de la persona (unica) y el
            # id_externo de las versiones es el de las DDJJ que
            cod_app_externa = 'DDJJ_{}'.format(dj.persona.__class__.__name__)
            # no puedo get or create porque requiere categoria y quiere evitar
            # duplicados si se pasa de nuevo el script apuntando a otra
            # categoria
            datos = DatoPortal.objects.filter(
                id_externo=dj.persona.id,
                cod_app_externa=cod_app_externa)
            if len(datos) > 0:
                dato = datos[0]
                created = False
            else:
                created = True
                titulo = 'Declaraciones Juradas de {} (cod {})'.format(
                    dj.persona.nombrepublico, dj.persona.id)
                self.stdout.write(
                    self.style.SUCCESS(
                        'Titulo {}'.format(titulo)))
                dato = DatoPortal.objects.create(
                    titulo=titulo,
                    categoria=categoria,
                    id_externo=dj.persona.id,
                    cod_app_externa=cod_app_externa)

            if created:
                datos_nuevos += 1
            else:
                datos_existentes += 1

            self.stdout.write(
                self.style.SUCCESS(
                    'Dato externo {} {}'.format(
                        cod_app_externa,
                        dj.persona.id)))
            if dj.publicada:
                dato.estado = DatoPortal.PUBLICADO
            else:
                dato.estado = DatoPortal.NO_PUBLICADO

            # si se define otra categoria no se duplican (por eso no esta en el
            # get or create del dato), se mueven:
            dato.categoria = categoria
            dato.titulo = 'Declaraciones Juradas de {}'.format(
                dj.persona.nombrepublico)
            dato.periodicidad = DatoPortal.ANUAL
            dato.html = '<p>Declaraciones Juradas de Bienes de {}.</p>'.format(
                dj.persona.nombrepublico)

            dato.save()

            # grabar esta version específica del dato (cada año de ddjj es una
            # version de esta serie-dato)
            # aca es DDJJ sola, una DDJJ es una version
            cod_app_externa = dj.__class__.__name__
            version, created = VersionDato.objects.get_or_create(
                dato=dato, id_externo=dj.id, cod_app_externa=cod_app_externa)

            if created:
                versiones_nuevas += 1
            else:
                versiones_existentes += 1

            version.titulo = 'Declaracion Jurada {}'.format(dj.anio)
            version.fecha = dj.created if dj.anio >= 2017 else datetime.date(
                dj.anio, 3, 1)
            version.save()

            # agregar todos los recursos
            pdf, created = ArchivoPDF.objects.get_or_create(
                version_dato=version, id_externo=dj.id, cod_app_externa=cod_app_externa, tipo=tipo_archivo)
            pdf.titulo = 'PDF DDJJ {} {}'.format(
                dj.anio, dj.persona.nombrepublico)
            pdf.licencia = licencia
            pdf.icon = 'pdf'

            pdf.url_externa = dj.get_url_pdf()
            pdf.dinamico = True  # para que no lo descargue de nuevo
            pdf.solo_imagenes = False
            pdf.save()

            # Version Excel
            xls, created = ArchivoXLS.objects.get_or_create(
                version_dato=version, id_externo=dj.id, cod_app_externa=cod_app_externa, tipo=tipo_archivo)
            xls.titulo = 'Excel DDJJ {} {}'.format(
                dj.anio, dj.persona.nombrepublico)
            xls.licencia = licencia
            xls.icon = 'xls'

            url_xls = reverse(
                'portaldedatos.declaraciones_juradas.declaracion_jurada',
                kwargs={
                    'funcionario': dj.persona.slug,
                    'anio': dj.anio,
                    'dj_id': dj.id,
                    'filetype': 'xls'})
            xls.url_externa = url_xls
            xls.dinamico = True  # para que no lo descargue de nuevo

            xls.save()

            # Version CSV
            csv, created = ArchivoCSV.objects.get_or_create(
                version_dato=version, id_externo=dj.id, cod_app_externa=cod_app_externa, tipo=tipo_archivo)
            csv.titulo = 'CSV DDJJ {} {}'.format(
                dj.anio, dj.persona.nombrepublico)
            csv.licencia = licencia
            csv.icon = 'csv'

            url_csv = reverse(
                'portaldedatos.declaraciones_juradas.declaracion_jurada',
                kwargs={
                    'funcionario': dj.persona.slug,
                    'anio': dj.anio,
                    'dj_id': dj.id,
                    'filetype': 'csv'})
            csv.url_externa = url_csv
            csv.dinamico = True  # para que no lo descargue de nuevo

            csv.save()

        self.stdout.write(self.style.SUCCESS('***********************'))
        self.stdout.write(
            self.style.SUCCESS(
                'Datos nuevos: {}, preexistentes: {}. Versiones nuevas {}, versiones existentes {}'.format(
                    datos_nuevos,
                    datos_existentes,
                    versiones_nuevas,
                    versiones_existentes)))
        self.stdout.write(self.style.SUCCESS('********* FIN *********'))
