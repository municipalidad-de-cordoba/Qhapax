"""
comprimir declaraciones juradas de todo un año para descarga y certificacion
"""
# !/usr/bin/python
from django.core.management.base import BaseCommand
from django.conf import settings
from django.db import transaction
from declaracionesjuradas.models import DeclaracionJurada
from portaldedatos.models import Recurso, ArchivoPDF
import zipfile
import os
from blockchain.models import (DocumentoCertificadoBlockChain,
                               IntermediarioBlockChain, HashFunction)


class Command(BaseCommand):
    help = """Comando para comprimir las DDJJs"""

    def add_arguments(self, parser):
        parser.add_argument(
            '--anio',
            nargs='?',
            default=2018,
            type=int,
            help='Año de las DDJJ a comprimir')
        parser.add_argument(
            '--preparar_blockchain',
            nargs='?',
            default=False,
            type=bool,
            help='Indica si se comienza un registro de docuemnto en blockchain')

    @transaction.atomic
    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Iniciando compresion de DDJJ'))
        # se requiere que este creada la categoria (en el comando exportar_ddjj_a_datos se crea)
        # categoria = CategoriaPortalDatos.objects.get(portal=portal, nombre='Declaraciones Juradas de Funcionarios')
        anio = options['anio']
        # lista de DDJJ a comprimir
        ddjjs = DeclaracionJurada.objects.filter(anio=anio, publicada=True)
        self.stdout.write(
            self.style.SUCCESS(
                'Se encontraron {} DDJJs'.format(
                    len(ddjjs))))

        archivo_comprimido_name = 'ddjj_{}.zip'.format(anio)
        archivo_comprimido_path = os.path.join(
            settings.MEDIA_ROOT, archivo_comprimido_name)

        archivo_comprimido = zipfile.ZipFile(archivo_comprimido_path, 'w')
        c = 1
        recursos = []  # recursos en el portal de datos
        for dj in ddjjs:
            self.stdout.write(
                self.style.SUCCESS(
                    'Agregando DDJJ ({}) {} de {} del {}'.format(
                        c,
                        dj.archivo.name,
                        dj.persona.nombrepublico,
                        dj.anio)))
            if dj.archivo:

                if os.path.isfile(dj.archivo.path):
                    archivo_comprimido.write(
                        dj.archivo.path,
                        dj.archivo.name,
                        compress_type=zipfile.ZIP_DEFLATED)
                    # agregarlo como recurso incluido
                    cod_app_externa = dj.__class__.__name__
                    recurso_pdf = ArchivoPDF.objects.get(
                        id_externo=dj.id, cod_app_externa=cod_app_externa)
                    recurso = Recurso.objects.get(pk=recurso_pdf.id)
                    recursos.append(recurso)
                else:
                    self.stdout.write(
                        self.style.ERROR(
                            'Archivo ROTO: {}. {} de {}'.format(
                                dj.archivo.path,
                                dj.persona.nombrepublico,
                                dj.anio)))
                    continue
            else:
                self.stdout.write(
                    self.style.ERROR(
                        'DDJJ SIN ARCHIVO: {} de {}'.format(
                            dj.persona.nombrepublico,
                            dj.anio)))

            c += 1

        # ejemplo de donde las paso despues
        # http://gobiernoabierto.cordoba.gob.ar/media/ddjj_2017.zip
        archivo_comprimido.close()

        preparar_blockchain = options['preparar_blockchain']
        if preparar_blockchain:
            self.stdout.write(self.style.SUCCESS('Preparando DOC Blockchain'))

            nombre = 'TODAS las DDJJ {} de funcionarios'.format(anio)
            recursos_incluidos = recursos

            intermediario = IntermediarioBlockChain.objects.get(
                pk=1)  # Signatura

            funcion_hash = HashFunction.objects.get(pk=2)  # sha256
            # resultado_hash = 'obtener hash del archivop'
            # sistema_firma = ECDSA

            info_extra_verificacion = 'El documento usa OpenTimeStamps por lo que requiere usar sus herramientas de verificación o tomar su código fuente abierto para construir una propia. Se adjunta OTS de verificación'

            doc, created = DocumentoCertificadoBlockChain.objects.get_or_create(
                nombre=nombre, funcion_hash=funcion_hash, resultado_hash='calculando ...', )
            self.stdout.write(
                self.style.SUCCESS(
                    'Creando DOC Blockchain: {}'.format(created)))
            doc.recursos_incluidos = recursos_incluidos
            doc.intermediario = intermediario
            doc.info_extra_verificacion = info_extra_verificacion
            doc.publicado = False
            doc.archivo_comprimido.name = archivo_comprimido_name
            doc.save()

        self.stdout.write(self.style.SUCCESS('***********************'))
        self.stdout.write(self.style.SUCCESS('********* FIN *********'))
