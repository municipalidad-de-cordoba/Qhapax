from django.contrib import admin
from django.core.urlresolvers import reverse
from .models import Bien, DeclaracionJurada, Deuda, TipoBien


class DeudaInline(admin.StackedInline):
    model = Deuda
    extra = 1


class BienInline(admin.StackedInline):
    model = Bien
    extra = 1


class DeclaracionJuradaAdmin(admin.ModelAdmin):
    inlines = [BienInline, DeudaInline]
    search_fields = [
        'persona__nombre',
        'persona__apellido',
        'persona__unique_id',
        'anio']
    exclude = ('id_portal_de_datos', 'ultima_exportacion_portal_de_datos')

    def ficha_funcionario(self, obj):
        ficha_url = reverse(
            'website.funcionarios.persona',
            kwargs={
                'persona': obj.persona.slug,
                'persona_id': obj.persona.id})
        return '<a target="_blank" href="{}">{} {}</a>'.format(
            ficha_url, obj.persona.nombre, obj.persona.apellido)
    ficha_funcionario.allow_tags = True

    list_display = (
        'persona',
        'anio',
        'ficha_funcionario',
        'publicada',
        'observaciones')
    list_filter = ('anio', )


admin.site.register(DeclaracionJurada, DeclaracionJuradaAdmin)
admin.site.register(Deuda)
admin.site.register(TipoBien)
admin.site.register(Bien)
