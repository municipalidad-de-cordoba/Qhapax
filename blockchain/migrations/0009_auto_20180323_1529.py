# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-23 18:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blockchain', '0008_documentocertificadoblockchain_uid'),
    ]

    operations = [
        migrations.CreateModel(
            name='SmartContract',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(help_text='Nombre que le damos al Contrato', max_length=120)),
                ('url_tecnico', models.URLField(blank=True, help_text='Link al contrato en el blockchain', null=True)),
                ('observaciones_publicas', models.TextField(blank=True, help_text='Detalles del contrato comprensibles para el público', null=True)),
                ('creado', models.DateTimeField(auto_now_add=True)),
                ('ultima_modificacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['nombre'],
            },
        ),
        migrations.CreateModel(
            name='Wallet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(help_text='Nombre que le damos al Wallet', max_length=120)),
                ('direccion', models.CharField(help_text='Direccion o ID único de la Wallet', max_length=240)),
                ('observaciones_publicas', models.TextField(blank=True, null=True)),
                ('creado', models.DateTimeField(auto_now_add=True)),
                ('ultima_modificacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['nombre'],
            },
        ),
        migrations.AlterField(
            model_name='documentocertificadoblockchain',
            name='archivo_comprimido',
            field=models.FileField(blank=True, help_text='Archivo final si hubiera un comprido que agrupe a otros', upload_to='documentos-certificados/'),
        ),
        migrations.AlterField(
            model_name='documentocertificadoblockchain',
            name='url_verificacion',
            field=models.URLField(blank=True, help_text='URL sugerida para verificar este documento', null=True),
        ),
        migrations.AlterField(
            model_name='intermediarioblockchain',
            name='url_verificacion_general',
            field=models.URLField(blank=True, help_text='URL sugerida para verificar transacciones del intermediario', null=True),
        ),
        migrations.AddField(
            model_name='wallet',
            name='intermediario',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='blockchain.IntermediarioBlockChain'),
        ),
        migrations.AddField(
            model_name='smartcontract',
            name='intermediario',
            field=models.ForeignKey(blank=True, help_text='Intermediario que definio o acompaña el contrato', null=True, on_delete=django.db.models.deletion.CASCADE, to='blockchain.IntermediarioBlockChain'),
        ),
        migrations.AddField(
            model_name='transaccion',
            name='smart_contract',
            field=models.ForeignKey(blank=True, help_text='En caso de que la transaccion se haga vinculada a un Smart Contract', null=True, on_delete=django.db.models.deletion.CASCADE, to='blockchain.SmartContract'),
        ),
        migrations.AddField(
            model_name='transaccion',
            name='wallet',
            field=models.ForeignKey(blank=True, help_text='En caso de que la transaccion se haga desde un wallet', null=True, on_delete=django.db.models.deletion.CASCADE, to='blockchain.Wallet'),
        ),
    ]
