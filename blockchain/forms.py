from django import forms
from portaldedatos.forms import RecursoWidget
from .models import DocumentoCertificadoBlockChain


class DocumentoCertificadoBlockChainForm(forms.ModelForm):

    class Meta:
        model = DocumentoCertificadoBlockChain
        fields = '__all__'
        widgets = {
            'recursos_incluidos': RecursoWidget
        }
