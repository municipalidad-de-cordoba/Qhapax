from django.conf.urls import url
from . import views


urlpatterns = [url(r'^$',
                   views.blockchain,
                   name='blockchain.index'),
               url(r'^documento-en-blockchain/(?P<uid>[\w-]+)$',
                   views.documento_en_blockchain,
                   name='blockchain.documento'),
               ]
