from django.contrib import admin
from .models import (BlockChain, IntermediarioBlockChain, Bloque,
                     ProveedoresReplicaDatos, HashFunction, ReplicaDatos,
                     DocumentoCertificadoBlockChain, Wallet, SmartContract)
from .forms import DocumentoCertificadoBlockChainForm


@admin.register(BlockChain)
class BlockChainAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'url', 'publicado']
    search_fields = ['nombre']


@admin.register(IntermediarioBlockChain)
class IntermediarioBlockChainAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'organizacion', 'url', 'publicado']
    search_fields = ['nombre']


@admin.register(Bloque)
class BloqueAdmin(admin.ModelAdmin):
    list_display = ['blockchain', 'id_bloque', 'url_bloque', 'publicado']
    search_fields = ['id_bloque']


@admin.register(ProveedoresReplicaDatos)
class ProveedoresReplicaDatosAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'url', 'publicado']
    search_fields = ['nombre']


class ReplicaDatosInLine(admin.StackedInline):
    model = ReplicaDatos
    extra = 1


@admin.register(HashFunction)
class HashFunctionAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'url', 'publicado']
    search_fields = ['nombre']
    # list_filter = []


@admin.register(DocumentoCertificadoBlockChain)
class DocumentoCertificadoBlockChainAdmin(admin.ModelAdmin):
    form = DocumentoCertificadoBlockChainForm
    list_display = [
        'nombre',
        'publicado',
        'funcion_hash',
        'intermediario',
        'sistema_firma',
        'wallet',
        'bloque',
        'smart_contract']
    search_fields = ['nombre', 'resultado_hash', 'transaccion']
    list_filter = [
        'firmantes',
        'funcion_hash',
        'intermediario',
        'sistema_firma',
        'wallet']
    exclude = ['uid', 'creado', 'ultima_modificacion']
    inlines = [ReplicaDatosInLine]

    # para que ande el select2
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )


@admin.register(Wallet)
class WalletAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'intermediario', 'direccion', 'publicado']
    search_fields = ['nombre', 'direccion']
    list_filter = ['intermediario']


@admin.register(SmartContract)
class SmartContractAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'intermediario', 'url_tecnico']
    search_fields = ['nombre']
    list_filter = ['intermediario']
