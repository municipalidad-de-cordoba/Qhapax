Blockchain
==========

Qhapax permite documentar recursos de datos que hayan sido *subidos* a Blockchain.
Esto permite que usuarios externos de los datos puedan certificar que están utilizando datos que son iguales a los que el gobierno ha liberado.

La forma de *subir* los datos es:

 - Agrupar más de un recurso en un archivo comprimido
 - Opcionalmente *firmar* el documento con alguna firma digital registrada (cuya clave pública se libera)
 - Obtener el *hash* (documentando la función usada) de ese archivo (firmado o no) y subirlo a algún blockchain.
 - Tomar el identificador de esta transacción y URL de verificación para poner a disposición de los interesados.

Validación por parte de los usuarios externos
---------------------------------------------

La utilidad de registrar datos en blockchain es permitir a organizaciones externas
reutilizar datos del municipio certificando su autenticidad AUN en casos de que el gobierno
modifique o elimine (por error o maliciosamente) datos que haya liberado en su portal.

Los usuarios externos al gobierno interesados en trabajar con datos y certificar que son datos oficiales deberán tener en cuenta:

 - Que deben guardar y alojar copia del documento a utilizar.
 - En caso de usarse deberán guardar copia de la clave pública de la firma utilizada.
 - Que deben conocer la función *hash* utilizada para poder replicar sobre el documento.
 - Que deben guardar la URL que muestra la transacción en blockchain coincidente con el hash y la firma digital (cuando exista).


 Otros casos similares
 ---------------------

 El Gobierno Nacional Argentino `certifica sus Boletines Oficiales en
 Blockchain <https://www.boletinoficial.gob.ar/#!estatica/certificacionBlockchain>`__ desde julio de 2017.
 La Comisión Nacional de Energía de Chile utiliza Blockchain para certificar inviolabilidad de los
 datos `<https://www.cne.cl/prensa/prensa-2018/febrero-2018/comision-nacional-de-energia-se-convertira-en-la-primera-entidad-publica-en-utilizar-blockchain-en-chile/>`__
