from django.db import models
from firma_digital.models import FirmaDigital, SistemaFirma
from portaldedatos.models import Recurso
from organizaciones.models import Organizacion
from django.urls import reverse
import uuid


class BlockChain(models.Model):
    ''' cadena de bloques que puede registrar transacciones '''
    nombre = models.CharField(max_length=90)
    url = models.URLField(
        null=True,
        blank=True,
        help_text='Url con info de la cadena')
    publicado = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre


class IntermediarioBlockChain(models.Model):
    ''' que organización gestiono la subida a blockchain '''
    organizacion = models.ForeignKey(
        Organizacion,
        null=True,
        blank=True,
        help_text='Por si es una organizacion que deba estar en nuestra DB')
    nombre = models.CharField(max_length=90, help_text='Nombre de Fantasía')
    url = models.URLField(null=True, blank=True)
    url_verificacion_general = models.URLField(
        null=True,
        blank=True,
        help_text='URL sugerida para verificar del intermediario')
    publicado = models.BooleanField(default=True)

    @property
    def nombre_entidad(self):
        nombre2 = '' if self.organizacion is None else ' ({})'.format(
            self.organizacion.nombre)
        return '{}{}'.format(self.nombre, nombre2)

    def __str__(self):
        return self.nombre_entidad

    class Meta:
        verbose_name = 'Intermediario para BlockChain'
        verbose_name_plural = 'Intermediarios para BlockChain'


class Wallet(models.Model):
    ''' en algunos casos podemos tener una wallet
        (propia o administrada por intermediario) que registra
        las transacciones como reemplazo (o complemento) de la firma digital '''
    nombre = models.CharField(max_length=120,
                              help_text='Nombre que le damos al Wallet')
    blockchain = models.ForeignKey(BlockChain, on_delete=models.CASCADE)
    intermediario = models.ForeignKey(
        IntermediarioBlockChain, null=True, blank=True)
    direccion = models.CharField(max_length=240,
                                 help_text='Direccion o ID único de la Wallet')
    url_wallet = models.URLField(
        max_length=450,
        null=True,
        blank=True,
        help_text='Una de las URLs válidas para ver datos de la wallet')
    observaciones_publicas = models.TextField(null=True, blank=True)
    publicado = models.BooleanField(default=True)

    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} {}'.format(self.nombre, self.direccion)

    class Meta:
        ordering = ['nombre']


class SmartContract(models.Model):
    ''' Contratos " " "Inteligentes " " " usados '''
    nombre = models.CharField(max_length=120,
                              help_text='Nombre que le damos al Contrato')
    blockchain = models.ForeignKey(BlockChain, on_delete=models.CASCADE)
    intermediario = models.ForeignKey(
        IntermediarioBlockChain,
        null=True,
        blank=True,
        help_text='Intermediario que definio o acompaña el contrato')
    url_tecnico = models.URLField(
        null=True,
        blank=True,
        help_text='Link al contrato en el blockchain')
    url_repo = models.URLField(
        null=True,
        blank=True,
        help_text='Link al repositorio de código donde abrimos a comentarios y sugerencias')
    observaciones_publicas = models.TextField(
        null=True,
        blank=True,
        help_text='Detalles del contrato comprensibles para el público')
    publicado = models.BooleanField(default=True)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']


class Bloque(models.Model):
    ''' bloque en un blockchain '''
    blockchain = models.ForeignKey(BlockChain, on_delete=models.CASCADE)
    id_bloque = models.CharField(max_length=250, null=True, blank=True,
                                 help_text='ID del bloque usado')
    url_bloque = models.URLField(
        max_length=450,
        null=True,
        blank=True,
        help_text='Una de las URLs válidas para ver datos deL bloque')
    publicado = models.BooleanField(default=True)

    def __str__(self):
        return '{} en {}'.format(self.id_bloque, self.blockchain.nombre)


class HashFunction(models.Model):
    ''' funciones hash usadas sobre los datos o transacciones a subir a algun Blockchain '''

    nombre = models.CharField(max_length=90)
    url = models.URLField(
        null=True,
        blank=True,
        help_text='Url con info de la funcion hash')
    publicado = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre


class ProveedoresReplicaDatos(models.Model):
    ''' sistemas o externos que guardan replica de datos '''
    nombre = models.CharField(max_length=90)
    url = models.URLField(null=True, blank=True, help_text='Url del proveedor')
    observaciones_publicas = models.TextField(null=True, blank=True)
    publicado = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre


class DocumentoCertificadoBlockChain(models.Model):
    ''' Documentos certificados en BlockChain '''
    nombre = models.CharField(max_length=90)

    recursos_incluidos = models.ManyToManyField(
        Recurso,
        blank=True,
        help_text='Lista de recursos en este sistema que fueron incluidos')
    archivo_comprimido = models.FileField(
        upload_to='documentos-certificados/',
        blank=True,
        help_text='Archivo final si hubiera un comprido que agrupe a otros')
    intermediario = models.ForeignKey(
        IntermediarioBlockChain,
        null=True,
        blank=True,
        help_text='Intermediario responsable de la subida')
    firmantes = models.ManyToManyField(
        FirmaDigital,
        blank=True,
        help_text='Personas u organizaciones firmantes del archivo final')
    funcion_hash = models.ForeignKey(
        HashFunction,
        help_text='Funcion Hash usada sobre el documento firmado')
    resultado_hash = models.TextField(
        help_text='Resultado de la funcion hash sobre el documento')
    sistema_firma = models.ForeignKey(SistemaFirma, null=True, blank=True)
    smart_contract = models.ForeignKey(
        SmartContract,
        null=True,
        blank=True,
        help_text='En caso de que el documento se haya subido vía un Smart Contract')
    wallet = models.ForeignKey(
        Wallet,
        null=True,
        blank=True,
        help_text='En caso de que se use un wallet que nos interese')
    # esto podría ir en el módulo de firma digital
    resultado_firma = models.TextField(
        null=True,
        blank=True,
        help_text='Resultado de la firma sobre el documento')
    bloque = models.ForeignKey(
        Bloque,
        null=True,
        blank=True,
        help_text='Bloque donde está el documento')
    publicado = models.BooleanField(default=True)
    url_verificacion = models.URLField(
        null=True,
        blank=True,
        help_text='URL sugerida para verificar este documento')
    archivo_verificacion = models.FileField(
        upload_to='archivos-verificacion/',
        null=True,
        blank=True,
        help_text='Archivo de ayuda de validación (OTS o similares)')
    uid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    creado = models.DateTimeField(auto_now=False, auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    importancia = models.PositiveIntegerField(
        default=100,
        help_text='Mas alto es más importante, para mostrar los mas relevantes en listas')

    info_extra_verificacion = models.TextField(
        null=True,
        blank=True,
        help_text='Ayuda oi detalles finales para la verificación de este documento')

    def __str__(self):
        return self.nombre

    def get_absolute_url(self):
        return reverse('blockchain.documento', kwargs={'uid': self.uid})

    class Meta:
        verbose_name = 'Documento certificado en BlockChain'
        verbose_name_plural = 'Documentos certificados en BlockChain'
        ordering = ['-importancia', '-creado']


class ReplicaDatos(models.Model):
    ''' cada una de las replicas en servidores externos de un documento subido '''
    documento = models.ForeignKey(
        DocumentoCertificadoBlockChain,
        null=True,
        on_delete=models.SET_NULL)
    proveedor = models.ForeignKey(
        ProveedoresReplicaDatos,
        null=True,
        on_delete=models.SET_NULL)

    url_replica = models.URLField(
        help_text='Link para ver los datos en la replica')
    observaciones_publicas = models.TextField(null=True, blank=True)
    publicado = models.BooleanField(default=True)
