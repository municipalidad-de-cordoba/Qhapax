from django.shortcuts import render
from blockchain.models import (BlockChain, IntermediarioBlockChain,
                               DocumentoCertificadoBlockChain,
                               ProveedoresReplicaDatos)
from django.views.decorators.cache import cache_page
from django.shortcuts import get_object_or_404


@cache_page(60 * 60)  # 1 h
def blockchain(request):
    ''' Cargar seccion especial "Blockchain" '''
    # pasar las redes de blockchain que usamos, las firmas digitales, los
    # intermediarios y algunos datos de ejemplo
    blockchains = BlockChain.objects.filter(publicado=True)
    intermediarios = IntermediarioBlockChain.objects.filter(publicado=True)
    proveedores_replica = ProveedoresReplicaDatos.objects.filter(
        publicado=True)
    documentos_en_blockchain = DocumentoCertificadoBlockChain.objects.filter(
        publicado=True)[:25]
    context = {'blockchains': blockchains, 'intermediarios': intermediarios,
               'documentos_en_blockchain': documentos_en_blockchain,
               'proveedores_replica': proveedores_replica}
    url = 'website/{}/blockchain.html'.format(request.website.template)
    return render(request, url, context)


@cache_page(60 * 60)  # 1 h
def documento_en_blockchain(request, uid):
    ''' Mostrar un documento que subimos a blockchain '''
    # pasar las redes de blockchain que usamos, las firmas digitales, los
    # intermediarios y algunos datos de ejemplo
    documento = get_object_or_404(
        DocumentoCertificadoBlockChain,
        uid=uid,
        publicado=True)

    # para simplificar la interpretacion:
    bloque = None if documento.bloque is None else documento.bloque
    blockchain = None if documento.bloque is None else documento.bloque.blockchain
    wallet = None if documento.wallet is None else documento.wallet

    context = {
        'documento': documento,
        'bloque': bloque,
        'blockchain': blockchain,
        'wallet': wallet}
    url = 'website/{}/blockchain-ficha-documento.html'.format(
        request.website.template)
    return render(request, url, context)
